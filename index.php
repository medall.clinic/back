<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("color_title", "questions-header--background-color--dark");
$APPLICATION->SetPageProperty("title", "Medall - клиника эстетической медицины и цифровой стоматологии");
$APPLICATION->SetPageProperty("description", "Клиника эстетической медицины и цифровой стоматологии Medall в Санкт-Петербурге представляет профессиональные услуги любой сложности. У нас вы можете провести операции подтяжки лица, липосакции, маммопластики и другие процедуры. Наши специалисты предоставляют персональный подход и помогают выбрать оптимальный метод лечения. Узнайте стоимость и сроки проведения операции на нашем сайте или по телефону: +7 (812) 603-02-01.");
$APPLICATION->SetTitle("Medall - клиника эстетической медицины и цифровой стоматологии");
include __DIR__."/home/banner.php";
include __DIR__."/home/numbers.php";
include __DIR__."/home/important.php";
include __DIR__."/home/directions.php";
include __DIR__."/home/category-directions.php";
include __DIR__."/home/form.php";
include __DIR__."/home/reasons.php";
include __DIR__."/home/staff.php";
include __DIR__."/home/feedback.php";
include __DIR__."/home/worlds.php";?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
