<?php


// входящий вебхук

$apiUrl = 'https://medall24.bitrix24.ru/rest/30/mqmuxc440l45s9fw/crm.lead.add.json';
$accessToken = 'qqqeee123'; //пароль от сайта;

// https://cp55162-bitrix-lpv3n.tw1.ru/rest/1/ id ответсвенного

// код вебхука i7egktog30g60i15 обязательно

// метод crm.lead.add обязательно


// формируем параметры для создания лида
if(!empty($_POST['title']) && !empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['second_name'])){
    $sourceDescription = 'Запрос ' . $_POST['title'];
    $sourceDescription .= "\n-------------------\n";
    $leadData = [
        'fields' => [
            "TITLE" => $_POST['title'],
            "NAME" => $_POST['name']. ' ' . $_POST['second_name'],
            "PHONE" => [
                "0" => [
                    "VALUE" => $_POST['phone'],
                    "VALUE_TYPE" => "WORK",
                ],
            ],
            'OPENED' => 'N', // Доступно для всех
            'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
            "UF_CRM_5D8E19ED4F970" => ["VALUE" => $_POST['email']],
            "COMMENTS" => $_POST['comments'],
            "SOURCE_DESCRIPTION" => $sourceDescription,
            'UTM_SOURCE'=>$_POST['utm_source'],

        ],
        'params' => ['REGISTER_SONET_EVENT' => 'Y']
    ];


// Преобразуем данные в формат JSON
$jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
$ch = curl_init($apiUrl);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
));
// Выполнение запроса
$response = curl_exec($ch);

// Проверка на ошибки
if ($response === false) {
    echo 'Ошибка cURL: ' . curl_error($ch);
} else {
    // Обработка ответа
    $responseData = json_decode($response, true);


}
$success = [
    'send' => 'yes',
];
echo json_encode($success);

// Закрытие cURL-соединения
curl_close($ch);
}
