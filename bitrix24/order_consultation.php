<?
require_once (__DIR__."/crest.php");

$request = $_POST;

$fields = [
	"TITLE" => "Новая запись на консультацию",
	"UF_CRM_1614098395219" => "Доктор: ".$request["doctor_full_name"],
	"NAME" => $request["name"],
	"UF_CRM_1614069566946" => $request["service"],
	"PHONE" => [
		"0" => [
			"VALUE" => $request["phone"],
			"VALUE_TYPE" => "WORK",
		],
	],
	"UF_CRM_5EDCEF508A591" => $request["connectionType"],
	"COMMENTS" => $request["comments"],
];

foreach ($request as $paramName => $paramValue){
	if(strpos($paramName, 'utm_') !== false){
		$paramName = strtoupper($paramName);
		$fields[$paramName] = $paramValue;
	}
}

$result = CRest::call("crm.lead.add", ["FIELDS" => $fields]);

exit(
	json_encode($result)
);