Array
(
[result] => Array
(
[ID] => Array
(
[type] => integer
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => ID
)

[TITLE] => Array
(
[type] => string
[isRequired] => 1
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Название лида
)

[HONORIFIC] => Array
(
[type] => crm_status
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[statusType] => HONORIFIC
[title] => Обращение
)

[NAME] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Имя
)

[SECOND_NAME] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Отчество
)

[LAST_NAME] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Фамилия
)

[BIRTHDATE] => Array
(
[type] => date
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Дата рождения
)

[COMPANY_TITLE] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Название компании
)

[SOURCE_ID] => Array
(
[type] => crm_status
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[statusType] => SOURCE
[title] => Источник
)

[SOURCE_DESCRIPTION] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Дополнительно об источнике
)

[STATUS_ID] => Array
(
[type] => crm_status
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[statusType] => STATUS
[title] => Статус
)

[STATUS_DESCRIPTION] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Дополнительно о статусе
)

[STATUS_SEMANTIC_ID] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Состояние статуса
)

[POST] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Должность
)

[ADDRESS] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Адрес
)

[ADDRESS_2] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Адрес (стр. 2)
)

[ADDRESS_CITY] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Город
)

[ADDRESS_POSTAL_CODE] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Почтовый индекс
)

[ADDRESS_REGION] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Район
)

[ADDRESS_PROVINCE] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Область
)

[ADDRESS_COUNTRY] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Страна
)

[ADDRESS_COUNTRY_CODE] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Код страны
)

[ADDRESS_LOC_ADDR_ID] => Array
(
[type] => integer
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Идентификатор адреса местоположения
)

[CURRENCY_ID] => Array
(
[type] => crm_currency
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Валюта
)

[OPPORTUNITY] => Array
(
[type] => double
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Сумма
)

[OPENED] => Array
(
[type] => char
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Доступен для всех
)

[COMMENTS] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Комментарий
)

[HAS_PHONE] => Array
(
[type] => char
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Задан телефон
)

[HAS_EMAIL] => Array
(
[type] => char
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Задан e-mail
)

[HAS_IMOL] => Array
(
[type] => char
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Задана открытая линия
)

[ASSIGNED_BY_ID] => Array
(
[type] => user
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Ответственный
)

[CREATED_BY_ID] => Array
(
[type] => user
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Кем создан
)

[MODIFY_BY_ID] => Array
(
[type] => user
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Кем изменен
)

[DATE_CREATE] => Array
(
[type] => datetime
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Дата создания
)

[DATE_MODIFY] => Array
(
[type] => datetime
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Дата изменения
)

[COMPANY_ID] => Array
(
[type] => crm_company
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Компания
)

[CONTACT_ID] => Array
(
[type] => crm_contact
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[isDeprecated] => 1
[title] => Контакт
)

[CONTACT_IDS] => Array
(
[type] => crm_contact
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] =>
[title] => CONTACT_IDS
)

[IS_RETURN_CUSTOMER] => Array
(
[type] => char
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Повторный лид
)

[DATE_CLOSED] => Array
(
[type] => datetime
[isRequired] =>
[isReadOnly] => 1
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Дата завершения
)

[ORIGINATOR_ID] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Внешний источник
)

[ORIGIN_ID] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Идентификатор элемента во внешнем источнике
)

[UTM_SOURCE] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Рекламная система
)

[UTM_MEDIUM] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Тип трафика
)

[UTM_CAMPAIGN] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Обозначение рекламной кампании
)

[UTM_CONTENT] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Содержание кампании
)

[UTM_TERM] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] =>
[title] => Условие поиска кампании
)

[PHONE] => Array
(
[type] => crm_multifield
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] =>
[title] => Телефон
)

[EMAIL] => Array
(
[type] => crm_multifield
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] =>
[title] => E-mail
)

[WEB] => Array
(
[type] => crm_multifield
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] =>
[title] => Сайт
)

[IM] => Array
(
[type] => crm_multifield
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] =>
[title] => Мессенджер
)

[UF_CRM_5D8E19ED432E2] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] => 1
[title] => UF_CRM_5D8E19ED432E2
[listLabel] => Телефон
[formLabel] => Телефон
[filterLabel] => Телефон
)

[UF_CRM_5D8E19ED4F970] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] => 1
[isDynamic] => 1
[title] => UF_CRM_5D8E19ED4F970
[listLabel] => E-mail
[formLabel] => E-mail
[filterLabel] => E-mail
)

[UF_CRM_TRANID] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_TRANID
[listLabel] => UF_CRM_TRANID
[formLabel] => UF_CRM_TRANID
[filterLabel] => UF_CRM_TRANID
)

[UF_CRM_COOKIES] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_COOKIES
[listLabel] => UF_CRM_COOKIES
[formLabel] => UF_CRM_COOKIES
[filterLabel] => UF_CRM_COOKIES
)

[UF_CRM_1569916256965] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_1569916256965
[listLabel] => Источник заявки
[formLabel] => Источник заявки
[filterLabel] => Источник заявки
)

[UF_CRM_SECRET] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_SECRET
[listLabel] => Секретный ключ
[formLabel] => Секретный ключ
[filterLabel] => UF_CRM_SECRET
)

[UF_CRM_UTMNETWORK] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_UTMNETWORK
[listLabel] => UF_CRM_UTMNETWORK
[formLabel] => UF_CRM_UTMNETWORK
[filterLabel] => UF_CRM_UTMNETWORK
)

[UF_CRM_UTMPLACEMENT] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_UTMPLACEMENT
[listLabel] => UF_CRM_UTMPLACEMENT
[formLabel] => UF_CRM_UTMPLACEMENT
[filterLabel] => UF_CRM_UTMPLACEMENT
)

[UF_CRM_UTMPHRASE] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_UTMPHRASE
[listLabel] => UF_CRM_UTMPHRASE
[formLabel] => UF_CRM_UTMPHRASE
[filterLabel] => UF_CRM_UTMPHRASE
)

[UF_CRM_UTMGBID] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_UTMGBID
[listLabel] => UF_CRM_UTMGBID
[formLabel] => UF_CRM_UTMGBID
[filterLabel] => UF_CRM_UTMGBID
)

[UF_CRM_UTMREGION] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_UTMREGION
[listLabel] => UF_CRM_UTMREGION
[formLabel] => UF_CRM_UTMREGION
[filterLabel] => UF_CRM_UTMREGION
)

[UF_CRM_5DB1E3DC86752] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 76
[VALUE] => менее 50
)

[1] => Array
(
[ID] => 78
[VALUE] => 50-250
)

[2] => Array
(
[ID] => 80
[VALUE] => 250-500
)

[3] => Array
(
[ID] => 82
[VALUE] => более 500
)

)

[title] => UF_CRM_5DB1E3DC86752
[listLabel] => Кол-во сотрудников
[formLabel] => Кол-во сотрудников
[filterLabel] => Кол-во сотрудников
)

[UF_CRM_5E70F568A653B] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 140
[VALUE] => Да
)

[1] => Array
(
[ID] => 142
[VALUE] => Нет
)

)

[title] => UF_CRM_5E70F568A653B
[listLabel] => Выезжали ли вы из Российской Федерации за последние 3 недели?
[formLabel] => Выезжали ли вы из Российской Федерации за последние 3 недели?
[filterLabel] => Выезжали ли вы из Российской Федерации за последние 3 недели?
)

[UF_CRM_5E70F568BC68D] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 144
[VALUE] => Да
)

[1] => Array
(
[ID] => 146
[VALUE] => Нет
)

)

[title] => UF_CRM_5E70F568BC68D
[listLabel] => Есть ли у Вас либо лиц, с которыми Вы близко контактировали следующие симптомы: слабость, повышение температуры, затрудненное дыхание, сухой кашель, головная боль, тяжесть в грудной клетке, боль в горле
[formLabel] => Есть ли у Вас либо лиц, с которыми Вы близко контактировали следующие симптомы: слабость, повышение температуры, затрудненное дыхание, сухой кашель, головная боль, тяжесть в грудной клетке, боль в горле
[filterLabel] => Есть ли у Вас либо лиц, с которыми Вы близко контактировали следующие симптомы: слабость, повышение температуры, затрудненное дыхание, сухой кашель, головная боль, тяжесть в грудной клетке, боль в горле
)

[UF_CRM_5E70F568CE627] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 148
[VALUE] => Да
)

[1] => Array
(
[ID] => 150
[VALUE] => Нет
)

)

[title] => UF_CRM_5E70F568CE627
[listLabel] => Был ли у Вас контакт с лицами, заболевшими COVID-2019?
[formLabel] => Был ли у Вас контакт с лицами, заболевшими COVID-2019?
[filterLabel] => Был ли у Вас контакт с лицами, заболевшими COVID-2019?
)

[UF_CRM_5E99C2EFB6515] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5E99C2EFB6515
[listLabel] => Мы ценим Ваше время и безопасность, поэтому просим Вас указать данные участников обследования для подготовки документов
[formLabel] => Мы ценим Ваше время и безопасность, поэтому просим Вас указать данные участников обследования для подготовки документов
[filterLabel] => Мы ценим Ваше время и безопасность, поэтому просим Вас указать данные участников обследования для подготовки документов
)

[UF_CRM_1587135841973] => Array
(
[type] => address
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_1587135841973
[listLabel] => Адрес проведения исследования
[formLabel] => Адрес проведения исследования
[filterLabel] => Адрес проведения исследования
)

[UF_CRM_5E99CBBCC6BEA] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5E99CBBCC6BEA
[listLabel] => ФИО, дата рождения, номер паспорта, кем и когда выдан, место жительства
[formLabel] => ФИО, дата рождения, номер паспорта, кем и когда выдан, место жительства
[filterLabel] => ФИО, дата рождения, номер паспорта, кем и когда выдан, место жительства
)

[UF_CRM_1587147161463] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 199
[VALUE] => Анализ на COVID-19 2250 руб./чел
)

[1] => Array
(
[ID] => 201
[VALUE] => Анализ на COVID-19 + осмотр терапевта 4550 руб./чел
)

)

[title] => UF_CRM_1587147161463
[listLabel] => Вид услуги
[formLabel] => Вид услуги
[filterLabel] => Вид услуги
)

[UF_CRM_5EA004B88E556] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EA004B88E556
[listLabel] => Количество обследуемых лиц
[formLabel] => Количество обследуемых лиц
[filterLabel] => Количество обследуемых лиц
)

[UF_CRM_5ED8BCF45D0AA] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5ED8BCF45D0AA
[listLabel] => Номер паспорта
[formLabel] => Номер паспорта
[filterLabel] => Номер паспорта
)

[UF_CRM_5ED8BCF474B09] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5ED8BCF474B09
[listLabel] => Кем выдан паспорт, когда
[formLabel] => Кем выдан паспорт, когда
[filterLabel] => Кем выдан паспорт, когда
)

[UF_CRM_5ED8BCF48353C] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 278
[VALUE] => муж
)

[1] => Array
(
[ID] => 280
[VALUE] => жен
)

)

[title] => UF_CRM_5ED8BCF48353C
[listLabel] => Пол
[formLabel] => Пол
[filterLabel] => Пол
)

[UF_CRM_5ED8BCF498E44] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5ED8BCF498E44
[listLabel] => Гражданство
[formLabel] => Гражданство
[filterLabel] => Гражданство
)

[UF_CRM_5ED8BCF4A8C34] => Array
(
[type] => address
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5ED8BCF4A8C34
[listLabel] => Адрес регистрации
[formLabel] => Адрес регистрации
[filterLabel] => Адрес регистрации
)

[UF_CRM_5ED8BCF4BB2A3] => Array
(
[type] => address
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5ED8BCF4BB2A3
[listLabel] => Фактический адрес проживания
[formLabel] => Фактический адрес проживания
[filterLabel] => Фактический адрес проживания
)

[UF_CRM_5ED8BCF4CA35A] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5ED8BCF4CA35A
[listLabel] => email для отправки результатов
[formLabel] => email для отправки результатов
[filterLabel] => email для отправки результатов
)

[UF_CRM_5EDCEDD03BD2C] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 568
[VALUE] => Да
)

[1] => Array
(
[ID] => 570
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD03BD2C
[listLabel] => Заболевания сердца
[formLabel] => Заболевания сердца
[filterLabel] => Заболевания сердца
)

[UF_CRM_5EDCEDD04E505] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 572
[VALUE] => Да
)

[1] => Array
(
[ID] => 574
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD04E505
[listLabel] => Инфаркт миокарда
[formLabel] => Инфаркт миокарда
[filterLabel] => Инфаркт миокарда
)

[UF_CRM_5EDCEDD05BE73] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 576
[VALUE] => Да
)

[1] => Array
(
[ID] => 578
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD05BE73
[listLabel] => Наличие кардиостимулятора
[formLabel] => Наличие кардиостимулятора
[filterLabel] => Наличие кардиостимулятора
)

[UF_CRM_5EDCEDD0687A1] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 580
[VALUE] => Да
)

[1] => Array
(
[ID] => 582
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0687A1
[listLabel] => Заболевания сосудов
[formLabel] => Заболевания сосудов
[filterLabel] => Заболевания сосудов
)

[UF_CRM_5EDCEDD075873] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 584
[VALUE] => Да
)

[1] => Array
(
[ID] => 586
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD075873
[listLabel] => Инсульт
[formLabel] => Инсульт
[filterLabel] => Инсульт
)

[UF_CRM_5EDCEDD0830C5] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 588
[VALUE] => Нет
)

[1] => Array
(
[ID] => 590
[VALUE] => Повышение
)

[2] => Array
(
[ID] => 592
[VALUE] => Понижение
)

[3] => Array
(
[ID] => 594
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD0830C5
[listLabel] => Повышение или понижение артериального давления
[formLabel] => Повышение или понижение артериального давления
[filterLabel] => Повышение или понижение артериального давления
)

[UF_CRM_5EDCEDD09167F] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 596
[VALUE] => Да
)

[1] => Array
(
[ID] => 598
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD09167F
[listLabel] => Заболевания легких
[formLabel] => Заболевания легких
[filterLabel] => Заболевания легких
)

[UF_CRM_5EDCEDD0A1BAF] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 600
[VALUE] => Да
)

[1] => Array
(
[ID] => 602
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0A1BAF
[listLabel] => Бронхиальная астма
[formLabel] => Бронхиальная астма
[filterLabel] => Бронхиальная астма
)

[UF_CRM_5EDCEDD0B0285] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 604
[VALUE] => Да
)

[1] => Array
(
[ID] => 606
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0B0285
[listLabel] => Заболевания желудочно-кишечного тракта
[formLabel] => Заболевания желудочно-кишечного тракта
[filterLabel] => Заболевания желудочно-кишечного тракта
)

[UF_CRM_5EDCEDD0BF9BD] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 608
[VALUE] => Да
)

[1] => Array
(
[ID] => 610
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0BF9BD
[listLabel] => Заболевания печени
[formLabel] => Заболевания печени
[filterLabel] => Заболевания печени
)

[UF_CRM_5EDCEDD0D16B1] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 612
[VALUE] => Да
)

[1] => Array
(
[ID] => 614
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0D16B1
[listLabel] => Заболевания почек
[formLabel] => Заболевания почек
[filterLabel] => Заболевания почек
)

[UF_CRM_5EDCEDD0E239E] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 616
[VALUE] => Да
)

[1] => Array
(
[ID] => 618
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0E239E
[listLabel] => Заболевания щитовидной, паращитовидной железы, др. желез
[formLabel] => Заболевания щитовидной, паращитовидной железы, др. желез
[filterLabel] => Заболевания щитовидной, паращитовидной железы, др. желез
)

[UF_CRM_5EDCEDD0F33DE] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 620
[VALUE] => Да
)

[1] => Array
(
[ID] => 622
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD0F33DE
[listLabel] => Сахарный диабет
[formLabel] => Сахарный диабет
[filterLabel] => Сахарный диабет
)

[UF_CRM_5EDCEDD10BB33] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 624
[VALUE] => Да
)

[1] => Array
(
[ID] => 626
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD10BB33
[listLabel] => Травмы
[formLabel] => Травмы
[filterLabel] => Травмы
)

[UF_CRM_5EDCEDD11807A] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 628
[VALUE] => Да
)

[1] => Array
(
[ID] => 630
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD11807A
[listLabel] => Сотрясение головного мозга
[formLabel] => Сотрясение головного мозга
[filterLabel] => Сотрясение головного мозга
)

[UF_CRM_5EDCEDD124119] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 632
[VALUE] => Да
)

[1] => Array
(
[ID] => 634
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD124119
[listLabel] => Эпилепсия и др. заболевания центральной и периферической нервной системы
[formLabel] => Эпилепсия и др. заболевания центральной и периферической нервной системы
[filterLabel] => Эпилепсия и др. заболевания центральной и периферической нервной системы
)

[UF_CRM_5EDCEDD1312DC] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 636
[VALUE] => Да
)

[1] => Array
(
[ID] => 638
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1312DC
[listLabel] => Заболевание крови
[formLabel] => Заболевание крови
[filterLabel] => Заболевание крови
)

[UF_CRM_5EDCEDD13E78D] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 640
[VALUE] => Да
)

[1] => Array
(
[ID] => 642
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD13E78D
[listLabel] => Нарушение свертываемости крови
[formLabel] => Нарушение свертываемости крови
[filterLabel] => Нарушение свертываемости крови
)

[UF_CRM_5EDCEDD14C89A] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 644
[VALUE] => Да
)

[1] => Array
(
[ID] => 646
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD14C89A
[listLabel] => Заболевания ЛОР органов (уха, горла, носа)
[formLabel] => Заболевания ЛОР органов (уха, горла, носа)
[filterLabel] => Заболевания ЛОР органов (уха, горла, носа)
)

[UF_CRM_5EDCEDD15A504] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 648
[VALUE] => Да
)

[1] => Array
(
[ID] => 650
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD15A504
[listLabel] => Заболевание костной системы, суставов
[formLabel] => Заболевание костной системы, суставов
[filterLabel] => Заболевание костной системы, суставов
)

[UF_CRM_5EDCEDD167C82] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 652
[VALUE] => Да
)

[1] => Array
(
[ID] => 654
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD167C82
[listLabel] => Заболевания кожи
[formLabel] => Заболевания кожи
[filterLabel] => Заболевания кожи
)

[UF_CRM_5EDCEDD17535A] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 656
[VALUE] => Да
)

[1] => Array
(
[ID] => 658
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD17535A
[listLabel] => Нейродермит
[formLabel] => Нейродермит
[filterLabel] => Нейродермит
)

[UF_CRM_5EDCEDD18251A] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 660
[VALUE] => Да
)

[1] => Array
(
[ID] => 662
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD18251A
[listLabel] => ВИЧ
[formLabel] => ВИЧ
[filterLabel] => ВИЧ
)

[UF_CRM_5EDCEDD190FE1] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 664
[VALUE] => Да
)

[1] => Array
(
[ID] => 666
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD190FE1
[listLabel] => Гепатит
[formLabel] => Гепатит
[filterLabel] => Гепатит
)

[UF_CRM_5EDCEDD1A06CC] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 668
[VALUE] => Да
)

[1] => Array
(
[ID] => 670
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1A06CC
[listLabel] => Грибковые заболевания (были, есть)
[formLabel] => Грибковые заболевания (были, есть)
[filterLabel] => Грибковые заболевания (были, есть)
)

[UF_CRM_5EDCEDD1AEF28] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 672
[VALUE] => Да
)

[1] => Array
(
[ID] => 674
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1AEF28
[listLabel] => Была ли длительная необъяснимая лихорадка
[formLabel] => Была ли длительная необъяснимая лихорадка
[filterLabel] => Была ли длительная необъяснимая лихорадка
)

[UF_CRM_5EDCEDD1BDE8E] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 676
[VALUE] => Да
)

[1] => Array
(
[ID] => 678
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1BDE8E
[listLabel] => Постоянно увеличены лимфатические узлы (железы)
[formLabel] => Постоянно увеличены лимфатические узлы (железы)
[filterLabel] => Постоянно увеличены лимфатические узлы (железы)
)

[UF_CRM_5EDCEDD1CCBDE] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 680
[VALUE] => Да
)

[1] => Array
(
[ID] => 682
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1CCBDE
[listLabel] => Беспричинные головные боли
[formLabel] => Беспричинные головные боли
[filterLabel] => Беспричинные головные боли
)

[UF_CRM_5EDCEDD1DA77B] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 684
[VALUE] => Да
)

[1] => Array
(
[ID] => 686
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1DA77B
[listLabel] => Отмечалась ли потеря веса в последние 6 месяцев
[formLabel] => Отмечалась ли потеря веса в последние 6 месяцев
[filterLabel] => Отмечалась ли потеря веса в последние 6 месяцев
)

[UF_CRM_5EDCEDD1E9E52] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 688
[VALUE] => Да
)

[1] => Array
(
[ID] => 690
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD1E9E52
[listLabel] => Венерические заболевания
[formLabel] => Венерические заболевания
[filterLabel] => Венерические заболевания
)

[UF_CRM_5EDCEDD20319B] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 692
[VALUE] => Да
)

[1] => Array
(
[ID] => 694
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD20319B
[listLabel] => Являетесь ли Вы донором
[formLabel] => Являетесь ли Вы донором
[filterLabel] => Являетесь ли Вы донором
)

[UF_CRM_5EDCEDD210B19] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 696
[VALUE] => Да
)

[1] => Array
(
[ID] => 698
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD210B19
[listLabel] => Проводились ли переливания крови
[formLabel] => Проводились ли переливания крови
[filterLabel] => Проводились ли переливания крови
)

[UF_CRM_5EDCEDD21EF15] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 700
[VALUE] => Да
)

[1] => Array
(
[ID] => 702
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD21EF15
[listLabel] => Инфекционные заболевания
[formLabel] => Инфекционные заболевания
[filterLabel] => Инфекционные заболевания
)

[UF_CRM_5EDCEDD22D9B2] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 704
[VALUE] => Да
)

[1] => Array
(
[ID] => 706
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD22D9B2
[listLabel] => Проводилась ли лучевая терапия, химиотерапия за последние 10 лет
[formLabel] => Проводилась ли лучевая терапия, химиотерапия за последние 10 лет
[filterLabel] => Проводилась ли лучевая терапия, химиотерапия за последние 10 лет
)

[UF_CRM_5EDCEDD23E24E] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 708
[VALUE] => Да
)

[1] => Array
(
[ID] => 710
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD23E24E
[listLabel] => Другие заболевания
[formLabel] => Другие заболевания
[filterLabel] => Другие заболевания
)

[UF_CRM_5EDCEDD24BBE8] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 712
[VALUE] => Да
)

[1] => Array
(
[ID] => 714
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD24BBE8
[listLabel] => Ваша работа связана (ранее или сейчас) с вредными факторами: химическими, физическими (излучения, высокое давление, вибрации и т.д.)
[formLabel] => Ваша работа связана (ранее или сейчас) с вредными факторами: химическими, физическими (излучения, высокое давление, вибрации и т.д.)
[filterLabel] => Ваша работа связана (ранее или сейчас) с вредными факторами: химическими, физическими (излучения, высокое давление, вибрации и т.д.)
)

[UF_CRM_5EDCEDD25E743] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 716
[VALUE] => Да
)

[1] => Array
(
[ID] => 718
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEDD25E743
[listLabel] => Бывает ли головокружение, потеря сознания, одышка при введении анестетиков или др. лекарственных препаратов
[formLabel] => Бывает ли головокружение, потеря сознания, одышка при введении анестетиков или др. лекарственных препаратов
[filterLabel] => Бывает ли головокружение, потеря сознания, одышка при введении анестетиков или др. лекарственных препаратов
)

[UF_CRM_5EDCEDD26E23B] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 720
[VALUE] => Нет
)

[1] => Array
(
[ID] => 722
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD26E23B
[listLabel] => Есть ли инвалидность
[formLabel] => Есть ли инвалидность
[filterLabel] => Есть ли инвалидность
)

[UF_CRM_5EDCEDD27BA0D] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 724
[VALUE] => Нет
)

[1] => Array
(
[ID] => 726
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD27BA0D
[listLabel] => на местные анестетики
[formLabel] => на местные анестетики
[filterLabel] => на местные анестетики
)

[UF_CRM_5EDCEDD289A35] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 728
[VALUE] => Нет
)

[1] => Array
(
[ID] => 730
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD289A35
[listLabel] => на антибиотики
[formLabel] => на антибиотики
[filterLabel] => на антибиотики
)

[UF_CRM_5EDCEDD2979AC] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 732
[VALUE] => Нет
)

[1] => Array
(
[ID] => 734
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2979AC
[listLabel] => на сульфанилимиды
[formLabel] => на сульфанилимиды
[filterLabel] => на сульфанилимиды
)

[UF_CRM_5EDCEDD2A7B52] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 736
[VALUE] => Нет
)

[1] => Array
(
[ID] => 738
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2A7B52
[listLabel] => на препараты йода
[formLabel] => на препараты йода
[filterLabel] => на препараты йода
)

[UF_CRM_5EDCEDD2B6125] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 740
[VALUE] => Нет
)

[1] => Array
(
[ID] => 742
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2B6125
[listLabel] => на гормональные препараты
[formLabel] => на гормональные препараты
[filterLabel] => на гормональные препараты
)

[UF_CRM_5EDCEDD2C5704] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 744
[VALUE] => Нет
)

[1] => Array
(
[ID] => 746
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2C5704
[listLabel] => на другие лекарственные препараты
[formLabel] => на другие лекарственные препараты
[filterLabel] => на другие лекарственные препараты
)

[UF_CRM_5EDCEDD2D36DC] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 748
[VALUE] => Нет
)

[1] => Array
(
[ID] => 750
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2D36DC
[listLabel] => на пыльцу и растения
[formLabel] => на пыльцу и растения
[filterLabel] => на пыльцу и растения
)

[UF_CRM_5EDCEDD2E1FC6] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 752
[VALUE] => Нет
)

[1] => Array
(
[ID] => 754
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2E1FC6
[listLabel] => на пищевые продукты
[formLabel] => на пищевые продукты
[filterLabel] => на пищевые продукты
)

[UF_CRM_5EDCEDD2F118B] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 756
[VALUE] => Нет
)

[1] => Array
(
[ID] => 758
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD2F118B
[listLabel] => на шерсть животных
[formLabel] => на шерсть животных
[filterLabel] => на шерсть животных
)

[UF_CRM_5EDCEDD30AE44] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 760
[VALUE] => Нет
)

[1] => Array
(
[ID] => 762
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD30AE44
[listLabel] => на другие вещества
[formLabel] => на другие вещества
[filterLabel] => на другие вещества
)

[UF_CRM_5EDCEDD319A40] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEDD319A40
[listLabel] => Постоянно или периодически принимаю лекарственные препараты, какие
[formLabel] => Постоянно или периодически принимаю лекарственные препараты, какие
[filterLabel] => Постоянно или периодически принимаю лекарственные препараты, какие
)

[UF_CRM_5EDCEDD324DA4] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 764
[VALUE] => Нет
)

[1] => Array
(
[ID] => 766
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEDD324DA4
[listLabel] => Проводилось ли лечение иных заболеваний за прошедшие дни, недели, месяцы
[formLabel] => Проводилось ли лечение иных заболеваний за прошедшие дни, недели, месяцы
[filterLabel] => Проводилось ли лечение иных заболеваний за прошедшие дни, недели, месяцы
)

[UF_CRM_5EDCEDD333A32] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEDD333A32
[listLabel] => Состою на учете в лечебном учреждении
[formLabel] => Состою на учете в лечебном учреждении
[filterLabel] => Состою на учете в лечебном учреждении
)

[UF_CRM_5EDCEDD33EB03] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEDD33EB03
[listLabel] => Последнее общемедицинское обследование проводилось, дата
[formLabel] => Последнее общемедицинское обследование проводилось, дата
[filterLabel] => Последнее общемедицинское обследование проводилось, дата
)

[UF_CRM_5EDCEF4F1982F] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 968
[VALUE] => Нет
)

[1] => Array
(
[ID] => 970
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F1982F
[listLabel] => - Аллергические реакции
[formLabel] => - Аллергические реакции
[filterLabel] => - Аллергические реакции
)

[UF_CRM_5EDCEF4F303EA] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 972
[VALUE] => Нет
)

[1] => Array
(
[ID] => 974
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F303EA
[listLabel] => - Продолжительное кровотечение
[formLabel] => - Продолжительное кровотечение
[filterLabel] => - Продолжительное кровотечение
)

[UF_CRM_5EDCEF4F42B33] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 976
[VALUE] => Нет
)

[1] => Array
(
[ID] => 978
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F42B33
[listLabel] => - Потеря сознания
[formLabel] => - Потеря сознания
[filterLabel] => - Потеря сознания
)

[UF_CRM_5EDCEF4F51C08] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEF4F51C08
[listLabel] => - Какие-либо другие осложнения во время и после лечения, какие
[formLabel] => - Какие-либо другие осложнения во время и после лечения, какие
[filterLabel] => - Какие-либо другие осложнения во время и после лечения, какие
)

[UF_CRM_5EDCEF4F5F594] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 980
[VALUE] => Нет
)

[1] => Array
(
[ID] => 982
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F5F594
[listLabel] => Наличие болей и щелканье в нижнечелюстном суставе
[formLabel] => Наличие болей и щелканье в нижнечелюстном суставе
[filterLabel] => Наличие болей и щелканье в нижнечелюстном суставе
)

[UF_CRM_5EDCEF4F70C4F] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 984
[VALUE] => Нет
)

[1] => Array
(
[ID] => 986
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F70C4F
[listLabel] => Кровоточивость десен при чистке зубов (периодическая, постоянная)
[formLabel] => Кровоточивость десен при чистке зубов (периодическая, постоянная)
[filterLabel] => Кровоточивость десен при чистке зубов (периодическая, постоянная)
)

[UF_CRM_5EDCEF4F818C6] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEF4F818C6
[listLabel] => Появление герпеса «простуды» на губах с периодичностью в год
[formLabel] => Появление герпеса «простуды» на губах с периодичностью в год
[filterLabel] => Появление герпеса «простуды» на губах с периодичностью в год
)

[UF_CRM_5EDCEF4F90454] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 988
[VALUE] => Нет
)

[1] => Array
(
[ID] => 990
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F90454
[listLabel] => Появление трещин губ, заед
[formLabel] => Появление трещин губ, заед
[filterLabel] => Появление трещин губ, заед
)

[UF_CRM_5EDCEF4F9F2F7] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 992
[VALUE] => Нет
)

[1] => Array
(
[ID] => 994
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4F9F2F7
[listLabel] => Изменилось положение губы (верхней, нижней) или изменилась улыбка
[formLabel] => Изменилось положение губы (верхней, нижней) или изменилась улыбка
[filterLabel] => Изменилось положение губы (верхней, нижней) или изменилась улыбка
)

[UF_CRM_5EDCEF4FAF42E] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 996
[VALUE] => Нет
)

[1] => Array
(
[ID] => 998
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4FAF42E
[listLabel] => Бруксизм (ночное скрежетание зубов)
[formLabel] => Бруксизм (ночное скрежетание зубов)
[filterLabel] => Бруксизм (ночное скрежетание зубов)
)

[UF_CRM_5EDCEF4FBECDB] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1000
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1002
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4FBECDB
[listLabel] => Периодическое появление язв в полости рта
[formLabel] => Периодическое появление язв в полости рта
[filterLabel] => Периодическое появление язв в полости рта
)

[UF_CRM_5EDCEF4FCD494] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1004
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1006
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4FCD494
[listLabel] => Хотелось бы изменить цвет или форму зубов
[formLabel] => Хотелось бы изменить цвет или форму зубов
[filterLabel] => Хотелось бы изменить цвет или форму зубов
)

[UF_CRM_5EDCEF4FDDE46] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1008
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1010
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4FDDE46
[listLabel] => Периодическая или постоянная сухость во рту
[formLabel] => Периодическая или постоянная сухость во рту
[filterLabel] => Периодическая или постоянная сухость во рту
)

[UF_CRM_5EDCEF4FEC6AC] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1012
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1014
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF4FEC6AC
[listLabel] => Чувствую запах изо рта
[formLabel] => Чувствую запах изо рта
[filterLabel] => Чувствую запах изо рта
)

[UF_CRM_5EDCEF5009FCF] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1016
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1018
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF5009FCF
[listLabel] => Курение
[formLabel] => Курение
[filterLabel] => Курение
)

[UF_CRM_5EDCEF5019228] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1020
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1022
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF5019228
[listLabel] => Алкоголь
[formLabel] => Алкоголь
[filterLabel] => Алкоголь
)

[UF_CRM_5EDCEF502A817] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1024
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1026
[VALUE] => Да
)

[2] => Array
(
[ID] => 1028
[VALUE] => В прошлом
)

)

[title] => UF_CRM_5EDCEF502A817
[listLabel] => Наркотические вещества
[formLabel] => Наркотические вещества
[filterLabel] => Наркотические вещества
)

[UF_CRM_5EDCEF5039FCC] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1030
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1032
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF5039FCC
[listLabel] => Беременны ли Вы
[formLabel] => Беременны ли Вы
[filterLabel] => Беременны ли Вы
)

[UF_CRM_5EDCEF50488D5] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1034
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1036
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF50488D5
[listLabel] => Являетесь ли Вы кормящей матерью
[formLabel] => Являетесь ли Вы кормящей матерью
[filterLabel] => Являетесь ли Вы кормящей матерью
)

[UF_CRM_5EDCEF505A6E1] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1038
[VALUE] => Нет
)

[1] => Array
(
[ID] => 1040
[VALUE] => Да
)

)

[title] => UF_CRM_5EDCEF505A6E1
[listLabel] => Имеется ли нарушение менструального цикла
[formLabel] => Имеется ли нарушение менструального цикла
[filterLabel] => Имеется ли нарушение менструального цикла
)

[UF_CRM_5EDCEF5069DA3] => Array
(
[type] => enumeration
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[items] => Array
(
[0] => Array
(
[ID] => 1042
[VALUE] => Да
)

[1] => Array
(
[ID] => 1044
[VALUE] => Нет
)

)

[title] => UF_CRM_5EDCEF5069DA3
[listLabel] => Постоянно или периодически принимаете противозачаточные препараты
[formLabel] => Постоянно или периодически принимаете противозачаточные препараты
[filterLabel] => Постоянно или периодически принимаете противозачаточные препараты
)

[UF_CRM_5EDCEF507B684] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEF507B684
[listLabel] => Последнее посещение врача-гинеколога
[formLabel] => Последнее посещение врача-гинеколога
[filterLabel] => Последнее посещение врача-гинеколога
)

[UF_CRM_5EDCEF508A591] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEF508A591
[listLabel] => Предпочитаемый способ связи
[formLabel] => Предпочитаемый способ связи
[filterLabel] => Предпочитаемый способ связи
)

[UF_CRM_5EDCEF5096404] => Array
(
[type] => date
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEF5096404
[listLabel] => Дата заполнения анкеты
[formLabel] => Дата заполнения анкеты
[filterLabel] => Дата заполнения анкеты
)

[UF_CRM_5EDCEF50A251A] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCEF50A251A
[listLabel] => ФИО законного представителя (для пациентов моложе 15 лет) 15 лет
[formLabel] => ФИО законного представителя (для пациентов моложе 15 лет) 15 лет
[filterLabel] => ФИО законного представителя (для пациентов моложе 15 лет) 15 лет
)

[UF_CRM_5EDCF320400D9] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_5EDCF320400D9
[listLabel] => ФИО законного представителя (для пациентов моложе 15 лет)
[formLabel] => ФИО законного представителя (для пациентов моложе 15 лет)
[filterLabel] => ФИО законного представителя (для пациентов моложе 15 лет)
)

[UF_CRM_ZOOM_MEET_LINK] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_ZOOM_MEET_LINK
[listLabel] => Ссылка на Zoom
[formLabel] => Ссылка на Zoom
[filterLabel] => UF_CRM_ZOOM_MEET_LINK
)

[UF_CRM_ZOOM_MEET_TZ] => Array
(
[type] => string
[isRequired] =>
[isReadOnly] =>
[isImmutable] =>
[isMultiple] =>
[isDynamic] => 1
[title] => UF_CRM_ZOOM_MEET_TZ
[listLabel] => Дата/время Zoom
[formLabel] => Дата/время Zoom
[filterLabel] => UF_CRM_ZOOM_MEET_TZ
)

)

[time] => Array
(
[start] => 1614068636.4187
[finish] => 1614068636.4532
[duration] => 0.034554004669189
[processing] => 0.0062251091003418
[date_start] => 2021-02-23T11:23:56+03:00
[date_finish] => 2021-02-23T11:23:56+03:00
)

)

