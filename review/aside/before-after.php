<?
// Если это Главная страница раздела Отзывов
if(empty($_GET["direction"])){
	$cacheType = "N";
	$sortBy1 = "RAND";
	$newsCount = 3;
	$arFilter = false;
}else{
	// Если это Раздел Отзывов
	$cacheType = "A";
	$sortBy1 = "ACTIVE_FROM";
	$newsCount = 99;
	$arFilter = ["PROPERTY_DIRECTION_BEFORE_AFTER_VALUE" => $_GET["direction"]];
}
?>

<section class="mainContent__ArticleBlock mainContent__ArticleBlock__beforeAfter">
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"before_after_right",
		Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "N",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => $cacheType,
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array("",""),

			"FILTER_NAME" => "arFilter",

			"HIDE_LINK_WHEN_NO_DETAIL" => "N",

			"IBLOCK_ID" => "21",
			"IBLOCK_TYPE" => "before_after",

			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "Y",
			"MESSAGE_404" => "",

			"NEWS_COUNT" => $newsCount,

			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array("","fas_before_after","Profile_before_after"),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => $sortBy1,
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"STRICT_SECTION_CHECK" => "N"
		)
	);?>
</section>