<div class="blockFilter">
    <form action="">

        <div class="select__block">
            <select class="select" name="direction" onChange="submit();">
                <option value="">Все направления</option>
				<? foreach($arDirection as $v): ?>
					<? if(!empty($_REQUEST["direction"]) && $_REQUEST["direction"] == $v): ?>
						<option value="<?= $v ?>" selected><?= $v ?></option>
					<? else: ?>
						<option value="<?= $v ?>"><?= $v ?></option>
					<? endif ?>
				<? endforeach ?>
            </select>
        </div>

    </form>
</div>
