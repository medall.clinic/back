<section class="mainContent__left">
	<div class="mainContent__wrap">

		<? include __DIR__."/breadcrumbs.php" ?>
		<? include __DIR__."/title.php" ?>
		<? include __DIR__."/desc.php" ?>

		<div class="innerPageContent__mainBlock">
			<div class="reviewPage__content">
				<? include __DIR__."/filter.php" ?>
				<? include __DIR__."/reviews.php" ?>
			</div>
		</div>

	</div>
</section>
