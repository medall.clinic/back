<?
\Bitrix\Main\Loader::IncludeModule('iblock');

// получим все направления
$arDirection = [];
$res = CIBlockElement::GetList(
    [],
    ["IBLOCK_ID" => 17],
    false,
    false,
    ["PROPERTY_DIRECTION_REVIEW"]
);

while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();
	$arDirection[$arFields['PROPERTY_DIRECTION_REVIEW_ENUM_ID']] = $arFields['PROPERTY_DIRECTION_REVIEW_VALUE'];
}