    <section class="container page-section">
        <div class="content">
            <h1>Прайс лист
            </h1>
            <div class="page-subsection">
                <div class="table-wrap">
                    <table class="table">
                        <colgroup class="table__colgroup table__colgroup--3">
                            <col class="table__col"/>
                            <col class="table__col"/>
                            <col class="table__col"/>
                        </colgroup>
                        <tr class="table__row">
                            <th class="table__header">Наименование
                            </th>
                            <th class="table__header">Артикул
                            </th>
                            <th class="table__header">Цена
                            </th>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-1" colspan="3">Общие виды работ
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Аппликационная анестезия
                            </td>
                            <td class="table__cell">
                            </td>
                            <td class="table__cell">
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Инфильтрационная анестезия
                            </td>
                            <td class="table__cell">
                            </td>
                            <td class="table__cell">
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Проводниковая анестезия
                            </td>
                            <td class="table__cell">
                            </td>
                            <td class="table__cell">
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-1" colspan="3">Косметология
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-2" colspan="3">Аппаратная косметология
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-3" colspan="3">Ультразвуковой SMAS - лифтинг
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Ультразвуковое лечение кожи. Ультразвуковой SMAS - лифтинг. Все лицо
                            </td>
                            <td class="table__cell">A22.01.001.001
                            </td>
                            <td class="table__cell">65 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Ультразвуковое лечение кожи. Ультразвуковой SMAS - лифтинг. Верхняя треть лица
                            </td>
                            <td class="table__cell">A22.01.001.002
                            </td>
                            <td class="table__cell">20 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Ультразвуковое лечение кожи. Ультразвуковой SMAS - лифтинг. Средняя часть лица
                            </td>
                            <td class="table__cell">A22.01.001.003
                            </td>
                            <td class="table__cell">40 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Ультразвуковое лечение кожи. Ультразвуковой SMAS - лифтинг. Средняя и нижняя трети лица
                            </td>
                            <td class="table__cell">A22.01.001.004
                            </td>
                            <td class="table__cell">50 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-3" colspan="3">CO2
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Лазерная шлифовка кожи. СО2 лазерное омоложение. Все лицо
                            </td>
                            <td class="table__cell">A22.01.002.001
                            </td>
                            <td class="table__cell">25 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Лазерная шлифовка кожи. СО2 лазерное омоложение. Лицо и шея
                            </td>
                            <td class="table__cell">A22.01.002.002
                            </td>
                            <td class="table__cell">35 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-2" colspan="3">Общие косметология
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Введение искусственных наполнителей в мягкие ткани с целью коррекции формы канюлей
                            </td>
                            <td class="table__cell">А11.01.013
                            </td>
                            <td class="table__cell">
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-1" colspan="3">Пластическая хирургия
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-2" colspan="3">Интимная пластика
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Пластика малых половых губ
                            </td>
                            <td class="table__cell">А16.20.098
                            </td>
                            <td class="table__cell">60 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Рассечение девственной плевы
                            </td>
                            <td class="table__cell">A16.20.021
                            </td>
                            <td class="table__cell">15 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-1" colspan="3">Трихология
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Первичная консультация трихолога (без фототрихограммы)
                            </td>
                            <td class="table__cell">В04.071.001
                            </td>
                            <td class="table__cell">2 000,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Первичная консультация трихолога  (с фототрихограммой)
                            </td>
                            <td class="table__cell">В04.071.002
                            </td>
                            <td class="table__cell">2 500,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-1" colspan="3">Флебология
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Прием (осмотр, консультация) врача - сердечно-сосудистого хирурга первичный
                            </td>
                            <td class="table__cell">B01.043.001.001
                            </td>
                            <td class="table__cell">3 500,00
                            </td>
                        </tr>
                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Наложение повязки при нарушении кожных покровов (доп перевязка)
                            </td>
                            <td class="table__cell">А15.01.001.001
                            </td>
                            <td class="table__cell">50,00
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
