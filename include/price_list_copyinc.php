price_list_inc.php    <section class="container page-section">
        <div class="content">
            <h1>Прайс лист
            </h1>
            <div class="page-subsection">
                <div class="table-wrap">
                    <table class="table">
                        <colgroup class="table__colgroup table__colgroup--3">
                            <col class="table__col"/>
                            <col class="table__col"/>
                            <col class="table__col"/>
                        </colgroup>
                        <tr class="table__row">
                            <th class="table__header">Наименование
                            </th>
                            <th class="table__header">Артикул
                            </th>
                            <th class="table__header">Цена
                            </th>
                        </tr>

                        <tr class="table__row">
                            <td class="table__cell table__cell--sub-4">Ультразвуковое лечение кожи. Ультразвуковой SMAS - лифтинг. Средняя и нижняя трети лица
                            </td>
                            <td class="table__cell">A22.01.001.004
                            </td>
                            <td class="table__cell">50 000,00
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </section>
