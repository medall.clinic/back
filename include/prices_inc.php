<?php
// Загружаем XML файл с использованием пути к DOCUMENT_ROOT
$xmlFilePath = $_SERVER['DOCUMENT_ROOT'].'/import/Price.xml';
$xml = simplexml_load_file($xmlFilePath);
$services = [];
$parentCode = '';
$parentId = '';
$parentServices = []; // Ассоциативный массив для хранения родительских услуг и их подуслуг
$plasticServices = [
        'Липосакция по зонам',
        'Липосакция по областям',
        'Липофилинг',
        'Ринопластика (старое пока что)',
        'Ринопластика',
        'лор-операции',
    ];
/*
 * Гинекология
 * Аппартная гинекология
 * Эстетическая гинекология
 * Флебология
 * Сестринские манипуляции
 * Импланты
 * Компрессионное белье
 * Осмотр (консультация) врачом-анестезиологом-реаниматологом повторный
 * Ассистенция врача-пластического хирурга длительностью до 3 часов
 * Ассистенция врача-пластического хирурга длительностью 4-5 часов
 * Ассистенция врача-пластического хирурга длительностью 6 и более часов
 * Прием (осмотр, консультация) врача-пластического хирурга повторный. Контрольный осмотр
 * Перевязка чистых (неифицированных) поверхностных ран после хирургического вмешательства
 * Назначение лекарственных препаратов в предоперационном периоде
 * Осмотр врача-пластического хирурга в отделении стационара
 * Прием (осмотр, консультация) врача-пластического хирурга к.м.н. первичный
 * Назначение диетической терапии
 * Назначение лечебно-оздоровительного режима
 * Психологическая адаптация
 * Прием (осмотр, консультация) врача-пластического хирурга предоперационный
 * Прием (осмотр, консультация) врача-пластического хирурга предоперационный (трансплантация волос)
 * * * */
$exception = [
    '82e12a40-508b-11ee-b639-0015179e7828',
    '52151e31-5398-11ee-b63a-0015179e7828',
    '0e7d65e6-5c28-11ee-b63a-0015179e7828',
    '1e70cd0e-770c-11ec-a06c-0015179e7828',
    'fad7c2da-74c9-11ee-b63f-0015179e7828',
    '699f0578-8189-11ef-82dc-0015179e7828',
    '763ae6bf-8189-11ef-82dc-0015179e7828',
    '294ec2c8-0067-11ee-93aa-0015179e7828',
    'f66ee5d0-68ed-11ef-9e9f-0015179e7828',
    '2622fa64-68ee-11ef-9e9f-0015179e7828',
    '3ce14ec2-68ee-11ef-9e9f-0015179e7828',
    '94d49213-3c7c-11ec-a78e-0015179e7828',
    '94d49214-3c7c-11ec-a78e-0015179e7828',
    '94d49215-3c7c-11ec-a78e-0015179e7828',
    '94d49216-3c7c-11ec-a78e-0015179e7828',
    'dcdddd0b-8fcb-11ec-af2e-0015179e7828',
    '106e3575-6135-11ee-b63c-0015179e7828',
    'b2f8bf09-74cd-11ee-b63f-0015179e7828',
    'f48676c3-74cd-11ee-b63f-0015179e7828',
    '0ac29977-74cf-11ee-b63f-0015179e7828',
    'baea92af-a3b3-11ed-a480-0015179e7828',
    '106a220c-779b-11ec-a06c-0015179e7828',
];
foreach ($xml->Каталог as $catalog) {
    $name = (string)$catalog->Наименование;
    $price = (float)$catalog->Цена;
    $isFolder = (string)$catalog->ЭтоПапка;
    $uid = (string)$catalog->UID;
    $parentId = (string)$catalog->Родитель;

    // Если это родительская услуга
    if ($isFolder == 'true' && !in_array($uid, $exception)) {
        $cleanedName = preg_replace('/^\d+\s*-\s*/', '', $name);
        $parentServices[$uid] = [
            'name' => trim($cleanedName),
            'children' => [], // Инициализация дочерних услуг
            'parentId' => $parentId,

        ];
    }

    // Если это дочерняя услуга
    if ($isFolder == 'false' && !in_array($uid, $exception)) {
        $childServices[$uid] = [
            'name' => $name,
            'price' => $price,
            'parentId' => $parentId,

        ];
    }
}

// Теперь связываем дочерние услуги с родителями
foreach ($childServices as $child) {
    $parentId = $child['parentId'];
    if (isset($parentServices[$parentId])) {
        $parentServices[$parentId]['children'][] = [
            'name' => $child['name'],
            'price' => $child['price'],
        ];
    }
}

$checkNames = [];
$hasPlasticSurgery = '';
$test = $parentServices
?>





<section class="container page-section page-section--background-color page-section--background-color--dark prices-banner">
    <picture>
        <source srcset="/images/prices/prices-banner_s.webp, /images/prices/prices-banner_s@2x.webp 2x" media="(max-width: 719px)"/>
        <source srcset="/images/prices/prices-banner_m.webp, /images/prices/prices-banner_m@2x.webp 2x" media="(min-width: 720px) and (max-width: 1199px)"/>
        <source srcset="/images/prices/prices-banner_l.webp, /images/prices/prices-banner_l@2x.webp 2x" media="(min-width: 1200px)"/>
        <source srcset="/images/prices/prices-banner_s.jpg, /images/prices/prices-banner_s@2x.jpg 2x" media="(max-width: 719px)"/>
        <source srcset="/images/prices/prices-banner_m.jpg, /images/prices/prices-banner_m@2x.jpg 2x" media="(min-width: 720px) and (max-width: 1199px)"/>
        <source srcset="/images/prices/prices-banner_l.jpg, /images/prices/prices-banner_l@2x.jpg 2x" media="(min-width: 1200px)"/><img class="prices-banner__image" src="/images/prices/prices-banner.jpg" loading="lazy" decoding="async"/>
    </picture>
    <h1 class="prices-banner__title">ЦЕНЫ НА УСЛУГИ КЛИНИКИ</h1>
    <div class="prices-banner__text">Сайт предоставляет справочную информацию для ознакомления  с ценами, аналогично любому рекламному источнику.  Полный прайс-лист смотрите на странице «документы». Итоговая стоимость лечения определяется в зависимости от объёма работы  и устанавливается только после личного осмотра специалистом. Чтобы узнать стоимость услуги для вас, запишитесь на первичную консультацию.</div>
    <a class="button prices-banner__button" href="#contacts-form">Записаться на консультацию</a>
</section>
<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="prices-layout page-subsection">
            <div class="prices-filters prices-layout__filters">
                <div class="form-input form-input--select form-input--secondary prices-filters__select prices-filters__select--direction">
                    <select class="form-input__field" name="direction">
                        <option value=""></option>
                    </select>
                    <span class="label form-input__label">Выберите направление
                    </span>
                </div>
                <div class="form-input form-input--select form-input--secondary prices-filters__select prices-filters__select--service">
                    <select class="form-input__field" name="service">
                        <option value=""></option>
                    </select>
                    <span class="label form-input__label">Выберите услугу
                    </span>
                </div>
                <label class="form-input form-input--search prices-filters__search">
                    <input class="form-input__field" type="search">
                    <span class="label form-input__label">Быстрый поиск по ценам
                    </span>
                </label>
            </div>
            <div class="prices-layout__tables">
                <?php foreach ($parentServices as $parentService) {
                    if (empty($parentService['children'])) continue; // Пропускаем, если нет подуслуг

                    $priceMap = []; // для хранения цен по уникальным услугам

                    // Сбор цен для подуслуг
                    foreach ($parentService['children'] as $service) {
                        if ($service['price'] == 0) continue; // Пропускаем услуги с нулевой ценой


                        // Убираем категории из названия услуги для группировки
                        $baseName = preg_replace('/\s*\(\s*экспертный уровень\)\s*|\s*\(липоскульптура\)\s*|\s*\(\d+\s+категория\)\s*|\s*\(\d+\s+категория сложности\)\s*|\s*\(\d+\s+категории сложности\)\s*|\s*\(\d+\s+степень сложности\)\s*|\s*\(\d+\s+уровень сложности\)\s*/ui', '', $service['name']);
                        if (!isset($priceMap[trim($baseName)])) {
                            $priceMap[trim($baseName)] = [$service['price']];
                        } else {
                            $priceMap[trim($baseName)][] = $service['price'];
                        }

                        $prices[] = $service['price'];
                        $serviceNames[] = $service['name'];
                        if (
                            $parentService['parentId'] == '052676d9-74e7-11ef-82be-0015179e7828'
                            || in_array($parentService['name'], $plasticServices)
                        ) {
                            $hasPlasticSurgery = true;
                        } else {
                            $hasPlasticSurgery = false;

                        }
                    }
                    $dataDirection = $hasPlasticSurgery ? 'Пластическая хирургия' : 'Косметология';

                    // Вывод названия родительской услуги
                    echo '<div class="prices-table prices-layout__table" data-direction="' . htmlspecialchars($dataDirection) .'">';
                    echo '<div class="prices-table__title">'.$parentService["name"].'</div>';
                    echo '<div class="prices-table__inner">';

                    // Вывод подуслуг с их ценами
                    foreach ($priceMap as $serviceName => $servicePrices) {
                        $minPrice = min($servicePrices);
                        $maxPrice = max($servicePrices);
                        $formattedMinPrice = number_format($minPrice, 0, ',', ' ') . ' ₽';
                        $formattedMaxPrice = number_format($maxPrice, 0, ',', ' ') . ' ₽';

                        // Выводим уникальную подуслугу с диапазоном цен

                        echo '<div class="prices-table__row">';
                        echo '<div class="prices-table__cell">' . htmlspecialchars($serviceName) .'</div>';
                        if ($formattedMinPrice != $formattedMaxPrice && $formattedMaxPrice > 0) {
                            echo '<div class="prices-table__cell">' .' от ' . $formattedMinPrice . ' до ' . $formattedMaxPrice . '</div>';

                        }
                        if ($formattedMinPrice == $formattedMaxPrice && $formattedMaxPrice) {
                            echo '<div class="prices-table__cell">' . $formattedMinPrice . '</div>';

                        }


                        echo '</div>';

                    }
                    echo '</div>';
                    echo '</div>';


                } ?>
            </div>

            <div class="grid grid--justify--center"><a class="grid__cell grid__cell--l--2 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button prices-layout__filters-button" href="/">Показать еще</a>
            </div>
        </div>
    </div>
</section>
<section class="container page-section" id="contacts-form">
    <div class="content">
        <div class="page-subsection">
            <h2 class="page-section__title text-align-center">Записаться на приём
            </h2>
            <p class="page-section__subtitle">В комментариях укажите, какая услуга, доктор или вопрос Вас интересует.</p>
        </div>
        <div class="page-subsection grid grid--justify--center">
            <form class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form form-ajax" data-is-validate="true" data-is-answer-in-popup="true" data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации">
                <label class="form-input form-input--text form__input">
                    <input class="form-input__field" type="text" name="name" data-validation-required>
                    <span class="label form-input__label">ФИО
                    </span>
                </label>
                <label class="form-input form-input--tel form__input">
                    <input class="form-input__field" type="tel" name="phone" data-validation-required>
                    <span class="label form-input__label">Телефон
                    </span>
                </label>
                <label class="form-input form-input--textarea form__input">
                    <textarea class="form-input__field" name="message"></textarea>
                    <span class="label form-input__label">Комментарии
                    </span>
                </label>
                <button class="button form__submit" type="submit">Оставить заявку
                </button>
                <div class="form__agreement home-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</div>
                <input type="hidden" name="title" value="Заявка со страницы цен">
                <input type="hidden" name="action" value="consultation_register">
            </form>
        </div>
    </div>
</section>
<section class="container page-section page-section--no-margin-bottom">
    <div class="content">
        <p>Цены на услуги размещены с ознакомительной целью и не являются публичной офертой. В стоимость операции не входят наркоз, пребывание в палате и анализы. С полным перечнем платных медицинских услуг с указанием цен в рублях можно ознакомиться ниже:</p>
        <div class="grid p">
            <div class="grid__cell grid__cell--m--4 grid__cell--s--6"><a class="prices-item" href="/plastic.pdf">Полный прайс отделеления пластической хирургии</a>
            </div>
            <div class="grid__cell grid__cell--m--4 grid__cell--s--6"><a class="prices-item" href="/cosmetology.pdf">Полный прайс отделеления косметологии</a>
            </div>
        </div>
    </div>
    </div>
</section>
