<?php
require_once __DIR__ . '/lib/tfpdf.php';

use include\lib\tFPDF;


// Загружаем XML из файла
$xmlFile = $_SERVER['DOCUMENT_ROOT'] .'/import/Price.xml';
$dom = new DOMDocument();
if (!$dom->load($xmlFile)) {
    die('Ошибка загрузки XML файла: ' . $dom->error); // Более информативное сообщение об ошибке
}
$xpath = new DOMXPath($dom);
$xpath->registerNamespace('ns', 'S2');


// Извлекаем данные из XML, используя XPath для более эффективного поиска
$catalogs = $xpath->query('//ns:Каталог');

if (!$catalogs) {
    die("Не найдено ни одного элемента 'Каталог' в XML-файле.");
}


$services = [];
$arrExceptionParent = [
    'Услуги пластическая хирургия новые',
    'Акция "LPG + Массаж лица"',
    ];
$arrExceptionChild = ['Акция "LPG + Массаж лица"',];

foreach ($catalogs as $catalog) {
    $uid = $catalog->getElementsByTagName('UID')->item(0)->nodeValue;
    $parentUid = $catalog->getElementsByTagName('Родитель')->item(0)->nodeValue;
    $isFolder = $catalog->getElementsByTagName('ЭтоПапка')->item(0)->nodeValue === 'true';
    $serviceData = [
        'Код' => $catalog->getElementsByTagName('Код')->item(0)->nodeValue ?? '', // Используем ?? для обработки отсутствующих элементов
        'Услуга' => $catalog->getElementsByTagName('Наименование')->item(0)->nodeValue ?? '',
        'Код ОКМУ по федеральному справочнику' => $catalog->getElementsByTagName('Артикул')->item(0)->nodeValue ?? '',
        'Наименование Услуги по ОКМУ федерального справочника' => $catalog->getElementsByTagName('ПолноеНаименование')->item(0)->nodeValue ?? '',
        'Цена' => $catalog->getElementsByTagName('Цена')->item(0)->nodeValue ?? '',
        'Родитель' => $parentUid,
        'ЭтоПапка' => $isFolder,
    ];

    $services[$uid] = $serviceData;
}

// Создание PDF-объекта
$pdf = new tFPDF('L', 'mm', ['3100','2000']);
$pdf->AddPage();
$pdf->AddFont('DejaVu','','DejaVuSans-Bold.ttf',true);


$pdf->SetFont('DejaVu','','24'); // Более разумный размер шрифта


$header = [
    'Код',
    'Услуга',
    'Код ОКМУ по федеральному справочнику',
    'Наименование Услуги по ОКМУ федерального справочника',
    'Цена',
    ];
$w = [100, 500, 300, 500, 100,];

// Вывод заголовка таблицы
for($i=0; $i<count($header); $i++) {
    $pdf->Cell($w[$i],20,$header[$i],1,0,'C'); // Уменьшили высоту ячейки
}
$pdf->Ln();

foreach ($services as $uid => $service) {
    $isFolder = $service['ЭтоПапка'] === 'true';
    $priceOutput = $service['Цена'] ?: '';


    if ($service['ЭтоПапка'] && !in_array($service['Услуга'],$arrExceptionParent)) {

        $pdf->AddFont('DejaVu','','DejaVuSans-Bold.ttf',true);

        $pdf->SetFont('DejaVu','','20'); // Более разумный размер шрифта

        // Вывод родительской услуги
        $serviceName = preg_replace('/^\d+\s*-\s*/u', '', $service['Услуга']);
        $pdf->Cell($w[0], 20, $service['Код'], 1, 0, 'C');
        $pdf->Cell($w[1], 20, $serviceName, 1, 0, 'C');
        $pdf->Cell($w[2], 20, $service['Код ОКМУ по федеральному справочнику'], 1, 0, 'C');
        $pdf->Cell($w[3], 20, $service['Наименование Услуги по ОКМУ федерального справочника'], 1, 0, 'C');
        $pdf->Cell($w[4], 20, $priceOutput, 1, 1, 'C'); // Используем $priceOutput здесь

    }
    if (!$service['ЭтоПапка'] && !in_array($service['Услуга'],$arrExceptionChild)) {
        $pdf->AddFont('DejaVu','','DejaVuSans.ttf',true);

        $pdf->SetFont('DejaVu','','14'); // Более разумный размер шрифта
        // Вывод дочерней услуги
        $pdf->Cell($w[0], 15, $service['Код'], 1, 0, 'C');
        $pdf->Cell($w[1], 15, ' ' . $service['Услуга'], 1, 0, 'L');
        $pdf->Cell($w[2], 15, $service['Код ОКМУ по федеральному справочнику'], 1, 0, 'L');
        $pdf->Cell($w[3], 15, $service['Наименование Услуги по ОКМУ федерального справочника'], 1, 0, 'L');
        $pdf->Cell($w[4], 15, $priceOutput . ' ₽', 1, 1, 'C'); // Используем $priceOutput здесь
    }
}

$pdf->Output('price_list.pdf', 'F');

?>
