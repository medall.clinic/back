<?
// для каждой секции нужно добавить имя пункта меню.
// если этот пункт заполнен -> он будет в меню страницы вместо стандартного.
// если название не указано -> выводилось стандартное название пункта

$refsMenuLinkAnchorToDbPropertyCode = [
	"reasons" => "REASONS_MENU_TITLE", // показания
	"3d-modelling" => "MODELING_MENU_TITLE", // 3d моделирование
	"advantages" => "ADVANTAGES_MENU_TITLE", // преимущества (о клинике)
	"doctors" => "DOCTORS_MENU_TITLE", // наши врачи
	"works" => "WORKS_MENU_TITLE", // до/после
	"price" => "PRICES_MENU_TITLE", // цены
	"credit" => "CREDIT_MENU_TITLE", // рассрочка
	"results" => "REVIEWS_MENU_TITLE", // результаты
	"questions" => "QUESTIONS_MENU_TITLE", // faq
	"contact" => "FORM_CONTACTS_MENU_TITLE", // задать вопрос

	"banner" => "BANNER_MENU_TITLE", // баннер
	"comfort" => "COMFORT_MENU_TITLE", // комфорт
	"sequence" => "STAGES_MENU_TITLE", // этапы
	"history" => "HISTORY_MENU_TITLE", // история
];

CModule::IncludeModule("iblock");

$elementCode = $_REQUEST["ELEMENT_CODE"];
$elementCodeExplode = explode('?', $elementCode);
$elementCode = reset($elementCodeExplode);

$arOrder = [];
$arFilter = ["IBLOCK_ID" => IBLOCK_ID_LANDINGS, "CODE" => $elementCode];
$arGroupBy = false;
$arNavStartParams = [];
$arSelectFields = ["IBLOCK_ID", "ID"];

$res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

$bxRes = $res->GetNextElement();
$bxElement = array_merge($bxRes->GetFields(), $bxRes->GetProperties());

$aMenuLinks = [];

foreach ($refsMenuLinkAnchorToDbPropertyCode as $menuLinkAnchor => $dbPropertyCode) {
	if(empty($bxElement[$dbPropertyCode])){continue;}
	if(empty($bxElement[$dbPropertyCode]["VALUE"])){continue;}
	$menuLinkName = $bxElement[$dbPropertyCode]["VALUE"];
	$menuLinkAnchor = "#section-".$menuLinkAnchor;

	$aMenuLinks[] = [$menuLinkName, $menuLinkAnchor];
}