<? $pathTemplateLanding = "/bitrix/templates/landing/" ?>
<?
if(!isUserDeveloper()){
	$APPLICATION->SetAdditionalCSS($pathTemplateLanding."/styles/style.css?v=2");
}
?>
<? $APPLICATION->AddHeadScript($pathTemplateLanding.'/scripts/script.js'); ?>
<? $APPLICATION->AddHeadScript($pathTemplateLanding.'/scripts/bitrix24.js?v=2'); ?>
<div class="landing-page">
  <?
  $APPLICATION->IncludeComponent(
  	"bitrix:news.detail",
    isUserDeveloper() ? "landing" : "service-as-landing",
  	Array(
  		"ACTIVE_DATE_FORMAT" => "j F Y",
  		"ADD_ELEMENT_CHAIN" => "Y",
  		"ADD_SECTIONS_CHAIN" => "N",
  		"AJAX_MODE" => "N",
  		"AJAX_OPTION_ADDITIONAL" => "",
  		"AJAX_OPTION_HISTORY" => "N",
  		"AJAX_OPTION_JUMP" => "N",
  		"AJAX_OPTION_STYLE" => "Y",
  		"BROWSER_TITLE" => "-",
  		"CACHE_GROUPS" => "N",
  		"CACHE_TIME" => "36000000",
  		"CACHE_TYPE" => "N",
  		"CHECK_DATES" => "Y",
  		"DETAIL_URL" => "",
  		"DISPLAY_BOTTOM_PAGER" => "Y",
  		"DISPLAY_DATE" => "Y",
  		"DISPLAY_NAME" => "Y",
  		"DISPLAY_PICTURE" => "Y",
  		"DISPLAY_PREVIEW_TEXT" => "Y",
  		"DISPLAY_TOP_PAGER" => "N",
  		"ELEMENT_CODE" => $elementCode,
  		"ELEMENT_ID" => "",
  
  		"FIELD_CODE" => array("NAME","SORT","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE",""),
  		"IBLOCK_ID" => "47", // Лэндинги/Лэндинги
  		"IBLOCK_TYPE" => "landings",
  
  		"IBLOCK_URL" => "",
  		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
  		"MESSAGE_404" => "",
  		"META_DESCRIPTION" => "-",
  		"META_KEYWORDS" => "-",
  		"PAGER_BASE_LINK_ENABLE" => "N",
  		"PAGER_SHOW_ALL" => "N",
  		"PAGER_TEMPLATE" => ".default",
  		"PAGER_TITLE" => "Страница",
  		"PROPERTY_CODE" => [
  
  			"BANNER_TITLE",
  			"BANNER_IMAGE",
  			"BANNER_TEXT",
  			"BANNER_PRICE",
  			"BANNER_IS_PRICE_FROM",
  			"BANNER_BUTTON",
  			"BANNER_ADDITIONAL_TEXT",
  
  			"REASONS_TITLE",
  			"REASONS_TEXT",
  			"REASONS_ITEMS",
  			"REASONS_LIST_ITEMS_PROS",
  			"REASONS_LIST_ITEMS_CONS",
  			"REASONS_ADDITIONAL_TEXT",
  
  			"MODELING_TITLE",
  			"MODELING_TEXT",
  			"MODELING_IMAGES",
  			"MODELING_BUTTON",
  			"MODELING_ADDITIONAL_TEXT",
  
  			"ADVANTAGES_TITLE",
  			"ADVANTAGES_ITEMS",
  			"ADVANTAGES_IMAGES",
  			"ADVANTAGES_ADDITIONAL_TEXT",
  
  			"DOCTORS_TITLE",
  			"DOCTORS_TEXT",
  			"DOCTORS_ITEMS",
  			"DOCTORS_GLOBAL_ITEMS",
  			"DOCTORS_ADDITIONAL_TEXT",
  
  			"WORKS_TITLE",
  			"WORKS_ITEMS",
  			"WORKS_ADDITIONAL_TEXT",
  
  			"PRICES_TITLE",
  			"PRICES_TEXT_1",
  			"PRICES_TEXT_2",
  			"PRICES_COMPLEX_ITEMS",
  			"PRICES_SIMPLE_ITEMS",
  			"PRICES_ADDITIONAL_TEXT",
  
  			"COMFORT_TITLE",
  			"COMFORT_TEXT",
  			"COMFORT_IMAGES",
  			"COMFORT_ADDITIONAL_TEXT",
  
  			"CREDIT_TITLE",
  			"CREDIT_TEXT",
  			"CREDIT_ADDITIONAL_TEXT",
  
  			"STAGES_TITLE",
  			"STAGES_LIST_ITEMS",
  			"STAGES_BANNER_IMAGE",
  			"STAGES_BANNER_TEXT",
  			"STAGES_ADDITIONAL_TEXT",
  
  			"REVIEWS_TITLE",
  			"REVIEWS_IMAGES",
  			"REVIEWS_ADDITIONAL_TEXT",
  
  			"QUESTIONS_TITLE",
  			"QUESTIONS_ITEMS",
  			"QUESTIONS_IMAGE",
  			"QUESTIONS_ADDITIONAL_TEXT",
  
  			"HISTORY_TITLE",
  			"HISTORY_ITEMS",
  			"HISTORY_ADDITIONAL_TEXT",
  
  			"FORM_CONTACTS_TITLE",
  			"FORM_SCRIPT",
  
  			"REASONS_MENU_TITLE",
  			"MODELING_MENU_TITLE",
  			"ADVANTAGES_MENU_TITLE",
  			"DOCTORS_MENU_TITLE",
  			"WORKS_MENU_TITLE",
  			"PRICES_MENU_TITLE",
  			"CREDIT_MENU_TITLE",
  			"REVIEWS_MENU_TITLE",
  			"QUESTIONS_MENU_TITLE",
  			"FORM_CONTACTS_MENU_TITLE",
  
  			"BANNER_MENU_TITLE",
  			"COMFORT_MENU_TITLE",
  			"STAGES_MENU_TITLE",
  			"HISTORY_MENU_TITLE",
  
  			"VERT_SORT",
  
  		],
  		"SET_BROWSER_TITLE" => "Y",
  		"SET_CANONICAL_URL" => "N",
  		"SET_LAST_MODIFIED" => "N",
  		"SET_META_DESCRIPTION" => "Y",
  		"SET_META_KEYWORDS" => "Y",
  		"SET_STATUS_404" => "Y",
  		"SET_TITLE" => "Y",
  		"SHOW_404" => "Y",
  		"STRICT_SECTION_CHECK" => "N",
  		"USE_PERMISSIONS" => "N",
  		"USE_SHARE" => "N"
  	)
  );
  ?>
</div>

