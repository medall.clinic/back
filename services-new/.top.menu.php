<?
$aMenuLinks = [
	[
		"Показания",
		"#section-reasons"
	],
	[
		"3D моделирование",
		"#section-3d-modelling"
	],
	[
		"О клинике",
		"#section-advantages"
	],
	[
		"Наши врачи",
		"#section-doctors"
	],
	[
		"До/После",
		"#section-works"
	],
	[
		"Цены",
		"#section-price"
	],
	[
		"Рассрочка",
		"#section-credit"
	],
	[
		"Результаты",
		"#section-results"
	],
	[
		"Faq",
		"#section-questions"
	],
	[
		"Задать вопрос",
		"#section-contact"
	],
	[
		"Контакты",
		"#section-contacts"
	],
];
?>