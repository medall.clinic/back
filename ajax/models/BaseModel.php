<?
abstract class BaseModel
{
	const SITE_CODE = "s1";
	const MAKE_IBLOCK_ELEMENT = true;
	const SEND_EMAIL = true;

	public $iblockId;

	protected $messages = [];

	protected $arParamsCode;

	protected $dateForCode;
	protected $dateForUser;

	public function __construct($request)
	{
		if (empty($request)) {
			die("No data in request");
		}

		if (!empty($request["req_name"])) {
			throw new Exception("Invalid data");
		}

		$this->dateForCode = ConvertTimeStamp(time(), "FULL");
		$this->dateForUser = date('Y/m/d H:i:s');

		//code generation
		$this->arParamsCode = array(
			"max_len" => 255,
			"change_case" => "L",
			"replace_space" => '-',
			"replace_other" => '-',
			"delete_repeat_replace" => true
		);

		CModule::IncludeModule("iblock");
	}

	protected function getCode($subject)
	{
		return CUtil::translit($subject, "ru", $this->arParamsCode);
	}

	public function sendEmail($emailFields, $files = [])
	{
		$duplicate = "Y";
		$messageId = "";

		if(!CEvent::SendImmediate(static::EVENT_NAME, self::SITE_CODE, $emailFields, $duplicate, $messageId, $files)){
			throw new Exception("Ошибка при отправке e-mail");
		}
		$this->messages[] = "E-mail send";
	}

	public function getEmailFields($newElementId)
	{
		$backendLink = "http://" . SITE_SERVER_NAME . "/bitrix/admin/iblock_element_edit.php";
		$backendLink .= "?IBLOCK_ID=" . $this->iblockId . "&type=" . static::IBLOCK_TYPE . "&ID=" . $newElementId;

		return [
			"ID" => $newElementId,
			"DATE" => $this->dateForUser,
			"BE_LINK" => $backendLink
		];
	}

	public function getEmailFiles($elementId)
	{
		return [];
	}


	public function getMessage()
	{
		$message = "";

		if ($this->messages) {
			$message = implode(" - ", $this->messages);
		}

		return $message;
	}

	public function doPostAction(){

    }

}