<?
class Review extends BaseModel
{
	const EVENT_NAME = "REVIEW_FORM";
	const SUBJECT_INIT = "Новый отзыв";
	const IBLOCK_TYPE = "review";
	const IBLOCK_ID = 17;
	const NAME_FIELD_NAME = "";
	const MESSAGE_FIELD_NAME = "";

	public $name;
	public $message;
	public $phone;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->name = $request["name"];
		$this->message = $request["message"];
		$this->phone = $request["phone"];

		if (empty($this->name)) {throw new Exception('Поле "ФИО" не заполнено');}
        if (empty($this->phone)) {throw new Exception('Поле "Телефон" не заполнено');}
        if (empty($this->message)) {throw new Exception('Поле "Сообщение" не заполнено');}


    }

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => '',
			"NAME" => $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,

		];


	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);
		$emailFields["AUTHOR"] = $this->name;
		$emailFields["TEXT"] = $this->message;
		$emailFields["AUTHOR_CITY"] = $this->city;

		return $emailFields;
	}

}
