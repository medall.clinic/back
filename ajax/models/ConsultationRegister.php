<?php
class ConsultationRegister extends BaseModel
{
    const EVENT_NAME = "CONSULTATION_REGISTER";
    const SUBJECT_INIT = "Запись на консультацию";
    const IBLOCK_TYPE = "contacts";
    const SEND_EMAIL = false;

    public $name;
    public $phone;
    public $email;
    public $comment;
    public $curPage;
    public $referer;

    public function __construct($request)
    {
        parent::__construct($request);

        if(static::MAKE_IBLOCK_ELEMENT){
            $this->iblockId = IBLOCK_ID_CONSULTATION_REGISTER;
        }

        $this->name = $request["name"];
        $this->phone = $request["phone"];
        $this->email = $request["email"];
        $this->comment = $request["comment"];
        $this->surname = $request["surname"];
        $this->cv = $request["cv"];
        $this->referer = $_SERVER['HTTP_REFERER'];


        $this->curPage = $this->getCurPage($request["cur_page"]);

        if (empty($this->name)) {throw new Exception("Name is empty");}
        if (empty($this->phone)) {throw new Exception("Phone is empty");}
    }

    public function createIblockElement()
    {
        $subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
        $subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

        $element = new CIBlockElement;

        $arElementFields = [
            "IBLOCK_ID" => $this->iblockId,
            "NAME" => $subjectForUser,
            "CODE" => $this->getCode($subjectForCode),
            "ACTIVE" => "N",
            "DATE_ACTIVE_FROM" => $this->dateForCode,
            "PROPERTY_VALUES" => [
                "NAME" => $this->name,
                "PHONE" => $this->phone,
                "EMAIL" => $this->email,
                "COMMENT" => $this->comment,
                "CURRENT_PAGE" => $this->curPage,
            ]
        ];

        if (!$newElementId = $element->Add($arElementFields)) {
            throw new Exception($element->LAST_ERROR);
        }

        $this->messages[] = "New IBlock element created, Id = " . $newElementId;

        return $newElementId;
    }

    public function getEmailFields($newElementId)
    {
        $emailFields = parent::getEmailFields($newElementId);

        $emailFields["NAME"] = $this->name;
        $emailFields["PHONE"] = $this->phone;
        $emailFields["EMAIL"] = $this->email;
        $emailFields["COMMENT"] = $this->comment;
        $emailFields["CURRENT_PAGE"] = $this->curPage;

        return $emailFields;
    }
    public function mailSend()
    {
        $to = 'admin@medall.clinic' . ', '; // кому отправляем
        $to .= 'marketing@medall.clinic' . ', '; // Внимание! Так пишем второй и тд адреса
// не забываем запятую. Даже в последнем контакте лишней не будет
// Для начинающих! $to .= точка в этом случае для Дописывания в переменную

        $subject = 'Отправлена заявка со страницы "Контакты" medall.clinic/about/contacts/';
        $message = '<table width="width: 100%;">
  <tr>
    <th colspan="2" style="padding-bottom: 15px; font-size: 24px">Информационное сообщение сайта medall.clinic</th>
  </tr>
  <br>
  <tr>
    <td colspan="2" style="text-align: center; border-bottom: 1px solid #222;" width="width: 100%;"  ></td>
  </tr>
  <tr>
    <td style="padding: 15px 0;" colspan="2">Отправлена заявка со страницы "Контакты"
      <a href="medall.clinic/about/contacts/" style="padding-bottom: 20px;">
        https://medall.clinic/about/contacts/
      </a>
    </td>

  </tr>
  <tr>
    <td style="padding-bottom: 15px;"><span style="padding-right: 5px">ФИО:</span>' .$_POST['name']. '</td>
  </tr>
  <tr>
    <td style="padding-bottom: 15px;" ><span style="padding-right: 5px;">Телефон:</span>' .$_POST['phone']. '</td>
  </tr>
  <br>
  <tr>
    <td style="padding-bottom: 15px;"><span style="padding-right: 5px;">Комментарий:</span>' .$_POST['comment']. '</td>
  </tr>
</table>';

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8 \r\n";

        $headers .= "From: medal.clinic <admin@medall.clinic>\r\n"; // от кого
//            $headers .= 'Cc: secondnick@example.com' . "\r\n"; // копия сообщения на этот адрес
//            $headers .= "Bcc: yournick-archive@yourmail.ru\r\n"; // скрытая копия сообщения на этот
        $message = str_replace('\n', "\n", $message);

        mail($to, $subject, $message, $headers);


    }

    public function doPostAction()
    {

        $apiUrl = 'https://medall24.bitrix24.ru/rest/30/mqmuxc440l45s9fw/crm.lead.add.json';


        // Новые лнгдинги start
        if(
            !empty($_POST['name'])
            && !empty($_POST['phone'])
            && !empty($this->curPage)
            && !empty($_POST['comment'])
            && empty($_POST['second_name'])


        ){
            file_put_contents(__DIR__ . '/log.txt', 'новый лендинг' . PHP_EOL, FILE_APPEND);

            $sourceDescription = $this->curPage;
            $sourceDescription .= "\n-------------------\n";
            $leadData = [
                'fields' => [
                    "TITLE" => $this->curPage,
                    "NAME" => $this->name,
                    "PHONE" => [
                        "0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'OPENED' => 'N', // Доступно для всех
                    'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
                    "UF_CRM_5D8E19ED4F970" => ["VALUE" => $this->email],
                    "COMMENTS" => $this->comment,
                    "SOURCE_DESCRIPTION" => $sourceDescription,
                    'UTM_SOURCE'=>$_POST['utm_source'],

                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];


// Преобразуем данные в формат JSON
            $jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',

            ));
// Выполнение запроса
            $response = curl_exec($ch);

// Проверка на ошибки
            if ($response === false) {
                echo 'Ошибка cURL: ' . curl_error($ch);
            } else {
                // Обработка ответа
                $responseData = json_decode($response, true);


            }

// Закрытие cURL-соединения
            curl_close($ch);
        }
        // Новые лнгдинги end




        // старые лендинги start
        if(
            !empty($_POST['name'])
            && !empty($_POST['phone'])
            && !empty($this->curPage)
            && !empty($_POST['comments'])
            && !empty($_POST['second_name'])


        ){
            file_put_contents(__DIR__ . '/log.txt', 'старый лендинг' . PHP_EOL, FILE_APPEND);

            $sourceDescription = $this->curPage;
            $sourceDescription .= "\n-------------------\n";
            $leadData = [
                'fields' => [
                    "TITLE" => $this->curPage,
                    "NAME" =>  $this->name . ' ' . $_POST['second_name'],
                    "PHONE" => [
                        "0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'OPENED' => 'N', // Доступно для всех
                    'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
                    "UF_CRM_5D8E19ED4F970" => ["VALUE" => $this->email],
                    "COMMENTS" => $_POST['comments'],
                    "SOURCE_DESCRIPTION" => $sourceDescription,
                    'UTM_SOURCE'=>$_POST['utm_source'],

                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];


// Преобразуем данные в формат JSON
            $jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',

            ));
// Выполнение запроса
            $response = curl_exec($ch);

// Проверка на ошибки
            if ($response === false) {
                echo 'Ошибка cURL: ' . curl_error($ch);
            } else {
                // Обработка ответа
                $responseData = json_decode($response, true);


            }

// Закрытие cURL-соединения
            curl_close($ch);
        }

        // старые лендинги end


        // HR

        if(
            !empty($_POST['name'])
            && !empty($_POST['phone'])
            && !empty($this->curPage)
            && !empty($_POST['cv'])
            && !empty($_POST['surname'])


        ){
            $sourceDescription = $this->curPage;
            $sourceDescription .= "\n-------------------\n";
            $leadData = [
                'fields' => [
                    "TITLE" => $this->curPage,
                    "NAME" => $this->name . ' ' . $_POST['surname'],
                    "PHONE" => [
                        "0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'OPENED' => 'N', // Доступно для всех
                    'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
                    "UF_CRM_5D8E19ED4F970" => ["VALUE" => $this->email],
                    "COMMENTS" => $_POST['cv'],
                    "SOURCE_DESCRIPTION" => $sourceDescription,
                    'UTM_SOURCE'=>$_POST['utm_source'],

                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];


// Преобразуем данные в формат JSON
            $jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',

            ));
// Выполнение запроса
            $response = curl_exec($ch);

// Проверка на ошибки
            if ($response === false) {
                echo 'Ошибка cURL: ' . curl_error($ch);
            } else {
                // Обработка ответа
                $responseData = json_decode($response, true);


            }

// Закрытие cURL-соединения
            curl_close($ch);
        }

        // старые HR end

        // короткие формы

        if(
            !empty($_POST['name'])
            && !empty($_POST['phone'])
            && !empty($this->curPage)
            && empty($_POST['comment'])
            && empty($_POST['comments'])
            && empty($_POST['cv'])
            && empty($_POST['surname'])


        ){
            file_put_contents(__DIR__ . '/log.txt', 'короткие формы' . PHP_EOL, FILE_APPEND);

            $sourceDescription = $this->curPage;
            $sourceDescription .= "\n-------------------\n";
            $leadData = [
                'fields' => [
                    "TITLE" => $this->curPage,
                    "NAME" => $this->name,
                    "PHONE" => [
                        "0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'OPENED' => 'N', // Доступно для всех
                    'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
                    "UF_CRM_5D8E19ED4F970" => ["VALUE" => $this->email],
                    "COMMENTS" => $_POST['cv'],
                    "SOURCE_DESCRIPTION" => $sourceDescription,
                    'UTM_SOURCE'=>$_POST['utm_source'],

                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];


// Преобразуем данные в формат JSON
            $jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',

            ));
// Выполнение запроса
            $response = curl_exec($ch);

// Проверка на ошибки
            if ($response === false) {
                echo 'Ошибка cURL: ' . curl_error($ch);
            } else {
                // Обработка ответа
                $responseData = json_decode($response, true);


            }

// Закрытие cURL-соединения
            curl_close($ch);
        }
        // короткие end


        if (
            !empty($_POST['name'])
            && !empty($_POST['phone'])
            && !empty($_POST['comment'] && strpos($this->referer, "contacts") !== false)
        ) {
            $this->mailSend();
        }





    }

    private function getCurPage($cur_page)
    {


        if($cur_page == "/plastic/" or $cur_page == "/plastic-dev/"){
            return 'Запрос на консультацию с главной страницы направления "Пластическая хирургия"';
        }

        if($cur_page == "/cosmetology/"){
            return 'Запрос на консультацию с главной страницы направления "Косметология"';
        }

        if($cur_page == "/dentistry/"){
            return 'Запрос на консультацию с главной страницы направления "Стоматология"';
        }

        if($cur_page == "/gynecology/"){
            return 'Запрос на консультацию с главной страницы направления "Гинекология"';
        }
        if($cur_page == "/transplantation/"){
            return 'Запрос на консультацию с главной страницы направления "Пересадка волос"';
        }
        if($cur_page == "/phlebology/"){
            return 'Запрос на консультацию с главной страницы направления "Флебология"';
        }
        if(!empty($_POST['title'])){
            return $_POST['title'];
        }else{
            return 'Запрос на консультацию с главной страницы';
        }


    }

}