<?
$sectionCode = false;
$sectionLeftMargin = false;
$sectionRightMargin = false;

if(!empty($_GET["SECTION_CODE"])){
	$sectionCode = $_GET["SECTION_CODE"];
	$sectionCodeExplode = explode('?', $sectionCode);
	$sectionCode = reset($sectionCodeExplode);
}

if($sectionCode){
	$sectionRes = CIBlockSection::GetList(
		[],
		["CODE" => $sectionCode]
	);

	$section = $sectionRes->Fetch();
	if($section){
		$sectionLeftMargin = $section["LEFT_MARGIN"];
		$sectionRightMargin = $section["RIGHT_MARGIN"];
	}
}
