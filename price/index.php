<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if ($_SERVER['REQUEST_URI'] == '/price/cosmetology/') {
    $APPLICATION->SetTitle("Цены на косметологию в клинике Medall");
    $APPLICATION->SetPageProperty("description", "Цены на косметологию в Спб. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic.");
} elseif ($_SERVER['REQUEST_URI'] == '/price/plastic/') {
    $APPLICATION->SetTitle("Цены на пластическую хирургию в клинике Medall");
    $APPLICATION->SetPageProperty("description", "Цены в Спб на пластические операции. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic.");
} elseif ($_SERVER['REQUEST_URI'] == '/price/') {
    $APPLICATION->SetTitle("Цены на услуги пластической хирургии, косметологии и стоматологии Медалл");
    $APPLICATION->SetPageProperty("description", "Цены на услуги в Спб. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic.");
}
?>
<div class="mainContent innerPageContent innerPageContent--padding">

	<section class="mainContent__left">
		<div class="mainContent__wrap">
			<? include __DIR__."/main_content/index.php" ?>
		</div>
	</section>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
