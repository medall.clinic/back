
<section class="page-section container">
        <div class="content">
          <h2 class="page-subsection page-subheader">специализация</h2>
          <div class="page-subsection">
            <div class="specialisation">
              <div class="specialisation__name">ИМПЛАНТАЦИЯ ЗУБОВ</div>
              <div class="specialisation__arrow"></div>
              <ul class="specialisation__list">
                <li class="specialisation__item">NobelZygoma® Зигома</li>
                <li class="specialisation__item">Имплантация All-on-4®</li>
              </ul>
            </div>
            <div class="specialisation">
              <div class="specialisation__name">ХИРУРГИЯ И ИМПЛАНТАЦИЯ</div>
              <div class="specialisation__arrow"></div>
              <ul class="specialisation__list">
                <li class="specialisation__item">Имплантация All-on-6</li>
              </ul>
            </div>
          </div>
        </div>
      </section>