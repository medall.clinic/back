<section class="page-section page-section--background-color page-section--background-color--dark container">
        <div class="content">
          <h2 class="page-subsection page-subheader">ЗАПИШИТЕСЬ НА КОНСУЛЬТАЦИЮ<small>любым удобным Вам способом</small></h2>
          <div class="contacts page-subsection">
            <div class="contacts__item contacts__item--whatsapp contacts__item--small">+7 (931) 349-18-98</div>
            <div class="contacts__item contacts__item--tel contacts__item--small">+7 (812) 670-56-75</div>
            <div class="contacts__item contacts__item--email contacts__item--small">admin@medall.clinic</div>
            <div class="contacts__item contacts__item--address">Левашовский пр., 24, Санкт-Петербург, 197110</div>
          </div>
          <div class="grid grid--justify--center page-subsection">
            <form class="grid__cell grid__cell--l--10 grid__cell--xs--12 grid contacts_to_bitrix24">
              <div class="form-input form-input--text grid__cell grid__cell--m--6 grid__cell--xs--12">
                <input class="form-input__field" type="text" name="firstName" required="required" placeholder="Имя *"></input>
              </div>
              <div class="form-input form-input--text grid__cell grid__cell--m--6 grid__cell--xs--12">
                <input class="form-input__field" type="text" name="lastName" required="required" placeholder="Фамилия *"></input>
              </div>
              <div class="form-input form-input--tel grid__cell grid__cell--m--6 grid__cell--xs--12">
                <input class="form-input__field" type="tel" name="tel" required="required" placeholder="Телефон *"></input>
              </div>
              <div class="form-input form-input--email grid__cell grid__cell--m--6 grid__cell--xs--12">
                <input class="form-input__field" type="email" name="email" required="required" placeholder="E-mail *"></input>
              </div>
              <div class="form-input form-input--textarea grid__cell grid__cell--xs--12">
                <textarea class="form-input__field" name="email" required="required"></textarea>
              </div>
              <div class="grid__cell grid__cell--xs--12">
                <p>Нажимая на кнопку «Отправить», я даю согласие на обработку персональных данных</p>
              </div>
              <button class="button button--secondary grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--xs--6" type="submit">Отправить
              </button>
            </form>
          </div>
        </div>
      </section>