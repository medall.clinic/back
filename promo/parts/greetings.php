<section class="page-section page-section--background-color page-section--background-color--gradient container">
	<div class="content">
		<div class="grid grid--cols--12">
			<div class="grid__cell grid__cell--xs--12 grid__cell--s--8 grid__cell--m--6 grid__cell--l--6">
				<h1 class="page-header">ЗДРАВСТВУЙТЕ! <br> БЛАГОДАРИМ ЗА&nbsp;ИНТЕРЕС <br> К&nbsp;КЛИНИКЕ MEDALL!</h1>
				<p>Подписывайтесь на наши социальные сети, там мы делимся новостями клиники, работами врачей и действующими акциями.</p>
			</div>
		</div>
	</div>
</section>
