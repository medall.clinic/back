<style scoped>
	:root {
		--form-field-input-color: #045658;
		--form-field-input-background-color: #f5f8f6;
		--form-field-input-border-color: #f5f8f6;
		--form-field-input-border-color-focus: #045658;
		--form-field-input-border-radius: 16px;
		--form-field-options-transition-duration: .4s;
	}
	.discounts-form {
		display: flex;
		flex-direction: column;
	}
	.discounts-subtitle+.discounts-form__description,
	.discounts-form__description+.discounts-form__field {
		margin-top: 40px;
	}
	.discounts-form__field+.discounts-form__field {
		margin-top: 30px;
	}
	.form-input {
		isolation: isolate;
		display: block;
	}
	.form-input__current, .form-input__field {
		font-size: 18px !important;
		line-height: 1.5;
		-webkit-appearance: none;
		-moz-appearance: none;
		appearance: none;
		display: block;
		padding: 15px 32px 16px;
		color: var(--form-field-input-color);
		border: 1px solid;
		border-color: var(--form-field-input-border-color) !important;
		border-radius: var(--form-field-input-border-radius);
		transition: border-color var(--form-field-options-transition-duration);
		resize: none;
	}
	.form-input__current, .form-input__field, .form-input__options {
		width: 100%;
		background-color: var(--form-field-input-background-color);
	}
	.form-input__current:focus, .form-input__field:focus {
		outline: 0;
		--form-field-input-border-color: var(--form-field-input-border-color-focus);
		--form-field-options-transition-duration: .1s;
	}
	.discounts-form__agreement {
		font-size: 14px;
		line-height: 1.5;
		margin-top: 10px;
		text-align: center;
	}
	.discounts-form__agreement {
		font-size: 14px;
		line-height: 1.5;
		margin-top: 10px;
		text-align: center;
	}
	.discounts-form__submit {
		align-self: center;
		width: fit-content !important;
		margin-top: 24px;
	}
	@media (min-width: 760px){
		.discounts-subtitle {
			font-size: 32px;
			line-height: 1.25;
			text-transform: uppercase;
		}
	}
</style>

<form
	class="grid__cell grid__cell--m--8 grid__cell--s--10 grid__cell--xs--12 discounts-form contacts_to_bitrix24"
	id="discounts-form"
>

	<h2 class="page-subsection page-subheader">заполните данные и мы совсем скоро свяжемся с вами</h2>

	<p class="discounts-form__description">В комментарии напишите, какая услуга и акция вас интересует</p>

	<label class="form-input form-input--text discounts-form__field discounts-form__field">
		<input class="form-input__field" type="text" placeholder="ФИО *" name="name" required>
	</label>

	<label class="form-input form-input--tel discounts-form__field discounts-form__field">
		<input class="form-input__field" type="tel" placeholder="Телефон *" name="phone" required>
	</label>

	<label class="form-input form-input--email discounts-form__field discounts-form__field">
		<input class="form-input__field" type="email" placeholder="Электронная почта" name="email">
	</label>

	<label class="form-input form-input--textarea discounts-form__field discounts-form__field">
		<textarea class="form-input__field" placeholder="Комментарии" name="comments"></textarea>
	</label>

	<p class="discounts-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</p>

    <input type="hidden" name="title" value="Заявка журнал Россия">

	<button
        class="button button--type--submit discounts-form__submit"
        type="submit"
    >Оставить заявку</button>

	<? if(!empty($_GET)): ?>
		<? foreach ($_GET as $paramName => $paramValue): ?>
			<? if(strpos($paramName, 'utm_') !== false): ?>
                <input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
			<? endif ?>
		<? endforeach ?>
	<? endif ?>

</form>

<style>
    .invalid-feedback{
        padding: 5px;
        color: IndianRed;
        font-size: 0.87rem;
        opacity: 0.8;
        bottom: 0;
    }
    input.is-invalid, textarea.is-invalid{
        border-color: IndianRed;
    }
</style>
