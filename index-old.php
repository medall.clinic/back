<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Medall - клиника эстетической медицины и цифровой стоматологии");
$APPLICATION->SetPageProperty("description", "Клиника эстетической медицины и цифровой стоматологии Medall в Санкт-Петербурге представляет профессиональные услуги любой сложности. У нас вы можете провести операции подтяжки лица, липосакции, маммопластики и другие процедуры. Наши специалисты предоставляют персональный подход и помогают выбрать оптимальный метод лечения. Узнайте стоимость и сроки проведения операции на нашем сайте или по телефону: +7 (812) 603-02-01.");
$APPLICATION->SetTitle("Medall - клиника эстетической медицины и цифровой стоматологии");?>
<!-- Главный экран главной страницы-->

<section class="homeMainScreen">

    <? if(!hideContentForAdmin()): ?>
      <div class="homeMainScreen__image">
        <video autoplay="" muted="" loop="" id="myVideo" style="width: 100%;height: 100%;object-fit: cover;">
          <source src="/bitrix/templates/corp_services_gray_new/video/home-background.mp4" type="video/mp4">
        </video>
      </div>
  <? endif ?>
    
	<div class="container">
		<div class="homeMainScreen__block">
			<div class="homeMainScreen__title title">
				<h1>Клиника MEDALL<br>Эстетическая медицина Цифровая стоматология</h1>
			</div>
		</div>
	</div>
</section>
<? /* <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"bgimg",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => 1,
		"FIELD_CODE" => array("",""),
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "service_inf",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array("","video",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
);?> */ ?> <!-- О клинике-->
<div class="aboutClinikMainScreen">
	<div class="container">
		<div class="aboutClinikMainScreen__title title">
			<h1>Клиника MEDALL – всё лучшее в медицине для Вас!</h1>
		</div>
		<div class="aboutClinikMainScreen__desc">
			<a href="https://medall.clinic/lp/giftlandings%20/" class="button" style="width: fit-content; padding-right: 2rem; padding-left: 2rem;">Подарочный сертификат</a>
			<? /* <p>MEDALL — ВСЕ ЛУЧШЕЕ В МЕДИЦИНЕ ДЛЯ ВАС!</p>
      <p>У клиники два основных направления работы:  эстетическая медицина и цифровая стоматология.</p>
      <p>Направление эстетической медицины включает в себя:</p>
      <p>
        ● отделение пластической хирургии<br>
        ● отделение косметологии<br>
        ● отделение пересадки волос<br>
        ● отделение флебологии и лечения трофических язв<br>
        ● отделение мануальной терапии
      </p> */ ?>
		</div>
	</div>
</div>
 <!-- Блок с ссылками--> <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"main_direction",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array("PREVIEW_TEXT",""),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "service_inf",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array("URL",""),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N"
	)
);?> <!-- Наша философия--> <section class="ourPhilosophy">
<div class="container">
	<div class="ourPhilosophy__block">
		<div class="ourPhilosophy__left wow animated fadeInLeft">
			<div class="ourPhilosophy__left__text">
                <p>История клиники насчитывает уже более 15&nbsp;лет и&nbsp;нам доверяют свыше
                30&nbsp;000&nbsp;пациентов. </p>
                <p>MEDALL соответствует самым высоким российским и&nbsp;мировым стандартам клиник эстетической медицины и&nbsp;стоматологии. В&nbsp;работе применяется исключительно современное диагностическое и&nbsp;лечебное оборудование, а&nbsp;также новейшие медицинские технологии и&nbsp;материалы.</p>
                <p>В&nbsp;штате клиники числится более 120&nbsp;сотрудников. Специалисты клиники регулярно и&nbsp;непрерывно повышают свою квалификацию как в&nbsp;России, так и&nbsp;за&nbsp;рубежом; являются действующими членами известных профильных ассоциаций и&nbsp;участниками международных конгрессов, лидерами мнений новейших методик, ведущих производителей оборудования и&nbsp;материалов. Их&nbsp;опыт подтвержден десятками тысяч оказанных услуг и&nbsp;проведенных операций.</p>
			</div>
		</div>
		<div class="ourPhilosophy__right wow animated fadeInRight">
			<div class="ourPhilosophy__title title">
				<h2>Наша философия</h2>
			</div>
			<div class="ourPhilosophy__desc">
				<p>
					 Основной принцип нашей работы – предлагать пациенту не отдельную методику, а комплексную программу коррекции. Поэтому мы собрали команду профессионалов, которые дополняют друг друга и способны решить любую проблему по профильным медицинским направлениям Клиники.
				</p>
				<p>
					 Открытый, доверительный контакт между врачом и пациентом - незаменимая основа для достижения успешного результата. В Клинике создана уютная и практически семейная атмосфера. Врачи работают как единая команда, нацеленная на удовлетворение индивидуальных потребностей каждого пациента.
				</p>
				<p>
					 MEDALL - все лучшее в медицине для Вас!
				</p>
			</div>
			<? /* <div class="ourPhilosophy__link">
 <a class="button" href="/about">О клинике</a>
			</div> */ ?>
		</div>
	</div>
</div>
 </section>
<!-- Все врачи--> <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"doctor_home",
	array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "360000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "",
		),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "doctor",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "30",
		"PROPERTY_CODE" => array(
			0 => "io_doctor",
			1 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"COMPONENT_TEMPLATE" => "doctor_home",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?> <!-- Наша география-->
<div <? /* class="ourGeograpfy" */ ?>>
	<? /* <div class="container">
		<div class="ourGeograpfy__title title beforeLine">
			 Наша география
		</div>
		<div class="ourGeograpfy__desc">
			 К нашим специалистам едут не только со всех уголков России, <br>
			 но и со всего мира
		</div>
		<div class="ourGeograpfy__blockMap">
			<div class="ourGeograpfy__locationItem item1">
				<div class="ourGeograpfy__locationItem__text">
					 Канада
				</div>
			</div>
			<div class="ourGeograpfy__locationItem item2">
				<div class="ourGeograpfy__locationItem__text">
					 Санкт-Петербург
				</div>
			</div>
			<div class="ourGeograpfy__locationItem item3">
				<div class="ourGeograpfy__locationItem__text">
					 Точка 3
				</div>
			</div>
			<div class="ourGeograpfy__locationItem item4">
				<div class="ourGeograpfy__locationItem__text">
					 Точка 4
				</div>
			</div>
			<div class="ourGeograpfy__locationItem item5">
				<div class="ourGeograpfy__locationItem__text">
					 Точка 5
				</div>
			</div>
			<div class="ourGeograpfy__locationItem item6">
				<div class="ourGeograpfy__locationItem__text">
					 Точка 6
				</div>
			</div>
		</div> */ ?>
		<div class="ourGeograpfy__logos">
			<? /* <div class="ourGeograpfy__logos__item">
				<div class="ourGeograpfy__logos__item__img">
 <img src="/bitrix/templates/corp_services_gray_new/img/logosPartner1.jpg" alt="картинка">
				</div>
				<div class="ourGeograpfy__logos__item__text">
					<p>
						 База педиатрической <br>
						 академии <br>
						 Санкт-Петербурга
					</p>
				</div>
			</div> */ ?>
			<div class="ourGeograpfy__logos__item">
				<div class="ourGeograpfy__logos__item__img">
 <img src="/bitrix/templates/corp_services_gray_new/img/logosPartner2.jpg" alt="картинка">
				</div>
				<div class="ourGeograpfy__logos__item__text">
					<p>
						 Член Ассоциации <br>
						 Частных Клиник <br>
						 Санкт-Петербурга
					</p>
				</div>
			</div>
			<div class="ourGeograpfy__logos__item">
				<div class="ourGeograpfy__logos__item__img">
 <img src="/bitrix/templates/corp_services_gray_new/img/logosPartner3.jpg" alt="картинка">
				</div>
				<div class="ourGeograpfy__logos__item__text">
					<p>
						 База медико <br>
						 социального института
					</p>
				</div>
			</div>
			<div class="ourGeograpfy__logos__item">
				<div class="ourGeograpfy__logos__item__img">
                    <img src="/bitrix/templates/corp_services_gray_new/img/logosPartner4.jpg" alt="картинка">
				</div>
				<div class="ourGeograpfy__logos__item__text">
					<p>
						 Cтатус референс <br>
						 клиники KaVo
					</p>
				</div>
			</div>
			<div class="ourGeograpfy__logos__item">
				<div class="ourGeograpfy__logos__item__img">
                    <img src="/bitrix/templates/corp_services_gray_new/img/logosPartner5.jpg" alt="картинка">
				</div>
				<div class="ourGeograpfy__logos__item__text">
					<p>
						 Центр клинического мастерства
						 <br>
						 Nobel Biocare
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
 <!-- Скидка на пластику груди--> <section class="discountBreastPlastic">
<div class="discountBreastPlastic__wrap">
	<div class="discountBreastPlastic__slider">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"stock_home",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array("DATE_ACTIVE_FROM","DATE_ACTIVE_TO",""),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "stock",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array("title_action","direction_action","moreprocedure_action","bg_action","img_main_action","img_bg_action",""),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N"
	)
);?><br>
	</div>
	<div class="discountBreastPlastic__controlsWrap">
		<div class="discountBreastPlastic__control discountBreastPlastic__prev">
		</div>
		<div class="discountBreastPlastic__control discountBreastPlastic__next">
		</div>
	</div>
</div>
 </section>
 <div class="services-main-photos">
 	<div class="services-main-photos__wrapper">
 		<div class="services-main-photos__slider">
 			<div class="services-main-photos__item"><img src="/images/services/slider-1.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-18.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-19.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-20.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-2.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-3.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-4.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-5.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-6.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-7.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-8.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-9.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-10.jpg" alt=""> </div>
 			<!-- <div class="services-main-photos__item"><img src="/images/services/slider-11.jpg" alt=""> </div> -->
 			<!-- <div class="services-main-photos__item"><img src="/images/services/slider-12.jpg" alt=""> </div> -->
 			<div class="services-main-photos__item"><img src="/images/services/slider-13.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-14.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-15.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-16.jpg" alt=""> </div>
 			<div class="services-main-photos__item"><img src="/images/services/slider-17.jpg" alt=""> </div>
 		</div>
 		<div class="services-main-photos__arrows">
 			<div class="services-main-photos__arrow services-main-photos__arrow--prev"></div>
 			<div class="services-main-photos__arrow services-main-photos__arrow--next"></div>
 		</div>
 	</div>
 </div>
<br>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
