<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
?><!DOCTYPE html>
<html lang="ru">

    <head>
        <?php $APPLICATION->ShowHead() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include __DIR__."/include/styles-header.php" ?>
        <title><?php $APPLICATION->ShowTitle() ?></title>
        <link rel="icon" href="https://medall.clinic/favicon.ico?v=3" type="image/x-icon">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <link rel="preload" href="/local/front/template/fonts/Montserrat-Regular.woff2" as="font" type="font/woff2" crossorigin="anonymous">
        <link rel="preload" href="/local/front/template/fonts/Montserrat-Medium.woff2" as="font" type="font/woff2" crossorigin="anonymous">
        <link rel="preload" href="/local/front/template/fonts/Montserrat-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
        
        <?php $request_uri = $_SERVER['REQUEST_URI'];?>


        <?php if (preg_match('/\?PAGEN_\d+/', $request_uri)) {?>
            <meta name="robots" content="noindex, follow" />
            <link rel="canonical" href="https://medall.clinic/articles/">

        <?php }?>
        <?php if ($request_uri  == '/articles/'){?>
            <link rel="canonical" href="https://medall.clinic/articles/">

        <? } ?>




        <?php define('NEW_TEMPLATE', true) ?>


        <?php

        $p1 = "/dentistry/";
        if (!strstr($APPLICATION->GetCurDir(), $p1)) {?>
            <?php /* <!-- comfortel -->
            <script type="text/javascript">
                var _calltr_obj=".phone";
            </script>
            <script src="https://virt117.pbx.comfortel.pro/call-tracking.js?_id=1624540366" type="text/javascript" async></script>
            <!-- comfortel --> */?>

            <!-- виджеты тг/вотсап/вк -->
            <script>
            (function(w,d,u){
                    var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://cdn-ru.bitrix24.ru/b10164883/crm/site_button/loader_18_kfv3n4.js');
            </script>
            <!-- виджеты тг/вотсап/вк -->
        <?php } ?>
        
        <!-- calltouch -->
        <script>
            (function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)},m=typeof Array.prototype.find === 'function',n=m?"init-min.js":"init.js";s.async=true;s.src="https://mod.calltouch.ru/"+n+"?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","k0satx2r");
        </script>
        <!-- calltouch -->


        <?php /* <!-- calltouch -->
<script>
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)},m=typeof Array.prototype.find === 'function',n=m?"init-min.js":"init.js";s.async=true;s.src="https://mod.calltouch.ru/"+n+"?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","k0satx2r");
</script>
<!-- calltouch -->
        <!--calltouch request-->
        <script>
            Element.prototype.matches || (Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector), Element.prototype.closest || (Element.prototype.closest = function (e) { for (var t = this; t;) { if (t.matches(e)) return t; t = t.parentElement } return null });
            var ct_get_val = function (form, selector) { if (!!form.querySelector(selector)) { return form.querySelector(selector).value; } else { return ''; } }
            document.addEventListener('click', function (e) {
                var t_el = e.target;
                if (t_el.closest('form [type="submit"]')) {
                    try {
                        var form = t_el.closest('form');
                        var fio = ct_get_val(form, '[name="name"]');
                        var phone = ct_get_val(form, '[name="phone"]');
                        var email = ct_get_val(form, '[name="email"]');
                        var comment = ct_get_val(form, '[name="comment"]');
                        var ct_site_id = window.ct('calltracking_params', 'k0satx2r').siteId;;
                        var sub = 'Заявка с ' + location.hostname;

                        var ct_data = {
                            fio: fio,
                            phoneNumber: phone,
                            email: email,
                            subject: sub,
                            comment: comment,
                            requestUrl: location.href,
                            sessionId: window.ct('calltracking_params', 'k0satx2r').sessionId
                        };
                        var post_data = Object.keys(ct_data).reduce(function (a, k) { if (!!ct_data[k]) { a.push(k + '=' + encodeURIComponent(ct_data[k])); } return a }, []).join('&');
                        var CT_URL = 'https://api.calltouch.ru/calls-service/RestAPI/requests/' + ct_site_id + '/register/';
                        if ((!!phone || !!email) && !window.ct_snd_flag) {
                            window.ct_snd_flag = 1; setTimeout(function () { window.ct_snd_flag = 0; }, 20000);
                            var request = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
                            request.open("POST", CT_URL, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            request.send(post_data);
                        }
                    } catch (e) { console.log(e); }
                }
            });
            var _ctreq_b24 = function (data) {
                var sid = window.ct('calltracking_params', 'k0satx2r').siteId;
                var request = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
                var post_data = Object.keys(data).reduce(function (a, k) { if (!!data[k]) { a.push(k + '=' + encodeURIComponent(data[k])); } return a }, []).join('&');
                var url = 'https://api.calltouch.ru/calls-service/RestAPI/' + sid + '/requests/orders/register/';
                request.open("POST", url, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); request.send(post_data);
            };
            window.addEventListener('b24:form:submit', function (e) {
                var form = event.detail.object;
                if (form.validated) {
                    var fio = ''; var phone = ''; var email = ''; var comment = '';
                    form.getFields().forEach(function (el) {
                        if (el.name == 'LEAD_NAME' || el.name == 'CONTACT_NAME') { fio = el.value(); }
                        if (el.name == 'LEAD_PHONE' || el.name == 'CONTACT_PHONE') { phone = el.value(); }
                        if (el.name == 'LEAD_EMAIL' || el.name == 'CONTACT_EMAIL') { email = el.value(); }
                        if (el.name == 'LEAD_COMMENTS' || el.name == 'DEAL_COMMENTS ') { comment = el.value(); }
                    });
                    var sub = form.title || 'Заявка с формы Bitrix24 ' + location.hostname;
                    var ct_data = { fio: fio, phoneNumber: phone, email: email, comment: comment, subject: sub, requestUrl: location.href, sessionId: window.ct('calltracking_params', 'k0satx2r').sessionId };
                    console.log(ct_data);
                    if (!!phone || !!email) _ctreq_b24(ct_data);
                }
            });
        </script>
        <!--calltouch request--> */?>

    </head>

<body>

<?php include $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/landing/include/header/code.php" ?>

<?php /* <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
        k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(67423126, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
        });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/67423126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter --> */ ?>
<?php include __DIR__."/include/header/index.php" ?>
    <main>
        <?php
        $currUrl = $_SERVER["REQUEST_URI"];
        /* Врачи start */
            $IBlockDoctors = 6;
            $patternDoctor = '/(?<=doctor\/)[^\/]+/';
            $filter = '/(?<=rezultaty-rabot\/)[^\/]+/';
            $basePathDoctor = '/doctor/';
            $codeDoctor = '';
            $arFieldsDoctor = [];
        if (preg_match($patternDoctor, $currUrl, $matches)) {
            $result = $matches[0];
            $codeDoctor = $result;

        }
        $arSelectDoctor =
            [
                'IBLOCK_NAME',
                'NAME',
                'CODE',
                'IBLOCK_CODE',
            ];
        $arFilterDoctor = [
            'IBLOCK_ID'    => $IBlockDoctors,
            'ACTIVE_DATE'  => 'Y',
            'ACTIVE'       => 'Y',
            'CODE'         => $codeDoctor,
        ];

        $resDoctor = CIBlockElement::GetList([], $arFilterDoctor, false, false, $arSelectDoctor);
        while($obDoctor = $resDoctor->GetNext())
        {
            $arFieldsDoctor = $obDoctor;
        }


        //end
        $IBlockBlockService = 38;
        $IBlockPageNoSection = 11;
        $IBlockBlockPage = 60;
        $IBlockLandingId = 47;
        $sectionCode = $currUrl;
        $patternService = '/(?<=plastic\/|cosmetology\/|about\/|hr\/|gynecology\/|phlebology\/|dentistry\/|transplatation\/|rassrochka\/';
        $patternService .= 'soglasheniya-na-obrabotku-personalnykh-dannykh\/|laboratorymedall\/)[^\/]+/';
        $sectionCode = preg_replace("/^\/|\/$/", "", $currUrl);
        $elementCode = explode("/", $sectionCode);
        $arFilterServiceSection = ['IBLOCK_ID' => [$IBlockBlockService,$IBlockBlockPage,$IBlockLandingId],'CODE' => $elementCode[0]];
        $arSelectServiceSection = ['ID', 'NAME', 'CODE',];
        $arFieldsService = [];

        // Получаем раздел по коду
        $res = CIBlockSection::GetList(
            [],
           $arFilterServiceSection,
            false,
           $arSelectServiceSection
        );

        while($obSection = $res->GetNext()){
            $arFieldsServiceSection = $obSection;

        }
        $arSelectService =
            [
                'IBLOCK_NAME',
                'NAME',
                'CODE',
                'IBLOCK_CODE',
                'IBLOCK_SECTION_ID',
            ];
        $arFilterService = [
            'IBLOCK_ID' => [$IBlockLandingId,$IBlockBlockService,$IBlockPageNoSection,],
            'ACTIVE_DATE' => 'Y',
            'ACTIVE' => 'Y',
            'CODE' =>array($elementCode[0],$elementCode[1], $elementCode[2]),
        ];
        $resService = CIBlockElement::GetList([], $arFilterService, false, false, $arSelectService);
        while($obService = $resService->GetNext())
        {
            $arFieldsService = $obService;

        }

        if (preg_match($patternDoctor, $currUrl, $matches)) {?>

            <div class="container breadcrumbs">
                <div class="content breadcrumbs__inner">
                    <a class="breadcrumbs__parent" href="/">Главная</a>
                    <?php if (!empty($arFieldsDoctor['IBLOCK_NAME'])) {?>
                        <a class="breadcrumbs__parent" href="/<?php echo $arFieldsDoctor['IBLOCK_CODE'] ?>/"><?php echo $arFieldsDoctor['IBLOCK_NAME']?></a>

                    <?php }?>
                    <?php if (!empty($arFieldsDoctor['NAME'])) {?>
                        <div class="breadcrumbs__current"><?php echo $arFieldsDoctor['NAME']?></div>
                    <?php }?>
                </div>
            </div>
        <?php } elseif (preg_match($basePathDoctor, $_SERVER['REQUEST_URI'],$matches) && !preg_match($filter, $_SERVER['REQUEST_URI'], $matches)){?>
            <div class="container breadcrumbs">
                <div class="content breadcrumbs__inner">
                    <a class="breadcrumbs__parent" href="/">Главная</a>
                    <?php if (!empty($arFieldsDoctor['IBLOCK_NAME'])) {?>
                        <div class="breadcrumbs__current"><?php echo $arFieldsDoctor['IBLOCK_NAME']?></div>

                    <?php }?>

                </div>
            </div>
        <?php } ?>

        <?php if(
                $currUrl == '/plastic/'
                || $currUrl == '/cosmetology/'
                || $currUrl == '/about/'
                || $currUrl == '/gynecology/'
                || $currUrl == '/phlebology/'
                || $currUrl == '/hr/'
                || $currUrl == '/patsientam/'
                || $currUrl == '/dentistry/'
                || $currUrl == '/transplantation/'
                || $currUrl == '/rassrochka/'
                || $currUrl == '/soglasheniya-na-obrabotku-personalnykh-dannykh/'
                || $currUrl == '/laboratorymedall/'
                || $currUrl == '/anketa/'
                || $currUrl == '/nalog/'
                || $currUrl == '/merch/'

        ) {?>

             <?php /* Если нет дочерней станицы */?>

            <div class="container breadcrumbs">
                <div class="content breadcrumbs__inner">
                    <a class="breadcrumbs__parent" href="/">Главная</a>
                    <?php if (!empty($arFieldsServiceSection['NAME'])) {?>
                        <div class="breadcrumbs__current"><?php echo $arFieldsServiceSection['NAME'] ?></div>
                    <?php } else {?>
                        <div class="breadcrumbs__current"><?$APPLICATION->ShowTitle()?></div>
                    <?php } ?>
                </div>
            </div>
        <?php }


        if (preg_match($patternService,$currUrl,$matches)){?>
            <div class="container breadcrumbs">
                <div class="content breadcrumbs__inner">
                    <a class="breadcrumbs__parent" href="/">Главная</a>
                    <?php if (!empty($arFieldsServiceSection['NAME'])) {?>
                        <a class="breadcrumbs__parent" href="/<?php echo $arFieldsServiceSection['CODE'] ?>/"> <?php echo  $arFieldsServiceSection['NAME']?></a>
                    <?php }?>
                    <?php if (!empty($arFieldsService['NAME'])) {?>
                        <div class="breadcrumbs__current"><?php echo $arFieldsService['NAME']?></div>
                    <?php }?>
                </div>
            </div>
        <?php } ?>






