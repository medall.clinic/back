<div class="modal modal--width--32rem" id="ajax-response">
    <div class="modal__inner">
        <div class="h4 modal-header">
            <div class="modal-title" data-title=""></div>
        </div>
        <div class="modal-body p" data-body=""></div>
        <button class="modal__close" type="button"></button>
    </div>
</div>