<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$styles = [];
global $APPLICATION;
$styles[] = "rest";
if ($_SERVER['REQUEST_URI'] == '/price-list/'){
    $styles[] = 'price-list.rest';
}
if ($APPLICATION->GetCurPage(false) == '/prices/'){
    $styles[] = 'prices.rest';
}
if (strpos($_SERVER['REQUEST_URI'], '/articles/') === 0) {
    $styles[] = "article.rest";
}
if (strpos($_SERVER['REQUEST_URI'], '/rezultaty-rabot/') !== false){
    $styles[] = "works.rest";
}
if (strpos($_SERVER['REQUEST_URI'], '/review/') !== false){
    $styles[] = "reviews.rest";
}


if(isHomePage()){
	$styles[] = "main.rest";
}

if(is404Page()){
	$styles[] = "404.rest";
}

if(isDiscountsPage()){
	$styles[] = "discounts.rest";
}

if(isLandingPage()){
	$styles[] = "landing.rest";
}

if(isCategoryPage()){
	$styles[] = "category.rest";
}

if(isSearchPage()){
	$styles[] = "search.rest";
}

if(isDoctorsPage()){
	$styles[] = "doctors.rest";
}

if(isDoctorPage()){
	$styles[] = "doctor.rest";
}

if (isDocumentsPage()) {
	$styles[] = "documents.rest";
}

if (isContactsPage()) {
	$styles[] = "contacts.rest";
}

if ($_SERVER['REQUEST_URI'] == '/about/vakansii/') {
	$styles[] = "vacancies.rest";
}

foreach($styles as $style): ?>
    <link rel="preload" href="/local/front/template/styles/<?= $style ?>.css?v=<?= require "version.php" ?>" as="style" onload="this.onload=null;this.rel='stylesheet'">
<? endforeach ?>

