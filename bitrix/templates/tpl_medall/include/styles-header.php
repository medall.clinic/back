<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$styles = [];

$styles[] = "core";
if (strpos($_SERVER['REQUEST_URI'], '/articles/') === 0){
    $styles[] = "article.core";
}
if (strpos($_SERVER['REQUEST_URI'], '/rezultaty-rabot/') !== false){
    $styles[] = "works.core";
}
if (strpos($_SERVER['REQUEST_URI'], '/review/') !== false){
    $styles[] = "reviews.core";
}
if ($_SERVER['REQUEST_URI'] == '/price-list/'){
    $styles[] = "price-list.core";
}
if (strpos($_SERVER['REQUEST_URI'],'/prices/') !== false){
    $styles[] = "prices.core";
}
if(isHomePage()){
	$styles[] = "main.core";
}

if(is404Page()){
	$styles[] = "404.core";
}

if(isDiscountsPage()){
	$styles[] = "discounts.core";
}

if(isCategoryPage()){
	$styles[] = "category.core";
}

if(isSearchPage()){
	$styles[] = "search.core";
}

if(isDoctorsPage()){
	$styles[] = "doctors.core";
}

if(isDoctorPage()){
	$styles[] = "doctor.core";
}

if (isDocumentsPage()) {
	$styles[] = "documents.core";
}

if (isContactsPage()) {
	$styles[] = "contacts.core";
}

if ($_SERVER['REQUEST_URI'] == '/about/vakansii/') {
	$styles[] = "vacancies.core";
}

foreach($styles as $style): ?>
    <link rel="preload" href="/local/front/template/styles/<?= $style ?>.css?v=<?= require "version.php" ?>" as="style" onload="this.onload=null;this.rel='stylesheet'">
<? endforeach ?>

<? if (isHomePage()) { ?>
	<link rel="preload" as="image" href="/local/front/template/images/home-header_xs.webp" type="image/webp" importance="high">
	<link rel="preload" as="image" href="/local/front/template/images/home-header_xs@2x.webp" type="image/webp" importance="high">
	<link rel="preload" as="image" href="/local/front/template/images/home-header_l.webp" type="image/webp" importance="high">
	<link rel="preload" as="image" href="/local/front/template/images/home-header_l@2x.webp" type="image/webp" importance="high">
<? } ?>
