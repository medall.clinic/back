<!-- <a class="header__tel" href="tel:+78126030201" style="white-space: nowrap;">+7 (812) 603-02-01</a> -->
<a class="header__tel-mobile hidden-hd hidden-xl hidden-l hidden-m hidden-s" href="tel:+78126030201"></a>
<a class="header__tel hidden-xs" href="tel:+78126030201">+7 (812) 603-02-01</a>

<div class="header__address">Левашовский пр., д.24</div>

<a class="header__email" href="mailto:admin@medall.clinic">admin@medall.clinic</a>

<div class="header__schedule">Ежедневно с 09:00 до 21:00</div>
