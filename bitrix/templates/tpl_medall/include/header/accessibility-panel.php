<div class="accessibility-panel header__accessibility-panel">
            <div class="accessibility-panel__section accessibility-panel__section--fotn-size">
              <div class="accessibility-panel__subtitle">Размер шрифта</div>
              <label class="accessibility-panel__item accessibility-panel__item--font-size accessibility-panel__item--font-size--small">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-font-size" value="small" checked><span class="accessibility-panel__label"></span>
              </label>
              <label class="accessibility-panel__item accessibility-panel__item--font-size accessibility-panel__item--font-size--medium">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-font-size" value="medium"><span class="accessibility-panel__label"></span>
              </label>
              <label class="accessibility-panel__item accessibility-panel__item--font-size accessibility-panel__item--font-size--big">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-font-size" value="large"><span class="accessibility-panel__label"></span>
              </label>
            </div>
            <div class="accessibility-panel__section accessibility-panel__section--color">
              <div class="accessibility-panel__subtitle">Цвет фона и шрифта</div>
              <label class="accessibility-panel__item accessibility-panel__item--color accessibility-panel__item--color--black-on-white">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-color" value="black-on-white" checked><span class="accessibility-panel__label"></span>
              </label>
              <label class="accessibility-panel__item accessibility-panel__item--color accessibility-panel__item--color--white-on-black">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-color" value="white-on-black"><span class="accessibility-panel__label"></span>
              </label>
            </div>
            <div class="accessibility-panel__section accessibility-panel__section--images">
              <div class="accessibility-panel__subtitle">Изображения</div>
              <label class="accessibility-panel__item accessibility-panel__item--image accessibility-panel__item--image--on">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-images" value="on" checked><span class="accessibility-panel__label"></span>
              </label>
              <label class="accessibility-panel__item accessibility-panel__item--image accessibility-panel__item--image--off">
                <input class="accessibility-panel__radio" type="radio" name="accessibility-images" value="off"><span class="accessibility-panel__label"></span>
              </label>
            </div>
            <div class="accessibility-panel__section">
              <button class="accessibility-panel__button" type="button">Обычная версия сайта</button>
            </div>
          </div>