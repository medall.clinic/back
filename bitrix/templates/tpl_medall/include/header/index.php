<header class="container header">
    <? include __DIR__."/accessibility-panel.php" ?>
    <div class="content header__inner">

        <? include __DIR__."/logo.php" ?>
        <? include __DIR__."/contacts.php" ?>
        <button class="header__accessibility-button" type="button"></button>
        <? include __DIR__."/search.php" ?>
        <a class="header__sign-button button hidden-s hidden-xs" href="#">Записаться на прием</a>
        <a class="header__sign-button header__sign-button--mobile hidden-hd hidden-xl hidden-l hidden-m" href="#"></a>
        <? include __DIR__."/menu-hamburger.php" ?>
        <? /* include __DIR__."/new_menu.php" */?>
        <nav class="header-services-top header__services-top">
          <ul class="header-services-top__list"></ul>
        </nav>
    </div>


    <?$p1 = "/dentistry/";?>

    <?if (strstr($APPLICATION->GetCurDir(), $p1)) { ?>

        <!-- виджеты тг/вотсап/вк -->
        <script>
            (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://cdn-ru.bitrix24.ru/b10164883/crm/site_button/loader_20_41ofgh.js');
        </script>
        <!-- виджеты тг/вотсап/вк -->

        <!--calltouch request-->
        <script>
            Element.prototype.matches || (Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector), Element.prototype.closest || (Element.prototype.closest = function (e) { for (var t = this; t;) { if (t.matches(e)) return t; t = t.parentElement } return null });
            var ct_get_val = function (form, selector) { if (!!form.querySelector(selector)) { return form.querySelector(selector).value; } else { return ''; } }
            document.addEventListener('click', function (e) {
                var t_el = e.target;
                if (t_el.closest('form [type="submit"]')) {
                    try {
                        var form = t_el.closest('form');
                        var fio = ct_get_val(form, '[name="name"]');
                        var phone = ct_get_val(form, '[name="phone"]');
                        var email = ct_get_val(form, '[name="email"]');
                        var comment = ct_get_val(form, '[name="comment"]');
                        var ct_site_id = window.ct('calltracking_params', 'k0satx2r').siteId;;
                        var sub = 'Заявка с ' + location.hostname;

                        var ct_data = {
                            fio: fio,
                            phoneNumber: phone,
                            email: email,
                            subject: sub,
                            comment: comment,
                            requestUrl: location.href,
                            sessionId: window.ct('calltracking_params', 'k0satx2r').sessionId
                        };
                        var post_data = Object.keys(ct_data).reduce(function (a, k) { if (!!ct_data[k]) { a.push(k + '=' + encodeURIComponent(ct_data[k])); } return a }, []).join('&');
                        var CT_URL = 'https://api.calltouch.ru/calls-service/RestAPI/requests/' + ct_site_id + '/register/';
                        if ((!!phone || !!email) && !window.ct_snd_flag) {
                            window.ct_snd_flag = 1; setTimeout(function () { window.ct_snd_flag = 0; }, 20000);
                            var request = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
                            request.open("POST", CT_URL, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            request.send(post_data);
                        }
                    } catch (e) { console.log(e); }
                }
            });
            var _ctreq_b24 = function (data) {
                var sid = window.ct('calltracking_params', 'k0satx2r').siteId;
                var request = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
                var post_data = Object.keys(data).reduce(function (a, k) { if (!!data[k]) { a.push(k + '=' + encodeURIComponent(data[k])); } return a }, []).join('&');
                var url = 'https://api.calltouch.ru/calls-service/RestAPI/' + sid + '/requests/orders/register/';
                request.open("POST", url, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); request.send(post_data);
            };
            window.addEventListener('b24:form:submit', function (e) {
                var form = event.detail.object;
                if (form.validated) {
                    var fio = ''; var phone = ''; var email = ''; var comment = '';
                    form.getFields().forEach(function (el) {
                        if (el.name == 'LEAD_NAME' || el.name == 'CONTACT_NAME') { fio = el.value(); }
                        if (el.name == 'LEAD_PHONE' || el.name == 'CONTACT_PHONE') { phone = el.value(); }
                        if (el.name == 'LEAD_EMAIL' || el.name == 'CONTACT_EMAIL') { email = el.value(); }
                        if (el.name == 'LEAD_COMMENTS' || el.name == 'DEAL_COMMENTS ') { comment = el.value(); }
                    });
                    var sub = form.title || 'Заявка с формы Bitrix24 ' + location.hostname;
                    var ct_data = { fio: fio, phoneNumber: phone, email: email, comment: comment, subject: sub, requestUrl: location.href, sessionId: window.ct('calltracking_params', 'k0satx2r').sessionId };
                    console.log(ct_data);
                    if (!!phone || !!email) _ctreq_b24(ct_data);
                }
            });
        </script>
        <!--calltouch request-->

    <?php } ?>


</header>
<section class="container">
    <div class="content">

        <? include __DIR__."/menu-flat.php" ?>
    </div>
</section>
