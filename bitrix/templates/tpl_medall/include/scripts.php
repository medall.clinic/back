<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<?
$scripts = [
    "script",
    // "bitrix24",
];
$request_uri = $_SERVER['REQUEST_URI'];
if (strpos($request_uri, '/prices/') !== false){
    $scripts[] = "prices";
}
// Условие для проверки request_uri
if (strpos($request_uri, '/articles/') === 0 || preg_match('~^/articles/\?PROPERTY_DIRECTION_ARTICLES_VALUE/[^/]+$~', $request_uri)) {
    $scripts[] = "article";

}


if (strpos($request_uri, '/rezultaty-rabot/') !== false){
    $scripts[] = "works";
		?> <script src="https://cdn.jsdelivr.net/npm/image-compare-viewer@1.5.0/dist/image-compare-viewer.min.js"></script> <?php
}

if (strpos($request_uri, '/review/') !== false){
    $scripts[] = "reviews";
}
if(isHomePage()){
	$scripts[] = "main";
}

if(is404Page()){
	$scripts[] = "404";
}

if(isDiscountsPage()){
	$scripts[] = "discounts";
}

if(isLandingPage()){
	$scripts[] = "landing";
	?> <script src="https://cdn.jsdelivr.net/npm/image-compare-viewer@1.5.0/dist/image-compare-viewer.min.js"></script> <?php
}

if(isCategoryPage()){
	$scripts[] = "category";
}

if(isDoctorsPage()){
	$scripts[] = "doctors";
}

if(isDoctorPage()){
	$scripts[] = "doctor";
	?> <script src="https://cdn.jsdelivr.net/npm/image-compare-viewer@1.5.0/dist/image-compare-viewer.min.js"></script> <?php
}

if (isDocumentsPage()) {
	$scripts[] = "documents";
}

if (isContactsPage()) {
	$scripts[] = "contacts";
}

if ($_SERVER['REQUEST_URI'] == '/about/vakansii/') {
	$scripts[] = "vacancies";
}

foreach($scripts as $script):
    $src = "/local/front/template/scripts/".$script.".js?v=".require "version.php";
    ?>
    <script defer src="<?= $src ?>"></script>
<? endforeach ?>

