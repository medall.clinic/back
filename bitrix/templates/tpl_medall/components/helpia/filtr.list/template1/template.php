<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// Задаем фильтр
global $arFilter;

$arFilter = null;

if(!empty($_REQUEST["SECTION_ID"])){
	$arFilter = ["SECTION_ID" => $_REQUEST["SECTION_ID"]];
}

$propertyCodeValueKey = "PROPERTY_".$arResult['PROPERTY_CODE']."_VALUE";
if(!empty($_REQUEST[$propertyCodeValueKey])){
	$propertyCodeValue = $_REQUEST[$propertyCodeValueKey];
	$arFilter = [$propertyCodeValueKey => $propertyCodeValue];
}
?>
<div class="blockFilter">
	<form method="get" action="" enctype="multipart/form-data">

        <? if(!empty($arResult['sections'])): ?>
            <? foreach($arResult['sections'] as $k => $v): ?>
			<label class="blockFilter__label">
				<input type="checkbox" value="<?= $k ?>" name="SECTION_ID" onclick="submit();">
				<span class="blockFilter__checkbox__text"><?= $v ?></span>
			</label>
			<? endforeach ?>
		<? endif ?>

		<div class="select__block">
			<select class="select" name="PROPERTY_<?=$arResult['PROPERTY_CODE']?>_VALUE" onChange="submit();">
				<option value="">Все направления</option>
				<?foreach($arResult['direction'] as $k=>$v){
					if(!empty($_REQUEST["PROPERTY_".$arResult['PROPERTY_CODE']."_VALUE"]) && $_REQUEST["PROPERTY_".$arResult['PROPERTY_CODE']."_VALUE"] == $v){
						?><option value="<?echo $v;?>" selected><?echo $v;?></option><?
					} else{
						?><option value="<?echo $v;?>"><?echo $v;?></option><?
					}
				}?>
			</select>
		</div>
	</form>
</div>