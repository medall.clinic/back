<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if (!empty($arResult)):?>
    <nav class="header-services-top header__services-top">
        <ul class="header-services-top__list">

            <?
            $previousLevel = 0;
            foreach($arResult as $arItem):?>

            <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
            <?endif?>

            <?if ($arItem["IS_PARENT"]):?>

            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
            <li class="header-services-top__item header-services-top__item--parent">
                <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                <ul class="header-services-top__list header-services-top__list--sub header-services-top__list--sub-1">
                    <?else:?>
                    <li class="header-services-top__item header-services-top__item--sub header-services-top__item--sub-1<?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                        <a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
                        <ul class="header-services-top__list header-services-top__list--sub header-services-top__list--sub-1">
                            <?endif?>

                            <?else:?>

                                <?if ($arItem["PERMISSION"] > "D"):?>

                                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                        <li class="header-services-top__item">
                                            <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                                        </li>
                                    <?else:?>
                                        <li class="header-services-top__item header-services-top__item--sub header-services-top__item--sub-1<?if ($arItem["SELECTED"]):?> item-selected<?endif?>">
                                            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                                        </li>
                                    <?endif?>

                                <?else:?>

                                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                                        <li class="header-services-top__item">
                                            <a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
                                        </li>
                                    <?else:?>
                                        <li class="header-services-top__item header-services-top__item--sub header-services-top__item--sub-1">
                                            <a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
                                        </li>
                                    <?endif?>

                                <?endif?>

                            <?endif?>

                            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

                            <?endforeach?>

                            <?if ($previousLevel > 1)://close last item tags?>
                                <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
                            <?endif?>

                        </ul>
    </nav>
    <div class="menu-clear-left"></div>
<?endif?>
