<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<nav class="header-navigation__menu header-navigation__menu--others">

    <ul class="header-navigation__list">
        <? $previousLevel = 0 ?>
        <? foreach($arResult as $arItem): ?>

            <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
                <?= str_repeat("</ul> </li>", ($previousLevel - $arItem["DEPTH_LEVEL"])) ?>
            <? endif ?>

            <?php
            $classes = ["header-navigation__item"];

            if (!empty($arItem["PARAMS"]["add_class"])) {
                $classes[] = $arItem["PARAMS"]["add_class"];
            }

            if ($arItem["IS_PARENT"]) {
                $classes[] = "header-navigation__item--parent";
            }

            if ($arItem["SELECTED"]) {
                $classes[] = "header-navigation__item--active";
            }

            if ($arItem["DEPTH_LEVEL"] > 1) {
                $classes[] = "header-navigation__item--sub header-navigation__item--sub-1";
            }

            $classes = implode(" ", $classes);
            ?>
        <li class="<?= $classes ?>">

            <a href="<?= !empty($arItem["LINK"]) ? $arItem["LINK"] : '#' ?>"><?= $arItem["TEXT"] ?></a>

            <? if ($arItem["IS_PARENT"]): ?>
                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-1">
            <? else: ?>
                </li>
            <? endif ?>

            <? $previousLevel = $arItem["DEPTH_LEVEL"] ?>

        <? endforeach ?>

        <? if ($previousLevel > 1): //close last item tags ?>
            <?= str_repeat("</ul> </li>", ($previousLevel-1)) ?>
        <? endif ?>

    </ul>
</nav>
