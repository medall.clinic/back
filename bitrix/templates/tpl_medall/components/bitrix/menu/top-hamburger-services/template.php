<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$rsParentSection = CIBlockSection::GetByID(158);
$arrElements = [];
$idService = 159;
$idServiceGrud = 161;
$idServiceBody = 163;
$idServiceDentistryOrtodontiya = 180;
$idServiceDentistryParodontologiya = 453;
$idServiceDentistryOrtopediya = 184;
$idServiceDentistryDetskaya = 183;
$idServiceDentistryTerapiya = 182;
$idServiceDentistryChirurgiya = 181;
$idServiceAppGynecology = 461;
$idServiceGynecologyLech = 462;
$idServiceGynecologyUhod = 463;
$idServiceGynecologyInjector = 460;
$idServiceFace = 162;
$idServiceCosmetologyMethod = 398;
$idServiceCosmetologyUhod = 450;
$idServiceCosmetologyLazer = 452;
$idServiceCosmetologyInjection = 451;
$idServiceCosmetologyTrihology = 438;
$idServiceCosmetologyManual = 439;
$idServiceCosmetologyReabilitation = 454;
$idServiceCosmetologyTherapy = 182;
$idServiceCosmetologyLab = 3479;
$subMenuLevelOne = false;

/* получение разделов */
if ($arParentSection = $rsParentSection->GetNext()) {
    $arFilter = [
        'IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
        '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
        '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
        '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
    ]; // выберет потомков без учета активности

    $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilter);
    while ($arSect = $rsSect->Fetch()) {
        $arrElements[] = $arSect;
    }
}

/* Грудь */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceGrud,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsPlasticGrud[] = $ob;
}

foreach ($arFieldsPlasticGrud as $arField) {
    $arPlasticGrud[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalPlasticGrud = implode("\n", $arPlasticGrud);

/* end Грудь */

/* Тело */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceBody,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFielarPlasticBody[] = $ob;
}

foreach ($arFielarPlasticBody as $arField) {
    $arPlasticBody[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalPlasticBody = implode("\n", $arPlasticBody);

/* end Тело */

/* Лицо */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL', 'PROPERTY_655'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceFace,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsPlasticFace[] = $ob;
}

foreach ($arFieldsPlasticFace as $arField) {
    $arPlasticFace[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalPlasticFace = implode("\n", $arPlasticFace);

/* end Лицо */

/* Аппаратные методики */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyMethod,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyMethod[] = $ob;
}

foreach ($arFieldsCosmetologyMethod as $arField) {
    $arCosmetologyMethod[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyMethod = implode("\n", $arCosmetologyMethod);

/* end Аппаратные методики */

/* Уходовые процедуры */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyUhod,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyUhod[] = $ob;
}

foreach ($arFieldsCosmetologyUhod as $arField) {
    $arCosmetologyUhod[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyUhod = implode("\n", $arCosmetologyUhod);

/* end Уходовые процедуры */

/* Лазерный уход */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyLazer,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyLazer[] = $ob;
}

foreach ($arFieldsCosmetologyLazer as $arField) {
    $arCosmetologyLazer[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyLazer = implode("\n", $arCosmetologyLazer);

/* end Лазерный уход */

/* Иньекции */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyInjection,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyInjection[] = $ob;
}

foreach ($arFieldsCosmetologyInjection as $arField) {
    $arCosmetologyInjection[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyInjection = implode("\n", $arCosmetologyInjection);

/* end Иньекции */

/* Трихология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyTrihology,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyTrihology[] = $ob;
}

foreach ($arFieldsCosmetologyTrihology as $arField) {
    $arCosmetologyTrihology[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyTrihology = implode("\n", $arCosmetologyTrihology);

/* end Трихология */

/* Остеопатия и мануальная терапия */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyManual,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyManual[] = $ob;
}

foreach ($arFieldsCosmetologyManual as $arField) {
    $arCosmetologyManual[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyManual = implode("\n", $arCosmetologyManual);

/* end Остеопатия и мануальная терапия */

/* подготовка к операции и реабилитация */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyReabilitation,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyReabilitation[] = $ob;
}

foreach ($arFieldsCosmetologyReabilitation as $arField) {
    $arCosmetologyReabilitation[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyReabilitation = implode("\n", $arCosmetologyReabilitation);

/* end подготовка к операции и реабилитация */

/* Лабораторная диагностика */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'ID' => $idServiceCosmetologyLab,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyLab[] = $ob;
}

foreach ($arFieldsCosmetologyLab as $arField) {
    $arCosmetologyLab[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyLab = implode("\n", $arCosmetologyLab);

/* end Лабораторная диагностика */

/* Терапия */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceCosmetologyTherapy,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsCosmetologyTherapy[] = $ob;
}

foreach ($arFieldsCosmetologyTherapy as $arField) {
    $arCosmetologyTherapy[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalCosmetologyTherapy = implode("\n", $arCosmetologyTherapy);

/* end Терапия */

/* Аппаратные процедуры Гинекология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceAppGynecology,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsAppGynecology[] = $ob;
}

foreach ($arFieldsAppGynecology as $arField) {
    $arAppGynecology[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalAppGynecology = implode("\n", $arAppGynecology);

/* end Аппаратные процедуры Гинекология */

/* Инъекции Гинекология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceGynecologyInjector,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsGynecologyInjector[] = $ob;
}

foreach ($arFieldsGynecologyInjector as $arField) {
    $arGynecologyInjector[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalGynecologyInjector = implode("\n", $arGynecologyInjector);

/* end Инъекции Гинекология */

/* Лечение Гинекология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceGynecologyLech,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsGynecologyLech[] = $ob;
}

foreach ($arFieldsGynecologyLech as $arField) {
    $arGynecologyLech[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalGynecologyLech = implode("\n", $arGynecologyLech);

/* end Лечение Гинекология */

/* Уходовые процедуры Гинекология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceGynecologyUhod,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsGynecologyUhod[] = $ob;
}

foreach ($arFieldsGynecologyUhod as $arField) {
    $arGynecologyUhod[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

$resultTotalGynecologyUhod = implode("\n", $arGynecologyUhod);

/* Уходовые процедуры Гинекология end */

/* Ортодонтия Стоматология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceDentistryOrtodontiya,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsDentistryOrtodontiya[] = $ob;
}

foreach ($arFieldsDentistryOrtodontiya as $arField) {
    $arDentistryOrtodontiya[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalDentistryOrtodontiya = implode("\n", $arDentistryOrtodontiya);

/* end Ортодонтия Стоматология */

/* Ортопедия Стоматология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceDentistryOrtopediya,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsDentistryOrtopediya[] = $ob;
}

foreach ($arFieldsDentistryOrtopediya as $arField) {
    $arDentistryOrtopediya[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalDentistryOrtopediya = implode("\n", $arDentistryOrtopediya);

/* end Ортопедия Стоматология */

/* Парадонтология Стоматология */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceDentistryParodontologiya,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsDentistryParadontologiya[] = $ob;
}

foreach ($arFieldsDentistryParadontologiya as $arField) {
    $arDentistryParadontologiya[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

// Преобразуем массив $arPlasticGrud в строку, чтобы можно было легко вставить в нужное место
$resultTotalDentistryParodontologiya = implode("\n", $arDentistryParadontologiya);

/* end Парадонтология Стоматология */

/* Получение элементов детской стоматологии */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceDentistryDetskaya,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsDentistryDetskaya[] = $ob;
}

foreach ($arFieldsDentistryDetskaya as $arField) {
    $arDentistryDetskaya[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

$resultTotalDentistryDetskaya = implode("\n", $arDentistryDetskaya); // Преобразуем массив в строку

/* Получение элементов терапии */
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceDentistryTerapiya,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsDentistryTerapiya[] = $ob;
}

foreach ($arFieldsDentistryTerapiya as $arField) {
    $arDentistryTerapiya[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}

$resultTotalDentistryTerapiya = implode("\n", $arDentistryTerapiya); // Преобразуем массив в строку

/* Получение элементов хирургии и имплантации */
$arSelect = ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL'];
$arFilter = [
    "IBLOCK_ID" => 47,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'SECTION_ID' => $idServiceDentistryChirurgiya,
    '!=PROPERTY_655_VALUE' => 'Y',
];
$res = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);

while ($ob = $res->GetNext()) {
    $arFieldsChirurgiya[] = $ob;
}

foreach ($arFieldsChirurgiya as $arField) {
    $arChirurgiya[] = '<li class="header-navigation__item header-navigation__item--sub header-navigation__item--sub-2"><a href="' . $arField['DETAIL_PAGE_URL'] . '">' . $arField['NAME'] . '</a></li>';
}
$resultTotalDentistryChirurgiya = implode("\n", $arChirurgiya); // Преобразуем массив в строку

// Теперь добавим в меню:
?>
<nav class="header-navigation__menu header-navigation__menu--services">
    <ul class="header-navigation__list">

        <?php foreach ($arResult as $key => $arItem) { ?>
            <?php
            if (
                $arItem['TEXT'] == 'Пластическая хирургия' ||
                $arItem['TEXT'] == 'Косметология' ||
                $arItem['TEXT'] == 'Гинекология' ||
                $arItem['TEXT'] == 'Стоматология'
            ) {
                $classes = ["header-navigation__item header-navigation__item--parent"];
            } else {
                $classes = ["header-navigation__item"];
            }
            if (!empty($arItem["PARAMS"]["add_class"])) {
                $classes[] = $arItem["PARAMS"]["add_class"];
            }
            $classes = implode(" ", $classes);
            ?>

            <?php if ($arItem['TEXT'] == 'Пластическая хирургия') { ?>
                <li class="<?php echo $classes ?>">
                    <a href="<?php echo $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-1">
                        <?php if (!empty($resultTotalPlasticGrud)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Грудь</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalPlasticGrud ?></ul>
                            </li><!-- end Грудь -->
                        <?php } ?>
                        <?php if (!empty($resultTotalPlasticFace)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Лицо</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalPlasticFace ?></ul>
                            </li><!-- end Лицо -->
                        <?php } ?>
                        <?php if (!empty($resultTotalPlasticBody)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Тело</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalPlasticBody ?></ul>
                            </li><!-- end Тело -->
                        <?php } ?>
                    </ul>
                </li>

            <?php } elseif ($arItem['TEXT'] == 'Косметология') { ?>
                <li class="<?php echo $classes ?>">
                    <a href="<?php echo $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-1">
                        <?php if (!empty($resultTotalCosmetologyMethod)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Аппаратные методики</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyMethod ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyUhod)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Уходовые процедуры</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyUhod ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyLazer)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Лазерные методики</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyLazer ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyInjection)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Инъекционные методики</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyInjection ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyTrihology)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Трихология</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyTrihology ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyManual)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Остеопатия и мануальная терапия</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyManual ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyReabilitation)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Подготовка к операции и реабилитация</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyReabilitation ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalCosmetologyLab)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Лабораторная диагностика</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalCosmetologyLab ?></ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } elseif ($arItem['TEXT'] == 'Гинекология') { ?>
                <li class="<?php echo $classes ?>">
                    <a href="<?php echo $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-1">
                        <?php if (!empty($resultTotalAppGynecology)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Аппаратные процедуры</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalAppGynecology ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalGynecologyInjector)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Инъекционные процедуры</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalGynecologyInjector ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalGynecologyLech)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Лечение</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalGynecologyLech ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalGynecologyUhod)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Уходовые процедуры</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalGynecologyUhod ?></ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } elseif ($arItem['TEXT'] == 'Стоматология') { ?>
                <li class="<?php echo $classes ?>">
                    <a href="<?php echo $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-1">
                        <?php if (!empty($resultTotalDentistryOrtodontiya)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Ортодонтия</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalDentistryOrtodontiya ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalDentistryOrtopediya)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Ортопедия</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalDentistryOrtopediya ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalDentistryParodontologiya)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Парадонтология</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalDentistryParodontologiya ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalDentistryDetskaya)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Детская стоматология</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalDentistryDetskaya ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalDentistryTerapiya)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Терапия</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalDentistryTerapiya ?></ul>
                            </li>
                        <?php } ?>
                        <?php if (!empty($resultTotalDentistryChirurgiya)) { ?>
                            <li class="header-navigation__item header-navigation__item--parent header-navigation__item--sub header-navigation__item--sub-1">
                                <span>Хирургия</span>
                                <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-2"><?php echo $resultTotalDentistryChirurgiya ?></ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } else { ?>
                <li class="<?= $classes ?>">
                    <a href="<?= htmlspecialchars($arItem["LINK"]) ?>"><?= htmlspecialchars($arItem["TEXT"]) ?></a>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>
