<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// Получаем текущий URI
$current_uri = $_SERVER['REQUEST_URI'];
$is_cosmetology_page = false;
// Проверяем, что текущий URI соответствует заданному адресу
if ($current_uri === '/cosmetology/') {
$is_cosmetology_page = true;
}
?>

<nav class="header-services header__services">
    <ul class="header-services__list">
        <li class="header-services__item header-services__item--parent"><span>Направления</span>
            <ul class="header-services__list header-services__list--sub header-services__list--sub-1">
            </ul>
        </li>
        <? /* <?php foreach($arResult as $arItem): ?>
            <?php
            $classes = ["header-services__item"];
            $has_sublist = false; // Инициализируем флаг наличия подсписка

            if (!empty($arItem["PARAMS"]["add_class"])) {
                $classes[] = $arItem["PARAMS"]["add_class"];
            }

            // Проверяем текст элемента и добавляем классы при необходимости
            if ($arItem["TEXT"] === "Косметология" ||
                $arItem["TEXT"] === "Пластическая хирургия" ||
                $arItem["TEXT"] === "Гинекология") {
                $classes[] = "header-services__item--active";
                $classes[] = "header-services__item--parent";
                $has_sublist = true; // Устанавливаем флаг наличия подсписка
            }
            ?>
            <li class="<?= implode(" ", $classes) ?>">
                <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                <?php if ($has_sublist): ?>
                    <ul class="header-services__list header-services__list--sub header-services__list--sub-1">
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?> */?>
    </ul>
</nav>
