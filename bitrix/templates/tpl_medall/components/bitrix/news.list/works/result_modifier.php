<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arFieldsDirectionSection = [];
$arIdServiceSectionElements = [];
$arIdServiceElements = [];
$doctorData = [];
$serviceData = [];

$arrException = ['Флебология', 'Гинекология', 'Пересадка волос', 'Рассрочка'];
$arFieldsSelectSection = ['NAME', 'ID', 'CODE'];

// Helper function to get sections
function getSections($parentSectionID, $arrException, $arFieldsSelectSection)
{
    $arFields = [];
    $rsGeneralPageSection = CIBlockSection::GetByID($parentSectionID);
    if ($arParentSection = $rsGeneralPageSection->GetNext()) {
        $arFilterSection = [
            'IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
            '!=NAME' => $arrException,
            '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
            '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
            '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
        ];
        $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilterSection, false, $arFieldsSelectSection);
        while ($arSect = $rsSect->GetNext()) {
            $arFields[] = $arSect;
        }
    }
    return $arFields;
}

// Get Direction Sections
$arFieldsDirectionSection = getSections(433, $arrException, $arFieldsSelectSection);

// Get Service Section Elements
$sectionIds = [160, 159];
foreach ($sectionIds as $sectionId) {
    $arIdServiceSectionElements = array_merge($arIdServiceSectionElements, array_column(getSections($sectionId, $arrException, $arFieldsSelectSection), 'ID'));
}

// Get Service Elements
$arFilter = ['IBLOCK_ID' => 47, 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $arIdServiceSectionElements];
$arSelect = ["ID", "NAME", 'DETAIL_PAGE_URL', 'CODE'];
$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
while ($ob = $res->GetNext()) {
    $serviceData[$ob['ID']] = [
        'NAME' => $ob['NAME'],
        'URL' => $ob['DETAIL_PAGE_URL'],
        'CODE' => $ob['CODE'],
    ];
    $arIdServiceElements[] = $ob['ID'];
}

// Get Doctor Elements
$arFilterDoctor = [
    'IBLOCK_ID' => 6,
    'ACTIVE' => 'Y',
    [
        'LOGIC' => 'OR',
        ['PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE' => $arIdServiceElements],
        ['PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE' => $arIdServiceElements],
    ]
];
$arSelectDoctor = [
    "ID", "NAME", 'PREVIEW_TEXT', 'DETAIL_PAGE_URL', 'PROPERTY_IO_DOCTOR'
];
$res = CIBlockElement::GetList([], $arFilterDoctor, false, false, $arSelectDoctor);
while ($obDoctor = $res->GetNext()) {
    $doctorData[$obDoctor['ID']] = [
        'URL' => $obDoctor['DETAIL_PAGE_URL'],
        'IO' => $obDoctor['PROPERTY_IO_DOCTOR_VALUE'],
        'NAME' => $obDoctor['NAME'],
        'TEXT' => $obDoctor['PREVIEW_TEXT'],
    ];
}

// Add the prepared data to the result array
$arResult['FIELDS_DIRECTION_SECTION'] = $arFieldsDirectionSection;
$arResult['SERVICE_DATA'] = $serviceData;
$arResult['DOCTOR_DATA'] = $doctorData;
