<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// Functions
function generateWebPImage($jpegData) {
    $pictures = '';
    $webpWidth = ''; // Ширина изображения
    $webpHeight = '';
    $pathWebp = str_ireplace([".jpg", ".jpeg", ".png"], ".webp", strtolower($jpegData));
    $img = '';
    if (!file_exists($_SERVER["DOCUMENT_ROOT"] . $pathWebp)) {
        // Load image
        if (stripos($jpegData, '.png') !== false) {
            $img = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        } elseif (stripos($jpegData, '.jpg') !== false || stripos($jpegData, '.jpeg') !== false) {
            $img = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        }

        // Resize image
        $width = imagesx($img);
        $height = imagesy($img);
        $newWidth = 800;
        $newHeight = ($height / $width) * $newWidth;

        $resizedImg = imagecreatetruecolor($newWidth, $newHeight);
        imagealphablending($resizedImg, false);
        imagesavealpha($resizedImg, true);
        imagecopyresampled($resizedImg, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        // Save WebP image
        imagewebp($resizedImg, $_SERVER["DOCUMENT_ROOT"] . $pathWebp, 100);
        imagedestroy($img);
        imagedestroy($resizedImg);
    }
// Проверяем существование файла WebP
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . $pathWebp)) {
        // Получаем размеры WebP изображения
        $imageInfo = getimagesize($_SERVER["DOCUMENT_ROOT"] . $pathWebp);
        if ($imageInfo !== false) {
            $webpWidth = $imageInfo[0]; // Ширина изображения
            $webpHeight = $imageInfo[1]; // Высота изображения


            // Записываем ширину и высоту в файл лога
        }

    }


    return '<img src="' . $pathWebp . '" alt="" loading="lazy" decoding="async" width="'.$webpWidth.'" height="'.$webpHeight.'" class="icv__img icv__img-a">';
}

function generateWebPImageWithControl($jpegData) {
    $webpWidth = ''; // Ширина изображения
    $webpHeight = '';
    $pathWebp = str_ireplace([".jpg", ".jpeg", ".png"], ".webp", strtolower($jpegData));
    $img = '';
    if (!file_exists($_SERVER["DOCUMENT_ROOT"] . $pathWebp)) {
        // Load image
        if (stripos($jpegData, '.png') !== false) {
            $img = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        } elseif (stripos($jpegData, '.jpg') !== false || stripos($jpegData, '.jpeg') !== false) {
            $img = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        }

        // Resize image
        $width = imagesx($img);
        $height = imagesy($img);
        $newWidth = 800;
        $newHeight = ($height / $width) * $newWidth;

        $resizedImg = imagecreatetruecolor($newWidth, $newHeight);
        imagealphablending($resizedImg, false);
        imagesavealpha($resizedImg, true);
        imagecopyresampled($resizedImg, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        // Save WebP image
        imagewebp($resizedImg, $_SERVER["DOCUMENT_ROOT"] . $pathWebp, 100);
        imagedestroy($img);
        imagedestroy($resizedImg);
    }
// Проверяем существование файла WebP
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . $pathWebp)) {
        // Получаем размеры WebP изображения
        $imageInfo = getimagesize($_SERVER["DOCUMENT_ROOT"] . $pathWebp);
        if ($imageInfo !== false) {
            $webpWidth = $imageInfo[0]; // Ширина изображения
            $webpHeight = $imageInfo[1]; // Высота изображения


            // Записываем ширину и высоту в файл лога
        }

    }
    return '<div class="icv__wrapper">
                <img src="' . $pathWebp . '" alt="" loading="lazy" decoding="async" width="'.$webpWidth.'" height="'.$webpHeight.'" class="icv__img icv__img-b">
            </div>
            <div class="icv__control">
                <div class="icv__control-line"></div>
                <div class="icv__theme-wrapper">
                    <div class="icv__arrow-wrapper"></div>
                    <div class="icv__arrow-wrapper"></div>
                </div>
                <div class="icv__control-line"></div>
            </div>';
}

function getDoctorLink($doctorId, $doctorData) {
    if (isset($doctorData[$doctorId])) {
        return '<div class="works-item__doctor">
                    ' . $doctorData[$doctorId]['TEXT'] . '
                    <a href="' . $doctorData[$doctorId]['URL'] . '">' . $doctorData[$doctorId]['NAME'] . ' ' . $doctorData[$doctorId]['IO'] . '</a>
                </div>';
    }
    return '';
}

function getDoctorData($doctorId, $doctorData) {
    if (isset($doctorData[$doctorId])) {
        return trim($doctorData[$doctorId]['NAME']) . ' ' . trim($doctorData[$doctorId]['IO']);
    }
    return '';
}

function getServiceData($serviceId, $serviceData) {
    if (isset($serviceData[$serviceId])) {
        return trim($serviceData[$serviceId]['NAME']);
    }
    return '';
}

function getService($serviceId, $serviceData, $serviceName,$serviceLink) {
    if (!empty($serviceId) && empty($serviceName) && empty($serviceLink)) {
        return '<a class="works-item__service" href="' . $serviceData[$serviceId]['URL'] . '">' . $serviceData[$serviceId]['NAME'] . '</a>';
    }
    if (!empty($serviceId) && !empty($serviceName) && empty($serviceLink)) {
        return '<a class="works-item__service" href="' . $serviceData[$serviceId]['URL'] . '">' . $serviceName . '</a>';
    }
    if (!empty($serviceId) && empty($serviceName) && !empty($serviceLink)) {
        return '<a class="works-item__service" href="' . $serviceLink . '">' . $serviceData[$serviceId]['NAME'] . '</a>';
    }
    if (!empty($serviceId) && !empty($serviceName) && !empty($serviceLink)) {
        return '<a class="works-item__service" href="' . $serviceLink . '">' . $serviceName . '</a>';
    }
    return '';
}

// Use prepared data
$fieldsDirectionSection = $arResult['FIELDS_DIRECTION_SECTION'];
$serviceData = $arResult['SERVICE_DATA'];
$doctorData = $arResult['DOCTOR_DATA'];

// Your HTML and PHP logic here
?>



<section class="container page-section">
    <div class="content">
        <h1><?php echo $APPLICATION->GetTitle('title')?></h1>
    </div>
</section>

<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="works-filter page-subsection">
            <div class="works-filter__inner">
                <div class="form-input form-input--select form-input--secondary works-filter__select works-filter__select--directions">
                    <select class="form-input__field" name="direction">
                        <option value=""></option>
                        <?/* <?php foreach ($fieldsDirectionSection as $direction) { ?>
                            <?php if (!empty($direction['NAME'])) { ?>
                                <option value="<?php echo $direction['NAME']; ?>">
                                    <?php echo $direction['NAME']; ?>
                                </option>
                            <?php } ?>
                        <?php } ?> */?>
                    </select>
                    <span class="label form-input__label">Выберите направление</span>
                </div>
                <div class="form-input form-input--select form-input--secondary works-filter__select works-filter__select--services works-filter__select works-filter__select--services">
                    <select class="form-input__field" name="service">
                      <option value=""></option>
                    </select>
                        <span class="label form-input__label">Выберите услугу
                        </span>
                  </div>
                  <div class="form-input form-input--select form-input--secondary works-filter__select works-filter__select--doctors works-filter__select works-filter__select--doctors">
                    <select class="form-input__field" name="doctor">
                      <option value=""></option>
                    </select>
                        <span class="label form-input__label">Выберите специалиста
                        </span>
                  </div>

            </div>
        </div>
        <div class="grid page-subsection works-item-list">
            <?php foreach ($arResult["ITEMS"] as $arItem) { ?>


                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <?php if (!empty($arItem['NAME']) || !empty($arItem['DISPLAY_PROPERTIES']['SERVICE_NAME']['~VALUE'])) { ?>
                    <div class="works-item grid__cell grid__cell--m--3 grid__cell--s--6" id="<?php echo $this->GetEditAreaId($arItem['ID']); ?>"
                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['SERVICE_DIRECTION_NAME']['~VALUE'])) { ?>
                            data-direction="<?php echo $arItem['DISPLAY_PROPERTIES']['SERVICE_DIRECTION_NAME']['~VALUE']; ?>"
                        <?php } ?>

                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'])) { ?>
                            data-service="<?php echo getServiceData($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'], $serviceData); ?>"
                        <?php } ?>

                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['DOCTOR']['~VALUE'])) { ?>
                         data-doctor="<?php echo getDoctorData($arItem['DISPLAY_PROPERTIES']['DOCTOR']['~VALUE'], $doctorData); ?>"
            <?php } ?>>

            <div class="image-compare icv icv__icv--horizontal standard">
                            <?php echo generateWebPImage($arItem['DISPLAY_PROPERTIES']['PHOTO_BEFORE']['FILE_VALUE']['SRC']); ?>
                            <?php echo generateWebPImageWithControl($arItem['DISPLAY_PROPERTIES']['PHOTO_AFTER']['FILE_VALUE']['SRC']); ?>
                        </div>
                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'])) { ?>
                            <?php echo getService($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'], $serviceData,$arItem['DISPLAY_PROPERTIES']['SERVICE_NAME']['~VALUE'],$arItem['DISPLAY_PROPERTIES']['SERVICE_LINK']['~VALUE']); ?>
                        <?php } ?>

                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['DOCTOR']['VALUE'])) { ?>
                            <?php echo getDoctorLink($arItem['DISPLAY_PROPERTIES']['DOCTOR']['VALUE'], $doctorData); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

        <div class="grid grid--justify--center page-subsection"><a class="button works-item-load-button" href="#">Показать еще   </a>
          </div>
    </div>
</section>
<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="grid page-subsection"></div>
    </div>
</section>
