<?php
namespace Footer;

class Slider
{
	public $title;
	public $videoThumb;
	public $videoURL;
	public $slides;

	public function __construct()
	{
		$res = \CIBlockElement::GetList([], ["IBLOCK_ID" => 64, "CODE" => "world_of_medall"]);
		if($bxElement = $res->GetNextElement()){
			$this->init($bxElement->GetFields(), $bxElement->GetProperties());
		}
	}

	private function init($fields, $props)
	{
		$this->title = $fields["NAME"];
		$this->videoThumb = getIbElementImageSrc($props["VIDEO_THUMB"]["VALUE"]);
		$this->videoURL = $props["VIDEO_URL"]["VALUE"];
		$this->slides = array_map(function ($pictureId){
			return new Slide($pictureId);
		}, $props["SLIDES"]["VALUE"]);
	}

}