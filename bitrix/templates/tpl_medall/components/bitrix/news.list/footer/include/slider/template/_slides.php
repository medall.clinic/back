<? /** @var $slider Footer\Slider */ ?>
<? /** @var $slide Footer\Slide  */ ?>
<div
	class="slide swiper-slide footer-gallery__slide footer-gallery__slide--video"
	id="footer-gallery-slide-video"
>
	<div class="iframe-responsive">
		<div class="iframe-responsive__iframe">
			<div data-video-id="<?= $slider->videoURL ?>" id="footer-gallery-video"></div>
		</div>
	</div>
</div>

<? foreach ($slider->slides as $k => $slide): ?>
	<div class="slide swiper-slide footer-gallery__slide" id="footer-gallery-slide-<?= $k ?>">
		<picture>
			<source
                srcset="<?= WebPHelper::getOrCreateWebPUrl($slide->srcFull, [ 1024 ]) ?>"
                type="image/webp"
            />
			<img
                class="footer-gallery__slide-contents"
                src="<?= $slide->srcFull ?>"
                loading="lazy"
                decoding="async"
                width="<?= DimensionHelper::getWidth($slide->srcFull) ?>"
                height="<?= DimensionHelper::getHeight($slide->srcFull) ?>"
                alt=""
            />
		</picture>
	</div>
<? endforeach ?>
