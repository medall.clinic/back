<section class="page-section footer-map footer__map">
	<div class="footer-map__body" id="map"></div>
	<? /* <script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script> */?>
	<script>
      let isYandexMapShowed = false;
      document.addEventListener("scroll", function () {
        if(!isYandexMapShowed){
          ymaps.ready(init);
          isYandexMapShowed = true;
        }
      });

      function init () {
        const windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        let iconSize, iconOffset;

        if(windowWidth > 780) {
          iconSize = [162, 230];
          iconOffset = [-81, -143.75];
        }

        if(windowWidth <= 780) {
          iconSize = [90, 128];
          iconOffset = [-45, -80];
        }

        if(windowWidth <= 480) {
          iconSize = [60, 80];
          iconOffset = [-30, -50];
        }

        const myMap = new ymaps.Map('map', {
          center: [59.966217579684,30.29],
          controls: ['zoomControl'],
          zoom: 12
        }, {
          searchControlProvider: 'yandex#search'
        });

        myMap.behaviors.disable('scrollZoom');

        const balloon = 'Левашовский пр., д.24, 500 м от ст. Чкаловская'

        //Добавление меток на карту
        myMap.geoObjects.add(
          new ymaps.Placemark(
            [59.966217579684, 30.285785228836],
            {
              balloonContent: balloon,
              iconCaption: balloon,
            },
            {
              iconLayout: 'default#image',
              iconImageHref: '/bitrix/templates/tpl_medall/images/common.svg#pin',
              // iconImageHref: '/bitrix/templates/tpl_medall/images/pin.svg', // Своё изображение иконки метки
              iconImageSize: iconSize, // Размеры метки
              iconImageOffset: iconOffset // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
            }
          )
        );

      }
	</script>
</section>
