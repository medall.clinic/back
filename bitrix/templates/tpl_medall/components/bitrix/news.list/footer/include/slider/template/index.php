<? /** @var $slider Footer\Slider */ ?>
<?php if ($APPLICATION->GetCurPage(false) !== '/hr/') {  ?>
    <section class="container page-section page-section--background-color page-section--background-color--dark  footer-gallery">

        <div class="content footer-gallery__grid">
            <? include __DIR__."/_thumbs.php" ?>
        </div>

        <div class="slider footer-gallery__slider">
            <? include __DIR__."/_slider.php" ?>
        </div>

    </section>
<?php } else { ?>
    <section class="container page-section page-section--background-color page-section--background-color--dark  footer-gallery--hr footer-gallery">

        <div class="content footer-gallery__grid">
            <? include __DIR__."/_thumbs.php" ?>
        </div>

        <div class="slider footer-gallery__slider">
            <? include __DIR__."/_slider.php" ?>
        </div>

    </section>

<?php } ?>

