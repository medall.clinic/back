<? /** @var $slider Footer\Slider */ ?>
<? /** @var $slide Footer\Slide  */ ?>
<a
	class="footer-gallery__video"
	href="#footer-gallery-slide-video"
>
	<picture>
		<source srcset="<?= WebPHelper::getOrCreateWebPUrl($slider->videoThumb) ?>" type="image/webp"/>
		<img class="footer-gallery__image" src="<?= $slider->videoThumb ?>" loading="lazy" decoding="async" alt=""/>
	</picture>
</a>

<? foreach ($slider->slides as $k => $slide): ?>
	<a class="footer-gallery__item" href="#footer-gallery-slide-<?= $k ?>">
		<picture>
			<source
                srcset="<?= WebPHelper::getOrCreateWebPUrl($slide->srcThumb, [248, 496]) ?>"
                type="image/webp"
            />
			<img
                class="footer-gallery__image"
                src="<?= $slide->srcThumb ?>"
                loading="lazy"
                decoding="async"
                alt=""
            />
		</picture>
	</a>
<? endforeach ?>