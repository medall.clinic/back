<? /** @var $slider Footer\Slider */ ?>
<div class="slider__slides swiper">

	<div class="slider__wrapper swiper-wrapper">
		<? include __DIR__."/_slides.php" ?>
	</div>

	<div class="slider__arrow slider__arrow--prev"></div>
	<div class="slider__arrow slider__arrow--next"></div>
	<div class="slider__pagination"></div>

</div>

<button class="footer-gallery__slider-close" type="button"></button>

<h2 class="footer-gallery__slider-title"><?= $slider->title ?></h2>