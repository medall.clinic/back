<?php
namespace Footer;

class Slide
{
	public $srcFull;
	public $srcThumb;

	public function __construct($pictureId)
	{
		$this->srcFull = getIbElementImageSrc($pictureId);
		$this->srcThumb = str_replace('.', '-thumb.', $this->srcFull);

		if(!file_exists($_SERVER["DOCUMENT_ROOT"].$this->srcThumb)){
			copy($_SERVER["DOCUMENT_ROOT"].$this->srcFull, $_SERVER["DOCUMENT_ROOT"].$this->srcThumb);
		}
	}

}