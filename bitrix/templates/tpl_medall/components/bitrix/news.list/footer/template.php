<? foreach($arResult["ITEMS"] as $block): ?>

    <? if(file_exists(__DIR__.'/include/'.$block["CODE"].'.php')): ?>
        <? include __DIR__.'/include/'.$block["CODE"].'.php' ?>
    <? else: ?>
		<?= $block["~PREVIEW_TEXT"] ?>
	<? endif ?>

<? endforeach ?>