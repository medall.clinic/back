<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arResult2=array();
foreach($arResult["ITEMS"] as $arItem){
    $arResult2[$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM_ID']]['NAME']=$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM'];
    $arResult2[$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM_ID']]['ID']=$arItem['PROPERTIES']['direction_doctor']['VALUE_XML_ID'];
    $arResult2[$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM_ID']]['ITEMS'][]=$arItem;

    //echo '<pre>'; print_r($arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM']); echo '</pre>';

}
?>
    <section class="container page-section page-section--margin--small doctors-layout">
        <div class="content">
            <div class="form-input form-input--select form-input--secondary page-subsection doctors-layout__select page-subsection doctors-layout__select">
                <select class="form-input__field">
                    <option value="all">Все направления</option>
                    <? foreach($arResult2 as $arItem2){ ?>
                        <option value="<?=$arItem2['ID']?>"><?=$arItem2['NAME']?></option>
                    <? } ?>
                </select>
                <span class="label form-input__label">Все направления</span>
            </div>

            <div class="page-subsection doctors-layout__category" data-category="plastic">
                <h2 class="doctors-layout__title">Пластическая хирургия</h2>
                <div class="grid doctors-layout__list">

                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>

                        <? foreach ($arItem['PROPERTIES']['direction_doctors']['VALUE_XML_ID'] as $item) {

                            if (!empty($item) && $item == 'plastics'){

                                $this->AddEditAction(
                                    $arItem['ID'],
                                    $arItem['EDIT_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
                                );

                                $this->AddDeleteAction(
                                    $arItem['ID'],
                                    $arItem['DELETE_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                                ); ?>
                                <div class="doctors-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 doctors-layout__item">
                                    <a class="doctors-item__title"
                                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span><?= $arItem["NAME"] ?></span> <?= $arItem['PROPERTIES']['io_doctor']['VALUE'] ?>
                                    </a>
                                    <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <div class="doctors-item__description"> <?= $arItem["PREVIEW_TEXT"]; ?></div>
                                    <? endif; ?>
                                    <a class="doctors-item__image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <picture>
                                            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                                                    type="image/webp"/>
                                            <img class="doctors-item__image" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                 loading="lazy" decoding="async"/>
                                        </picture>
                                    </a>
                                </div>


                            <? } ?>

                        <? } ?>
                    <? } ?>

                </div>
            </div>


            <div class="page-subsection doctors-layout__category" data-category="dentistry">
                <h2 class="doctors-layout__title">Стоматология</h2>
                <div class="grid doctors-layout__list">
                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                        <? foreach ($arItem['PROPERTIES']['direction_doctors']['VALUE_XML_ID'] as $item) {
                            if (!empty($item) && $item == 'dentistry'){

                                $this->AddEditAction(
                                    $arItem['ID'],
                                    $arItem['EDIT_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
                                );

                                $this->AddDeleteAction(
                                    $arItem['ID'],
                                    $arItem['DELETE_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                                ); ?>
                                <div class="doctors-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 doctors-layout__item">
                                    <a class="doctors-item__title"
                                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span><?= $arItem["NAME"] ?></span> <?= $arItem['PROPERTIES']['io_doctor']['VALUE'] ?>
                                    </a>
                                    <?
                                    if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <div class="doctors-item__description"><?= $arItem["PREVIEW_TEXT"]; ?></div>
                                    <?
                                    endif; ?>
                                    <a class="doctors-item__image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <picture>
                                            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                                                    type="image/webp"/>
                                            <img class="doctors-item__image" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                loading="lazy" decoding="async"/>
                                        </picture>
                                    </a>
                                </div>


                            <? } ?>

                        <? } ?>
                    <? } ?>

                </div>
            </div>

            <div class="page-subsection doctors-layout__category" data-category="cosmetology">
                <h2 class="doctors-layout__title">Косметология</h2>
                <div class="grid doctors-layout__list">
                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                        <? foreach ($arItem['PROPERTIES']['direction_doctors']['VALUE_XML_ID'] as $item) {
                            if (!empty($item) &&  $item == 'cosmetology'){
                                $this->AddEditAction(
                                    $arItem['ID'],
                                    $arItem['EDIT_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
                                );

                                $this->AddDeleteAction(
                                    $arItem['ID'],
                                    $arItem['DELETE_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                                ); ?>
                                <div class="doctors-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 doctors-layout__item">
                                    <a class="doctors-item__title"
                                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span><?= $arItem["NAME"] ?></span> <?= $arItem['PROPERTIES']['io_doctor']['VALUE'] ?>
                                    </a>
                                    <?
                                    if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <div class="doctors-item__description"><?= $arItem["PREVIEW_TEXT"]; ?></div>
                                    <?
                                    endif; ?>
                                    <a class="doctors-item__image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <picture>
                                            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                                                    type="image/webp"/>
                                            <img class="doctors-item__image" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                 loading="lazy" decoding="async"/>
                                        </picture>
                                    </a>
                                </div>


                            <? } ?>

                        <? } ?>
                    <? } ?>

                </div>
            </div>

            <div class="page-subsection doctors-layout__category" data-category="hair-transplant">
                <h2 class="doctors-layout__title">Пересадка волос</h2>
                <div class="grid doctors-layout__list">
                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                        <? foreach ($arItem['PROPERTIES']['direction_doctors']['VALUE_XML_ID'] as $item) {
                            if (!empty($item) && $item == 'hair-transplant'){

                                $this->AddEditAction(
                                    $arItem['ID'],
                                    $arItem['EDIT_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
                                );

                                $this->AddDeleteAction(
                                    $arItem['ID'],
                                    $arItem['DELETE_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                                ); ?>
                                <div class="doctors-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 doctors-layout__item">
                                    <a class="doctors-item__title"
                                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span><?= $arItem["NAME"] ?></span> <?= $arItem['PROPERTIES']['io_doctor']['VALUE'] ?>
                                    </a>
                                    <?
                                    if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <div class="doctors-item__description"><?= $arItem["PREVIEW_TEXT"]; ?></div>
                                    <?
                                    endif; ?>
                                    <a class="doctors-item__image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <picture>
                                            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                                                    type="image/webp"/>
                                            <img class="doctors-item__image" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                 loading="lazy" decoding="async"/>
                                        </picture>
                                    </a>
                                </div>


                            <? } ?>

                        <? } ?>
                    <? } ?>

                </div>
            </div>

            <div class="page-subsection doctors-layout__category" data-category="phlebology">
                <h2 class="doctors-layout__title">Флебология</h2>
                <div class="grid doctors-layout__list">
                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                        <? foreach ($arItem['PROPERTIES']['direction_doctors']['VALUE_XML_ID'] as $item) {
                            if (!empty($item) && $item == 'phlebology'){

                                $this->AddEditAction(
                                    $arItem['ID'],
                                    $arItem['EDIT_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
                                );

                                $this->AddDeleteAction(
                                    $arItem['ID'],
                                    $arItem['DELETE_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                                ); ?>
                                <div class="doctors-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 doctors-layout__item">
                                    <a class="doctors-item__title"
                                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span><?= $arItem["NAME"] ?></span> <?= $arItem['PROPERTIES']['io_doctor']['VALUE'] ?>
                                    </a>
                                    <?
                                    if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <div class="doctors-item__description"><?= $arItem["PREVIEW_TEXT"]; ?></div>
                                    <?
                                    endif; ?>
                                    <a class="doctors-item__image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <picture>
                                            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                                                    type="image/webp"/>
                                            <img class="doctors-item__image" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                 loading="lazy" decoding="async"/>
                                        </picture>
                                    </a>
                                </div>


                            <? } ?>

                        <? } ?>
                    <? } ?>

                </div>
            </div>

            <div class="page-subsection doctors-layout__category" data-category="anaesthesia">
                <h2 class="doctors-layout__title">Анестезиологическое отделение</h2>
                <div class="grid doctors-layout__list">
                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                        <? foreach ($arItem['PROPERTIES']['direction_doctors']['VALUE_XML_ID'] as $item) {
                            if (!empty($item) && $item == 'anaesthesia'){

                                $this->AddEditAction(
                                    $arItem['ID'],
                                    $arItem['EDIT_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
                                );

                                $this->AddDeleteAction(
                                    $arItem['ID'],
                                    $arItem['DELETE_LINK'],
                                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                                ); ?>
                                <div class="doctors-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 doctors-layout__item">
                                    <a class="doctors-item__title"
                                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span><?= $arItem["NAME"] ?></span> <?= $arItem['PROPERTIES']['io_doctor']['VALUE'] ?>
                                    </a>
                                    <?
                                    if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <div class="doctors-item__description"><?= $arItem["PREVIEW_TEXT"]; ?></div>
                                    <?
                                    endif; ?>
                                    <a class="doctors-item__image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                        <picture>
                                            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                                                    type="image/webp"/>
                                            <img class="doctors-item__image" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                                 loading="lazy" decoding="async"/>
                                        </picture>
                                    </a>
                                </div>


                            <? } ?>

                        <? } ?>
                    <? } ?>

                </div>
            </div>


        </div>
    </section>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?endif;?>
