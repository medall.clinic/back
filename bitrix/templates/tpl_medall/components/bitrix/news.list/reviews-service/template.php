<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
$dateFormat = "j F Y"; // Формат: день месяц год

?>
<?php if (!empty($arResult['ITEMS'])) {?>
    <section
            class="container page-section page-section--background-color page-section--background-color--light home-feedback">
        <div class="content">
            <h2 class="page-section__title page-subsection text-align-center"><?php echo Loc::getMessage('TITLE_REVIEWS') ?></h2>
            <div class="slider page-subsection home-feedback__slider reviews-item-list">
                <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">
                        <?php foreach ($arResult["ITEMS"] as $key => $arItem) {
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            $formattedDate = FormatDate($dateFormat, MakeTimeStamp($arItem["DATE_ACTIVE_FROM"], CSite::GetDateFormat()));
                            ?>

                            <div class="slide swiper-slide">
                                <div class="reviews-item reviews-item--text"
                                     data-service="<?php echo $arResult['FILTER_NAME_SERVICE'][$key] ?>"
                                     data-doctor="<?php echo $arResult['FILTER_NAME_DOCTOR'][$key] ?>">

                                    <div class="reviews-item__platform">
                                        Отзыв оставлен на сайте
                                        <?php if ($arItem['PROPERTIES']['RATING_LABEL']['VALUE']) { ?>
                                            <a href="<?php echo $arItem['PROPERTIES']['RATING_LABEL']['URL'] ?>"
                                               target="_blank"><?php echo $arItem['PROPERTIES']['RATING_LABEL']['VALUE'] ?></a>
                                        <?php } ?>
                                    </div>
                                    <div class="reviews-item__text">
                                        <?php if (!empty($arItem["PREVIEW_TEXT"])) { ?>
                                            <?php echo $arItem["PREVIEW_TEXT"] ?>
                                        <?php } ?>
                                    </div>
                                    <?php if (!empty($arItem['PROPERTIES']['doctor_review']['VALUE'])) {
                                        // Проверяем, есть ли хотя бы один доктор с непустым именем
                                        $hasDoctor = false;
                                        foreach ($arItem['DOCTORS'] as $propKey => $property) {
                                            if (in_array($property['ID'], $arItem['PROPERTIES']['doctor_review']['VALUE']) && !empty($property['NAME'])) {
                                                $hasDoctor = true;
                                                break; // Можно выйти из цикла, если хоть один доктор найден
                                            }
                                        }

                                        if ($hasDoctor) { // Если есть доктор с ненулевым именем, выводим блок
                                            ?>
                                            <div class="reviews-item__doctor">Отзыв о:
                                                <?php foreach ($arItem['DOCTORS'] as $propKey => $property) { ?>
                                                    <?php if (in_array($property['ID'], $arItem['PROPERTIES']['doctor_review']['VALUE']) && !empty($property['NAME'])) { ?>
                                                        <a class="reviews-item__doctor-link"
                                                           href="<?php echo $property['DETAIL_PAGE_URL'] ?>">
                                                            <?php echo $property['NAME'] . ' ' . $property['~PROPERTY_IO_DOCTOR_VALUE'] ?>
                                                        </a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <?php
                                        }
                                    } ?>
                                    <?php if (!empty($arItem['PROPERTIES']['SERVICE']['VALUE'])) { ?>

                                        <div class="reviews-item__service">
                                            Услуга:
                                            <?php foreach ($arItem['SERVICE'] as $propKey => $property) { ?>
                                                <?php if (in_array($property['ID'], $arItem['PROPERTIES']['SERVICE']['VALUE'])) { ?>
                                                    <a class="reviews-item__service-link"
                                                       href="<?php echo $property['DETAIL_PAGE_URL'] ?>"><?php echo $property['NAME'] ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                    <div class="slider__pagination"></div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>





