<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
$dateFormat = "j F Y"; // Формат: день месяц год

?>
<?php if ($arParams["DISPLAY_TOP_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?><br/>
<?php endif; ?>
<? /* <?php $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "template1",
    array(
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "PATH" => "/",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "template1"
    ),
    false
); ?> */?>



<section
        class="container page-section page-section--background-color page-section--background-color--light home-feedback">
    <div class="content">
        <h2 class="page-section__title page-subsection text-align-center"><?php echo Loc::getMessage('TITLE_REVIEWS') ?></h2>
        <div class="slider page-subsection home-feedback__slider reviews-item-list">
            <div class="slider__slides swiper">
                <div class="slider__wrapper swiper-wrapper">
                    <?php foreach ($arResult["ITEMS"] as $key => $arItem) {
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        $formattedDate = FormatDate($dateFormat, MakeTimeStamp($arItem["DATE_ACTIVE_FROM"], CSite::GetDateFormat()));
                        ?>

                        <div class="slide swiper-slide">
                            <div class="reviews-item reviews-item--text"
                                data-service="<?php echo $arResult['FILTER_NAME_SERVICE'][$key] ?>"
                                data-doctor="<?php echo $arResult['FILTER_NAME_DOCTOR'][$key] ?>">

                                <div class="reviews-item__platform">
                                    Отзыв оставлен на сайте
                                    <?php if ($arItem['PROPERTIES']['RATING_LABEL']['VALUE']) { ?>
                                    <a href="<?php echo $arItem['PROPERTIES']['RATING_LABEL']['URL'] ?>"
                                        target="_blank"><?php echo $arItem['PROPERTIES']['RATING_LABEL']['VALUE'] ?></a>
                                    <?php } ?>
                                </div>
                                <div class="reviews-item__text">
                                    <?php if (!empty($arItem["PREVIEW_TEXT"])) { ?>
                                        <?php echo $arItem["PREVIEW_TEXT"] ?>
                                    <?php } ?>
                                </div>
                                <?php if (!empty($arItem['PROPERTIES']['doctor_review']['VALUE'])) {
                                    // Проверяем, есть ли хотя бы один доктор с непустым именем
                                    $hasDoctor = false;
                                    foreach ($arItem['DOCTORS'] as $propKey => $property) {
                                        if (in_array($property['ID'], $arItem['PROPERTIES']['doctor_review']['VALUE']) && !empty($property['NAME'])) {
                                            $hasDoctor = true;
                                            break; // Можно выйти из цикла, если хоть один доктор найден
                                        }
                                    }

                                    if ($hasDoctor) { // Если есть доктор с ненулевым именем, выводим блок
                                        ?>
                                        <div class="reviews-item__doctor">Отзыв о:
                                            <?php foreach ($arItem['DOCTORS'] as $propKey => $property) { ?>
                                                <?php if (in_array($property['ID'], $arItem['PROPERTIES']['doctor_review']['VALUE']) && !empty($property['NAME'])) { ?>
                                                    <a class="reviews-item__doctor-link"
                                                    href="<?php echo $property['DETAIL_PAGE_URL'] ?>">
                                                        <?php echo $property['NAME'] . ' ' . $property['~PROPERTY_IO_DOCTOR_VALUE'] ?>
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <?php
                                    }
                                } ?>
                                <?php if (!empty($arItem['PROPERTIES']['SERVICE']['VALUE'])) { ?>

                                    <div class="reviews-item__service">
                                        Услуга:
                                        <?php foreach ($arItem['SERVICE'] as $propKey => $property) { ?>
                                            <?php if (in_array($property['ID'], $arItem['PROPERTIES']['SERVICE']['VALUE'])) { ?>
                                                <a class="reviews-item__service-link"
                                                href="<?php echo $property['DETAIL_PAGE_URL'] ?>"><?php echo $property['NAME'] ?></a>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="slider__arrow slider__arrow--prev"></div>
                <div class="slider__arrow slider__arrow--next"></div>
                <div class="slider__pagination"></div>
            </div>
        </div>
    </div>
</section>

<? /* <section class="container page-section" id="review-form">
    <div class="content">
        <div class="page-subsection">
            <h2 class="text-align-center">Оставить отзыв
            </h2>
        </div>
        <div class="grid grid--justify--center page-subsection">
            <form class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form form-ajax"
                  data-is-validate="true" data-is-answer-in-popup="true"
                  data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации">
                <label class="form-input form-input--text form__input">
                    <input class="form-input__field" type="text" name="name" data-validation-required>
                    <span class="label form-input__label">ФИО
                        </span>
                </label>
                <label class="form-input form-input--tel form__input">
                    <input class="form-input__field" type="tel" name="phone" data-validation-required>
                    <span class="label form-input__label">Телефон
                        </span>
                </label>
                <label class="form-input form-input--textarea form__input">
                    <textarea class="form-input__field" name="message"></textarea>
                    <span class="label form-input__label">Расскажите о вашем визите в клинику
                        </span>
                </label>
                <button class="button form__submit" type="submit">Отправить отзыв
                </button>
                <div class="form__agreement home-form__agreement">Нажимая кнопку «Отправить отзыв», я даю согласие на <a
                            href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.
                </div>
                <input type="hidden" name="action" value="review">
            </form>
        </div>
    </div>
</section> */?>
