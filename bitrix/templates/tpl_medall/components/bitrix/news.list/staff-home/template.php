<div class="slider__wrapper swiper-wrapper">
	<? foreach($arResult["ITEMS"] as $doctor): ?>
		<div
            class="slide swiper-slide home-staff__slide"
            data-category="<?= $doctor["PROPERTIES"]["direction_doctor"]["VALUE_XML_ID"] ?>"
        >
			<a href="<?= $doctor["DETAIL_PAGE_URL"] ?>" class="home-staff__name"><?= $doctor["NAME"]." ".$doctor["PROPERTIES"]["io_doctor"]["VALUE"] ?></a>
			<div class="home-staff__description"><?= $doctor["PREVIEW_TEXT"] ?></div>
			<a class="button home-staff__button" href="<?= $doctor["DETAIL_PAGE_URL"] ?>">Подробнее</a>
			<a class="home-staff__image" href="<?= $doctor["DETAIL_PAGE_URL"] ?>">
				<picture>
					<source srcset="<?= WebPHelper::getOrCreateWebPUrl($doctor["PREVIEW_PICTURE"]["SRC"], [ 400 ]) ?>" type="image/webp"/>
					<img class="home-staff__image" src="<?= $doctor["PREVIEW_PICTURE"]["SRC"] ?>" loading="lazy" decoding="async" alt=""/>
				</picture>
			</a>
		</div>
	<? endforeach ?>
</div>

<div class="slider__arrow slider__arrow--prev"></div>
<div class="slider__arrow slider__arrow--next"></div>
<div class="slider__pagination"></div>
