<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="grid__cell grid__cell--m--7 grid__cell--xs--12">
    <div class="landing-team__details">

		<? if($doctor->skills): ?>
            <h3 class="font-xl">Ключевые навыки и специализация:</h3>
            <ul class="list-marked">
				<? foreach ($doctor->skills as $skill): ?>
                    <li><?= $skill ?></li>
				<? endforeach ?>
            </ul>
		<? endif ?>

        <? if($doctor->keySkill): ?>
            <div class="landing-team__important"><?= $doctor->keySkill ?></div>
		<? endif ?>

        <? if($doctor->education): ?>
            <h3 class="font-xl">Образование:</h3>
            <? foreach ($doctor->education as $education): ?>
                <p><?= $education ?></p>
			<? endforeach ?>
		<? endif ?>

        <a class="button landing-team__button" href="#section-contact">Записаться на консультацию</a>

    </div>
</div>