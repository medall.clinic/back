<?php
namespace Directions\Plastic;

class Direction
{
	public $title;
	public $sections;

	public function __construct($title, $directionsRootSectionId)
	{
		$this->title = $title;
		$this->sections = $this->getSections($directionsRootSectionId);
	}

	private function getSections($servicesSectionId)
	{
		$arOrder = ["SORT" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => IBLOCK_ID_LANDINGS,
			"IBLOCK_TYPE" => "landings",
			"SECTION_ID" => $servicesSectionId,
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		];

		$rsSect = \CIBlockSection::GetList($arOrder, $arFilter);
		
		$sections = [];
		while($arResult = $rsSect->Fetch()){
			$sections[] = new Section($arResult);
		}
		return $sections;
	}

}