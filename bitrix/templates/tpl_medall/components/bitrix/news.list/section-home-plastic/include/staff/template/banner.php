<? /** @var $doctor Medreclama\Landing\Doctor */ ?>

<div class="doctor-header">
        <div class="grid grid--justify--center doctor-header__inner">
        <? if($doctor->image): ?>
          <picture>
            <source
              srcset="<?= WebPHelper::getOrCreateWebPUrl($doctor->image, [560, 1120]) ?>"
              type="image/webp"
            >
            <img class="grid__cell grid__cell--s--5 grid__cell--xs--12 doctor-header__image" src="<?=$doctor->image?>" loading="lazy" decoding="async" alt="<?=$doctor->name?>"/>
          </picture>
        <? endif ?>
          <div class="grid__cell grid__cell--s--7 grid__cell--xs--12 doctor-header__info">
            <h3 class="doctor-header__name"><?= $doctor->name ?></h3>
            <? if ($doctor->description): ?>
              <div class="doctor-header__description"><?= $doctor->description ?></div>
            <? endif; ?>
            <? if ($doctor->workExperience): ?>
              <div class="doctor-header__experience">стаж работы: <?=$doctor->workExperience?></div>
            <? endif; ?>
            <a class="button doctor-header__button" href="#contacts-form">Записаться</a>
            <a class="button button--secondary doctor-header__button" href="<?= $doctor->url ?>">Подробнее</a>
          </div>
          <? if ($doctor->instagram || $doctor->vk): ?>
            <div class="grid__cell grid__cell--s--5 grid__cell--xs--12 grid grid--justify--center doctor-header__socials">
              <? if ($doctor->vk): ?>
                <a class="link-icon link-icon--icon--vk" href="<?= $doctor->vk ?>"></a>
              <? endif; ?>
              <? if ($doctor->instagram): ?>
                <a class="link-icon link-icon--icon--instagram" href="<?= $doctor->instagram ?>"></a>
              <? endif; ?>
            </div>
          <? endif; ?>
        </div>
</div>
