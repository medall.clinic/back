<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<div class="slider home-staff__slider">
    <div class="slider__slides swiper">

        <div class="slider__wrapper swiper-wrapper">

			<? foreach ($doctors->doctors as $doctor): ?>
				<? include __DIR__."/slide.php" ?>
			<? endforeach ?>
        </div>

        <div class="slider__arrow slider__arrow--prev"></div>
        <div class="slider__arrow slider__arrow--next"></div>
        <div class="slider__pagination"></div>

    </div>
</div>