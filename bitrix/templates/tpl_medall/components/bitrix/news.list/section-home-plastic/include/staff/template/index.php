<? /** @var Directions\Plastic\Doctors $doctors */ ?>

<?  
    $section_classes = '';
    // if (count($doctors->items) === 1) {
        $section_classes = '  page-section--background-color page-section--background-color--light page-section--logo';
    // }

?>

<section class="container page-section<?=$section_classes?>" id="category-staff">
    <div class="content">

		<? if($doctors->title): ?>
            <h2 class="page-subsection text-align-center"><?= $doctors->title ?></h2>
		<? endif ?>

        <? if($doctors->text): ?>
            <div class="page-subsection grid grid--padding--y">
                <?= $doctors->text ?>
            </div>
		<? endif ?>

        <? if($doctors->doctors): ?>
            <div class="page-subsection home-staff">
                <?if(count($doctors->doctors) > 1):?>
                    <? include __DIR__."/slider.php" ?>
                <?else:?>
                    <?$doctor = $doctors->doctors[0]?>
                    <? include __DIR__."/banner.php" ?>
                <?endif;?>
                <? include __DIR__."/blocks.php" ?>
            </div>
		<? endif ?>
    </div>
</section>
