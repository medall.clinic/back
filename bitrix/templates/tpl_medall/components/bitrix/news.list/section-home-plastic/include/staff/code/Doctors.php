<?php
namespace Directions\Plastic;

class Doctors
{
    public $title;
    public $doctors;

    public function __construct($title, $doctorsPropertyId, $doctorsPropertyArr)
    {
        $this->title = $title;
        $this->doctors = $this->getDoctors($doctorsPropertyId,$doctorsPropertyArr);
    }


    private function getDoctors($doctorsPropertyId,$doctorsPropertyArr)
    {
        $array = $doctorsPropertyArr;
        $search_value = $doctorsPropertyId;
        $key = array_search($search_value, $array);
        $arOrder = ['SORT' => 'ASC'];
        $arFilter = [
            'IBLOCK_ID' => IBLOCK_ID_DOCTORS,
            'IBLOCK_TYPE' => 'doctor',
            'PROPERTY_direction_doctors_VALUE' => $array[$key],
            'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
        ];
        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'DETAIL_PAGE_URL',
            'PREVIEW_TEXT',
            'PREVIEW_PICTURE',
            'PROPERTY_io_doctor',
        ];

        $rsSect = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

        $doctors = [];
        while($arResult = $rsSect->GetNext()){
            $doctors[] = new Doctor($arResult);
        }
        return $doctors;
    }

}