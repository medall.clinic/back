<? /** @var Directions\Plastic\Direction $direction */ ?>
<section class="container page-section page-section--background-color page-section--background-color--light page-section--logo category-directions">

	<h2 class="category-directions__title page-section__title text-align-center page-subsection content"
    ><?= $direction->title ?></h2>

	<div class="slider category-directions__slider page-subsection content">
		<div class="slider__slides swiper">

			<div class="slider__wrapper swiper-wrapper">
				<? foreach ($direction->sections as $section): ?>
                    <? include __DIR__."/slide.php" ?>
				<? endforeach ?>
			</div>

			<div class="slider__pagination"></div>

		</div>
	</div>

</section>