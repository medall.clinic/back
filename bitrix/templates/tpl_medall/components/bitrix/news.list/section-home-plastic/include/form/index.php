<section class="container page-section" id="contacts-form">
	<div class="content">

		<div class="page-subsection">
			<h2 class="page-section__title text-align-center">Записаться на приём</h2>
			<p class="page-section__subtitle">В комментариях укажите, какая услуга, доктор или вопрос Вас интересует.</p>
		</div>

		<div class="page-subsection grid grid--justify--center home-form">
            <? include __DIR__."/form.php" ?>
		</div>

	</div>
</section>
