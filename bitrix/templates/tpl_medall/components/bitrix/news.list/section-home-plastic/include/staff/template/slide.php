<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="slide swiper-slide home-staff__slide">
<a href="<?= $doctor->url ?>" class="home-staff__name"><?= $doctor->name ?></a>
<div class="home-staff__description"><?= $doctor->description ?></div>
			<a class="button home-staff__button" href="<?= $doctor->url ?>">Подробнее</a>
			<a class="home-staff__image" href="<?= $doctor->url ?>">
				<picture>
					<source srcset="<?= WebPHelper::getOrCreateWebPUrl($doctor->image) ?>" type="image/webp"/>
					<img loading="lazy" class="home-staff__image" src="<?= $doctor->image ?>" decoding="async" alt=""/>
				</picture>
			</a>
</div>
