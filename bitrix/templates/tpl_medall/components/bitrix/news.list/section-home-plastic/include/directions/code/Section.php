<?php
namespace Directions\Plastic;

class Section
{
	public $title;
	public $id;
	public $image;
	public $services;

	public function __construct($arResult)
	{
		$this->title = $arResult["NAME"];
		$this->id = $arResult["ID"];
		$this->image = getIbElementImageSrc($arResult["PICTURE"]);
		$this->services = $this->getServices($arResult["ID"]);
	}

	private function getServices($sectionId)
	{
		$arOrder = ["SORT" => "ASC", "ID" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => IBLOCK_ID_LANDINGS,
			"IBLOCK_TYPE" => "landings",
			"SECTION_ID" => $sectionId,
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		];
		$arSelect = ["ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL"];

		$res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

		$services = [];
		while($arResult = $res->GetNext()) {
			$services[] = new Service($arResult);
		}

		return $services;
	}

}