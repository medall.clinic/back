<?php
namespace Directions\Plastic;

class Service
{
	public $title;
	public $url;

	public function __construct($arResult)
	{
		$this->title = $arResult["NAME"];
		$this->url = $arResult["DETAIL_PAGE_URL"];
	}
}