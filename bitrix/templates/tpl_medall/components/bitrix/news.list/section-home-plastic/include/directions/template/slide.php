<? /** @var Directions\Plastic\Section $section */ ?>
<? /** @var Directions\Plastic\Service $service */ ?>
<div class="category-direction swiper-slide category-directions__slide">

	<div class="category-direction__title"><?= $section->title ?></div>

	<div class="category-direction__menu form-input form-input--select">

		<div class="category-direction__menu-name form-input__current">Виды услуг</div>

		<ul class="category-direction__list form-input__options">
			<? foreach ($section->services as $service): ?>
				<li class="category-direction__item">
					<a href="<?= $service->url ?>"><?= $service->title ?></a>
				</li>
			<? endforeach ?>
		</ul>

	</div>

    <picture>
        <source srcset="/images/category/directions-<?= $section->id ?>_s.webp, /images/category/directions-<?= $section->id ?>_s@2x.webp 2x" media="(max-width: 719px)"/>
        <source srcset="/images/category/directions-<?= $section->id ?>_m.webp, /images/category/directions-<?= $section->id ?>_m@2x.webp 2x" media="(min-width: 720px)"/>
        <source srcset="/images/category/directions-<?= $section->id ?>_s.jpg, /images/category/directions-<?= $section->id ?>_s@2x.jpg 2x" media="(max-width: 719px)"/>
        <source srcset="/images/category/directions-<?= $section->id ?>_m.jpg, /images/category/directions-<?= $section->id ?>_m@2x.jpg 2x" media="(min-width: 720px)"/>
        <img class="category-direction__image" src="/images/category/directions-<?= $section->id ?>.jpg" loading="lazy" decoding="async" alt=""/>
    </picture>

</div>
