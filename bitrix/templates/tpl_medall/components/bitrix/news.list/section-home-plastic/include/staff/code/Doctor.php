<?php
namespace Directions\Plastic;

class Doctor
{
	public $name;
	public $description;
	public $url;
	public $image;

	public function __construct($arResult)
	{
		$this->name = $arResult["NAME"]." ".$arResult["PROPERTY_IO_DOCTOR_VALUE"];
		$this->description = $arResult["PREVIEW_TEXT"];
		$this->url = $arResult["DETAIL_PAGE_URL"];
		$this->image = getIbElementImageSrc($arResult["PREVIEW_PICTURE"]);
	}


}