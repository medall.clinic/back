<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<picture>

	<source
		srcset="<?= WebPHelper::getOrCreateWebPUrl($doctor->image) ?>"
		type="image/webp"
	/>

	<img
		src="<?= $doctor->image ?>"
		loading="lazy"
		decoding="async"
		alt="<?= $doctor->name ?>"
	/>

</picture>
