
<? foreach($arResult["ITEMS"] as $block): ?>
    <? if(file_exists(__DIR__.'/include/'.$block["CODE"].'/index.php')): ?>
        
        <? include __DIR__.'/include/'.$block["CODE"].'/index.php' ?>
    <? else: ?>
		<?= $block["PROPERTIES"]["TEXT"]["~VALUE"]["TEXT"] ?>
        <? if($block["PROPERTIES"]["ADDITIONAL_TEXT"]["~VALUE"]["TEXT"]): ?>
            <?= $block["PROPERTIES"]["ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] ?>
	    <? endif ?>
	<? endif ?>

<? endforeach ?>