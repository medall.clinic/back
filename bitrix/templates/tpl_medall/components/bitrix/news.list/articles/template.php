<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>


        <?php foreach ($arResult["ITEMS"] as $arItem) { ?>
            <?php $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
            <article
                    class="article-item articles-layout__item grid__cell grid__cell--m--4 grid__cell--s--6 articles-layout__item"
                    data-category="<?php echo $arItem['~TAGS'] ?>"><a class="article-item__link"
                                                                     href="<?php echo $arItem['DETAIL_PAGE_URL'] ?>">
                    <?php if (!empty($arItem["PREVIEW_PICTURE"]['SRC'])){ ?>
                    <picture>
                        <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($arItem['PREVIEW_PICTURE']['SRC']) ?>"
                                type="image/webp">
                        <source srcset="<?php echo $arItem['PREVIEW_PICTURE']['SRC'] ?>">
                        <img class="article-item__image" src="<?php echo $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                             loading="lazy" decoding="async" alt="<?php echo $arItem['NAME'] ?>">
                    </picture>
                </a>
                <?php if (!empty($arItem['~TAGS'])){?>
                    <a class="article-item__hashtag" href="search/index.php?tags=Пластика" target="_blank">

                        #<?php echo $arItem['~TAGS'] ?>
                    </a>
                <?}?>
                <?php if (!empty($arItem['NAME'])){?>
                    <a class="article-item__title"
                       href="<?php echo $arItem['DETAIL_PAGE_URL'] ?>"><?php echo $arItem['NAME'] ?></a>
                <?}?>

                <?php } else { ?>
                    <picture>
                        <source srcset="/images/article-empty.webp" type="image/webp">
                        <source srcset="/images/article-empty.jpg">
                        <img class="article-item__image" src="/images/article-empty.jpg"
                             loading="lazy" decoding="async" alt="Методы увеличения груди имплантатами">
                    </picture>
                    </a>
                    <?php if (!empty($arItem['~TAGS'])){?>
                        <a class="article-item__hashtag" href="search/index.php?tags=Пластика" target="_blank">
                            #<?php echo $arItem['~TAGS'] ?>
                        </a>
                    <?}?>
                    <?php if (!empty($arItem['NAME'])){?>
                        <a class="article-item__title"
                           href="<?php echo $arItem['DETAIL_PAGE_URL'] ?>"><?php echo $arItem['NAME'] ?></a>
                    <?}?>

                <?php } ?>

            </article>


        <?php } ?>
        <?php if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
           <?= $arResult["NAV_STRING"] ?>
        <?php endif; ?>


