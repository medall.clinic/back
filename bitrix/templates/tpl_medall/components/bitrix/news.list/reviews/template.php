<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
$dateFormat = "j F Y"; // Формат: день месяц год

?>
<?php if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?php endif;?>
<?php $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "template1",
    array(
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "PATH" => "/",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "template1"
    ),
    false
);?>
<section class="container page-section">
    <div class="content">
        <h1><?php echo Loc::getMessage('TITLE_REVIEWS')?></h1>
    </div>
</section>
<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="slider reviews-platforms">
            <div class="slider__slides swiper swiper-initialized swiper-horizontal swiper-backface-hidden">
                <div class="slider__wrapper swiper-wrapper">
                    <?php foreach($arResult["BASE_RATING"] as $key => $arItem){?>
                        <?

                        ?>
                        <div class="slide swiper-slide reviews-platforms__slide swiper-slide-active" style="width: 248px; margin-right: 40px;"><a class="reviews-platforms__item" href="<?php echo $arItem['PROPERTY_RATING_LINK_VALUE'] ?>" target="_blank"> <img class="reviews-platforms__image" src="<?php echo $arResult['PICTURES'][$key]['SRC']?>" decoding="async" width="60" height="60">
                                <div class="reviews-platforms__value"><?php echo $arItem['PROPERTY_RATING_BALL_VALUE'] ?></div>
                                <div class="reviews-platforms__name"><?php echo $arItem['NAME']?></div></a>
                        </div>
                    <?php }?>


                </div>
                <div class="slider__arrow slider__arrow--prev slider__arrow--disabled"></div>
                <div class="slider__arrow slider__arrow--next"></div>
                <div class="slider__pagination swiper-pagination-bullets swiper-pagination-horizontal"><span class="slider__pagination-item slider__pagination-item--active"></span><span class="slider__pagination-item"></span><span class="slider__pagination-item"></span><span class="slider__pagination-item"></span></div>
            </div>
        </div>
    </div>
</section>
<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="reviews-filter page-subsection">
            <div class="reviews-filter__inner">
                <div class="form-input form-input--select form-input--secondary reviews-filter__select reviews-filter__select--services reviews-filter__select reviews-filter__select--services hidden-xs hidden-s hidden-m hidden-l">
                    <select class="form-input__field" name="service">
                        <option value=""></option>
                        <?php foreach($arResult["SERVICES"] as $service):?>
                            <option value="<?php echo $service['ID']?>"><?php echo $service['NAME']?></option>
                        <?php endforeach;?>
                    </select>
                    <span class="label form-input__label">Выберите услугу</span>
                    <button class="form-input__select-button" type="button"></button>
                </div>
                <div class="form-input form-input--select form-input--secondary reviews-filter__select reviews-filter__select--doctors reviews-filter__select reviews-filter__select--doctors">
                    <select class="form-input__field" name="doctor">
                        <option value=""></option>
                        <?php foreach($arResult["DOCTORS"] as $doctor):?>
                            <option value="<?php echo $doctor['ID']?>"><?php echo $doctor['NAME']?> </option>
                        <?php endforeach;?>
                    </select>
                    <span class="label form-input__label">Выберите специалиста</span>
                    <button class="form-input__select-button" type="button"></button>
                </div>
                <a class="button" href="#review-form">Оставить отзыв</a>
            </div>

        </div>

        <div class="grid page-subsection reviews-item-list reviews-item-list--text">
            <?php foreach($arResult["ITEMS"] as $key => $arItem){?>


                <?php $formattedDate = FormatDate($dateFormat, MakeTimeStamp($arItem["DATE_ACTIVE_FROM"], CSite::GetDateFormat()));?>

                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="reviews-item reviews-item--text grid__cell grid__cell--m--6" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-service="<?php echo $arResult['FILTER_NAME_SERVICE'][$key]?>" data-doctor="<?php echo $arResult['FILTER_NAME_DOCTOR'][$key]?>">
                    <div class="reviews-item__platform">
                        Отзыв оставлен на сайте
                        <?php if ($arItem['PROPERTIES']['RATING_LABEL']['VALUE']){?>

                            <a href="<?php echo $arItem['PROPERTIES']['RATING_LABEL']['URL']?>" target="_blank"><?php echo $arItem['PROPERTIES']['RATING_LABEL']['VALUE']?></a>
                        <?php }?>
                    </div>
                    <div class="reviews-item__name"><?php echo $arItem["DISPLAY_PROPERTIES"]['NAME_REVIEW']['VALUE']?></div>
                    <div class="reviews-item__date hidden-xs hidden-s hidden-m hidden-l">
                        <?php if (!empty($arItem["DATE_ACTIVE_FROM"])){?>
                            <?php echo $formattedDate ?>

                        <?php }?>
                    </div>
                    <?php
                    if (!empty($arItem["DISPLAY_PROPERTIES"]["RATING"]["VALUE"])){?>
                        <div class="reviews-item__rating reviews-item__rating--<?php echo $arItem["DISPLAY_PROPERTIES"]["RATING"]["VALUE"]?> hidden-xs hidden-s hidden-m hidden-l">
                            <?php
                            if ($arItem["DISPLAY_PROPERTIES"]["RATING"]["VALUE"]) {?>
                                <div class="reviews-item__rating-star"> </div>
                                <div class="reviews-item__rating-star"> </div>
                                <div class="reviews-item__rating-star"> </div>
                                <div class="reviews-item__rating-star"> </div>
                                <div class="reviews-item__rating-star"> </div>

                            <?php } ?>

                        </div>
                    <?}
                    ?>

                    <div class="reviews-item__text">
                        <?php if (!empty($arItem["PREVIEW_TEXT"])){?>
                            <?php echo $arItem["PREVIEW_TEXT"]?>

                        <?php }?>
                    </div>
                    <?php if (!empty($arItem['PROPERTIES']['doctor_review']['VALUE'])) {
                        // Проверяем, есть ли хотя бы один доктор с непустым именем
                        $hasDoctor = false;
                        foreach ($arItem['DOCTORS'] as $propKey => $property) {
                            if (in_array($property['ID'], $arItem['PROPERTIES']['doctor_review']['VALUE']) && !empty($property['NAME'])) {
                                $hasDoctor = true;
                                break; // Можно выйти из цикла, если хоть один доктор найден
                            }
                        }

                        if ($hasDoctor) { // Если есть доктор с ненулевым именем, выводим блок
                            ?>
                            <div class="reviews-item__doctor">Отзыв о:
                                <?php foreach ($arItem['DOCTORS'] as $propKey => $property) { ?>
                                    <?php if (in_array($property['ID'], $arItem['PROPERTIES']['doctor_review']['VALUE']) && !empty($property['NAME'])) { ?>
                                        <a class="reviews-item__doctor-link" href="<?php echo $property['DETAIL_PAGE_URL'] ?>">
                                            <?php echo $property['NAME'] . ' ' . $property['~PROPERTY_IO_DOCTOR_VALUE'] ?>
                                        </a>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <?php
                        }
                    } ?>


                    <?php if (!empty($arItem['PROPERTIES']['SERVICE']['VALUE'])){?>

                        <div class="reviews-item__service">

                            Услуга:


                            <?php foreach ($arItem['SERVICE'] as $propKey => $property){?>
                                <?php if (in_array($property['ID'],$arItem['PROPERTIES']['SERVICE']['VALUE'])){?>
                                    <a class="reviews-item__service-link" href="<?php echo $property['DETAIL_PAGE_URL']?>"><?php echo $property['NAME']?></a>

                                <?php }?>


                            <?php }?>
                        </div>
                    <? } ?>



                </div>
            <?php };?>
        </div>

        <div class="grid grid--justify--center page-subsection">
            <a class="button reviews-item-load-button" href="#">Показать еще</a>
        </div>
    </div>

</section>

<section class="container page-section" id="review-form">
    <div class="content">
        <div class="page-subsection">
            <h2 class="text-align-center">Оставить отзыв
            </h2>
        </div>
        <div class="grid grid--justify--center page-subsection">
            <form class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form form-ajax" data-is-validate="true" data-is-answer-in-popup="true" data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации">
                <label class="form-input form-input--text form__input">
                    <input class="form-input__field" type="text" name="name" data-validation-required>
                    <span class="label form-input__label">ФИО
                        </span>
                </label>
                <label class="form-input form-input--tel form__input">
                    <input class="form-input__field" type="tel" name="phone" data-validation-required>
                    <span class="label form-input__label">Телефон
                        </span>
                </label>
                <label class="form-input form-input--textarea form__input">
                    <textarea class="form-input__field" name="message"></textarea>
                    <span class="label form-input__label">Расскажите о вашем визите в клинику
                        </span>
                </label>
                <button class="button form__submit" type="submit">Отправить отзыв
                </button>
                <div class="form__agreement home-form__agreement">Нажимая  кнопку «Отправить отзыв», я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</div>
                <input type="hidden" name="action" value="review">
            </form>
        </div>
    </div>
</section>
