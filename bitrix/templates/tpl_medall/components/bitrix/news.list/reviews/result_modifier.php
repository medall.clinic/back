<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arFieldsService = [];
$arSelectService = ['NAME', 'ID', 'DETAIL_PAGE_URL'];
$arFilterService = [
    'IBLOCK_ID' => 47,
    'ACTIVE' => 'Y',
    '!=PROPERTY_703_VALUE' => 'Да',

];

$res = CIBlockElement::GetList([], $arFilterService, false, false, $arSelectService);
while ($arResService = $res->GetNext()) {
    $arFieldsService[] = $arResService; // сохраняем имя по ID
}


$arFieldsRating = [];
$arSelectRating = [
    'NAME',
    'ID',
    'DETAIL_PAGE_URL',
    'PROPERTY_RATING_BALL',
    'PROPERTY_RATING_LINK',
    'PROPERTY_RATING_NAME',
    'PROPERTY_PICTURES',
];
$arFilterRating = [
    'IBLOCK_ID' => 69,
    'ACTIVE' => 'Y',
];

$resRating = CIBlockElement::GetList([], $arFilterRating, false, false, $arSelectRating);
while ($arRes = $resRating->GetNext()) {
    $arFieldsRating[] = $arRes; // сохраняем имя по ID
}
foreach ($arResult['ITEMS'] as $key => $arField) {
    $arResult['BASE_RATING'] = $arFieldsRating;
    $pictures = CFile::GetFileArray($arResult['BASE_RATING'][$key]['PROPERTY_PICTURES_VALUE']);
    $arResult['PICTURES'][$key]['SRC'] = $pictures['SRC'];

}


$arFieldsDoctors = [];
$arSelectDoctors = ['NAME', 'ID', 'DETAIL_PAGE_URL','PROPERTY_io_doctor'];
$arFilterDoctors = [
    'IBLOCK_ID' => 6,
    'ACTIVE' => 'Y',
];

$resDoc = CIBlockElement::GetList([], $arFilterDoctors, false, false, $arSelectDoctors);
while ($arResDoctor = $resDoc->GetNext()) {
    $arFieldsDoctors[] = $arResDoctor; // сохраняем имя по ID
}


$arResult['DOCTORS'] = $arFieldsDoctors;
foreach ($arResult['ITEMS'] as $key => $arField) {
    $arResult['ITEMS'][$key]['DOCTORS'] = $arFieldsDoctors;

    $nameFilterDoctor = []; // Сбрасываем массив перед новым заполнением

    foreach ($arResult['DOCTORS'] as $Dockey => $docField) {
        if (in_array($docField['ID'], $arField['PROPERTIES']['doctor_review']['VALUE'])) {
            $nameFilterDoctor[] = trim($docField['NAME']) . ' ' . trim($docField['~PROPERTY_IO_DOCTOR_VALUE']);
        }
    }

    // Сохраняем строку в нужный элемент
    $arResult['FILTER_NAME_DOCTOR'][$key] = implode(',', $nameFilterDoctor);
}

// Вывод результата
$arResult['SERVICE'] = $arFieldsDoctors;

foreach ($arResult['ITEMS'] as $key => $arField) {
    $arResult['ITEMS'][$key]['SERVICE'] = $arFieldsService;
    $nameFilterService = [];
    foreach ($arResult['ITEMS'][$key]['SERVICE'] as $serviceKey => $serviceField) {
        if (in_array($serviceField['ID'], $arField['PROPERTIES']['SERVICE']['VALUE'])) {
               $nameFilterService[] = $serviceField['NAME'];
        }

    }

    $arResult['FILTER_NAME_SERVICE'][$key] = implode(',', $nameFilterService);

}
foreach ($arResult['ITEMS'] as $key => $arField) {
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'2GIS') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://2gis.ru/spb/firm/70000001035838469?m=30.285963%2C59.966204%2F16';
    }
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'Яндекс Карты') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://yandex.ru/maps/org/medall/35461995871/?ll=30.285847%2C59.966174&z=17';
    }
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'Google maps') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://www.google.com/maps/place/Клиника+MEDALL/@59.966168,30.2832555,17z/data=!4m8!3m7!1s0x469631cd7ce1a2bb:0x9e8f4e1bbc2adb66!8m2!3d59.966168!4d30.2858304!9m1!1b1!16s%2Fg%2F1v4k559y?entry=ttu';
    }
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'СберЗдоровье') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://spb.docdoc.ru/clinic/klinika_cifrovoj_stomatologii_medall';
    }
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'НаПоправку') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://spb.napopravku.ru/clinics/medall-centry-esteticheskoy-mediciny-i-cifrovoy-stomatologii/';
    }
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'ПроДокторов') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://prodoctorov.ru/spb/lpu/63649-medall/';
    }
    if (strcasecmp($arField['PROPERTIES']['RATING_LABEL']['VALUE'],'Докту') === 0) {
        $arResult['ITEMS'][$key]['PROPERTIES']['RATING_LABEL']['URL'] = 'https://doctu.ru/spb/net/medall';
    }
}
