<?
  $res = CIBlockSection::GetByID($arParams["SECTION_ID"]);
  if($arSection = $res->GetNext())
  {
    echo $arSection["DESCRIPTION"];
  };
?>
<? foreach ($arResult["ITEMS"] as $item) {?>
  <div class="page-subsection">
    <h2 class="vacancies-list__subtitle"><?=$item["NAME"]?></h2>
    <?= $item["DETAIL_TEXT"]?>
  </div>
<? } ?>