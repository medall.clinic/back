<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<ul class="pagination page-subsection">

    <?php if ($arResult["NavPageNomer"] > 1): ?>
        <li class="pagination__item pagination__item--prev">
            <?php if($arResult["bSavePage"]): ?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
            <?php else: ?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>

            <?php endif; ?>
        </li>
    <?php else: ?>
        <li class="pagination__item pagination__item--prev disabled"><a href="#"></a></li>
    <?php endif; ?>

    <?php while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>
        <li class="pagination__item">
            <?php if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                <a class="pagination__link pagination__link--active" href="#"><?=$arResult["nStartPage"]?></a>
            <?php else: ?>
                <a class="pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
            <?php endif; ?>
        </li>
        <?php $arResult["nStartPage"]++; ?>
    <?php endwhile; ?>

    <?php if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
        <li class="pagination__item pagination__item--next">
            <?php if($arResult["bSavePage"]): ?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
            <?php else: ?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>

            <?php endif; ?>
        </li>
    <?php else: ?>
        <li class="pagination__item pagination__item--next disabled"><a href="#"></a></li>
    <?php endif; ?>

</ul>
