<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$previewText = '';
$doctorIds = [];
$arFieldsDirectionSection = [];
$arFieldsSelectSection = ['NAME', 'ID'];
$arIdServiceSectionElements = [];
$arIdServiceElements = [];
$arDoctors = [];
$doctorDesc = [];
$arrException = ['Флебология', 'Гинекология', 'Пересадка волос', 'Рассрочка'];

$rsGeneralPageSection = CIBlockSection::GetByID(433);
if ($arParentSection = $rsGeneralPageSection->GetNext()) {
    $arFilterSection = [
        'IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
        '!=NAME' => $arrException,
        '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
        '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
        '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
    ];
    $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilterSection, false, $arFieldsSelectSection);
    while ($arSect = $rsSect->GetNext()) {
        $arFieldsDirectionSection[] = $arSect;
    }
}

$sectionIds = [160, 159];
foreach ($sectionIds as $sectionId) {
    $rsGeneralPageSection = CIBlockSection::GetByID($sectionId);
    if ($arParentSection = $rsGeneralPageSection->GetNext()) {
        $arFilterSection = [
            'IBLOCK_ID' => 47,
            'ACTIVE' => 'Y',
            '!=NAME' => $arrException,
            '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
            '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
            '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
        ];
        $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilterSection, false, $arFieldsSelectSection);
        while ($arSect = $rsSect->GetNext()) {
            $arIdServiceSectionElements[] = $arSect['ID'];
        }
    }
}

$arFilter = ['IBLOCK_ID' => 47, 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $arIdServiceSectionElements];
$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", 'DETAIL_PAGE_URL', "IBLOCK_SECTION_ID"];
$arFields = [];
$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
while ($ob = $res->GetNext()) {
    $arFields[] = $ob;
    $arIdServiceElements[] = $ob['ID'];
}
$serviceData = [];
foreach ($arFields as $service) {
    $serviceData[$service['ID']] = [
        'NAME' => $service['NAME'],
        'URL' => $service['DETAIL_PAGE_URL'],

    ];
}

$arFilterDoctor = [
    'IBLOCK_ID' => 6,
    'ACTIVE' => 'Y',
    'PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE' => $arIdServiceElements,
    'PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE' => $arIdServiceElements,
];
$arSelectDoctor = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    'PREVIEW_TEXT',
    'DETAIL_PAGE_URL',
    "DATE_ACTIVE_FROM",
    "IBLOCK_SECTION_ID",
    "PROPERTY_specialization_doctor_plastic",
    'PROPERTY_IO_DOCTOR',
];
$arFieldsDoctor = [];
$res = CIBlockElement::GetList([], $arFilterDoctor, false, false, $arSelectDoctor);
while ($obDoctor = $res->GetNext()) {
    $arFieldsDoctor[] = $obDoctor;
}
$doctorData = [];

foreach ($arFieldsDoctor as $doctor) {

    $doctorData[$doctor['ID']] = [
        'URL' => $doctor['DETAIL_PAGE_URL'],
        'IO' => $doctor['PROPERTY_IO_DOCTOR_VALUE'],
        'NAME' => $doctor['NAME'],
        'TEXT' => $doctor['PREVIEW_TEXT'] // Предполагается, что в PREVIEW_TEXT хранится ссылка на страницу врача

    ];
}


?>


<section class="container page-section">
    <div class="content">
        <h1><?php $APPLICATION->ShowTitle()?></h1>
    </div>
</section>
<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="works-filter page-subsection">
            <div class="works-filter__inner">
                <div class="form-input form-input--select form-input--secondary works-filter__select works-filter__select--directions">
                    <select class="form-input__field" name="direction">
                        <?php foreach ($arFieldsDirectionSection as $direction): ?>
                            <?php if (!empty($direction['NAME'])): ?>
                                <option value="<?php echo $direction['NAME']; ?>"><?php echo $direction['NAME']; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                    <span class="label form-input__label">Выберите направление</span>
                </div>
                <div class="form-input form-input--select form-input--secondary works-filter__select works-filter__select--services">
                    <select class="form-input__field" name="service">
                        <?php foreach ($arFields as $field): ?>
                            <option value="<?php echo $field['NAME']; ?>"><?php echo $field['NAME']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="label form-input__label">Выберите услугу</span>
                </div>
                <div class="form-input form-input--select form-input--secondary works-filter__select works-filter__select--doctors">
                    <select class="form-input__field" name="doctor">
                        <?php
                        $addedNames = array(); // Массив для отслеживания добавленных имен

                        foreach ($arFieldsDoctor as $doctor):
                            if (!in_array($doctor['NAME'], $addedNames)):
                                $addedNames[] = $doctor['NAME']; // Добавляем имя в массив уже добавленных имен
                                ?>
                                <option value="<?php echo $doctor['NAME']; ?>">
                                    <?php echo $doctor['NAME'] . ' ' .$doctor['PROPERTY_IO_DOCTOR_VALUE']; ?>
                                </option>
                            <?php
                            endif;
                        endforeach;
                        ?>
                    </select>
                    <span class="label form-input__label">Выберите специалиста</span>
                </div>
            </div>
        </div>
        <div class="grid page-subsection"></div>
    </div>
</section>
<section class="container page-section page-section--margin--small">
    <div class="content">
        <div class="grid page-subsection">
            <?php foreach ($arResult["ITEMS"] as $arItem): ?>

                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <?php if (!empty($arItem['NAME']) && !empty($arItem['DISPLAY_PROPERTIES']['PHOTO_AFTER']['FILE_VALUE']['SRC'])): ?>
                    <div class="works-item grid__cell grid__cell--m--3 grid__cell--s--6" id="<?php echo $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="image-compare icv icv__icv--horizontal standard">
                            <?php echo generateWebPImage($arItem['DISPLAY_PROPERTIES']['PHOTO_BEFORE']['FILE_VALUE']['SRC']); ?>
                            <?php echo generateWebPImageWithControl($arItem['DISPLAY_PROPERTIES']['PHOTO_AFTER']['FILE_VALUE']['SRC']); ?>
                        </div>
                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'])){
                            echo getService($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'], $serviceData);

                        }?>
                        <?php if (
                            empty($arItem['DISPLAY_PROPERTIES']['SERVICE_BINDING']['~VALUE'])
                            && !empty($arItem['DISPLAY_PROPERTIES']['SERVICE_LINK']['~VALUE'])


                        ) {?>
                            <a class="works-item__service"
                               href="<?php echo $arItem['DISPLAY_PROPERTIES']['SERVICE_LINK']['~VALUE']?>">
                                <?php echo $arItem['NAME']?>
                            </a>


                        <?php }?>
                        <?php if (!empty($arItem['DISPLAY_PROPERTIES']['DOCTOR']['VALUE'])): ?>
                            <?php echo getDoctorLink($arItem['DISPLAY_PROPERTIES']['DOCTOR']['VALUE'], $doctorData); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<?php
function generateWebPImage($jpegData) {
    $pathWebp = str_replace([".jpg", ".jpeg", ".png"], ".webp", strtolower($jpegData));
    if (!file_exists($pathWebp)) {
        if (strpos($jpegData, '.png') !== false) {
            $img = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        } elseif (strpos($jpegData, '.jpg') !== false || strpos($jpegData, '.jpeg') !== false) {
            $img = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        }
        imagepalettetotruecolor($img);
        imagealphablending($img, true);
        imagesavealpha($img, true);
        imagewebp($img, $_SERVER["DOCUMENT_ROOT"] . $pathWebp, 100);
        imagedestroy($img);
    }
    return '<img src="' . $pathWebp . '" alt="Пьезоринопластика" loading="lazy" decoding="async" width="720" height="720" class="icv__img icv__img-a">';
}

function generateWebPImageWithControl($jpegData) {
    $pathWebp = str_replace([".jpg", ".jpeg", ".png"], ".webp", strtolower($jpegData));
    if (!file_exists($pathWebp)) {
        if (strpos($jpegData, '.png') !== false) {
            $img = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        } elseif (strpos($jpegData, '.jpg') !== false || strpos($jpegData, '.jpeg') !== false) {
            $img = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"] . $jpegData);
        }
        imagepalettetotruecolor($img);
        imagealphablending($img, true);
        imagesavealpha($img, true);
        imagewebp($img, $_SERVER["DOCUMENT_ROOT"] . $pathWebp, 100);
        imagedestroy($img);
    }
    return '<div class="icv__wrapper">
                <img src="' . $pathWebp . '" alt="Пьезоринопластика" loading="lazy" decoding="async" width="720" height="720" class="icv__img icv__img-b">
            </div>
            <div class="icv__control">
                <div class="icv__control-line"></div>
                <div class="icv__theme-wrapper">
                    <div class="icv__arrow-wrapper"></div>
                    <div class="icv__arrow-wrapper"></div>
                </div>
                <div class="icv__control-line"></div>
            </div>';
}

function getDoctorLink($doctorId, $doctorData) {
    if (isset($doctorData[$doctorId])) {
        return '<div class="works-item__doctor">
                    '.$doctorData[$doctorId]['TEXT'].'

                     <a href="' .  $doctorData[$doctorId]['URL'] . '">' . $doctorData[$doctorId]['NAME'] . ' ' . $doctorData[$doctorId]['IO'].'</a>

                </div>';
    }
    return '';
}

function getService($serviceId, $serviceData) {
    if (isset($serviceData[$serviceId])) {
        return '<a class="works-item__service"  href="' .  $serviceData[$serviceId]['URL'] . '">' . $serviceData[$serviceId]['NAME'] . '</a>';
    }
    return '';
}
?>
