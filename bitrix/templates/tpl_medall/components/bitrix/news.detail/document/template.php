<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="container page-section">
	<div class="content">
		<h1>Общая юридическая информация</h1>
		<p class="p text-marked">свидетельства и лицензии</p>
		<h2>Общая информация</h2>
		<div class="page-subsection">
			<?= $arResult["DETAIL_TEXT"];?>
		</div>
		<div class="page-subsection">
			<ul class="p list-unordered">
				<?
					foreach ($arResult["DISPLAY_PROPERTIES"]['fire_document']['FILE_VALUE'] as $a=>$d) {
					$r = explode(".", $d['SRC']);
				?>
					<li class="list-unordered__item">
						<a href="<?=$d['SRC']?>"><?=$d['ORIGINAL_NAME']?> (<?=end($r);?>, <?echo CFile::FormatSize($d['FILE_SIZE'])?>)</a>
					</li>
				<? } ?>
			</ul>
		</div>
		<? if($arResult["DISPLAY_PROPERTIES"]["licenses_document"]["VALUE"]): ?>
			<h2>Лицензии клиники</h2>
			<div class="page-subsection">
				<div class="slider documents-slider">
					<div class="slider__slides swiper">
						<div class="slider__wrapper swiper-wrapper">
							<? foreach($arResult["DISPLAY_PROPERTIES"]["licenses_document"]["VALUE"] as $licenseId): ?>
								<? $licenseSrc = CFile::GetPath($licenseId); ?>
								<a class="slide swiper-slide" href="<?=$licenseSrc?>" data-glightbox="data-glightbox" data-gallery="licenses">
									<picture>
										<source srcset="<?=WebPHelper::getOrCreateWebPUrl($licenseSrc, [200, 400])?>"/>
										<img src="<?=$licenseSrc?>" loading="lazy" decoding="async" alt=""/>
									</picture>
								</a>
							<? endforeach ?>
						</div>
						<div class="slider__arrow slider__arrow--prev"></div>
						<div class="slider__arrow slider__arrow--next"></div>
						<div class="slider__pagination"></div>
					</div>
				</div>
			</div>
		<? endif; ?>
	</div>
</section>
