<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Medreclama\Landing\Landing;
$this->setFrameMode(true);
$this->addExternalCss("/local/front/template/styles/landing.core.css");
define("IS_LANDING_PAGE", true); // for landing.rest.css

$models = require __DIR__."/system/init.php";

/** @var $model Landing */
foreach ($models as $model) {
	echo $model->render();
}
