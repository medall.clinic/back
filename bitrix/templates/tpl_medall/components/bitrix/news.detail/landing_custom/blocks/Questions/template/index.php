<?php
/** @var $questions Medreclama\Landing\Questions */ ?>
<?php
/** @var $question Medreclama\Landing\Question */ ?>
<?php
/** @var $button Medreclama\Landing\Button */ ?>
<?php
$button = $questions->button ?>
<section class="page-section page-section--background-color page-section--background-color--dark">
    <div class="container questions-header questions-header--background-color--dark">
        <h2 class="content text-align-center">Ответы на эти вопросы помогут вам принять правильное решение
        </h2>
    </div>
    <div class="container questions-list">
        <div class="content grid">
            <div class="grid__cell grid__cell--xs--12">
                <div class="page-subsection">
                    <?php foreach ($questions->items as $question){ ?>

                    <div class="questions-list__item">
                        <?php if (!empty($question->text)){?>
                            <div class="questions-list__name"><?php echo $question->text?></div>

                        <?php }?>
                        <div class="questions-list__body">
                            <?php if (!empty($question->answer)){?>
                                <p>
                                    <?php echo $question->answer?>
                                </p>

                            <?php }?>

                        </div>
                    </div>
                    <?php };?>

                </div>
                <?php if (!empty($questions->link)){?>
                    <div class="questions-list__sources"><a class="questions-list__sources-name" href="#">Источники</a>
                        <ul class="questions-list__sources-list">
                            <?php
                            foreach ($questions->link as $link){?>
                                <?php if (!empty($link)){?>
                                    <li class="questions-list__sources-item"><?php echo $link?></li>

                                <?php }?>

                            <?php } ?>

                        </ul>
                    </div>
                <?php }?>

            </div>
        </div>
    </div>
</section>
