<?php
/** @var $reviewsservice Medreclama\Landing\ReviewsService */

// Предположим, что у Вас есть объект, в котором определен метод processReviews
$arResult = []; // Инициализируйте $arResult, если это необходимо

// Вызов метода processReviews
list(
    $feedbackValues,
    $serviceNames,
    $serviceUrls,
    $doctorNames,
    $doctorUrls,
    $feedbackPlatforms,
    $ratings,
    $date,
    ) = $reviewsservice->processReviews($arResult);

$reviews = $reviewsservice->loadReviews();
$serviceNames = array_unique($serviceNames);
$feedbackPlatformsUrl = $arResult['URL'];
$ratings = array_unique($ratings);
$date = array_unique($date);

$statusLink = false;
$pattern = '/\b(plastic|cosmetology|phlebology|transplantation|dentistry|gynecology)\b/i';
$url = $_SERVER['REQUEST_URI'];

if (preg_match($pattern, $url)) {
    $statusLink = true; // Найдено
} else {
    $statusLink = false; // Не найдено
}



// Установите локаль, если нужно отображение месяца на нужном языке
setlocale(LC_TIME, 'ru_RU.UTF-8');
?>


<section class="container page-section page-section--background-color page-section--background-color--light home-feedback"
    <?php if (!empty($reviewsservice->reviewsIdBlock)){?>
        id="<?php echo $reviewsservice->reviewsIdBlock ?>"
    <?php } ?>>

    <div class="content">
        <h2 class="page-section__title page-subsection text-align-center">Отзывы
        </h2>
        <div class="slider page-subsection home-feedback__slider reviews-item-list">
            <div class="slider__slides swiper swiper-initialized swiper-horizontal swiper-backface-hidden">
                <div class="slider__wrapper swiper-wrapper">
                    <?php foreach ($reviews as $key => $review){
                        $formattedDate = strtolower(FormatDate("d F Y", MakeTimeStamp($date[$key])));

                        ?>

                        <?php if (!in_array($review['ID'],$reviewsservice->idCount)){?>

                            <div class="slide swiper-slide" style="">
                                <div class="reviews-item" data-service="<?php echo $serviceNames[$key]?>" data-doctor="<?php echo $doctorNames[$key]?>">
                                    <div class="reviews-item__platform">
                                        Отзыв оставлен на сайте
                                        <?php if(!empty($feedbackPlatforms[$review['ID']]) && !$statusLink){?>
                                            <a href="<?php echo $feedbackPlatformsUrl[$review['ID']]?>" target="_blank"><?php echo $feedbackPlatforms[$review['ID']]?></a>

                                        <?php } elseif(!empty($feedbackPlatforms[$review['ID']]) && $statusLink) {?>
                                            <?php echo $feedbackPlatforms[$review['ID']]?>

                                        <?php } ?>

                                    </div>
                                    <?php if (!empty($arResult['NAME_REVIEW'][$review['ID']][$key])){?>
                                        <div class="reviews-item__name"><?php echo  $arResult['NAME_REVIEW'][$review['ID']][$key] ?></div>

                                    <?php }?>
                                    <div class="reviews-item__date hidden-xs hidden-s hidden-m hidden-l"><?php echo $formattedDate?></div>
                                    <?php if (!empty($ratings[$key])){?>
                                        <div class="reviews-item__rating reviews-item__rating--<?php echo $ratings[$key] ?> hidden-xs hidden-s hidden-m hidden-l">
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                        </div>
                                    <?php } ?>
                                    <div class="reviews-item__text"><?php echo $review['~PREVIEW_TEXT']?>
                                    </div>
                                    <?php if (!empty($arResult['DOCTOR_NAME'][$review['ID']][0])){?>
                                        <div class="reviews-item__doctor">Отзыв о:
                                            <?php foreach ($arResult['DOCTOR_NAME'] as $d => $doctors) {
                                                ?>
                                                <?php foreach ($doctors as $index => $doctor) { ?>


                                                    <?php if ($d == $review['ID']) {?>
                                                        <a class="reviews-item__doctor-link" href="<?= $doctorUrls[$d][$index] ?>"><?= $doctor ?></a>

                                                    <?php } ?>


                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                    <?php }?>


                                    <?php
                                    if (!empty($serviceNames)){?>
                                        <div class="reviews-item__service">Услуга:
                                            <?php foreach ($serviceNames as $s => $service){?>
                                                <a class="reviews-item__doctor-link" href="<?=$serviceUrls[$s]?>"><?php echo $service?></a>

                                            <?php }?>
                                        </div>

                                    <?php } ?>

                                </div>
                            </div>

                        <?php }?>



                        <?php
                        $reviewsservice->idCount[] = $review['ID'];

                    } ?>


                </div>
                <div class="slider__arrow slider__arrow--prev slider__arrow--disabled"></div>
                <div class="slider__arrow slider__arrow--next"></div>
                <div class="slider__pagination swiper-pagination-bullets swiper-pagination-horizontal"><span class="slider__pagination-item slider__pagination-item--active"></span><span class="slider__pagination-item"></span><span class="slider__pagination-item"></span><span class="slider__pagination-item"></span><span class="slider__pagination-item"></span><span class="slider__pagination-item"></span></div>
            </div>
        </div>
    </div>
</section>
