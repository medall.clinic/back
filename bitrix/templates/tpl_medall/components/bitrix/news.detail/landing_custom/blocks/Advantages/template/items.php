<? /** @var $advantage Medreclama\Landing\Advantage */ ?>
<? /** @var $advantages Medreclama\Landing\Advantages */ ?>
<div class="page-subsection grid">
	<? foreach ($advantages->items as $advantage): ?>
        <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
            <h3 class="h4"><?= $advantage->title ?></h3>
			<?= $advantage->text ?>
        </div>
	<? endforeach ?>
</div>