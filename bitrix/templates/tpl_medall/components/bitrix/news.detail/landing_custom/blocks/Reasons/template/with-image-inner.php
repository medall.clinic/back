<?/** @var Medreclama\Landing\Reason $reason */?>
<div class="grid__cell grid__cell--l--4 grid__cell--xs--4 landing-reason__image">
    <picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($reason->image) ?>" type="image/webp"/>
        <img src="<?= $reason->image ?>" alt="<?= $reason->text ?>" loading="lazy" decoding="async"/>
    </picture>
</div>

<? if($reason->text): ?>
    <div class="grid__cell grid__cell--l--8 grid__cell--xs--8 landing-reason__info">
		<?= $reason->text ?>
    </div>
<? endif ?>