<?
namespace Medreclama\Landing;

class Operations extends Landing
{
    public $description;
    public $items;
    public $title;
    public $pathSrcArr;
    public $arrThumbs;
    /**
     * @var array|bool|mixed|null
     */
    public $fileArray;

    public function __construct(array $arResult)
    {
        parent::__construct($arResult);
        $this->title = $this->landingProps["TYPES_OF_OPERATIONS_HEADING"]['VALUE'];

        $this->description = $this->landingProps["TYPES_OF_OPERATIONS_DESC"]['VALUE']['TEXT'];
        $this->blockId = $this->getBlockId($this->landingProps["OPERATIONS_BLOCK_ID"]);

        $this->items = $this->getItems($this->landingProps["TYPES_OF_OPERATIONS"]["VALUE"]);

        $this->button = new Button($this->landingProps["OPERATIONS_BUTTON_TEXT"], $this->landingProps["OPERATIONS_BUTTON_LINK"], $this->landingProps["OPERATIONS_BUTTON_FANCYBOX"]);

        $this->additionalText = $this->landingProps["OPERATIONS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["OPERATIONS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

        $this->generalBlockId = $this->landingProps["OPERATIONS_GENERAL_BLOCK"]["VALUE"];
        $this->menuItem = new MenuItem($this->landingProps["OPERATIONS_MENU_TITLE"]["VALUE"], $this->landingProps["OPERATIONS_MENU_LINK"]["VALUE"], $this->blockId);

        $this->isShow = $this->isShow();


        return true;
    }

    private function getItems($elementsId)
    {
        if(empty($elementsId) AND empty($imagesId)){
            return false;
        }

        $items = [];

        if($elementsId){
            $elements = $this->getElements($elementsId);

            if($elements){
                /** @var Element $element */
                foreach ($elements as $element){
                    $item = new Operation;
                    $item->picture = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
                    $item->title_h3 = $element->props["TITLE_H3"]['~VALUE'];
                    $item->sub_items = $element->props["TITLE_H4"]['~VALUE'];
                    $item->description = $element->fields["~PREVIEW_TEXT"];
                    $item->descriptionListing = $element->props["DESCRIPTION_LISTING"]['~VALUE'];
                    $item->thumbListing = $element->props["THUMB_LISTING"]['VALUE'];
                    $this->getPicturesArr($element->props["THUMB_LISTING"]['VALUE']);

                    $items[] = $item;


                }
            }
        }




        return $items;
    }

    public function getPicturesArr($arrPictures): array{

        foreach ($arrPictures as $k => $picture){

            $this->fileArray = \CFile::MakeFileArray($picture);

            if (isset($this->fileArray['tmp_name'])) {
                // Применяем регулярное выражение, чтобы обрезать путь до /upload
                $pattern = '/\/upload.*/';
                if (preg_match($pattern, $this->fileArray['tmp_name'], $matches)) {
                    // Обновляем путь в tmp_name, начиная с /upload
                    $this->fileArray['tmp_name'] = $matches[0];

                }

            }
            $this->pathSrcArr[$picture] = $this->fileArray;

        }

        return $this->pathSrcArr;

    }


    private function isShow()
    {

        if(
           !empty($item->thumbListing)
        ){
            return false;
        }

        return true;
    }
}
