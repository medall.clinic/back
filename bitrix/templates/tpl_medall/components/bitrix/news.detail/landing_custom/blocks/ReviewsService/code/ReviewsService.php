<?php
namespace Medreclama\Landing;
class ReviewsService extends Landing
{
    public $titleMenu;
    public array $arFilterReview;
    public array $arFieldsReview = [];
    public array $arSelectReview;
    public array $arFieldsDoctors = [];
    public array $arFilterDoctors;
    public array $arSelectDoctors;
    public object $resReview;
    public object $resDoctor;
    public object $resServiceDevelop;
    public object $resServiceProduction;

    // Новые свойства для услуг
    public array $arFieldsService = [];
    public array $arSelectService = ['NAME', 'ID', 'DETAIL_PAGE_URL'];
    public array $arFilterServiceProduction = ['IBLOCK_ID' => 47, 'ACTIVE' => 'Y', '!=PROPERTY_703_VALUE' => 'Да'];
    public array $arFilterServiceDevelop = ['IBLOCK_ID' => 47, 'ACTIVE' => 'Y', '=PROPERTY_703_VALUE' => 'Да'];
    public array $arrIdService;
    public array $arrIdDoctor;

    public  array $old_id;
    public array $serviceCheck;
    public array $uniqueReviews = [];
    public array $idCount = [];
    public  $reviewsIdBlock;
    /**
     * @var mixed
     */
    public $statusContext;


    public function __construct($arResult)
    {

        parent::__construct($arResult);
         $this->titleMenu = 'jhjhj';
        $this->arFilterReview = ['IBLOCK_ID' => 17, 'ACTIVE' => 'Y', '=PROPERTY_SERVICE'=> $arResult['ID']];
        $this->arSelectReview = [
            'ID',
            'NAME',
            'PROPERTY_RATING',
            'PREVIEW_TEXT',
            'PROPERTY_DOCTOR_REVIEW',
            'PROPERTY_SERVICE',
            "PROPERTY_FEEDBACK_PLATFORM",
            "PROPERTY_RATING_LABEL",
            "PROPERTY_NAME_REVIEW",
            'DATE_ACTIVE_FROM',
        ];
        $this->resReview = \CIBlockElement::GetList([], $this->arFilterReview, false, false, $this->arSelectReview);
        $this->loadReviews();
        $this->loadServices();
        $this->loadDoctors();
        $this->processReviews($arResult);
        $this->reviewsIdBlock = $this->landingProps['REVIEWS_BLOCK_ID']['VALUE'];
        $this->statusContext = $this->landingProps['STATUS_CONTEXT']['VALUE'];


        $this->isShow = $this->isShow();

    }

    public function loadReviews()
    {
        while ($obj = $this->resReview->GetNext()) {
            $this->arFieldsReview[] = $obj;
        }

        return $this->arFieldsReview;
    }

    public function loadServices()
    {
            $this->resServiceDevelop = \CIBlockElement::GetList([], $this->arFilterServiceDevelop, false, false, $this->arSelectService);
            $this->resServiceProduction = \CIBlockElement::GetList([], $this->arFilterServiceProduction, false, false, $this->arSelectService);

        if (!empty($this->resServiceDevelop) && $_SERVER['REQUEST_URI'] != '/review/') {
            while ($arResService = $this->resServiceDevelop->GetNext()) {
                $this->arFieldsService[] = $arResService;
            }

        }
        if (!empty($this->resServiceProduction)){
            while ($arResService = $this->resServiceProduction->GetNext()) {
                $this->arFieldsService[] = $arResService;
            }
        }


        foreach ($this->arFieldsService as $service) {
            if (!empty($service['NAME'])) {
                $this->arrIdService[$service['ID']] = $service['NAME'];
                $this->arrIdService['URL'][$service['ID']] = $service['DETAIL_PAGE_URL'];
            }
        }

        return $this->arFieldsService;
    }

    public function loadDoctors()
    {
        $this->arSelectDoctors = ['NAME', 'ID', 'DETAIL_PAGE_URL', 'PROPERTY_IO_DOCTOR'];
        $this->arFilterDoctors = ['IBLOCK_ID' => 6, 'ACTIVE' => 'Y'];

        $this->resDoctor = \CIBlockElement::GetList([], $this->arFilterDoctors, false, false, $this->arSelectDoctors);
        while ($arResDoctor = $this->resDoctor->GetNext()) {
            $this->arFieldsDoctors[] = $arResDoctor;
        }

        foreach ($this->arFieldsDoctors as $doctor) {
            $this->arrIdDoctor[$doctor['ID']] = $doctor['NAME'] . ' ' . $doctor['PROPERTY_IO_DOCTOR_VALUE'];
            $this->arrIdDoctor['URL'][$doctor['ID']] = $doctor['DETAIL_PAGE_URL'];
        }
    }



    public function processReviews(&$arResult): array
    {
        foreach ($this->arFieldsReview as $key => $arField) {
                if ($arField['~PROPERTY_DOCTOR_REVIEW_VALUE']) {
                    $arResult["PROPERTIES"]["feedback"]["VALUE"][$arField['ID']] = $arField['ID'];
                }

                if (!empty($arField['~PROPERTY_SERVICE_VALUE'])) {
                    $this->serviceCheck[] = $this->arrIdService[$arField['~PROPERTY_SERVICE_VALUE']];
                    $arResult['SERVICE_NAME'][] = $this->arrIdService[$arField['~PROPERTY_SERVICE_VALUE']];
                    $arResult['SERVICE_URL'][] = $this->arrIdService['URL'][$arField['~PROPERTY_SERVICE_VALUE']];
                }

                if (!empty($arField['~PROPERTY_DOCTOR_REVIEW_VALUE'])) {
                    $arResult['DOCTOR_NAME'][$arField['ID']][] = $this->arrIdDoctor[$arField['~PROPERTY_DOCTOR_REVIEW_VALUE']];
                    $arResult['DOCTOR_URL'][$arField['ID']][] = $this->arrIdDoctor['URL'][$arField['~PROPERTY_DOCTOR_REVIEW_VALUE']];
                }
                if (!empty($arField['~PROPERTY_NAME_REVIEW_VALUE'])) {
                    $arResult['NAME_REVIEW'][$arField['ID']][] = $arField['~PROPERTY_NAME_REVIEW_VALUE'];
                }

                if (!empty($arField['~PROPERTY_RATING_VALUE'])) {
                    $arResult['RATING'][] = $arField['PROPERTY_RATING_VALUE'];
                }

            if (!empty($arField['~PROPERTY_RATING_LABEL_VALUE'])) {
                $arResult['FEEDBACK_PLATFORM'][$arField['ID']] = $arField['~PROPERTY_RATING_LABEL_VALUE'];
            }
            if (!empty($arField['DATE_ACTIVE_FROM'])) {
                $arResult['DATE_TIME'][] = $arField['DATE_ACTIVE_FROM'];
            }

            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], '2GIS') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://2gis.ru/spb/firm/70000001035838469?m=30.285963%2C59.966204%2F16';
            }
            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'Яндекс Карты') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://yandex.ru/maps/org/medall/35461995871/?ll=30.285847%2C59.966174&z=17';
            }
            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'Google maps') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://www.google.com/maps/place/Клиника+MEDALL/@59.966168,30.2832555,17z/data=!4m8!3m7!1s0x469631cd7ce1a2bb:0x9e8f4e1bbc2adb66!8m2!3d59.966168!4d30.2858304!9m1!1b1!16s%2Fg%2F1v4k559y?entry=ttu';
            }
            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'docdoc.ru') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://spb.docdoc.ru/clinic/klinika_cifrovoj_stomatologii_medall';
            }
            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'НаПоправку') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://spb.napopravku.ru/clinics/medall-centry-esteticheskoy-mediciny-i-cifrovoy-stomatologii/';
            }
            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'ПроДокторов') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://prodoctorov.ru/spb/lpu/63649-medall/';
            }
            if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'Докту') === 0) {
                $arResult['URL'][$arField['ID']] = 'https://doctu.ru/spb/net/medall';
            }

        }







// Возвращаем собранные данные после завершения цикла
        return [
            $arResult["PROPERTIES"]["feedback"]["VALUE"],
            $arResult['SERVICE_NAME'],
            $arResult['SERVICE_URL'],
            $arResult['DOCTOR_NAME'],
            $arResult['DOCTOR_URL'],
            $arResult['FEEDBACK_PLATFORM'],
            $arResult['RATING'],
            $arResult['DATE_TIME'],
        ];

    }

    public function isShow()
    {
        if(
            !$this->arFieldsReview
        ){

            return false;
        }

        return true;
    }





}
