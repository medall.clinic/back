<?php /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="slide swiper-slide home-staff__slide">

    <?php if($doctor->image) { ?>

        <?php if($doctor->hasDetails()) { ?>
            <a href="#doctor-<?php echo $doctor->id ?>" class="home-staff__image" data-modal>
                <?php include __DIR__."/photo.php" ?>
                <button
                        class="button button--size--small button--icon button--icon--dots home-staff__more"
                        type="button"
                ></button>
            </a>
        <?php } elseif($doctor->url) { ?>
            <a href="<?php echo $doctor->url ?>" class="home-staff__image">
                <?php include __DIR__."/photo.php" ?>
            </a>
        <?php } else { ?>
            <div class="home-staff__image"><?php include __DIR__."/photo.php" ?></div>
        <?php } ?>

    <?php } ?>
    <a class="home-staff__inner" href="<?php echo $doctor->url ?>">
        <?php if (!empty($doctor->name)) {?>
            <div class="home-staff__name"><?php echo $doctor->name ?></div>

        <?php } ?>

        <?php if (!empty($doctor->description)) {?>
            <div class="home-staff__description"><?php echo $doctor->description?></div>

        <?php } ?>




    </a>

    <?php if(!empty($doctor->link)) { ?>
        <?php if(empty($doctor->shortLink)) { ?>
            <div class="banner-team__button">
                <a class="btn" href="<?php echo $doctor->link ?>">Подробнее</a>
            </div>
        <?php } ?>
    <?php } else { ?>
        <a class="button button--size--small home-staff__button" href="#landing-form">Записаться</a>
    <?php } ?>

</div>
