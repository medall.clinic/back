<?php /** @var $stages Medreclama\Landing\Stages */ ?>
<?php /** @var $stage Medreclama\Landing\Stage */ ?>
<?php /** @var $finalStage Medreclama\Landing\Stage */ ?>
<?php /** @var $button Medreclama\Landing\Button */ ?>
<?php $finalStage = $stages->finalStage ?>
<?php $button = $stages->button ?>
<?php if ($stages->newTemplateStage == 'Да') {?>
    <section class="container page-section page-section--background-color landing-stages">
        <div class="content">
            <?php if (!empty($stages->title)){?>
                <h2 class="page-subsection text-align-center"><?php echo $stages->title?></h2>

            <?php }?>
            <div class="landing-stages__list page-subsection">
                <?php foreach ($stages->items as $item){?>
                    <div class="landing-stages__item">
                        <?php if (!empty($item->image)){?>
                            <picture>
                                <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($item->image, [ 800 ])?>" type="image/webp">
                                <source srcset="<?php echo $item->image ?>"><img class="landing-stages__image" src="" loading="lazy" decoding="async" alt="<?php echo $item->text ?>">
                            </picture>
                        <?php }?>

                        <div class="landing-stages__inner">
                            <h3 class="landing-stages__header"><?php echo $item->title ?></h3>
                            <div class="landing-stages__body"><?php echo $item->text ?></div>
                        </div>
                    </div>
                <?php }?>


            </div>
        </div>
    </section>
<?php }elseif ($stages->newTemplateStage != 'Да'){?>
    <section class="container page-section page-section--text--align--center"
        <?php if (!empty($stages->blockId)){?>
            id="<?php echo $stages->blockId ?>"
        <?php } ?>>

        <div class="content">

            <?php if($stages->title): ?>
                <h2 class="page-subsection text-align-center"><?= $stages->title ?></h2>
            <?php endif ?>

            <?php if($stages->items): ?>
                <?php if($stages->isAllStagesWithTitle): ?>
                    <ol class="grid grid--no-gap landing-sequence page-subsection">
                        <?php foreach ($stages->items as $stage): ?>
                            <?php include __DIR__."/stage.php" ?>
                        <?php endforeach ?>
                    </ol>
                <?php else: ?>
                    <div class="page-subsection grid grid--justify--center">
                        <ol class="grid grid--no-gap landing-sequence grid__cell grid__cell--m--10 grid__cell--xs--12">
                            <?php foreach ($stages->items as $stage): ?>
                                <?php include __DIR__."/stage.php" ?>
                            <?php endforeach ?>
                        </ol>
                    </div>

                <?php endif ?>
            <?php endif ?>

            <?php if($finalStage->text || $finalStage->image): ?>
                <?php include __DIR__."/final-stage.php" ?>
            <?php endif ?>

            <?php if($button->isExist()): ?>
                <div class="page-subsection grid grid--justify--center">
                    <?= $button->render('grid__cell') ?>
                </div>
            <?php endif ?>

        </div>
    </section>

<?php }?>
