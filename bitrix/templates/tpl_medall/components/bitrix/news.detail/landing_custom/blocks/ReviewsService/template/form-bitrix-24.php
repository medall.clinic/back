<? /** @var $banner Medreclama\Landing\ReviewsService */ ?>
<div class="landing-header__form" id="landing-header-form">
	<form class="form form-ajax">

		<h2 class="h4 form__header hidden-m hidden-s hidden-xs">Оставьте контактные данные и мы обязательно свяжемся с вами</h2>

		<label class="form-input form-input--text form__input form__input">
			<input class="form-input__field" type="text" name="name" required placeholder="ФИО *">
		</label>

		<label class="form-input form-input--tel form__input form__input">
			<input class="form-input__field" type="tel" name="phone" required placeholder="Телефон *">
		</label>

		<label class="form-input form-input--textarea form__input form__input">
			<textarea class="form-input__field" name="comments" placeholder="Комментарий"></textarea>
		</label>

		<div class="form__agreement">Нажимая кнопку «Отправить» я даю согласие на <a href="/">обработку персональных данных</a>.</div>

		<button class="button form__submit" type="submit">Отправить</button>

        <input type="hidden" name="title" value="<?= $banner->formTitle ?>">

		<? if($banner->utmTags): ?>
			<? foreach ($banner->utmTags as $utmTag): ?>
                <input type="hidden" name="<?= $utmTag->name ?>" value="<?= $utmTag->value ?>">
			<? endforeach ?>
		<? endif ?>

	</form>
</div>
