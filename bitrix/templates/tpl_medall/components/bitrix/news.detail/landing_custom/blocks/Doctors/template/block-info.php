<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="grid__cell grid__cell--m--5 grid__cell--xs--12">
    <div class="landing-team__info">

        <div class="landing-team__name"><?= $doctor->name ?></div>

		<? if($doctor->description): ?>
            <div class="landing-team__description"><?= $doctor->description ?></div>
		<? endif ?>

        <? if($doctor->workExperience): ?>
            <div class="landing-team__experience">Стаж работы <?= $doctor->workExperience ?></div>
		<? endif ?>

        <? if($doctor->detailImage): ?>
            <div class="landing-team__image">
                <img src="<?= $doctor->detailImage ?>" alt="<?= $doctor->name ?>">
            </div>
		<? endif ?>

    </div>
</div>