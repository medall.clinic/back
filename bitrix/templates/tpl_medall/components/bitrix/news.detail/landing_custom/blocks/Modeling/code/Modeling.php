<?
namespace Medreclama\Landing;

class Modeling extends Landing
{
	public $text;
	public $images;
	public $oldButton;
    public $template;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["MODELING_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["MODELING_BLOCK_ID"]);
		$this->text = $this->landingProps["MODELING_TEXT"]["~VALUE"] ? $this->landingProps["MODELING_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->images = $this->getImages($this->landingProps["MODELING_IMAGES"]["VALUE"]);
		$this->button = new Button($this->landingProps["MODELING_BUTTON_TEXT"], $this->landingProps["MODELING_BUTTON_LINK"], $this->landingProps["MODELING_BUTTON_FANCYBOX"]);
		$this->oldButton = $this->landingProps["MODELING_BUTTON"]["~VALUE"];
        $this->template = $this->landingProps['NEW_MODELING_TEMPLATE']['VALUE'];

		$this->additionalText = $this->landingProps["MODELING_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["MODELING_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["MODELING_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["MODELING_MENU_TITLE"]["VALUE"], $this->landingProps["MODELING_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(
			!$this->title &&
			!$this->text &&
			!$this->images
		){
			return false;
		}

		return true;
	}

}
