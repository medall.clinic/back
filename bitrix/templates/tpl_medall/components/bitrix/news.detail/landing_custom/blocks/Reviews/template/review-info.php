<? /** @var $review Medreclama\Landing\Review */ ?>
<div class="landing-stories__info">

    <? if($review->name): ?>
        <div class="landing-stories__name"><?= $review->name ?></div>
    <? endif ?>

    <? if($review->description): ?>
        <div class="landing-stories__description"><?= $review->description ?></div>
    <? endif ?>

</div>