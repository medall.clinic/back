<? /** @var $general Medreclama\Landing\General */ ?>
<? if($general->isSingleImage()): ?>
    <? $image = $general->getSingleImage() ?>
    <div class="landing-block__image grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--9">
        <picture>
            <source srcset="<?= WebPHelper::getOrCreateWebPUrl($image, [ 800 ]) ?>" type="image/webp"/>
            <img src="<?= $image ?>" loading="lazy" decoding="async" alt="" title=""/>
        </picture>
    </div>
<? else: ?>
    <div class="landing-block__image landing-block__image--slider grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12 ">
        <div class="slider slider--full-width landing-block__slider">
            <div class="slider__slides swiper">

                <div class="slider__wrapper swiper-wrapper">
					<? foreach ($general->images as $image): ?>
                    <div class="slide swiper-slide landing-block__slide">
                        <picture>
                            <source srcset="<?= WebPHelper::getOrCreateWebPUrl($image, [ 800 ]) ?>" type="image/webp"/>
                            <img src="<?= $image ?>" loading="lazy" decoding="async" alt="" title=""/>
                        </picture>
                    </div>
					<? endforeach ?>
                </div>

                <div class="slider__arrow slider__arrow--prev"></div>
                <div class="slider__arrow slider__arrow--next"></div>
                <div class="slider__pagination"></div>

            </div>
        </div>
    </div>
<? endif ?>
