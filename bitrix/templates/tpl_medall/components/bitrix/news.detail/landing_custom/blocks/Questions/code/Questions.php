<?
namespace Medreclama\Landing;

class Questions extends Landing
{
	public $items;
	public $image;
    public $link;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["QUESTIONS_TITLE"]["~VALUE"];
        $className = trim($this->landingProps["COLOR_TITLE"]["~VALUE"]);
        if ($className == 'светлый'){
            $this->classTitle = 'questions-header--background-color--light';
        }elseif($className == 'тёмный'){
            $this->classTitle = 'questions-header--background-color--dark';
        } else {
            $this->classTitle = 'questions-header--background-color--dark';
        }


		$this->blockId = $this->getBlockId($this->landingProps["QUESTIONS_BLOCK_ID"]);
		$this->items = $this->getItems($this->landingProps["QUESTIONS_ITEMS"]["VALUE"]);
		$this->image = $this->getImageSrc($this->landingProps["QUESTIONS_IMAGE"]["VALUE"]);
		$this->button = new Button($this->landingProps["QUESTIONS_BUTTON_TEXT"], $this->landingProps["QUESTIONS_BUTTON_LINK"], $this->landingProps["QUESTIONS_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["QUESTIONS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["QUESTIONS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["QUESTIONS_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["QUESTIONS_MENU_TITLE"]["VALUE"], $this->landingProps["QUESTIONS_MENU_LINK"]["VALUE"], $this->blockId);
        $this->link = $this->landingProps["QUESTIONS_LIST"]['VALUE'];

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Question;
			$item->text = $element->fields["NAME"];
			$item->answer = $element->fields["~PREVIEW_TEXT"];

			$items[] = $item;
		}

		return $items;
	}

	private function isShow()
	{
		if(!$this->items){
			return false;
		}

		return true;
	}


}
