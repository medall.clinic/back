<?php
/** @var $operations Medreclama\Landing\Operations */

?>

<section
        class="container page-section page-section--background-color page-section--background-color--light landing-block">
    <div class="content grid grid--justify--space-between">
        <div class="grid__cell grid__cell--xs--12 landing-block__info">
            <h2 class="landing-block__header text-align-center"><?php echo $operations->title ?></h2>
            <div class="landing-block__body">
                <div class="page-subsection">
                    <?php
                    if (!empty($operations->description)) {
                        ?>
                        <p><?php echo $operations->description ?></p>

                    <?php } ?>
                </div>
                <?php foreach ($operations->items as $key => $operation) { ?>


                    <div class="page-subsection landing-block-section-h3">
                        <h3 class="text-align-center landing-block-section-h3__title"><?php echo $operation->title_h3 ?></h3>
                        <div class="slider landing-block-section-h3__body">
                            <div class="slider__slides swiper">
                                <div class="slider__wrapper swiper-wrapper">
                                    <?php foreach ($operation->thumbListing as $v => $item) { ?>
                                        <?php if (array_key_exists($operation->thumbListing[$v], $operations->pathSrcArr)) { ?>
                                            <?php $x = $operation->thumbListing[$v] ?>

                                        <?php } ?>

                                        <div class="slide swiper-slide grid grid--justify--space-between landing-block-section-h3__slide">
                                            <?php if (!empty($operation->thumbListing[$v])) { ?>
                                                <div class="grid__cell grid__cell--m--5 grid__cell--s--6 grid__cell--xs--9 landing-block-section-h3__image">
                                                    <picture>
                                                        <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($operations->pathSrcArr[$x]['tmp_name'], [800]) ?>"
                                                                type="image/webp"/>
                                                        <source srcset="<?php echo $operations->pathSrcArr[$x]['tmp_name'] ?>"/>
                                                        <img src="<?php echo $operations->pathSrcArr[$x]['tmp_name'] ?>"
                                                            loading="lazy"
                                                            decoding="async" alt="<?php echo $item ?>"/>
                                                    </picture>
                                                </div>
                                            <?php } elseif (empty($operation->thumbListing[$v])) { ?>
                                                <div class="grid__cell grid__cell--m--5 grid__cell--s--6 grid__cell--xs--9 landing-block-section-h3__image">
                                                    <picture>
                                                        <source srcset="https://dummyimage.com/800x392/D9D9D9/D9D9D9.jpg"
                                                                type="image/webp"/>
                                                        <source srcset="https://dummyimage.com/800x392/D9D9D9/D9D9D9.jpg"/>
                                                        <img src="https://dummyimage.com/800x392/D9D9D9/D9D9D9.jpg"
                                                            loading="lazy"
                                                            decoding="async" alt="<?php echo $item ?>"/>
                                                    </picture>
                                                </div>
                                            <? } ?>
                                            <div class="grid__cell grid__cell--s--6 landing-block-section-h3__text">
                                                <?if (!empty($operation->sub_items[$v])){?>
                                                    <h4><?php echo $operation->sub_items[$v] ?></h4>

                                                <?}?>
                                                <?if (!empty($operation->descriptionListing[$v]['TEXT'] )){?>
                                                    <div><?php echo $operation->descriptionListing[$v]['TEXT'] ?></div>


                                                <?}?>
                                            </div>
                                        </div>

                                    <?php } ?>


                                </div>
                                <div class="slider__pagination"></div>
                            </div>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
</section>



