<?
namespace Medreclama\Landing;

class General extends Landing
{
    public $images;
    public $text;
    public $isImagesOrFormOnLeft;
    public $isFormEnabled;
    public  $hiddenText;
    public  $nameElement;
    public  $recommedationList;




    public  $postfix = '&js_api=1';
    public array $pathSrcArr = [];

    public $isColored = false;
    public $isColorLight = false;
    public $isColorDark = false;
    public $addClass = '';
    public array $fileArray = [];
    public $expand_block;
    public $recommedationTitle;


    public $formUtmTags;
    public $formTitle;

    public $additionalText;
    public $linkYoutube = '';
    public $previewText;

    public function __construct($elementId)
    {
        $element = $this->getElement($elementId);

        if (!$element) {
            return null;
        }
        $this->title = $element->props["TITLE"]["~VALUE"];
        $this->previewText = $element->fields["PREVIEW_TEXT"];
        $this->nameElement = $element->fields["NAME"];
        $this->recommedationTitle = $element->props["RECOMMENDATION_TITLE"]['VALUE'];
        $this->recommedationList = $element->props["RECOMMENDATION_LIST"]['VALUE'];
        $this->linkYoutube = $element->props["LINK_YOUTUBE"]["~VALUE"];
        $this->text = $element->props["TEXT"]["~VALUE"] ? $element->props["TEXT"]["~VALUE"]["TEXT"] : false;
        $this->blockId = $this->getBlockId($element->props["BLOCK_ID"]);
        $this->menuItem = new MenuItem($this->landingProps["MENU_TITLE"]["VALUE"], $this->landingProps["MENU_LINK"]["VALUE"], $this->blockId);
        $this->images = $this->getImages($element->props["IMAGES"]["VALUE"]);
        $this->isImagesOrFormOnLeft = $element->props["IMAGES_OR_FORM_POSITION"]["VALUE"] != "справа";
        $this->isFormEnabled = (bool)$element->props["IS_FORM_ENABLED"]["VALUE"];
        $this->expand_block = $element->props['EXPAND_BLOCK'];
        $this->isColored = (bool)$element->props["BG_COLOR"]["VALUE"];
        $this->isColorLight = $element->props["BG_COLOR"]["VALUE"] == 'светлый';
        $this->isColorDark = $element->props["BG_COLOR"]["VALUE"] == 'тёмный';
        $this->addClass = $element->props["ADD_CLASS"]["~VALUE"] ?: '';
        $this->formUtmTags = $this->getFormUtmTags();
        $this->formTitle = $this->getFormTitle($element->fields["NAME"]);
        $this->button = new Button($element->props["BUTTON_TEXT"], $element->props["BUTTON_LINK"], $element->props["BUTTON_FANCYBOX"]);
        $this->additionalText = $this->getAdditionalText($element->props["ADDITIONAL_TEXT"]);
        $this->hiddenText = $element->props['HIDDEN_BLOCK']['VALUE']['TEXT'];



        $this->isShow = $this->isShow();
        return  true;
    }


    public function isSingleImage(): bool
    {
        if(!$this->images){
            return false;
        }

        return count($this->images) == 1;
    }

    public function getSingleImage(): string
    {
        return reset($this->images);
    }

    private function getFormUtmTags(): array
    {
        if(empty($_GET)){
            return [];
        }

        $utmTags = [];
        foreach ($_GET as $paramName => $paramValue){
            if(strpos($paramName, 'utm_') !== false){
                $utmTags[] = new UtmTag($paramName, $paramValue);
            }
        }

        return $utmTags;
    }

    private function getFormTitle($name): string
    {
        global $APPLICATION;
        $curPage = $APPLICATION->GetCurPage();

        $title = (strpos($curPage, "landings") !== false) ? "лендинг" : "страница услуги";

        $title .= " - ".$curPage;
        $title .= ": ";
        $title .= "Блок страницы - ".$name;

        return $title;
    }

    private function getAdditionalText(array $prop): string
    {
        return $prop["~VALUE"] ? $prop["~VALUE"]["TEXT"] : "";
    }

    private function isShow(): bool
    {


        // Если все остальные поля пусты, возвращаем false
        if (empty($this->nameElement)) {
            return false;
        }

        // Если хотя бы одно из полей заполнено, возвращаем true
        return true;
    }


}
