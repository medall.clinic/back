<? /** @var $work Medreclama\Landing\Work */ ?>

<div class="slide swiper-slide landing-works__slide">
    <div class="works-item">
        <div class="image-compare">
            <img src="<?php echo $work->photoBefore?>" alt="<?php echo $work->title?>" loading="lazy" decoding="async" width="720" height="720">
            <img src="<?php echo $work->photoAfter?>" alt="<?php echo $work->title?>" loading="lazy" decoding="async" width="720" height="720">
        </div>
        <div class="works-item__service"> <?php echo $work->title?></div>
        <div class="works-item__doctor" >
            <?php echo $work->doctor['PREVIEW_TEXT']?>
            <a href="<?php echo $work->doctor['DETAIL_PAGE_URL']?>"><?php echo $work->doctor['NAME']?></a>
        </div>
    </div>
</div>
