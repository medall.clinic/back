<?php /** @var $reason Medreclama\Landing\Reason */ ?>
<?php /** @var $reasons Medreclama\Landing\Reasons */ ?>
<?php /** @var $button Medreclama\Landing\Button */ ?>
<?php $button = $reasons->button ?>
<?php
if ($reasons->newsTemplate == 'Да'){
 if (!empty($reasons->listItemsPros)) { ?>

    <section class="container page-section landing-block">
        <div class="content grid grid--justify--space-between">
            <div class="grid__cell grid__cell--xs--12 landing-block__info">
                <h2 class="landing-block__header text-align-center"><?php echo $reasons->title ?></h2>
                <div class="landing-block__body">
                    <div class="grid grid--justify--center grid--align-items--flex-start page-subsection"> 
                        <ul class="grid__cell grid__cell--m--10 landing-block__list">
                            <?php foreach ($reasons->listItemsPros as $item) { ?>

                                <li class="landing-block__item"><?php echo $item; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
 if (!empty($reasons->listItemsCons)) {
    $reasons->title = preg_replace('/\bПоказания\b/iu', 'Противопоказания', $reasons->title);
    ?>
    <section class="container page-section landing-block">
        <div class="content grid grid--justify--space-between">
            <div class="grid__cell grid__cell--xs--12 landing-block__info">
                <h2 class="landing-block__header text-align-center"><?php echo $reasons->title ?></h2>
                <div class="landing-block__body">
                    <div class="grid grid--justify--center grid--align-items--flex-start page-subsection"> 
                        <ul class="grid__cell grid__cell--m--10 landing-block__list">
                                <?php foreach ($reasons->listItemsCons as $item) { ?>
                                    <li class="landing-block__item"><?php echo $item; ?></li>
                                <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }
}

if ($reasons->newsTemplate != 'Да'){?>
    <section class="container page-section page-section--reasons"
        <?php if (!empty($reasons->blockId)){?>
            id="<?php echo $reasons->blockId ?>"
        <?php } ?>>

        <div class="content">
            <?php if($reasons->title){ ?>
                <h2 class="page-subsection text-align-center"><?php echo $reasons->title ?></h2>
            <?php } ?>



            <?php if($reasons->items){ ?>
                <ul class="grid grid--no-gap landing-reasons page-subsection">
                    <?php foreach ($reasons->items as $reason){ ?>
                        <?php include __DIR__."/reason.php" ?>
                    <?php } ?>
                </ul>
            <?php } ?>
            <?php if($reasons->hasListItems()){ ?>
                <div class="page-subsection">
                    <div class="grid grid--justify--center">

                        <?php if($reasons->listItemsPros){ ?>
                            <div class="grid__cell grid__cell--l--5 grid__cell--xs--12">
                                <div class="indications">
                                    <h3 class="indications__title">Показания</h3>
                                    <ul class="list-unordered indications__list">
                                        <?php foreach ($reasons->listItemsPros as $listItemPro){ ?>
                                            <li class="list-unordered__item indications__item"><?php echo $listItemPro ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if($reasons->listItemsCons){ ?>
                            <div class="grid__cell grid__cell--l--5 grid__cell--xs--12">
                                <div class="indications indications--contraindications">
                                    <h3 class="indications__title">Противопоказания</h3>
                                    <ul class="list-unordered indications__list">
                                        <?php foreach ($reasons->listItemsCons as $listItemCon){ ?>
                                            <li class="list-unordered__item indications__item"><?php echo $listItemCon ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if($button->isExist()): ?>
                <div class="page-subsection grid grid--justify--center">
                    <?= $button->render('grid__cell') ?>
                </div>
            <?php endif ?>

        </div>
    </section>
<?php }?>

