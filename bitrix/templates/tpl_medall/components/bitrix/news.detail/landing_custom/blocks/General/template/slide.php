<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="slide swiper-slide home-staff__slide">

	<? if($doctor->image): ?>

		<? if($doctor->hasDetails()): ?>
            <a href="#doctor-<?= $doctor->id ?>" class="home-staff__image" data-modal>
				<? include __DIR__."/photo.php" ?>
                <button
                    class="button button--size--small button--icon button--icon--dots home-staff__more"
                    type="button"
                ></button>
            </a>
		<? elseif($doctor->url): ?>
            <a href="<?= $doctor->url ?>" class="home-staff__image">
				<? include __DIR__."/photo.php" ?>
            </a>
		<? else: ?>
			<div class="home-staff__image"><? include __DIR__."/photo.php" ?></div>
		<? endif ?>

	<? endif ?>

	<a href="<?= $doctor->url ?>" class="home-staff__name"><?= $doctor->name ?></a>
	<div class="home-staff__description"><?= $doctor->description ?></div>

	<? if(!empty($doctor->link)): ?>
		<? if(empty($doctor->shortLink)): ?>
            <div class="banner-team__button">
                <a class="btn" href="<?= $doctor->link ?>">Подробнее</a>
            </div>
		<? endif ?>
	<? else: ?>
        <a class="button button--size--small home-staff__button" href="#landing-form">Записаться</a>
	<? endif ?>

</div>
