<?php /** @var $general Medreclama\Landing\General */ ?>
<?php
$classes = '';

if (!empty($this->linkYoutube)) {
    $classes .= 'landing-video ';
}
if ($this->expand_block['~VALUE'] == 'Да'){
    $classes .= 'landing-block--drop-down';

}

if ($general->isColorDark) {
    $classes .= 'page-section--background-color page-section--background-color--dark ';
}

if ($general->isColorLight) {
    $classes .= 'page-section--background-color page-section--background-color--light ';
}

$classes .= $general->addClass;
?>
<?php if (!empty($general->text) || !empty($general->linkYoutube) || !empty($general->recommedationList)){?>
    <div class="container page-section landing-block <?= $classes ?>" <?php if (!empty($general->blockId)) { ?>id="<?= $general->blockId ?>"<?php } ?>>
        <div class="content grid grid--justify--space-between">
            <?php if ($general->images) {
                if ($this->isImagesOrFormOnLeft) {
                    if ($general->isFormEnabled) {
                        include __DIR__ . "/form.php";
                        include __DIR__ . "/text-narrow.php";
                    } else {
                        include __DIR__ . "/images-desktop.php";
                        include __DIR__ . "/text-narrow.php";
                    }
                } else {
                    if ($general->isFormEnabled) {
                        include __DIR__ . "/text-narrow.php";
                        include __DIR__ . "/form.php";
                    } else {
                        include __DIR__ . "/text-narrow.php";
                        include __DIR__ . "/images-desktop.php";
                    }
                }
            } else {
                include __DIR__ . "/text-wide.php";
            } ?>
        </div>
    </div>

<?php }?>

