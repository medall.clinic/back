<? /** @var $form Medreclama\Landing\Form */ ?>
<? /** @var $utmTag Medreclama\Landing\UtmTag */ ?>
<section class="container page-section"
         <?php if (!empty($form->blockId)){?>
         id="<?= $form->blockId ?>"
<?php } ?>>
    <div class="content">
        <div class="page-subsection grid grid--align-items--center grid--justify--center">

            <div class="grid__cell grid__cell--xs--12">
                <? /* <h2 class="gift">Дарим подарок <small>за подписку на наш Instagram аккаунт <a href="https://instagram.com/medall.clinic ">@medall.clinic</a></small></h2> */?> 
                <h2 class="page-section__title text-align-center">Записаться на приём</h2>
                <p class="page-section__subtitle">В комментариях укажите, какая услуга, доктор или вопрос Вас интересует.</p>
            </div>

            <?/*  <div class="grid__cell grid__cell--s--6 grid__cell--xs--12">
                <picture>
                    <source srcset="/images/gift.webp" type="image/webp"/>
                    <img class="hidden-s hidden-xs" src="/images/gift.png" loading="lazy" decoding="async" alt=""/>
                </picture>
            </div> */?>

			<? if(!empty($form->script)): ?>
				<?= $form->script ?>
			<? else: ?>
				<? include __DIR__."/form-bitrix-24.php" ?>
			<? endif ?>

        </div>
    </div>
</section>
