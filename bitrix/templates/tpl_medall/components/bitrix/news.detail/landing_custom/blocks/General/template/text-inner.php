<?php
/** @var $general Medreclama\Landing\General */
/** @var $button Medreclama\Landing\Button */

$button = $general->button;
$url = $general->linkYoutube;
$short_text = '';
$remaining_text = '';

$linkYoutube = $url;

// Check if there is an expandable block for the text
if (!empty($this->expand_block)) {
    // Get the first 230 characters of the text
    $short_text = htmlspecialcharsBack($general->text);
    // Get the remaining part of the text
    $remaining_text = htmlspecialcharsBack($general->hiddenText);
}
?>

<?php if (!empty($general->text) && !empty($general->expand_block['~VALUE']) && empty($general->linkYoutube) && empty($general->recommedationList)) {
    // блок блок сворачивания
    ?>
    <?php if (!empty($general->title)) { ?>
        <h2 class="landing-block__header<?php if (empty($this->image)) { echo ' text-align-center'; } ?>">
            <?php echo $general->title; ?>
        </h2>
    <?php } ?>

    <div class="landing-block__body">
        <?php if (empty($general->expand_block['~VALUE'])) { ?>
            <?php echo $general->text; ?>
        <?php } elseif (!empty($general->expand_block['~VALUE'])) { ?>
            <div class="landing-block__top">
                <?php echo $short_text; ?>
            </div>
            <div class="landing-block__bottom">
                <?php echo $remaining_text; ?>
            </div>
        <?php } else { ?>
            <?php echo $general->text; ?>
        <?php } ?>
    </div>
<?php }

if (!empty($general->text) && empty($general->expand_block['~VALUE']) && empty($general->linkYoutube) && empty($general->recommedationList)) {
    // еслли есть текст пустая галочка разворачивания блока и пустой ютуб обычный
    ?>
    <?php if (!empty($general->title) && empty($general->images)) { ?>
        <h2 class="landing-block__header text-align-center">
            <?php echo $general->title; ?>
        </h2>
    <?php }elseif (!empty($general->title) && !empty($general->images)) {?>
        <h2 class="landing-block__header">
            <?php echo $general->title; ?>
        </h2>
    <?php }?>
    <div class="grid grid--justify--center page-subsection">
        <?php if (!empty($general->text)){?>
            <div class="landing-block__body"><?php echo $general->text; ?></div>

        <?php }?>
    </div>
<?php } ?>

<?php if (!empty($general->recommedationList)) { ?>
    <h2 class="landing-block__header text-align-center page-subsection">
        <?php echo $general->title; ?>
    </h2>
    <?php if (!empty($general->text)){?>
        <div class="page-subsection">
            <p><?php echo $general->text?></p>
        </div>
    <?php }?>

    <div class="page-subsection">


        <div class="landing-block__body">
            <div class="grid grid--justify--center grid--align-items--flex-start page-subsection"> 
                <ul class="grid__cell grid__cell--m--10 landing-block__list">
                    <?php foreach ($general->recommedationList as $item) { ?>
                        <?php if (!empty($item)) { ?>
                            <li class="landing-block__item"><?php echo $item ?></li>

                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            <?php if (!empty($general->recommedationTitle)){?>
                <div class="page-subsection">

                    <p><?php echo $general->recommedationTitle?></p>

                </div>

            <?php }?>

        </div>
    </div>
<?php } ?>


<?php if (!empty($general->linkYoutube)) {
    if (count($general->linkYoutube) == 1) { ?>
        <?php if ($general->title){?>
            <h2 class="landing-block__header<?php if (empty($general->images)) { echo ' text-align-center'; } ?>">
                <?php echo $general->title; ?>
            </h2>
        <?php }?>
        <div class="landing-block__body">
            <div class="grid grid--justify--center">
                <div class="grid__cell grid__cell--m--8 iframe-responsive">
                    <div class="iframe-responsive__iframe">
                        <iframe data-youtube="<?php echo $linkYoutube[0] . '&js_api=1'; ?>" src=""
                                allowfullscreen="allowfullscreen"></iframe>
                    </div>
                </div>
            </div>
        </div>

    <?php } elseif (count($general->linkYoutube) > 1) { ?>
        <?php if (!empty($general->title)) { ?>
            <h2 class="landing-block__header<?php if (empty($general->images)) { echo ' text-align-center'; } ?>">
                <?php echo $general->title; ?>
            </h2>
        <?php } ?>
        <div class="landing-block__body">
            <div class="slider">
                <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">
                        <?php foreach ($general->linkYoutube as $link) { ?>
                            <div class="slide swiper-slide landing-video__slide">
                                <div class="iframe-responsive__iframe landing-video__item">
                                    <iframe data-youtube="<?php echo $link . '&js_api=1'; ?>" src=""
                                            allowfullscreen="allowfullscreen"></iframe>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                </div>
            </div>
            <a class="landing-video__button" href="#">Показать еще</a>
        </div>
    <?php }
} ?>

<?php if ($button->isExist()) { ?>
    <?php echo $button->render('landing-block__button'); ?>
<?php } ?>
