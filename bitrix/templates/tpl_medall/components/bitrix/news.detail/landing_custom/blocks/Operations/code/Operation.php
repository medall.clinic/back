<?php

namespace Medreclama\Landing;

class Operation
{
    public $picture;
    public $primaryTitle;
    public $description;
    public $descriptionListing;
    public $title_h3;
    public $sub_items;
    public $thumbListing;
}
