<?php
/** @var $menulanding Medreclama\Landing\ReviewsService */


$str = '#VRACH,ВРАЧИ, 
#SECTION-ABOUT,ОБ ОПЕРАЦИИ, 
#SECTION-ABOUT,тест';

// Разбиваем строку на массив элементов
$items = explode(', ', $menulanding->titleMenu);

// Создаем пустой массив для хранения результата
$menu_items = [];

// Проходим по каждому элементу массива
foreach ($items as $item) {
    // Разбиваем элемент на ссылку и название
    $parts = explode(',', $item);

    // Получаем ссылку и название
    $link = strtolower(str_replace('#', '', trim($parts[0])));
    $title = trim($parts[1]);

    // Добавляем элемент в массив результата
    $menu_items[] = '<a class="landing-menu__item" href="#' . $link . '">' . $title . '</a>';
}

// Формируем HTML-код для меню
$menu_html = '<div class="landing-menu__list">' . implode('', $menu_items) . '</div>';

// Выводим результат
?>
<section CLASS="container page-section landing-menu">
    <div class="content landing-menu__inner">
        <?php echo $menu_html?>
    </div>

</section>



