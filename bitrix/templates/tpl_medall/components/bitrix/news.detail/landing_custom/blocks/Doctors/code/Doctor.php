<?
namespace Medreclama\Landing;

class Doctor
{
	public $id;
	public $name;
	public $url;
	public $image;
	public $detailImage;
	public $description;
	public $link;
	public $shortLink;
	public $skills;
	public $keySkill;
	public $education;
	public $workExperience;
	public $instagram;

	public function __construct($element)
	{
		$this->id = $element->fields["ID"];
		$this->name = $element->fields["NAME"];

		if(!empty($element->props["io_doctor"]["~VALUE"])){
			$this->name .= " ".$element->props["io_doctor"]["~VALUE"];
		}

		$this->image = getIbElementImageSrc($element->fields["PREVIEW_PICTURE"]);

		if($this->isGlobalDoctorIb($element->fields["IBLOCK_ID"])){
			$this->url = $element->fields["DETAIL_PAGE_URL"];
		}else{

			if(!empty($element->props["LINK"]["VALUE"])){
				$this->link = $element->props["LINK"]["VALUE"];
			}

			if(!empty($element->props["SHORT_LINK"]["VALUE"])){
				$this->shortLink = $element->props["SHORT_LINK"]["VALUE"];
			}

			$this->detailImage = getIbElementImageSrc($element->fields["DETAIL_PICTURE"]);

			if(!empty($element->props["SKILLS"]["VALUE"])){
				$this->skills = $element->props["SKILLS"]["VALUE"];
			}

			if(!empty($element->props["KEY_SKILL"]["VALUE"])){
				$this->keySkill = $element->props["KEY_SKILL"]["VALUE"];
			}

			if(!empty($element->props["EDUCATION"]["VALUE"])){
				$this->education = $element->props["EDUCATION"]["VALUE"];
			}
		}

		if(!empty($element->fields["PREVIEW_TEXT"])){
			$this->description = $element->fields["PREVIEW_TEXT"];
		}

		if(!empty($element->props["experience_doctor"]["VALUE"])){
			$this->workExperience = $element->props["experience_doctor"]["VALUE"];
		}
			
		if(!empty($element->props["instagram_username"]["VALUE"])){
			$this->instagram = $element->props["instagram_username"]["VALUE"];
		}

		if(!empty($element->props["vk_username"]["VALUE"])){
			$this->vk = $element->props["vk_username"]["VALUE"];
		}

	}

	public function hasDetails(): bool
	{
		return !empty($this->skills) OR !empty($this->education) OR !empty($this->keySkill);
	}

	private function isGlobalDoctorIb($id): bool
	{
		return $id == IBLOCK_ID_DOCTORS;
	}

}
?>
