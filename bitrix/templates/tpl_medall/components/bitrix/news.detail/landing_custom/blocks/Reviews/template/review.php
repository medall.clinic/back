<? /** @var $review Medreclama\Landing\Review */ ?>
<? if($review->videoId): ?>
    <div class="slide swiper-slide landing-stories__slide">
        <a
            class="landing-stories__image"
            href="https://www.youtube.com/embed/<?= $review->videoId ?>"
            data-glightbox="data-glightbox"
        >

			<? if($review->picture): ?>
                <picture>
                    <source srcset="<?=WebPHelper::getOrCreateWebPUrl($review->picture, [1500])?>"/>
                    <img src="<?= $review->picture ?>" alt="" loading="lazy" decoding="async" />
                </picture>
			<? endif ?>

            <? if($review->name OR $review->description): ?>
                <? include __DIR__."/review-info.php" ?>
			<? endif ?>

        </a>
    </div>
<? else: ?>
    <? if($review->picture): ?>
        <div class="slide swiper-slide landing-stories__slide">

            <div class="landing-stories__image">
                <img src="<?= $review->picture ?>" alt=""/>
            </div>

			<? if($review->name OR $review->description): ?>
				<? include __DIR__."/review-info.php" ?>
			<? endif ?>

        </div>
	<? endif ?>
<? endif ?>
