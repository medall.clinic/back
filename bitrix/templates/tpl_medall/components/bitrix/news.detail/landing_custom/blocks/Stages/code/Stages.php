<?
namespace Medreclama\Landing;

class Stages extends Landing
{
	public $items;
	public $listItems;
	public $finalStage;
    public $newTemplateStage;
	public $isAllStagesWithTitle;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["STAGES_TITLE"]["~VALUE"];
        $this->newTemplateStage = $this->landingProps["STAGE_NEW_TEMPLATE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["STAGES_BLOCK_ID"]);
		$this->items = $this->getItems($this->landingProps["STAGES_ITEMS"]["VALUE"]);
		$this->listItems = $this->landingProps["STAGES_LIST_ITEMS"]["VALUE"];
		$this->finalStage = $this->getFinalStage();
		$this->isAllStagesWithTitle = $this->findOutIsAllStagesWithTitle($this->items);
		$this->button = new Button($this->landingProps["STAGES_BUTTON_TEXT"], $this->landingProps["STAGES_BUTTON_LINK"], $this->landingProps["STAGES_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["STAGES_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["STAGES_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["STAGES_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["STAGES_MENU_TITLE"]["VALUE"], $this->landingProps["STAGES_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return [];
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Stage;
			$item->title = $element->props["NAME"]["~VALUE"];
			$item->text = $element->fields["~PREVIEW_TEXT"];
			$item->image = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);

			$items[] = $item;
		}

		return $items;
	}

	private function findOutIsAllStagesWithTitle($items)
	{
		$items = array_filter($items, function ($stage){
			return (bool)$stage->title;
		});

		return (bool)$items;
	}

	private function getFinalStage(): Stage
	{
		$finalStage = new Stage;
		$finalStage->image = $this->getImageSrc($this->landingProps["STAGES_BANNER_IMAGE"]["VALUE"]);
		$finalStage->text = $this->landingProps["STAGES_BANNER_TEXT"]["VALUE"];

		return $finalStage;
	}

	private function isShow()
	{


		return true;
	}

}
