<? /** @var $reason Medreclama\Landing\Reason */ ?>
<? if($reason->image): ?>
    <li class="grid__cell grid__cell--s--6 grid__cell--xs--12 landing-reasons__item">
        <? if($reason->link): ?>
            <a class="grid grid--no-gap landing-reason" href="<?= $reason->link ?>">
                <? include __DIR__."/with-image-inner.php" ?>
            </a>
        <? else: ?>
            <div class="grid grid--no-gap landing-reason">
                <? include __DIR__."/with-image-inner.php" ?>
            </div>
        <? endif ?>
    </li>
<? else: ?>
    <? if(!$reason->isSmallSize): ?>
        <li class="grid__cell grid__cell--s--4 grid__cell--xs--8 landing-reasons__item">
            <? if($reason->link): ?>
                <a class="grid grid--no-gap landing-reason" href="<?= $reason->link ?>">
                    <? include __DIR__."/wo-image-big-inner.php" ?>
                </a>
            <? else: ?>
                <div class="grid grid--no-gap landing-reason">
                    <? include __DIR__."/wo-image-big-inner.php" ?>
                </div>
            <? endif ?>
        </li>
    <? else: ?>
        <li class="grid__cell grid__cell--s--2 grid__cell--xs--4 landing-reasons__item">
            <? if($reason->link): ?>
                <a class="grid grid--no-gap landing-reason" href="<?= $reason->link ?>">
                    <? include __DIR__."/wo-image-small-inner.php" ?>
                </a>
            <? else: ?>
                <div class="grid grid--no-gap landing-reason">
                    <? include __DIR__."/wo-image-small-inner.php" ?>
                </div>
            <? endif ?>
        </li>
    <? endif ?>
<? endif ?>
