<?php /** @var $comfort Medreclama\Landing\Comfort */ ?>
<?php if ($comfort->template != 'Да'){?>
    <section class="container page-section page-section--background-color page-section--background-color--dark landing-comfort">
        <div class="content">
            <div class="page-subsection">
                <div class="grid grid--align-items--center">
                    <?php if($comfort->images): ?>
                        <div class="grid__cell grid__cell--m--6 hidden-s hidden-xs">
                            <div class="slider landing-comfort__slider">
                                <div class="slider__slides swiper">
                                    <div class="slider__wrapper swiper-wrapper">
                                        <?php foreach($comfort->images as $image): ?>
                                            <img src="<?=WebPHelper::getOrCreateWebPUrl($image, [ 1200 ])?>" alt="" class="slide landing-comfort__slide swiper-slide" loading="lazy"  decoding="async">
                                        <?php endforeach ?>
                                    </div>
                                    <div class="slider__arrow slider__arrow--prev"></div>
                                    <div class="slider__arrow slider__arrow--next"></div>
                                    <div class="slider__pagination"></div>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                    <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
                        <?php if($comfort->title){ ?>
                            <h2 class="page-subsection">
                                <?= $comfort->title ?>
                            </h2>
                        <?php } ?>
                        <div class="slider landing-comfort__slider page-subsection hidden-l hidden-m">
                            <div class="slider__slides swiper">
                                <div class="slider__wrapper swiper-wrapper">
                                    <?php foreach($comfort->images as $image): ?>
                                        <img src="<?=WebPHelper::getOrCreateWebPUrl($image, [ 1200 ])?>" alt="" class="slide landing-comfort__slide swiper-slide" loading="lazy" decoding="async">
                                    <?php endforeach ?>
                                </div>
                                <div class="slider__arrow slider__arrow--prev"></div>
                                <div class="slider__arrow slider__arrow--next"></div>
                                <div class="slider__pagination"></div>
                            </div>
                        </div>
                        <?php if($comfort->text): ?>
                            <div class="page-subsection">
                                <?= $comfort->text ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } ?>
<?php if ($comfort->template == 'Да'){?>
    <section class="container page-section page-section--background-color landing-banner-slider">
        <div class="landing-banner-slider__inner">
            <?php if (!empty($comfort->title)){?>
                <h2 class="landing-banner-slider__header"><?php echo $comfort->title?></h2>

            <?php }?>

            <?php if (!empty($comfort->text)){?>
                <div class="landing-banner-slider__body"><?php echo $comfort->text?></div>
            <?php }?>
        </div>
        <div class="slider landing-banner-slider__slider">

            <div class="slider__slides swiper">
                <div class="slider__wrapper swiper-wrapper">
                    <?php if (is_array($comfort->images) && !empty($comfort->images)){?>
                        <?php foreach($comfort->images as $image){ ?>
                            <?php if (!empty($image)){?>
                                <div class="slide swiper-slide landing-banner-slider__slide">
                                    <picture>
                                        <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($image, [ 1200 ])?>" type="image/webp"/>
                                        <source srcset="<?php echo $image ?>"/><img class="landing-banner-slider__image" src="<?php echo $image ?>" loading="lazy" decoding="async" alt=""/>
                                    </picture>
                                </div>
                            <?php }?>

                        <?php } ?>

                    <?php }?>


                </div>
                <div class="slider__arrow slider__arrow--prev"></div>
                <div class="slider__arrow slider__arrow--next"></div>
                <div class="slider__pagination"></div>
            </div>
        </div>
    </section>

<?php }?>



