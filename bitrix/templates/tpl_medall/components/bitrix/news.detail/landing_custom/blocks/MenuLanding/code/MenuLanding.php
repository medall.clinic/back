<?php
namespace Medreclama\Landing;

class MenuLanding extends Landing
{

    public $titleMenu;
    // другие свойства

    public function __construct($arResult)
    {
        parent::__construct($arResult);
        if (!empty($this->landingProps['MENU_LANDING']['~VALUE']['TEXT'])){
            $this->titleMenu = $this->landingProps['MENU_LANDING']['~VALUE']['TEXT'];
        }


        $this->isShow = $this->isShow();

        return true;
    }




	private function isShow()
	{
		if(
			!$this->titleMenu
		){

            return false;
		}

		return true;
	}

}
