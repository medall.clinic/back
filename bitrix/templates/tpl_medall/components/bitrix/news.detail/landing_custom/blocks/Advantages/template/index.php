<?php /** @var $advantages Medreclama\Landing\Advantages */ ?>
<?php /** @var $button Medreclama\Landing\Button */ ?>
<?php $button = $advantages->button ?>
<section
    class="container page-section page-section--gradient page-section--gradient--to-bottom"
    <?php
    if (!empty($advantages->blockId)){?>
        id="<?php echo $advantages->blockId ?>"

    <?php } ?>>
    <div class="content">

        <?php if($advantages->title): ?>
            <h2 class="page-subsection text-align-center"><?= $advantages->title ?></h2>
        <?php endif ?>

        <?php if($advantages->images): ?>
            <?php include __DIR__."/images.php" ?>
        <?php endif ?>

        <?php if($advantages->items): ?>
            <?php include __DIR__."/items.php" ?>
        <?php endif ?>

        <?php if($button->isExist()): ?>
            <div class="page-subsection grid grid--justify--center">
                <a
                    class="button grid__cell"
                    href="<?= $button->link ?>"
                    <?= $button->isFancybox ? "data-fancybox" : "" ?>
                ><?= $button->text ?></a>
            </div>
        <?php endif ?>

    </div>
</section>