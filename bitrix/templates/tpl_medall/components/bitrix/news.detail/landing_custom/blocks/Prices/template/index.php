<?php /** @var $prices Medreclama\Landing\Prices */ ?>
<?php /** @var $button Medreclama\Landing\Button */ ?>
<?php $pricesButton = $prices->button ?>
<section class="container page-section"
    <?php if (!empty($prices->blockId)){?>
        id="<?php echo $prices->blockId ?>"
    <?php } ?>>
    <?php
    $test = $prices
    ?>

    <div class="content">
        <?php if (!empty($prices->landingName) && !empty($prices->title)){?>
            <div class="page-subsection text-align-center">
                <h2><?= $prices->title ?></h2>
            </div>
        <?php }?>



        <?php if($prices->text): ?>
            <div class="page-subsection">
                <?= $prices->text ?>

            </div>
        <?php endif ?>

        <?php if($prices->complexItems): ?>
            <div class="page-subsection">
                <?php include __DIR__."/complex.php" ?>
            </div>
        <?php endif ?>

        <?php if(!empty($prices->xmlPriceList)) { ?>
            <div class="page-subsection">
                <?php include __DIR__."/simple.php" ?>
            </div>
        <?php } ?>

        <?php if($prices->text2): ?>
            <div class="page-subsection">
                <?= $prices->text2 ?>
            </div>
        <?php endif ?>

        <?php if($pricesButton->isExist()): ?>
            <div class="page-subsection grid grid--justify--center">
                <?= $pricesButton->render() ?>
            </div>
        <?php endif ?>

    </div>
</section>
