<?php /** @var $work Medreclama\Landing\Work */ ?>
<?php /** @var $works Medreclama\Landing\Works */ ?>
<?php /** @var $button Medreclama\Landing\Button */ ?>
<?php $button = $works->button ?>
<section class="container page-section"
    <?php if (!empty($works->blockId)) { ?>
        id="<?php echo $works->blockId ?>"
    <?php } ?>>

    <div class="content">

        <?php if ($works->title): ?>
            <h2 class="page-subsection text-align-center"><?= $works->title ?></h2>
        <?php endif ?>

        <?php if ($works->items): ?>
            <div class="landing-works page-subsection">
                <div class="slider">
                    <div class="slider__slides swiper">

                        <div class="slider__wrapper swiper-wrapper">
                            <?php
                            // Обрезаем массив до 8 элементов
                            $limitedItems = array_slice($works->items, 0, 8);
                            foreach ($limitedItems as $work): ?>
                                <?php include __DIR__."/work.php" ?>
                            <?php endforeach ?>
                        </div>

                        <div class="slider__arrow slider__arrow--prev"></div>
                        <div class="slider__arrow slider__arrow--next"></div>
                        <div class="slider__pagination"></div>

                    </div>
                </div>
            </div>
        <?php endif ?>

        <?php if ($button->isExist()): ?>
            <div class="page-subsection grid grid--justify--center">
                <?= $button->render() ?>
            </div>
        <?php endif ?>

    </div>
</section>
