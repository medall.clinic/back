<div class="landing-block__image hidden-hd hidden-xl hidden-l hidden-m hidden-s">
    <picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($image), [ 800 ] ?>" type="image/webp"/>
        <img src="<?= $image ?>" loading="lazy" decoding="async" alt="" title=""/>
    </picture>
</div>
