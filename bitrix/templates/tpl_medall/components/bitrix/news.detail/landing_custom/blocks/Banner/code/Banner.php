<?
namespace Medreclama\Landing;

class Banner extends Landing
{
	public $subtitle;
	public $image;
	public $text;
	public $price;
    public $bannerList;
	public $isPriceFrom;
	public $oldButton;
	public $formTitle;
	public $utmTags;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["BANNER_TITLE"]["~VALUE"];
        $this->bannerList = $this->landingProps["BANNER_LIST"]["~VALUE"];
		$this->subtitle = $this->landingProps["BANNER_SUBTITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["BANNER_BLOCK_ID"]);
		$this->image = $this->getImageSrc($this->landingProps["BANNER_IMAGE"]["VALUE"]);
		$this->price = \StringsHelper::numberFormat($this->landingProps["BANNER_PRICE"]["VALUE"]);
		$this->isPriceFrom = (bool)$this->landingProps["BANNER_IS_PRICE_FROM"]["~VALUE"];
		$this->button = new Button($this->landingProps["BANNER_BUTTON_TEXT"], $this->landingProps["BANNER_BUTTON_LINK"], $this->landingProps["BANNER_BUTTON_FANCYBOX"]);
		$this->oldButton = $this->landingProps["BANNER_BUTTON"]["VALUE"];
		$this->additionalText = $this->landingProps["BANNER_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["BANNER_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->generalBlockId = $this->landingProps["BANNER_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["BANNER_MENU_TITLE"]["VALUE"], $this->landingProps["BANNER_MENU_LINK"]["VALUE"], $this->blockId);

		$this->formTitle = $this->getTitle($arResult["NAME"]);
		$this->utmTags = $this->getUtmTags();

		$this->isShow = $this->isShow();
        $this->getProperties($this->landingProps["BANNER_TEXT"]["~VALUE"]['TEXT']);

		return true;
	}
    public function getProperties($arrProp) : string
    {
      if (is_array($arrProp)) {
          foreach ($arrProp as $k => $v) {
              if (!empty($v)){
                  $this->text .= $v;

              }

          }
          return $this->text;

      } else {
          return '';
      }
    }

	private function getTitle(string $landingName): string
	{
		$pageType = $this->isPageTypeLanding() ? "лендинг" : "страница услуги";

		if(!empty($this->landingProps["FORM_CONTACTS_TITLE"]["~VALUE"])){
			$title = trim($this->landingProps["FORM_CONTACTS_TITLE"]["~VALUE"]);
		}else{
			$title = trim($landingName);
		}

		return $pageType.": ".$title;
	}

	private function getUtmTags()
	{
		if(empty($_GET)){
			return false;
		}

		$utmTags = [];
		foreach ($_GET as $paramName => $paramValue){
			if(strpos($paramName, 'utm_') !== false){
				$utmTags[] = new UtmTag($paramName, $paramValue);
			}
		}

		return $utmTags;
	}

	private function isShow()
	{
		if(
			!$this->title &&
			!$this->image &&
			!$this->text &&
			!$this->price &&
			!$this->oldButton
		){
			return false;
		}

		return true;
	}

}
