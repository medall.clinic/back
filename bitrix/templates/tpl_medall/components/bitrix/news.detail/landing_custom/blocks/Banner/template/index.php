<?php /** @var $banner Medreclama\Landing\Banner */ ?>
<?php /** @var $button Medreclama\Landing\Button */ ?>
<?php $button = $banner->button ?>


<section class="container page-section landing-header"
    <?php
    if (!empty($banner->blockId)) {?>
        id="<?php echo $banner->blockId ?>"

    <?php } ?>>

    <div class="content">

        <div class="grid grid--align-items--center">
            <div class="grid__cell grid__cell--m--7 grid__cell--s--6 grid__cell--xs--12 landing-header__info">

                <?php if (!empty($banner->title)) { ?>
                    <h1 class="landing-header__title"><?php echo $banner->title ?></h1>

                <?php } ?>
                <?php if (!empty($banner->bannerList)) { ?>
                    <div class="landing-header__body">
                        <ul class="landing-header__list">
                            <?php foreach ($banner->bannerList as $list){?>
                                <?php if (!empty($list)){?>
                                    <li class="landing-header__item"> <?php echo $list ?></li>

                                <?php }?>

                            <?php }?>

                        </ul>
                    </div>

                <?php } ?>

                <?php if (!empty($banner->subtitle)) { ?>
                    <div class="landing-header__subtitle"><?= $banner->subtitle ?></div>
                <?php } ?>



                <?php if ($button->isExist()) { ?>
                    <?php echo $button->render('landing-header__button') ?>
                <?php } ?>

            </div>
            <?php if (!empty($banner->image)) {?>
                <div class="grid__cell grid__cell--m--5 grid__cell--s--6 grid__cell--xs--12 landing-header__image">

                    <picture>
                        <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($banner->image, [800]) ?>"
                                type="image/webp"/>
                        <img fetchpriority="high" width="800" height="800" src="<?php echo $banner->image ?>"
                             alt="<?php echo $banner->title ?>"/>
                    </picture>
                </div>

            <?php } else {?>
                <picture>
                    <source srcset="https://dummyimage.com/800x392/D9D9D9/D9D9D9.jpg"
                            type="image/webp"/>
                    <source srcset="https://dummyimage.com/800x392/D9D9D9/D9D9D9.jpg"/>
                    <img src="https://dummyimage.com/800x392/D9D9D9/D9D9D9.jpg"
                         loading="lazy"
                         decoding="async" alt=""/>
                </picture>
            <?php }?>

        </div>

        <?php if ($button->isExist()) { ?>
            <?php include __DIR__ . "/form-bitrix-24.php" ?>
        <?php } ?>

    </div>
</section>
