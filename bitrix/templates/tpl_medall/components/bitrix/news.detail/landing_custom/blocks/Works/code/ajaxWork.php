<?php


require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');


if ($_SERVER['REQUEST_METHOD'] === 'POST') {


    $data = json_decode(file_get_contents('php://input'), true);
    $offset = $data['offset'];
    $limit = $data['limit'];

    // Загружаем нужное количество элементов (например, из базы данных или массива)

    $response = [];

    // Отправляем JSON-ответ
    header('Content-Type: application/json');
    echo 'ok';
    exit;
}
