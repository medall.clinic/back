<?
namespace Medreclama\Landing;

class Reasons extends Landing
{
	public $text;
	public $items;
	public $listItemsPros;
	public $listItemsCons;
    public $newsTemplate;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
        $this->newsTemplate = $this->landingProps["REASONS_NEW_TEMPLATE"]['~VALUE'];

		$this->blockId = $this->getBlockId($this->landingProps["REASONS_BLOCK_ID"]);
		$this->text = $this->landingProps["REASONS_LIST_ITEMS_PROS"]["VALUE"] ? $this->landingProps["REASONS_LIST_ITEMS_CONS"]["VALUE"] : false;
		$this->items = $this->getItems($this->landingProps["REASONS_ITEMS"]["VALUE"]);
		$this->listItemsPros = $this->landingProps["REASONS_LIST_ITEMS_PROS"]["VALUE"];
		$this->listItemsCons = $this->landingProps["REASONS_LIST_ITEMS_CONS"]["VALUE"];
		$this->button = new Button($this->landingProps["REASONS_BUTTON_TEXT"], $this->landingProps["REASONS_BUTTON_LINK"], $this->landingProps["REASONS_BUTTON_FANCYBOX"]);
        $this->title =  $this->landingProps["REASONS_TITLE"]["VALUE"];

        $this->additionalText = $this->landingProps["REASONS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["REASONS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["REASONS_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["REASONS_MENU_TITLE"]["VALUE"], $this->landingProps["REASONS_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Reason;
			$item->image = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
			$item->text = $element->props["TEXT"]["~VALUE"];
			$item->link = $element->props["LINK"]["VALUE"];
			$item->isSmallSize = $this->isItemSmallSize($element);

			$items[] = $item;
		}

		return $items;
	}

	private function isItemSmallSize(Element $element): bool
	{
		if($element->fields["PREVIEW_PICTURE"]){
			return false;
		}

		return (bool)$element->props["IS_SMALL_SIZE"]["VALUE"];
	}

	private function isShow()
	{
		if(
			!$this->title &&
			!$this->text &&
			!$this->items &&
			!$this->listItemsPros &&
			!$this->listItemsCons
		){
			return false;
		}

		return true;
	}

	public function hasListItems(): bool
	{
		return !empty($this->listItemsPros) OR !empty($this->listItemsCons);
	}

}
