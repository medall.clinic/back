<?
namespace Medreclama\Landing;

class Works extends Landing
{
	public $items;

    public function __construct(array $arResult = [])
    {
        parent::__construct($arResult);

        $this->title = $this->landingProps["WORKS_TITLE"]["~VALUE"];
        $this->blockId = $this->getBlockId($this->landingProps["WORKS_BLOCK_ID"]);
        $this->items = $this->getItems($this->landingProps["WORKS_ITEMS"]["VALUE"]);
        $this->button = new Button($this->landingProps["WORKS_BUTTON_TEXT"], $this->landingProps["WORKS_BUTTON_LINK"], $this->landingProps["WORKS_BUTTON_FANCYBOX"]);

        $this->additionalText = $this->landingProps["WORKS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["WORKS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

        $this->generalBlockId = $this->landingProps["WORKS_GENERAL_BLOCK"]["VALUE"];
        $this->menuItem = new MenuItem($this->landingProps["WORKS_MENU_TITLE"]["VALUE"], $this->landingProps["WORKS_MENU_LINK"]["VALUE"], $this->blockId);
        $this->detailPage = $this->arResult['DETAIL_PAGE_URL'];
        $this->isShow = $this->isShow();
        foreach ($this->items as $key => &$item) {
            if (!empty($item->title)){
                $item->doctor['SERVICE'] =  $item->title;
                $item->doctor['SERVICE_URL'] = $this->detailPage;

            }
            if (!empty($item->doctor['PREVIEW_TEXT'])){
                $item->doctor['POST'] = $item->doctor['PREVIEW_TEXT'];
            }


            // Удаляем старый ключ title
            // Обновляем элемент в массиве items
            $this->items[$key] = $item; // Обновляем элемент в массиве

        }

        $this->saveItemsToJson();


        return true;
    }

    private function saveItemsToJson()
    {
        if ($this->items) {
            // Проверяем существование SERVICE_URL перед сохранением
            $existingData = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/data/doctors.json'), true);
            $existingServiceUrls = array_column($existingData, 'doctor', 'SERVICE_URL');

            foreach ($this->items as $item) {
                if (!in_array($item->doctor['SERVICE_URL'], $existingServiceUrls)) {
                    // Добавляем новый элемент в массив
                    $existingData[] = $item;
                }
            }

            // Конвертируем массив в JSON
            $jsonData = json_encode($existingData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

            // Обеспечиваем кодировку UTF-8 с BOM
            $utf8Bom = "\xEF\xBB\xBF"; // UTF-8 BOM
            $jsonDataWithBom = $utf8Bom . $jsonData;

            // Определяем путь к JSON файлу
            $jsonFilePath = $_SERVER['DOCUMENT_ROOT'] . '/data/doctors.json';

            // Сохраняем JSON данные в файл с кодировкой UTF-8
            file_put_contents($jsonFilePath, $jsonDataWithBom);
        }
    }

    private function convertToWebP($path) {

        return preg_replace('/\.(jpg|jpeg|png)$/i', '.webp', $path);
    }
	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElementsSectionsWorks();

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Work;
            $item->doctor = $element->fields['PROPERTY_DOCTOR_VALUE'];
            $item->photoBefore = $this->convertToWebP($element->fields['PROPERTY_PHOTO_BEFORE_VALUE']);
            $item->photoAfter = $this->convertToWebP($element->fields['PROPERTY_PHOTO_AFTER_VALUE']);
            $item->title = $element->fields['SERVICE_NAME'];

            $items[] = $item;
		}


        return $items;
	}

	private function isShow()
	{
		if(!$this->items){
			return false;
		}

		return true;
	}


}
