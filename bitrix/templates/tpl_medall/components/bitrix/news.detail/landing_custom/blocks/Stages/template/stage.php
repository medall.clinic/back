<? /** @var $stage Medreclama\Landing\Stage */ ?>
<li class="grid__cell grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12 landing-sequence__item">

    <? if($stage->image): ?>
        <picture>
            <source srcset="<?= WebPHelper::getOrCreateWebPUrl($stage->image) ?>" type="image/webp"/>
            <img class="landing-sequence__image" src="<?= $stage->image ?>" loading="lazy" decoding="async" alt=""/>
        </picture>
    <? endif ?>

    <? if($stage->title): ?>
        <div class="landing-sequence__name"><?= $stage->title ?></div>
    <? endif ?>

    <? if($stage->text): ?>
        <div class="landing-sequence__description"><?= $stage->text ?></div>
    <? endif ?>

</li>

