<? /** @var $price Medreclama\Landing\Price */ ?>
<? /** @var $prices Medreclama\Landing\Prices */ ?>
<div class="grid grid--justify--center landing-prices">
	<? foreach($prices->complexItems as $price): ?>
        <div class="landing-price landing-price--undefined grid__cell grid__cell--m--5 grid__cell--s--6 grid__cell--xs--12 prices__item">

            <div class="landing-price__inner">
                <div class="landing-price__name"><?= $price->title ?></div>
                <div class="landing-price__value"><span><?= $price->value ?> ₽</span></div>
                <div class="landing-price__desc"><?= $price->description ?></div>
                <div class="landing-price__doctors"><strong><?= $price->doctors ?></strong></div>
            </div>

			<?= $price->button->render('landing-price__button') ?>

        </div>
	<? endforeach ?>
</div>