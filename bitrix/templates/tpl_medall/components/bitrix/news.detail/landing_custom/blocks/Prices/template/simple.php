<?php /** @var $price Medreclama\Landing\Price */ ?>
<?php /** @var $prices Medreclama\Landing\Prices */ ?>
<?php
if (!empty($prices->xmlPriceList)) {
    ?>
    <div class="table-price">
        <?php foreach ($prices->xmlPriceList as $item) { ?>
                <?php if ($item['MIN_PRICE'] && $item['MAX_PRICE']) {?>
                <div class="table-price__row">
                    <div class="table-price__name"><?php echo $item['NAME'] ?></div>
                    <div class="table-price__value"><span>от <?php echo number_format($item['MIN_PRICE'], 0, ',', ' ') ?> ₽ до <?php echo number_format($item['MAX_PRICE'], 0, ',', ' ') ?> ₽</span></div>
                </div>
            <?php } else {?>
                <div class="table-price__row">
                    <div class="table-price__name"><?php echo $item['NAME'] ?></div>
                    <div class="table-price__value"><span><?php echo number_format($item['MIN_PRICE'], 0, ',', ' ') ?>  ₽</span></div>
                </div>
            <?php }?>

        <?php } ?>
    </div>
<?php } ?>
