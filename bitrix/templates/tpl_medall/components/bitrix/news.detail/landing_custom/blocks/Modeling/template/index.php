<?php /** @var $modeling Medreclama\Landing\Modeling */ ?>
<?php
if ($modeling->template != 'Да'){?>
    <section class="page-section page-section--3d-modelling"
        <?php if (!empty($modeling->blockId)){?>
            id="<?php echo $modeling->blockId ?>"
        <?php } ?>>

        <?php if($modeling->title): ?>
            <div class="page-subsection section-header">
                <div class="container">
                    <div class="content">
                        <h2><?php echo $modeling->title ?></h2>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <div class="page-subsection">
            <div class="container">
                <div class="content">
                    <div class="grid grid--padding-y grid--align-center">

                        <div class="grid__cell grid__cell--m-6 grid__cell--xs-12">

                            <?= $modeling->text ?>

                            <?php if($modeling->images): ?>
                                <div class="slider slider--simple splide hidden-m hidden-l hidden-xl hidden-hd">
                                    <div class="splide__track">
                                        <ul class="splide__list">
                                            <?php foreach($modeling->images as $image): ?>
                                                <li class="splide__slide">
                                                    <div class="slider__image">
                                                        <img src="<?= $image ?>" alt="">
                                                    </div>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif ?>

                            <?php if($modeling->oldButton): ?>
                                <div class="get-now">
                                    <p class="get-now__info">Запишитесь прямо сейчас</p>
                                    <p>
                                        <a
                                                class="btn btn--secondary get-now__button"
                                                href="#section-contact"
                                        ><?= $modeling->oldButton ?></a>
                                    </p>
                                </div>
                            <?php endif ?>

                        </div>

                        <?php if($modeling->images): ?>
                            <div class="grid__cell grid__cell--m-6 grid__cell--s-hidden grid__cell--xs-hidden">
                                <div class="slider slider--simple splide">
                                    <div class="splide__track">
                                        <ul class="splide__list">
                                            <?php foreach($modeling->images as $image): ?>
                                                <li class="splide__slide">
                                                    <div class="slider__image">
                                                        <img src="<?= $image ?>" alt="">
                                                    </div>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php }?>

<?php if ($modeling->template == 'Да'){?>
    <section class="container page-section page-section--background-color page-section--background-color--dark landing-banner">
        <div class="landing-banner__image">
            <?php foreach ($modeling->images as $image){?>
                <picture>
                    <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($image, [800]) ?>" type="image/webp">
                    <source srcset="<?php echo $image?>"><img src="<?php echo $image?>" loading="lazy" decoding="async" alt="<?php echo $modeling->title?>">
                </picture>
            <?php }?>

        </div>
        <?php if (!empty($modeling->title)){?>
            <h2 class="landing-banner__header"><?php  echo $modeling->title?></h2>

        <?php }?>
        <?php if (!empty($modeling->text)){?>
            <div class="landing-banner__body"><?php echo $modeling->text?></div><a class="landing-banner__button button" href="#landing-form"><?php echo $modeling->oldButton?></a>
        <?php }?>
    </section>
<?php }?>
