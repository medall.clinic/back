<?php
namespace Medreclama\Landing;

class Prices extends Landing
{
    public $text;
    public $text2;
    public $complexItems;
    public $simpleItems;
    public array $xmlPriceList;

    public function __construct(array $arResult)
    {
        parent::__construct($arResult);

        $this->title = $this->landingProps["PRICES_TITLE"]["~VALUE"];
        $this->blockId = $this->getBlockId($this->landingProps["PRICES_BLOCK_ID"]);

        $this->text = $this->landingProps["PRICES_TEXT_1"]["~VALUE"] ? $this->landingProps["PRICES_TEXT_1"]["~VALUE"]["TEXT"] : "";
        $this->text2 = $this->landingProps["PRICES_TEXT_2"]["~VALUE"] ? $this->landingProps["PRICES_TEXT_2"]["~VALUE"]["TEXT"] : "";

        $this->complexItems = $this->complexItems($this->landingProps["PRICES_COMPLEX_ITEMS"]["VALUE"]);
        $this->simpleItems = $this->simpleItems($this->landingProps["PRICES_SIMPLE_ITEMS"]["VALUE"]);

        $this->button = new Button($this->landingProps["PRICES_BUTTON_TEXT"], $this->landingProps["PRICES_BUTTON_LINK"], $this->landingProps["PRICES_BUTTON_FANCYBOX"]);

        $this->additionalText = $this->landingProps["PRICES_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["PRICES_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

        $this->generalBlockId = $this->landingProps["PRICES_GENERAL_BLOCK"]["VALUE"];
        $this->menuItem = new MenuItem($this->landingProps["PRICES_MENU_TITLE"]["VALUE"], $this->landingProps["PRICES_MENU_LINK"]["VALUE"], $this->blockId);
        $this->isShow = $this->isShow();
        $this->xmlPriceList = $this->getXmlFile();

        return true;
    }
    public function getXmlFile(): array
    {
        global $APPLICATION;
        $xmlFilePath = $_SERVER['DOCUMENT_ROOT'].'/import/Price.xml';
        $xml = simplexml_load_file($xmlFilePath);
        $arrChildElements = [];
        $UID = $this->landingName; // Предполагаю, что это свойство класса

        foreach ($xml->Каталог as $catalog) {
            if ($catalog->ЭтоПапка == 'false' && (float)$catalog->Цена > 0) {
                $fullName = (string)$catalog->Наименование;
                $price = (float)$catalog->Цена;
                $parentId = (string)$catalog->Родитель;

                // Условие: если полное наименование содержит подстроку из $UID
                if (stripos(trim($fullName), trim($UID)) !== false || $parentId == $UID) {
                    // Удаляем категорию из названия для группировки
                    $baseName = preg_replace('/\s*\(\s*экспертный уровень\)\s*|\s*\(липоскульптура\)\s*|\s*\(\d+\s+категория\)\s*|\s*\(\d+\s+категория сложности\)\s*|\s*\(\d+\s+категории сложности\)\s*|\s*\(\d+\s+степень сложности\)\s*|\s*\(\d+\s+уровень сложности\)\s*/ui', '', $fullName); // Удаляем текст в скобках
                    if (in_array(trim(trim($baseName)), $this->landingExce)){
                        continue;
                    }

                    // Добавляем или обновляем запись в массиве
                    if (empty($arrChildElements[$baseName])) {
                        $arrChildElements[trim($baseName)] = [
                            'NAME' => trim($baseName),
                            'PRICES' => [] // Массив для хранения цен
                        ];
                    }
                    $arrChildElements[trim($baseName)]['PRICES'][] = $price; // Добавляем цену в массив


                }
            }
        }

        // Теперь вычисляем минимальные и максимальные цены для каждой группы
        $result = [];
        foreach ($arrChildElements as $element) {
            $minPrice = min($element['PRICES']);
            $maxPrice = max($element['PRICES']);

            // Проверка: если минимальная и максимальная цена равны, добавляем только minPrice
            if ($minPrice === $maxPrice) {
                $result[] = [
                    'NAME' => $element['NAME'],
                    'MIN_PRICE' => $minPrice
                ];
            } else {
                // В противном случае добавляем и minPrice, и maxPrice
                $result[] = [
                    'NAME' => $element['NAME'],
                    'MIN_PRICE' => $minPrice,
                    'MAX_PRICE' => $maxPrice
                ];
            }
        }

        // Возвращаем массив с найденными элементами
        return $result; // возврат значений массива
    }




    private function complexItems($elementsId)
    {
        if(empty($elementsId)){
            return false;
        }

        $elements = $this->getElements($elementsId);

        $items = [];

        /** @var Element $element */
        foreach ($elements as $element){
            $item = new Price;
            $item->title = $element->props["TITLE"]["~VALUE"];
            $item->value = \StringsHelper::numberFormat($element->props["VALUE"]["~VALUE"]);
            $item->description = $element->props["DESCRIPTION"]["~VALUE"];
            $item->doctors = $element->props["DOCTORS"]["~VALUE"];
            $item->button = new Button($element->props["BUTTON_TEXT"], $element->props["BUTTON_LINK"], $element->props["BUTTON_FANCYBOX"]);

            $items[] = $item;
        }

        return $items;
    }

    private function simpleItems($elementsId)
    {
        if(empty($elementsId)){
            return false;
        }

        $elements = $this->getElements($elementsId);

        $items = [];

        /** @var Element $element */
        foreach ($elements as $element){
            $item = new Price;
            $item->title = $element->fields["NAME"];
            $item->value = $element->props["price"]["VALUE"];

            if(is_numeric($item->value)){
                $item->value = \StringsHelper::numberFormat($item->value);
            }

            $items[] = $item;
        }

        return $items;
    }

    private function isShow()
    {
        if(!$this->complexItems && !$this->simpleItems){
            return false;
        }

        return true;
    }


}
