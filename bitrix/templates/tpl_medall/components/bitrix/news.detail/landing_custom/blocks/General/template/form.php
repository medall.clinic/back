<? /** @var $general Medreclama\Landing\General */ ?>
<form class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12 landing-block__form form contacts_to_bitrix24">

	<label class="form-input form-input--text <?= $this->isColorLight ? 'form-input--secondary' : '' ?> form__input form__input">
		<input class="form-input__field" type="text" name="name" required placeholder="ФИО *">
	</label>

	<label class="form-input form-input--tel <?= $this->isColorLight ? 'form-input--secondary' : '' ?> form__input form__input">
		<input class="form-input__field" type="tel" name="phone" required placeholder="Телефон *">
	</label>

	<div class="form__agreement">Нажимая кнопку «Отправить» я даю согласие на <a href="/">обработку персональных данных</a>.</div>

	<button class="button form__submit" type="submit">Отправить</button>

	<input type="hidden" name="title" value="<?= $general->formTitle ?>">

	<? if($general->formUtmTags): ?>
		<? foreach ($general->formUtmTags as $utmTag): ?>
			<input type="hidden" name="<?= $utmTag->name ?>" value="<?= $utmTag->value ?>">
		<? endforeach ?>
	<? endif ?>

</form>
