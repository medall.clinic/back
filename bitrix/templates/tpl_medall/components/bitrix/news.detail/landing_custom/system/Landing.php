<?php
namespace Medreclama\Landing;

class Landing
{
    public $title;
    public $blockId;
    public  string $landingName;
    public $button;
    public $additionalText;
    public $generalBlockId;
    public $menuItem;
    public $landingProps;
    public $landingExce;
    public $isShow;
    public string $curPage;

    public $arResult;

    public $elementId;
    public $detailUrl;

    public function __construct(array $arResult)
    {
        $this->arResult = $arResult;

        $this->landingName = $arResult["PROPERTIES"]["LANDING_NAME"]["VALUE"];
        foreach ($arResult["PROPERTIES"]["LANDING_EXCEPTION"]["VALUE"] as $key => $value ){
            $this->landingExce[] = $value;
        }


        $this->landingProps = $arResult["PROPERTIES"];
        $this->isShow = true;
        $this->curPage = $_SERVER['REQUEST_URI'];
        $this->detailUrl = $arResult["DETAIL_PAGE_URL"];


    }

    public function create(string $modelName): ?self
    {
        if(strpos($modelName, "General") !== false){
            $className = "Medreclama\Landing\General";
            $elementId = (int)substr($modelName, strpos($modelName, '_') + 1);
            return new $className($elementId);
        }else{
            $className = "Medreclama\Landing\\".$modelName;
            if(!class_exists($className)){
                return null;
            }
            return new $className($this->arResult);
        }
    }

    protected function getElement(int $elementId): ?Element
    {
        $res = \CIBlockElement::GetList([], ["ID" => $elementId]);

        if(!$bxElement = $res->GetNextElement()){
            return null;
        }

        $element = new Element;
        $element->fields = $bxElement->GetFields();
        $element->props = $bxElement->GetProperties();

        return $element;
    }

    protected function getElements(array $elementsId): array
    {
        $res = \CIBlockElement::GetList(["id" => $elementsId], ["ID" => $elementsId]);

        $elements = [];

        while($bxElement = $res->GetNextElement()){
            $element = new Element;
            $element->fields = $bxElement->GetFields();
            $element->props = $bxElement->GetProperties();
            $elements[] = $element;
        }

        return $elements;
    }



    protected function getElementsSectionsWorks(): array
    {
        $arSelect = [
            'NAME',
            'PROPERTY_SERVICE_BINDING',
            'PROPERTY_DOCTOR',
            'PROPERTY_PHOTO_AFTER',
            'PROPERTY_PHOTO_BEFORE',
            'PROPERTY_SERVICE_NAME',


        ];

        $elements = [];
        $doctorIds = [];
        $photoIds = [];
        $serviceIds = [];

        if (!empty($this->arResult['ID'])) {
            $this->elementId = $this->arResult['ID'];
        }
        if ($this->curPage == '/plastic/podtyazhka-grudi-test/') {
            $this->elementId = 1239;
        }
        if ($this->curPage == '/plastic/blefaroplastika-uslugi-test/') {
            $this->elementId = 1238;
        }
        if ($this->curPage == '/plastic/podtyazhka-litsa-test/') {
            $this->elementId = 1234;
        }
        if ($this->curPage == '/plastic/lipofiling-yagodits-test/') {
            $this->elementId = 4292;
        }
        if ($this->curPage == '/plastic/uvelichenie-grudi-usluga-test/') {
            $this->elementId = 1236;
        }
        if ($this->curPage == '/plastic/uvelichenie-grudi-ads/') {
            $this->elementId = 1236;
        }
        if ($this->curPage == '/plastic/liposaktsiya-tela-uslugi-test/') {
            $this->elementId = 1246;
        }
        if ($this->curPage == '/plastic/liposaktsiya-tela-uslugi-ads/') {
            $this->elementId = 1246;
        }
        if ($this->curPage == '/plastic/mammoplastika-test/') {
            $this->elementId = [1236,1239];
        }
        if ($this->curPage == '/plastic/mammoplastika-ads/') {
            $this->elementId = [1236,1239];
        }

        $arFilter = [
            "IBLOCK_ID" => 68,
            '=PROPERTY_SERVICE_BINDING' => $this->elementId,

        ];

        $res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($bxElement = $res->GetNextElement()) {
            $element = new Element;
            $fields = $bxElement->GetFields();

            // Собираем ID врачей и фото
            if (!empty($fields['PROPERTY_DOCTOR_VALUE'])) {
                $doctorIds[] = $fields['PROPERTY_DOCTOR_VALUE'];
            }
            if (!empty($fields['PROPERTY_PHOTO_AFTER_VALUE'])) {
                $photoIds[] = $fields['PROPERTY_PHOTO_AFTER_VALUE'];
            }
            if (!empty($fields['PROPERTY_PHOTO_BEFORE_VALUE'])) {
                $photoIds[] = $fields['PROPERTY_PHOTO_BEFORE_VALUE'];
            }
            if (!empty($fields['PROPERTY_SERVICE_BINDING_VALUE'])) {
                $serviceIds[] = $fields['PROPERTY_SERVICE_BINDING_VALUE'];
            }


            $element->fields = $fields;
            $elements[] = $element;
        }

        // Убираем дубликаты ID
        $doctorIds = array_unique($doctorIds);
        $photoIds = array_unique($photoIds);
        $serviceIds = array_unique($serviceIds);


        // Выполняем один запрос для получения всех врачей
        $doctors = [];
        if (!empty($doctorIds)) {
            $doctorFilter = [
                'IBLOCK_ID' => 6, // Замените на ID инфоблока врачей
                'ID' => $doctorIds,
            ];
            $doctorRes = \CIBlockElement::GetList([], $doctorFilter, false, false, ['ID', 'NAME','PROPERTY_IO_DOCTOR','PREVIEW_TEXT','DETAIL_PAGE_URL']);
            while ($doctor = $doctorRes->GetNext()) {
                $doctors[$doctor['ID']] = [
                    'NAME' => $doctor['NAME'] . ' ' . $doctor['PROPERTY_IO_DOCTOR_VALUE'],
                    'PREVIEW_TEXT' => $doctor['PREVIEW_TEXT'],
                    'DETAIL_PAGE_URL' => $doctor['DETAIL_PAGE_URL'],
                ];
            }
        }

        // Выполняем один запрос для получения всех фото
        $photos = [];
        if (!empty($photoIds)) {
            $photoRes = \CFile::GetList([], ['@ID' => implode(',', $photoIds)]);
            while ($photo = $photoRes->Fetch()) {
                $photos[$photo['ID']] = \CFile::GetPath($photo['ID']);
            }
        }

        // Выполняем запрос для получения всех услуг
        $services = [];
        if (!empty($serviceIds)) {
            $serviceFilter = [
                'IBLOCK_ID' => 47,
                'ID' => $serviceIds,
            ];
            $serviceRes = \CIBlockElement::GetList([], $serviceFilter, false, false, ['ID', 'NAME']);
            while ($service = $serviceRes->GetNext()) {
                $services[$service['ID']] = $service['NAME'];
            }
        }

        // Подставляем имена врачей и пути к фото
        foreach ($elements as &$element) {
            if (!empty($element->fields['PROPERTY_DOCTOR_VALUE'])) {
                $element->fields['PROPERTY_DOCTOR_VALUE'] = $doctors[$element->fields['PROPERTY_DOCTOR_VALUE']] ?? null;
            }
            if (!empty($element->fields['PREVIEW_TEXT'])) {
                $element->fields['PREVIEW_TEXT'] = $doctors[$element->fields['PREVIEW_TEXT']] ?? null;
            }
            if (!empty($element->fields['PROPERTY_PHOTO_AFTER_VALUE'])) {
                $element->fields['PROPERTY_PHOTO_AFTER_VALUE'] = $photos[$element->fields['PROPERTY_PHOTO_AFTER_VALUE']] ?? null;
            }
            if (!empty($element->fields['PROPERTY_PHOTO_BEFORE_VALUE'])) {
                $element->fields['PROPERTY_PHOTO_BEFORE_VALUE'] = $photos[$element->fields['PROPERTY_PHOTO_BEFORE_VALUE']] ?? null;
            }
            $element->fields['SERVICE_NAME'] = $element->fields['PROPERTY_SERVICE_NAME_VALUE'] ?? null;

            if (!empty($element->fields['PROPERTY_SERVICE_BINDING_VALUE']) && empty($element->fields['PROPERTY_SERVICE_NAME_VALUE'])) {
                $serviceId = $element->fields['PROPERTY_SERVICE_BINDING_VALUE'];
                $element->fields['SERVICE_NAME'] = $services[$serviceId] ?? null;
            } else {
                $element->fields['SERVICE_NAME'] = $element->fields['PROPERTY_SERVICE_NAME_VALUE'];
            }

        }


        return $elements;
    }


    protected function getImages($imagesId)
    {
        if(empty($imagesId)){
            return false;
        }

        $images = [];

        foreach ($imagesId as $imageId){
            $images[] = $this->getImageSrc($imageId);
        }

        return $images;
    }

    protected function getImageSrc($imageId)
    {
        return !empty($imageId) ? \CFile::GetPath($imageId) : null;
    }

    protected function getPrice($price)
    {
        return !empty($price) ? \StringsHelper::numberFormat($price) : null;
    }

    public function getShortClassName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function render(): string
    {
        $className = $this->getShortClassName();
        $templatePath = __DIR__."/../blocks/".$className."/template/index.php";

        $result = "";

        if($this->isShow){
            ob_start();
            // в шаблонах используется переменная имя которой совпадает с именем класса
            $objName = strtolower($className);
            $$objName = $this;
            require $templatePath;
            $result .= ob_get_clean();
        }

        if(!empty($this->additionalText)){
            $additionalText = $this->addUtmTags($this->additionalText);
            $result .= $additionalText;
        }

        return $result;
    }

    public function getVertSortedModelsClassNames(): array
    {
        $vertSort = $this->landingProps["VERT_SORT"]["VALUE"];

        if(empty($vertSort)){
            return $this->getDefaultVertSortedModelsClassNames();
        }

        $vertSortElements = $this->getElements($vertSort);
        $classNames = [];
        foreach ($vertSortElements as $vertSortElement){
            if($this->isGeneralBlock($vertSortElement->fields["IBLOCK_CODE"])){
                $classNames[] = "General_".$vertSortElement->fields["ID"];
            }else{
                $classNames[] = ucfirst($vertSortElement->fields["CODE"]);
            }
        }

        return $classNames;
    }

    public function isPageTypeLanding(): bool
    {
        $resLandingsHomeSection = \CIBlockSection::GetByID(SECTION_ID_LANDINGS_LANDINGS);
        $landingsHomeSection = $resLandingsHomeSection->GetNext();

        $arOrder = ["LEFT_MARGIN" => "ASC"];
        $arFilter = [
            "IBLOCK_ID" => IBLOCK_ID_LANDINGS,
            "LEFT_MARGIN" => $landingsHomeSection["LEFT_MARGIN"],
            "RIGHT_MARGIN" => $landingsHomeSection["RIGHT_MARGIN"]
        ];
        $bIncCnt = false;
        $Select = ["ID", "IBLOCK_ID", "NAME"];
        $NavStartParams = false;
        $rsSections = \CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

        while ($arSection = $rsSections->Fetch())
        {
            if((int)$arSection["ID"] == (int)$this->arResult["IBLOCK_SECTION_ID"]){
                return true;
            }
        }

        return false;
    }

    private function getDefaultVertSortedModelsClassNames(): array
    {
        return [
            "Banner",
            "BannerFacts",
            "Reasons",
            "Modeling",
            "Advantages",
            "Doctors",
            "Works",
            "History",
            "Prices",
            "Comfort",
            "Credit",
            "Stages",
            "Reviews",
            "Questions",
            "Form",
        ];
    }

    protected function getBlockId($property)
    {
        return !empty($property["VALUE"]) ? $property["VALUE"] : $property["DEFAULT_VALUE"];
    }

    private function isGeneralBlock($iblockCode): bool
    {
        return $iblockCode == "general";
    }

    private function addUtmTags($text)
    {
        if(strpos($text, "</form>") === false){
            return $text;
        }

        if(empty($_GET)){
            return $text;
        }

        $utmTags = [];
        foreach ($_GET as $paramName => $paramValue){
            if(strpos($paramName, 'utm_') !== false){
                $utmTags[] = '<input type="hidden" name="'.$paramName.'" value="'.$paramValue.'">';
            }
        }

        $utmTags = implode("\n", $utmTags);

        $text = str_replace("</form>", $utmTags."\n</form>", $text);

        return $text;
    }

}
