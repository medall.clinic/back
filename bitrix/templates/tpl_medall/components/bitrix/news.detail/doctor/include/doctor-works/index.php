<?
include __DIR__."/code/Work.php";

$works = [];

$worksId = $arResult["PROPERTIES"]["result_doctor"]["VALUE"];
if($worksId){
	foreach ($worksId as $workId){
		$element = getIbElement($workId);
		if($element && $element->fields["IBLOCK_ID"] == IBLOCK_ID_LANDINGS_WORKS){
			$works[] = new Medreclama\Doctor\Work($element);
		}
	}
}

if($works){
	include __DIR__."/template/index.php";
}
