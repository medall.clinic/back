<?
  $directions_enums = CIBlockPropertyEnum::GetList(
      Array("DEF"=>"DESC", "SORT"=>"ASC"),
      Array("IBLOCK_ID"=>6, "CODE"=>"direction_doctor")
  );
  $directions = array();
  while($ar_fields = $directions_enums->GetNext())
  {
      $direction = array(
      "ID" => $ar_fields["ID"],
      "XML_ID" => $ar_fields["XML_ID"],
      "NAME" => $ar_fields["VALUE"],
      "LIST" => array(),
      );
      $list = CIBlockElement::GetList (
      Array("ID" => "ASC"),
      Array("IBLOCK_ID" => 6, "PROPERTY_DIRECTION_DOCTOR" => $direction["ID"], 'ACTIVE' => 'Y'),
      false,
      false,
      Array('NAME', 'DETAIL_PAGE_URL', "PROPERTY_io_doctor", 'PROPERTY_DIRECTION_DOCTOR')
      );
      while($doctor = $list->GetNext())
      {
      $direction["LIST"][] = $doctor;
      }
      $directions[] = $direction;
  }
  $directions_json = array();
  foreach ($directions as $direction) {
      foreach ($direction["LIST"] as $item) {
      $directions_json[$direction["XML_ID"]][] = array(
          'name' => $item['NAME'].' '.$item['PROPERTY_IO_DOCTOR_VALUE'],
          'value' => $item['DETAIL_PAGE_URL']
      );
      }
  }
?>
<div class="doctor-select grid__cell grid__cell--xs--12 doctor-header__select">

  <script>window.doctorSelectStaff = <?=json_encode($directions_json)?></script>
  <div class="form-input form-input--select form-input--secondary doctor-select__input doctor-select__input--directions doctor-select__input doctor-select__input--directions">
    <select class="form-input__field">
    <? foreach ($directions as $direction) { ?>
      <option value="<?=$direction["XML_ID"]?>"><?=$direction["NAME"]?></option>
    <?}?>
    </select>
    <span class="label form-input__label">Специализация</span>
  </div>
  <div class="form-input form-input--select form-input--disabled form-input--secondary doctor-select__input doctor-select__input--items doctor-select__input doctor-select__input--items">
   <select class="form-input__field" disabled>
     <option value="">ФИО врача</option>
   </select>
    <span class="label form-input__label">ФИО врача</span>
  </div>
</div>
