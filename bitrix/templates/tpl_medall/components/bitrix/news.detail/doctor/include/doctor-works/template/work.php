<? /** @var $work Medreclama\Doctor\Work */ ?>
<a class="slide swiper-slide slide swiper-slide doctor-works__slide" href="<?=$work->image?>" data-glightbox="data-glightbox" data-gallery="doctor-works">
		<picture>
		<source srcset="<?= WebPHelper::getOrCreateWebPUrl($work->image, [600]) ?>" type="image/webp"/>
		<img src="<?= $work->image ?>" loading="lazy" decoding="async" alt="" class="doctor-works__image"/>
		</picture>
</a>
