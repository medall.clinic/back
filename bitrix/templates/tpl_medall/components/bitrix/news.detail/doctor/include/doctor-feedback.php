<?php if (!empty($feedback) && !empty($linkReview['~VALUE'])){ ?>
    <section class="container page-section page-section--background-color page-section--background-color--light home-feedback">
        <div class="content">
            <h2 class="page-section__title page-subsection text-align-center">Отзывы</h2>
            <div class="slider page-subsection home-feedback__slider">
                <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">
                        <?foreach($feedback as $arItem){?>
                            <div class="slide swiper-slide home-feedback__slide">
                                <? $res = CIBlockElement::GetByID($arItem);
                                $ar_res = $res->GetNext();
                                echo $ar_res['PREVIEW_TEXT'];
                                ?>
                            </div>
                        <?}?>
                    </div>
                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                    <div class="slider__pagination"></div>
                </div>
            </div>
            <a class="button home-feedback__button" href="<?php echo $linkReview['~VALUE'];?>">Оставить отзыв</a>
        </div>
    </section>
<?php }?>

<?php if (empty($feedback) && !empty($linkReview['~VALUE'])) { ?>
    <section class="container page-section page-section--background-color page-section--background-color--light home-feedback">
        <div class="content">
            <h2 class="page-section__title page-subsection text-align-center">Отзывы</h2>
            <a class="button home-feedback__button" href="<?php echo $linkReview['~VALUE'];?>">Оставить отзыв</a>
        </div>
    </section>
<?php } ?>

