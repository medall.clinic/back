<? /** @var array $works */ ?>
<section class="container page-section page-section--background-color page-section--background-color--dark doctor-works">
    <div class="content">
        <h2 class="page-subsection text-align-center doctor-works__title">работы специалиста</h2>
        <div class="slider page-subsection doctor-works__slider">
        <div class="slider__slides swiper">
            <div class="slider__wrapper swiper-wrapper">
                <? foreach ($works as $work): ?>
                    <? include __DIR__."/work.php" ?>
                <? endforeach ?>
            </div>
            <div class="slider__arrow slider__arrow--prev"></div>
            <div class="slider__arrow slider__arrow--next"></div>
            <div class="slider__pagination"></div>
        </div>
        </div>
    </div>
</section>
