<?php
$url = $arResult["PROPERTIES"]['video_block']['~VALUE'];
$pattern = "/https:\/\/youtu\.be\/(.*)/";
preg_match($pattern, $url, $matches);
$linkYoutube = $matches[1]; // содержит jsvHBcAPl38?si=P2nV_w9CJOuLSTaq
?>
<section class="container page-section">
    <div class="content grid grid--justify--center">
        <div class="grid__cell grid__cell--m--9 iframe-responsive">
            <div class="iframe-responsive__iframe">
                <iframe data-youtube="<?php echo $linkYoutube ?>" src="" allowfullscreen="allowfullscreen"></iframe>
            </div>
        </div>
    </div>
</section>
