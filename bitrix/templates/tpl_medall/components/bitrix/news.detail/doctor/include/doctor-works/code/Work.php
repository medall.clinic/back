<?
namespace Medreclama\Doctor;

class Work
{
	public $image;
	public $doctor;
	public $title;
	public $description1;
	public $description2;

	public function __construct($element)
	{
		$this->image = getIbElementImageSrc($element->fields["PREVIEW_PICTURE"]);
		$this->doctor = $element->props["DOCTOR"]["VALUE"];
		$this->title = $element->props["TITLE"]["VALUE"];

		if(!empty($element->props["DESCRIPTION_1"]["~VALUE"]["TEXT"])){
			$this->description1 = $element->props["DESCRIPTION_1"]["~VALUE"]["TEXT"];

        }

		if(!empty($element->props["DESCRIPTION_2"]["~VALUE"]["TEXT"])){
			$this->description2 = $element->props["DESCRIPTION_2"]["~VALUE"]["TEXT"];
		}

	}

}
