<section class="container page-section">
    <div class="content">
      <h2 class="page-subsection text-align-center">Вам могут быть интересны эти направления</h2>
        <div class="page-subsection grid grid--justify--center">
			<? foreach ($links as $link): ?>
        <?
          $res = \CIBlockElement::GetList([], ["ID" => $link]);
          $ar_res = $res->GetNextElement()->GetProperties();
          $text = $ar_res["TEXT"]["VALUE"];
        ?>
                <a
                    class="grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 link-block"
                    href="<?= $ar_res["LINK"]["VALUE"] ?>"
                >
                    <p class="link-block__image">
                        <img src="<?= \CFile::GetPath($ar_res["IMAGE"]["VALUE"]) ?>" alt="<?= $text ?>" title="<?= $text ?>">
                        <span class="button link-block__button">Подробнее</span>
                    </p>
                    <p><?= $text ?></p>
                </a>
			<? endforeach ?>
        </div>

    </div>
</section>