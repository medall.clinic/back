<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$idPicture = [];
$previewSrcWebp = '';
$name = '';
$this->setFrameMode(true);
$arFilter = array(
    "NAME" => $arResult['NAME'],
);
?>


<div class="container page-section">
    <div class="content">
        <?php if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
            <h1><?=$arResult["NAME"]?></h1>
        <?php endif;?>
        <div class="grid grid--justify--space-between page-subsection article">
            <div class="grid__cell grid__cell--m--8 article__inner">
                <div class="article__header">
                    <?php if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]){?>
                        <div class="article__header-top">
                            <?php $res = CIBlockElement::GetByID($arResult['PROPERTIES']['author_articles']['VALUE']);
                            if ($ar_res = $res->GetNext()) {
                                $db_props = CIBlockElement::GetProperty(6, $arResult['PROPERTIES']['author_articles']['VALUE'], array("sort" => "asc"));
                                if ($ar_props = $db_props->Fetch()) {
                                    $idPicture = CFile::GetFileArray($ar_res['PREVIEW_PICTURE']);
                                    $previewSrcWebp = WebPHelper::getOrCreateWebPUrl($idPicture['SRC'], [200, 200]);
                                    $name = $ar_res['NAME'];
                                } ?>
                            <?php } ?>
                            <?php if (!empty($arResult['PROPERTIES']['author_articles']['VALUE'])){?>
                                <div class="article__author">
                                    <picture>
                                        <source srcset="<?php echo $previewSrcWebp ?>" type="image/webp">
                                        <source srcset="<?php echo $idPicture['SRC']?>">
                                        <img class="article__author-image" src="<?php echo $idPicture['SRC']?>" loading="lazy" width="60px"  height="60px" decoding="async" alt="">
                                    </picture>
                                    <a class="article__author-name" href="<?php echo $ar_res['DETAIL_PAGE_URL']?>"><?php echo $name ?></a>
                                </div>
                                <div class="article__date">
                                    <?php if (!empty($arResult["DISPLAY_ACTIVE_FROM"])): ?>
                                        <?=$arResult["DISPLAY_ACTIVE_FROM"]?>
                                    <?php endif; ?>
                                </div>
                            <?php } else {?>
                                <div class="article__author">

                                </div>
                                <div class="article__date">
                                    <?php if (!empty($arResult["DISPLAY_ACTIVE_FROM"])): ?>
                                        <?=$arResult["DISPLAY_ACTIVE_FROM"]?>
                                    <?php endif; ?>
                                </div>

                            <?php } ?>


                        </div>

                    <?php } ?>


                    <div class="article__header-text">
                        <?php if (!empty($arResult["PREVIEW_TEXT"])): ?>
                            <p><?php echo $arResult["PREVIEW_TEXT"]; ?></p>
                        <?php endif; ?>
                    </div>

                    <?php if (!empty($arResult["DETAIL_PICTURE"]["SRC"])){ ?>
                        <picture>
                            <source srcset="
                            <?php echo WebPHelper::getOrCreateWebPUrl($arResult["DETAIL_PICTURE"]["SRC"])?>"
                                    type="image/webp">
                            <source srcset="<?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>">
                            <img class="article__image" src="<?php echo $arResult["DETAIL_PICTURE"]["SRC"]?>"
                                 loading="lazy" decoding="async" alt="<?php echo $arResult['NAME']?>">
                        </picture>

                    <?php } ?>

                </div>
                <div class="article__body">
                    <?php if (!empty($arResult["DETAIL_TEXT"])) { ?>
                        <?php echo $arResult["DETAIL_TEXT"]; ?>
                    <?php } ?>

                    <div class="article__footer">

                        <a class="article__back" href="https://medall.clinic/articles/">Назад</a>
                        <div class="article__hashtag">
                            <?php if (!empty($arResult["FIELDS"])){ ?>
                                <?php foreach($arResult["FIELDS"] as $code => $value){ ?>
                                    <?php
                                    $arrStr = explode(' ',$value);
                                    $arrStr[0] = str_replace(',',' ',$arrStr[0]);
                                    ?>
                                        <?php if (
                                                $arrStr[0] != '(ceo@medall.clinic)'
                                                && $arrStr[0] != '(marketing@medall.clinic)' && !empty($value)
                                    ){?>

                                        <a href="/search/index.php?tags=<?=$arrStr[0]?>" target=_blank># <?php echo $arrStr[0];?></a>

                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="grid__cell grid__cell--m--4 article__aside">


                <div class="grid page-subsection">
                    <?php $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "other_articls",
                        array(
                            "ACTIVE_DATE_FORMAT" => "22/02/07",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "N",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array(
                                0 => "TAGS",
                                1 => "PREVIEW_PICTURE",
                                2 => "DETAIL_TEXT",
                                3 => "DETAIL_PICTURE",
                                4 => "",
                            ),
                            "FILTER_NAME" => "arFilter",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "9",
                            "IBLOCK_TYPE" => "articles",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "2",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array(
                                0 => "current_articles",
                                1 => "display_page_articles",
                                2 => "direction_articles",
                                3 => "fix",
                                4 => "direction",
                                5 => "",
                            ),
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "RAND",
                            "SORT_BY2" => "RAND",
                            "SORT_ORDER1" => "RAND",
                            "SORT_ORDER2" => "RAND",
                            "STRICT_SECTION_CHECK" => "N",
                            "COMPONENT_TEMPLATE" => "other_articls",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO"
                        ),
                        false
                    );?>
                </div>

            </div>

        </div>
    </div>

</div>
