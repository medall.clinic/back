<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<picture>

	<source
		srcset="<?= WebPHelper::getOrCreateWebPUrl($doctor->image, [ 400 ]) ?>" type="image/webp"/>

	<img
		src="<?= $doctor->image ?>"
		loading="lazy"
		decoding="async"
		fetchpriority="low"
		alt="<?= $doctor->name ?>"
		class="home-staff__image"
	/>

</picture>
