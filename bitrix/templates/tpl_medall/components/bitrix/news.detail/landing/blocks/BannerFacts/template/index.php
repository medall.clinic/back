<? /** @var $bannerFact Medreclama\Landing\BannerFact */ ?>
<? /** @var $bannerfacts Medreclama\Landing\BannerFacts */ ?>
<section class="container page-section landing-facts"
         <?php if (!empty($bannerfacts->blockId)){?>
     id="<?php echo $bannerfacts->blockId ?>">

    <?php } ?>

    <div class="content">

		<? if(empty($bannerfacts->images)): ?>
            <div class="landing-facts__background"></div>
		<? elseif(count($bannerfacts->images) == 1): ?>
            <div class="landing-facts__background">
                <img src="<?= reset($bannerfacts->images) ?>" alt=""/>
            </div>
		<? else: ?>
            <div class="slider slider--full-width landing-facts__background">
                <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">
						<? foreach ($bannerfacts->images as $imageSrc): ?>
                        <div class="slide swiper-slide landing-facts__slide">
                            <img src="<?= $imageSrc ?>" alt=""/>
                        </div>
						<? endforeach ?>
                    </div>
                    <div class="slider__pagination"></div>
                </div>
            </div>
		<? endif ?>

        <? if($bannerfacts->items): ?>
            <div class="container">
                <div class="content">
                    <div class="grid grid--justify--center grid--nowrap--hd grid--nowrap--xl grid--nowrap--l landing-facts__list">
						<? foreach($bannerfacts->items as $bannerFact): ?>
                            <div class="grid__cell grid__cell--l--auto grid__cell--m--6 grid__cell--xs--12 landing-facts__item">
                                <div class="landing-facts__name"><?= $bannerFact->title ?></div>
                                <div class="landing-facts__body"><?= $bannerFact->text ?></div>
                            </div>
						<? endforeach ?>
                    </div>
                </div>
            </div>
		<? endif ?>

    </div>
</section>