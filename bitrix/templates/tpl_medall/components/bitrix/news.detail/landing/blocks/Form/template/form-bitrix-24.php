<?$url = $_SERVER['REQUEST_URI'];?>
<? /* <form class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form contacts_to_bitrix24">

    <h3 class="h4 form__header">Оставьте контактные данные и мы обязательно свяжемся с вами</h3>

    <label class="form-input form-input--text form__input form__input">
        <input class="form-input__field" type="text" name="name" required placeholder="Имя *">
    </label>

    <label class="form-input form-input--text form__input form__input">
        <? if($url == "/dentistry/endodontiya/"): ?>
            <input class="form-input__field" type="text" name="second_name" placeholder="Фамилия">
        <? else: ?>
            <input class="form-input__field" type="text" name="second_name" required placeholder="Фамилия *">
        <? endif ?>
    </label>

    <label class="form-input form-input--tel form__input form__input">
        <input class="form-input__field" type="tel" name="phone" required placeholder="Телефон *">
    </label>

    <label class="form-input form-input--email form__input form__input">
        <input class="form-input__field" type="email" name="email" required placeholder="E-mail">
    </label>

    <label class="form-input form-input--textarea form__input form__input">
        <textarea class="form-input__field" name="comments" placeholder="Комментарий"></textarea>
    </label>

    <p class="form__agreement">Нажимая кнопку «Отправить» я даю согласие на <a href="/">обработку персональных данных</a>.</p>

    <button class="button p form__submit" type="submit">Отправить</button> */ ?>

<form
    class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form form-ajax home-form"
    data-is-validate="true"
    data-is-answer-in-popup="true"
    data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации"
>

	<label class="form-input form-input--text form__input form__input">
		<input class="form-input__field" type="text" name="name" data-validation-required>
		<span class="label form-input__label">ФИО</span>
	</label>

	<label class="form-input form-input--tel form__input form__input">
		<input class="form-input__field" type="tel" name="phone" data-validation-required>
		<span class="label form-input__label">Телефон</span>
	</label>

	<? /* <label class="form-input form-input--email form__input form__input">
		<input class="form-input__field" type="email" name="email">
		<span class="label form-input__label">Электронная почта</span>
	</label> */?>

	<label class="form-input form-input--textarea form__input form__input">
		<textarea class="form-input__field" name="comment"></textarea>
		<span class="label form-input__label">Комментарии</span>
	</label>

	<button class="button form__submit" type="submit">Оставить заявку</button>

	<div class="form__agreement home-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</div>

    <input type="hidden" name="title" value="<?= $form->title ?>">
    <input type="hidden" name="action" value="consultation_register">


    <? if($form->utmTags): ?>
		<? foreach ($form->utmTags as $utmTag): ?>
            <input type="hidden" name="<?= $utmTag->name ?>" value="<?= $utmTag->value ?>">
		<? endforeach ?>
	<? endif ?>

</form>
