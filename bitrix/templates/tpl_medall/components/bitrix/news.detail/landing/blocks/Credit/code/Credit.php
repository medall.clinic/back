<?
namespace Medreclama\Landing;

class Credit extends Landing
{
	public $text;
	public $images;

	public function __construct($arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["CREDIT_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["CREDIT_BLOCK_ID"]);
		$this->text = $this->landingProps["CREDIT_TEXT"]["~VALUE"] ? $this->landingProps["CREDIT_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->button = new Button($this->landingProps["CREDIT_BUTTON_TEXT"], $this->landingProps["CREDIT_BUTTON_LINK"], $this->landingProps["CREDIT_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["CREDIT_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["CREDIT_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["CREDIT_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["CREDIT_MENU_TITLE"]["VALUE"], $this->landingProps["CREDIT_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(!$this->text){
			return false;
		}

		return true;
	}

}
