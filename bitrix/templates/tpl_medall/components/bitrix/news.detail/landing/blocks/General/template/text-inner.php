<?php
/** @var $general Medreclama\Landing\General */
/** @var $button Medreclama\Landing\Button */

$button = $general->button;
$url = $general->linkYoutube;

$linkYoutube = $url;
?>

<?php

/** start Блок стандартной верски. **/

if($general->title && empty($general->linkYoutube)) { ?>
    <h2 class="landing-block__header"><?php echo $general->title ?></h2>
<?php } ?>

<?php if($general->images) { ?>
    <?php // include __DIR__."/images-mobile.php" ?>
<?php } ?>

<?php if($general->text && empty($general->linkYoutube)) { ?>
    <div class="landing-block__body"><?php echo $general->text ?></div>
<?php }

/** end Блок стандартной верски. **/

?>

<?php if (!empty($general->linkYoutube) && !empty($general->title) && !empty($general->text)) {

    /* блок верски если есть ссылка на видео и проставлен заголовок и текст */

    ?>

    <h2 class="landing-block__header text-align-center"><?php echo $general->title ?></h2>
    <div class="grid grid--justify--center">
        <div class="grid__cell grid__cell--m--9 iframe-responsive">
            <div class="iframe-responsive__iframe">
                <iframe data-youtube="<?php echo $linkYoutube ?>" src="" allowfullscreen="allowfullscreen"></iframe>
            </div>
        </div>
        <div class="landing-block__body"><?php echo $general->text ?></div>
    </div>
<?php } ?>

<?php if (!empty($general->linkYoutube) && !empty($general->title) && empty($general->text)) {

    /* блок верски если есть ссылка на видео и проставлен заголовок */

   ?>

    <h2 class="landing-block__header text-align-center"><?php echo $general->title ?></h2>
    <div class="grid grid--justify--center">
        <div class="grid__cell grid__cell--m--9 iframe-responsive">
            <div class="iframe-responsive__iframe">
                <iframe data-youtube="<?php echo $linkYoutube ?>" src="" allowfullscreen="allowfullscreen"></iframe>
            </div>
        </div>
    </div>
<?php }?>

   <?php if(!empty($general->linkYoutube) && empty($general->title) && empty($general->text)) {

    /* блок верски если есть ссылка но нет заголовка */

  ?>
    <div class="grid grid--justify--center">
        <div class="grid__cell grid__cell--m--9 iframe-responsive">
            <div class="iframe-responsive__iframe">
                <iframe data-youtube="<?php echo $linkYoutube ?>" src="" allowfullscreen="allowfullscreen"></iframe>
            </div>
        </div>
    </div>
<?php } ?>





<?php if($button->isExist()): ?>
    <?= $button->render('landing-block__button') ?>
<?php endif ?>
