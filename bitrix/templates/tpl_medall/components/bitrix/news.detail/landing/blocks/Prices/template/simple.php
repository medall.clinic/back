<? /** @var $price Medreclama\Landing\Price */ ?>
<? /** @var $prices Medreclama\Landing\Prices */ ?>
<div class="table-price">
	<? foreach($prices->simpleItems as $price): ?>
        <div class="table-price__row">
            <div class="table-price__name"><?= $price->title ?></div>
            <div class="table-price__value"><span><?= $price->value ?></span> ₽</div>
        </div>
	<? endforeach ?>
</div>