<? /** @var $doctor Medreclama\Landing\Doctor */ ?>

<div class="doctor-header">

        <div class="grid grid--justify--center doctor-header__inner">
        <? if($doctor->image): ?>
          <a class="doctor-header__image-link" href="<?= $doctor->url ?>">
            <picture>
              <source
                srcset="<?= WebPHelper::getOrCreateWebPUrl($doctor->image, [560, 1120]) ?>"
                type="image/webp"
              >
              <img class="grid__cell grid__cell--s--5 grid__cell--xs--12 doctor-header__image" src="<?=$doctor->image?>" loading="lazy" decoding="async" alt="<?=$doctor->name?>"/>
            </picture>
          </a>
        <? endif ?>
          <div class="grid__cell grid__cell--s--7 grid__cell--xs--12 doctor-header__info">
            <div class="h1 doctor-header__name">
              <a href="<?= $doctor->url ?>"><?= $doctor->name ?></a>
            </div>
            <? if ($doctor->description): ?>
              <div class="doctor-header__description"><?= $doctor->description ?></div>
            <? endif; ?>
            <? if ($doctor->workExperience): ?>
              <div class="doctor-header__experience">стаж работы: <?=$doctor->workExperience?></div>
            <? endif; ?>
            <a class="button doctor-header__button" href="#landing-form">Записаться</a>
            <a class="button button--secondary doctor-header__button" href="<?= $doctor->url ?>">Подробнее</a>
          </div>
          <? if ($doctor->instagram || $doctor->vk): ?>
            <div class="grid__cell grid__cell--s--5 grid__cell--xs--12 grid grid--justify--center doctor-header__socials">
              <? if ($doctor->vk): ?>
                <a class="link-icon link-icon--icon--vk" href="https://vk.com/<?= $doctor->vk ?>"></a>
              <? endif; ?>
              <? if ($doctor->instagram): ?>
                <a class="link-icon link-icon--icon--instagram" href="https://instagram.com/<?= $doctor->instagram ?>"></a>
              <? endif; ?>
            </div>
            <? if ($doctor->instagram): ?>
              <div class="grid__cell grid__cell--xs--12 doctor-header__clarification">* Социальная сеть Instagram запрещена на территории РФ.</div>
            <? endif; ?>
          <? endif; ?>
        </div>
</div>
