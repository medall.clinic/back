<? /** @var $comfort Medreclama\Landing\Comfort */ ?>
<? /*
<section class="page-section page-section--background-color page-section--background-color--dark" id="<?= $comfort->blockId ?>">
    <div class="container">
        <div class="content">
            <div class="page-subsection">
                <div class="grid grid--padding-y grid--align-center">

                    <? if($comfort->images): ?>
                    <div class="grid__cell grid__cell--m--6 hidden-s hidden-xs">
                        <div class="slider slider--simple splide">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <? foreach($comfort->images as $image): ?>
                                    <li class="splide__slide">
                                        <div class="slider__image">
                                            <img src="<?= $image ?>" alt="">
                                        </div>
                                    </li>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
					<? endif ?>

                    <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">

                        <? if($comfort->title): ?>
                        <div class="page-subsection">
                            <h2><?= $comfort->title ?></h2>
                        </div>
                        <? endif ?>

                        <? if($comfort->images): ?>
                        <div class="page-subsection hidden-hd hidden-xl hidden-l hidden-m">
                            <div class="slider slider--simple splide">
                                <div class="splide__track">
                                    <ul class="splide__list">
                                        <? foreach($comfort->images as $image): ?>
                                        <li class="splide__slide">
                                            <div class="slider__image">
                                                <img src="<?= $image ?>" alt="">
                                            </div>
                                        </li>
										<? endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
						<? endif ?>

                        <? if($comfort->text): ?>
                        <div class="page-subsection">
							<?= $comfort->text ?>
                        </div>
                        <? endif ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section> */ ?>
      <section class="container page-section page-section--background-color page-section--background-color--dark landing-comfort">
        <div class="content">
          <div class="page-subsection">
            <div class="grid grid--align-items--center">
                <? if($comfort->images): ?>
                <div class="grid__cell grid__cell--m--6 hidden-s hidden-xs">
                    <div class="slider landing-comfort__slider">
                    <div class="slider__slides swiper">
                        <div class="slider__wrapper swiper-wrapper">
                            <? foreach($comfort->images as $image): ?>
                                <img src="<?=WebPHelper::getOrCreateWebPUrl($image, [ 1200 ])?>" alt="" class="slide landing-comfort__slide swiper-slide" loading="lazy"  decoding="async">
                            <? endforeach ?>
                        </div>
                        <div class="slider__arrow slider__arrow--prev"></div>
                        <div class="slider__arrow slider__arrow--next"></div>
                        <div class="slider__pagination"></div>
                    </div>
                    </div>
                </div>
                <? endif ?>
              <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">                      
                <? if($comfort->title): ?>
                    <h2 class="page-subsection">
                        <?= $comfort->title ?>
                    </h2>
                <? endif ?>
                <div class="slider landing-comfort__slider page-subsection hidden-l hidden-m">
                  <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">
                        <? foreach($comfort->images as $image): ?>
                            <img src="<?=WebPHelper::getOrCreateWebPUrl($image, [ 1200 ])?>" alt="" class="slide landing-comfort__slide swiper-slide" loading="lazy" decoding="async">
                        <? endforeach ?>
                    </div>
                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                    <div class="slider__pagination"></div>
                  </div>
                </div>
                <? if($comfort->text): ?>
                    <div class="page-subsection">
                        <?= $comfort->text ?>
                    </div>
                <? endif ?>
              </div>
            </div>
          </div>
        </div>
      </section>
