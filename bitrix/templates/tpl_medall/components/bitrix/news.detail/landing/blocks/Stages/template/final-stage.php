<div class="landing-result grid grid--no-gap grid--align-items--center page-subsection">

	<? if($finalStage->text): ?>
		<div class="grid__cell grid__cell--m--7 grid__cell--s--6 grid__cell--xs--12 landing-result__info"><?= $finalStage->text ?></div>
	<? endif ?>

	<? if($finalStage->image): ?>
		<div class="grid__cell grid__cell--m--5 grid__cell--s--6 grid__cell--xs--12 landing-result__image">
			<picture>
				<source srcset="<?= WebPHelper::getOrCreateWebPUrl($finalStage->image, [700]) ?>" type="image/webp"/>
				<img src="<?= $finalStage->image ?>" loading="lazy" decoding="async" alt="">
			</picture>
		</div>
	<? endif ?>

</div>
