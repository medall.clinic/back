<?/** @var Medreclama\Landing\Reason $reason */?>
<? if($reason->text): ?>
    <div class="grid__cell grid__cell--xs--12 landing-reason__info">
        <?= $reason->text ?>
    </div>
<? endif ?>