<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $doctors->button ?>
<div class="page-subsection grid grid--justify--center">
    <a
        class="button"
        href="<?= $button->link ?>"
		<?= $button->isFancybox ? "data-fancybox" : "" ?>
    ><?= $button->text ?></a>
</div>