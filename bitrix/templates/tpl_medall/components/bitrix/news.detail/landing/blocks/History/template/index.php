<? /** @var $history Medreclama\Landing\History */ ?>
<section class="container page-section page-section--text--align--center"

    <?php
    if (!empty($history->blockId)){?>
         id="<?php echo $history->blockId ?>"
    <?php } ?>>
    <div class="content">

		<? if($history->title): ?>
            <div class="page-subsection">
                <h2><?= $history->title ?></h2>
            </div>
		<? endif ?>

        <? if($history->items): ?>
            <div class="page-subsection">
                <div class="grid grid--justify--center">
					<? foreach ($history->items as $item): ?>
                        <div class="grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12">
							<?= $item ?>
                        </div>
					<? endforeach ?>
                </div>
            </div>
		<? endif ?>

        <script async src="https://www.instagram.com/embed.js" data-skip-moving="true"></script>

    </div>
</section>