<? /** @var $link Medreclama\Landing\Link */ ?>
<? /** @var $links Medreclama\Landing\Links */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $links->button ?>
<section class="container page-section"
         <?php if (!empty($links->blockId)){?>
    id="<?php echo $links->blockId ?>"
    <?php } ?>>

    <div class="content">

		<? if($links->title): ?>
            <h2 class="page-subsection text-align-center"><?= $links->title ?></h2>
		<? endif ?>

        <div class="page-subsection grid grid--justify--center">
			<? foreach ($links->items as $link): ?>
                <a
                    class="grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 link-block"
                    href="<?= $link->link ?>"
                >
                    <div class="link-block__image">
                        <picture>
                            <source srcset="<?= WebPHelper::getOrCreateWebPUrl($link->image, [ 700 ]) ?>" type="image/webp"/>
                            <img src="<?= $link->image ?>" alt="<?= $link->text ?>" title="<?= $link->text ?>" loading="lazy" decoding="async">
                        </picture>
                        <span class="button link-block__button">Подробнее</span>
                    </div>
                    <p class="link-block__name"><?= $link->text ?></p>
                </a>
			<? endforeach ?>
        </div>

    </div>
</section>
