<? /** @var $work Medreclama\Landing\Work */ ?>

<div class="slide swiper-slide landing-works__slide">

	<? if($work->image): ?>
        <div class="landing-works__image">
            <picture>
                <source srcset="<?=WebPHelper::getOrCreateWebPUrl($work->image, [ 800 ])?>"/>
                <img src="<?= $work->image ?>" loading="lazy" decoding="async" alt="<?= $work->doctor ?>, <?= $work->title ?>">
            </picture>
        </div>
	<? endif ?>

	<? if($work->description1): ?>
        <div class="grid grid--no-gap landing-works__description">
			<? if(empty($work->description2)): ?>
                <div class="grid__cell grid__cell--xs--12"><?= $work->description1 ?></div>
            <? else: ?>
                <div class="grid__cell grid__cell--l--6 grid__cell--xs--12"><?= $work->description1 ?></div>
                <div class="grid__cell grid__cell--l--6 grid__cell--xs--12"><?= $work->description2 ?></div>
			<? endif ?>
        </div>
	<? endif ?>

    <? if($work->doctor or $work->title): ?>
        <div class="landing-works__info">
            <? if($work->doctor): ?>
                <strong>Врач: </strong><?= $work->doctor ?><br>
            <? endif ?>
            <? if($work->title): ?>
                <strong>Было выполнено: </strong><?= $work->title ?>
            <? endif ?>
        </div>
	<? endif ?>

</div>

