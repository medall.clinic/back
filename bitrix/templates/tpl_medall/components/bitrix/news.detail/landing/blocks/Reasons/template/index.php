<? /** @var $reason Medreclama\Landing\Reason */ ?>
<? /** @var $reasons Medreclama\Landing\Reasons */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $reasons->button ?>
<section class="container page-section page-section--reasons"
    <?php if (!empty($reasons->blockId)){?>
        id="<?php echo $reasons->blockId ?>"
    <?php } ?>>

    <div class="content">
		<? if($reasons->title): ?>
            <h2 class="page-subsection text-align-center"><?= $reasons->title ?></h2>
		<? endif ?>

        <? if($reasons->text): ?>
            <div class="page-subsection">
				<?= $reasons->text ?>
            </div>
		<? endif ?>

        <? if($reasons->items): ?>
            <ul class="grid grid--no-gap landing-reasons page-subsection"> 
                <? foreach ($reasons->items as $reason): ?>
                    <? include __DIR__."/reason.php" ?>
                <? endforeach ?>
            </ul>
		<? endif ?>
        <? if($reasons->hasListItems()): ?>
            <div class="page-subsection">
                <div class="grid grid--justify--center">

					<? if($reasons->listItemsPros): ?>
                        <div class="grid__cell grid__cell--l--5 grid__cell--xs--12">
                            <div class="indications">
                                <h3 class="indications__title">Показания</h3>
                                <ul class="list-unordered indications__list">
                                    <? foreach ($reasons->listItemsPros as $listItemPro): ?>
                                        <li class="list-unordered__item indications__item"><?= $listItemPro ?></li>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    <? endif ?>

					<? if($reasons->listItemsCons): ?>
                        <div class="grid__cell grid__cell--l--5 grid__cell--xs--12">
                            <div class="indications indications--contraindications">
                                <h3 class="indications__title">Противопоказания</h3>
                                <ul class="list-unordered indications__list">
                                    <? foreach ($reasons->listItemsCons as $listItemCon): ?>
                                        <li class="list-unordered__item indications__item"><?= $listItemCon ?></li>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    <? endif ?>
                </div>
            </div>
        <? endif ?>
		<? if($button->isExist()): ?>
            <div class="page-subsection grid grid--justify--center">
				<?= $button->render('grid__cell') ?>
            </div>
		<? endif ?>

    </div>
</section>