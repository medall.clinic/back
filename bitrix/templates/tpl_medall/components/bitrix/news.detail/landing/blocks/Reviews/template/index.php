<? /** @var $reviews Medreclama\Landing\Reviews */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $reviews->button ?>
<section class="page-section"

    <?php if (!empty($reviews->blockId)){?>
        id="<?php echo $reviews->blockId ?>"
    <?php } ?>>

	<? if($reviews->title): ?>
        <div class="page-subsection container text-align-center">
            <div class="content">
                <h2><?= $reviews->title ?></h2>
				<?= $reviews->description ?>
            </div>
        </div>
	<? endif ?>

    <? if($reviews->items): ?>
        <div class="page-subsection">
            <div class="slider slider--full-width landing-stories">
                <div class="slider__slides swiper">

                    <div class="slider__wrapper swiper-wrapper">
						<? foreach($reviews->items as $review): ?>
							<? include __DIR__."/review.php" ?>
						<? endforeach ?>
                    </div>

                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                    <div class="slider__pagination"></div>

                </div>
            </div>
        </div>
	<? endif ?>

    <? if($button->isExist()): ?>
        <div class="page-subsection grid grid--justify--center">
			<?= $button->render('grid__cell') ?>
        </div>
	<? endif ?>
</section>