<? /** @var $advantages Medreclama\Landing\Advantages */ ?>
<div class="grid grid--no-gap grid--cols--10 landing-advantages page-subsection">

	<? foreach ($advantages->images as $k => $image): ?>
        <? if(!$k || $k == 2): ?>
            <div class="grid__cell grid__cell--m--2 grid__cell--s--3 grid__cell--xs--5 landing-advantages__item">
                <img src="<?= $image ?>" alt=""/>
            </div>
	    <? else: ?>
            <div class="grid__cell grid__cell--m--4 grid__cell--s--4 grid__cell--xs--10 landing-advantages__item">
                <img src="<?= $image ?>" alt=""/>
            </div>
		<? endif ?>
	<? endforeach ?>

    <div class="grid__cell grid__cell--l--hidden grid__cell--m--2 grid__cell--s--hidden grid__cell--xs--hidden landing-advantages__end"></div>

</div>