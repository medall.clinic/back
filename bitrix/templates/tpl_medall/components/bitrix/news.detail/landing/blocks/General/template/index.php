<? /** @var $general Medreclama\Landing\General */ ?>
<div
    class="container page-section landing-block
        <?= $general->isColorDark ? 'page-section--background-color page-section--background-color--dark' : '' ?>
        <?= $general->isColorLight ? 'page-section--background-color page-section--background-color--light' : '' ?>
        <?= $general->addClass?>
    "
    <?php
    if (!empty($general->blockId)){?>
        id="<?= $general->blockId ?>"
    <?php } ?>>
    <div class="content grid grid--justify--center">
		<? if($general->images OR $general->isFormEnabled): ?>
            <? if($this->isImagesOrFormOnLeft): ?>
			    <? if($general->isFormEnabled): ?>
					<? include __DIR__."/form.php" ?>
					<? include __DIR__."/text-narrow.php" ?>
				<? else: ?>
					<? include __DIR__."/images-desktop.php" ?>
					<? include __DIR__."/text-narrow.php" ?>
				<? endif ?>
            <? else: ?>
				<? if($general->isFormEnabled): ?>
					<? include __DIR__."/text-narrow.php" ?>
					<? include __DIR__."/form.php" ?>
				<? else: ?>
					<? include __DIR__."/text-narrow.php" ?>
					<? include __DIR__."/images-desktop.php" ?>
				<? endif ?>
            <? endif ?>
		<? else: ?>
			<? include __DIR__."/text-wide.php" ?>
		<? endif ?>
    </div>
</div>
