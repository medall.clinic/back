<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<? foreach ($doctors->items as $doctor): ?>
	<? if(!$doctor->hasDetails()){continue;} ?>
	<div class="modal landing-team__pop-up" id="doctor-<?= $doctor->id ?>">
		<div class="modal__inner">

			<div class="grid grid--no-gap">
				<? include __DIR__."/block-info.php" ?>
				<? include __DIR__."/block-details.php" ?>
			</div>

			<button class="modal__close" type="button"></button>

		</div>
	</div>
<? endforeach ?>