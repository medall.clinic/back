<?
namespace Medreclama\Landing;

class Works extends Landing
{
	public $items;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["WORKS_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["WORKS_BLOCK_ID"]);
		$this->items = $this->getItems($this->landingProps["WORKS_ITEMS"]["VALUE"]);
		$this->button = new Button($this->landingProps["WORKS_BUTTON_TEXT"], $this->landingProps["WORKS_BUTTON_LINK"], $this->landingProps["WORKS_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["WORKS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["WORKS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["WORKS_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["WORKS_MENU_TITLE"]["VALUE"], $this->landingProps["WORKS_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Work;
			$item->image = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
			$item->doctor = $element->props["DOCTOR"]["VALUE"];
			$item->title = $element->props["TITLE"]["VALUE"];

			if(!empty($element->props["DESCRIPTION_1"]["~VALUE"]["TEXT"])){
				$item->description1 = $element->props["DESCRIPTION_1"]["~VALUE"]["TEXT"];
			}

			if(!empty($element->props["DESCRIPTION_2"]["~VALUE"]["TEXT"])){
				$item->description2 = $element->props["DESCRIPTION_2"]["~VALUE"]["TEXT"];
			}

			$items[] = $item;
		}

		return $items;
	}

	private function isShow()
	{
		if(!$this->items){
			return false;
		}

		return true;
	}


}
