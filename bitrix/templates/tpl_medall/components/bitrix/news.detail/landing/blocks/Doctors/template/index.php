<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<?
    $section_classes = '';
    if (count($doctors->items) === 1) {
        $section_classes = '  page-section--background-color page-section--background-color--light page-section--logo';
    }

?>
<section class="container page-section<?=$section_classes?>"
    <?php
    if (!empty($doctors->blockId)){?>
        id="<?php echo $doctors->blockId ?>"

    <?php } ?>>
    <div class="content">

		<? if($doctors->title): ?>
            <h2 class="page-subsection text-align-center"><?= $doctors->title ?></h2>
		<? endif ?>

        <? if($doctors->text): ?>
            <? /* <div class="page-subsection grid grid--padding--y"> */?>

            <div class="page-subsection">
                <?= $doctors->text ?>
            </div>
		<? endif ?>

        <? if($doctors->items): ?>
            <div class="page-subsection home-staff">
                <?if(count($doctors->items) > 1):?>
                    <? include __DIR__."/slider.php" ?>
                <?else:?>
                    <?$doctor = $doctors->items[0]?>
                    <? include __DIR__."/banner.php" ?>
                <?endif;?>
                <? include __DIR__."/blocks.php" ?>
            </div>
		<? endif ?>

		<? if($doctors->button->isExist()): ?>
			<? include __DIR__."/button.php" ?>
		<? endif ?>

    </div>
</section>
