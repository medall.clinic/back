<? /** @var $button Medreclama\Landing\Button */ ?>
<a
	class="button <?= $addClass ?>"
	href="<?= $button->link ?>"
	<?= $button->isFancybox ? "data-glightbox data-height=\"fit-content\" data-width=\"600px\"" : "" ?>
><?= $button->text ?></a>
