<? /** @var $banner Medreclama\Landing\Banner */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $banner->button ?>

<section class="container page-section landing-header"
    <?php
    if (!empty($banner->blockId)){?>
         id="<?php echo $banner->blockId ?>"

    <?php } ?>>

    <div class="content">

        <div class="grid grid--align-items--center">
            <div class="grid__cell grid__cell--m--7 grid__cell--s--6 grid__cell--xs--12 landing-header__info">

				<? if($banner->title): ?>
                    <h1 class="landing-header__title"><?= $banner->title ?></h1>
				<? endif ?>

				<? if($banner->subtitle): ?>
                    <div class="landing-header__subtitle"><?= $banner->subtitle ?></div>
				<? endif ?>

				<? if($banner->text): ?>
                    <div class="landing-header__body">
						<?= $banner->text ?>
                    </div>
				<? endif ?>

				<? if($button->isExist()): ?>
                    <?= $button->render('landing-header__button') ?>
				<? endif ?>

            </div>

            <div class="grid__cell grid__cell--m--5 grid__cell--s--6 grid__cell--xs--12 landing-header__image">

                <picture>
                    <source srcset="<?= WebPHelper::getOrCreateWebPUrl($banner->image, [ 800 ]) ?>" type="image/webp"/>
                    <img fetchpriority="high" width="800" height="800" src="<?= $banner->image ?>" alt="<?= $banner->title ?>"/>
                </picture>
            </div>
        </div>

		<? if($button->isExist()): ?>
            <? include __DIR__."/form-bitrix-24.php" ?>
		<? endif ?>

    </div>
</section>
