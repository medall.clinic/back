<? /** @var $credit Medreclama\Landing\Credit */ ?>
<section class="page-section page-section--background-color page-section--background-color--dark page-section--logo banner-price"
    <?php
    if (!empty($credit->blockId)){?>
        id="<?php echo  $credit->blockId?>"

    <?php } ?>>

        <h2 class="banner-price__title">цены
        </h2>
        <p class="banner-price__info">Дорогие пациенты! Точную стоимость операции может посчитать только Ваш пластический хирург после онлайн или офлайн-консультации. Это зависит от Ваших индивидуальных особенностей и сложности операции, количества часов наркоза и времени нахождения в палате. На страницах услуг мы указываем минимальную цену.</p>
        <h2 class="banner-price__title">Рассрочка
        </h2>
        <p class="banner-price__info">Наши пациенты могут оформить рассрочку в клинике, что упрощает процесс подачи документов и сокращает время ожидания. Рассрочка может быть оформлена на члена Вашей семьи (в том числе не на родственника). Единственный документ, который вам понадобится для оформления — паспорт гражданина РФ.</p>
        <a class="button banner-price__button p" href="/rassrochka/">Подробнее
        </a>
        <picture>
          <source srcset="/images/category/plastic/banner-price_s.webp, /images/category/plastic/banner-price_s@2x.webp 2x" media="(max-width: 719px)"/>
          <source srcset="/images/category/plastic/banner-price_m.webp, /images/category/plastic/banner-price_m@2x.webp 2x" media="(min-width: 720px)"/>
          <source srcset="/images/category/plastic/banner-price_s.jpg, /images/category/plastic/banner-price_s@2x.jpg 2x" media="(max-width: 719px)"/>
          <source srcset="/images/category/plastic/banner-price_m.jpg, /images/category/plastic/banner-price_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="banner-price__image" src="/images/category/plastic/banner-price.jpg" loading="lazy" alt="" decoding="async"/>
        </picture>
      </section>
<? /* <section
    class="container page-section page-section--gradient page-section--gradient--to-bottom page-section--credit"
    id="<?= $credit->blockId ?>"
>
    <div class="content">

        <? if($credit->title || $credit->text): ?>
            <div class="page-subsection text-align-center">
				<? if($credit->title): ?>
                    <h2><?= $credit->title ?></h2>
				<? endif ?>
				<? if($credit->text): ?>
					<?= $credit->text ?>
				<? endif ?>
            </div>
        <? endif ?>

        <div class="page-subsection grid">

            <div class="block-icon block-icon--partners grid__cell grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
                <div class="block-icon__name">Банки-партнёры</div>
                <div class="block-icon__desc">Средства рассрочки предоставляются банками-партнерами — <strong>Tinkoff, Credit EuropeBank, МТС банк, Ренессанс, Восточный экспресс банк.</strong></div>
            </div>

            <div class="block-icon block-icon--sum grid__cell grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
                <div class="block-icon__name">Сумма рассрочки</div>
                <div class="block-icon__desc">Рассрочка  предоставляется на сумму <strong>200 000 рублей</strong>.</div>
            </div>

            <div class="block-icon block-icon--timeline grid__cell grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
                <div class="block-icon__name">Срок рассрочки</div>
                <div class="block-icon__desc">Рассрочку можно оформить на срок <strong>до 11 месяцев</strong>.</div>
            </div>

            <div class="block-icon block-icon--time grid__cell grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
                <div class="block-icon__name">Время</div>
                <div class="block-icon__desc">Ответ банка для рассмотрения/ предоставления рассрочки требует <strong>не более 30 минут</strong>.</div>
            </div>

        </div>

    </div>
</section> */ ?>
