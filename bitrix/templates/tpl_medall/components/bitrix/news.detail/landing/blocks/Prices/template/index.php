<? /** @var $prices Medreclama\Landing\Prices */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $pricesButton = $prices->button ?>
<section class="container page-section"
    <?php if (!empty($prices->blockId)){?>
        id="<?php echo $prices->blockId ?>"
    <?php } ?>>

    <div class="content">
        <div class="container">
            <div class="content">

				<? if($prices->title): ?>
                    <div class="page-subsection text-align-center">
                        <h2><?= $prices->title ?></h2>
                    </div>
				<? endif ?>

                <? if($prices->text): ?>
                    <div class="page-subsection">
						<?= $prices->text ?>
                    </div>
				<? endif ?>

                <? if($prices->complexItems): ?>
                    <div class="page-subsection">
                        <? include __DIR__."/complex.php" ?>
                    </div>
				<? endif ?>

                <? if($prices->simpleItems): ?>
                    <div class="page-subsection">
                        <? include __DIR__."/simple.php" ?>
                    </div>
                <? endif ?>

                <? if($prices->text2): ?>
                    <div class="page-subsection">
                        <?= $prices->text2 ?>
                    </div>
                <? endif ?>

                <? if($pricesButton->isExist()): ?>
                    <div class="page-subsection grid grid--justify--center">
						<?= $pricesButton->render() ?>
                    </div>
				<? endif ?>

            </div>
        </div>
    </div>
</section>