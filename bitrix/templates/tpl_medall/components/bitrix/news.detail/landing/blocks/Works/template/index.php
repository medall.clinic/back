<? /** @var $work Medreclama\Landing\Work */ ?>
<? /** @var $works Medreclama\Landing\Works */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $works->button ?>
<section class="container page-section"
    <?php if (!empty($works->blockId)){?>
        id="<?php echo $works->blockId ?>"
    <?php } ?>>

    <div class="content">

		<? if($works->title): ?>
            <h2 class="page-subsection text-align-center"><?= $works->title ?></h2>
		<? endif ?>

        <? if($works->items): ?>
            <div class="landing-works page-subsection">
                <div class="slider">
                    <div class="slider__slides swiper">

                        <div class="slider__wrapper swiper-wrapper">
							<? foreach ($works->items as $work): ?>
								<? include __DIR__."/work.php" ?>
							<? endforeach ?>
                        </div>

                        <div class="slider__arrow slider__arrow--prev"></div>
                        <div class="slider__arrow slider__arrow--next"></div>
                        <div class="slider__pagination"></div>

                    </div>
                </div>
            </div>
        <? endif ?>

        <? if($button->isExist()): ?>
            <div class="page-subsection grid grid--justify--center">
				<?= $button->render() ?>
            </div>
		<? endif ?>

    </div>
</section>