<?php
/** @var $questions Medreclama\Landing\Questions */ ?>
<?php
/** @var $question Medreclama\Landing\Question */ ?>
<?php
/** @var $button Medreclama\Landing\Button */ ?>
<?php
$button = $questions->button ?>
<section class="page-section page-section--no-margin--after-background"
    <?php if (!empty($questions->blockId)){?>
        id="<?php echo $questions->blockId ?>"
    <?php } ?>>


    <?php if($questions->title): ?>
        <div class="container questions-header <?php echo  $this->classTitle ?>">
            <h2 class="content"><?= $questions->title ?></h2>
        </div>
    <?php endif ?>

    <div class="container">
        <div class="content grid grid--align-items--flex-start">

            <div class="grid__cell grid__cell--m--8 grid__cell--xs--12">

                <?php
                if($questions->items): ?>
                    <div class="page-subsection">
                        <?php foreach ($questions->items as $question): ?>
                            <div class="question">
                                <div class="question__name"><?= $question->text ?></div>
                                <div class="question__body"><?= $question->answer ?></div>
                            </div>
                        <?php
                        endforeach ?>
                    </div>
                <?php endif ?>

                <?php
                if($button->isExist()): ?>
					<?= $button->render('page-subsection') ?>
                <?php
                endif ?>

            </div>

            <?php
            if($questions->image): ?>
                <img
                    src="<?= $questions->image ?>"
                    class="grid__cell grid__cell--m--4 border-radius-xl hidden-xs hidden-s"
                    loading="lazy"
                    decoding="async"
                    alt=""
                />
            <?php
            endif ?>

        </div>
    </div>

</section>