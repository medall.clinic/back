<? /** @var $stages Medreclama\Landing\Stages */ ?>
<? /** @var $stage Medreclama\Landing\Stage */ ?>
<? /** @var $finalStage Medreclama\Landing\Stage */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $finalStage = $stages->finalStage ?>
<? $button = $stages->button ?>
<section class="container page-section page-section--text--align--center"
    <?php if (!empty($stages->blockId)){?>
        id="<?php echo $stages->blockId ?>"
    <?php } ?>>

    <div class="content">

		<? if($stages->title): ?>
            <h2 class="page-subsection text-align-center"><?= $stages->title ?></h2>
		<? endif ?>

        <? if($stages->items): ?>
            <? if($stages->isAllStagesWithTitle): ?>
                <ol class="grid grid--no-gap landing-sequence page-subsection">
					<? foreach ($stages->items as $stage): ?>
                        <? include __DIR__."/stage.php" ?>
					<? endforeach ?>
                </ol>
            <? else: ?>
                <div class="page-subsection grid grid--justify--center">
                    <ol class="grid grid--no-gap landing-sequence grid__cell grid__cell--m--10 grid__cell--xs--12">
						<? foreach ($stages->items as $stage): ?>
							<? include __DIR__."/stage.php" ?>
						<? endforeach ?>
                    </ol>
                </div>

            <? endif ?>
		<? endif ?>

		<? if($finalStage->text || $finalStage->image): ?>
			<? include __DIR__."/final-stage.php" ?>
		<? endif ?>

		<? if($button->isExist()): ?>
            <div class="page-subsection grid grid--justify--center">
				<?= $button->render('grid__cell') ?>
            </div>
		<? endif ?>

    </div>
</section>