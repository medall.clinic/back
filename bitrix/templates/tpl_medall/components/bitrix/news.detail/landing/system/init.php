<?
/** @var array $arResult */
use Medreclama\Landing\Landing;

require_once __DIR__."/Landing.php";
require_once __DIR__."/Element.php";
require_once __DIR__."/MenuItem.php";
require_once __DIR__."/UtmTag.php";

$landing = new Landing($arResult);

$dir = new RecursiveDirectoryIterator(__DIR__.'/../blocks/');
foreach (new RecursiveIteratorIterator($dir) as $file) {
	if (!is_dir($file)) {
		if( fnmatch('*.php', $file) && $file->getFilename() !== "init.php" && strpos($file->getPath(), 'code')){
			require_once $file;
		}
	}
}

$vertSortedModelsClassNames = $landing->getVertSortedModelsClassNames();

$models = [];
foreach ($vertSortedModelsClassNames as $className){
	$model = $landing->create($className);
	if(!$model){continue;}
	$models[] = $model;
	if($model->generalBlockId){
		$model = $landing->create("General_".$model->generalBlockId);
		$models[] = $model;
	}
}

return $models;