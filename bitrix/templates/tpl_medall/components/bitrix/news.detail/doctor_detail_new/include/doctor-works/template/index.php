<? /** @var array $works */ ?>
<? /** @var array $arrElements */ ?>
<?php

function getServiceById($idService)
{
    $res = CIBlockElement::GetByID($idService);
    if($ar_res = $res->GetNext()){
        return $ar_res['DETAIL_PAGE_URL'];

    }
}

function getServiceName($idService)
{
    $res = CIBlockElement::GetByID($idService);
    if($ar_res = $res->GetNext()){
        return $ar_res['NAME'];

    }
}

function convertToWebP($path) {
    return preg_replace('/\.(jpg|jpeg|png)$/i', '.webp', $path);
}
function getPathAfter($src)
{
    convertToWebP($src);
    return CFile::GetFileArray($src)['SRC'];
}
function getPathBefore($src)
{
    return CFile::GetFileArray($src)['SRC'];
}

?>
<?php if (!empty($works[0]->arrFieldsList)) {?>
    <section class="container page-section page-section--background-color page-section--background-color--dark doctor-works">
        <div class="content">
            <h2 class="page-subsection text-align-center doctor-works__title">работы специалиста</h2>
            <div class="slider page-subsection doctor-works__slider">
                <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">
                        <? foreach ($works[0]->arrFieldsList as $key => $field ){
                            // Получаем путь до фото после
                            $photoAfterPath = getPathAfter($field->fields['~PROPERTY_PHOTO_AFTER_VALUE']);
                            $photoBeforePath = getPathBefore($field->fields['~PROPERTY_PHOTO_BEFORE_VALUE']);
                            $serviceResult = getServiceById($field->fields['~PROPERTY_SERVICE_BINDING_VALUE']);
                            if (!empty($field->fields['~PROPERTY_SERVICE_BINDING_VALUE']) && empty($field->fields['PROPERTY_SERVICE_NAME_VALUE'])) {
                                $serviceResultName = getServiceName($field->fields['PROPERTY_SERVICE_BINDING_VALUE']);
                            } else {
                                $serviceResultName = $field->fields['PROPERTY_SERVICE_NAME_VALUE'];
                            }


                            ?>

                            <div class="slide swiper-slide doctor-works__slide">
                                <div class="works-item">
                                    <div class="image-compare icv icv__icv--horizontal standard">
                                        <img src="<?php echo convertToWebP($photoBeforePath) ?>" alt="<?php echo $field->fields['NAME']?>" loading="lazy" decoding="async" width="720" height="720" class="icv__img icv__img-a">
                                        <div class="icv__wrapper">
                                            <img src="<?php echo convertToWebP($photoAfterPath) ?>" alt="<?php echo $field->fields['NAME']?>" loading="lazy" decoding="async" width="720" height="720" class="icv__img icv__img-b">
                                        </div>
                                        <div class="icv__control">
                                            <div class="icv__control-line"></div>
                                            <div class="icv__theme-wrapper">
                                                <div class="icv__arrow-wrapper"></div>
                                                <div class="icv__arrow-wrapper"></div>
                                            </div>
                                            <div class="icv__control-line"></div>
                                        </div>
                                    </div>
                                    <a class="works-item__service" href="<?php echo $serviceResult?>"><?php echo $serviceResultName?></a>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                    <div class="slider__pagination"></div>
                </div>
            </div>
        </div>
    </section>

<?php }?>

