<section class="container page-section" id="doctor-form">
	<div class="content">

		<div class="page-subsection">
			<h2 class="page-section__title text-align-center">Хотите записаться на консультацию?</h2>
			<p class="page-section__subtitle">Оставьте свои контактные данные и мы обязательно свяжемся с Вами.</p>
		</div>

		<div class="page-subsection grid grid--justify--center">
		<form
				class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form form-ajax home-form"
				data-is-validate="true"
				data-is-answer-in-popup="true"
				data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации"
		>

			<label class="form-input form-input--text form__input form__input">
				<input class="form-input__field" type="text" name="name" data-validation-required>
				<span class="label form-input__label">ФИО</span>
			</label>

			<label class="form-input form-input--tel form__input form__input">
				<input class="form-input__field" type="tel" name="phone" data-validation-required>
				<span class="label form-input__label">Телефон</span>
			</label>

			<? /* <label class="form-input form-input--email form__input form__input">
				<input class="form-input__field" type="email" name="email">
				<span class="label form-input__label">Электронная почта</span>
			</label> */ ?>

			<label class="form-input form-input--textarea form__input form__input">
				<textarea class="form-input__field" name="comment"></textarea>
				<span class="label form-input__label">Комментарии</span>
			</label>

			<button class="button form__submit" type="submit">Оставить заявку</button>

			<div class="form__agreement home-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</div>

			<? if(!empty($_GET)): ?>
				<? foreach ($_GET as $paramName => $paramValue): ?>
					<? if(strpos($paramName, 'utm_') !== false): ?>
						<input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
					<? endif ?>
				<? endforeach ?>
			<? endif ?>

				<input type="hidden" name="title" value="страница доктора: <?= trim($arResult["NAME"]) ?>">
				<input type="hidden" name="action" value="consultation_register">

		</form>
		</div>

	</div>
</section>
