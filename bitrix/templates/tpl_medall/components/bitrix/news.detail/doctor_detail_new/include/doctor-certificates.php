<section class="container page-section page-section--background-color page-section--background-color--dark doctor-certificates">
  <div class="content">
    <h2 class="page-subsection text-align-center doctor-certificates__title">сертификаты</h2>
    <div class="slider page-subsection doctor-certificates__slider">
      <div class="slider__slides swiper">
        <div class="slider__wrapper swiper-wrapper">
          <? if ($certificates["ID"]): ?>
            <a class="slide swiper-slide slide swiper-slide doctor-certificates__slide" href="<?=$certificates["SRC"]?>" data-glightbox="data-glightbox" data-gallery="doctor-certificates">
              <picture>
                <source srcset="<?= getResizedImageCopyPath(WebPHelper($certificates["SRC"])) ?>" type="image/webp"/>
                <img src="<?= getResizedImageCopyPath($certificates["SRC"]) ?>" loading="lazy" decoding="async" class="doctor-certificates__image" alt=""/>
              </picture>
            </a>
          <? else: ?>
            <? foreach($certificates as $v): ?>
              <a class="slide swiper-slide slide swiper-slide doctor-certificates__slide" href="<?=$v["SRC"]?>" data-glightbox="data-glightbox" data-gallery="doctor-certificates">
                <picture>
                  <source srcset="<?= getResizedImageCopyPath(WebPHelper($v["SRC"])) ?>" type="image/webp"/>
                  <img src="<?= getResizedImageCopyPath($v["SRC"]) ?>" loading="lazy" decoding="async" class="doctor-certificates__image" alt=""/>
                </picture>
              </a>
            <? endforeach ?>
          <?endif;?>
        </div>
        <div class="slider__arrow slider__arrow--prev"></div>
        <div class="slider__arrow slider__arrow--next"></div>
        <div class="slider__pagination"></div>
      </div>
    </div>
  </div>
</section>