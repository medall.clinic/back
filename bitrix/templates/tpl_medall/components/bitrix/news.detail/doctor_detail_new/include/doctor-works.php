
<section class="container page-section page-section--background-color page-section--background-color--dark doctor-works">
                    <div class="content">
                      <h2 class="page-subsection text-align-center doctor-works__title">работы специалиста
                      </h2>
                      <div class="slider page-subsection doctor-works__slider">
                        <div class="slider__slides swiper">
                          <div class="slider__wrapper swiper-wrapper">
                            <a class="slide swiper-slide slide swiper-slide doctor-works__slide" href="/images/doctor-work-1.jpg" data-glightbox="data-glightbox" data-gallery="doctor-works">
                              <picture>
                                <source srcset="/images/doctor-work-1.webp" type="image/webp"/>
                                <source srcset="/images/doctor-work-1.jpg"/><img src="/images/doctor-work-1.jpg" loading="lazy" decoding="async" alt=""/>
                              </picture>
                            </a>
                            <a class="slide swiper-slide slide swiper-slide doctor-works__slide" href="/images/doctor-work-2.jpg" data-glightbox="data-glightbox" data-gallery="doctor-works">
                              <picture>
                                <source srcset="/images/doctor-work-2.webp" type="image/webp"/>
                                <source srcset="/images/doctor-work-2.jpg"/><img src="/images/doctor-work-2.jpg" loading="lazy" decoding="async" alt=""/>
                              </picture>
                            </a>
                            <a class="slide swiper-slide slide swiper-slide doctor-works__slide" href="/images/doctor-work-1.jpg" data-glightbox="data-glightbox" data-gallery="doctor-works">
                              <picture>
                                <source srcset="/images/doctor-work-1.webp" type="image/webp"/>
                                <source srcset="/images/doctor-work-1.jpg"/><img src="/images/doctor-work-1.jpg" loading="lazy" decoding="async" alt=""/>
                              </picture>
                            </a>
                            <a class="slide swiper-slide slide swiper-slide doctor-works__slide" href="/images/doctor-work-2.jpg" data-glightbox="data-glightbox" data-gallery="doctor-works">
                              <picture>
                                <source srcset="/images/doctor-work-2.webp" type="image/webp"/>
                                <source srcset="/images/doctor-work-2.jpg"/><img src="/images/doctor-work-2.jpg" loading="lazy" decoding="async" alt=""/>
                              </picture>
                            </a>
                          </div>
                          <div class="slider__arrow slider__arrow--prev"></div>
                          <div class="slider__arrow slider__arrow--next"></div>
                          <div class="slider__pagination"></div>
                        </div>
                      </div>
                    </div>
                  </section>
