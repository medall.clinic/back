<?
namespace Medreclama\Doctor;

class Work
{
	public $image;
	public $doctor;
	public $title;
	public $description1;
	public $description2;
    public $id;
    public $arrFieldsList = [];

	public function __construct($element, $id = null)
	{
        $this->arrFieldsList = $element;
        $this->id = $id;
	}

}
