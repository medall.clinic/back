
<section class="container page-section page-section--logo page-section--background-color page-section--background-color--light doctor-education">
  <div class="content">
    <? if ($education): ?>
      <div class="page-subsection grid">
        <h2 class="grid__cell grid__cell--m--5 grid__cell--xs--12 doctor-education__subtitle h2">Основное образование</h2>
        <div class="grid__cell grid__cell--m--7 grid__cell--xs--12 doctor-education__info">
          <?=$education?>
      </div>
    <? endif ?>
    <? if ($educationAdditional): ?>
      <div class="page-subsection grid">
        <h2 class="grid__cell grid__cell--m--5 grid__cell--xs--12 doctor-education__subtitle h2">дополнительное образование
        </h2>
        <div class="grid__cell grid__cell--m--7 grid__cell--xs--12 doctor-education__info">
          <?=$educationAdditional?>
        </div>
      </div>
    <? endif ?>
  </div>
  </div>
</section>