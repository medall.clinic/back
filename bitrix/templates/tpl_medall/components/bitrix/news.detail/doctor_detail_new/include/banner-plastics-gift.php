<section class="container page-section page-section--background-color page-section--background-color--dark banner-plastics-gift-new">
  <picture>
    <source srcset="/images/category/plastic/banner-plastics-gift-new_s.webp, /images/category/plastic/banner-plastics-gift-new_s@2x.webp 2x" media="(max-width: 719px)"/>
    <source srcset="/images/category/plastic/banner-plastics-gift-new_m.webp, /images/category/plastic/banner-plastics-gift-new_m@2x.webp 2x" media="(min-width: 720px) and (max-width: 1199px)"/>
    <source srcset="/images/category/plastic/banner-plastics-gift-new_l.webp, /images/category/plastic/banner-plastics-gift-new_l@2x.webp 2x" media="(min-width: 1200px)"/>
    <source srcset="/images/category/plastic/banner-plastics-gift-new_s.jpg, /images/category/plastic/banner-plastics-gift-new_s@2x.jpg 2x" media="(max-width: 719px)"/>
    <source srcset="/images/category/plastic/banner-plastics-gift-new_m.jpg, /images/category/plastic/banner-plastics-gift-new_m@2x.jpg 2x" media="(min-width: 720px) and (max-width: 1199px)"/>
    <source srcset="/images/category/plastic/banner-plastics-gift-new_l.jpg, /images/category/plastic/banner-plastics-gift-new_l@2x.jpg 2x" media="(min-width: 1200px)"/><img class="banner-plastics-gift-new__image" src="/images/category/plastic/banner-plastics-gift-new.jpg" loading="lazy" decoding="async" alt="Подарок при выписке"/>
  </picture>
  <h2 class="banner-plastics-gift-new__title">Подарок при выписке
  </h2>
  <div class="banner-plastics-gift-new__description">MEDALL всегда готов на большее ради своих пациентов! Мы разработали классный брендовый мерч, который будем дарить Вам при выписке. </div>
  <a class="button banner-plastics-gift-new__button" href="/merch/">Подробнее</a>
</section>
