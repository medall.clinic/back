<section class="container page-section page-section--background-color page-section--background-color--light page-section--logo doctor-header">
<div class="content grid grid--justify--center doctor-header__inner">
  <? include __DIR__.'/doctor-select.php' ?>
  <picture>
    <source
      srcset="<?= WebPHelper::getOrCreateWebPUrl($arResult["DISPLAY_PROPERTIES"]["photo_doctor"]["FILE_VALUE"]["SRC"], [560, 1120]) ?>"
      type="image/webp"
    >
    <img class="grid__cell grid__cell--s--5 grid__cell--xs--12 doctor-header__image" src="<?=$photo?>" loading="lazy" decoding="async" alt="<?=$fullname?>"/>
  </picture>
  <div class="grid__cell grid__cell--s--7 grid__cell--xs--12 doctor-header__info">
    <h1 class="h1 doctor-header__name"><?=$surname?><br><?=$io?></h1>
    <div class="doctor-header__description"><?=$short_description?></div>
    <div class="doctor-header__experience">стаж работы: <?=$experience?></div>
    
    <? if ($schedule): ?>
      <div class="doctor-header__schedule"> 
        <div class="doctor-header__schedule-title">График приема: </div>
        <div class="doctor-header__schedule-text"><?=$schedule?></div>
      </div>
    <? endif ?>
    
    <? if ($arResult["PROPERTIES"]["direction_doctor"]["VALUE_ENUM_ID"] !== "1034"): ?>
      <a class="button doctor-header__button" href="#doctor-form">Запись на консультацию</a>
    <? endif; ?>
  </div>
  <? if ($instagram || $vk): ?>
    <div class="grid__cell grid__cell--s--5 grid__cell--xs--12 grid grid--justify--center grid--align-items--center doctor-header__socials">
    <? if ($vk): ?>
      <a class="link-icon link-icon--icon--vk" href="https://vk.com/<?=$vk?>" target="_blank"></a>
    <? endif ?>
    <? if ($instagram): ?>
      <a class="link-icon link-icon--icon--instagram" href="https://instagram.com/<?=$instagram?>" target="_blank"></a>
    <? endif ?>
    </div>
    <? if ($instagram): ?>
      <div class="grid__cell grid__cell--xs--12 doctor-header__clarification">* Социальная сеть Instagram запрещена на территории РФ.</div>
    <? endif; ?>
  <? endif ?>
</div>
</section>
