<?php $feedback = $arResult["PROPERTIES"]["feedback"]["VALUE"];
$hasValue = false;

foreach ($feedback as $key => $value) {
if (!empty($value)) {
$hasValue = true;
break; // Выход из цикла, если найдено хотя бы одно значение
}
}?>

<?php if ($hasValue === true){ ?>

    <section class="container page-section page-section--background-color page-section--background-color--light home-feedback">
        <div class="content">
            <h2 class="page-section__title page-subsection text-align-center">Отзывы</h2>
            <div class="slider page-subsection home-feedback__slider reviews-item-list">
                <div class="slider__slides swiper">
                    <div class="slider__wrapper swiper-wrapper">

                        <?php foreach($arResult["PROPERTIES"]["feedback"]["VALUE"] as $key => $arItem){?>
                            <?php

                            $res = CIBlockElement::GetByID($arItem);
                            $ar_res = $res->GetNext();
                            $service = array_unique($arResult['SERVICE_NAME'][$key]);
                            $url = array_unique($arResult['SERVICE_URL'][$key]);
                            $doctors = array_unique($arResult['DOCTOR_NAME'][$key]);
                            $doctorsURL = array_unique($arResult['DOCTOR_URL'][$key]);
                            $ratingCount = 0;
                            $rating = array_unique($arResult['RATING'][$key]);
                            $dateString = $ar_res["ACTIVE_FROM"];
                            $platformName = array_unique($arResult['FEEDBACK_PLATFORM'][$key]);
                            $platformURL = array_unique($arResult['FEEDBACK_PLATFORM']['URL'][$key]);
                            $time = array_unique($arResult['DATE_ACTIVE_FROM'][$key]);

                            // Установите локаль, если нужно отображение месяца на нужном языке
                            setlocale(LC_TIME, 'ru_RU.UTF-8');


                            // Форматируем дату в нужный формат


                             if (!empty($doctors)){?>
                                <div class="slide swiper-slide">
                                    <div class="reviews-item" data-service="<?php echo isset($arResult['SERVICE_NAME'][$arItem]) ? implode(',', array_unique($arResult['SERVICE_NAME'][$arItem])) : ''; ?>"
                                         data-doctor="<?php echo isset($arResult['DOCTOR_NAME'][$arItem]) ? implode(',', array_unique($arResult['DOCTOR_NAME'][$arItem])) : ''; ?>">

                                            <div class="reviews-item__platform">
                                                Отзыв оставлен на сайте
                                                <?php if (!empty($platformURL[0])){?>

                                                <a href="<?php echo $platformURL[0]?>" target="_blank"><?=$platformName[0]?></a>
                                                <?php }?>
                                            </div>



                                        <?php if (!empty($arResult['NAME_REVIEW'][$arItem])){?>
                                             <div class="reviews-item__name"><?php echo $arResult['NAME_REVIEW'][$arItem];?></div>

                                        <?php }?>
                                        <div class="reviews-item__date hidden-xs hidden-s hidden-m hidden-l"><?php echo strtolower(FormatDate("d F Y", MakeTimeStamp($arResult['DATE_TIME'][$arItem][0])))?></div>

                                        <div class="reviews-item__rating reviews-item__rating--<?php echo $rating[0]?> hidden-xs hidden-s hidden-m hidden-l">
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>
                                            <div class="reviews-item__rating-star"></div>

                                        </div>
                                        <div class="reviews-item__text"><?php echo $ar_res['PREVIEW_TEXT'];?></div>

                                        <div class="reviews-item__doctor">Отзыв о:

                                            <?php foreach ($doctors as $d => $doctor){?>
                                                <a class="reviews-item__doctor-link" href="<?php echo  $doctorsURL[$d] ?>">
                                                    <?php echo $doctor;?>
                                                </a>

                                            <?php }?>

                                        </div>

                                        <?php if (!empty($service)){?>
                                            <div class="reviews-item__service">Услуга:
                                                <?php foreach ($service as $v => $value){?>
                                                    <a class="reviews-item__service-link" href="<?php echo  $url[$v] ?>"><?php echo $value ?></a>
                                                <?php }?>
                                            </div>
                                        <?php }?>



                                    </div>
                                </div>
                             <?php }else{
                                continue;
                            } ?>
                        <?php }?>

                    </div>
                    <div class="slider__arrow slider__arrow--prev"></div>
                    <div class="slider__arrow slider__arrow--next"></div>
                    <div class="slider__pagination"></div>
                </div>
            </div>
            <a class="button home-feedback__button" href="<?php echo $linkReview['~VALUE'];?>">Оставить отзыв</a>
        </div>
    </section>
<?php } else{ ?>
    <section class="container page-section page-section--background-color page-section--background-color--light home-feedback">
        <div class="content">
            <h2 class="page-section__title page-subsection text-align-center">Отзыв</h2>
            <a class="button home-feedback__button" href="<?php echo $linkReview['~VALUE'];?>">Оставить отзыв</a>
        </div>
    </section>
<?php } ?>



