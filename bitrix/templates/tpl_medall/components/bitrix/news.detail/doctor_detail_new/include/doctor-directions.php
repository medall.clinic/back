<?php
$specialization = [];
$id = [];
$result = [];
$state = true;
$arrResultIblock = [];
$arSelect = [
    "ID",
    "NAME",
    'PROPERTY_646',
    'PROPERTY_647',
    'PROPERTY_648',
    'PROPERTY_649',


];
foreach ($arResult["DISPLAY_PROPERTIES"]['list_operation_general']['LINK_ELEMENT_VALUE'] as $item){
    $id[] = $item['ID'];
}

$arFilter = ['IBLOCK_ID' => 66, 'ACTIVE' => 'Y', 'ID' => $id];
$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
while ($ob = $res->fetch()) {
    $arrResultIblock[] = $ob;
}
foreach ($arrResultIblock as $K => $item){
    if (!empty($item['PROPERTY_647_VALUE'])) {
        $result['ITEMS'][$K]['NAME'] = $item['PROPERTY_646_VALUE'];

    }

    if (!empty($item['PROPERTY_647_VALUE'])) {
        $result['ITEMS'][$K]['PROPS']['NAME'] = $item['PROPERTY_647_VALUE'];
    }


    if (!empty($item['PROPERTY_648_VALUE'])) {
        $result['ITEMS'][$K]['PROPS']['LINK'] = $item['PROPERTY_648_VALUE'];
    }




}
foreach ($arResult["DISPLAY_PROPERTIES"]['news_card_doctor'] as $item){
    if (in_array('yes',$item,)){

        $state = true;



    }
    if (in_array('no',$item)){

        $state = false;


    }
}


//Пластика
if($state === false && !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_plastic']['LINK_ELEMENT_VALUE'])){

    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_plastic']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }

}

//Косметология
if($state === false && !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_cosmetology']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_cosmetology']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}

//Флебология
if($state === false && !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_phlebology']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_phlebology']['LINK_ELEMENT_VALUE'] as $k => $v){
        $specialization[0]['NAME'] = "Флебология";
        $specialization[0]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
        $specialization[0]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
    }
}

//Пересадка волос
if($state === false && !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_hair_transplant']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_hair_transplant']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}

//Стоматология
if($state === false && !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_dentistry']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_dentistry']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}

//Гинекология
if($state === false && !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_gynecology'])){

    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_gynecology']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];

            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}
?>

<section class="container page-section page-section--logo page-section--background-color page-section--background-color--light doctor-directions">
    <div class="content">
        <h2 class="page-subsection text-align-center">специализация</h2>
        <div class="page-subsection grid grid--justify--space-between grid--align-items--flex-start doctor-directions__inner">

            <?php if ($specialization_image): ?>
                <picture>
                    <source srcset="<?= WebPHelper::getOrCreateWebPUrl($specialization_image["SRC"], [ 560, 1120 ]) ?>" type="image/webp"/>
                    <img
                            src="<?= $specialization_image["SRC"] ?>"
                            loading="lazy"
                            decoding="async"
                            class="grid__cell grid__cell--s--5 doctor-directions__image"
                            width=<?=$specialization_image['WIDTH']?>
                            height=<?=$specialization_image['HEIGHT']?>
                            alt=""
                    />
                </picture>
            <?php endif; ?>
            <?php if ($state === false){?>
                <div class="grid__cell grid__cell--s--6 grid__cell--xs--12 doctor-directions__info">
                    <?php foreach($specialization as $v): ?>

                        <div class="page-subsection grid grid--align-items--center doctor-directions__section">
                            <h3 class="grid__cell grid__cell--m--6 grid__cell--xs--12 doctor-directions__subtitle"><?= $v["NAME"] ?></h3>
                            <ul class="grid__cell grid__cell--m--6 grid__cell--xs--12 doctor-directions__list">
                                <?php if ($v['NAME'] != 'ГИНЕКОЛОГИЯ' && $v['NAME'] != 'ДИАГНОСТИКА' && $v['NAME'] != 'ЭСТЕТИЧЕСКАЯ ГИНЕКОЛОГИЯ') { ?>
                                    <?php foreach($v["VALUE"] as $l){ ?>

                                        <li><a href="<?= $l["DETAIL_PAGE_URL"] ?>"><?= $l["NAME"] ?></a></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php }elseif ($state === true){?>
                <div class="grid__cell grid__cell--s--6 grid__cell--xs--12 doctor-directions__info">

                    <?php foreach($result['ITEMS'] as $key => $item){ ?>

                        <div class="page-subsection grid grid--align-items--center doctor-directions__section">
                            <h3 class="grid__cell grid__cell--m--6 grid__cell--xs--12 doctor-directions__subtitle"><?= $item['NAME'][0] ?></h3>
                            <ul class="grid__cell grid__cell--m--6 grid__cell--xs--12 doctor-directions__list">
                                <?php foreach($item['PROPS']['NAME'] as $k => $l){ ?>
                                    <?php if(!empty($item['PROPS']['LINK'])){?>
                                        <li><a href="<?= $item['PROPS']['LINK'][$k] ?>"><?= $l ?> </a></li>

                                    <?php } else { ?>
                                        <li><a href="#"><?= $l ?> </a></li>

                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            <?php }?>

        </div>
    </div>
</section>