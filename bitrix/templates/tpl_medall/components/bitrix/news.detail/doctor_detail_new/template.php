<?php
    $surname = $arResult["NAME"];
    $io = $arResult["PROPERTIES"]["io_doctor"]["VALUE"];
    $fullname = $surname.' '.$io;
    $short_description = $arResult['PREVIEW_TEXT'];
    $experience = $arResult["PROPERTIES"]["experience_doctor"]["VALUE"];
    $photo = $arResult["PREVIEW_PICTURE"]["SRC"];
    $instagram = $arResult["PROPERTIES"]["instagram_username"]["VALUE"];
    $vk = $arResult["PROPERTIES"]["vk_username"]["VALUE"];
    $details = $arResult["DETAIL_TEXT"];
    $education = $arResult["DISPLAY_PROPERTIES"]["education_doctor"]["DISPLAY_VALUE"];
    $educationAdditional = $arResult["DISPLAY_PROPERTIES"]["advanced_training_doctor"]["DISPLAY_VALUE"];
    $certificates = $arResult["DISPLAY_PROPERTIES"]["certificate_doctor"]["FILE_VALUE"];
    $specialization_image = CFile::GetFileArray($arResult["PROPERTIES"]['specialization_image']['VALUE']);
    $links = $arResult["PROPERTIES"]["links"]["VALUE"];
    $schedule = $arResult["DISPLAY_PROPERTIES"]["schedule_doctor"]["DISPLAY_VALUE"];
    $linkReview =  $arResult["PROPERTIES"]["link_review"];

?>


<?php include __DIR__.'/include/doctor-header.php' ?>
<?php if ($details) {
    include __DIR__.'/include/doctor-description.php';
} ?>
<?php if ($education || $educationAdditional) {
    include __DIR__.'/include/doctor-education.php';
} ?>
<?php if ($certificates) {
    include __DIR__.'/include/doctor-certificates.php';
} ?>
<?php if (
    !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_plastic']['LINK_ELEMENT_VALUE']) ||
    !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_cosmetology']['LINK_ELEMENT_VALUE']) ||
    !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_phlebology']['LINK_ELEMENT_VALUE']) ||
    !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_hair_transplant']['LINK_ELEMENT_VALUE']) ||
    !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_dentistry']['LINK_ELEMENT_VALUE']) ||
    !empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_gynecology']['LINK_ELEMENT_VALUE']) ||
    !empty($arResult["DISPLAY_PROPERTIES"]['list_operation_general']['LINK_ELEMENT_VALUE'])


) {
    include __DIR__.'/include/doctor-directions.php';
} ?>

<?php include __DIR__.'/include/doctor-works/index.php' ?>

<?php if (!empty($feedback) && !empty($linkReview)) {
    include __DIR__.'/include/doctor-feedback.php';
}?>
<?php if (empty($feedback) && !empty($linkReview)) {
    include __DIR__.'/include/doctor-feedback.php';
}?>
<?php if (!empty($arResult["PROPERTIES"]['video_block']['~VALUE'])) {
    include __DIR__.'/include/doctor-video-block.php';
}?>
<?php if ($arResult["PROPERTIES"]["direction_doctor"]["VALUE_ENUM_ID"] !== "1034") { ?>
    <?php include __DIR__.'/include/form.php' ?>
<?php } ?>


<?php if ($links) { ?>
    <?php include __DIR__.'/include/doctor-links.php';?>
<?php }; ?>
<?php
    if ($arResult["PROPERTIES"]["direction_doctor"]["VALUE_ENUM_ID"] === "12") {
        include __DIR__.'/include/banner-plastics-gift.php';
    }
?>
