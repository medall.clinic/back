<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arFilter = ['IBLOCK_ID' => 17,'ACTIVE' => 'Y',];
$arSelect = [
    'ID',
    'NAME',
    'PROPERTY_RATING',
    'PREVIEW_TEXT',
    'PROPERTY_DOCTOR_REVIEW',
    'PROPERTY_SERVICE',
    "PROPERTY_FEEDBACK_PLATFORM",
    "PROPERTY_RATING_LABEL",
    'PROPERTY_NAME_REVIEW',
    'DATE_ACTIVE_FROM',
];
$res = CIBlockElement::GetList([],$arFilter, false, false,$arSelect);
$arFieldsReview = [];
$arFieldsDoctors = [];
$arrIdService = [];
$arrIdDoctor = [];
$serviceNames = []; // Новый массив для хранения названий услуг
$doctorNames = []; // Новый массив для хранения названий услуг
$serviceCheck = [];


while ($obj = $res->GetNext()) {
    $arFieldsReview[] = $obj;
}

$arFieldsService = [];
$arSelectService = ['NAME', 'ID', 'DETAIL_PAGE_URL'];
$arFilterService = [
    'IBLOCK_ID' => 47,
    'ACTIVE' => 'Y',
];

$res = CIBlockElement::GetList([], $arFilterService, false, false, $arSelectService);
while ($arResService = $res->GetNext()) {
    $arFieldsService[] = $arResService; // сохраняем имя по ID
}
foreach ($arFieldsService as $service) {
    if (!empty($service['NAME'])){
        $arrIdService[$service['ID']] = $service['NAME'];
        $arrIdService['URL'][$service['ID']] = $service['DETAIL_PAGE_URL'];

    }
}

$arSelectDoctors = ['NAME', 'ID', 'DETAIL_PAGE_URL','PROPERTY_IO_DOCTOR'];
$arFilterDoctors = [
    'IBLOCK_ID' => 6,
    'ACTIVE' => 'Y',
];

$resDoc = CIBlockElement::GetList([], $arFilterDoctors, false, false, $arSelectDoctors);
while ($arResDoctor = $resDoc->GetNext()) {
    $arFieldsDoctors[] = $arResDoctor; // сохраняем имя по ID
}

foreach ($arFieldsDoctors as $doctor) {

    $arrIdDoctor[$doctor['ID']] = $doctor['NAME'] . ' ' . $doctor['PROPERTY_IO_DOCTOR_VALUE'];
    $arrIdDoctor['URL'][$doctor['ID']] = $doctor['DETAIL_PAGE_URL'];
}
$old_id = [];
// Новый массив для хранения имен врачей по ID отзыва
$doctorNamesByFeedbackId = [];

// Обработка отзывов
foreach ($arFieldsReview as $key => $arField) {
    if (empty($arField['~PROPERTY_DOCTOR_REVIEW_VALUE'])){
      $arResult['EMPTY'][] = $arField['ID'];

    }

    if ($arField['~PROPERTY_DOCTOR_REVIEW_VALUE'] == $arResult['ID']){


        $arResult["PROPERTIES"]["feedback"]["VALUE"][$arField['ID']] = $arField['ID']; // Сохранение ID

    }
    if (!empty($arField['~PROPERTY_SERVICE_VALUE'])) {
        $serviceCheck[] = $arrIdService[$arField['~PROPERTY_SERVICE_VALUE']];
         $arResult['SERVICE_NAME'][$arField['ID']][] = $arrIdService[$arField['~PROPERTY_SERVICE_VALUE']];
        $arResult['SERVICE_URL'][$arField['ID']][] = $arrIdService['URL'][$arField['~PROPERTY_SERVICE_VALUE']];
    }

    if (!empty($arField['~PROPERTY_DOCTOR_REVIEW_VALUE'])) {

        // Сохранение имени врача в массив по ID отзыва
        $arResult['DOCTOR_NAME'][$arField['ID']][] = $arrIdDoctor[$arField['~PROPERTY_DOCTOR_REVIEW_VALUE']];
        $arResult['DOCTOR_URL'][$arField['ID']][] = $arrIdDoctor['URL'][$arField['~PROPERTY_DOCTOR_REVIEW_VALUE']];


    }
    if (!empty($arField['~PROPERTY_RATING_VALUE'])) {
        $arResult['RATING'][$arField['ID']][] = $arField['PROPERTY_RATING_VALUE'];

    }
    if (!empty($arField['~PROPERTY_RATING_LABEL_VALUE'])) {
        $arResult['FEEDBACK_PLATFORM'][$arField['ID']][] = $arField['~PROPERTY_RATING_LABEL_VALUE'];

    }
    if (!empty($arField['DATE_ACTIVE_FROM'])) {
        $arResult['DATE_TIME'][$arField['ID']][] = $arField['DATE_ACTIVE_FROM'];

    }
    $arResult['NAME_REVIEW'][$arField['ID']] = $arField['PROPERTY_NAME_REVIEW_VALUE'];

    $old_id[] = $arField['ID'];

    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], '2GIS') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://2gis.ru/spb/firm/70000001035838469?m=30.285963%2C59.966204%2F16';
    }
    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'],'Яндекс Карты') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://yandex.ru/maps/org/medall/35461995871/?ll=30.285847%2C59.966174&z=17';
    }
    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'], 'Google maps') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://www.google.com/maps/place/Клиника+MEDALL/@59.966168,30.2832555,17z/data=!4m8!3m7!1s0x469631cd7ce1a2bb:0x9e8f4e1bbc2adb66!8m2!3d59.966168!4d30.2858304!9m1!1b1!16s%2Fg%2F1v4k559y?entry=ttu';
    }
    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'],'docdoc.ru') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://spb.docdoc.ru/clinic/klinika_cifrovoj_stomatologii_medall';
    }
    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'],'НаПоправку') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://spb.napopravku.ru/clinics/medall-centry-esteticheskoy-mediciny-i-cifrovoy-stomatologii/';
    }
    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'],'ПроДокторов') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://prodoctorov.ru/spb/lpu/63649-medall/';
    }
    if (strcasecmp($arField['~PROPERTY_RATING_LABEL_VALUE'],'Докту') === 0){
        $arResult['FEEDBACK_PLATFORM']['URL'][$arField['ID']][] = 'https://doctu.ru/spb/net/medall';
    }
}

// В итоге, массив $doctorNamesByFeedbackId будет содержать имена врачей, сгруппированные по ID отзывов.

// Теперь у Вас есть массив $serviceNames с названиями услуг







