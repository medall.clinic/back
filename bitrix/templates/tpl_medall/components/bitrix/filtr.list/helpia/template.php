<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// Задаем фильтр
global $arFilter;

$arFilter = null;

if(!empty($_REQUEST["SECTION_ID"])){
	$arFilter = ["SECTION_ID" => $_REQUEST["SECTION_ID"]];
}

$propertyCodeValueKey = "PROPERTY_".$arResult['PROPERTY_CODE']."_VALUE";
if(!empty($_REQUEST[$propertyCodeValueKey])){
	$propertyCodeValue = $_REQUEST[$propertyCodeValueKey];
	$arFilter = [$propertyCodeValueKey => $propertyCodeValue];
}
?>


            <div class="articles-layout__filter page-subsection">
                <div class="form-input form-input--select form-input--secondary articles-layout__select--active">
                    <select class="form-input__field" name="PROPERTY_<?=$arResult['PROPERTY_CODE']?>_VALUE" onChange="submit();">
                        <option value="all">Все направления</option>
                        <?foreach($arResult['direction'] as $k=>$v){
                            if(!empty($_REQUEST["PROPERTY_".$arResult['PROPERTY_CODE']."_VALUE"]) && $_REQUEST["PROPERTY_".$arResult['PROPERTY_CODE']."_VALUE"] == $v){
                                ?><option value="<?echo $v;?>" selected><?echo $v;?></option><?
                            } else{
                                ?><option value="<?echo $v;?>"><?echo $v;?></option><?
                            }
                        }?>
                    </select>
                    <span class="label form-input__label">Все направления</span>
                </div>
            </div>



