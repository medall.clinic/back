<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true);
?>
<div class="header-search header__search">

    <button class="header-search__open" type="button"></button>

    <div class="header-search__popup">

        <button class="header-search__close" type="button"></button>

        <form
            class="header-search__form"
            action="<?= $arResult["FORM_ACTION"] ?>"
            method="post"
        >

            <label class="form-input form-input--search header-search__input header-search__input">
                <input
                    class="form-input__field"
                    type="search"
                    name="q"
                    value=""
                    required
                >
            </label>

            <button
                class="header-search__submit"
                type="submit"
                name="s"
            ></button>

        </form>

    </div>

</div>