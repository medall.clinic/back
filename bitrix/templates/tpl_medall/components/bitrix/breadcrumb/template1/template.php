<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
    return "";

$strReturn = '';

// Check for font-awesome.css and add if not already included
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
    $strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= '<div class="container breadcrumbs">
    <div class="content breadcrumbs__inner">
        <a class="breadcrumbs__parent" href="'.SITE_DIR.'">Главная</a>';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    if ($arResult[$index]["LINK"] <> "" && $index != $itemSize-1) {
        $strReturn .= '<a href="'.$arResult[$index]["LINK"].'" class="breadcrumbs__parent">'.$title.'</a>';
    } else {
        $strReturn .= '<div class="breadcrumbs__current">'.$title.'</div>';
    }
}

$strReturn .= '</div>
</div>';

return $strReturn;
?>
