<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$Result = '';
if($arResult["REQUEST"]["QUERY"]!=''){$Result = $arResult["REQUEST"]["QUERY"];}
else{$Result = $arResult["REQUEST"]["TAGS"];}
?>
<h1>Результаты поиска: <?=$Result?></h1>
<div class="innerPageContent__mainBlock">
	<div class="searchResaltPage__content">
		<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):?>
			<p class="p text-marked">
				<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
		</p><?
		endif;?>
		<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
		<?elseif($arResult["ERROR_CODE"]!=0):?>
			<p><?=GetMessage("SEARCH_ERROR")?></p>
			<p class="p text-marked text-marked--error"><?=$arResult["ERROR_TEXT"];?></p>
			<p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
			<p><?=GetMessage("SEARCH_SINTAX")?></p>
			<h2><?=GetMessage("SEARCH_LOGIC")?></h2>
			<div class="table-wrap p">
				<table class="table">
					<tr class="table__row">
						<th class="table__header"><?=GetMessage("SEARCH_OPERATOR")?></td>
						<th class="table__header"><?=GetMessage("SEARCH_SYNONIM")?></td>
						<th class="table__header"><?=GetMessage("SEARCH_DESCRIPTION")?></td>
					</tr>
					<tr class="table__row">
						<td class="table__cell"><?=GetMessage("SEARCH_AND")?></td>
						<td class="table__cell">and, &amp;, +</td>
						<td class="table__cell"><?=GetMessage("SEARCH_AND_ALT")?></td>
					</tr>
					<tr class="table__row">
						<td class="table__cell"><?=GetMessage("SEARCH_OR")?></td>
						<td class="table__cell">or, |</td>
						<td class="table__cell"><?=GetMessage("SEARCH_OR_ALT")?></td>
					</tr>
					<tr class="table__row">
						<td class="table__cell"><?=GetMessage("SEARCH_NOT")?></td>
						<td class="table__cell">not, ~</td>
						<td class="table__cell"><?=GetMessage("SEARCH_NOT_ALT")?></td>
					</tr>
					<tr class="table__row">
						<td class="table__cell">( )</td>
						<td class="table__cell">&nbsp;</td>
						<td class="table__cell"><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
					</tr>
				</table>
			</div>
		<?elseif(count($arResult["SEARCH"])>0):
			$SEARCH = array();
			$os = array(22,5,19,18,11,1);
			foreach($arResult["SEARCH"] as $arItem){
				$res = CIBlockElement::GetByID($arItem['ITEM_ID']);
				if($arRes = $res->Fetch()){
					$res = CIBlock::GetByID($arRes['IBLOCK_ID']);
					if($ar_res = $res->GetNext()){
						if (!in_array($ar_res['ID'], $os)) {
							$SEARCH[$arRes['IBLOCK_ID']]["NAME"] = $ar_res['NAME'];
							$SEARCH[$arRes['IBLOCK_ID']]["SEARCH"][] = $arItem;
						}
					}
				}
			}
			foreach($SEARCH as $arItem){
				?><h2><?=$arItem['NAME']?></h2><?
				foreach($arItem["SEARCH"] as $arItem2){?>
					<div class="search-result p">
            <h3 class="search-result__title"><a href="<?echo $arItem2["URL"]?>"><?echo $arItem2["TITLE_FORMATED"]?></a></h3>
            <p><?echo $arItem2["BODY_FORMATED"]?></p>
          </div>
					<?
				}
			}
		else:?>
			
			<p class="p text-marked"><?=GetMessage("SEARCH_NOTHING_TO_FOUND");?></p>
		<?endif;?>
	</div>
</div>