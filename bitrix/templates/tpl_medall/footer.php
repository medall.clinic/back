        </main>

        <footer class="footer">
			<? include __DIR__."/include/footer/index.php" ?>
        </footer>

        <div id="panel"><? $APPLICATION->ShowPanel() ?></div>
        <? include __DIR__."/include/footer/modals/index.php" ?>
        <? include __DIR__."/include/styles-footer.php" ?>
        <? include __DIR__."/include/scripts.php" ?>

        <?php $current_uri = $_SERVER['REQUEST_URI'];

        // Путь к папке с файлами
        $folder_path = $_SERVER['DOCUMENT_ROOT'] .'/bitrix/templates/tpl_medall/scripts/service/';

        // Получаем список файлов в папке
        $files = scandir($folder_path);

        // Перебираем файлы
        foreach ($files as $file) {

            // Игнорируем . и ..
            if ($file == '.' || $file == '..') {
                continue;
            }
            $filename = pathinfo($file, PATHINFO_FILENAME);
            $random_number = mt_rand(10, 99);
            // Сравниваем текущий URI с базовым именем файла
            if ($filename == basename($current_uri, '.php')) {
                // Подключаем соответствующий скрипт
                echo "<script src='/bitrix/templates/tpl_medall/scripts/service/$filename.js?v=$random_number'></script>";
                break; // Можно прервать цикл после первого совпадения, если файлы уникальны
            }
        }?>



        </body>


</html>
