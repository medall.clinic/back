document.addEventListener('DOMContentLoaded', () => {
    const $contactsToBitrix24 = document.querySelectorAll('form.contacts_to_bitrix24')
    $contactsToBitrix24.forEach(($form) => {
        $form.addEventListener('submit', addContactsToBitrix24)
    })
    document.querySelector('form').addEventListener('submit', () => fbq('track', 'Lead'))
});

function addContactsToBitrix24(e)
{
    e.preventDefault();
    const $form = e.target;

    const validator = new Validator($form);
    if (!validator.validateFields()) {
        return false;
    }

    const formData = new FormData($form);

    const localStorageUtmSource = getLocalStorageUtmValue('utm_source');
    if(localStorageUtmSource && !formData.has('utm_source')){
        formData.set('utm_source', localStorageUtmSource)
    }

    const localStorageUtmMedium = getLocalStorageUtmValue('utm_medium');
    if(localStorageUtmMedium && !formData.has('utm_medium')){
        formData.set('utm_medium', localStorageUtmMedium)
    }

    var localStorageUtmTerm = getLocalStorageUtmValue('utm_term');
    if(localStorageUtmTerm && !formData.has('utm_term')){
        formData.set('utm_term', localStorageUtmTerm)
    }

    fetch(
      '/bitrix24/add_contacts.php',
      {
          method: 'POST',
          body: formData
      }
    )
      .then(response => response.json())
      .then((response) => {
        if (response.result) {
            console.log('Создан лид "Контактные данные со страницы Акции"');
            let successText = '<p>Ваши контактные данные отправлены!</p>';
            successText += '<p>В ближайшее время с Вами свяжутся менеджеры клиники.</p>';
            $form.innerHTML = successText
        }
    }).catch((response) => {
        console.log('Request fail: ' + response);
    })

}

function getLocalStorageUtmValue(utmKey)
{
    let b24CrmGuestUtm = localStorage.getItem('b24_crm_guest_utm');

    if(!b24CrmGuestUtm){
        return false;
    }

    b24CrmGuestUtm = JSON.parse(b24CrmGuestUtm);
    if(!b24CrmGuestUtm.list){
        return false;
    }

    if(!b24CrmGuestUtm.list[utmKey]){
        return false;
    }

    return b24CrmGuestUtm.list[utmKey];
}