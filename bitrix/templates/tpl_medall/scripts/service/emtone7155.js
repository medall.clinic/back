const nodeScriptIs = (node) => node.tagName === 'SCRIPT';

const nodeScriptClone = (node) => {
  const script = document.createElement('script');
  script.text = node.innerHTML;
  let i = 0;
  let attr;
  const attrs = node.attributes;
  while (i < attrs.length) {
    script.setAttribute((attr = attrs[i]).name, attr.value);
    i += 1;
  }
  return script;
};

const nodeScriptReplace = (node) => {
  if (nodeScriptIs(node) === true) {
    node.parentNode.replaceChild(nodeScriptClone(node), node);
  } else {
    let i = 0;
    const children = node.childNodes;
    while (i < children.length) {
      nodeScriptReplace(children[i]);
      i += 1;
    }
  }

  return node;
};

const counters = (code, where = 'body') => {
  const place = ['body', 'head'].includes(where) ? where : 'body';
  const block = document.createElement('div');
  block.innerHTML = code;
  const userEvents = () => {
    window.removeEventListener('scroll', userEvents);
    window.removeEventListener('mousemove', userEvents);
    document[place].append(...nodeScriptReplace(block).children);
  };
  window.addEventListener('scroll', userEvents);
  window.addEventListener('mousemove', userEvents);
};
counters(`<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '754193912100577');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=754193912100577&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?169",t.onload=function(){VK.Retargeting.Init("VK-RTRG-1134845-3iXEK"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-1134845-3iXEK" style="position:fixed; left:-999px;" alt=""/></noscript>`, 'head');