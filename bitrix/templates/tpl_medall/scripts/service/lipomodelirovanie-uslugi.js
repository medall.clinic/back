const nodeScriptIs = (node) => node.tagName === 'SCRIPT';

const nodeScriptClone = (node) => {
  const script = document.createElement('script');
  script.text = node.innerHTML;
  let i = 0;
  let attr;
  const attrs = node.attributes;
  while (i < attrs.length) {
    script.setAttribute((attr = attrs[i]).name, attr.value);
    i += 1;
  }
  return script;
};

const nodeScriptReplace = (node) => {
  if (nodeScriptIs(node) === true) {
    node.parentNode.replaceChild(nodeScriptClone(node), node);
  } else {
    let i = 0;
    const children = node.childNodes;
    while (i < children.length) {
      nodeScriptReplace(children[i]);
      i += 1;
    }
  }

  return node;
};

const counters = (code, where = 'body') => {
  const place = ['body', 'head'].includes(where) ? where : 'body';
  const block = document.createElement('div');
  block.innerHTML = code;
  const userEvents = () => {
    window.removeEventListener('scroll', userEvents);
    window.removeEventListener('mousemove', userEvents);
    document[place].append(...nodeScriptReplace(block).children);
  };
  window.addEventListener('scroll', userEvents);
  window.addEventListener('mousemove', userEvents);
};
counters(`<!-- Marquiz script start 11 --> 
<script> 
(function(w, d, s, o){ 
 var j = d.createElement(s); j.async = true; j.src = '//script.marquiz.ru/v2.js';j.onload = function() { 
 if (document.readyState !== 'loading') Marquiz.init(o); 
 else document.addEventListener("DOMContentLoaded", function() { 
 Marquiz.init(o); 
 }); 
 }; 
 d.head.insertBefore(j, d.head.firstElementChild); 
})(window, document, 'script',  { 
 host: '//quiz.marquiz.ru', 
 region: 'eu', 
 id: '61542bfc22fc0a003fc80c3d', 
 autoOpen: 15, 
 autoOpenFreq: 'always', 
 openOnExit: false, 
 disableOnMobile: false 
 } 
); 
</script> 
<!-- Marquiz script end -->

<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?169",t.onload=function(){VK.Retargeting.Init("VK-RTRG-1134831-7rwrX"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-1134831-7rwrX" style="position:fixed; left:-999px;" alt=""/></noscript>

<!-- Comfortel script start --> 
<script type="text/javascript">
var _calltr_obj=".phone";
</script>
<script src="http://virt117.pbx.comfortel.pro/call-tracking.js?_id=1624540836" type="text/javascript"></script>`, 'head');