/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/blocks/home-directions/home-directions.js":
/*!*******************************************************!*\
  !*** ./src/blocks/home-directions/home-directions.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   HomeDirections: () => (/* binding */ HomeDirections)
/* harmony export */ });
/* harmony import */ var _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/scripts/modules/ClassObserver */ "./src/common/scripts/modules/ClassObserver.js");


class HomeDirections {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-directions .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__.ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}


/***/ }),

/***/ "./src/blocks/home-feedback/home-feedback.js":
/*!***************************************************!*\
  !*** ./src/blocks/home-feedback/home-feedback.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   HomeFeedback: () => (/* binding */ HomeFeedback)
/* harmony export */ });
/* harmony import */ var _common_scripts_enums__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/scripts/enums */ "./src/common/scripts/enums.js");
/* harmony import */ var _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/scripts/modules/ClassObserver */ "./src/common/scripts/modules/ClassObserver.js");



class HomeFeedback {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-feedback .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 40,
    breakpoints: {
      [_common_scripts_enums__WEBPACK_IMPORTED_MODULE_0__.breakpoints.s]: {
        slidesPerView: 2,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_1__.ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}


/***/ }),

/***/ "./src/blocks/home-important/home-important.js":
/*!*****************************************************!*\
  !*** ./src/blocks/home-important/home-important.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   HomeImportant: () => (/* binding */ HomeImportant)
/* harmony export */ });
/* harmony import */ var _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/scripts/modules/ClassObserver */ "./src/common/scripts/modules/ClassObserver.js");
/* harmony import */ var _common_scripts_enums__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/scripts/enums */ "./src/common/scripts/enums.js");



class HomeImportant {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-important .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 20,
    breakpoints: {
      [_common_scripts_enums__WEBPACK_IMPORTED_MODULE_1__.breakpoints.s]: {
        slidesPerView: 4,
        spaceBetween: 40,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__.ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
    window.media.addChangeListener('min', 's', () => this.#slider.destroy());
    window.media.addChangeListener('max', 's', () => {
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    });
  };
}


/***/ }),

/***/ "./src/blocks/home-reasons/home-reasons.js":
/*!*************************************************!*\
  !*** ./src/blocks/home-reasons/home-reasons.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   HomeReasons: () => (/* binding */ HomeReasons)
/* harmony export */ });
/* harmony import */ var _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/scripts/modules/ClassObserver */ "./src/common/scripts/modules/ClassObserver.js");


class HomeReasons {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-reasons .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__.ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}


/***/ }),

/***/ "./src/blocks/home-staff/home-staff.js":
/*!*********************************************!*\
  !*** ./src/blocks/home-staff/home-staff.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   homeStaffInit: () => (/* binding */ homeStaffInit)
/* harmony export */ });
/* harmony import */ var _common_scripts_modules_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/scripts/modules/helpers */ "./src/common/scripts/modules/helpers.js");
/* harmony import */ var _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/scripts/modules/ClassObserver */ "./src/common/scripts/modules/ClassObserver.js");



class HomeStaff {
  #element = document.querySelector('.home-staff');
  #select = this.#element?.querySelector('.home-staff__select .form-input__field');
  #sliderElement = this.#element?.querySelector('.slider__slides');
  #slides = this.#sliderElement?.querySelectorAll('.home-staff__slide');
  #sliderWrapper = this.#sliderElement?.querySelector('.swiper-wrapper');
  #categories = {};
  #previousCategory = 'all';
  #slider;
  #html = document.querySelector('html');
  #observer;

  #sliderSettings = {
    pagination: {
      el: '.slider__pagination',
      dynamicBullets: true,
    },
  };

  #randomizeSlides = () => {
    const shuffledSlides = (0,_common_scripts_modules_helpers__WEBPACK_IMPORTED_MODULE_0__.shuffleArray)(Array.from(this.#slides));
    Array.from(this.#sliderWrapper.children).forEach((item) => item.remove());
    shuffledSlides.forEach((item) => this.#sliderWrapper.append(item));
  };

  #createCategories = () => {
    Array.from(this.#sliderWrapper.children).forEach((item) => {
      if (!this.#categories[item.dataset.category]) this.#categories[item.dataset.category] = [];
      this.#categories[item.dataset.category].push(item);
    });
    this.#categories.all = Array.from(this.#sliderWrapper.children);
  };

  #changeList = (event) => {
    const category = event.target.value;
    if (category !== this.#previousCategory) {
      Array.from(this.#sliderWrapper.children)
        .forEach((item) => item.remove());
      this.#categories[category].forEach((item) => this.#sliderWrapper.append(item));
      this.#slider.instance.update();
      this.#slider.instance.slideTo(0, 10);
      this.#previousCategory = category;
    }
  };

  init = () => {
    if (!this.#element) return;
    this.#randomizeSlides();
    this.#createCategories();
    this.#slider = new window.Slider(this.#sliderElement, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_1__.ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
    this.#select?.addEventListener('change', this.#changeList);
  };
}

const homeStaffInit = () => {
  const homeStaff = new HomeStaff();
  homeStaff.init();
};


/***/ }),

/***/ "./src/blocks/slider-world/slider-world.js":
/*!*************************************************!*\
  !*** ./src/blocks/slider-world/slider-world.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   SliderWorld: () => (/* binding */ SliderWorld)
/* harmony export */ });
/* harmony import */ var _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/scripts/modules/ClassObserver */ "./src/common/scripts/modules/ClassObserver.js");


class SliderWorld {
  #html = document.querySelector('html');
  #element = document.querySelector('.slider-world .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 0,
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new _common_scripts_modules_ClassObserver__WEBPACK_IMPORTED_MODULE_0__.ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}


/***/ }),

/***/ "./src/common/scripts/enums.js":
/*!*************************************!*\
  !*** ./src/common/scripts/enums.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   breakpoints: () => (/* binding */ breakpoints)
/* harmony export */ });
/* harmony import */ var _settings_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../settings.json */ "./src/common/settings.json");


const { breakpoints } = _settings_json__WEBPACK_IMPORTED_MODULE_0__;


/***/ }),

/***/ "./src/common/scripts/modules/ClassObserver.js":
/*!*****************************************************!*\
  !*** ./src/common/scripts/modules/ClassObserver.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   ClassObserver: () => (/* binding */ ClassObserver)
/* harmony export */ });
class ClassObserver {
  #targetNode;
  #classToWatch;
  #classAddedCallback;
  #classRemovedCallback;
  #observer;
  #lastClassState;

  constructor(targetNode, classToWatch, classAddedCallback, classRemovedCallback) {
    this.#targetNode = targetNode;
    this.#classToWatch = classToWatch;
    this.#classAddedCallback = classAddedCallback;
    this.#classRemovedCallback = classRemovedCallback;
    this.#observer = null;
    this.#lastClassState = targetNode.classList.contains(this.#classToWatch);
  }

  init = () => {
    this.observer = new MutationObserver(this.mutationCallback);
    this.observe();
  };

  observe = () => this.observer.observe(this.#targetNode, { attributes: true });

  disconnect = () => this.observer.disconnect();

  mutationCallback = (mutationsList) => {
    mutationsList.forEach((mutation) => {
      if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
        const currentClassState = mutation.target.classList.contains(this.#classToWatch);
        if (this.lastClassState !== currentClassState) {
          this.lastClassState = currentClassState;
          if (currentClassState) this.#classAddedCallback();
          else this.#classRemovedCallback();
        }
      }
    });
  };
}


/***/ }),

/***/ "./src/common/scripts/modules/helpers.js":
/*!***********************************************!*\
  !*** ./src/common/scripts/modules/helpers.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   removeHashFromUrl: () => (/* binding */ removeHashFromUrl),
/* harmony export */   shuffleArray: () => (/* binding */ shuffleArray)
/* harmony export */ });
const shuffleArray = (array) => Array(array.length).fill(null)
  .map((_, i) => [Math.random(), i])
  .sort(([a], [b]) => a - b)
  .map(([, i]) => array[i]);

const removeHashFromUrl = () => {
  const uri = window.location.toString();
  if (uri.indexOf('#') > 0) {
    const cleanUri = uri.substring(0, uri.indexOf('#'));
    window.history.replaceState({}, document.title, cleanUri);
  }
};


/***/ }),

/***/ "./src/common/settings.json":
/*!**********************************!*\
  !*** ./src/common/settings.json ***!
  \**********************************/
/***/ ((module) => {

module.exports = JSON.parse('{"siteName":"Medall","breakpoints":{"s":"720","m":"1200","l":"1600"}}');

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!********************************!*\
  !*** ./src/pages/main/main.js ***!
  \********************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocks_home_important_home_important__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../blocks/home-important/home-important */ "./src/blocks/home-important/home-important.js");
/* harmony import */ var _blocks_home_staff_home_staff__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../blocks/home-staff/home-staff */ "./src/blocks/home-staff/home-staff.js");
/* harmony import */ var _blocks_home_reasons_home_reasons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../blocks/home-reasons/home-reasons */ "./src/blocks/home-reasons/home-reasons.js");
/* harmony import */ var _blocks_home_feedback_home_feedback__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../blocks/home-feedback/home-feedback */ "./src/blocks/home-feedback/home-feedback.js");
/* harmony import */ var _blocks_slider_world_slider_world__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../blocks/slider-world/slider-world */ "./src/blocks/slider-world/slider-world.js");
/* harmony import */ var _blocks_home_directions_home_directions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../blocks/home-directions/home-directions */ "./src/blocks/home-directions/home-directions.js");







document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new _blocks_home_important_home_important__WEBPACK_IMPORTED_MODULE_0__.HomeImportant();
  (0,_blocks_home_staff_home_staff__WEBPACK_IMPORTED_MODULE_1__.homeStaffInit)();
  // eslint-disable-next-line no-new
  new _blocks_home_directions_home_directions__WEBPACK_IMPORTED_MODULE_5__.HomeDirections();
  // eslint-disable-next-line no-new
  new _blocks_home_reasons_home_reasons__WEBPACK_IMPORTED_MODULE_2__.HomeReasons();
  // eslint-disable-next-line no-new
  new _blocks_home_feedback_home_feedback__WEBPACK_IMPORTED_MODULE_3__.HomeFeedback();
  // eslint-disable-next-line no-new
  new _blocks_slider_world_slider_world__WEBPACK_IMPORTED_MODULE_4__.SliderWorld();
});

})();

/******/ })()
;
//# sourceMappingURL=main.js.map