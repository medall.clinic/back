<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="allDoctorBlock">
	<div class="allDoctorBlock__slider">
		<?foreach($arResult["ROWS"] as $arItems):?>
			<?foreach($arItems as $arItem):?>
				<?if(is_array($arItem)):?>
					<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));?>
					<div class="allDoctorBlock__item">
						<div class="allDoctorBlock__item__wrap">
							<div class="allDoctorBlock__item__imgWrap">
								<a class="allDoctorBlock__item__img" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=$arItem["PICTURE"]["SRC"]?>);"></a>
							</div>
							<a class="allDoctorBlock__item__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
							<div class="allDoctorBlock__item__work"><?echo $arItem['PREVIEW_TEXT']?></div>
						</div>
					</div>
				<?endif?>
			<?endforeach?>
		<?endforeach?>
	</div>
	<div class="allDoctorBlock__controls">
		<div class="allDoctorBlock__control allDoctorBlock__prev">
		</div>
		<div class="allDoctorBlock__control allDoctorBlock__next">
		</div>
	</div>
</div>