<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//delayed function must return a string
if(empty($arResult)) return "";
if($arResult[count($arResult)-1]["LINK"]!="" && $arResult[count($arResult)-1]["LINK"]!=$GLOBALS["APPLICATION"]->GetCurPage(false))
$arResult[] = Array("TITLE"=>$GLOBALS["APPLICATION"]->GetTitle());

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++){
	if($index!=0){$strReturn .= '&nbsp;&mdash;&nbsp;';}
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "" && $arResult[$index]["LINK"]!=$GLOBALS["APPLICATION"]->GetCurPage(false))
		$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">'.$title.'</a>';
	else
		$strReturn .= '<span id="bx_breadcrumb_'.$index.'" class="bx-breadcrumbs-name">'.$title.'</span>';
}
$strReturn .= '</p><b class="r0 bottom"></b>';
return $strReturn;
?>
