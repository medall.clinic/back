<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="mainContent__title__desc"><p>Статьи</p></div>
<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
	<div class="mainContent__title title">
		<h1><?=$arResult["NAME"]?></h1>
	</div>
<?endif;?>
<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
	<div class="mainContent__title__desc">
		<p><?=$arResult["DISPLAY_ACTIVE_FROM"]?></p>
	</div>
<?endif;?>
<div class="mainContent__text">
	<p><?echo $arResult["PREVIEW_TEXT"];?></p>
	<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="картинка" title="Грудные импланты">
	<?echo $arResult["DETAIL_TEXT"];?>
	
	<?$res = CIBlockElement::GetByID($arResult['PROPERTIES']['author_articles']['VALUE']);
	if($ar_res = $res->GetNext()){
		$db_props = CIBlockElement::GetProperty(6, $arResult['PROPERTIES']['author_articles']['VALUE'], array("sort" => "asc"));
		if($ar_props = $db_props->Fetch()){?>
			<div class="articleCardBlock__author">
				<p class="articleCardBlock__author__title">Автор статьи:</p>
				<p class="articleCardBlock__author__name">
					<span><?echo $ar_res['NAME']?></span> <?echo $ar_props['VALUE']?> </p>
				<p class="articleCardBlock__author__desc"><?echo $ar_res['PREVIEW_TEXT']?></p>
			</div><?
		}
	}?>
	<div class="articleCardBlock__hashTag">
		<?foreach($arResult["FIELDS"] as $code=>$value):?>
			<a href="/search/index.php?tags=<?=$value?>" target=_blank># <?echo $value;?></a>
		<?endforeach;?>
	</div>
	<a class="articleBack" href="<?=$arResult["LIST_PAGE_URL"]?>">Назад</a>
</div>