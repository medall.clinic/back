<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form class="search__form" method="get" enctype="multipart/form-data" action="<?=$arResult["FORM_ACTION"]?>">
	<div class="search__form__wrap">
		<input name="q" type="search" placeholder="Введите слово для поиска" required>
		<button class="buttonTwo search__button" type="submit">Искать</button>
	</div>
	<!--<div id="search-button">
		<input type="submit" name="s" id="search-submit-button" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" onfocus="this.blur();">
	</div>
	<div class="search-box"><input type="text" name="q"></div>-->
</form>