<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="reviewPage__videoReview__block">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
		<div class="reviewPage__block">
			<div class="reviewPage__block__left">
				<a class="reviewPage__block__linkImg" href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" data-fancybox="reviews">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="Отзыв">
				</a>
			</div>
			<div class="reviewPage__block__right">
				<div class="reviewPage__block__content">
					<p>Процедура: <span class="green"><?echo $arItem["NAME"]?></span></p>
					<p>Врач: <span class="green"><?=$arItem["DISPLAY_PROPERTIES"]['doctor_review']['DISPLAY_VALUE']?></span></p>
					<div class="reviewPage__block__date"><p><?=$arItem['ACTIVE_FROM']?></p></div>
					<div class="reviewPage__block__text"><p><?=$arItem['PREVIEW_TEXT']?></p></div>
					<div class="reviewPage__block__link">
						<a class="more" href="#">Смотреть результаты</a>
					</div>
				</div>
			</div>
		</div>
	<?endforeach;?>
</div>