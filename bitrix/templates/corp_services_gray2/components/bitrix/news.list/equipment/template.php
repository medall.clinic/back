<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arResult2=array();
foreach($arResult["ITEMS"] as $arItem){
	$arResult2[$arItem['PROPERTIES']['direction_equipment']['VALUE_ENUM_ID']]['NAME']=$arItem['PROPERTIES']['direction_equipment']['VALUE_ENUM'];
	$arResult2[$arItem['PROPERTIES']['direction_equipment']['VALUE_ENUM_ID']]['ITEMS'][]=$arItem;
}
foreach($arResult2 as $arItem2){
	?><div class="equipment__direction"><?=$arItem2['NAME']?></div>
	<div class="equipment__block"><?
		foreach($arItem2["ITEMS"] as $arItem){?>
			<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
			
			<div class="equipment__item">
				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
					<a class="equipment__item__title openPopup" href="#equipment<?=$arItem["ID"]?>">
						<span><?echo $arItem["NAME"]?></span>
					</a>
				<?endif;?>
				<a class="equipment__item__img openPopup" href="#equipment<?=$arItem["ID"]?>" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
				<div class="equipment__item__content">
					<div class="equipment__item__desc">
						<?if($arItem['PROPERTIES']['producing_country']['VALUE']){?>
							<div class="equipment__item__manufacturer">
								<div class="equipment__item__manufacturer__title">Страна производитель:</div>
								<div class="equipment__item__manufacturer__text"><?=$arItem['PROPERTIES']['producing_country']['VALUE']?></div>
							</div>
						<?}
						if($arItem['PROPERTIES']['producing_country']['VALUE']){?>
							<div class="equipment__item__manufacturer">
								<div class="equipment__item__manufacturer__title">Компания:</div>
								<div class="equipment__item__manufacturer__text"><?=$arItem['PROPERTIES']['company_equipment']['VALUE']?></div>
							</div>
						<?}?>
					</div>
					<div class="equipment__item__text">
						<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
							<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						<?endif;?>
					</div>
				</div>
			</div>
			
			<div class="popupEquipment" id="equipment<?=$arItem["ID"]?>">
				<div class="popup__close">
					<div class="popup__close__line"></div>
					<div class="popup__close__line"></div>
				</div>
				<div class="popupEquipment__block">
					<div class="popupEquipment__img" style="background-image:url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"></div>
					<div class="popupEquipment__text">
						<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
							<a class="equipment__item__title openPopup" href="#equipment<?=$arItem["ID"]?>">
								<div class="equipment__item__title"><?echo $arItem["NAME"]?></div>
							</a>
						<?endif;?>
						<div class="equipment__item__desc">
							<?if($arItem['PROPERTIES']['producing_country']['VALUE']){?>
								<div class="equipment__item__manufacturer">
									<div class="equipment__item__manufacturer__title">Страна производитель:</div>
									<div class="equipment__item__manufacturer__text"><?=$arItem['PROPERTIES']['producing_country']['VALUE']?></div>
								</div>
							<?}
							if($arItem['PROPERTIES']['producing_country']['VALUE']){?>
								<div class="equipment__item__manufacturer">
									<div class="equipment__item__manufacturer__title">Компания:</div>
									<div class="equipment__item__manufacturer__text"><?=$arItem['PROPERTIES']['company_equipment']['VALUE']?></div>
								</div>
							<?}?>
						</div>
						<div class="equipment__item__text">
							<?echo $arItem["DETAIL_TEXT"];?>
						</div>
					</div>
				</div>
			</div>
		<?}?>
	</div>
<?}?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>