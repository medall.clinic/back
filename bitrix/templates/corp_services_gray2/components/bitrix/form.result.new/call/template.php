<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" class="popup__content-form" data-wow-delay="0.7s">
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
	<?=bitrix_sessid_post()?>
	
	<input class="phone" type="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_242"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_242"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_242"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_242"]["CAPTION"]?>"
	required>
	
	<input type="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_377"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_377"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_377"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_377"]["CAPTION"]?>"
	required>
	<div class="popup__content-form-block">
		<div class="popup__content-form-block-button">
			<input class="button popup__content-form-button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
		</div>
	</div>
</form>