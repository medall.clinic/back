<?
class ConsultationRegisterModel extends BaseModel
{
	const EVENT_NAME = "CONSULTATION_REGISTER";
	const SUBJECT_INIT = "Запись на консультацию";
	const IBLOCK_TYPE = "contacts";
	const SEND_EMAIL = false;

	public $name;
	public $phone;
	public $email;
	public $comment;
	public $preferableConnection;
	public $curPage;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = IBLOCK_ID_CONSULTATION_REGISTER;
		}

		$this->name = $request["name"];
		$this->phone = $request["phone"];
		$this->email = $request["email"];
		$this->comment = $request["comment"];
		$this->preferableConnection = implode(', ', $request["preferable_connection"]);
		$this->curPage = $this->getCurPage($request["cur_page"]);

		if (empty($this->name)) {throw new Exception("Name is empty");}
		if (empty($this->phone)) {throw new Exception("Phone is empty");}
	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"NAME" => $this->name,
				"PHONE" => $this->phone,
				"EMAIL" => $this->email,
				"COMMENT" => $this->comment,
				"PREFERABLE_CONNECTION" => $this->preferableConnection,
				"CURRENT_PAGE" => $this->curPage,
			]
		];

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		$this->messages[] = "New IBlock element created, Id = " . $newElementId;

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["NAME"] = $this->name;
		$emailFields["PHONE"] = $this->phone;
		$emailFields["EMAIL"] = $this->email;
		$emailFields["COMMENT"] = $this->comment;
		$emailFields["PREFERABLE_CONNECTION"] = $this->preferableConnection;
		$emailFields["CURRENT_PAGE"] = $this->curPage;

		return $emailFields;
	}

    public function doPostAction()
    {

        $apiUrl = 'https://medall24.bitrix24.ru/rest/30/mqmuxc440l45s9fw/crm.lead.add.json';

// https://cp55162-bitrix-lpv3n.tw1.ru/rest/1/ id ответсвенного

// код вебхука i7egktog30g60i15 обязательно

// метод crm.lead.add обязательно


// формируем параметры для создания лида
        if(!empty($_POST['name']) && !empty($_POST['phone']) && !empty($this->curPage) && empty($_POST['title'])){
            $sourceDescription = $this->curPage;
            $sourceDescription .= "\n-------------------\n";
            $leadData = [
                'fields' => [
                    "TITLE" => $this->curPage,
                    "NAME" => $this->name,
                    "PHONE" => [
                        "0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'OPENED' => 'N', // Доступно для всех
                    'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
                    "UF_CRM_5D8E19ED4F970" => ["VALUE" => $this->email],
                    "COMMENTS" => $this->comment,
                    "SOURCE_DESCRIPTION" => $sourceDescription,
                    'UTM_SOURCE'=>$_POST['utm_source'],

                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];


// Преобразуем данные в формат JSON
            $jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',

            ));
// Выполнение запроса
            $response = curl_exec($ch);

// Проверка на ошибки
            if ($response === false) {
                echo 'Ошибка cURL: ' . curl_error($ch);
            } else {
                // Обработка ответа
                $responseData = json_decode($response, true);


            }

// Закрытие cURL-соединения
            curl_close($ch);
        }

        if(!empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['title'])){
            $sourceDescription = 'Запрос ' . $_POST['title'];
            $sourceDescription .= "\n-------------------\n";
            $leadData = [
                'fields' => [
                    "TITLE" => $_POST['title'],
                    "NAME" => $this->name,
                    "PHONE" => [
                        "0" => [
                            "VALUE" => $this->phone,
                            "VALUE_TYPE" => "WORK",
                        ],
                    ],
                    'OPENED' => 'N', // Доступно для всех
                    'SOURCE_ID' => 'WEBFORM', // Источник вебсайт
                    "UF_CRM_5D8E19ED4F970" => ["VALUE" => $this->email],
                    "COMMENTS" => $this->comment,
                    "SOURCE_DESCRIPTION" => $sourceDescription,
                    'UTM_SOURCE'=>$_POST['utm_source'],

                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];


// Преобразуем данные в формат JSON
            $jsonData = json_encode($leadData);

// отправляем запрос в Б24 и обрабатываем ответ
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',

            ));
// Выполнение запроса
            $response = curl_exec($ch);

// Проверка на ошибки
            if ($response === false) {
                echo 'Ошибка cURL: ' . curl_error($ch);
            } else {
                // Обработка ответа
                $responseData = json_decode($response, true);


            }

// Закрытие cURL-соединения
            curl_close($ch);
        }



    }

	private function getCurPage($cur_page)
	{
		if($cur_page == "/plastic/"){
			return 'Запрос на консультацию с главной страницы направления "Пластическая хирургия"';
		}

		if($cur_page == "/cosmetology/"){
			return 'Запрос на консультацию с главной страницы направления "Косметология"';
		}

		if($cur_page == "/dentistry/"){
			return 'Запрос на консультацию с главной страницы направления "Стоматология"';
		}

		return "Запрос на консультацию с главной страницы";
	}

}



