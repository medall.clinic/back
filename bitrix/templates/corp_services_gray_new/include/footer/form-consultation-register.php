<form
	id="feedBackForm"
	name="SIMPLE_FORM_1"
	action="/"
	method="POST"
	class="feedBackForm wow animated fadeIn consultation_register validated-form submitJS"
	data-wow-delay="0.7s"
>

	<div class="feedBackForm__block">
		<div class="feedBackForm__left">
			<input type="text" name="name" placeholder="Ваше имя" required="">
			<input type="text" name="phone" placeholder="Телефон" class="phone" required="">
			<? /* <input type="email" name="email" placeholder="E-mail"> */?>
		</div>
		<div class="feedBackForm__right">
			<textarea name="comment" placeholder="Ваши вопросы или комментарии"></textarea>
		</div>
	</div>

	<div class="feedBackForm__checkbox__title">
		<p>Предпочтительный способ связи:</p>
	</div>

	<div class="feedBackForm__checkbox__wrap">

		<label class="feedBackForm__checkbox__label">
			<input type="checkbox" name="preferable_connection[]" id="5" value="Телефон">
			<span class="checkbox__feedBack"><svg class="checkboxIcon" width="19" height="29"><use xlink:href="#smartphone"></use></svg><span>Телефон</span></span>
		</label>

		<label class="feedBackForm__checkbox__label">
			<input type="checkbox" name="preferable_connection[]" id="6" value="E-mail">
			<span class="checkbox__feedBack"><svg class="checkboxIcon" width="28" height="21"><use xlink:href="#letter"></use></svg><span>E-mail</span></span>
		</label>

		<label class="feedBackForm__checkbox__label">
			<input type="checkbox" name="preferable_connection[]" id="7" value="Whatsapp">
			<span class="checkbox__feedBack"><svg class="checkboxIcon" width="31" height="30"><use xlink:href="#whatsapp"></use></svg><span>Whatsapp</span></span>
		</label>

		<label class="feedBackForm__checkbox__label">
			<input type="checkbox" name="preferable_connection[]" id="8" value="Telegram">
			<span class="checkbox__feedBack"><svg class="checkboxIcon" width="32" height="30"><use xlink:href="#telegram"></use></svg><span>Telegram</span></span>
		</label>

		<label class="feedBackForm__checkbox__label">
			<input type="checkbox" name="preferable_connection[]" id="9" value="Viber">
			<span class="checkbox__feedBack"><svg class="checkboxIcon" width="27" height="30"><use xlink:href="#viber"></use></svg><span>Viber</span></span>
		</label>

	</div>

	<input id="feedBackFormbutton" class="button feedBackForm__button" type="submit" name="web_form_submit" value="Отправить запрос">

    <? global $USER ?>
	<? if(!$USER->IsAuthorized()): ?>
	<div class="agreements">
		<label class="form__label feedBackForm__label">
			<input type="checkbox" name="soglasheniya" value="" required="" id="soglasheniya">
			<span class="checkbox__box"></span>
			<span class="form__label__text">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.</span>
		</label>
		<label class="form__label feedBackForm__label">
			<input type="checkbox" name="news_subscribe" value="50" required="" id="news_subscribe">
			<span class="checkbox__box"></span>
			<span class="form__label__text">Согласен(а) <a href="#">получать полезную информацию об акциях и специальных предложениях</a></span>
		</label>
	</div>
    <? endif ?>

    <input type="hidden" name="cur_page" value="<?= $curPage ?>">

    <? if(!empty($_GET)): ?>
        <? foreach ($_GET as $paramName => $paramValue): ?>
            <? if(strpos($paramName, 'utm_') !== false): ?>
                <input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
            <? endif ?>
        <? endforeach ?>
	<? endif ?>

</form>
