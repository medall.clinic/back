function ajaxFormSubmit(params)
{
    var event = params['event'];
    var thisForm = event.target;
    var url;
    var dataHandler = params['handler'];
    var successRedirect = params['redirect'];
    var successAction = params['successAction'];
    var popupTitle = params['popupTitle'];
    var popupText = params['popupText'];
    var errorPopupTitle = params['errorPopupTitle'];
    var errorPopupText = params['errorPopupText'];
    var btnCloseText = params['btnCloseText'];
    var formReset = params['formReset'];
    var model = params['model'];
    var message = params['message'];

    var data = new FormData(thisForm);

    var b24CrmGuestUtm = localStorage.getItem("b24_crm_guest_utm");
    if(b24CrmGuestUtm){
        data.append("b24_crm_guest_utm", b24CrmGuestUtm);
    }

    if (dataHandler != undefined) {
        url = '/bitrix/templates/corp_services_gray_new/ajax/' + dataHandler + '.php';
        if(model){
            url += "?model=" + model;
        }
    }else{
        url = window.location.href;
    }

    jQuery.ajax({
        url: url,
        data: data,
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data){
            if (data.result == "success") {

                if(successRedirect){
                    document.location.href = successRedirect;
                }

                if(formReset){
                    thisForm.reset();
                }

                if(successAction){
                    eval(successAction);
                }

                if(message){
                    var modalBody = $(thisForm).parents('.modal-content');
                    if(modalBody.length){
                        var messageText  = '<div class="fancybox-modal__title">' + message.title + '</div>';
                        messageText += message.text;
                        modalBody.html(messageText);
                    }
                }

            }else{
                popupTitle = errorPopupTitle;
                if(data.message != undefined){
                    popupText = data.message;
                }else{
                    popupText = errorPopupText;
                }
            }

            if(popupText) {
                var objButtonClose = false;
                if(btnCloseText != undefined)
                {
                    objButtonClose = new BX.PopupWindowButton({
                        text: btnCloseText,
                        className: '',
                        events: {
                            click : function(){this.popupWindow.close()}
                        }
                    })
                }

                var oPopup = new BX.PopupWindow('call-offer-form-wrapper', window.body, {
                    autoHide : true,
                    offsetTop : 1,
                    offsetLeft : 0,
                    lightShadow : true,
                    closeIcon : true,
                    closeByEsc : true,
                    overlay: {
                        backgroundColor: 'black', opacity: '80'
                    },
                    buttons: [objButtonClose],
                    titleBar: popupTitle
                });

                oPopup.setContent(popupText);
                oPopup.show();

                thisForm.reset();
            }


        },
        error: function(data, textStatus, errorThrown){
            console.log("Error");
            console.log(data);
        },
        complete: function(data, textStatus){}

    });

}






