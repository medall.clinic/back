<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"){
	if ($arResult["isFormTitle"]){?>
		<div class="blockFeedBack__title title wow animated fadeInLeft" data-wow-delay="0.5s">
			<h2><?=$arResult["FORM_TITLE"]?></h2>
		</div><?
	}
	if ($arResult["isFormImage"] == "Y"){?>
		<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
		<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
		<?
	}?>
	<p><?=$arResult["FORM_DESCRIPTION"]?></p><?
}?><!--test-->
<form id="feedBackForm" name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" class="feedBackForm wow animated fadeIn" data-wow-delay="0.7s">
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
	<?=bitrix_sessid_post()?>
	<div class="feedBackForm__block">
		<div class="feedBackForm__left">
			<input
			name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["STRUCTURE"][0]["QUESTION_ID"]?>"
			type="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
			placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["CAPTION"]?>"
			required>
			<input class="phone"
			name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["STRUCTURE"][0]["QUESTION_ID"]?>"
			type="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
			placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["CAPTION"]?>"
			required>
			<input
			name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_554"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_554"]["STRUCTURE"][0]["QUESTION_ID"]?>"
			type="email"
			placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_554"]["CAPTION"]?>" required>
		</div>
		<div class="feedBackForm__right">
			<textarea
			name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_455"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_455"]["STRUCTURE"][0]["QUESTION_ID"]?>"
			placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_455"]["CAPTION"]?>"
			></textarea>
		</div>
	</div>
	<div class="feedBackForm__checkbox__title"><p><?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["CAPTION"]?>:</p></div>
	<div class="feedBackForm__checkbox__wrap">
		<label class="feedBackForm__checkbox__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_546[]"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][0]["ID"]?>"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][0]["ID"]?>">
			<span class="checkbox__feedBack">
				<svg class="checkboxIcon" width="19" height="29">
					<use xlink:href="#smartphone"></use>
				</svg>
				<span>Телефон</span>
			</span>
		</label>
		<label class="feedBackForm__checkbox__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_546[]"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][1]["ID"]?>"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][1]["ID"]?>">
			<span class="checkbox__feedBack">
				<svg class="checkboxIcon" width="28" height="21">
					<use xlink:href="#letter"></use>
				</svg>
				<span>E-mail</span>
			</span>
		</label>
		<label class="feedBackForm__checkbox__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_546[]"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][2]["ID"]?>"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][2]["ID"]?>">
			<span class="checkbox__feedBack">
				<svg class="checkboxIcon" width="31" height="30">
					<use xlink:href="#whatsapp"></use>
				</svg>
				<span>Whatsapp</span>
			</span>
		</label>
		<label class="feedBackForm__checkbox__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_546[]"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][3]["ID"]?>"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][3]["ID"]?>">
			<span class="checkbox__feedBack">
				<svg class="checkboxIcon" width="32" height="30">
					<use xlink:href="#telegram"></use>
				</svg>
				<span>Telegram</span>
			</span>
		</label>
		<label class="feedBackForm__checkbox__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_546[]"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][4]["ID"]?>"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_546"]["STRUCTURE"][4]["ID"]?>">
			<span class="checkbox__feedBack">
				<svg class="checkboxIcon" width="27" height="30">
					<use xlink:href="#viber"></use>
				</svg>
				<span>Viber</span>
			</span>
		</label>
	</div>
	<?/*if($arResult["isUseCaptcha"] == "Y"){?>
		<label class="form__label feedBackForm__label">
			<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><br />
			<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
			<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
		</label>
	<?}*/?> 
	<input id="feedBackFormbutton" class="button feedBackForm__button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="Отправить запрос" />
	
	<div class="agreements">
    <label class="form__label feedBackForm__label">
      <input type="checkbox"
      name="form_checkbox_SIMPLE_QUESTION_903_CPfSL[]"
      value=""
      required>
      <span class="checkbox__box"></span>
      <span class="form__label__text">Нажимая на кнопку, я даю свое 
        <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.
      </span>
    </label>
		<? /* <label class="form__label feedBackForm__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_903[]"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903"]["STRUCTURE"][0]["ID"]?>"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903"]["STRUCTURE"][0]["ID"]?>"
			required>
			<span class="checkbox__box"></span>
			<span class="form__label__text"><?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903"]["CAPTION"]?>
				<a href="#">с политикой конфеденциальности</a>
			</span>
		</label> */ ?>
		<label class="form__label feedBackForm__label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_903_CPfSL[]"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903_CPfSL"]["STRUCTURE"][0]["ID"]?>"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903_CPfSL"]["STRUCTURE"][0]["ID"]?>"
			required>
			<span class="checkbox__box"></span>
			<span class="form__label__text"><?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903_CPfSL"]["CAPTION"]?>
				<a href="#"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_903_CPfSL"]["STRUCTURE"][0]['MESSAGE']?></a>
			</span>
		</label>
	</div>
	
</form>
