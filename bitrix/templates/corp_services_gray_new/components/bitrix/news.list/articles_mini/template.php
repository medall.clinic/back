<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$rez = explode('/',$_SERVER['SCRIPT_NAME']);
foreach($arResult["ITEMS"] as $arItem){?>
	<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	if($rez[1] == $arItem['PROPERTIES']['display_page_articles']['VALUE_XML_ID']){?>
		<div class="articlePlasticSurgery__item articlePlasticSurgery__textLeft wow animated fadeIn" data-wow-delay="0.9s" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
			<div class="articlePlasticSurgery__item__desc title beforeLine">
				<p><?echo $arItem["NAME"]?></p>
			</div>
			<?if($arItem['PROPERTIES']['current_articles']['VALUE']!=''){?>
				<div class="articlePlasticSurgery__item__currentArticles"><p>Актуальные статьи</p></div><?
			}?>
		</div>
		<div class="articlePlasticSurgery__item articlePlasticSurgery__textRight wow animated fadeInRight" data-wow-delay="1.1s">
			<div class="articlePlasticSurgery__item__text"><p><?echo $arItem["PREVIEW_TEXT"];?></p></div>
			<div class="articlePlasticSurgery__item__buttons">
				<a class="buttonTwo" href="<?echo $arItem["DETAIL_PAGE_URL"]?>">Читать целиком</a> <a class="more" href="#">Узнать больше</a>
			</div>
		</div>
	<?}
}?>