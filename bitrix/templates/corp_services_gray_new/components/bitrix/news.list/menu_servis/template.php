<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$rez = array();
foreach($arResult["ITEMS"] as $arItem){
	if(!isset($rez[$arItem["IBLOCK_SECTION_ID"]]["order"])){
		$obSection = CIBlockSection::GetList(array(), array("ID"=>$arItem["IBLOCK_SECTION_ID"]));
		while($arSection = $obSection->Fetch()){
			$rez[$arItem["IBLOCK_SECTION_ID"]]["order"] = $arSection['SORT'];
		}
	}
	$rez[$arItem["IBLOCK_SECTION_ID"]][] = $arItem;
}

/****************************** Добавляем пустые категории ************************************************/
global $filterDirection;
if(!empty($filterDirection["SECTION_ID"])){
	$resSections = CIBlockSection::GetList([], ["SECTION_ID" => $filterDirection["SECTION_ID"]]);
	while($section = $resSections->Fetch()){
		$sectionId = $section["ID"];
		if(!in_array($sectionId, array_keys($rez))){
			$rez[$sectionId] = ["order" => $section["SORT"]];
		}
	}
}
/****************************** /Добавляем пустые категории ***********************************************/


function sortByOrderField($a, $b) {
    if ($a["order"] == $b["order"]) {
        return 0;
    }
    return ($a["order"] < $b["order"]) ? -1 : 1;
}
uasort($rez, 'sortByOrderField');

$parentName = $arResult['NAME'];

foreach($rez as $k => $arResult){
	unset($arResult['order']);
	$obSection = CIBlockSection::GetList(array(), array("ID"=>$k));
	while($arSection = $obSection->Fetch()){
		$URL = CFile::GetPath($arSection["PICTURE"]);
		$NAME = $arSection['NAME'];
	}?>
	<div class="col">
		<div class="menuCategory__item">
			<div class="bgi" style="background-image: url('<?=$URL?>')"></div>
			<div class="content-wrapper">
				<? /* <div class="menuCategory__item__section_title"><?=$parentName?></div> */ ?>
				<div class="menuCategory__item__title"><h2><?=$NAME?></h2></div>
				<div class="menuCategory__item__submenu">
					<div class="menuCategory__item__submenu__wrap">
						<ul class="menuCategory__item__submenu__list">
							<?foreach($arResult as $arItem){
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
								<li class="menuCategory__item__submenu__item">
									<a class="menuCategory__item__submenu__link" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
								</li>
							<?}?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?}?>
