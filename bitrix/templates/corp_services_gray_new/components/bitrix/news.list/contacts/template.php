<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="contactsPage__content">
	<?$arResult2 = array();
	$Address = array();
	foreach($arResult["ITEMS"] as $arItem){$arResult2[$arItem["IBLOCK_SECTION_ID"]][]=$arItem;}
	foreach($arResult2 as $k => $arItem){?>
		<div class="contactsPage__content__item">
			<?$res = CIBlockSection::GetByID($k);
			if($ar_res = $res->GetNext()){?>
				<div class="contactsPage__content__title"><?=$ar_res['NAME']?></div>
				<div class="contactsPage__content__openingHours"><?=$ar_res['DESCRIPTION']?></div>
				<div class="contactsPage__content__connection">
					<?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_8_SECTION",$k,"UF_PHONE");
					if($arUF["UF_PHONE"]["VALUE"]!= ""){?>
						<a href="tel:<?=$arUF["UF_PHONE"]["VALUE"]?>"><?=$arUF["UF_PHONE"]["VALUE"]?></a><?
					}
					$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_8_SECTION",$k,"UF_EMAIL");
					if($arUF["UF_EMAIL"]["VALUE"]!= ""){?>
						<a href="mailto:<?=$arUF["UF_EMAIL"]["VALUE"]?>"><?=$arUF["UF_EMAIL"]["VALUE"]?></a><?
					}?>
				</div><?
			}?>
			<div class="contactsPage__content__addressBlock">
				<?foreach($arItem as $j){
					$tmp = explode(",", $j["DISPLAY_PROPERTIES"]['Address']['VALUE']);
					$point = array();
					$point["cord"] = Array((float)$tmp[0],(float)$tmp[1]);
					$point["balloon"] = $j['NAME'];
					$Address[] = $point;
					$this->AddEditAction($j['ID'], $j['EDIT_LINK'], CIBlock::GetArrayByID($j["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($j['ID'], $j['DELETE_LINK'], CIBlock::GetArrayByID($j["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
					<div class="contactsPage__content__address">
						<p><?=$j['NAME']?></p>
						<a class="contactsPage__content__address__linkDesc" href="#">Как добраться?</a>
						<div class="contactsPage__content__address__desc">
							<p><?=$j['DETAIL_TEXT']?></p>
						</div>
					</div><?
				}?>
			</div>
		</div><?
	}?>
</div>
<script type="text/javascript">window.map_addresses = <?echo json_encode($Address)?></script>