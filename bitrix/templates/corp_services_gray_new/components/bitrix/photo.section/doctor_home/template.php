<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="allDoctorBlock allDoctorBlock_home">

    <div class="allDoctorBlock__filter">
        <a href="#" data-direction="dentistry">Стоматология</a>
        <a href="#" data-direction="cosmetology">Косметология</a>
        <a href="#" data-direction="plastic">Пластическая хирургия</a>
        <a href="#" data-direction="phlebology">Флебология</a>
        <a href="#" data-direction="hair_transplant">Пересадка волос</a>
    </div>

	<div class="allDoctorBlock__slider">
		<?foreach($arResult["ROWS"] as $arItems):?>
			<?foreach($arItems as $arItem):?>

				<?if(is_array($arItem)):?>

					<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));?>

					<? $direction = $arItem["PROPERTIES"]["direction_doctor"]["VALUE_XML_ID"]; ?>

                    <div class="allDoctorBlock__item" data-direction="<?= $direction ?>">

						<div class="allDoctorBlock__item__wrap">
							<div class="allDoctorBlock__item__imgWrap">
								<a
                                    class="allDoctorBlock__item__img"
                                    href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                                    style="background-image:url();"
                                >
                              <picture>
                                <source srcset="<?= WebPHelper($arItem["PICTURE"]["SRC"]) ?>" type="image/webp">
                                <img src="<?= $arItem["PICTURE"]["SRC"] ?>" alt="<?=$arItem["NAME"]?>">
                              </picture>
                              </a>
							</div>
							<a class="allDoctorBlock__item__name" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
								<span><?=$arItem["NAME"]?></span>
								<?echo $arItem["PROPERTIES"]["io_doctor"]["VALUE"]?>
							</a>
							<div class="allDoctorBlock__item__work"><?echo $arItem['PREVIEW_TEXT']?></div>
						</div>
					</div>
				<?endif?>
			<?endforeach?>
		<?endforeach?>
	</div>
	<div class="allDoctorBlock__controls">
		<div class="allDoctorBlock__control allDoctorBlock__prev">
		</div>
		<div class="allDoctorBlock__control allDoctorBlock__next">
		</div>
	</div>
</div>
