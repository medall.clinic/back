<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="homeMainScreen">
  <div class="homeMainScreen__image">
    <picture>
      <? if ($arResult["FIELDS"]["PREVIEW_PICTURE"]) { ?>
        <source srcset="<?= WebPHelper($arResult["FIELDS"]["PREVIEW_PICTURE"]["SRC"]) ?>" type="image/webp" media="(max-width: 600px)">
        <source srcset="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" type="image/jpeg" media="(max-width: 600px)">
      <? } ?>
        <source srcset="<?= WebPHelper($arResult["DETAIL_PICTURE"]["SRC"]) ?>" type="image/webp">
        <img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" alt="">
    </picture>
  </div>
	<div class="container">
		<div class="homeMainScreen__block">
			<div class="homeMainScreen__title title">
				<?/* <h1><?= $arResult["DETAIL_TEXT"] ?></h1> */?>
        <h1>Цифровая стоматология Medall</h1>
			</div>
		</div>
	</div>
</section>
