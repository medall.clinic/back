<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="footer__top">
	<div class="footer__menu"><div><?
		if(!function_exists("__DrawLevelMenuTreeT2")){
			function __DrawLevelMenuTreeT2($ar, $ind, &$f){
				$res = '';
				$l = count($ar);
				$arItem = $ar[$ind];
				if(!$f){$res.= '</div>';}
				if($arItem["IS_PARENT"]){
					$res.= '<div class="footer__menu__item">
								<a class="footer__menu__title" href="'.$arItem["LINK"].'">'.$arItem["TEXT"].'</a>';
					for($i=$ind+1; $i<$l; $i++){
						$item = $ar[$i];
						if($arItem["DEPTH_LEVEL"]>=$item["DEPTH_LEVEL"])
							break;
						if($arItem["DEPTH_LEVEL"]==$item["DEPTH_LEVEL"]-1){
							$res.= __DrawLevelMenuTreeT2($ar, $i, $f = true);
						}
					}
				}
				else{
					$res.= '<a class="footer__menu__link" href="'.$arItem["LINK"].'">'.$arItem["TEXT"].'</a>';
				}
				return $res;
			}
		}
		foreach($arResult as $ind=>$arItem){
			$g=0;
			if($arItem["DEPTH_LEVEL"]==1) echo __DrawLevelMenuTreeT2($arResult, $ind, $f = false);
		}?>
	</div>
</div>
</div>
<?endif?>
