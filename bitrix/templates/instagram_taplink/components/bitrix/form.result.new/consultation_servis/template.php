<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<div class="blockForm">
	<div class="container">
		<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" class="form__consultation form" data-wow-delay="0.7s">
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
			<?=bitrix_sessid_post()?>
			
			<div class="form__title title wow animated fadeInUp">Консультация
				<span>Оставьте запрос</span>
			</div>
			<div class="form__input__wrap wow animated fadeInUp" data-wow-delay="0.5s">
				<input
				name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["STRUCTURE"][0]["QUESTION_ID"]?>"
				type="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
				placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_441"]["CAPTION"]?>"
				required>
				<input
				name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["STRUCTURE"][0]["QUESTION_ID"]?>"
				type="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
				placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_352"]["CAPTION"]?>"
				required>
				<label class="form__label">
					<input type="checkbox"
					name="form_checkbox_SIMPLE_QUESTION_903[]"
					value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903"]["STRUCTURE"][0]["ID"]?>"
					id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903"]["STRUCTURE"][0]["ID"]?>"
					required>
					<span class="checkbox__box"></span>
					<span class="form__label__text"><?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_903"]["CAPTION"]?>
						<a href="#">с политикой конфеденциальности</a>
					</span>
				</label>
				<input class="button form__button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="Обратный звонок" />
			</div>
		</form>
	</div>
</div><?