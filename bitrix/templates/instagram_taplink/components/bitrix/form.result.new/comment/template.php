<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" class="popup__content-form" data-wow-delay="0.7s">
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
	<?=bitrix_sessid_post()?>
	<input type="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_150"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_150"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_150"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_150"]["CAPTION"]?>"
	required>
	<input class="phone" type="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_227"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_227"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_227"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_227"]["CAPTION"]?>"
	required>
	<input type="email"
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_369"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_369"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_369"]["CAPTION"]?>"
	required>
	<input type="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_833"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_833"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_833"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_833"]["CAPTION"]?>"
	required>
	<div class="popup__content-form-date">
		<input type="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_662"]["STRUCTURE"][0]["FIELD_TYPE"]?>"
		name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_662"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_662"]["STRUCTURE"][0]["ID"]?>"
		placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_662"]["CAPTION"]?>"
		required>
	</div>
	<textarea
	name="form_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_751"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_751"]["STRUCTURE"][0]["ID"]?>"
	placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_751"]["CAPTION"]?>"
	></textarea>
	<div class="popup__content-form-block">
    <label class="form__label popup__content-form-label">
      <input type="checkbox"
      name="form_checkbox_SIMPLE_QUESTION_903_CPfSL[]"
      value=""
      required>
      <span class="checkbox__box"></span>
      <span class="form__label__text">Нажимая на кнопку, я даю свое 
        <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.
      </span>
    </label>
		<label class="form__label popup__content-form-label">
			<input type="checkbox"
			name="form_checkbox_SIMPLE_QUESTION_416[]"
			value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_416"]["STRUCTURE"][0]["ID"]?>"
			id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_416"]["STRUCTURE"][0]["ID"]?>"
			required>
			<span class="checkbox__box"></span>
			<span class="form__label__text">Согласен<a href="#">с политикой конфеденциальности</a></span>
		</label>
		<div class="popup__content-form-block-button">
			<input class="button popup__content-form-button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
		</div>
	</div>
</form>
