<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?foreach($arResult["ROWS"] as $arItems):?>
	<?foreach($arItems as $arItem):?>
		<?if(is_array($arItem)):?>
			<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));?>
			<div class="ourDoctors__slider__item">
				<div class="ourDoctors__slider__block">
					<div class="ourDoctors__slider__block__left">
						<div class="ourDoctors__slider__img__overlay">
							<div class="ourDoctors__slider__img">
                <picture>
                  <source srcset="<?= WebPHelper($arItem["PICTURE"]["SRC"]) ?>" type="image/webp">
                  <img src="<?=$arItem["PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?> <?=$arItem['PROPERTIES']['io_doctor']['VALUE']?>">
                </picture>
              </div>
						</div>
					</div>
					<div class="ourDoctors__slider__block__right">
						<div class="ourDoctors__slider__title">Наши врачи</div>
						<div class="ourDoctors__slider__person beforeLine title">
							<p><?=$arItem["NAME"]?> <?=$arItem['PROPERTIES']['io_doctor']['VALUE']?></p>
						</div>
						<div class="ourDoctors__slider__desc">
							<p><?echo $arItem['PREVIEW_TEXT']?></p>
						</div>
						<div class="ourDoctors__slider__buttonsWrap">
							<a class="button ourDoctors__slider__button__allInfo" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Полная информация</a>
							<a class="buttonTwo ourDoctors__slider__button__resultWork" href="#">Результаты работ</a>
						</div>
						<div class="ourDoctors__slider__allDoctors">
							<a class="more" href="/doctor/">Все врачи направления</a>
						</div>
					</div>
				</div>
			</div>
		<?endif?>
	<?endforeach?>
<?endforeach?>
