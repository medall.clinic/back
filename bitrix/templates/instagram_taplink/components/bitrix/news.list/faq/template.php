<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem):?>
	<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
	<div class="faqPageBlock">
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="faqPageBlock__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<span class="faqPageBlock__hashTag">
			<?=$arItem["DISPLAY_PROPERTIES"]['direction_faq']['VALUE']?>
		</span>
		<div class="faqPageBlock__question">
			<p class="faqPageBlock__title">Вопрос</p>
			<p class="faqPageBlock__question__text">
				<?=$arItem["DISPLAY_PROPERTIES"]['question_faq']['DISPLAY_VALUE']?>
			</p>
		</div>
		<div class="faqPageBlock__answer">
			<?$arFilter = Array("ID"=>$arItem["DISPLAY_PROPERTIES"]['doctor_faq']['VALUE']);
			$res = CIBlockElement::GetList(Array(), $arFilter);
			if ($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields(); // поля элемента
				$arProps = $ob->GetProperties(); // свойства элемента?>
				<div class="faqPageBlock__answer__img">
					<div class="faqPageBlock__answer__imgBlock" style="background-image:url(<?=CFile::GetPath($arFields['PREVIEW_PICTURE'])?>);"></div>
				</div>
				<div class="faqPageBlock__answer__content">
					<p class="faqPageBlock__title">Отвечает</p><p class="faqPageBlock__answer__name"><?=$arFields['NAME']?> <?=$arProps['io_doctor']['VALUE']?></p>
					<p class="faqPageBlock__answer__desc"><?=$arFields['PREVIEW_TEXT']?></p>
					<div class="faqPageBlock__answer__text">
						<p><?=$arItem["DISPLAY_PROPERTIES"]['answer_faq']['DISPLAY_VALUE']?></p>
					</div>
				</div><?
			}?>
		</div>
	</div>
<?endforeach;?>
<a class="faqPage__button button question" href="#" onclick="return false">Задать свой вопрос</a>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="paginator faqPage__paginator"><?=$arResult["NAV_STRING"]?></div>
<?endif;?>
