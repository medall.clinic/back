<?
$filter = [];
$filter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

if($arParams["USER_PARAM_SECTION_LEFT_MARGIN"] && $arParams["USER_PARAM_SECTION_RIGHT_MARGIN"]){
	$filter["LEFT_MARGIN"] = $arParams["USER_PARAM_SECTION_LEFT_MARGIN"];
	$filter["RIGHT_MARGIN"] = $arParams["USER_PARAM_SECTION_RIGHT_MARGIN"];
}

// получаем разделы
$dbResSect = CIBlockSection::GetList(
   ["left_margin" => "ASC"],
	$filter
);

//Получаем разделы и собираем в массив
while($sectRes = $dbResSect->GetNext())
{
	$arSections[] = $sectRes;
}

//Собираем  массив из Разделов и элементов
$arElementGroups = [];
foreach($arSections as $arSection){  
	
	foreach($arResult["ITEMS"] as $key => $arItem){
		
		 if($arItem["IBLOCK_SECTION_ID"] == $arSection["ID"]){
			$arSection["ELEMENTS"][] = $arItem;
		 }
	}
	
	$arElementGroups[] = $arSection;
	
}

$arResult["ITEMS"] = $arElementGroups;
?>