<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$rez = array();
foreach($arResult["ITEMS"] as $arItem){$rez[$arItem["IBLOCK_SECTION_ID"]][] = $arItem;}
foreach($rez as $k => $arResult){
	$obSection = CIBlockSection::GetList(array(), array("ID"=>$k));
	while($arSection = $obSection->Fetch()){
		$URL = CFile::GetPath($arSection["PICTURE"]);
		$NAME = $arSection['NAME'];
	}?>
	<div class="menuCategory__item" style="background-image: url('<?=$URL?>')">
		<div class="menuCategory__item__title">
			<h2><?=$NAME?></h2>
		</div>
		<div class="menuCategory__item__submenu">
			<div class="menuCategory__item__submenu__wrap">
				<ul class="menuCategory__item__submenu__list">
					<?foreach($arResult as $arItem){
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
						<li class="menuCategory__item__submenu__item">
							<a class="menuCategory__item__submenu__link" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
						</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
<?}?>