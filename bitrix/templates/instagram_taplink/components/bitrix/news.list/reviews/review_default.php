<div class="reviewPage__block">

    <? if($arItem["PREVIEW_PICTURE"]): ?>
    <div class="reviewPage__block__left">
        <a class="reviewPage__block__linkImg" href="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" data-fancybox="reviews">
            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="Отзыв">
        </a>
    </div>
    <? endif ?>

    <div class="reviewPage__block__right">
        <div class="reviewPage__block__content">

            <p>Процедура: <span class="green"><?= $arItem["NAME"] ?></span></p>
            <p>Врач: <span class="green"><?= $arItem["DISPLAY_PROPERTIES"]["doctor_review"]["DISPLAY_VALUE"] ?></span></p>

            <div class="reviewPage__block__date">
                <p><?= $arItem["ACTIVE_FROM"] ?></p>
            </div>

            <div class="reviewPage__block__text">
                <p><?= $arItem["PREVIEW_TEXT"] ?></p>
            </div>

        </div>
    </div>

</div>