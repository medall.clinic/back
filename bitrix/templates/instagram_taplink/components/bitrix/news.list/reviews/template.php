<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? foreach($arResult["ITEMS"] as $arItem): ?>

    <? if($arItem["IBLOCK_SECTION_ID"] == 36): ?>
        <? include __DIR__."/review_star.php" ?>
    <? elseif($arItem["IBLOCK_SECTION_ID"] == 37): ?>
        <? include __DIR__."/review_video.php" ?>
    <? else: ?>
        <? include __DIR__."/review_default.php" ?>
    <? endif ?>

<? endforeach ?>