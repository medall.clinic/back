<div class="reviewPage__videoReview__block">
    <div class="reviewPage__videoReview__item">

        <? if($arItem["DISPLAY_PROPERTIES"]["load_video_review"]["FILE_VALUE"]["SRC"]): ?>
            <div class="reviewPage__videoReview__video">
             <video style="width:100%; height:100%;" src="<?= $arItem["DISPLAY_PROPERTIES"]["load_video_review"]["FILE_VALUE"]["SRC"] ?>" controls></video>
            </div>
        <? elseif($arItem["DISPLAY_PROPERTIES"]["video_review"]["DISPLAY_VALUE"]): ?>
            <div class="reviewPage__videoReview__video">
                <?= $arItem["DISPLAY_PROPERTIES"]["video_review"]["DISPLAY_VALUE"] ?>
            </div>
        <? else: ?>
            <div class="reviewPage__videoReview__video" style="background-image:url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>);"></div>
        <? endif ?>

        <div class="reviewPage__videoReview__desc">
            <p>Процедура: <?= $arItem["NAME"] ?></p>
            <p>Врач: <?= $arItem["DISPLAY_PROPERTIES"]["doctor_review"]["DISPLAY_VALUE"] ?></p>
        </div>

    </div>
</div>

