<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem){
	foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){
		if($arProperty['CODE']=='fix'){$arItem['FIX'] = 1;}
	}
	if( isset($arItem['FIX']) && !empty($arItem['FIX']) ){
		if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])){
			if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])){
				$print1.= '<div class="newsPage__mainNews__right" style="background-image:url('.$arItem["PREVIEW_PICTURE"]["SRC"].');">
					<div class="newsPage__mainNews__imgWrap">
						<div class="newsPage__mainNews__img" style="background-image:url('.$arItem["PREVIEW_PICTURE"]["SRC"].');"></div>
					</div>
				</div>';
			}
		}
		$print1.= '<div class="newsPage__mainNews__left">';
			$print1.= '<div class="newsPage__mainNews__wrapText">';
				$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
				if($ar_res = $res->GetNext()){
					$print1.= '<div class="newsPage__mainNews__subsection">'.$ar_res['NAME'].'</div>';
				}
				if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]){
					if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])){
						$print1.= '<a class="newsPage__mainNews__title title beforeLine" href="'.$arItem["DETAIL_PAGE_URL"].'">'.$arItem["NAME"].'</a>';
					}
					else{$print1.= '<b>'.$arItem["NAME"].'</b><br />';}
				}
				if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]){
					$print1.= '<div class="newsPage__mainNews__date">'.$arItem["DISPLAY_ACTIVE_FROM"].'</div>';
				}
				if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]){
					$print1.= '<div class="newsPage__mainNews__desc">'.$arItem["PREVIEW_TEXT"].'...</div>';
				}
				
				/*$print1.= '<form id="tags'.$arItem["ID"].'" method="get" action="">
					<input type="hidden" value="'.$arItem['TAGS'].'" name="TAGS">
				</form>';
				$print1.= '<a class="newsPage__hashTag" href=""
				onclick="document.getElementById(\'tags'.$arItem["ID"].'\').submit();return false;"># '.$arItem["TAGS"].'</a>';*/
				
				$print1.= '<a class="newsPage__block__hashTag" href="/search/index.php?tags='.$arItem["TAGS"].'" target=_blank># '.$arItem["TAGS"].'</a>';
				
				$print1.= '<a class="button newsPage__mainNews__button" href="'.$arItem["DETAIL_PAGE_URL"].'">Читать полностью</a>';
			$print1.= '</div>';
		$print1.= '</div>';
	}
	else{
		$print2.= '<div class="newsPage__block__item">';
			$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
			if($ar_res = $res->GetNext()){
				$print2.= '<div class="newsPage__block__subsection">'.$ar_res['NAME'].'</div>';
			}
			if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]){
				if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])){
					$print2.= '<a class="newsPage__block__title" href="'.$arItem["DETAIL_PAGE_URL"].'">'.$arItem["NAME"].'</a>';
				}
				else{$print2.= '<b>'.$arItem["NAME"].'</b><br />';}
			}
			if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]){
				$print2.= '<div class="newsPage__block__date">'.$arItem["DISPLAY_ACTIVE_FROM"].'</div>';
			}
			if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])){
				if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])){
					$print2.= '<a class="newsPage__block__img" style="background-image:url('.$arItem["PREVIEW_PICTURE"]["SRC"].');" href="'.$arItem["DETAIL_PAGE_URL"].'"></a>';
				}
			}
			$print2.= '<div class="newsPage__block__desc">';
				if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]){
					$print2.= '<div class="newsPage__block__desc__text">'.$arItem["PREVIEW_TEXT"].'</div>';
				}
				/*$print2.= '<form id="tags'.$arItem["ID"].'" method="get" action="">
					<input type="hidden" value="'.$arItem['TAGS'].'" name="TAGS">
				</form>';
				$print2.= '<a class="newsPage__block__hashTag" href=""
				onclick="document.getElementById(\'tags'.$arItem["ID"].'\').submit();return false;"># '.$arItem["TAGS"].'</a>';*/
				
				$print2.= '<a class="newsPage__block__hashTag" href="/search/index.php?tags='.$arItem["TAGS"].'" target=_blank># '.$arItem["TAGS"].'</a>';
				$print2.= '<div class="newsPage__block__more">
					<a class="more" href="'.$arItem["DETAIL_PAGE_URL"].'">Читать</a>
				</div>';
			$print2.= '</div>';
		$print2.= '</div>';
	}
}?>

<div class="newsPage">
	<div class="newsPage__mainNews">
		<?print $print1;?>
	</div>
	<div class="container">
		<div class="newsPage__block">
			<?print $print2;?>
		</div>
		<div class="paginator">
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<br /><?=$arResult["NAV_STRING"]?>
			<?endif;?>
		</div>
	</div>
</div>