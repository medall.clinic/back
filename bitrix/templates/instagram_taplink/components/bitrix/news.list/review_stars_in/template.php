<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
	<div class="reviewStarsPage__block">
		<div class="reviewPage__starsReview__block" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
			<div class="reviewPage__starsReview__text"><p><?=$arItem['NAME']?></p></div>
		</div>
		<div class="reviewPage__starsReview__content">
			<p>Процедура: <?echo $arItem["NAME"]?></p>
			<p>Врач:
				<span class="green"><?=$arItem["DISPLAY_PROPERTIES"]['doctor_review']['DISPLAY_VALUE']?></span>
			</p>
			<div class="reviewPage__starsReview__content-text"><p><?=$arItem['PREVIEW_TEXT']?></p></div>
		</div>
		<div class="reviewStarPage__buttonWrap">
			<a class="reviewStarPage__button button openPopup" href="#reviewStar<?=$arItem['ID']?>">Читать целиом</a>
		</div>
	</div>
	<div class="popupReviewStar" id="reviewStar<?=$arItem['ID']?>">
		<div class="popup__close">
			<div class="popup__close__line"></div>
			<div class="popup__close__line"></div>
		</div>
		<div class="popupReviewStar__block">
			<div class="popupReviewStar__block-title title">
				<h3><?=$arItem['NAME']?></h3>
			</div>
			<div class="popupReviewStar__block-doctor">
				<p>Процедура: <?echo $arItem["NAME"]?></p>
				<p>Врач:
					<span class="green"><?=$arItem["DISPLAY_PROPERTIES"]['doctor_review']['DISPLAY_VALUE']?></span>
				</p>
			</div>
			<div class="popupReviewStar__block-content">
				<p><?=$arItem['PREVIEW_TEXT']?></p>
				<img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="Картинка">
				<?=$arItem['DETAIL_TEXT']?>
				<?if($arItem["DISPLAY_PROPERTIES"]["load_video_review"]["FILE_VALUE"]["SRC"]){?>
					<div class="popupReviewStar__video">
						<video style="width:100%; height:100%;" src="<?=$arItem["DISPLAY_PROPERTIES"]["load_video_review"]["FILE_VALUE"]["SRC"]?>" controls></video>
					</div><?
				}
				else{?>
					<div class="popupReviewStar__video">
						<?=$arItem["DISPLAY_PROPERTIES"]["video_review"]["DISPLAY_VALUE"]?>
					</div><?
				}?>
		
				<button class="popupReviewStar__button button">Закрыть</button>
			</div>
		</div>
	</div><?
}?>