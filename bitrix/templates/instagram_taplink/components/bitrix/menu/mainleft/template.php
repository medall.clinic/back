<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<ul class="menuMain__list">
<?
if(!function_exists("__DrawLevelMenuTreeT")):
function __DrawLevelMenuTreeT($ar, $ind, &$f){
	$res = '';
	$l = count($ar);
	$arItem = $ar[$ind];
	if($arItem["DEPTH_LEVEL"]==2){
		$css = "__submenu";
		$css2 = "__two";
	}
	if($arItem["IS_PARENT"]){
		for($i=$ind+1; $i<$l; $i++){
			$item = $ar[$i];
			if($arItem["DEPTH_LEVEL"]>=$item["DEPTH_LEVEL"])
				break;
			if($arItem["DEPTH_LEVEL"]==$item["DEPTH_LEVEL"]-1)
				$res .= __DrawLevelMenuTreeT($ar, $i, $f);
		}
		$res = '<li class="menuMain'.$css.'__item">
			<a class="menuMain'.$css.'__link" href="'.$arItem["LINK"].'">'.$arItem["TEXT"].'</a>
			<ul class="menuMain__submenu'.$css2.'">'.$res.'</ul>';
	}
    elseif(!empty($arItem["PARAMS"]["IS_EXTERNAL"])){
		$res = '<li class="menuMain'.$css.'__item">';
		$res .= '<a class="menuMain'.$css.'__link" href="'.$arItem["LINK"].'">'.$arItem["TEXT"].'</a>';
	}
	else{
		$res .= '<li class="menuMain__submenu__two__item">
			<a class="menuMain__submenu__two__link" href="'.$arItem["LINK"].'">'.$arItem["TEXT"].'</a>';
	}
	return $res;
}
endif;
foreach($arResult as $ind=>$arItem)
	if($arItem["DEPTH_LEVEL"]==1) echo __DrawLevelMenuTreeT($arResult, $ind, $f = false);
?>
</ul>
<?endif?>