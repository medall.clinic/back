<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<ul class="menu__list">
<?foreach($arResult as $arItem):?>
	<?if($arItem["SELECTED"]):?>
		<li class="menu__item active">
			<a class="menu__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		</li>
		<!--<li class="selected"><b class="r1"></b><b class="r0"></b><a href="<?/*=$arItem["LINK"]?>"><?=$arItem["TEXT"]*/?></a><b class="r0"></b><b class="r1"></b></li>-->
	<?else:?>
		<li class="menu__item">
			<a class="menu__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		</li>
		<!--<li><b class="r1"></b><b class="r0"></b><a href="<?/*=$arItem["LINK"]?>"><?=$arItem["TEXT"]*/?></a><b class="r0"></b><b class="r1"></b></li>-->
	<?endif?>
<?endforeach?>
</ul>		
<?endif?>
