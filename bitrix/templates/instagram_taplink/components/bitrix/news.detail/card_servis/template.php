<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Главный экран-->
<div class="breastEnlargementMainScreen">
	<div class="breastEnlargementMainScreen__slider">
		<?foreach($arResult["DISPLAY_PROPERTIES"]['slider_card']['VALUE'] as $k=>$v){
			$arFilter = Array("ID"=>$v);
			$res = CIBlockElement::GetList(Array(), $arFilter);
			if ($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields(); // поля элемента
				$arProps = $ob->GetProperties(); // свойства элемента 
				?><div class="breastEnlargementMainScreen__slider__block">
					<div class="breastEnlargementMainScreen__slider__item" style="background-image: url(<?=CFile::GetPath($arProps['image2_service_card']['VALUE'])?>);">
						<div class="breastEnlargementMainScreen__slider__item__right">
							<div class="breastEnlargementMainScreen__slider__item__img" style="background-image:url(<?=CFile::GetPath($arProps['image_service_card']['VALUE'])?>);"></div>
							<!--div class="breastEnlargementMainScreen__slider__item__img"><img src="<?=CFile::GetPath($arProps['image_service_card']['VALUE'])?>"/></div-->
						</div>
						<div class="breastEnlargementMainScreen__slider__item__left">
							<div class="breastEnlargementMainScreen__slider__item__title title">
								<h2><?=$arFields['NAME']?></h2>
							</div>
							<div class="breastEnlargementMainScreen__slider__item__desc">
								<p><?=$arProps['subtitle_service_card']['VALUE']?></p>
							</div>
							<div class="breastEnlargementMainScreen__slider__item__link">
								<a class="buttonTwo" href="<?=$arProps['link_service_card']['VALUE']?>">Подробнее</a>
							</div>
						</div>
					</div>
				</div><?
			}
		}?>
	</div>
	<div class="breastEnlargementMainScreen__controls">
		<div class="breastEnlargementMainScreen__control breastEnlargementMainScreen__prev"></div>
		<div class="breastEnlargementMainScreen__control breastEnlargementMainScreen__next"></div>
	</div>
</div>

<!-- Блок с ссылками-->
<? /*
triple_animation_title
triple_animation_img
triple_animation_link
triple_video_title
triple_video_img
triple_video_link
triple_gallery_title
triple_gallery_img
triple_gallery_link

*/ ?>
<div class="linksBlock">
	<? if(!empty($arResult["DISPLAY_PROPERTIES"]['triple_animation_img']) && !empty($arResult["DISPLAY_PROPERTIES"]['triple_animation_link'])): ?>
	<a class="linksBlock__item popup-youtube" href="https://www.youtube.com/embed/<?=$arResult["DISPLAY_PROPERTIES"]['triple_animation_link']['VALUE']?>?controls=0">
		<span class="linksBlock__item__icon" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]['triple_animation_img']['FILE_VALUE']['SRC']?>);"></span>
		<span class="linksBlock__item__title">
			<span><?=(!empty($arResult["DISPLAY_PROPERTIES"]['triple_animation_title']['VALUE']))?$arResult["DISPLAY_PROPERTIES"]['triple_animation_title']['VALUE']:'Анимация процедуры'?></span>
		</span>
		<!--span class="linksBlock__item__desc">
			<span><? //$arProps['subtitle_tiles']['VALUE']?></span>
		</span-->
	</a>
	<? endif; ?>

	<? if(!empty($arResult["DISPLAY_PROPERTIES"]['triple_video_img']) && !empty($arResult["DISPLAY_PROPERTIES"]['triple_video_link'])): ?>
	<a class="linksBlock__item popup-youtube" href="https://www.youtube.com/embed/<?=$arResult["DISPLAY_PROPERTIES"]['triple_video_link']['VALUE']?>?controls=0">
		<span class="linksBlock__item__icon" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]['triple_video_img']['FILE_VALUE']['SRC']?>);"></span>
		<span class="linksBlock__item__title">
			<span><?=(!empty($arResult["DISPLAY_PROPERTIES"]['triple_video_title']['VALUE']))?$arResult["DISPLAY_PROPERTIES"]['triple_video_title']['VALUE']:'Видео процедуры'?></span>
		</span>
		<!--span class="linksBlock__item__desc">
			<span><? //$arProps['subtitle_tiles']['VALUE']?></span>
		</span-->
	</a>
	<? endif; ?>

	<? if(!empty($arResult["DISPLAY_PROPERTIES"]['triple_gallery_img']) && !empty($arResult["DISPLAY_PROPERTIES"]['triple_gallery_link'])): ?>
	<a class="linksBlock__item" href="<?=$arResult["DISPLAY_PROPERTIES"]['triple_gallery_link']['VALUE']?>">
		<span class="linksBlock__item__icon" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]['triple_gallery_img']['FILE_VALUE']['SRC']?>);"></span>
		<span class="linksBlock__item__title">
			<span><?=(!empty($arResult["DISPLAY_PROPERTIES"]['triple_gallery_title']['VALUE']))?$arResult["DISPLAY_PROPERTIES"]['triple_gallery_title']['VALUE']:'Фотогалерея'?></span>
		</span>
		<!--span class="linksBlock__item__desc">
			<span><? //$arProps['subtitle_tiles']['VALUE']?></span>
		</span-->
	</a>
	<? endif; ?>
</div>
<? /*
<!-- Блок с ссылками-->
<div class="linksBlock">
	<?foreach($arResult["DISPLAY_PROPERTIES"]['tiles_service_card']['VALUE'] as $k=>$v){
		$arFilter = Array("ID"=>$v);
		$res = CIBlockElement::GetList(Array(), $arFilter);
		if ($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields(); // поля элемента
			//print_r($arFields);
			$arProps = $ob->GetProperties(); // свойства элемента 
			//print_r($arProps);
			?><a class="linksBlock__item" href="<?=$arProps['link_tiles']['VALUE']?>">
				<span class="linksBlock__item__icon" style="background-image: url(<?=CFile::GetPath($arFields['PREVIEW_PICTURE'])?>);"></span>
				<span class="linksBlock__item__title">
					<span><?=$arFields['NAME']?></span>
				</span>
				<span class="linksBlock__item__desc">
					<span><?=$arProps['subtitle_tiles']['VALUE']?></span>
				</span>
			</a><?
		}
	}?>
</div>
*/ ?>
<!-- Основной контент-->
<div class="mainContent">
	<section class="mainContent__left">
		<div class="mainContent__wrap">
			<div class="breadCrumbs">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
					"PATH" => "",
					"SITE_ID" => "s1",
					"START_FROM" => "0"
				));?>
			</div>
			<div class="mainContent__title title">
				<h1><?=$arResult["NAME"]?></h1>
			</div>
			<div class="mainContent__title__desc">
				<p><?=$arResult["DISPLAY_PROPERTIES"]['subtitle_card']['VALUE']?></p>
			</div>
			<div class="mainContent__text">
				<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" title="<?=$arResult["NAME"]?>">
				<?echo $arResult["PREVIEW_TEXT"];?>
				<?if($arResult["DISPLAY_PROPERTIES"]['photogallery_subtitle_card']['VALUE']!=''){?>
					<div class="fotoalbumLink">
						<div class="fotoalbumLink__right" style="background-image: url(<?=$arResult["DISPLAY_PROPERTIES"]['photogallery_img_card']['FILE_VALUE']['SRC']?>);"></div>
						<div class="fotoalbumLink__left">
							<div class="fotoalbumLink__title title beforeLine">Фотогалерея</div>
							<div class="fotoalbumLink__desc">
								<p><?=$arResult["DISPLAY_PROPERTIES"]['photogallery_subtitle_card']['VALUE']?></p>
							</div>
							<a class="more" href="<?=$arResult["DISPLAY_PROPERTIES"]['photogallery_link_card']['VALUE']?>">Перейти в галерею</a>
						</div>
					</div>
				<?}?>
				
				<?if($arResult["DISPLAY_PROPERTIES"]['d_subtitle_card']['VALUE']!=''){?>
					<div class="visualization__article" style="background: url(<?=$arResult["DISPLAY_PROPERTIES"]['d_link_img']['FILE_VALUE']['SRC']?>) no-repeat center center;">
						<div class="visualization__article__title title"><?=$arResult["DISPLAY_PROPERTIES"]['d_header_card']['VALUE']?></div>
						<div class="visualization__article__desc"><?=$arResult["DISPLAY_PROPERTIES"]['d_subtitle_card']['VALUE']?></div>
						<div class="visualization__article__text">Мы даем Вам возможность взглянуть на будующий результат с помощью 3д моделирования</div>
						<a class="visualization__article__more buttonTwo" href="<?=$arResult["DISPLAY_PROPERTIES"]['d_link_card']['VALUE']?>">Узнать подробнее</a>
					</div>
				<?}?>
				<div class="mainContent__text__currentArticles__block">
					<?$j=0;
					foreach($arResult["DISPLAY_PROPERTIES"]['articles_card']['LINK_ELEMENT_VALUE'] as $k => $v){
						$j++;
						$arFilter = Array("ID"=>$v['ID']);
						$res = CIBlockElement::GetList(Array(), $arFilter);
						if ($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields(); // поля элемента
							$arProps = $ob->GetProperties(); // свойства элемента
							/*if($j==1){
								?><div class="mainContent__text__article">
									<div class="mainContent__text__article__top" style="background-image: url(<?=CFile::GetPath($v['PREVIEW_PICTURE'])?>);">
										<div class="mainContent__text__article__title title beforeLine"><?=$v['NAME']?></div>
										<?if($arProps['current_articles']['VALUE']!=''){
											?><div class="mainContent__text__article__desc">Актульные статьи</div><?
										}?>
									</div>
									<div class="mainContent__text__article__bottom">
										<div class="mainContent__text__article__text">
											<?=$arFields['PREVIEW_TEXT']?>
											<a class="buttonTwo" href="<?=$v['DETAIL_PAGE_URL']?>">Читать целиком</a>
										</div>
									</div>
								</div><?
							} else{ */ ?>
								<div class="mainContent__text__currentArticles__item">
									<a class="mainContent__text__currentArticles__item__img" href="<?=$v['DETAIL_PAGE_URL']?>" style="background-image: url(<?=CFile::GetPath($v['PREVIEW_PICTURE'])?>);"></a>
									<div class="mainContent__text__currentArticles__item__desc">
										<?if($arProps['current_articles']['VALUE']!=''){
											?><p class="mainContent__text__currentArticles__item__desc--p">Актуальные статьи</p><?
										}?>
										<a class="mainContent__text__currentArticles__item__desc__title mainContent__text__title" href="<?=$v['DETAIL_PAGE_URL']?>"><?=$v['NAME']?></a>
										<a class="mainContent__text__currentArticles__item__desc__link more" href="<?=$v['DETAIL_PAGE_URL']?>">Читать</a>
									</div>
								</div><?
							//}
						}
					}?>
				</div>
				<div class="mainContent__text__helpfullInfo">
					<div class="mainContent__text__helpfullInfo__img">
						<img src="<?=$arResult["DISPLAY_PROPERTIES"]['img_addtext1_card']['FILE_VALUE']['SRC']?>">
					</div>
					<div class="mainContent__text__helpfullInfo__content">
						<div class="mainContent__text__title mainContent__text__helpfullInfo__title">
							<h2><?=$arResult["DISPLAY_PROPERTIES"]['title_addtext1_card']['VALUE']?></h2>
						</div>
						<div class="mainContent__text__helpfullInfo__text">
							<?=$arResult["DISPLAY_PROPERTIES"]['addtext1_card']['~VALUE']['TEXT']?>
						</div>
					</div>
				</div>
				<div class="mainContent__text__helpfullInfo mainContent__text__helpfullInfo--reverse">
					<div class="mainContent__text__helpfullInfo__img">
						<img src="<?=$arResult["DISPLAY_PROPERTIES"]['img_addtext2_card']['FILE_VALUE']['SRC']?>">
					</div>
					<div class="mainContent__text__helpfullInfo__content">
						<div class="mainContent__text__title mainContent__text__helpfullInfo__title">
							<h2><?=$arResult["DISPLAY_PROPERTIES"]['title_addtext2_card']['VALUE']?></h2>
						</div>
						<div class="mainContent__text__helpfullInfo__text">
							<?=$arResult["DISPLAY_PROPERTIES"]['addtext2_card']['~VALUE']['TEXT']?>
						</div>
					</div>
				</div>
				<div class="mainContent__text__helpfullInfo">
					<div class="mainContent__text__helpfullInfo__img">
						<img src="<?=$arResult["DISPLAY_PROPERTIES"]['img_addtext3_card']['FILE_VALUE']['SRC']?>">
					</div>
					<div class="mainContent__text__helpfullInfo__content">
						<div class="mainContent__text__title mainContent__text__helpfullInfo__title">
							<h2><?=$arResult["DISPLAY_PROPERTIES"]['title_addtext3_card']['VALUE']?></h2>
						</div>
						<div class="mainContent__text__helpfullInfo__text">
							<?=$arResult["DISPLAY_PROPERTIES"]['addtext3_card']['~VALUE']['TEXT']?>
						</div>
					</div>
				</div>
				
				<?echo $arResult["DETAIL_TEXT"];?>
				
			</div>
		</div>

		<div class="block_installment">
			<div class="mainContent__wrap">
				<div class="wrapper">
					<div class="icon"><img src="/bitrix/templates/corp_services_gray2_new/img/mess/dentistry/discount.png" alt=""></div>
					<div class="title">Мы предлагаем рассрочку</div>
					<div class="text">Лечение кариеса – это довольно длительная и сложная процедура. Способы и методы лечения зависят от степени запущенности заболевания</div>
					<div class="btn-wrapper">
						<a href="#" class="buttonTwo">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<aside class="mainContent__aside">
		<div class="mainContent__aside__wrap">
			<!-- Текст блок 1 -->
			<section class="mainContent__ArticleBlock">
				<!--div class="mainContent__ArticleBlock__title mainContent__text__title"><?=$arResult["DISPLAY_PROPERTIES"]['text_box1_card']['VALUE']?></div-->
				<ul class="mainContent__ArticleBlock__list">
					<?foreach($arResult["DISPLAY_PROPERTIES"]['articles_box1_card']['LINK_ELEMENT_VALUE'] as $k => $v){?>
						<li class="mainContent__ArticleBlock__list__item">
							<a class="mainContent__ArticleBlock__list__link" href="<?=$v['DETAIL_PAGE_URL']?>"><?=$v['NAME']?></a>
						</li><?
					}?>
				</ul>
			</section>
			<!-- Текст блок 5 -->
			<section class="mainContent__ArticleBlock">
				<? /* <div class="mainContent__ArticleBlock__title mainContent__text__title"><?=$arResult["DISPLAY_PROPERTIES"]['text_box5_card']['VALUE']?></div> */ ?>
				<ul class="mainContent__ArticleBlock__list">
					<?foreach($arResult["DISPLAY_PROPERTIES"]['articles_box5_card']['LINK_ELEMENT_VALUE'] as $k => $v){?>
						<li class="mainContent__ArticleBlock__list__item">
							<a class="mainContent__ArticleBlock__list__link" href="<?=$v['DETAIL_PAGE_URL']?>"><?=$v['NAME']?></a>
						</li><?
					}?>
				</ul>
			</section>
			<!-- Текст блок 2 -->
			<section class="mainContent__ArticleBlock">
				<div class="mainContent__ArticleBlock__title mainContent__text__title"><?=$arResult["DISPLAY_PROPERTIES"]['text_box2_card']['VALUE']?></div>
				<ul class="mainContent__ArticleBlock__list var-1">
					<?foreach($arResult["DISPLAY_PROPERTIES"]['articles_box2_card']['LINK_ELEMENT_VALUE'] as $k => $v){?>
						<?
						$res = CIBlockElement::GetByID($v['ID']);
						if($ar_res = $res->GetNext())
						?>
						<li class="mainContent__ArticleBlock__list__item">
							<a class="mainContent__ArticleBlock__list__link" href="<?=$v['DETAIL_PAGE_URL']?>">
								<div class="card">
									<div class="image" style="background-image: url(<?=CFile::GetPath($ar_res['PREVIEW_PICTURE'])?>)"><!--<img src="<?=CFile::GetPath($ar_res["PREVIEW_PICTURE"])?>" alt="<?=$v['NAME']?>" title="<?=$v['NAME']?>"/>--></div>
									<div class="content">
										<div class="title"><?=$v['NAME']?></div>
										<div class="text"><?=$ar_res['PREVIEW_TEXT']?></div>
									</div>
								</div>
							</a>
						</li><?
					}?>
				</ul>
			</section>
			<!-- Текст блок 3 -->
			<section class="mainContent__ArticleBlock">
				<div class="mainContent__ArticleBlock__title mainContent__text__title"><?=$arResult["DISPLAY_PROPERTIES"]['text_box3_card']['VALUE']?></div>
				<ul class="mainContent__ArticleBlock__list var-2">
					<?foreach($arResult["DISPLAY_PROPERTIES"]['articles_box3_card']['LINK_ELEMENT_VALUE'] as $k => $v){?>
						<?
						$res = CIBlockElement::GetByID($v['ID']);
						if($ar_res = $res->GetNext())
						?>
						<li class="mainContent__ArticleBlock__list__item">
							<div class="card">
								<div class="image"><img src="<?=CFile::GetPath($ar_res["PREVIEW_PICTURE"])?>" alt="<?=$v['NAME']?>" title="<?=$v['NAME']?>"/></div>
								<div class="content">
									<div class="title"><?=$v['NAME']?></div>
									<div class="text"><?=$ar_res['PREVIEW_TEXT']?></div>
									<a class="more" href="<?=$v['DETAIL_PAGE_URL']?>">Читать</a>
								</div>
							</div>
						</li><?
					}?>
				</ul>
			</section>
			<? /*
			<!-- Врачи направления -->
			<section class="mainContent__ArticleBlock mainContent__ArticleBlockDoktors">
				<div class="mainContent__ArticleBlock__title mainContent__text__title">Доктора направления</div>
				<div class="mainContent__ArticleBlock__doctor__sliderWrap">
					<div class="mainContent__ArticleBlock__doctor__slider">
						<?$j=0;
						foreach($arResult["DISPLAY_PROPERTIES"]['doctor_directions']['VALUE'] as $k => $v){
							$arFilter = Array("ID"=>$v);
							$res = CIBlockElement::GetList(Array(), $arFilter);
							if ($ob = $res->GetNextElement()){
								$arFields = $ob->GetFields(); // поля элемента
								$arProps = $ob->GetProperties(); // свойства элемента
								$j++;
								if($j==1){?><div class="mainContent__ArticleBlock__doctor__slider__item"><?}?>
								<div class="mainContent__ArticleBlock__doctor">
									<div class="mainContent__ArticleBlock__doctor__img">
										<img src="<?=CFile::GetPath($arFields['PREVIEW_PICTURE'])?>" alt="картинка">
									</div>
									<div class="mainContent__ArticleBlock__doctor__content">
										<div class="mainContent__ArticleBlock__doctor__name">
											<p><span><?=$arFields['NAME']?></span><?=$arProps['io_doctor']['VALUE']?></p>
										</div>
										<div class="mainContent__ArticleBlock__doctor__desc">
											<p><?=$arFields['PREVIEW_TEXT']?></p>
										</div>
									</div>
								</div>
								<?if($j==3){
									?></div><?
									$j=0;
								}
							}
						}?>
					</div>
					<div class="mainContent__ArticleBlock__doctor__sliderContols">
						<div class="mainContent__ArticleBlock__doctor__control mainContent__doctor__prev"></div>
						<div class="mainContent__ArticleBlock__doctor__control mainContent__doctor__next"></div>
					</div>
				</div>
			</section>
			<!-- Промо блок -->
			<div class="mainContent__ArticleBlock__banner" style="background-image:url(<?=$arResult["DISPLAY_PROPERTIES"]['promo_img_card']['FILE_VALUE']['SRC']?>);">
				<div class="mainContent__ArticleBlock__banner__title"><?=$arResult["DISPLAY_PROPERTIES"]['promo_header_card']['VALUE']?></div>
				<a class="more mainContent__ArticleBlock__banner__link" href="<?=$arResult["DISPLAY_PROPERTIES"]['promo_link_card']['VALUE']?>">Узнать стоимость</a>
			</div>
			*/ ?>
			<!-- До и после -->
			<?if(!empty($arResult["DISPLAY_PROPERTIES"]['before_and_after']['VALUE'])){?>
				<section class="mainContent__ArticleBlock mainContent__ArticleBlock__beforeAfter">
					<div class="mainContent__ArticleBlock__title mainContent__text__title">До и после</div>
					<?foreach($arResult["DISPLAY_PROPERTIES"]['before_and_after']['VALUE'] as $k => $v){
						$arFilter = Array("ID"=>$v);
						$res = CIBlockElement::GetList(Array(), $arFilter);
						if ($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields(); // поля элемента
							$arProps = $ob->GetProperties(); // свойства элемента
							?><div class="mainContent__ArticleBlock__beforeImg">
								<img src="<?=CFile::GetPath($arProps["fas_before_after"]["VALUE"])?>">
							</div><?
						}
					}?>
					<a class="more" href="/before_after/">Перейти в галерею</a>
				</section>
			<?}?>
			<!-- Новости -->
			<section class="mainContent__ArticleBlock mainContent__newsBlock">
				<div class="mainContent__ArticleBlock__title mainContent__text__title">Новости</div>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "news_card", Array(
					"ACTIVE_DATE_FORMAT" => "d/m/y",	// Формат показа даты
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "N",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_DATE" => "Y",	// Выводить дату элемента
						"DISPLAY_NAME" => "Y",	// Выводить название элемента
						"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"FIELD_CODE" => array(	// Поля
							0 => "TAGS",
							1 => "",
						),
						"FILTER_NAME" => "arFilter",	// Фильтр
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
						"IBLOCK_ID" => "4",	// Код информационного блока
						"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"NEWS_COUNT" => "2",	// Количество новостей на странице
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Новости",	// Название категорий
						"PARENT_SECTION" => "",	// ID раздела
						"PARENT_SECTION_CODE" => "",	// Код раздела
						"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
						"PROPERTY_CODE" => array(	// Свойства
							0 => "fix",
							1 => "direction",
							2 => "",
						),
						"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SORT_BY1" => "RAND",	// Поле для первой сортировки новостей
						"SORT_BY2" => "RAND",	// Поле для второй сортировки новостей
						"SORT_ORDER1" => "RAND",	// Направление для первой сортировки новостей
						"SORT_ORDER2" => "RAND",	// Направление для второй сортировки новостей
						"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
						"COMPONENT_TEMPLATE" => "other_news"
					),
					false
				);?>
			</section>
			<!-- Текст блок 4 -->
			<section class="mainContent__ArticleBlock">
				<div class="mainContent__ArticleBlock__title mainContent__text__title"><?=$arResult["DISPLAY_PROPERTIES"]['text_box4_card']['VALUE']?></div>
				<ul class="mainContent__ArticleBlock__list">
					<?foreach($arResult["DISPLAY_PROPERTIES"]['articles_box4_card']['LINK_ELEMENT_VALUE'] as $k => $v){?>
						<li class="mainContent__ArticleBlock__list__item">
							<a class="mainContent__ArticleBlock__list__link" href="<?=$v['DETAIL_PAGE_URL']?>"><?=$v['NAME']?></a>
						</li><?
					}?>
				</ul>
			</section>
			<!-- Скидки -->
			<div class="mainContent__aside__discount">
				<div class="discountBreastPlastic__wrap">
					<div class="aside__discountBreastPlastic__slider">
						<?foreach($arResult["DISPLAY_PROPERTIES"]['sale_card']['LINK_ELEMENT_VALUE'] as $k => $v){
							$arFilter = Array("ID"=>$v['ID']);
							$res = CIBlockElement::GetList(Array(), $arFilter);
							if ($ob = $res->GetNextElement()){
								$arFields = $ob->GetFields(); // поля элемента
								$arProps = $ob->GetProperties(); // свойства элемента?>
								<div class="discountBreastPlastic__item">
									<div class="discountBreastPlastic__item__bgImg" style="background-image: url(img/bg/discountOnBreast.jpg)"></div>
									<div class="container">
										<div class="discountBreastPlastic__block">
											<div class="discountBreastPlastic__right">
												<div class="discountBreastPlastic__img">
													<img src="<?=CFile::GetPath($arProps['img_main_action']['VALUE'])?>" alt="картинка">
												</div>
											</div>
											<div class="discountBreastPlastic__left">
												<div class="discountBreastPlastic__title title beforeLine">
													<p><?=$v['NAME']?></p>
												</div>
												<div class="discountBreastPlastic__month">
													<p><?=$arFields['DATE_ACTIVE_FROM']?> - <?=$arFields['DATE_ACTIVE_TO']?></p>
												</div>
												<a class="discountBreastPlastic__button button" href="<?=$v['DETAIL_PAGE_URL']?>">Узнать подробнее</a>
												<div class="discountBreastPlastic__allOffer">
													<a class="more" href="/stock">Все спецпредложения</a>
												</div>
											</div>
										</div>
									</div>
								</div><?
							}
						}?>
					</div>
					<div class="discountBreastPlastic__controlsWrap">
						<div class="discountBreastPlastic__control discountBreastPlastic__prev aside"></div>
						<div class="discountBreastPlastic__control discountBreastPlastic__next aside"></div>
					</div>
				</div>
			</div>
		</div>
	</aside>
</div>
<!-- Слайдер скидок для планшетной и мобильной версии-->
<section class="discountBreastPlastic mainContent__aside__discount__tablets">
	<div class="discountBreastPlastic__wrap">
		<div class="discountBreastPlastic__slider">
			<?foreach($arResult["DISPLAY_PROPERTIES"]['sale_card']['LINK_ELEMENT_VALUE'] as $k => $v){
				$arFilter = Array("ID"=>$v['ID']);
				$res = CIBlockElement::GetList(Array(), $arFilter);
				if ($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields(); // поля элемента
					$arProps = $ob->GetProperties(); // свойства элемента?>
					<div class="discountBreastPlastic__item">
						<div class="discountBreastPlastic__item__bgImg" style="background-image: url(<?=CFile::GetPath($arProps['img_bg_action']['VALUE'])?>)"></div>
						<div class="container">
							<div class="discountBreastPlastic__block">
								<div class="discountBreastPlastic__right">
									<div class="discountBreastPlastic__img">
										<img src="<?=CFile::GetPath($arProps['img_main_action']['VALUE'])?>" alt="картинка">
									</div>
								</div>
								<div class="discountBreastPlastic__left">
									<div class="discountBreastPlastic__title title beforeLine">
										<p><?=$v['NAME']?></p>
									</div>
									<div class="discountBreastPlastic__month">
										<p><?=$arFields['DATE_ACTIVE_FROM']?> - <?=$arFields['DATE_ACTIVE_TO']?></p>
									</div>
									<a class="discountBreastPlastic__button button" href="<?=$v['DETAIL_PAGE_URL']?>">Узнать подробнее</a>
									<div class="discountBreastPlastic__allOffer">
										<a class="more" href="/stock">Все спецпредложения</a>
									</div>
								</div>
							</div>
						</div>
					</div><?
				}
			}?>
		</div>
		<div class="discountBreastPlastic__controlsWrap">
			<div class="discountBreastPlastic__control discountBreastPlastic__prev">
			</div>
			<div class="discountBreastPlastic__control discountBreastPlastic__next">
			</div>
		</div>
	</div>
</section>



<!-- Отзывы-->
<section class="reviews">
	<div class="container">
		<div class="reviews__desc wow animated fadeInLeft"><p>Мы гордимся результатами</p></div>
		<div class="reviews__title title beforeLine wow animated fadeInRight" data-wow-delay="0.5s">
			<h2>Что говорят наши пациенты</h2>
		</div>
		<div class="reviews__slogan wow animated fadeInLeft" data-wow-delay="0.8s">
			<p>Положительные отзывы наших пациентов - доказательство нашей успешной работы</p>
		</div>
	</div>
	<div class="reviews__slider__wrap wow animated fadeInRight" data-wow-delay="0.8s">
		<div class="reviews__slider__block">
			<div class="reviews__slider">
				<?foreach($arResult["DISPLAY_PROPERTIES"]['reviews']['VALUE'] as $k=>$v){
					$arFilter = Array("ID"=>$v);
					$res = CIBlockElement::GetList(Array(), $arFilter);
					if ($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields(); // поля элемента
						//print_r($arFields);
						$arProps = $ob->GetProperties(); // свойства элемента 
						//print_r($arProps);
						$res = CIBlockElement::GetByID($arProps['doctor_review']['VALUE']);
						if($ar_res = $res->GetNext()) $arProps['doctor_review'] = $ar_res;
						//print_r($arProps);
						?>
						<div class="reviews__slider__item">
							<div class="reviews__slider__item__img">
								<a class="reviews__slider__item__img__link" href="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" data-fancybox="gallery"> <img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" alt="Отзыв" title="Отзыв"> </a>
							</div>
							<div class="reviews__slider__item__doctor"><p>Врач: <a href="<?=$arProps['doctor_review']['DETAIL_PAGE_URL']?>"><?=$arProps['doctor_review']['NAME']?></a></p></div>
							<div class="reviews__slider__item__text"><p><?echo $arFields["PREVIEW_TEXT"];?></p></div>
						</div>
						<?
					}
				}?>
			</div>
		</div>
		<div class="reviews__slider__controls__block">
			<div class="reviews__slider__control reviews__slider__control__prev"></div>
			<div class="reviews__slider__control reviews__slider__control__next"></div>
		</div>
	</div>
	<div class="container">
		<div class="reviews__link wow animated fadeInLeft" data-wow-delay="0.8s">
			<a class="buttonTwo" href="/review">Все отзывы</a>
		</div>
	</div>
</section>



<? /*
<!-- Отзывы-->
<section class="reviews">
	<div class="container">
		<div class="reviews__desc wow animated fadeInLeft"><p>Мы гордимся результатами</p></div>
		<div class="reviews__title title beforeLine wow animated fadeInRight" data-wow-delay="0.5s">
			<h2>Что говорят наши пациенты</h2>
		</div>
		<div class="reviews__slogan wow animated fadeInLeft" data-wow-delay="0.8s">
			<p>Положительные отзывы наших пациентов - доказательство нашей успешной работы</p>
		</div>
	</div>
	<div class="reviews__slider__wrap wow animated fadeInRight" data-wow-delay="0.8s">
		<div class="reviews__slider__block">
			<div class="reviews__slider">
				 <?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"review_mini",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "N",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "Y",
						"COMPONENT_TEMPLATE" => "review_mini",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array(0=>"",1=>"",),
						"FILTER_NAME" => "",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "17",
						"IBLOCK_TYPE" => "review",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "5",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "",
						"PARENT_SECTION" => "39",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array(0=>"",1=>"doctor_review",),
						"SET_BROWSER_TITLE" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"SORT_BY1" => "ACTIVE_FROM",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "DESC",
						"SORT_ORDER2" => "ASC",
						"STRICT_SECTION_CHECK" => "N"
					)
				);?>
			</div>
		</div>
		<div class="reviews__slider__controls__block">
			<div class="reviews__slider__control reviews__slider__control__prev"></div>
			<div class="reviews__slider__control reviews__slider__control__next"></div>
		</div>
	</div>
	<div class="container">
		<div class="reviews__link wow animated fadeInLeft" data-wow-delay="0.8s">
			<a class="buttonTwo" href="/review">Все отзывы</a>
		</div>
	</div>
</section>
*/ ?>

<? /*
<!-- Мнение о клинике-->
<section class="weOfferLowPayment opinionAboutClinic">
	<div class="container">
		<div class="weOfferLowPayment__icon wow animated fadeInRight" data-wow-delay="0.3s"></div>
		<div class="opinionAboutClinic__title title wow animated fadeInLeft" data-wow-delay="0.5s">
			<h2>У вас есть свое мнение о нашей клинике?</h2>
		</div>
		<div class="weOfferLowPayment__desc wow animated fadeInRight" data-wow-delay="0.7s">
			<p>Оставьте свой отзыв. Нам очень важно что Вы думаете о нашей работе.</p>
		</div>
		<a class="weOfferLowPayment__link buttonTwo wow animated fadeIn comment" href="#" onclick="return false" data-wow-delay="1s">Оставить отзыв</a>
	</div>
</section>
*/ ?>
<!-- Блок с вопросом-->

<?if(!empty($arResult["DISPLAY_PROPERTIES"]['faq_card']['LINK_ELEMENT_VALUE'])){?>
	<div class="askYourQuestion">
		<div class="container"><div class="askYourQuestion__desc"><p>Популярные вопросы</p></div></div>
		<div class="askYourQuestion__sliderWrap">
			<div class="askYourQuestion__slider">
				<?foreach($arResult["DISPLAY_PROPERTIES"]['faq_card']['LINK_ELEMENT_VALUE'] as $k => $v){
					$arFilter = Array("ID"=>$v['ID']);
					$res = CIBlockElement::GetList(Array(), $arFilter);
					if ($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields(); // поля элемента
						$arProps = $ob->GetProperties(); // свойства элемента?>
						<div class="askYourQuestion__slider__item">
							<div class="container">
								<div class="askYourQuestion__block">
									<div class="askYourQuestion__left">
										<div class="askYourQuestion__title title">
											<h3><?=$arProps['question_faq']['~VALUE']['TEXT']?></h3>
										</div>
									</div>
									<div class="askYourQuestion__right">
										<div class="askYourQuestion__text">
											<p><?=$arProps['answer_faq']['~VALUE']['TEXT']?></p>
										</div>
										<a class="askYourQuestion__button buttonTwo question" href="" onclick="return false">Задать свой вопрос</a>
									</div>
								</div>
							</div>
						</div><?
					}
				}?>
			</div>
			<div class="askYourQuestion__controls">
				<div class="askYourQuestion__control askYourQuestion__slide__prev">
				</div>
				<div class="askYourQuestion__control askYourQuestion__slide__next">
				</div>
			</div>
		</div>
	</div>
<?}?>

<? $GLOBALS['arrFilter'] = array('ID' => $arResult["DISPLAY_PROPERTIES"]['doctor_directions']['VALUE']); ?>

<!-- Эту оперцию проводят-->
<div class="ourDoctors">
	<div class="container">
		<div class="ourDoctors__slider__wrap">
			<div class="ourDoctors__slider">
				 <?$APPLICATION->IncludeComponent(
					"bitrix:photo.section",
					"doctor_servis",
					Array(
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "N",
						"BROWSER_TITLE" => "-",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"COMPONENT_TEMPLATE" => "doctor_servis",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"DISPLAY_TOP_PAGER" => "N",
						"ELEMENT_SORT_FIELD" => "sort",
						"ELEMENT_SORT_ORDER" => "asc",
						"FIELD_CODE" => array(0=>"PREVIEW_TEXT",1=>"",),
						"FILTER_NAME" => "arrFilter",
						"IBLOCK_ID" => "6",
						"IBLOCK_TYPE" => "doctor",
						"LINE_ELEMENT_COUNT" => "3",
						"MESSAGE_404" => "",
						"META_DESCRIPTION" => "-",
						"META_KEYWORDS" => "-",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Фотографии",
						"PAGE_ELEMENT_COUNT" => "20",
						"PROPERTY_CODE" => array(0=>"io_doctor",1=>"",),
						"SECTION_CODE" => "",
						"SECTION_ID" => $_REQUEST["SECTION_ID"],
						"SECTION_URL" => "",
						"SECTION_USER_FIELDS" => array(0=>"",1=>"",2=>"",),
						"SET_LAST_MODIFIED" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N"
					)
				);?>
			</div>
			<div class="ourDoctors__slider__control__block">
				<div class="ourDoctors__slider__control ourDoctors__slide__prev">
				</div>
				<div class="ourDoctors__slider__control ourDoctors__slide__next">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Мы предлагаем низкий платеж-->
<section class="weOfferLowPayment">
	<div class="container">
		<div class="weOfferLowPayment__icon wow animated fadeInUp" data-wow-delay="0.3s">
		</div>
		<div class="weOfferLowPayment__title title wow animated fadeInUp" data-wow-delay="0.5s">
			<h2>Мы предлагаем низкие ежемесячные платежи</h2>
		</div>
		<div class="weOfferLowPayment__desc wow animated fadeInUp" data-wow-delay="0.7s">
			<p>Быстро, легко и безопасно. Мы предоставляем комфортные программы кредитования/рассрочки при оптимальных условиях. Сотрудники нашей Клиники могут помочь вам подобрать приемлемую для вас программу финансирования выбранных медицинских процедур.</p>
		</div>
		<a class="weOfferLowPayment__link buttonTwo wow animated fadeInUp" href="/lending" data-wow-delay="1s">Условия получения</a>
	</div>
</section>
<? /*
<!-- Статья по теме-->
<div class="articlePlasticSurgery articlesOnTheTopic">
	<div class="articlePlasticSurgery__item articlesOnTheTopic__currentArticles" style="background-image: url('<?=$arResult["DISPLAY_PROPERTIES"]['img_related_articles_card']['FILE_VALUE']['SRC']?>');">
		<div class="articlesOnTheTopic__currentArticles__title">Статьи по теме</div>
		<ul class="articlesOnTheTopic__currentArticles__list">
			<?foreach($arResult["DISPLAY_PROPERTIES"]['related_articles_card']['LINK_ELEMENT_VALUE'] as $k => $v){
				?><li class="articlesOnTheTopic__currentArticles__item">
					<a class="articlesOnTheTopic__currentArticles__link" href="<?=$v['DETAIL_PAGE_URL']?>"><?=$v['NAME']?></a>
				</li><?
			}
			?>
		</ul>
	</div>
	<div class="articlePlasticSurgery__item">
		<div class="articlePlasticSurgery__item__video wow animated fadeIn" data-wow-delay="0.7s"></div>
	</div>
	<div class="articlePlasticSurgery__item articlePlasticSurgery__textLeft wow animated fadeIn" data-wow-delay="0.9s">
		<div class="articlePlasticSurgery__item__desc articlesOnTheTopic__desc title"><p>Готовы начать?</p></div>
		<div class="articlePlasticSurgery__item__currentArticles"><p>Мы ждем Вашего звонка</p></div>
	</div>
	<div class="articlePlasticSurgery__item articlePlasticSurgery__textRight wow animated fadeInRight" data-wow-delay="1.1s">
		<div class="articlesOnTheTopic__callBack">
			<div class="articlesOnTheTopic__callBack__text">
				<p>Позвоните (812) 366-0000</p>
				<p>или оставьте номер телефона и мы вам перезвоним</p>
			</div>
			<a class="articlesOnTheTopic__callBack__button buttonTwo call" href="#" onclick="return false">Заказать звонок</a>
		</div>
	</div>
</div>
*/ ?>