<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Врачи-->
<script type="text/javascript">window.timing_doctor = <?echo json_encode($arResult["DISPLAY_PROPERTIES"]['timing_doctor']['DISPLAY_VALUE'])?></script>
<div class="doctorCardPage__mainScreen">
	<!-- breadCrumbs появляется при разрешении 780px и меньше-->
	<div class="container doctorCardPage__mainScreenWrapBreadCrumbs">
		<div class="breadCrumbs">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0"
			));?>
		</div>
	</div>
	<div class="doctorCardPage__mainScreen__right">
    <picture>
      <source srcset="<?= WebPHelper($arResult["DISPLAY_PROPERTIES"]['photo_doctor']['FILE_VALUE']['SRC']) ?>" type="image/webp">
      <img src="<?=$arResult["DISPLAY_PROPERTIES"]['photo_doctor']['FILE_VALUE']['SRC']?>" alt="">
    </picture>
  </div>
	<div class="doctorCardPage__mainScreen__left">
		<div class="doctorCardPage__mainScreen__textWrap">
			<div class="doctorCardPage__mainScreen__title title beforeLine"><?=$arResult["NAME"]?>
				<br /><?=$arResult['PROPERTIES']['io_doctor']['VALUE']?></div>
			<div class="doctorCardPage__mainScreen__work">
				<?echo $arResult["PREVIEW_TEXT"];?>
			</div>
			<div class="doctorCardPage__mainScreen__quote">
				<p><?=$arResult['PROPERTIES']['quote_doctor']['VALUE']['TEXT']?></p>
			</div>
			<div class="doctorCardPage__mainScreen__experience">
				<div class="doctorCardPage__mainScreen__experience__item">
					<div class="doctorCardPage__mainScreen__experience__title">Стаж работы</div>
					<div class="doctorCardPage__mainScreen__experience__text"><?=$arResult['PROPERTIES']['experience_doctor']['VALUE']?></div>
				</div>
				<div class="doctorCardPage__mainScreen__experience__item">
					<div class="doctorCardPage__mainScreen__experience__title"><?=$arResult['PROPERTIES']['title_operations_doctor']['VALUE']?></div>
					<div class="doctorCardPage__mainScreen__experience__text"><?=$arResult['PROPERTIES']['count_operations_doctor']['VALUE']?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mainContent doctorCardPageContainer">
	<section class="mainContent__left">
		<div class="mainContent__wrap">
			<div class="breadCrumbs">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
					"PATH" => "",
					"SITE_ID" => "s1",
					"START_FROM" => "0"
				));?>
			</div>
			<div class="innerPageContent__mainBlock">
				<div class="doctorCardPage__content">
					<p><? echo $arResult["DETAIL_TEXT"];?></p>
					<!-- Блок появляется на разрешении меньше 995px-->
					<div class="doctorCardPage__tabletBlock">
						<div class="doctorCardPage__tabletBlock__item">
							<div class="mainContent__ArticleBlock__title mainContent__text__title">Расписание врача</div>
							<div class="calendar">
								<div class="calendar__block">
									<div class="datepicker"></div>
								</div>
								<a class="button calendar__button openModalPopup" href="#orderConsultation">Запись на консультацию</a>
							</div>
						</div>
						<div class="doctorCardPage__tabletBlock__item">
							<div class="faqPage__askQuestion">
								<div class="faqPage__askQuestion__title title">У вас есть вопросы
									<br/> к врачу?</div>
								<a class="faqPage__askQuestion__link buttonTwo question" href="#" onclick="return false">Задать свой вопрос</a>
								<a class="faqPage__askQuestion__more more" href="/about/faq/">Читать все вопросы</a>
							</div>
						</div>
					</div>
					<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
					<div class="doctorCardPage__blockGrey">
						<div class="doctorCardPage__title greyTitle">Основное образование</div>
						<div class="doctorCardPage__blockGrey__text">
							<? print_r($arResult["DISPLAY_PROPERTIES"]['education_doctor']['DISPLAY_VALUE']);?>
							<a class="doctorCardPage__blockGrey__link" href="#">Смотреть полностью</a>
						</div>
					</div>
					<div class="doctorCardPage__blockGrey">
						<div class="doctorCardPage__title greyTitle">Повышение квалификации</div>
						<div class="doctorCardPage__blockGrey__text">
							<? print_r($arResult["DISPLAY_PROPERTIES"]['advanced_training_doctor']['DISPLAY_VALUE']); ?>
							<a class="doctorCardPage__blockGrey__link" href="#">Смотреть полностью</a>
						</div>
					</div>

					<!-- Сертификаты -->
					<div class="doctorCardPage__title greyTitle rbs">Сертификаты</div>
                    <div class="documentsBlockSlider" style="margin-top: 200px;">

                        <div class="diplomasSlider">

							<? foreach($arResult["DISPLAY_PROPERTIES"]['certificate_doctor']['FILE_VALUE'] as $k => $v): ?>
                                <div class="diplomasSlider__item">
                                    <a class="doctorCardPage__certificate__link" href="<?= WebPHelper($v['SRC']) ?>" data-fancybox="doctorCertificate">
                                        <picture>
                                            <img src="<?= getResizedImageCopyPath($v['SRC']) ?>" alt="">
                                            <source src="<?= getResizedImageCopyPath(WebPHelper($v['SRC'])) ?>" type="image/webp">
                                        </picture>
                                    </a>
                                </div>
							<? endforeach ?>
                        </div>

                        <div class="documentsSlider__controls">
                            <div class="documentsSlider__control documentsSlide__prev diplomasSlider__prev" style="left: 0px;"></div>
                            <div class="documentsSlider__control documentsSlide__next diplomasSlider__next"></div>
                        </div>

                    </div>

                    <!-- видео -->
					<?
                    if(!empty($arResult["DISPLAY_PROPERTIES"]['video_doctor'])){?>
					<div class="doctorCardPage__video">
						<video class="doctorCardPage__video__item" src="<?=$arResult["DISPLAY_PROPERTIES"]['video_doctor']['FILE_VALUE']['SRC']?>" controls></video>
						<!--<div class="doctorCardPage__video__item" style="background-image:url(img/doctorCardVideo.jpg);"></div>-->
						<a class="doctorCardPage__video__link more" href="#">Все видео</a>
					</div>
					<? } ?>
					<!-- Специализация -->
					<div class="doctorCardPage__title greyTitle">Специализация</div>
					<?
             $specialization = array();
					//Пластика
					foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_plastic']['LINK_ELEMENT_VALUE'] as $k => $v){
						$db_old_groups = CIBlockElement::GetElementGroups($k, true);
						while($ar_group = $db_old_groups->Fetch()){
							$specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
						}
					}
					//Косметология
					foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_cosmetology']['LINK_ELEMENT_VALUE'] as $k => $v){
						$db_old_groups = CIBlockElement::GetElementGroups($k, true);
						while($ar_group = $db_old_groups->Fetch()){
							$specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
						}
					}
					//Флебология
					foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_phlebology']['LINK_ELEMENT_VALUE'] as $k => $v){
						$specialization[0]['NAME'] = "Флебология";
						$specialization[0]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
						$specialization[0]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
					}
					//Пересадка волос
					foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_hair_transplant']['LINK_ELEMENT_VALUE'] as $k => $v){
						$db_old_groups = CIBlockElement::GetElementGroups($k, true);
						while($ar_group = $db_old_groups->Fetch()){
							$specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
						}
					}
					//Стоматология
					foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_dentistry']['LINK_ELEMENT_VALUE'] as $k => $v){
						$db_old_groups = CIBlockElement::GetElementGroups($k, true);
						while($ar_group = $db_old_groups->Fetch()){
							$specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
							$specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
						}
					}       
					foreach($specialization as $k => $v){
                    ?>
						<div class="doctorCardPage__specialization">
							<div class="doctorCardPage__specialization__left">
								<div class="doctorCardPage__specialization__title"><?=$v['NAME']?></div>
							</div>
							<div class="doctorCardPage__specialization__right">
							<?
                            foreach($v['VALUE'] as $j => $l){
								?><a class="doctorCardPage__specialization__text" href="<?=$l['DETAIL_PAGE_URL']?>"><?=$l['NAME']?></a><?
							}
							?>
							</div>
						</div><?
					}?>
				</div>
			</div>
		</div>
	</section>
	<aside class="mainContent__aside doctorCardPage__aside">
		<div class="mainContent__aside__wrap">
			<div class="calendarWrap">
				<div class="mainContent__ArticleBlock__title mainContent__text__title">Расписание врача</div>
				<div class="calendar">
					<div class="calendar__block">
						<div class="datepicker"></div>
					</div>
					<a class="button calendar__button openModalPopup" href="#orderConsultation">Запись на консультацию</a>
				</div>
			</div>
			<!-- Попап Запись на консультацию-->
			<div class="popup" id="orderConsultation">
				<div class="popup__close">
					<div class="popup__close__line"></div>
					<div class="popup__close__line"></div>
				</div>
				<div class="popup__content">
					<div class="popup__content-title">Запись на консультацию</div>
					<div class="popup__content-desc"><?=$arResult["NAME"]?> <?=$arResult['PROPERTIES']['io_doctor']['VALUE']?></div>
					<?$APPLICATION->IncludeComponent(
						"bitrix:form.result.new", 
						"appointment_consultation", 
						array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"CHAIN_ITEM_LINK" => "",
							"CHAIN_ITEM_TEXT" => "",
							"EDIT_URL" => "",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"LIST_URL" => "",
							"SEF_MODE" => "N",
							"SUCCESS_URL" => "",
							"USE_EXTENDED_ERRORS" => "N",
							"WEB_FORM_ID" => "3",
							"COMPONENT_TEMPLATE" => "appointment_consultation",
							"VARIABLE_ALIASES" => array(
								"WEB_FORM_ID" => "WEB_FORM_ID",
								"RESULT_ID" => "RESULT_ID",
							)
						),
						false
					);?>
					<button class="popup__button-close buttonTwo" type="button">Вернуться на сайт</button>
				</div>
			</div>
			<div class="partners">
				<div class="mainContent__ArticleBlock__title mainContent__text__title">Партнерство</div>
				<div class="partners__block">
					<?foreach($arResult["DISPLAY_PROPERTIES"]['partnership_doctor']['FILE_VALUE'] as $k => $v){
						?><a class="partners__block__link" href="<?=$v['SRC']?>">
							<img src="<?=$v['SRC']?>" alt="картинка">
						</a><?
					}?>
				</div>
			</div>
			<!-- Результаты работ врача -->
			<section class="mainContent__ArticleBlock__beforeAfter doctorCardPage__workResalt">
				<div class="mainContent__ArticleBlock__title mainContent__text__title">Результаты работ врача</div>
				<?foreach($arResult["DISPLAY_PROPERTIES"]['result_doctor']['VALUE'] as $k=>$v){
					$arFilter = Array("ID"=>$v);
					$res = CIBlockElement::GetList(Array(), $arFilter);
					if ($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields(); // поля элемента
						$arProps = $ob->GetProperties(); // свойства элемента ?>
						<div class="mainContent__ArticleBlock__beforeImg">
							<img src="<?=CFile::GetPath($arProps['fas_before_after']['VALUE'])?>" alt="картинка">
						</div>
					<?}
				}?>
				<a class="more" href="/before_after">Смотреть все</a>
			</section>
			<div class="faqPage__askQuestion">
				<div class="faqPage__askQuestion__title title">У вас есть вопросы
					<br/> к врачу?</div>
				<a class="faqPage__askQuestion__link buttonTwo openModalPopup" href="#question">Задать свой вопрос</a>
				<a class="faqPage__askQuestion__more more" href="/about/faq/">Читать все вопросы</a>
			</div>
		</div>
	</aside>
</div>
