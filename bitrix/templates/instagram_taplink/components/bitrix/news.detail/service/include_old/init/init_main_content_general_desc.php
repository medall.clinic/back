<?
$generalDescId = !empty($displayProperties["general_desc"]["VALUE"]) ? $displayProperties["general_desc"]["VALUE"] : null;
$generalDescItemsId = !empty($displayProperties["general_desc_items"]["VALUE"]) ? $displayProperties["general_desc_items"]["VALUE"] : null;
$generalDesc_add_content = !empty($displayProperties["general_desc_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["general_desc_add_content"]["DISPLAY_VALUE"] : null;

$generalDesc = [];
if ($generalDescId) {
	$res = CIBlockElement::GetList([], ["ID" => $generalDescId]);
	$ob = $res->GetNextElement();
	$fields = $ob->GetFields();
	$generalDesc = [
		"title" => $props["title"]["VALUE"],
		"text" => $fields["~PREVIEW_TEXT"],
		"items" => getGeneralDescItems($generalDescItemsId),
	];
}



function getGeneralDescItems($itemsId)
{
	if (empty($itemsId)) {
		return false;
	}

	$items = [];
	$res = CIBlockElement::GetList([], ["ID" => $itemsId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$items[] = [
			"title" => $fields["NAME"],
			"text" => $fields["~PREVIEW_TEXT"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"is_width_wide" => !empty($props["width"]["VALUE"]) && $props["width"]["VALUE_XML_ID"] == "wide" ? true : false,
		];
	}

	return $items;
}