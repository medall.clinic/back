<div class="block-icon">
  <? if($item["img"]): ?><div class="block-icon__icon"><img src="<?= $item["img"] ?>"/></div><? endif ?>
  <div class="block-icon__body">
    <h3 class="block-icon__header"><?= $item["title"] ?></h3>
    <div class="block-icon__text">
      <?= $item["text"] ?>
    </div>
  </div>
</div>
