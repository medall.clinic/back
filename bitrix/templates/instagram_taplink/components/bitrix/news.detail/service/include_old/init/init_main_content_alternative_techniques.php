<?
/** @var array $displayProperties */
$alternativeTechniquesId = $displayProperties["alternative_techniques"]["VALUE"];

$alternativeTechniques = [];
if ($alternativeTechniquesId) {
	$res = CIBlockElement::GetList([], ["ID" => $alternativeTechniquesId]);

	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$alternativeTechniques[] = [
			"title" => $fields["NAME"],
			"link" => $props["link"]["VALUE"],
		];
	}

}

$alternative_techniques_add_content = !empty($displayProperties["alternative_techniques_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["alternative_techniques_add_content"]["DISPLAY_VALUE"] : null;
