<? if($asideBlockArticlesWoBg): ?>
<section class="mainContent__ArticleBlock">

	<div class="mainContent__ArticleBlock__title mainContent__text__title">
		<?= $asideBlockArticlesWoBg["title"] ?>
    </div>

	<? if($asideBlockArticlesWoBg["articles"]): ?>
    <ul class="mainContent__ArticleBlock__list var-2">

		<? foreach($asideBlockArticlesWoBg["articles"] as $article): ?>
        <li class="mainContent__ArticleBlock__list__item">
            <div class="card">

                <? if($article["img"]): ?>
                <div class="image">
                    <img
                        src="<?= $article["img"] ?>"
                        alt="<?= $article["title"] ?>"
                        title="<?= $article["title"] ?>"
                    />
                </div>
                <? endif ?>

                <div class="content">
                    <div class="title"><?= $article["title"] ?></div>
                    <div class="text"><?= $article["text"] ?></div>
                    <a class="more" href="<?= $article["link"] ?>">Читать</a>
                </div>

            </div>
        </li>
		<? endforeach ?>

	</ul>
	<? endif ?>

</section>
<? endif ?>