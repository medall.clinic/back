<? if($prices): ?>
  <div class="service_page-section">
    <h2 class="service_page-section__subheader">Цены</h2>
    <table class="table-price">
    <tr>
      <th>Наименование процедуры </th>
      <th>Цена</th>
        <? if($prices["isPricesHasSaleOffers"]): ?>
          <th>Акционная цена</th>
        <? endif ?>
    </tr>
    <? foreach($prices["items"] as $price): ?>
        <? include __DIR__."/price.php" ?>
    <? endforeach ?>
    </table>
  </div>
<? endif ?>

<? if($prices_add_content): ?>
    <div class="prices_add_content">
		<?= $prices_add_content ?>
    </div>
<? endif ?>
