<? if($stagesBlocks): ?>
    <? foreach ($stagesBlocks as $stagesBlock): ?>
    <div class="service_page-section">

        <? if($stagesBlock["title"]): ?>
        <h2 class="service_page-section__subheader">
            <?= $stagesBlock["title"] ?>
        </h2>
        <? endif ?>

        <? if($stagesBlock["items"]): ?>
            <div class="block-marked">
                <? foreach($stagesBlock["items"] as $stage): ?>
                    <? include __DIR__."/stage.php" ?>
                <? endforeach ?>
            </div>
        <? endif ?>
    </div>
    <? endforeach ?>
<? endif ?>

<? if($stages_add_content): ?>
    <div class="stages_add_content">
		<?= $stages_add_content ?>
    </div>
<? endif ?>
