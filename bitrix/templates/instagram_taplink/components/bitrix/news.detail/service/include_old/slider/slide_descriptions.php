<? /** @var array $slide */ ?>
<? if($slide["descriptions"]): ?>
    <div class="block-wrapper block-34-4-two-cols-with-text-and-icons">
        <section class="landing-block g-bg-primary-dark-v3 g-pt-55 g-pb-0">
            <div class="container">
                <div class="row landing-block-inner">

                    <? foreach ($slide["descriptions"] as $description): ?>
                        <? include __DIR__."/slide_description.php" ?>
					<? endforeach ?>

                </div>
            </div>
        </section>
    </div>
<? endif ?>