<?
/** @var array $ourDoctorsId */
?>
<? if($ourDoctorsId): ?>
    <?
    $GLOBALS['arrFilter'] = [
        'ID' => $ourDoctorsId
    ];
    ?>
    <!-- Эту оперцию проводят-->
    <div class="ourDoctors">
        <div class="container">
            <div class="ourDoctors__slider__wrap">

                <div class="ourDoctors__slider">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:photo.section",
                        "doctor_servis",
                        Array(
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPONENT_TEMPLATE" => "doctor_servis",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "FIELD_CODE" => array(0=>"PREVIEW_TEXT",1=>"",),
                            "FILTER_NAME" => "arrFilter",
                            "IBLOCK_ID" => "6",
                            "IBLOCK_TYPE" => "doctor",
                            "LINE_ELEMENT_COUNT" => "3",
                            "MESSAGE_404" => "",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Фотографии",
                            "PAGE_ELEMENT_COUNT" => "20",
                            "PROPERTY_CODE" => array(0=>"io_doctor",1=>"",),
                            "SECTION_CODE" => "",
                            "SECTION_ID" => $_REQUEST["SECTION_ID"],
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array(0=>"",1=>"",2=>"",),
                            "SET_LAST_MODIFIED" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N"
                        )
                    );?>
                </div>

                <div class="ourDoctors__slider__control__block">
                    <div class="ourDoctors__slider__control ourDoctors__slide__prev">
                    </div>
                    <div class="ourDoctors__slider__control ourDoctors__slide__next">
                    </div>
                </div>

            </div>
        </div>
    </div>
<? endif ?>

<? if($doctor_directions_add_content): ?>
    <div class="doctor_directions_add_content">
		<?= $doctor_directions_add_content ?>
    </div>
<? endif ?>

