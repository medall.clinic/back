<?
$recommendationsBlocksIds = !empty($displayProperties["recommendations"]["VALUE"]) ? $displayProperties["recommendations"]["VALUE"] : null;

$recommendationsBlocks = [];
if($recommendationsBlocksIds){
	foreach ($recommendationsBlocksIds as $recommendationsBlockId){
		$res = CIBlockElement::GetList([], ["ID" => $recommendationsBlockId]);
		$ob = $res->GetNextElement();
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$recommendationsBlocks[] = [
			"title" => $props["title"]["VALUE"],
			"items" => getRecommendationsItems($props),
		];
	}
}

$recommendations_add_content = !empty($displayProperties["recommendations_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["recommendations_add_content"]["DISPLAY_VALUE"] : null;


function getRecommendationsItems($props)
{
	if(empty($props["items"]["VALUE"])){
		return false;
	}

	$itemsId = $props["items"]["VALUE"];

	$items = [];
	$res = CIBlockElement::GetList([], ["ID" => $itemsId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$items[] = [
			"title" => $fields["NAME"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"text" => $fields["~PREVIEW_TEXT"],
		];
	}

	return $items;
}
