<? if($reviews): ?>
<section class="reviews">

	<div class="container">
		<div class="reviews__desc wow animated fadeInLeft">
			<p>Мы гордимся результатами</p>
		</div>
		<div class="reviews__title title beforeLine wow animated fadeInRight" data-wow-delay="0.5s">
			<h2>Что говорят наши пациенты</h2>
		</div>
		<div class="reviews__slogan wow animated fadeInLeft" data-wow-delay="0.8s">
			<p>Положительные отзывы наших пациентов - доказательство нашей успешной работы</p>
		</div>
	</div>

	<div class="reviews__slider__wrap wow animated fadeInRight" data-wow-delay="0.8s">
		<div class="reviews__slider__block">
			<div class="reviews__slider">

                <? foreach($reviews as $review): ?>
                    <? include __DIR__."/review.php" ?>
				<? endforeach ?>

            </div>
		</div>
		<div class="reviews__slider__controls__block">
			<div class="reviews__slider__control reviews__slider__control__prev"></div>
			<div class="reviews__slider__control reviews__slider__control__next"></div>
		</div>
	</div>

	<div class="container">
		<div
            class="reviews__link wow animated fadeInLeft"
            data-wow-delay="0.8s"
        >
            <a class="buttonTwo" href="/review">Все отзывы</a>
        </div>
	</div>

</section>
<? endif ?>

<? if($reviews_add_content): ?>
    <div class="reviews_add_content">
		<?= $reviews_add_content ?>
    </div>
<? endif ?>


