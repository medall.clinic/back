<?
$departmentInfoId = !empty($displayProperties["department_info"]["VALUE"]) ? $displayProperties["department_info"]["VALUE"] : null;

$departmentInfo = null;
if ($departmentInfoId) {
	$res = CIBlockElement::GetList([], ["ID" => $departmentInfoId]);
	$ob = $res->GetNextElement();
	$fields = $ob->GetFields();
	$props = $ob->GetProperties();
	$departmentInfo = [
		"title" => $fields["NAME"],
		"text" => $fields["PREVIEW_TEXT"],
		"slides" => getDepartmentInfoSlides($props),
	];
}

function getDepartmentInfoSlides($props)
{
	if(empty($props["slides"]["VALUE"])){
		return false;
	}

	$slidesId = $props["slides"]["VALUE"];

	$slides = [];
	$res = CIBlockElement::GetList([], ["ID" => $slidesId]);
	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$slides[] = [
			"title" => $fields["NAME"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"link" => getSlideLink($fields, $props),
		];
	}

	return $slides;
}

$department_info_add_content = !empty($displayProperties["department_info_add_content"]["VALUE"]["TEXT"]) ? $displayProperties["department_info_add_content"]["VALUE"]["TEXT"] : null;

function getSlideLink($fields, $props)
{
	if(!empty($props["video_id"]["VALUE"])){
		return "https://www.youtube.com/embed/".$props["video_id"]["VALUE"]."?controls=0&rel=0";
	}

	if(!empty($fields["DETAIL_PICTURE"])){
		return CFile::GetPath($fields["DETAIL_PICTURE"]);
	}

	if(!empty($fields["PREVIEW_PICTURE"])){
		return CFile::GetPath($fields["PREVIEW_PICTURE"]);
	}

	return false;
}