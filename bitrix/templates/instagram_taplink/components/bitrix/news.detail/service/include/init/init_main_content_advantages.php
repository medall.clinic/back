<?
/** @var array $displayProperties */
$advantagesId = $displayProperties["advantages_items"]["VALUE"];

$advantages = [];
if ($advantagesId) {
	$res = CIBlockElement::GetList([], ["ID" => $advantagesId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$advantages[] = [
			"title" => $fields["NAME"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"text" => $fields["~PREVIEW_TEXT"],
		];
	}
}

$advantages_add_content = !empty($displayProperties["advantages_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["advantages_add_content"]["DISPLAY_VALUE"] : null;