<? /** @var array $slide */ ?>
<a class="service_grid__cell service_grid__cell--s-4 service_grid__cell--6 dep-info__gallery-item dep-info__gallery-item" href="<?= $slide["link"] ?>" data-fancybox="slider-department">
  <div class="dep-info__image">
    <picture>
      <source srcset="<?= WebPHelper($slide["img"]) ?>" type="image/webp">
      <img src="<?= $slide["img"] ?>" alt="<?= $slide["title"] ?>"/>
    </picture>
    <div class="dep-info__image-name"><?= $slide["title"] ?></div>
  </div>
</a>
