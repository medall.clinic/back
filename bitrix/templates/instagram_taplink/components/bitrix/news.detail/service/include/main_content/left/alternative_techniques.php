<? if($alternativeTechniques): ?>
<div class="service_page-section">
  <h2 class="service_page-section__subheader">Альтернативные методики:</h2>
  <ul class="list-marked">
    <? foreach($alternativeTechniques as $alternativeTechnique): ?>
        <li>
        <? if($alternativeTechnique["link"]): ?>
            <a href="<?= $alternativeTechnique["link"] ?>"><?= $alternativeTechnique["title"] ?></a>
        <? else: ?>
            <?= $alternativeTechnique["title"] ?>
        <? endif ?>
        </li>
    <? endforeach ?>
  </ul>
</div>
<? endif ?>

<? if($alternative_techniques_add_content): ?>
    <div class="alternative_techniques_add_content">
		<?= $alternative_techniques_add_content ?>
    </div>
<? endif ?>
