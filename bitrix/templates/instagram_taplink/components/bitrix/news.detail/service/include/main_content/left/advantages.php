<? if($advantages): ?>
<div class="service_page-section">
  <h2 class="service_page-section__subheader"></h2>
  <div class="service_grid service_grid--padding-y ">

  <div class="service_grid__cell service_grid__cell--m-12">

      <div class="service_page-subsection">
        <? foreach ($advantages as $advantage): ?>
            <div class="block-icon">

                <? if($advantage["img"]): ?>
                    <div class="block-icon__icon">
                        <img src="<?= $advantage["img"] ?>" />
                    </div>
                <? endif ?>

                <div class="block-icon__body">
                    <h3 class="block-icon__header"><?= $advantage["title"] ?></h3>
                    <div class="block-icon__text">
                        <?= $advantage["text"] ?>
                    </div>
                </div>

            </div>
        <? endforeach ?>
      </div>
    </div>

  </div>
</div>
<? endif ?>

<? if($advantages_add_content): ?>
    <div class="advantages_add_content">
		<?= $advantages_add_content ?>
    </div>
<? endif ?>
