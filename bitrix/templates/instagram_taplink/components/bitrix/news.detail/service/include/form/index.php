<div class="landing-page" style="margin-top: 20px; margin-bottom: 120px; padding: 0 5px; font-size: inherit;">
	<section class="page-section" id="section-contact">
		<div class="grid grid--justify-center">
			<div class="grid__cell grid__cell--m-8 grid__cell--s-10 grid__cell--12 ">

				<div class="page-subsection">
					<h2 style="font-size: 32px; line-height 1.25; font-family: inherit;">Остались вопросы? Оставьте свои контактные данные и мы обязательно свяжемся с Вами.</h2>
				</div>

				<div class="page-subsection">
					<? include __DIR__."/form.php" ?>
				</div>

			</div>
		</div>
	</section>
</div>
