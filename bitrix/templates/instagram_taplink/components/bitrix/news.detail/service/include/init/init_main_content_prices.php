<?
/** @var array $displayProperties */
$pricesId = $displayProperties["prices"]["VALUE"];

$prices = [];
if ($pricesId) {
	$res = CIBlockElement::GetList([], ["ID" => $pricesId]);

	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$prices[] = [
			"title" => $fields["NAME"],
			"price_raw" => $props["price"]["VALUE"],
			"sale_price_raw" => $props["sale_price"]["VALUE"],
		];
	}

}

if($prices){
	$prices = [
		"items" => formatPrices($prices),
		"isPricesHasSaleOffers" => isPricesHasSaleOffers($prices),
	];
}

$prices_add_content = !empty($displayProperties["prices_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["prices_add_content"]["DISPLAY_VALUE"] : null;

function formatPrices($prices)
{
	return array_map(function($price){
		$price["price"] = 0;
		$price["sale_price"] = 0;
		if(!empty($price["price_raw"])){
			$price["price"] = number_format($price["price_raw"], 0, ".", " ");
		}
		if(!empty($price["sale_price_raw"])){
			$price["sale_price"] = number_format($price["sale_price_raw"], 0, ".", " ");
		}
		return $price;
	}, $prices);
}

function isPricesHasSaleOffers($prices)
{
	return (bool)array_filter($prices, function($price){
	    return !empty($price["sale_price_raw"]);
	});
}