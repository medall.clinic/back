<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="doctorCardPage__mainScreen">
	<? include __DIR__."/main_screen/index.php" ?>
</div>

<div class="mainContent doctorCardPageContainer">
    <? include __DIR__."/content/index.php" ?>
    <? include __DIR__."/aside/index.php" ?>
</div>
