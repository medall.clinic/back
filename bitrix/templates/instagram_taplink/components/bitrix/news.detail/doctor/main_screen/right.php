<div class="doctorCardPage__mainScreen__right">
	<picture>
        <source
            srcset="<?= WebPHelper($arResult["DISPLAY_PROPERTIES"]["photo_doctor"]["FILE_VALUE"]["SRC"]) ?>"
            type="image/webp"
        >
        <img src="<?= $arResult["DISPLAY_PROPERTIES"]["photo_doctor"]["FILE_VALUE"]["SRC"] ?>">
    </picture>
</div>

