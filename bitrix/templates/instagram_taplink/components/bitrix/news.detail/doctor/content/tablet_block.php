<!-- Блок появляется на разрешении меньше 995px-->
<div class="doctorCardPage__tabletBlock">
	<div class="doctorCardPage__tabletBlock__item">
		<div class="calendarWrap">
			<div class="mainContent__ArticleBlock__title mainContent__text__title"></div>
			<div class="calendar">
				<a class="button calendar__button openModalPopup" href="#orderConsultation">Запись на консультацию</a>
			</div>
		</div>
	</div>
	<div class="doctorCardPage__tabletBlock__item">
		<div class="faqPage__askQuestion">
			<div class="faqPage__askQuestion__title title">У вас есть вопросы
				<br/> к врачу?</div>
			<a class="faqPage__askQuestion__link buttonTwo question" href="#" onclick="return false">Задать свой вопрос</a>
			<a class="faqPage__askQuestion__more more" href="/about/faq/">Читать все вопросы</a>
		</div>
	</div>
</div>