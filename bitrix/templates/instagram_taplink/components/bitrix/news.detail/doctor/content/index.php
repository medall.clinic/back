<section class="mainContent__left">

	<div class="mainContent__wrap">

		<? include __DIR__."/breadcrumbs.php" ?>

		<div class="innerPageContent__mainBlock">
			<div class="doctorCardPage__content">
				<? include __DIR__."/description.php" ?>
				<? include __DIR__."/tablet_block.php" ?>
				<? include __DIR__."/photos.php" ?>
				<? include __DIR__."/education.php" ?>
				<? include __DIR__."/qualification.php" ?>
				<? include __DIR__."/certificates.php" ?>
				<? include __DIR__."/before-after.php" ?>
				<? include __DIR__."/specialization.php" ?>
			</div><!-- doctorCardPage__content -->
		</div> <!-- innerPageContent__mainBlock -->

	</div><!-- mainContent__wrap -->

</section>