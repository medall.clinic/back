<? if($arResult["PROPERTIES"]["gallery_photos"]["VALUE"]): ?>
    <? $photosId = $arResult["PROPERTIES"]["gallery_photos"]["VALUE"] ?>
    <div class="doctor-photos">

        <? if(count($photosId) > 1): ?>
        <div class="doctor-photos__slider">
            <? foreach ($photosId as $photoId): ?>
                <? $photoPath = CFile::GetPath($photoId) ?>
                <div class="doctor-photos__item">
                    <picture>
                        <source srcset="<?= WebPHelper($photoPath) ?>" type="image/webp">
                        <img src="<?= $photoPath ?>" alt="<?= $arResult["NAME"] ?>">
                    </picture>
                </div>
            <? endforeach ?>
        </div>

        <div class="doctor-photos__prev"></div>
        <div class="doctor-photos__next"></div>

        <? else: ?>
            <? $firstPhotoId = reset($photosId) ?>
			<? $photoPath = CFile::GetPath($firstPhotoId) ?>
            <picture>
                <source srcset="<?= WebPHelper($photoPath) ?>" type="image/webp">
                <img src="<?= $photoPath ?>" alt="<?= $arResult["NAME"] ?>">
            </picture>
        <? endif ?>

    </div>
<? endif ?>