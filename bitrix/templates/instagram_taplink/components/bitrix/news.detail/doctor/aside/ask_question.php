<div class="faqPage__askQuestion">
	<div class="faqPage__askQuestion__title title">У вас есть вопросы<br/> к врачу?</div>
	<a class="faqPage__askQuestion__link buttonTwo openModalPopup" href="#question">Задать свой вопрос</a>
	<a class="faqPage__askQuestion__more more" href="/about/faq/">Читать все вопросы</a>
</div>