<!-- Попап Запись на консультацию-->
<? $doctorFullName = $arResult["NAME"]." ".$arResult["PROPERTIES"]["io_doctor"]["VALUE"] ?>
<div class="popup" id="orderConsultation">

	<div class="popup__close">
		<div class="popup__close__line"></div>
		<div class="popup__close__line"></div>
	</div>

	<div class="popup__content">

		<div class="popup__content-title">Запись на консультацию</div>

        <div class="popup__content-desc"><?= $doctorFullName ?></div>

		<h3>Запись на консультацию</h3>

        <p></p>

        <form
            action=""
            method="POST"
            class="order_consultation_popup popup__content-form"
            data-wow-delay="0.7s"
        >

			<input type="hidden" name="doctor_full_name" value="<?= $doctorFullName ?>" >

			<input type="text" name="name" placeholder="ФИО" required>
			<input type="text" name="service" placeholder="Услуга которая Вас интересует" required>
			<input type="text" name="phone" placeholder="Телефон" class="phone" required>

			<div class="form-connection-type">
				<p><strong>Удобный формат связи</strong></p>
				<label class="form__label">
					<input type="radio" name="connectionType" value="Звонок" checked>
					Звонок
				</label>
				<label class="form__label">
					<input type="radio" name="connectionType" value="Сообщение в Telegram">
					Сообщение в Telegram
				</label>
				<label class="form__label">
					<input type="radio" name="connectionType" value="Сообщение в WhatsApp">
					Сообщение в WhatsApp
				</label>
			</div>

			<textarea name="comments" placeholder="Дополнительная информация" ></textarea>

			<div class="popup__content-form-block">

                <label class="form__label popup__content-form-label">
					<input
                        type="checkbox"
                        name="form_checkbox_SIMPLE_QUESTION_506[]"
                        value="23"
                        id="23"
                        required
                        checked
                    >
					<span class="checkbox__box"></span>
					<span class="form__label__text">Согласен <a href="#">с политикой конфеденциальности</a></span>
				</label>

				<div class="popup__content-form-block-button">
					<input
                        class="button popup__content-form-button"
                        type="submit"
                        name="web_form_submit"
                        value="Отправить"
                    />
				</div>

			</div>

			<? if(!empty($_GET)): ?>
				<? foreach ($_GET as $paramName => $paramValue): ?>
					<? if(strpos($paramName, 'utm_') !== false): ?>
                        <input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
					<? endif ?>
				<? endforeach ?>
			<? endif ?>

		</form>

        <button class="popup__button-close buttonTwo" type="button">Вернуться на сайт</button>

	</div> <!-- popup__content -->

</div> <!-- popup -->
