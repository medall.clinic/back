<!-- шапка мобильная -->
<header class="header container hidden-hd hidden-xl hidden-l">
	<div class="header__content content">
		<div class="header__item"><a class="header__logo" href="/"></a></div>
		<div class="header__item header__contacts">
			<?= $phoneNumber ?><br>
			Левашовский пр., д.24<br>
			<a href="mailto:admin@medall.clinic">admin@medall.clinic</a>
		</div>
	</div>
</header>
<!-- /шапка мобильная -->