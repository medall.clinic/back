<?
use SocialModels\SocialModel;

require_once __DIR__."/initialise.php";

$phoneNumber = "+7 (812) 603-02-01";
$b24FormTitle = "Форма записи";

if(!empty($_GET["social"])){
	try{
		$socialModel = SocialModel::create($_GET["social"]);
		$phoneNumber = $socialModel->phone;
		$socialModel->setTitle();
		$b24FormTitle .= " ".$socialModel->title;
	}catch (\Exception $e){}
}
