<?
require_once __DIR__."/SocialModels/SocialModel.php";

$dir = new RecursiveDirectoryIterator(__DIR__."/SocialModels");
foreach (new RecursiveIteratorIterator($dir) as $file) {
	if (!is_dir($file)) {
		if( fnmatch('*.php', $file) ){
			require_once $file;
		}
	}
}
