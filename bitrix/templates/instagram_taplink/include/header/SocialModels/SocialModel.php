<?
namespace SocialModels;

abstract class SocialModel
{
	public static function create(string $code): self
	{
		if($code == "ig"){return new Instagram;}
		if($code == "ok"){return new Odnoklassniki;}
		if($code == "tg"){return new Telegram;}
		if($code == "vk"){return new Vkontakte;}
		if($code == "zen"){return new YandexZen;}
		if($code == "tenchat"){return new TenChat;}

		throw new \Exception("Не найден обработчик социальной сети");
	}

	public function setTitle()
	{
		global $APPLICATION;
		$APPLICATION->SetPageProperty("title", "Medall - Форма записи ".$this->title);
	}

}