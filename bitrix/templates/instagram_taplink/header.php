<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?>
<? $htmlClass = isServiceInNewDesign($curPage, $_REQUEST["CODE"]) ? 'class="landing-page"' : "" ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru" <?= $htmlClass ?>>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<? /* <meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content=""> */ ?>
		<meta name="viewport" content="width=device-width initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">
    <link rel="icon" href="/favicon.svg" type="image/svg">
		<title><?$APPLICATION->ShowTitle()?></title>
		<!--css-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat&amp;amp;subset=cyrillic&display=swap">
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/slick.css">
        <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fancybox.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		
		<?if (!strpos($_SERVER['REQUEST_URI'], "socials") && !strpos($_SERVER['REQUEST_URI'], "promo")){?>
			<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/style.css?v=14">
		<?} else {?>
			<link rel="stylesheet" type="text/css" href="/socials/template/styles/style.css?v=14">
		<?}?>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/main.css?v=13">
		<!--script-->
		<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/slick.min.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js?v=2"></script>
		<?if (!strpos($_SERVER['REQUEST_URI'], "socials") && !strpos($_SERVER['REQUEST_URI'], "promo")){?>
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js?v=2"></script>
		<?} else {?>
			<script type="text/javascript" src="/socials/template/scripts/script.js?v=14"></script>
		<?}?>
        <script src="/bitrix/templates/landing/scripts/bitrix24.js"></script>

		<?$APPLICATION->ShowHead();?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-DPSJ4YYBE3"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'G-DPSJ4YYBE3');
		</script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '754193912100577');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=754193912100577&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

		<? include $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/landing/include/header/code.php" ?>
		<? include $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/calltouch.html" ?>

    </head>

	<body class="innerPage">
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
			(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
			m[i].l=1*new Date();
			for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
			k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
			(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

			ym(67423126, "init", {
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
			});
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/67423126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->


    <? include __DIR__."/include/header/header.php" ?>

		<nav class="menuMain">
				<div class="menuMain__left">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "mainleft", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "2",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "mainleft",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"COMPONENT_TEMPLATE" => "mainright"
						),
						false
					);?>
				</div>
				<div class="menuMain__right">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "mainright", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "2",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "mainright",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"COMPONENT_TEMPLATE" => "tree"
						),
						false
					);?>
				</div>
		</nav>
