<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$Result = '';
if($arResult["REQUEST"]["QUERY"]!=''){$Result = $arResult["REQUEST"]["QUERY"];}
else{$Result = $arResult["REQUEST"]["TAGS"];}
?>
<div class="mainContent__title title">
	<h1>Результаты поиска:
		<span><?=$Result?></span>
	</h1>
</div>
<div class="innerPageContent__mainBlock">
	<div class="searchResaltPage__content">
		<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):?>
			<div class="search-language-guess">
				<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
			</div><br /><?
		endif;?>
		<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
		<?elseif($arResult["ERROR_CODE"]!=0):?>
			<p><?=GetMessage("SEARCH_ERROR")?></p>
			<?ShowError($arResult["ERROR_TEXT"]);?>
			<p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
			<br /><br />
			<p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
			<table border="0" cellpadding="5">
				<tr>
					<td align="center" valign="top"><?=GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
					<td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
				</tr>
				<tr>
					<td align="center" valign="top"><?=GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
					<td><?=GetMessage("SEARCH_AND_ALT")?></td>
				</tr>
				<tr>
					<td align="center" valign="top"><?=GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
					<td><?=GetMessage("SEARCH_OR_ALT")?></td>
				</tr>
				<tr>
					<td align="center" valign="top"><?=GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
					<td><?=GetMessage("SEARCH_NOT_ALT")?></td>
				</tr>
				<tr>
					<td align="center" valign="top">( )</td>
					<td valign="top">&nbsp;</td>
					<td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
				</tr>
			</table>
		<?elseif(count($arResult["SEARCH"])>0):
			$SEARCH = array();
			$os = array(22,5,19,18,11,1);
			foreach($arResult["SEARCH"] as $arItem){
				$res = CIBlockElement::GetByID($arItem['ITEM_ID']);
				if($arRes = $res->Fetch()){
					$res = CIBlock::GetByID($arRes['IBLOCK_ID']);
					if($ar_res = $res->GetNext()){
						if (!in_array($ar_res['ID'], $os)) {
							$SEARCH[$arRes['IBLOCK_ID']]["NAME"] = $ar_res['NAME'];
							$SEARCH[$arRes['IBLOCK_ID']]["SEARCH"][] = $arItem;
						}
					}
				}
			}
			foreach($SEARCH as $arItem){
				?><div class="searchResaltPage__title greyTitle"><?=$arItem['NAME']?></div><?
				foreach($arItem["SEARCH"] as $arItem2){?>
					<div class="searchResaltPage__block">
						<a class="searchResaltPage__block__title greyTitle" href="<?echo $arItem2["URL"]?>"><?echo $arItem2["TITLE_FORMATED"]?></a>
						<div class="searchResaltPage__block__text">
							<p><?echo $arItem2["BODY_FORMATED"]?></p>
						</div>
					</div><?
				}
			}
		else:?>
			<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
		<?endif;?>
	</div>
</div>