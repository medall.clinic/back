<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$sectionCode = $arParams["USER_PARAM_SECTION_CODE"];
?>
<? foreach ($arResult["SECTIONS"] as $arSection): ?>
    <? if ($sectionCode && $sectionCode == $arSection["CODE"]): ?>
        <a
            class="price-tabs__item"
            href="<?= $arSection["SECTION_PAGE_URL"] ?>"
        ><strong><?= $arSection["NAME"] ?></strong></a>
    <? else: ?>
        <a
            class="price-tabs__item"
            href="<?= $arSection["SECTION_PAGE_URL"] ?>"
        ><?= $arSection["NAME"] ?></a>
    <? endif ?>
<? endforeach ?>
