<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" class="form__subscribe form" data-wow-delay="0.7s">
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
	<?=bitrix_sessid_post()?>
	<div class="form__subscribe__left wow animated fadeInUp">
		<div class="form__subscribe__title title">
			<p>Получайте акции
				<br/> и спецпредложения</p>
		</div>
		<div class="form__subscribe__desc">
			<p>Выберите способ получения уведомлений</p>
		</div>
	</div>
	<div class="form__subscribe__right wow animated fadeInUp" data-wow-delay="0.5s">
		<div class="form__subscribeInput__wrap">
			<div class="form__subscribeInput__icon">
				<input
				type="email"
				name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_682"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_682"]["STRUCTURE"][0]["ID"]?>"
				placeholder="<?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_682"]["CAPTION"]?>" required>
			</div>
			<label class="form__label">
				<input type="checkbox"
				name="form_checkbox_SIMPLE_QUESTION_579[]"
				value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_579"]["STRUCTURE"][0]["ID"]?>"
				id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_579"]["STRUCTURE"][0]["ID"]?>"
				required>
				<span class="checkbox__box"></span>
				<span class="form__label__text"><?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_579"]["CAPTION"]?>
					<a href="#">с политикой конфеденциальности</a>
				</span>
			</label>
			<input class="button form__subscribe__button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="Подписаться" />
		</div>
		<div class="form__subscribe__checkboxWrap">
			<label class="form__subscribe__checkbox">
				<input type="checkbox"
				name="form_checkbox_SIMPLE_QUESTION_253[]"
				id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_253"]["STRUCTURE"][0]["ID"]?>"
				value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_253"]["STRUCTURE"][0]["ID"]?>">
				<span class="checkbox__subscribe">
					<svg class="checkboxIcon" width="31" height="30">
						<use xlink:href="#whatsapp"></use>
					</svg>
					<span>Whatsapp</span>
				</span>
			</label>
			<label class="form__subscribe__checkbox">
				<input type="checkbox"
				name="form_checkbox_SIMPLE_QUESTION_253[]"
				id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_253"]["STRUCTURE"][1]["ID"]?>"
				value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_253"]["STRUCTURE"][1]["ID"]?>">
				<span class="checkbox__subscribe">
					<svg class="checkboxIcon" width="32" height="30">
						<use xlink:href="#telegram"></use>
					</svg>
					<span>Telegram</span>
				</span>
			</label>
			<label class="form__subscribe__checkbox">
				<input type="checkbox"
				name="form_checkbox_SIMPLE_QUESTION_253[]"
				id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_253"]["STRUCTURE"][2]["ID"]?>"
				value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_253"]["STRUCTURE"][2]["ID"]?>">
				<span class="checkbox__subscribe">
					<svg class="checkboxIcon" width="27" height="30">
						<use xlink:href="#viber"></use>
					</svg>
					<span>Viber</span>
				</span>
			</label>
		</div>
	</div>
</form><?