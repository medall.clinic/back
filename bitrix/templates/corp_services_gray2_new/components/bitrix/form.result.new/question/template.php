<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"){
		if ($arResult["isFormTitle"]){
			?><h3><?=$arResult["FORM_TITLE"]?></h3><?
		}
		if ($arResult["isFormImage"] == "Y"){?>
			<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a><?
		}?>
		<p><?=$arResult["FORM_DESCRIPTION"]?></p><?
	}?>
	<form name="<?=$arResult["WEB_FORM_NAME"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" class="popup__content-form" data-wow-delay="0.7s">
		<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>">
		<?=bitrix_sessid_post()?>
		<input type="text" name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_266"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_266"]["STRUCTURE"][0]["ID"]?>" placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_266"]["CAPTION"]?>" required>
		<input class="phone" type="text" name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_891"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_891"]["STRUCTURE"][0]["ID"]?>" placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_891"]["CAPTION"]?>" required>
		<input type="email" name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_812"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_812"]["STRUCTURE"][0]["ID"]?>" placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_812"]["CAPTION"]?>" required>
		<input type="text" name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_458"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_458"]["STRUCTURE"][0]["ID"]?>" placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_458"]["CAPTION"]?>" required>
		<div class="popup__content-form-date">
			<input type="text" name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_841"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_841"]["STRUCTURE"][0]["ID"]?>" placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_841"]["CAPTION"]?>" required>
		</div>
		<textarea name="form_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_959"]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_959"]["STRUCTURE"][0]["ID"]?>" placeholder="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_959"]["CAPTION"]?>" ></textarea>
		<div class="popup__content-form-block">
			<label class="form__label popup__content-form-label">
				<input type="checkbox"
				name="form_checkbox_SIMPLE_QUESTION_506[]"
				value="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_618"]["STRUCTURE"][0]["ID"]?>"
				id="<?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_618"]["STRUCTURE"][0]["ID"]?>"
				required>
				<span class="checkbox__box"></span>
				<span class="form__label__text"><?echo $arResult["QUESTIONS"]["SIMPLE_QUESTION_618"]["CAPTION"]?>
					<a href="#">с политикой конфеденциальности</a>
				</span>
			</label>
			<div class="popup__content-form-block-button">
				<input class="button popup__content-form-button" type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
			</div>
		</div>
</form><?