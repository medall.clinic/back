<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arSecElItem){
	$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_23_SECTION",$arSecElItem['ID'],"UF_PRICE");
	$pdf = CFile::GetPath($arUF["UF_PRICE"]['VALUE']);
	if(!empty($arSecElItem['ELEMENTS'])){?>
		<div class="pricePage__block">
			<div class="pricePage__block__left">
				<div class="pricePage__block__title title"><?=$arSecElItem['NAME']?></div>
			</div>
			<div class="pricePage__block__right"><?
				if(!empty($arSecElItem['ELEMENTS'])){
					foreach($arSecElItem['ELEMENTS'] as $arItem){?>
						<div class="pricePage__block__link">
							<a class="more openPopup" href="#price<?=$arItem['ID']?>"><?=$arItem["NAME"]?></a>
						</div>
						<!-- попапы Расценок-->
						<div class="popupPrice" id="price<?=$arItem['ID']?>">
							<div class="popup__close">
								<div class="popup__close__line"></div>
								<div class="popup__close__line"></div>
							</div>
							<div class="popupPrice__blockWrapper">
								<div class="popupPrice__title title"><?=$arItem["NAME"]?></div>
								<div class="popupPrice__desc">Прайс</div>
								<div class="popupPrice__text"><?
									foreach($arItem['DISPLAY_PROPERTIES']['service_price']['~VALUE'] as $k => $v){
										$res = CIBlockElement::GetList(Array(), Array("ID"=>$v));
										if ($ob = $res->GetNextElement()){
											$arFields = $ob->GetFields(); // поля элемента
											$arProps = $ob->GetProperties(); // свойства элемента
											?>
											<div class="popupPrice__block">
												<div class="popupPrice__block__left"><p><?=$arFields['NAME']?></p></div>
												<div class="popupPrice__block__right">
													<p>
														<?if($arProps['old_price']['VALUE']!=''){?>
															<span class="lineThrough"><?=$arProps['old_price']['VALUE']?></span><?
														}?>
														<?=$arProps['new_price']['VALUE']?>
													</p>
												</div>
											</div><?
										}
									}?>
								</div>
								<div class="popupPrice__attention">
									<?=$arItem['DISPLAY_PROPERTIES']['reminder_price']['~VALUE']['TEXT']?>
								</div>
								
								<?if($pdf!=''){?>
									<div class="popupPrice__buttonWrap">
										<a class="button popupPrice__button" href="<?=$pdf?>" target="_blank">Смотреть полный прайс</a>
									</div><?
								}?>
							</div>
						</div><?
					}
				}?>
			</div>
		</div><?
	}
}?>