<? foreach($arResult["ITEMS"] as $section): ?>

    <? if($section["DEPTH_LEVEL"] == 1): ?>
        <h2 class="price-section"><?= $section["NAME"] ?></h2>
    <? endif ?>

    <? if($section["DEPTH_LEVEL"] == 2): ?>
        <h3 class="price-category"><?= $section["NAME"] ?></h3>
	<? endif ?>

    <? if($section["DEPTH_LEVEL"] == 3): ?>
        <h4 class="price-service"><?= $section["NAME"] ?></h4>
	<? endif ?>

    <? if(!empty($section["ELEMENTS"])): ?>
        <? foreach ($section["ELEMENTS"] as $element): ?>
            <div class="price-block">

                <div class="price-block__left"><?= $element["NAME"] ?></div>

                <div class="price-block__right">
                <? $price = number_format($element["PROPERTIES"]["price"]["VALUE"], 0, ",", " ") ?>
                <? if(empty($element["PROPERTIES"]["sale_price"]["VALUE"])): ?>
                    от <?= $price ?> руб.
                <? else: ?>
					<? $salePrice = number_format($element["PROPERTIES"]["sale_price"]["VALUE"], 0, ",", " ") ?>
                    <span class="price-block__old-price"><?= $price ?> руб.</span>
                    <?= $salePrice ?> руб.
                <? endif ?>
                </div>

            </div>
        <? endforeach ?>
    <? endif ?>

<? endforeach ?>