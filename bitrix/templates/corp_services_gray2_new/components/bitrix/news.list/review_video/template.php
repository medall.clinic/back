<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="reviewPage__videoReview__block">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
		<div class="reviewPage__videoReview__item">
			<div class="reviewPage__videoReview__video" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
			<div class="reviewPage__videoReview__desc">
				<p>Процедура: <?echo $arItem["NAME"]?></p>
				<p>Врач: <?=$arItem["DISPLAY_PROPERTIES"]['doctor_review']['DISPLAY_VALUE']?></p>
			</div>
		</div>
	<?endforeach;?>
</div>
<div class="reviewPage__buttonWrap">
	<a class="reviewPage__button button" href="review_video">Смотреть все</a>
</div>