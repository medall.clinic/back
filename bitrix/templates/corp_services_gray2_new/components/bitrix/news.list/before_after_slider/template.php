<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="beforeAfterPage__block">
<? foreach($arResult["ITEMS"] as $arItem): ?>
    <?
    // определяемся с процедурой
    $Procedure = '';
    if($arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['VALUE']]['NAME']){
        $Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['VALUE']]['NAME'];
    }
    elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['VALUE']]['NAME']){
        $Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['VALUE']]['NAME'];
    }
    elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['VALUE']]['NAME']){
        $Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['VALUE']]['NAME'];
    }
    elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['VALUE']]['NAME']){
        $Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['VALUE']]['NAME'];
    }
    elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['VALUE']]['NAME']){
        $Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['VALUE']]['NAME'];
    }
    $doctorNAME = '';
    $doctorio_doctor = '';
    $arFilter = Array("ID"=>$arItem["DISPLAY_PROPERTIES"]['Doctor_before_after']['VALUE']);
    $res = CIBlockElement::GetList(Array(), $arFilter);
    if ($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields(); // поля элемента
        $arProps = $ob->GetProperties(); // свойства элемента
        $doctorNAME = $arFields['NAME'];
        $doctorio_doctor = $arProps['io_doctor']['VALUE'];
    }
    ?>
    <div class="beforeAfterPage__item beforeAfterPage__item__wrapper">

		<? if(!empty($arItem["PROPERTIES"]["photos"]["VALUE"])): ?>
            <div class="beforeAfterPage__slider-wrapper">
                <div class="beforeAfterPage__slider">
		            <? foreach ($arItem["PROPERTIES"]["photos"]["VALUE"] as $photoId): ?>
                    <a
                        class="beforeAfterPage__img openPopup"
                        href="#bx_<?= $arItem["ID"] ?>"
                    >
                        <div
                            class="beforeAfterPage__slide"
                            style="background-image: url(<?= CFile::GetPath($photoId) ?>)">
                        </div>
                    </a>
					<? endforeach ?>
                </div>
            </div>
        <? endif ?>

        <div class="beforeAfterPage__content">
            <div class="beforeAfterPage__text">
                <p>
                    <span class="beforeAfterPage__text__title">Процедура:</span>
                    <span class="green"><?= $Procedure ?></span>
                </p>
                <p>
                    <span class="beforeAfterPage__text__title">Врач:</span>
                    <span class="green"><?= $doctorNAME ?> <?= $doctorio_doctor ?></span>
                </p>
            </div>
        </div>

    </div>

    <!--Попапы-->
    <div class="popupBeforeAfter" id="bx_<?= $arItem["ID"] ?>">

        <div class="popup__close">
            <div class="popup__close__line"></div>
            <div class="popup__close__line"></div>
        </div>

        <div class="popupBeforeAfter__block beforeAfterPage__item__wrapper">

            <? if(!empty($arItem["PROPERTIES"]["photos"]["VALUE"])): ?>
                <div class="popupBeforeAfter__slider-wrapper">
                    <div class="popupBeforeAfter__slider">
                        <? foreach ($arItem["PROPERTIES"]["photos"]["VALUE"] as $photoId): ?>
                            <div class="popupBeforeAfter__img">
                                <span style="background-image: url(<?= CFile::GetPath($photoId) ?>)"></span>
                            </div>
                        <? endforeach ?>
                    </div>
                </div>
            <? endif ?>

            <div class="popupBeforeAfter__text">

                <div class="popupBeforeAfter__textBlock">
                    <div class="popupBeforeAfter__textBlock__left">
                        <p>
                            <span class="beforeAfterPage__text__title">Процедура:</span>
                            <span class="green"><?= $Procedure ?></span>
                        </p>
                    </div>
                    <div class="popupBeforeAfter__textBlock__right">
                        <p>
                            <span class="beforeAfterPage__text__title">Врач:</span>
                            <span class="green"><?= $doctorNAME ?> <?= $doctorio_doctor ?></span>
                        </p>
                    </div>
                </div>

                <? if(!empty($arItem["DISPLAY_PROPERTIES"]['review_before_after']['VALUE'])): ?>
                <div class="popupBeforeAfter__textdesc">
                    <p>
                        <span class="beforeAfterPage__text__title">Отзыв пациента:</span>
                        <?
                        $arFilter = Array("ID"=>$arItem["DISPLAY_PROPERTIES"]['review_before_after']['VALUE']);
                        $res = CIBlockElement::GetList(Array(), $arFilter);
                        if ($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields(); // поля элемента
                            echo $arFields['DETAIL_TEXT'];
                        }
                        ?>
                    </p>
                </div>
                <? endif ?>

            </div> <!-- popupBeforeAfter__text -->

        </div> <!-- popupBeforeAfter__block beforeAfterPage__item__wrapper -->

    </div> <!-- popupBeforeAfter -->
<? endforeach ?>
</div> <!-- beforeAfterPage__block -->

<? if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<div class="paginator faqPage__paginator">
		<?= $arResult["NAV_STRING"] ?>
	</div>
<? endif ?>