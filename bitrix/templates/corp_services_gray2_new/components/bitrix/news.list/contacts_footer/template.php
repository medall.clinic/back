<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<section class="footer-contacts contacts">
	<div class="contacts__left rbs">
		<div class="contacts__contentWrap wow animated fadeIn" data-wow-delay="0.3s">
			<div class="contacts__title title beforeLine">
				<h2>Контакты</h2>
			</div>
			<div class="contacts__tabs__content">
				<div class="contacts__tabs__content__item active" itemscope="" itemtype="http://schema.org/Organization">
						<? /* <h3>Клиника эстетической медицины и цифровой стоматологии MEDALL</h3> */ ?>
						<h3>Внимание! С 1 июля городская парковка на Левашовском проспекте и Петроградском острове стала платной. Тариф - 100 руб./час. Вы можете воспользоваться внутренней парковкой клиники при наличии свободных мест.</h3>
						<a class="contacts__tabs__content__phone" href="tel:+78126030201" itemprop="telephone">+7 (812) 6&zwj;03-02-01</a>
						<a class="contacts__tabs__content__email" href="mailto:admin@medall.clinic" itemprop="email">admi&zwj;n@medall.clinic</a>
						<h3>Учебный центр GROSSMASTER</h3>
						<a class="contacts__tabs__content__phone" href="tel:+78126606145" itemprop="telephone">+7 (812) 660-61-45</a>
						<a class="contacts__tabs__content__email" href="mailto:grossmaster@medall.clinic" itemprop="email">grossmaster@medall.clinic</a>
						<div class="contacts__tabs__content__address" itemprop="address">Левашовский пр., д.24, 500 м от ст. Чкаловская</div>
					</div>
			</div>
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "social_network", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "24",	// Код информационного блока
		"IBLOCK_TYPE" => "social_network",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "20",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "network_img",
			1 => "network_link",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		),
		false
	);?>
	<p>В других социальных сетях вы всегда нас сможете найти по никнейму <strong>medall.clinic</strong> </p>
			<div class="contacts__tabs__wrap" style="display: none">
				<?$arResult2 = array();
				$Address = array();
				foreach($arResult["ITEMS"] as $arItem){$arResult2[$arItem["IBLOCK_SECTION_ID"]][]=$arItem;}?>
				<div class="contacts__tabs"><?
					$r=0;
					foreach($arResult2 as $k => $arItem){
						$r++;
						$act='';
						if($r==1){$act='active';}
						$res = CIBlockSection::GetByID($k);
						if($ar_res = $res->GetNext()){?>
							<div class="contacts__tabs__item <?=$act?>">
								<a class="contacts__tabs__link" href="#"><?=$ar_res['NAME']?></a>
							</div><?
						}
					}?>
				</div>
				<div class="contacts__tabs__content">
					<?$r=0;
					foreach($arResult2 as $k => $arItem){
						$r++;
						$act='';
						if($r==1){$act='active';}?>
						<div class="contacts__tabs__content__item <?=$act?>">
							<?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_8_SECTION",$k,"UF_PHONE");
							if($arUF["UF_PHONE"]["VALUE"]!= ""){?>
								<a class="contacts__tabs__content__phone" href="tel:<?=$arUF["UF_PHONE"]["VALUE"]?>"><?=$arUF["UF_PHONE"]["VALUE"]?></a><?
							}
							$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_8_SECTION",$k,"UF_EMAIL");
							if($arUF["UF_EMAIL"]["VALUE"]!= ""){?>
								<a class="contacts__tabs__content__email" href="mailto:<?=$arUF["UF_EMAIL"]["VALUE"]?>"><?=$arUF["UF_EMAIL"]["VALUE"]?></a><?
							}
							foreach($arItem as $j){
								$tmp = explode(",", $j["DISPLAY_PROPERTIES"]['Address']['VALUE']);
								$point = array();
								$point["cord"] = Array((float)$tmp[0],(float)$tmp[1]);
								$point["balloon"] = $j['NAME'];

								$Address[] = $point;

								$this->AddEditAction($j['ID'], $j['EDIT_LINK'], CIBlock::GetArrayByID($j["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($j['ID'], $j['DELETE_LINK'], CIBlock::GetArrayByID($j["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
								<div class="contacts__tabs__content__address"><?=$j['NAME']?></div><?
							}?>
						</div><?
					}?>
				</div>
			</div>
		</div>
	</div>
	<div class="contacts__right">
		<div class="map" id="map"></div>
	</div>
</section>
<script type="text/javascript">window.map_addresses = <?echo json_encode($Address)?></script>
