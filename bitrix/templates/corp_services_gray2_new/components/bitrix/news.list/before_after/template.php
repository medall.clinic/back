<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="beforeAfterPage__block">
<?foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
	// определяемся с процедурой
	$Procedure = '';
	if($arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['VALUE']]['NAME']){
		$Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_plastic_before_after']['VALUE']]['NAME'];
	}
	elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['VALUE']]['NAME']){
		$Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_cosmetology_before_after']['VALUE']]['NAME'];
	}
	elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['VALUE']]['NAME']){
		$Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_phlebology_before_after']['VALUE']]['NAME'];
	}
	elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['VALUE']]['NAME']){
		$Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_hair_transplant_before_after']['VALUE']]['NAME'];
	}
	elseif($arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['VALUE']]['NAME']){
		$Procedure = $arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['LINK_ELEMENT_VALUE'][$arItem["DISPLAY_PROPERTIES"]['Procedure_dentistry_before_after']['VALUE']]['NAME'];
	}
	$doctorNAME = '';
	$doctorio_doctor = '';
	$arFilter = Array("ID"=>$arItem["DISPLAY_PROPERTIES"]['Doctor_before_after']['VALUE']);
	$res = CIBlockElement::GetList(Array(), $arFilter);
	if ($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields(); // поля элемента
		$arProps = $ob->GetProperties(); // свойства элемента
		$doctorNAME = $arFields['NAME'];
		$doctorio_doctor = $arProps['io_doctor']['VALUE'];
	}
	
	$one = '';
	$two = '';
	$three = '';
	
	$oneactive = 'active';
	$twoactive = '';
	$threeactive = '';
	
	if($arItem["DISPLAY_PROPERTIES"]['fas_before_after']['FILE_VALUE']['SRC']==''){
		$one = 'style="display:none;"';
		$oneactive = '';
		$twoactive = 'active';
	}
	if($arItem["DISPLAY_PROPERTIES"]['threequarters_before_after']['FILE_VALUE']['SRC']==''){
		$two = 'style="display:none;"';
		$twoactive = '';
		$threeactive = 'active';
	}
	if($arItem["DISPLAY_PROPERTIES"]['Profile_before_after']['FILE_VALUE']['SRC']==''){
		$three = 'style="display:none;"';
		$threeactive = '';
	}?>
	<!--Попапы-->
	<div class="popupBeforeAfter" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="popup__close">
			<div class="popup__close__line"></div>
			<div class="popup__close__line"></div>
		</div>
		<div class="popupBeforeAfter__block beforeAfterPage__item__wrapper">
			<div class="popupBeforeAfter__img">
				<span class="beforeAfterPage__img__block beforeAfterPage__img__one <?=$oneactive?>" style="background-image:url(<?=$arItem["DISPLAY_PROPERTIES"]['fas_before_after']['FILE_VALUE']['SRC']?>);"></span>
				<span class="beforeAfterPage__img__block beforeAfterPage__img__two <?=$twoactive?>" style="background-image:url(<?=$arItem["DISPLAY_PROPERTIES"]['threequarters_before_after']['FILE_VALUE']['SRC']?>);"></span>
				<span class="beforeAfterPage__img__block beforeAfterPage__img__three <?=$threeactive?>" style="background-image:url(<?=$arItem["DISPLAY_PROPERTIES"]['Profile_before_after']['FILE_VALUE']['SRC']?>);"></span>
			</div>
			<div class="popupBeforeAfter__text">
				<div class="beforeAfterPage__tabs">
					<a <?=$one?> class="beforeAfterPage__tab beforeAfterPage__tab--one <?=$oneactive?>" href="#"><?=$arItem["DISPLAY_PROPERTIES"]['fas_text_before_after']['VALUE']?></a>
					<a <?=$two?> class="beforeAfterPage__tab beforeAfterPage__tab--two <?=$twoactive?>" href="#"><?=$arItem["DISPLAY_PROPERTIES"]['threequarters_text_before_after']['VALUE']?></a>
					<a <?=$three?> class="beforeAfterPage__tab beforeAfterPage__tab--three <?=$threeactive?>" href="#"><?=$arItem["DISPLAY_PROPERTIES"]['Profile_text_before_after']['VALUE']?></a>
				</div>
				<div class="popupBeforeAfter__textBlock">
					<div class="popupBeforeAfter__textBlock__left">
						<p>
							<span class="beforeAfterPage__text__title">Процедура:</span>
							<span class="green"><?=$Procedure?></span>
						</p>
						<p>
							<span class="beforeAfterPage__text__title">Доп коррекция:</span>
							<span class="green"><?=$arItem["DISPLAY_PROPERTIES"]['Correction_before_after']['VALUE']?></span>
						</p>
					</div>
					<div class="popupBeforeAfter__textBlock__right">
						<p>
							<span class="beforeAfterPage__text__title">Период:</span>
							<span><?=$arItem["DISPLAY_PROPERTIES"]['Period_before_after']['VALUE']?></span>
						</p>
						<p>
							<span class="beforeAfterPage__text__title">Врач:</span>
							<span class="green"><?=$doctorNAME?> <?=$doctorio_doctor?></span>
						</p>
					</div>
				</div>
				<div class="popupBeforeAfter__textdesc">
					<p>
						<span class="beforeAfterPage__text__title">Отзыв пациента:</span>
						<?$arFilter = Array("ID"=>$arItem["DISPLAY_PROPERTIES"]['review_before_after']['VALUE']);
						$res = CIBlockElement::GetList(Array(), $arFilter);
						if ($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields(); // поля элемента
							$arProps = $ob->GetProperties(); // свойства элемента
							echo $arFields['DETAIL_TEXT'];
						}?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="beforeAfterPage__item beforeAfterPage__item__wrapper">
		<a class="beforeAfterPage__img openPopup" href="#<?=$this->GetEditAreaId($arItem['ID']);?>">
			<span class="beforeAfterPage__img__block beforeAfterPage__img__one <?=$oneactive?>" style="background-image:url(<?=$arItem["DISPLAY_PROPERTIES"]['fas_before_after']['FILE_VALUE']['SRC']?>);"></span>
			<span class="beforeAfterPage__img__block beforeAfterPage__img__two <?=$twoactive?>" style="background-image:url(<?=$arItem["DISPLAY_PROPERTIES"]['threequarters_before_after']['FILE_VALUE']['SRC']?>);"></span>
			<span class="beforeAfterPage__img__block beforeAfterPage__img__three <?=$threeactive?>" style="background-image:url(<?=$arItem["DISPLAY_PROPERTIES"]['Profile_before_after']['FILE_VALUE']['SRC']?>);"></span>
		</a>
		<div class="beforeAfterPage__content">
			<div class="beforeAfterPage__tabs">
				<a <?=$one?> class="beforeAfterPage__tab beforeAfterPage__tab--one <?=$oneactive?>" href="#"><?=$arItem["DISPLAY_PROPERTIES"]['fas_text_before_after']['VALUE']?></a>
				<a <?=$two?> class="beforeAfterPage__tab beforeAfterPage__tab--two <?=$twoactive?>" href="#"><?=$arItem["DISPLAY_PROPERTIES"]['threequarters_text_before_after']['VALUE']?></a>
				<a <?=$three?> class="beforeAfterPage__tab beforeAfterPage__tab--three <?=$threeactive?>" href="#"><?=$arItem["DISPLAY_PROPERTIES"]['Profile_text_before_after']['VALUE']?></a>
			</div>
			<div class="beforeAfterPage__text">
				<p>
					<span class="beforeAfterPage__text__title">Процедура:</span>
					<span class="green"><?=$Procedure?></span>
				</p>
				<p>
					<span class="beforeAfterPage__text__title">Врач:</span>
					<span class="green"><?=$doctorNAME?> <?=$doctorio_doctor?></span>
				</p>
				<p>
					<span class="beforeAfterPage__text__title">Период:</span>
					<span><?=$arItem["DISPLAY_PROPERTIES"]['Period_before_after']['VALUE']?></span>
				</p>
			</div>
		</div>
	</div>
<?}?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="paginator faqPage__paginator">
		<?=$arResult["NAV_STRING"]?>
	</div>
<?endif;?>