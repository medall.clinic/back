<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arResult2=array();
foreach($arResult["ITEMS"] as $arItem){
	$arResult2[$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM_ID']]['NAME']=$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM'];
	$arResult2[$arItem['PROPERTIES']['direction_doctor']['VALUE_ENUM_ID']]['ITEMS'][]=$arItem;
}
foreach($arResult2 as $arItem2){
	?><div class="doctorPage__title greyTitle"><?=$arItem2['NAME']?></div>
<!--test-->
	<div class="doctorPage__block"><?
		foreach($arItem2["ITEMS"] as $arItem){?>
			<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
			<div class="doctorPage__item">
				<a class="doctorPage__img" href="<?=$arItem['DETAIL_PAGE_URL']?>">
          <picture>
            <source srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>" type="image/webp">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="картинка" loading="lazy" decoding="async">
          </picture>
				</a>
				<a class="doctorPage__name" href="<?=$arItem['DETAIL_PAGE_URL']?>">
					<span><?echo $arItem["NAME"]?></span><?=$arItem['PROPERTIES']['io_doctor']['VALUE']?></a>
				<div class="doctorPage__work">
					<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
						<p><?echo $arItem["PREVIEW_TEXT"];?></p>
					<?endif;?>
				</div>
			</div>
		<?}?>
	</div>
<?}?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>