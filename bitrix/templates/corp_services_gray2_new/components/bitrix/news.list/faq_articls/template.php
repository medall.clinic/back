<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
	
	<div class="mainContent__text__currentArticles__item articlesCardBlock__item">
		<a class="mainContent__text__currentArticles__item__img" href="<?echo $arItem["DETAIL_PAGE_URL"]?>/" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
		<div class="mainContent__text__currentArticles__item__desc">
			<?if($arItem['PROPERTIES']['current_articles']['VALUE']!=''){
				?><p class="mainContent__text__currentArticles__item__desc--p">Актуальные статьи</p><?
			}?>
			<a class="mainContent__text__currentArticles__item__desc__title mainContent__text__title" href="<?echo $arItem["DETAIL_PAGE_URL"]?>/"><?echo $arItem["NAME"]?></a>
			<div class="articlesBlock__item__hashTag__wrap">
				<a class="articlesBlock__item__hashTag" href="/search/index.php?tags=<?=$arItem["TAGS"]?>" target=_blank># <?echo $arItem["TAGS"]?></a>
			</div>
			<a class="mainContent__text__currentArticles__item__desc__link more" href="<?echo $arItem["DETAIL_PAGE_URL"]?>/">Читать</a>
		</div>
	</div>
<?endforeach;?>
