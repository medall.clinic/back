<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$j=0;
foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$j++;
	if($j == 1){
		?><div class="reviewPage__videoReview__block"><?
	}?>
	<div class="reviewPage__videoReview__item"><?
		if($arItem["DISPLAY_PROPERTIES"]["load_video_review"]["FILE_VALUE"]["SRC"]){?>
			<div class="reviewPage__videoReview__video">
				<video style="width:100%; height:100%;" src="<?=$arItem["DISPLAY_PROPERTIES"]["load_video_review"]["FILE_VALUE"]["SRC"]?>" controls></video>
			</div><?
		}
		elseif($arItem["DISPLAY_PROPERTIES"]["video_review"]["DISPLAY_VALUE"]){?>
			<div class="reviewPage__videoReview__video">
				<?=$arItem["DISPLAY_PROPERTIES"]["video_review"]["DISPLAY_VALUE"]?>
			</div><?
		}
		else{?>
			<div class="reviewPage__videoReview__video" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div><?
		}?>
		<div class="reviewPage__videoReview__desc">
			<p>Процедура: <?echo $arItem["NAME"]?></p>
			<p>Врач: <?=$arItem["DISPLAY_PROPERTIES"]['doctor_review']['DISPLAY_VALUE']?></p>
		</div>
	</div>
	<?if($j == 3){
		?></div><?
		$j=0;
	}
}?>