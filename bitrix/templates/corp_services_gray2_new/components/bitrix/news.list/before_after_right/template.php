<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
<section class="mainContent__ArticleBlock mainContent__ArticleBlock__beforeAfter">
	<div class="mainContent__ArticleBlock__title mainContent__text__title">До и после</div>
	<? foreach($arResult["ITEMS"] as $arItem): ?>
        <? if(!empty($arItem["PROPERTIES"]["photos"]["VALUE"])): ?>
		<div class="mainContent__ArticleBlock__beforeImg">
			<img src="<?= CFile::GetPath(reset($arItem["PROPERTIES"]["photos"]["VALUE"])) ?>" alt="">
		</div>
        <? endif ?>
    <? endforeach ?>
	<a class="more" href="/before_after/">Перейти в галерею</a>
</section>