<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem):?>
	<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
	<div class="reviews__slider__item">
		<div class="reviews__slider__item__img">
			<a class="reviews__slider__item__img__link" href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" data-fancybox="gallery"> <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="Отзыв" title="Отзыв"> </a>
		</div>
		<div class="reviews__slider__item__doctor"><p>Врач: <?=$arItem["DISPLAY_PROPERTIES"]['doctor_review']['DISPLAY_VALUE']?></p></div>
		<div class="reviews__slider__item__text"><p><?echo $arItem["PREVIEW_TEXT"];?></p></div>
	</div>
<?endforeach;?>