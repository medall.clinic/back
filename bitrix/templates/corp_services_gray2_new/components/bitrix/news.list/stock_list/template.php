<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem){?>
	<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$stdop = '';
	if($arItem["DISPLAY_PROPERTIES"]['bg_action']['VALUE_XML_ID'] == "dark"){
		$stdop = 'discountBreastPlastic__item--revese';
	}
	?>
	<div class="discountBreastPlastic__item <?=$stdop?>">
		<div class="discountBreastPlastic__item__bgImg" style="background-image: url(<?=$arItem["DISPLAY_PROPERTIES"]['img_bg_action']['FILE_VALUE']['SRC']?>)"></div>
		<div class="container">
			<div class="discountBreastPlastic__block">
				<div class="discountBreastPlastic__right">
					<div class="discountBreastPlastic__img">
						<img src="<?=$arItem["DISPLAY_PROPERTIES"]['img_main_action']['FILE_VALUE']['SRC']?>" alt="картинка">
					</div>
				</div>
				<div class="discountBreastPlastic__left">
					<div class="discountBreastPlastic__category"><?=$arItem["DISPLAY_PROPERTIES"]['direction_action']['VALUE']?></div>
					<div class="discountBreastPlastic__title title beforeLine">
						<p><?=$arItem["DISPLAY_PROPERTIES"]['title_action']['~VALUE']['TEXT']?></p>
					</div>
					<div class="discountBreastPlastic__month">
						<p><?=$arItem['DATE_ACTIVE_FROM']?> - <?=$arItem['DATE_ACTIVE_TO']?></p>
					</div>
					<a class="discountBreastPlastic__button button" href="<?=$arItem['DETAIL_PAGE_URL']?>">Узнать подробнее</a>
				</div>
			</div>
		</div>
	</div>
<?}?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="paginator">
		<?=$arResult["NAV_STRING"]?>
	</div>
<?endif;?>