<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
  <? /*
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
  */ ?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	
	<div class="downloadLinks">
		<?foreach($arResult["DISPLAY_PROPERTIES"]['fire_document']['FILE_VALUE'] as $a=>$d){
			$r = explode(".", $d['SRC']);?>
			<div class="downloadLinks__block">
				<a class="downloadLink" href="<?=$d['SRC']?>" download>
					<span class="green"><?=$d['ORIGINAL_NAME']?></span>
					<span class="small">(<?=end($r);?>, <?echo CFile::FormatSize($d['FILE_SIZE'])?>)</span>
				</a>
			</div>
		<?}?>
	</div>

    <? if($arResult["DISPLAY_PROPERTIES"]["licenses_document"]["VALUE"]): ?>
	<div class="documentsPage__title greyTitle">Лицензии клиники</div>
	<div class="documentsBlockSlider">
		<div class="documentsSlider licensesSlider">
			<? foreach($arResult["DISPLAY_PROPERTIES"]["licenses_document"]["VALUE"] as $licenseId): ?>
                <? $licenseSrc = CFile::GetPath($licenseId) ?>
				<div class="documentsSlider__item">
					<a class="documentsSlider__item__link" href="<?= $licenseSrc ?>" data-fancybox="licenses">
						<img src="<?= $licenseSrc ?>" alt="Лицензия" title="Лицензия">
					</a>
				</div>
			<? endforeach ?>
		</div>
		<div class="documentsSlider__controls">
			<div class="documentsSlider__control documentsSlide__prev licensesSlider__prev"></div>
			<div class="documentsSlider__control documentsSlide__next licensesSlider__next"></div>
		</div>
	</div>
    <? endif ?>

    <? if($arResult["DISPLAY_PROPERTIES"]["diplomas_document"]["VALUE"]): ?>
	<div class="documentsPage__title greyTitle">Дипломы</div>
	<div class="documentsBlockSlider">
		<div class="documentsSlider diplomasSlider">
			<? foreach($arResult["DISPLAY_PROPERTIES"]["diplomas_document"]["VALUE"] as $diplomaId): ?>
				<? $diplomaSrc = CFile::GetPath($diplomaId) ?>
				<div class="documentsSlider__item">
					<a class="documentsSlider__item__link" href="<?= $diplomaSrc ?>" data-fancybox="diplomas">
						<img src="<?= $diplomaSrc ?>" alt="Документ" title="Документ">
					</a>
				</div>
			<? endforeach ?>
		</div>
		<div class="documentsSlider__controls">
			<div class="documentsSlider__control documentsSlide__prev diplomasSlider__prev"></div>
			<div class="documentsSlider__control documentsSlide__next diplomasSlider__next"></div>
		</div>
	</div>
    <? endif ?>

</div>
