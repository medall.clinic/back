<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="mainContent__title__desc"><p>Статьи</p></div>

<div class="content">
    <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
            <h1><?=$arResult["NAME"]?></h1>
    <?endif;?>
    <div class="grid grid--justify--space-between page-subsection article">
        <div class="grid__cell grid__cell--m--8 article__inner">
            <div class="article__header">
                <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
                    <div class="article__date">
                        <p><?=$arResult["DISPLAY_ACTIVE_FROM"]?></p>
                    </div>
                <div class="article__header-text">
                    <p><?echo $arResult["PREVIEW_TEXT"];?></p>
                </div>
                <?endif;?>
            </div>

            <div class="article__body">

                <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="картинка" title="Грудные импланты">
                <?echo $arResult["DETAIL_TEXT"];?>

                <?$res = CIBlockElement::GetByID($arResult['PROPERTIES']['author_articles']['VALUE']);
                if($ar_res = $res->GetNext()){
                    $db_props = CIBlockElement::GetProperty(6, $arResult['PROPERTIES']['author_articles']['VALUE'], array("sort" => "asc"));
                    if($ar_props = $db_props->Fetch()){?>
                        <div class="articleCardBlock__author">
                        <p class="articleCardBlock__author__title">Автор статьи:</p>
                        <p class="articleCardBlock__author__name">
                            <span><?echo $ar_res['NAME']?></span> <?echo $ar_props['VALUE']?> </p>
                        <p class="articleCardBlock__author__desc"><?echo $ar_res['PREVIEW_TEXT']?></p>
                        </div><?
                    }
                }?>
                <div class="article__footer">
                    <div class="article__hashtag">
                        <?foreach($arResult["FIELDS"] as $code=>$value):?>
                            <a href="/search/index.php?tags=<?=$value?>" target=_blank># <?echo $value;?></a>
                        <?endforeach;?>
                    </div>
                    <a class="article__back" href="<?=$arResult["LIST_PAGE_URL"]?>">Назад</a>
                </div>

            </div>
        </div>
    </div>
</div>
