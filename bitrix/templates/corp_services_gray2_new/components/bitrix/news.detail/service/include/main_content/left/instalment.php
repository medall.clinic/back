<div class="block_installment">
	<div class="mainContent__wrap">
		<div class="wrapper">
			<div class="icon"><img src="/bitrix/templates/corp_services_gray2_new/img/mess/dentistry/discount.png" alt=""></div>
			<div class="title">Мы предлагаем рассрочку</div>
			<div class="text">Для своих пациентов мы предлагаем специальную программу оплаты услуг — беспроцентная рассрочка</div>
			<div class="btn-wrapper"><a class="buttonTwo" href="/rassrochka/">Подробнее</a></div>
		</div>
	</div>
</div>

<? if($installment_add_content): ?>
    <div class="installment_add_content">
		<?= $installment_add_content ?>
    </div>
<? endif ?>
