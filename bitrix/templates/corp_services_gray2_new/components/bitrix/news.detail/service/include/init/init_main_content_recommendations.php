<?
$recommendationIds = !empty($displayProperties["recommendations_items"]["VALUE"]) ? $displayProperties["recommendations_items"]["VALUE"] : null;

$recommendations = [];

if($recommendationIds){
	$res = CIBlockElement::GetList([], ["ID" => $recommendationIds]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$recommendations[] = [
			"title" => $fields["NAME"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"text" => $fields["~PREVIEW_TEXT"],
		];
	}
}

$recommendations_add_content = !empty($displayProperties["recommendations_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["recommendations_add_content"]["DISPLAY_VALUE"] : null;