<? if ($recommendations): ?>
        <div class="service_page-section">
            <div class="block-marked">
            <? foreach ($recommendations as $recommendation): ?>
                <div class="block-icon block-icon--marked-content-light block-marked__content block-marked__content--icon">
					<? if($recommendation["img"]): ?>
                        <div class="block-icon__icon">
                            <img src="<?= $recommendation["img"] ?>" alt=""/>
                        </div>
					<? endif ?>
                    <div class="block-icon__body">
                        <h3 class="block-icon__header"><?= $recommendation["title"] ?></h3>
                        <div class="block-icon__text">
							<?= $recommendation["text"] ?>
                        </div>
                    </div>
                </div>
            <? endforeach ?>
            </div>
        </div>
<? endif ?>

<? if ($recommendations_add_content): ?>
    <div class="recommendations_add_content">
		<?= $recommendations_add_content ?>
    </div>
<? endif ?>
