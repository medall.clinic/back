<? if($departmentInfo): ?>
<div class="dep-info">
  <div class="service_grid service_grid--justify-center ">
    <div class="service_grid__cell service_grid__cell--m-8 service_grid__cell--s-10 service_grid__cell--12 ">
      <div class="service_grid service_grid--padding-y ">
        <div class="service_grid__cell service_grid__cell--m-4 service_grid__cell--12 dep-info__text">
          <? include __DIR__."/text.php" ?>
        </div>
        <div class="service_grid__cell service_grid__cell--m-8 service_grid__cell--12 dep-info__gallery">
          <div class="service_grid service_grid--padding-y ">
            <? foreach($departmentInfo["slides"] as $slide): ?>
              <? include __DIR__."/slide.php" ?>
            <? endforeach ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<? endif ?>

<? if($department_info_add_content): ?>
    <div class="department_info_add_content">
		<?= $department_info_add_content ?>
    </div>
<? endif ?>
