<? /*<? if($generalDesc): ?>
<div class="mainContent__block">

    <div class="container g-max-width-800 g-pa-0 text-center">

        <h2><?= $generalDesc["title"] ?></h2>

        <? if($generalDesc["text"]): ?>
        <div class="landing-block-node-text g-font-size-16 g-py-30 u-heading-v2-8--bottom u-heading-v2-8--top g-brd-primary">
			<?= $generalDesc["text"] ?>
        </div>
        <? endif ?>

    </div>

    <? if($generalDesc["items"]): ?>
    <section class="landing-block g-pb-0 g-bg-main g-pt-20">
        <div class="container">
            <div class="row landing-block-inner">

                <? foreach($generalDesc["items"] as $item): ?>
                    <? include __DIR__."/general_desc-item.php" ?>
                <? endforeach ?>

            </div>
        </div>
    </section>
	<? endif ?>

</div>
<? endif ?> */ ?>
<? if($generalDesc): ?>
  <div class="service_page-section service-headline">

    <? if($generalDesc["title"]): ?>
        <h2 class="service_page-section__subheader">
          <?= $generalDesc["title"] ?>
        </h2>
    <? endif ?>

    <? if($generalDesc["text"]): ?>
        <div class="service_page-subsection">
            <div class="block-quote"><?= $generalDesc["text"] ?></div>
        </div>
    <? endif ?>

    <? if($generalDesc["items"]): ?>
      <div class="service_page-subsection">
        <div class="service_grid service_grid--padding-y ">
          <? foreach($generalDesc["items"] as $item): ?>
              <? include __DIR__."/general_desc-item.php" ?>
          <? endforeach ?>
        </div>
      </div>
    <? endif ?>

  </div>
<? endif ?>

<? if($generalDesc_add_content): ?>
    <div class="generalDesc_add_content">
		<?= $generalDesc_add_content ?>
    </div>
<? endif ?>
