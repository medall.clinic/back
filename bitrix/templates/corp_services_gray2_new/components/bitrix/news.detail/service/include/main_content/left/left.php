<div class="mainContent__left">

	<div class="mainContent__wrap">
        <? foreach($vertSortBlocks as $block): ?>
            <? $path = __DIR__."/".$block["file_name"].".php" ?>
            <? if(file_exists($path)): ?>
				<? include $path ?>
            <? endif ?>
        <? endforeach ?>
	</div>

	<? include __DIR__."/instalment.php" ?>

</div>
