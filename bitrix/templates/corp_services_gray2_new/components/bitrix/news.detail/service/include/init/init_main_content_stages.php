<?
$stageIds = !empty($displayProperties["stages_items"]["VALUE"]) ? $displayProperties["stages_items"]["VALUE"] : null;

$stages = [];

if($stageIds){
	$res = CIBlockElement::GetList([], ["ID" => $stageIds]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$stages[] = [
			"title" => $fields["NAME"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"text" => $fields["~PREVIEW_TEXT"],
		];
	}
}

$stages_add_content = !empty($displayProperties["stages_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["stages_add_content"]["DISPLAY_VALUE"] : null;