<? /** @var array $item */ ?>

<div class="service_grid__cell service_grid__cell--m-<?= ($item["is_width_wide"]) ? "12" : "6" ?> ">
  <div class="block-icon">
    <? if($item["img"]): ?>
      <div class="block-icon__icon"><img src="<?= $item["img"] ?>" alt=""/></div>
    <? endif ?>
    <div class="block-icon__body">
      <h3 class="block-icon__header"><?= $item["title"] ?></h3>
      <? if($item["text"]): ?>
        <div class="block-icon__text">
          <?= $item["text"] ?>
        </div>
      <? endif ?>
    </div>
  </div>
</div>
