<? if($videoBlockLinks): ?>
<div class="service_page-section">
    <? foreach ($videoBlockLinks as $videoBlockLink): ?>
	<div class="iframe-responsive">
		<div class="iframe-responsive__iframe">
			<iframe src="<?= $videoBlockLink ?>" style="border: 0;" allowfullscreen></iframe>
		</div>
	</div>
    <? endforeach ?>
</div>
<? endif ?>