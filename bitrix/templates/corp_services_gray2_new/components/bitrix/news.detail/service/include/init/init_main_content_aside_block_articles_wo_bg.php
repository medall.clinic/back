<?
/** @var array $displayProperties */
$asideBlockArticlesWoBgId = $displayProperties["aside_block_articles_wo_bg"]["VALUE"];

$asideBlockArticlesWoBg = null;
if ($asideBlockArticlesWoBgId) {
	$res = CIBlockElement::GetList([], ["ID" => $asideBlockArticlesWoBgId]);
	$ob = $res->GetNextElement();
	$fields = $ob->GetFields();
	$props = $ob->GetProperties();
	$asideBlockArticlesWoBg = [
		"title" => $fields["NAME"],
		"articles" => getAsideBlockArticlesWoBg($props),
	];
}

function getAsideBlockArticlesWoBg($props)
{
	if(empty($props["articles"]["VALUE"])){
		return false;
	}

	$articlesId = $props["articles"]["VALUE"];

	$articles = [];
	$res = CIBlockElement::GetList([], ["ID" => $articlesId]);
	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$articles[] = [
			"title" => $fields["NAME"],
			"text" => $fields["~PREVIEW_TEXT"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"link" => $fields["DETAIL_PAGE_URL"],
		];
	}

	return $articles;
}