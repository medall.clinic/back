<?
/** @var array $displayProperties */
$asideBlockArticlesBgId = $displayProperties["aside_block_articles_bg"]["VALUE"];

$asideBlockArticlesBg = null;
if ($asideBlockArticlesBgId) {
	$res = CIBlockElement::GetList([], ["ID" => $asideBlockArticlesBgId]);
	$ob = $res->GetNextElement();
	$fields = $ob->GetFields();
	$props = $ob->GetProperties();
	$asideBlockArticlesBg = [
		"title" => $fields["NAME"],
		"articles" => getAsideBlockArticlesBg($props),
	];
}

function getAsideBlockArticlesBg($props)
{
	if(empty($props["articles"]["VALUE"])){
		return false;
	}

	$articlesId = $props["articles"]["VALUE"];

	$articles = [];
	$res = CIBlockElement::GetList([], ["ID" => $articlesId]);
	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$articles[] = [
			"title" => $fields["NAME"],
			"text" => $fields["~PREVIEW_TEXT"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"link" => $fields["DETAIL_PAGE_URL"],
		];
	}

	return $articles;
}