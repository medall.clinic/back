<tr>
  <td><?= $price["title"] ?></td>
  <? if(!$price["sale_price"]): ?>
    <td><?= $price["price"] ?> руб.</td>
    <td></td>
  <? else: ?>
    <td><del><?= $price["price"] ?> руб.</del></td>
    <td><span class="table-price__discount"><?= $price["sale_price"] ?> руб.</span></td>
  <? endif ?>
</tr>
