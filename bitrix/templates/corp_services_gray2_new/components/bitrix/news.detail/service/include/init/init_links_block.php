<?
$linksBlockId = !empty($displayProperties["links_block"]["VALUE"]) ? $displayProperties["links_block"]["VALUE"] : null;
$linksBlock_add_content = !empty($displayProperties["links_block_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["links_block_add_content"]["DISPLAY_VALUE"] : null;

$linksBlock = [];
if ($linksBlockId) {
	$res = CIBlockElement::GetList([], ["ID" => $linksBlockId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$linksBlock[] = [
			"title" => $fields["NAME"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"is_video_link" => (bool)$props["video_id"]["VALUE"],
			"link" => getLinkBlockValue($props),
		];
	}
}

function getLinkBlockValue($props)
{
	if($props["video_id"]["VALUE"]){
		return "https://www.youtube.com/embed/".$props["video_id"]["VALUE"]."?controls=0&rel=0";
	}else{
		return $props["link"]["VALUE"];
	}
}