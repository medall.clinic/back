<div class="landing-block-node-card col-md-6 g-mb-40 col-lg-4 g-mb-0--last landing-card">
	<div class="media">

		<? if($description["img"]): ?>
			<div class="d-flex g-mt-25 g-mr-40 g-width-64 justify-content-center">
				<span class="landing-block-node-card-icon-container d-block g-font-size-48 g-line-height-1 g-color-white-opacity-0_9">
					<img src="<?= $description["img"] ?>" alt="">
				</span>
			</div>
		<? endif ?>

		<div class="media-body align-self-center">

			<h3 class="landing-block-node-card-title h5 text-uppercase g-font-weight-800 g-color-white landing-block-node-card-title h5 text-uppercase g-font-weight-800 g-color-white landing-block-node-card-title h5 text-uppercase g-font-weight-800 g-color-white"><?= $description["title"] ?></h3>

			<? if($description["text"]): ?>
				<div class="landing-block-node-card-text g-font-size-default mb-0 g-color-gray-light-v2 landing-block-node-card-text g-font-size-default mb-0 g-color-gray-light-v2 landing-block-node-card-text g-font-size-default mb-0 g-color-gray-light-v2">
					<?= $description["text"] ?>
				</div>
			<? endif ?>

		</div>

	</div>
</div>
