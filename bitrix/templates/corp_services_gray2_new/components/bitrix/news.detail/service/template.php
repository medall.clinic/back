<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? $pathTemplateLanding = "/bitrix/templates/landing/" ?>
<? $APPLICATION->SetAdditionalCSS($pathTemplateLanding."/styles/style.css?v=3"); ?>
<? include __DIR__."/include/init/init.php" ?>
<? include __DIR__."/include/slider/slider.php" ?>
<? include __DIR__."/include/slider/links_block.php" ?>
<? include __DIR__."/include/main_content/main_content.php" ?>
<? include __DIR__."/include/reviews.php" ?>
<? include __DIR__."/include/our_doctors.php" ?>
<? include __DIR__."/include/feedback.php" ?>
<? include __DIR__."/include/department_info/department_info.php" ?>
<? include __DIR__."/include/form/index.php" ?>
