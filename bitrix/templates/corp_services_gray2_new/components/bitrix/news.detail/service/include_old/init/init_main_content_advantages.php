<?
/** @var array $displayProperties */
$advantagesId = $displayProperties["advantages"]["VALUE"];

$advantages = null;
if ($advantagesId) {
	$res = CIBlockElement::GetList([], ["ID" => $advantagesId]);
	$ob = $res->GetNextElement();
	$fields = $ob->GetFields();
	$props = $ob->GetProperties();
	$advantages = [
		"title" => $props["title"]["VALUE"],
		"subtitle" => $fields["~PREVIEW_TEXT"],
		"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
		"items" => getAdvantagesItems($props),
	];
}

$advantages_add_content = !empty($displayProperties["advantages_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["advantages_add_content"]["DISPLAY_VALUE"] : null;


function getAdvantagesItems($props)
{
	if(empty($props["advantages_items"]["VALUE"])){
		return false;
	}

	$advantagesItemsId = $props["advantages_items"]["VALUE"];

	$advantagesItems = [];
	$res = CIBlockElement::GetList([], ["ID" => $advantagesItemsId]);
	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$advantagesItems[] = [
			"title" => $fields["NAME"],
			"text" => $fields["~PREVIEW_TEXT"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
		];
	}

	return $advantagesItems;
}