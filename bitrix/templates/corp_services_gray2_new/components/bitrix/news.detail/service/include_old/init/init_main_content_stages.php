<?
$stagesBlocksIds = !empty($displayProperties["stages"]["VALUE"]) ? $displayProperties["stages"]["VALUE"] : null;

$stagesBlocks = [];
if($stagesBlocksIds){
	foreach ($stagesBlocksIds as $stagesBlockId){
		$res = CIBlockElement::GetList([], ["ID" => $stagesBlockId]);
		$ob = $res->GetNextElement();
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$stagesBlocks[] = [
			"title" => $props["title"]["VALUE"],
			"items" => getStagesItems($props),
		];
	}
}

$stages_add_content = !empty($displayProperties["stages_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["stages_add_content"]["DISPLAY_VALUE"] : null;


function getStagesItems($props)
{
	if(empty($props["stages_items"]["VALUE"])){
		return false;
	}

	$stagesItemsId = $props["stages_items"]["VALUE"];

	$stagesItems = [];
	$res = CIBlockElement::GetList([], ["ID" => $stagesItemsId]);
	while($ob = $res->GetNextElement()){
		$fields = $ob->GetFields();
		$stagesItems[] = [
			"title" => $fields["NAME"],
			"text" => $fields["~PREVIEW_TEXT"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
		];
	}

	return $stagesItems;
}