<? /** @var  array $stage */ ?>

<div class="block-icon block-icon--marked-content-dark block-marked__content block-marked__content--icon">
  <? if($stage["img"]): ?>
    <div class="block-icon__icon"><img src="<?= $stage["img"] ?>"/></div>
  <? endif ?>
  <div class="block-icon__body">
    <h3 class="block-icon__header"><?= $stage["title"] ?></h3>
    <div class="block-icon__text">
      <?= $stage["text"] ?>
    </div>
  </div>
</div>
