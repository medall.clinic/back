<? if($slides || $sliderInfo): ?>
<div class="breastEnlargementMainScreen">

	<? if($slides): ?>
	<div class="breastEnlargementMainScreen__slider">
		<? foreach($slides as $slide) : ?>
            <? include __DIR__."/slide.php" ?>
		<? endforeach ?>
    </div>

	<div class="breastEnlargementMainScreen__controls">
		<div class="breastEnlargementMainScreen__control breastEnlargementMainScreen__prev"></div>
		<div class="breastEnlargementMainScreen__control breastEnlargementMainScreen__next"></div>
	</div>
    <? endif ?>

    <? if($sliderInfo): ?>
        <div class="slider__info">
			<?= $sliderInfo ?>
        </div>
	<? endif ?>

</div>
<? endif ?>


<? if($slides_add_content): ?>
<div class="slides_add_content">
    <?= $slides_add_content ?>
</div>
<? endif ?>



