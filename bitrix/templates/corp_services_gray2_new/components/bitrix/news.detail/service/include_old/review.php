<? /** @var array $review */ ?>
<div class="reviews__slider__item">

	<? if($review["img"]): ?>
	<div class="reviews__slider__item__img">
		<a
			class="reviews__slider__item__img__link"
			href="<?= $review["img"] ?>"
			data-fancybox="gallery"
			tabindex="0"
		>
			<img
				src="<?= $review["img"] ?>"
				alt="Отзыв"
				title="Отзыв"
			>
		</a>
	</div>
	<? endif ?>

	<? if($review["doctor"]): ?>
	<div class="reviews__slider__item__doctor">
		<p>
			Врач:
			<a href="<?= $review["doctor"]["link"] ?>" tabindex="0"><?= $review["doctor"]["name"] ?></a>
		</p>
	</div>
	<? endif ?>


	<div class="reviews__slider__item__text">
		<?= $review["text"] ?>
	</div>

</div>
