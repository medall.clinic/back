<? if($linksBlock): ?>
<div class="linksBlock">
    <? foreach ($linksBlock as $linkBlock):  ?>

        <a
            class="linksBlock__item <? if($linkBlock["is_video_link"]): ?>popup-youtube<? endif ?>"
            href="<?= $linkBlock["link"] ?>"
        >
            <? if($linkBlock["img"]): ?>
                <span
                    class="linksBlock__item__icon"
                    style="background-image: url(<?= $linkBlock["img"] ?>);"
                ></span>
            <? endif ?>

            <span class="linksBlock__item__title">
                <span><?= $linkBlock["title"] ?></span>
            </span>

        </a>

	<? endforeach ?>
</div>
<? endif ?>

<? if($linksBlock_add_content): ?>
    <div class="linksBlock_add_content">
		<?= $linksBlock_add_content ?>
    </div>
<? endif ?>


