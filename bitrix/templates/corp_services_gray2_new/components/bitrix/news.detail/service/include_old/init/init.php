<?
$displayProperties = $arResult["DISPLAY_PROPERTIES"];

include __DIR__."/init_slider.php";
include __DIR__."/init_links_block.php";

include __DIR__."/init_main_content_vertical_sort.php";

include __DIR__."/init_main_content_general_desc.php";
include __DIR__."/init_main_content_advantages.php";
include __DIR__."/init_main_content_video_block.php";
include __DIR__."/init_main_content_stages.php";
include __DIR__."/init_main_content_recommendations.php";
include __DIR__."/init_main_content_prices.php";
include __DIR__."/init_main_content_alternative_techniques.php";

include __DIR__."/init_main_content_aside_block_articles_bg.php";
include __DIR__."/init_main_content_aside_block_articles_wo_bg.php";

include __DIR__."/init_reviews.php";
include __DIR__."/init_our_doctors.php";
include __DIR__."/init_department_info.php";

include __DIR__."/init_feedback.php";
include __DIR__."/init_installment.php";