<?
$videoIds = !empty($displayProperties["video"]["VALUE"]) ? $displayProperties["video"]["VALUE"] : null;

$videoBlockLinks = [];
if ($videoIds) {
	$res = CIBlockElement::GetList([], ["ID" => $videoIds]);
	while ($ob = $res->GetNextElement()) {
		$props = $ob->GetProperties();
		$videoBlockLinks[] = $props["video_id"]["VALUE"];
	}
}