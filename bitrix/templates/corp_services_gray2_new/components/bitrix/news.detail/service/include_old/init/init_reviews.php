<?
$reviewsId = $arResult["DISPLAY_PROPERTIES"]['reviews']['VALUE'];

$reviews = [];
if ($reviewsId) {
	$res = CIBlockElement::GetList([], ["ID" => $reviewsId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$reviews[] = [
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
			"text" => $fields["~PREVIEW_TEXT"],
			"doctor" => getDoctor($props),
		];
	}
}

$reviews_add_content = !empty($displayProperties["reviews_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["reviews_add_content"]["DISPLAY_VALUE"] : null;

function getDoctor($props)
{
	if(empty($props["doctor_review"]["VALUE"])){
	    return false;
	}

	$doctorId = $props["doctor_review"]["VALUE"];
	$res = CIBlockElement::GetByID($doctorId);
	if(!$arRes = $res->GetNext()){
		return false;
	}

	$doctor = [
		"name" => $arRes["NAME"],
		"link" => $arRes["DETAIL_PAGE_URL"],
	];

	return $doctor;
}