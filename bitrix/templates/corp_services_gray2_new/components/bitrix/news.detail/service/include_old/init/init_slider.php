<?
$slidesId = !empty($displayProperties["slider_card"]["VALUE"]) ? $displayProperties["slider_card"]["VALUE"] : null;
$slides_add_content = !empty($displayProperties["slider_card_add_content"]["DISPLAY_VALUE"]) ? $displayProperties["slider_card_add_content"]["DISPLAY_VALUE"] : null;

$slides_add = [];

if ($slidesId) {
	$res = CIBlockElement::GetList([], ["ID" => $slidesId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$props = $ob->GetProperties();
		$slides[] = [
			"title" => $fields["NAME"],
			"subtitle" => $props["subtitle_service_card"]["VALUE"],
			"img_front" => !empty($props["image_service_card"]["VALUE"]) ? CFile::GetPath($props["image_service_card"]["VALUE"]) : null,
			"img_back" => !empty($props["image2_service_card"]["VALUE"]) ? CFile::GetPath($props["image2_service_card"]["VALUE"]) : null,
			"link" => $props["link_service_card"]["VALUE"],
			"descriptions" => !empty($props["descriptions"]["VALUE"]) ? getSlideDescriptions($props["descriptions"]["VALUE"]) : null,
		];
	}
}

function getSlideDescriptions($descriptionsId)
{
	$descriptions = [];
	$res = CIBlockElement::GetList([], ["ID" => $descriptionsId]);
	while ($ob = $res->GetNextElement()) {
		$fields = $ob->GetFields();
		$descriptions[] = [
			"title" => $fields["NAME"],
			"text" => $fields["PREVIEW_TEXT"],
			"img" => !empty($fields["PREVIEW_PICTURE"]) ? CFile::GetPath($fields["PREVIEW_PICTURE"]) : null,
		];
	}

	return $descriptions;
}
