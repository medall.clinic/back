<? if($advantages): ?>
        <div class="service_page-section">
          <h2 class="service_page-section__subheader"></h2>
          <div class="service_grid service_grid--padding-y ">

              <? if(!empty($advantages["img"])): ?>
              <div class="service_grid__cell service_grid__cell--m-4 ">
                  <img src="<?= $advantages["img"] ?>">
              </div>

              <div class="service_grid__cell service_grid__cell--m-8 ">
              <? else: ?>
              <div class="service_grid__cell service_grid__cell--m-12">
              <? endif ?>

              <div class="service_page-subsection">
                <div class="why-we-header">

                    <div class="why-we-header__sub">
                        <?= $advantages["subtitle"] ?>
                    </div>

                    <? if($advantages["title"]): ?>
                        <div class="why-we-header__main">
                            <?= $advantages["title"] ?>
                        </div>
                    <? endif ?>

                </div>
              </div>
              <div class="service_page-subsection">
                <? if($advantages["items"]): ?>
                    <? foreach ($advantages["items"] as $item): ?>
                        <? include __DIR__."/advantage.php" ?>
                    <? endforeach ?>
                <? endif ?>
              </div>
            </div>

          </div>
        </div>
<? endif ?>

<? if($advantages_add_content): ?>
    <div class="advantages_add_content">
		<?= $advantages_add_content ?>
    </div>
<? endif ?>
