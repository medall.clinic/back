<?
$vertSortId = !empty($displayProperties["vert_sort"]["VALUE"]) ? $displayProperties["vert_sort"]["VALUE"] : null;

$vertSortBlocks = [];
if ($vertSortId) {
	$vertSortBlocksData = getVertSortBlocksData($vertSortId);
	$vertSortBlocks = applySortToBlacks($vertSortBlocksData, $vertSortId);
}

function getVertSortBlocksData($vertSortId)
{
	$vertSortBlocks = [];

	$res = CIBlockElement::GetList([], ["ID" => $vertSortId]);
	while($obj = $res->GetNext()) {
		$vertSortBlocks[] = [
			"file_name" => $obj["CODE"],
			"title" => $obj["NAME"],
			"id" => $obj["ID"],
		];
	}

	return $vertSortBlocks;
}

function applySortToBlacks($vertSortBlocksData, $vertSortId)
{
	return array_map(function($id) use ($vertSortBlocksData) {
		return reset(array_filter($vertSortBlocksData, function($vertSortBlockData) use ($id) {
			return $vertSortBlockData["id"] == $id;
		}));
	}, $vertSortId);
}