<? /** @var array $slide */ ?>
<div
    class="breastEnlargementMainScreen__slider__item"
    <? if($slide["img_back"]) : ?>style="background-image: url('<?= $slide["img_back"] ?>');" <? endif ?>
>

	<div class="breastEnlargementMainScreen__slider__item__right">
		<div
            class="breastEnlargementMainScreen__slider__item__img"
            <? if($slide["img_front"]) : ?>style="background-image: url('<?= $slide["img_front"] ?>');" <? endif ?>
        ></div>
	</div>

	<div class="breastEnlargementMainScreen__slider__item__left">

		<div class="breastEnlargementMainScreen__slider__item__title title">
			<h2><?= $slide["title"] ?></h2>
		</div>

		<div class="breastEnlargementMainScreen__slider__item__desc">
			<p><?= $slide["subtitle"] ?></p>
		</div>

        <? if($slide["link"]): ?>
		<div class="breastEnlargementMainScreen__slider__item__link">
            <a class="buttonTwo" href="<?= $slide["link"] ?>" tabindex="0">Подробнее</a>
        </div>
        <? endif ?>

	</div>

</div>
