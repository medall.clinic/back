<? if($asideBlockArticlesBg): ?>
<section class="mainContent__ArticleBlock">

    <div class="mainContent__ArticleBlock__title mainContent__text__title">
        <?= $asideBlockArticlesBg["title"] ?>
    </div>

    <? if($asideBlockArticlesBg["articles"]): ?>
    <ul class="mainContent__ArticleBlock__list var-1">

        <? foreach($asideBlockArticlesBg["articles"] as $article): ?>
        <li class="mainContent__ArticleBlock__list__item">
            <a class="mainContent__ArticleBlock__list__link" href="<?= $article["link"] ?>">
            <div class="card">
                <div class="image" style="background-image: url(<?= $article["img"] ?>)"></div>
                <div class="content">
                    <div class="title"><?= $article["title"] ?></div>
                    <div class="text"><?= $article["text"] ?></div>
                </div>
            </div>
            </a>
        </li>
        <? endforeach ?>

	</ul>
    <? endif ?>

</section>
<? endif ?>