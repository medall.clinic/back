<? if ($recommendationsBlocks): ?>
	<? foreach ($recommendationsBlocks as $recommendationsBlock): ?>
        <div class="service_page-section">

			<? if ($recommendationsBlock["title"]): ?>
                <h2 class="service_page-section__subheader">
					<?= $recommendationsBlock["title"] ?>
                </h2>
			<? endif ?>

			<? if ($recommendationsBlock["items"]): ?>
                <div class="block-marked">
					<? foreach ($recommendationsBlock["items"] as $recommendation): ?>
						<? include __DIR__ . "/recommendation.php" ?>
					<? endforeach ?>
                </div>
			<? endif ?>
        </div>
	<? endforeach ?>
<? endif ?>

<? if ($recommendations_add_content): ?>
    <div class="recommendations_add_content">
		<?= $recommendations_add_content ?>
    </div>
<? endif ?>
