<div class="doctorCardPage__mainScreen__left">
	<div class="doctorCardPage__mainScreen__textWrap">

		<div class="doctorCardPage__mainScreen__title title beforeLine">
            <?= $arResult["NAME"] ?><br />
            <?= $arResult["PROPERTIES"]["io_doctor"]["VALUE"] ?>
        </div>

        <div class="doctorCardPage__mainScreen__work"><?= $arResult["PREVIEW_TEXT"] ?></div>
        <? if($arResult["PROPERTIES"]["quote_doctor"]["VALUE"]["TEXT"]): ?>
            <div class="doctorCardPage__mainScreen__quote">
                <p><?= $arResult["PROPERTIES"]["quote_doctor"]["VALUE"]["TEXT"] ?></p>
            </div>
        <? endif ?>
        <div class="doctorCardPage__mainScreen__experience">

            <div class="doctorCardPage__mainScreen__experience__item">
                <div class="doctorCardPage__mainScreen__experience__title">Стаж работы</div>
                <div class="doctorCardPage__mainScreen__experience__text">
                    <?= $arResult["PROPERTIES"]["experience_doctor"]["VALUE"] ?>
                </div>
            </div>

            <div class="doctorCardPage__mainScreen__experience__item">

                <div class="doctorCardPage__mainScreen__experience__title">
                    <?= $arResult["PROPERTIES"]["title_operations_doctor"]["VALUE"] ?>
                </div>

                <div class="doctorCardPage__mainScreen__experience__text">
                    <?= $arResult["PROPERTIES"]["count_operations_doctor"]["VALUE"] ?>
                </div>

            </div>

		</div>

	</div>
</div>





