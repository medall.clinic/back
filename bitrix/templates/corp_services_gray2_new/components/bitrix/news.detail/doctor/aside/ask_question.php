<? if(
        $arResult["PROPERTIES"]["instagram_username"]["VALUE"] ||
        $arResult["PROPERTIES"]["vk_username"]["VALUE"]
): ?>
<div class="banner-social banner-social--aside">

    <p class="banner-social__title">Больше кейсов, экспертной информации и общения в моих социальных сетях</p>

    <div class="banner-social__links">

        <? if($arResult["PROPERTIES"]["instagram_username"]["VALUE"]): ?>
        <a
            class="banner-social__link banner-social__link--instagram"
            href="https://instagram.com/<?= $arResult["PROPERTIES"]["instagram_username"]["VALUE"] ?>"
        ></a>
        <? endif ?>

		<? if($arResult["PROPERTIES"]["vk_username"]["VALUE"]): ?>
        <a
            class="banner-social__link banner-social__link--vk"
            href="https://vk.com/<?= $arResult["PROPERTIES"]["vk_username"]["VALUE"] ?>"
        ></a>
		<? endif ?>

    </div>

    <p class="banner-social__clarification">*Instagram — проект Meta Platforms Inc., деятельность которой признана в России экстремисткой организацией и запрещена.</p>

</div>
<? endif ?>
