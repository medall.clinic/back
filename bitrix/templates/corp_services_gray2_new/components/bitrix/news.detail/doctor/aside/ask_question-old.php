<!--
<div class="faqPage__askQuestion">
	<div class="faqPage__askQuestion__title title">У вас есть вопросы<br/> к врачу?</div>
	<a class="faqPage__askQuestion__link buttonTwo openModalPopup" href="#question">Задать свой вопрос</a>
	<a class="faqPage__askQuestion__more more" href="/about/faq/">Читать все вопросы</a>
</div>
-->

<? if($arResult["PROPERTIES"]["instagram_username"]["VALUE"]): ?>
    <a
        href="https://instagram.com/<?= $arResult["PROPERTIES"]["instagram_username"]["VALUE"] ?>"
        class="button"
        style="width: 100%; padding-right: 2rem; padding-left: 2rem;"
    >@<?= $arResult["PROPERTIES"]["instagram_username"]["VALUE"] ?></a>
<? endif ?>

