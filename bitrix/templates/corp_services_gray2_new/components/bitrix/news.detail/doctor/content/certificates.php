<div class="doctorCardPage__title greyTitle rbs">Сертификаты</div>

<div class="documentsBlockSlider" style="margin-top: 200px;">

    <div class="diplomasSlider">

		<? foreach($arResult["DISPLAY_PROPERTIES"]["certificate_doctor"]["FILE_VALUE"] as $v): ?>
        <div class="diplomasSlider__item">
            <a
                class="doctorCardPage__certificate__link"
                href="<?= $v["SRC"] ?>"
                data-fancybox="doctorCertificate"
            >
                <picture>
                    <source srcset="<?= getResizedImageCopyPath(WebPHelper($v["SRC"])) ?>" type="image/webp">
                    <img src="<?= getResizedImageCopyPath($v["SRC"]) ?>" alt="" loading="lazy">
                </picture>
            </a>
        </div>
		<? endforeach ?>

    </div>

    <div class="documentsSlider__controls">
        <div class="documentsSlider__control documentsSlide__prev diplomasSlider__prev" style="left: 0px;"></div>
        <div class="documentsSlider__control documentsSlide__next diplomasSlider__next"></div>
    </div>

</div>
