<div class="landing-page" style="margin-top: 20px; margin-bottom: 120px; padding: 0 5px; font-size: inherit;">
	<section class="page-section" id="section-contact">
		<div class="page-subsection">
			<h2 style="font-size: 32px; line-height 1.25; font-family: inherit;">Хотите записаться на консультацию? Оставьте свои контактные данные и мы обязательно свяжемся с Вами.</h2>
		</div>

		<div class="page-subsection">
			<? include __DIR__."/form.php" ?>
		</div>
	</section>
</div>
