<?
// определяемся с процедурой
$Procedure = '';
if($beforeAfterItem['Procedure_plastic_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_plastic_before_after']['VALUE']]['NAME']){
	$Procedure = $beforeAfterItem['Procedure_plastic_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_plastic_before_after']['VALUE']]['NAME'];
}
elseif($beforeAfterItem['Procedure_cosmetology_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_cosmetology_before_after']['VALUE']]['NAME']){
	$Procedure = $beforeAfterItem['Procedure_cosmetology_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_cosmetology_before_after']['VALUE']]['NAME'];
}
elseif($beforeAfterItem['Procedure_phlebology_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_phlebology_before_after']['VALUE']]['NAME']){
	$Procedure = $beforeAfterItem['Procedure_phlebology_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_phlebology_before_after']['VALUE']]['NAME'];
}
elseif($beforeAfterItem['Procedure_hair_transplant_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_hair_transplant_before_after']['VALUE']]['NAME']){
	$Procedure = $beforeAfterItem['Procedure_hair_transplant_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_hair_transplant_before_after']['VALUE']]['NAME'];
}
elseif($beforeAfterItem['Procedure_dentistry_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_dentistry_before_after']['VALUE']]['NAME']){
	$Procedure = $beforeAfterItem['Procedure_dentistry_before_after']['LINK_ELEMENT_VALUE'][$beforeAfterItem['Procedure_dentistry_before_after']['VALUE']]['NAME'];
}
?>
<!--Попапы-->
<div class="popupBeforeAfter" id="bx_before_after__item_<?= $beforeAfterPhotoId ?>">

    <div class="popup__close">
        <div class="popup__close__line"></div>
        <div class="popup__close__line"></div>
    </div>

    <div class="popupBeforeAfter__block beforeAfterPage__item__wrapper">


        <div class="popupBeforeAfter__slider-wrapper">
            <div class="popupBeforeAfter__slider">
                <div class="popupBeforeAfter__img">
                    <span style="background-image: url(<?= $beforeAfterPhotoSrc ?>)"></span>
                </div>
            </div>
        </div>

        <div class="popupBeforeAfter__text">

            <div class="popupBeforeAfter__textBlock">
                <div class="popupBeforeAfter__textBlock__left">
                    <p>
                        <span class="beforeAfterPage__text__title">Процедура:</span>
                        <span class="green"><?= $Procedure ?></span>
                    </p>
                </div>
                <div class="popupBeforeAfter__textBlock__right">
                    <p>
                        <span class="beforeAfterPage__text__title">Врач:</span>
                        <span class="green"><?= $arResult["NAME"] ?> <?= $arResult["PROPERTIES"]["io_doctor"]["VALUE"] ?></span>
                    </p>
                </div>
            </div>

			<? if(!empty($beforeAfterItem['review_before_after']['VALUE'])): ?>
                <div class="popupBeforeAfter__textdesc">
                    <p>
                        <span class="beforeAfterPage__text__title">Отзыв пациента:</span>
						<?
						$arFilter = Array("ID"=>$beforeAfterItem["review_before_after"]["VALUE"]);
						$res = CIBlockElement::GetList(Array(), $arFilter);
						if ($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields(); // поля элемента
							echo $arFields['DETAIL_TEXT'];
						}
						?>
                    </p>
                </div>
			<? endif ?>

        </div> <!-- popupBeforeAfter__text -->

    </div> <!-- popupBeforeAfter__block beforeAfterPage__item__wrapper -->

</div> <!-- popupBeforeAfter -->