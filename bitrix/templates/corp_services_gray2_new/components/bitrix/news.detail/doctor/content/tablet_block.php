<!-- Блок появляется на разрешении меньше 995px-->
<div class="doctorCardPage__tabletBlock">
	<div class="doctorCardPage__tabletBlock__item">
		<div class="calendarWrap">
			<div class="mainContent__ArticleBlock__title mainContent__text__title"></div>
			<div class="calendar">
				<a class="button calendar__button openModalPopup" href="#orderConsultation">Запись на консультацию</a>
			</div>
		</div>
	</div>
	<? if(
        $arResult["PROPERTIES"]["instagram_username"]["VALUE"] ||
        $arResult["PROPERTIES"]["vk_username"]["VALUE"]
	): ?>
		<div class="banner-social">

				<p class="banner-social__title">Больше кейсов, экспертной информации и общения в моих социальных сетях</p>

				<div class="banner-social__links">

						<? if($arResult["PROPERTIES"]["instagram_username"]["VALUE"]): ?>
						<a
								class="banner-social__link banner-social__link--instagram"
								href="https://instagram.com/<?= $arResult["PROPERTIES"]["instagram_username"]["VALUE"] ?>"
						></a>
						<? endif ?>

				<? if($arResult["PROPERTIES"]["vk_username"]["VALUE"]): ?>
						<a
								class="banner-social__link banner-social__link--vk"
								href="https://vk.com/<?= $arResult["PROPERTIES"]["vk_username"]["VALUE"] ?>"
						></a>
				<? endif ?>

				</div>

				<p class="banner-social__clarification">*Instagram — проект Meta Platforms Inc., деятельность которой признана в России экстремисткой организацией и запрещена.</p>

		</div>
	<? endif ?>

	<? /*  <div class="doctorCardPage__tabletBlock__item">
		<div class="faqPage__askQuestion">
			<div class="faqPage__askQuestion__title title">У вас есть вопросы
				<br/> к врачу?</div>
			<a class="faqPage__askQuestion__link buttonTwo question" href="#" onclick="return false">Задать свой вопрос</a>
			<a class="faqPage__askQuestion__more more" href="/about/faq/">Читать все вопросы</a>
		</div>
	</div> */ ?>
</div>