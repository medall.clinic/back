<? /** @var array $works */ ?>
<div class="landing-page" style="margin-top: 20px; margin-bottom: 120px; padding: 0; font-size: inherit;">

                <div class="page-subsection">
                    <h2 class="text-align-center" style="font-size: 32px; line-height 1.25; font-family: inherit;">Работы специалистов клиники</h2>
                </div>

                <div class="page-subsection">
                    <div class="works">
                        <div class="splide">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <? foreach ($works as $work): ?>
                                        <? include __DIR__."/work.php" ?>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


</div>
