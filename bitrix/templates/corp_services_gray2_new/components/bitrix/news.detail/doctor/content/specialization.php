<?
$specialization = [];

//Пластика
if(!empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_plastic']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_plastic']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}

//Косметология
if(!empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_cosmetology']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_cosmetology']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}

//Флебология
if(!empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_phlebology']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_phlebology']['LINK_ELEMENT_VALUE'] as $k => $v){
        $specialization[0]['NAME'] = "Флебология";
        $specialization[0]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
        $specialization[0]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
    }
}

//Пересадка волос
if(!empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_hair_transplant']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_hair_transplant']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}

//Стоматология
if(!empty($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_dentistry']['LINK_ELEMENT_VALUE'])){
    foreach($arResult["DISPLAY_PROPERTIES"]['specialization_doctor_dentistry']['LINK_ELEMENT_VALUE'] as $k => $v){
        $db_old_groups = CIBlockElement::GetElementGroups($k, true);
        while($ar_group = $db_old_groups->Fetch()){
            $specialization[$ar_group['ID']]['NAME'] = $ar_group['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['NAME'] = $v['NAME'];
            $specialization[$ar_group['ID']]['VALUE'][$v['ID']]['DETAIL_PAGE_URL'] = $v['DETAIL_PAGE_URL'];
        }
    }
}
?>
<div class="doctorCardPage__title greyTitle">Специализация</div>

<? foreach($specialization as $v): ?>
<div class="doctorCardPage__specialization">

	<div class="doctorCardPage__specialization__left">
        <div class="doctorCardPage__specialization__title"><?= $v["NAME"] ?></div>
	</div>

	<div class="doctorCardPage__specialization__right">
        <? foreach($v["VALUE"] as $l): ?>
            <a
                class="doctorCardPage__specialization__text"
                disabled-href="<?= $l["DETAIL_PAGE_URL"] ?>"
            ><?= $l["NAME"] ?></a>
        <? endforeach ?>
    </div>

</div>
<? endforeach ?>