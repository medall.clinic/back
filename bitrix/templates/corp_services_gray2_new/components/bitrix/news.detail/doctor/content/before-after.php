<?
$beforeAfterItems = [];

$beforeAfterItemIds = $arResult["DISPLAY_PROPERTIES"]["result_doctor"]["VALUE"];
if($beforeAfterItemIds){
	$beforeAfterItemsRes = CIBlockElement::GetList([], ["ID" => $beforeAfterItemIds, "ACTIVE" => "Y"]);
	while($beforeAfterItem = $beforeAfterItemsRes->GetNextElement()){
		$properties = $beforeAfterItem->GetProperties();
		if(!empty($properties["photos"]["VALUE"])){
			$beforeAfterItems[] = $properties;
		}
	}
}
?>

<? if($beforeAfterItems): ?>
    <div class="doctorCardPage__title greyTitle">Результаты работ</div>

    <div class="doctor-before-after">

        <? foreach($beforeAfterItems as $beforeAfterItem): ?>

            <? $beforeAfterPhotoId = reset($beforeAfterItem["photos"]["VALUE"]); ?>
            <? $beforeAfterPhotoSrc = CFile::GetPath($beforeAfterPhotoId); ?>

            <div class="doctor-before-after__item">
                <a
                    href="#bx_before_after__item_<?= $beforeAfterPhotoId ?>"
                    class="openPopup"
                >
                    <img
                        src="<?= $beforeAfterPhotoSrc ?>"
                        alt="картинка"
                    >
                </a>
            </div>
            <? include __DIR__."/before_after_popup.php" ?>

        <? endforeach ?>

    </div>

    <a
        class="more"
        href="/before_after/?PROPERTY_DOCTOR_BEFORE_AFTER=<?= $arResult["ID"] ?>"
    >Смотреть все</a>

<? endif ?>