<form class="contacts_to_bitrix24 grid grid--padding-y" method="post">

	<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
		<div class="form-input form-input--text form__input">
			<input class="form-input__field" type="text" name="name" required="required" placeholder="Имя *">
		</div>
	</div>

	<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
		<div class="form-input form-input--text form__input">
			<input class="form-input__field" type="text" name="second_name" required="required" placeholder="Фамилия *">
		</div>
	</div>

	<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
		<div class="form-input form-input--tel form__input">
			<input class="form-input__field" type="tel" name="phone" required="required" placeholder="Телефон *">
		</div>
	</div>

	<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
		<div class="form-input form-input--email form__input">
			<input class="form-input__field" type="email" name="email" placeholder="E-mail">
		</div>
	</div>

	<div class="grid__cell grid__cell--xs-12">
		<div class="form-input form-input--textarea form__input">
			<textarea class="form-input__field" name="comments" placeholder="Комментарий"></textarea>
		</div>
	</div>

	<div class="grid__cell grid__cell--xs-12">
		<div class="form__agreement">Нажимая кнопку «Отправить» я даю согласие на <a href="/">обработку персональных данных</a>.</div>
	</div>

	<div class="grid__cell grid__cell--xs-12">
		<button class="btn" type="submit">Отправить</button>
	</div>

    <input type="hidden" name="title" value="страница доктора: <?= trim($arResult["NAME"]) ?>">

	<? if(!empty($_GET)): ?>
		<? foreach ($_GET as $paramName => $paramValue): ?>
			<? if(strpos($paramName, 'utm_') !== false): ?>
                <input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
			<? endif ?>
		<? endforeach ?>
	<? endif ?>


</form>
