<?
namespace Medreclama\Landing;

class Links extends Landing
{
	public $items;


	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["LINKS_TITLE"]["~VALUE"];
		$this->items = $this->getItems($this->landingProps["LINKS_ITEMS"]["~VALUE"]);
		$this->button = new Button($this->landingProps["LINKS_BUTTON_TEXT"], $this->landingProps["LINKS_BUTTON_LINK"], $this->landingProps["LINKS_BUTTON_FANCYBOX"]);
		$this->menuItem = new MenuItem($this->landingProps["LINKS_MENU_TITLE"]["VALUE"], $this->landingProps["LINKS_MENU_LINK"]["VALUE"], $this->blockId);
		$this->blockId = $this->getBlockId($this->landingProps["LINKS_BLOCK_ID"]);
		$this->generalBlockId = $this->landingProps["LINKS_GENERAL_BLOCK"]["VALUE"];
		$this->additionalText = $this->getAdditionalText($this->landingProps["LINKS_ADDITIONAL_TEXT"]);
		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(!$this->title && !$this->items && !$this->additionalText){
			return false;
		}

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Link;
			$item->image = $this->getImageSrc($element->props["IMAGE"]["VALUE"]);
			$item->text = $element->props["TEXT"]["VALUE"];
			$item->link = $element->props["LINK"]["VALUE"];
			$items[] = $item;
		}

		return $items;
	}

	private function getAdditionalText($prop)
	{
		return $prop["~VALUE"] ? $prop["~VALUE"]["TEXT"] : false;
	}

}
