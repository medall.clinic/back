<?
namespace Medreclama\Landing;

class Reason
{
	public $image;
	public $text;
	public $link;
	public $isSmallSize;
}
