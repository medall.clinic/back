<?
namespace Medreclama\Landing;

class Button
{
	public $text;
	public $link;
	public $isFancybox;

	public function __construct($text, $link, $fancybox)
	{
		$this->text = !empty($text["VALUE"]) ? $text["VALUE"] : $text["DEFAULT_VALUE"];
		$this->link = !empty($link["VALUE"]) ? $link["VALUE"] : $link["DEFAULT_VALUE"];
		$this->isFancybox = (bool)$fancybox["VALUE"];
	}

	public function isExist(): bool
	{
		return !empty($this->text) AND !empty($this->link);
	}

}
