<?
/** @var array $arResult */
use Medreclama\Landing\Landing;

require_once __DIR__."/Landing.php";

$landing = new Landing($arResult);

$dir = new RecursiveDirectoryIterator(__DIR__);
foreach (new RecursiveIteratorIterator($dir) as $file) {
	if (!is_dir($file)) {
		if( fnmatch('*.php', $file) && $file->getFilename() !== "init.php"){
			require_once $file;
		}
	}
}

$models = [];
foreach ($landing->getVertSortedModelsClassNames() as $className){
	$model = $landing->create($className);
	if(!$model){continue;}
	$models[] = $model;
	if($model->generalBlockId){
		$model = $landing->create("General_".$model->generalBlockId);
		$models[] = $model;
	}
}

return $models;