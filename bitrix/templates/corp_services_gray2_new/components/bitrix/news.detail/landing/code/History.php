<?
namespace Medreclama\Landing;

class History extends Landing
{
	public $text;
	public $items;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["HISTORY_TITLE"]["~VALUE"];

		$this->blockId = $this->getBlockId($this->landingProps["HISTORY_BLOCK_ID"]);
		$this->items = $this->landingProps["HISTORY_ITEMS"]["~VALUE"];
		$this->button = new Button($this->landingProps["HISTORY_BUTTON_TEXT"], $this->landingProps["HISTORY_BUTTON_LINK"], $this->landingProps["HISTORY_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["HISTORY_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["HISTORY_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["HISTORY_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["HISTORY_MENU_TITLE"]["VALUE"], $this->landingProps["HISTORY_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(!$this->items){
			return false;
		}

		return true;
	}

}
