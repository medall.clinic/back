<?
namespace Medreclama\Landing;

class BannerFacts extends Landing
{
	public $items;
	public $images;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->items = $this->getItems($this->landingProps["BANNER_FACTS_ITEMS"]["VALUE"]);
		$this->images = $this->getImages($this->landingProps["BANNER_FACTS_IMAGES"]["VALUE"]);
		$this->blockId = $this->getBlockId($this->landingProps["BANNER_FACTS_BLOCK_ID"]);
		$this->additionalText = $this->landingProps["BANNER_FACTS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["BANNER_FACTS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->generalBlockId = $this->landingProps["BANNER_FACTS_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["BANNER_FACTS_MENU_TITLE"]["VALUE"], $this->landingProps["BANNER_FACTS_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(!$this->items && !$this->images){
			return false;
		}

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new BannerFact();
			$item->title = $element->props["TITLE"]["VALUE"];
			$item->text = $element->props["TEXT"]["VALUE"];

			$items[] = $item;
		}

		return $items;
	}

}