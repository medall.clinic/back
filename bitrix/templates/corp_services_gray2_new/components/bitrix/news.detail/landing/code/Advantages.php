<?
namespace Medreclama\Landing;

class Advantages extends Landing
{
	public $items;
	public $images;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["ADVANTAGES_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["ADVANTAGES_BLOCK_ID"]);
		$this->items = $this->getItems($this->landingProps["ADVANTAGES_ITEMS"]["VALUE"]);
		$this->images = $this->getImages($this->landingProps["ADVANTAGES_IMAGES"]["VALUE"]);
		$this->button = new Button($this->landingProps["ADVANTAGES_BUTTON_TEXT"], $this->landingProps["ADVANTAGES_BUTTON_LINK"], $this->landingProps["ADVANTAGES_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["ADVANTAGES_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["ADVANTAGES_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["ADVANTAGES_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["ADVANTAGES_MENU_TITLE"]["VALUE"], $this->landingProps["ADVANTAGES_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems($elementsId)
	{
		if(empty($elementsId)){
			return false;
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$item = new Advantage;
			$item->title = $element->props["TITLE"]["VALUE"];
			$item->text = $element->fields["~PREVIEW_TEXT"];

			$items[] = $item;
		}

		return $items;
	}

	private function isShow()
	{
		if(
			!$this->title &&
			!$this->items &&
			!$this->images
		){
			return false;
		}

		return true;
	}


}
