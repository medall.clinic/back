<? /** @var $general Medreclama\Landing\General */ ?>
<form class="form contacts_to_bitrix24">

	<div class="form-input form-input--text form__input">
		<input
			class="form-input__field"
			type="text"
			name="name"
			required="required"
			placeholder="ФИО *"
		>
	</div>

	<div class="form-input form-input--tel form__input">
		<input
			class="form-input__field"
			type="tel"
			name="phone"
			required="required"
			placeholder="Телефон *"
		>
	</div>

	<div class="form__agreement">Нажимая кнопку «Отправить» я даю согласие на <a href="/">обработку персональных данных</a></div>

	<button class="btn" type="submit">Отправить</button>

	<input type="hidden" name="title" value="<?= $general->formTitle ?>">

	<? if($general->formUtmTags): ?>
		<? foreach ($general->formUtmTags as $utmTag): ?>
			<input type="hidden" name="<?= $utmTag->name ?>" value="<?= $utmTag->value ?>">
		<? endforeach ?>
	<? endif ?>

</form>