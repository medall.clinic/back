<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="banner-team__details">

	<? if($doctor->skills): ?>
		<h3 class="small">Ключевые навыки и специализация:</h3>
		<ul class="list-marked">
			<? foreach ($doctor->skills as $skill): ?>
				<li><?= $skill ?></li>
			<? endforeach ?>
		</ul>
	<? endif ?>

	<? if($doctor->keySkill): ?>
		<div class="banner-team__important"><?= $doctor->keySkill ?></div>
	<? endif ?>

	<? if($doctor->education): ?>
		<h3 class="small">Образование:</h3>
		<? foreach ($doctor->education as $education): ?>
			<p><?= $education ?></p>
		<? endforeach ?>
	<? endif ?>

	<div class="banner-team__button">
		<a class="btn" href="#section-contact">Записаться на консультацию</a>
	</div>

</div>
