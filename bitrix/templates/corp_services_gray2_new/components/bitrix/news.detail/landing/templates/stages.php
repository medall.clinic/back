<? /** @var $stages Medreclama\Landing\Stages */ ?>
<? /** @var $stage Medreclama\Landing\Stage */ ?>
<? /** @var $finalStage Medreclama\Landing\Stage */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $finalStage = $stages->finalStage ?>
<? $button = $stages->button ?>
<section class="page-section page-section--text-align-center" id="<?= $stages->blockId ?>">

	<div class="page-subsection container">
		<div class="content">

            <? if($stages->title): ?>
			<div class="page-subsection">
				<h2 class="text-align--center"><?= $stages->title ?></h2>
			</div>
            <? endif ?>

			<? if($stages->items): ?>
                <div class="page-subsection">
                    <ol class="grid grid--nopadding sequence">
						<? foreach ($stages->items as $stage): ?>
                            <li class="grid__cell grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12 sequence__item">

								<? if($stage->image): ?>
                                    <div class="sequence__image">
                                        <img src="<?= $stage->image ?>" alt=""/>
                                    </div>
								<? endif ?>

								<? if($stage->name): ?>
                                    <div class="sequence__name"><?= $stage->name ?></div>
								<? endif ?>

								<? if($stage->text): ?>
                                    <div class="sequence__description">
										<?= $stage->text ?>
                                    </div>
								<? endif ?>

                            </li>
						<? endforeach ?>
                    </ol>
                </div>
			<? endif ?>

            <? if($stages->listItems): ?>
                <div class="page-subsection">
                    <ol class="grid grid--padding-y list-sequence">
                        <? foreach ($stages->listItems as $listItem): ?>
                            <li class="grid__cell grid__cell--m-6 grid__cell--xs-12 list-sequence__item">
                                <div class="list-sequence__item-inner">
                                    <p><?= $listItem ?></p>
                                </div>
                            </li>
                        <? endforeach ?>
                    </ol>
                </div>
            <? endif ?>

		</div>
	</div>

    <? if($finalStage->text || $finalStage->image): ?>
	<div class="page-subsection">
		<div class="container banner-result">
			<div class="content">
				<div class="grid grid--nopadding grid--align-center">

                    <? if($finalStage->text): ?>
					<div class="grid__cell grid__cell--l-7 grid__cell--xs-12 banner-result__info"><?= $finalStage->text ?></div>
                    <? endif ?>

                    <? if($finalStage->image): ?>
                    <div class="grid__cell grid__cell--l-5 grid__cell--xs-12 banner-result__image">
                        <picture>
							<source srcset="<?= WebPHelper($finalStage->image) ?>" media="(-webkit-max-device-pixel-ratio: 1)" type="image/webp">
							<source srcset="<?= WebPHelper2x($finalStage->image) ?>" type="image/webp">
							<source srcset="<?= $finalStage->image ?>" media="(-webkit-max-device-pixel-ratio: 1)">
                            <img src="<?= $finalStage->image ?>" alt="">
						</picture>
					</div>
                    <? endif ?>

				</div>
			</div>
		</div>
	</div>
    <? endif ?>

	<? if($button->isExist()): ?>
    <div class="page-subsection">
        <div class="grid grid--justify-center">
            <div class="grid__cell grid__cell--xs-auto">
                <a
                    class="btn btn--wide"
                    href="<?= $button->link ?>"
					<?= $button->isFancybox ? "data-fancybox" : "" ?>
                ><?= $button->text ?></a>
            </div>
        </div>
    </div>
    <? endif ?>

</section>
