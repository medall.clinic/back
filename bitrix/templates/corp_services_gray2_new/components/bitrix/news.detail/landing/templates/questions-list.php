<? /** @var $question Medreclama\Landing\Question */ ?>
<? /** @var $questions Medreclama\Landing\Questions */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $questions->button ?>
<div class="page-subsection">
    <? foreach ($questions->items as $question): ?>
        <div class="question">
            <div class="question__name"><?= $question->text ?></div>
            <div class="question__body"><?= $question->answer ?></div>
        </div>
    <? endforeach ?>
</div>

<? if($button->isExist()): ?>
    <div class="page-subsection">
        <a
            class="btn"
            href="<?= $button->link ?>"
			<?= $button->isFancybox ? "data-fancybox" : "" ?>
        ><?= $button->text ?></a>
    </div>
<? endif ?>

