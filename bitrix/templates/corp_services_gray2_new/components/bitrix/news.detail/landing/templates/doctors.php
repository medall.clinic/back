<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<section class="page-section banner-team banner-team--all-on-4" id="<?= $doctors->blockId ?>">

    <div class="container">
        <div class="content">

			<? if($doctors->title): ?>
            <div class="page-subsection">
                <h2 class="banner-team__header text-align--center"><?= $doctors->title ?></h2>
            </div>
			<? endif ?>

			<? if($doctors->text): ?>
            <div class="page-subsection">
                <div class="grid grid--padding-y">
					<?= $doctors->text ?>
                </div>
            </div>
			<? endif ?>

			<? if($doctors->items): ?>
                <? include __DIR__."/doctors-photos.php" ?>
            <? endif ?>

        </div>
    </div>

    <div class="container banner-team__names">
        <div class="content">

			<? if($doctors->items): ?>
				<? include __DIR__."/doctors-blocks.php" ?>
			<? endif ?>

            <? include __DIR__."/doctors-button.php" ?>

        </div>
    </div>


</section>

