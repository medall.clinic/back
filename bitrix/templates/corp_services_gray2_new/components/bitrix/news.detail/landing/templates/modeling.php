<? /** @var $modeling Medreclama\Landing\Modeling */ ?>
<section class="page-section page-section--3d-modelling" id="<?= $modeling->blockId ?>">

    <? if($modeling->title): ?>
    <div class="page-subsection section-header">
        <div class="container">
            <div class="content">
                <h2><?= $modeling->title ?></h2>
            </div>
        </div>
    </div>
	<? endif ?>

    <div class="page-subsection">
        <div class="container">
            <div class="content">
                <div class="grid grid--padding-y grid--align-center">

                    <div class="grid__cell grid__cell--m-6 grid__cell--xs-12">

						<?= $modeling->text ?>

                        <? if($modeling->images): ?>
                        <div class="slider slider--simple splide hidden-m hidden-l hidden-xl hidden-hd">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <? foreach($modeling->images as $image): ?>
                                    <li class="splide__slide">
                                        <div class="slider__image">
                                            <img src="<?= $image ?>" alt="">
                                        </div>
                                    </li>
									<? endforeach ?>
                                </ul>
                            </div>
                        </div>
						<? endif ?>

                        <? if($modeling->oldButton): ?>
                        <div class="get-now">
                            <p class="get-now__info">Запишитесь прямо сейчас</p>
                            <p>
                                <a
                                    class="btn btn--secondary get-now__button"
                                    href="#section-contact"
                                ><?= $modeling->oldButton ?></a>
                            </p>
                        </div>
                        <? endif ?>

                    </div>

                    <? if($modeling->images): ?>
                    <div class="grid__cell grid__cell--m-6 grid__cell--s-hidden grid__cell--xs-hidden">
                        <div class="slider slider--simple splide">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <? foreach($modeling->images as $image): ?>
                                    <li class="splide__slide">
                                        <div class="slider__image">
                                            <img src="<?= $image ?>" alt="">
                                        </div>
                                    </li>
									<? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
					<? endif ?>

                </div>
            </div>
        </div>
    </div>
</section>