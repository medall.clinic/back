<? /** @var $review Medreclama\Landing\Review */ ?>
<li class="splide__slide">
	<? if($review->videoId): ?>
        <a
            class="stories__image"
            href="https://www.youtube.com/embed/<?= $review->videoId ?>"
            data-fancybox="data-fancybox"
        >

			<? if($review->picture): ?>
                <img src="<?= $review->picture ?>" alt="" />
			<? endif ?>

			<? if($review->name OR $review->description): ?>
                <div class="stories__info">
					<? if($review->name): ?>
                        <div class="stories__name"><?= $review->name ?></div>
					<? endif ?>
					<? if($review->description): ?>
                        <div class="stories__description"><?= $review->description ?></div>
					<? endif ?>
                </div>
			<? endif ?>
        </a>
	<? else: ?>
		<? if($review->picture): ?>
            <div class="stories__image">
                <img src="<?= $review->picture ?>" alt=""/>
				<? if($review->name OR $review->description): ?>
                    <div class="stories__info">
						<? if($review->name): ?>
                            <div class="stories__name"><?= $review->name ?></div>
						<? endif ?>
						<? if($review->description): ?>
                            <div class="stories__description"><?= $review->description ?></div>
						<? endif ?>
                    </div>
				<? endif ?>
            </div>
		<? endif ?>
	<? endif ?>
</li>