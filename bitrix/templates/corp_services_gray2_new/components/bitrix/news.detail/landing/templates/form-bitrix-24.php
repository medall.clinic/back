<form class="contacts_to_bitrix24 form">

	<h3 class="small form__header hidden-m hidden-s hidden-xs">Контактные данные</h3>

	<p class="hidden-m hidden-s hidden-xs">Оставьте контактные данные и мы обязательно свяжемся с вами. </p>

	<div class="form-input form-input--text form__input">
		<input class="form-input__field" type="text" name="name" required="required" placeholder="ФИО *">
	</div>

	<div class="form-input form-input--tel form__input">
		<input class="form-input__field" type="tel" name="phone" required="required" placeholder="Телефон *">
	</div>

	<div class="form-input form-input--tel form__input">
		<input class="form-input__field" type="email" name="email" placeholder="E-mail">
	</div>

	<div class="form-input form-input--textarea form__input">
		<textarea class="form-input__field" name="comments" placeholder="Комментарий"></textarea>
	</div>

	<div class="form__agreement">Нажимая кнопку «Отправить» я даю согласие на <a href="/">обработку персональных данных</a>.</div>

	<button class="btn" type="submit">Отправить</button>

	<input type="hidden" name="title" value="<?= $form->title ?>">

	<? if($form->utmTags): ?>
		<? foreach ($form->utmTags as $utmTag): ?>
			<input type="hidden" name="<?= $utmTag->name ?>" value="<?= $utmTag->value ?>">
		<? endforeach ?>
	<? endif ?>

</form>
