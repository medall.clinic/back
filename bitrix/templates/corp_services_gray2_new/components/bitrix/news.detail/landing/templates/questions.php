<? /** @var $questions Medreclama\Landing\Questions */ ?>
<section class="page-section page-section--questions" id="<?= $questions->blockId ?>">

    <? if($questions->title): ?>
	<div class="container questions-header">
		<div class="content">
			<h2><?= $questions->title ?></h2>
		</div>
	</div>
    <? endif ?>

	<div class="container">
		<div class="content">

			<? if($questions->items): ?>
			<div class="page-subsection">
				<? if($questions->image): ?>
                    <div class="grid grid--padding-y">
                        <div class="grid__cell grid__cell--l-7 grid__cell--xs-12">
							<? include __DIR__."/questions-list.php" ?>
                        </div>
                        <div class="grid__cell grid__cell--l-5 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden">
                            <img class="image-round" src="<?= $questions->image ?>" alt="" />
                        </div>
                    </div>
				<? else: ?>
					<? include __DIR__."/questions-list.php" ?>
				<? endif ?>
			</div>
			<? endif ?>

		</div>
	</div>

</section>
