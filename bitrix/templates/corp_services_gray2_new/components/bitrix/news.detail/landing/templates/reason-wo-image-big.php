<?/** @var Medreclama\Landing\Reason $reason */?>
<li class="grid__cell grid__cell--l-4 grid__cell--xs-8 reasons__item">

	<? if($reason->link): ?>
        <a class="grid grid--nopadding reason" href="<?= $reason->link ?>">
    <? else: ?>
        <div class="grid grid--nopadding reason">
    <? endif ?>

    <? if($reason->text): ?>
        <div class="grid__cell grid__cell--xs-12 reason__info">
            <?= $reason->text ?>
        </div>
    <? endif ?>

    <? if($reason->link): ?>
        </a>
    <? else: ?>
        </div>
    <? endif ?>

</li>