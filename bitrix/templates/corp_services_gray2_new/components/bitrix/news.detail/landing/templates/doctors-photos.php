<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="page-subsection">
	<div class="banner-team__photo-slider">
		<div class="splide">
			<div class="splide__track">
				<ul class="splide__list">

					<? foreach ($doctors->items as $doctor): ?>
					<li class="splide__slide">
						<div class="banner-team__photo-slide">

							<? if($doctor->image): ?>
                                <? if($doctor->hasDetails()): ?>
                                    <a href="#doctor-<?= $doctor->id ?>" data-fancybox>
                                        <img src="<?= $doctor->image ?>" alt="<?= $doctor->name ?>" />
                                        <div class="banner-team__more"></div>
                                    </a>
								<? elseif($doctor->url): ?>
                                    <a href="<?= $doctor->url ?>">
                                        <img src="<?= $doctor->image ?>" alt="<?= $doctor->name ?>" />
                                    </a>
								<? else: ?>
                                    <img src="<?= $doctor->image ?>" alt="<?= $doctor->name ?>" />
								<? endif ?>
							<? endif ?>

						</div>
					</li>
                    <? endforeach ?>

				</ul>
			</div>
		</div>
	</div>
</div>