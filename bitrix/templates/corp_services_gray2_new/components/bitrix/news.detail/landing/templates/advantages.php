<? /** @var $advantages Medreclama\Landing\Advantages */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $advantages->button ?>
<section class="page-section page-section--gradient" id="<?= $advantages->blockId ?>">
	<div class="container">
		<div class="content">

            <? if($advantages->title): ?>
			<div class="page-subsection text-align--center">
				<h2><?= $advantages->title ?></h2>
			</div>
            <? endif ?>

			<? if($advantages->images): ?>
                <? include __DIR__."/advantages-images.php" ?>
            <? endif ?>

            <? if($advantages->items): ?>
				<? include __DIR__."/advantages-items.php" ?>
            <? endif ?>

			<? if($button->isExist()): ?>
                <div class="page-subsection">
                <div class="grid grid--justify-center">
                    <div class="grid__cell grid__cell--xs-auto">
                        <a
                            class="btn btn--wide"
                            href="<?= $button->link ?>"
							<?= $button->isFancybox ? "data-fancybox" : "" ?>
                        ><?= $button->text ?></a>
                    </div>
                </div>
            </div>
			<? endif ?>

		</div>
	</div>
</section>
