<? /** @var $general Medreclama\Landing\General */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $general->button ?>
<h2 class="page-block__header"><?= $general->title ?></h2>

<? if($general->isSingleImage()): ?>
<div class="page-block__image hidden-hd hidden-xl hidden-l">
    <img src="<?= $general->getSingleImage() ?>" alt=""/>
</div>
<? endif ?>

<? if($general->text): ?>
    <div class="page-block__body"><?= $general->text ?></div>
<? endif ?>

<? if($button->isExist()): ?>
    <a
        class="btn page-block__button"
        href="<?= $button->link ?>"
		<?= $button->isFancybox ? "data-fancybox" : "" ?>
    ><?= $button->text ?></a>
<? endif ?>
