<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?echo $arResult["PREVIEW_TEXT"];?>
<div class="ourLicenses">
	<div class="ourLicenses__img" style="background-image:url(<?=$arResult["DISPLAY_PROPERTIES"]['licenses_about']['FILE_VALUE']['SRC']?>);"></div>
	<div class="ourLicenses__text">
		<div class="ourLicenses__title title beforeLine">
			<?=$arResult["DISPLAY_PROPERTIES"]['header_about']['VALUE']?>
		</div>
		<div class="ourLicenses__desc"><?=$arResult["DISPLAY_PROPERTIES"]['text_about']['DISPLAY_VALUE']?></div>
		<a class="ourLicenses__link more" href="<?=$arResult["DISPLAY_PROPERTIES"]['link_about']['DISPLAY_VALUE']?>">Смотреть</a>
	</div>
</div>
<?echo $arResult["DETAIL_TEXT"];?>