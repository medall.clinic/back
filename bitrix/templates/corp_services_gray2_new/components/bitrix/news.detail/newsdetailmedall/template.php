<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="mainContent__title__desc">
	<?$res = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
	if($ar_res = $res->GetNext()){?>
		<p><?echo $ar_res['NAME'];?></p>
	<?}?>
</div>
<div class="mainContent__title title">
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h1><?=$arResult["NAME"]?></h1>
	<?endif;?>
</div>
<div class="mainContent__title__desc">
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<p class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></p>
	<?endif;?>
</div>
<div class="mainContent__text">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if(strlen($arResult["DETAIL_TEXT"])>0){?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?}?>
	<div class="articleCardBlock__hashTag">
		<?foreach($arResult["FIELDS"] as $code=>$value):?>
			<a href="/search/index.php?tags=<?=$value?>" target=_blank># <?echo $value;?></a>
		<?endforeach;?>
	</div>
	<a class="articleBack" href="<?=$arResult["LIST_PAGE_URL"]?>">Назад</a>
</div>