<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="innerPageContent discountCardPage">
	<div class="container">
		<div class="breadCrumbs">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0"
			));?>
		</div>
		<div class="mainContent__title title">
			<h1><?=$arResult["NAME"]?></h1>
		</div>
		<div class="mainContent__title__desc">
			<p><?=$arResult["DATE_ACTIVE_FROM"]?>-<?=$arResult["DATE_ACTIVE_TO"]?></p>
		</div>
		<div class="innerPageContent__mainBlock">
			<div class="discountCardPage__content">
				<div class="discountCardPage__img">
					<img src="<?=$arResult["DISPLAY_PROPERTIES"]['img_main_action']['FILE_VALUE']['SRC']?>" alt="картинка">
				</div>
				<div class="discountCardPage__text">
					<?echo $arResult["DETAIL_TEXT"];?>
					<?if($arResult["DISPLAY_PROPERTIES"]['prices_action']['VALUE']!=''){?>
						<div class="discountCardPage__title greyTitle">Акционные цены</div>
						<div class="discountCardPage__blockPrice">
							<?foreach($arResult["DISPLAY_PROPERTIES"]['prices_action']['VALUE'] as $k => $v){
								$res = CIBlockElement::GetList(Array(), Array("ID"=>$v));
								if ($ob = $res->GetNextElement()){
									$arFields = $ob->GetFields(); // поля элемента
									$arProps = $ob->GetProperties(); // свойства элемента
									?><div class="discountCardPage__blockPrice__item">
										<div class="discountCardPage__blockPrice__left">
											<p><?=$arFields['NAME']?></p>
										</div>
										<div class="discountCardPage__blockPrice__right">
											<p><span class="lineThrough"><?=$arProps['old_price']['VALUE']?></span> <?=$arProps['new_price']['VALUE']?></p>
										</div>
									</div><?
								}
							}?>	
						</div>
					<?}?>
					<?if($arResult["DISPLAY_PROPERTIES"]['moreprocedure_action']['VALUE']!=''){?>
						<div class="discountCardPage__link">
							<a class="button" href="<?=$arResult["DISPLAY_PROPERTIES"]['moreprocedure_action']['VALUE']?>">Подробнее о процедуре</a>
						</div>
					<?}?>
					<a class="articleBack" href="/stock/">Назад</a>
				</div>
			</div>
		</div>
	</div>
</div>