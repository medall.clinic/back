<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="linksBlock linksBlockAbout">
<?foreach($arResult["ROWS"] as $arItems):?>
	<?foreach($arItems as $arItem):?>
		<?if(is_array($arItem)):?>
			<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));?>
			<a class="linksBlock__item" href="<?=$arItem['DISPLAY_PROPERTIES']['URL']['VALUE']?>">
				<!-- <span class="linksBlock__item__icon" style="background-image: url(<?= $arItem["PICTURE"]["SRC"] ?>); "></span> -->
        <div class="linksBlock__item__icon">
          <picture>
            <source srcset="<?= WebPHelper($arItem["PICTURE"]["SRC"]) ?>" type="image/webp">
            <img src="<?= $arItem["PICTURE"]["SRC"] ?>" alt="<?echo $arItem['PREVIEW_TEXT']?>">
          </picture>
        </div>

				<span class="linksBlock__item__title">
					<span><?echo $arItem['PREVIEW_TEXT']?></span>
				</span>
			</a>
		<?endif;?>
	<?endforeach?>
<?endforeach?>
</div>

<? /* <?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); */ ?>
<? /** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */ ?>
<? /*$this->setFrameMode(true);
?>
<div class="linksBlock linksBlockAbout">
<?foreach($arResult["ROWS"] as $arItems):?>
	<?foreach($arItems as $arItem):?>
		<?if(is_array($arItem)):?>
			<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));?>
			<a class="linksBlock__item" href="<?=$arItem['DISPLAY_PROPERTIES']['URL']['VALUE']?>">
				<span class="linksBlock__item__icon" style="background-image: url(<?=$arItem["PICTURE"]["SRC"]?>);"></span>
				<span class="linksBlock__item__title">
					<span><?echo $arItem['PREVIEW_TEXT']?></span>
				</span>
			</a>
		<?endif;?>
	<?endforeach?>
<?endforeach?>
</div> */ ?>
