<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$print='';
$j=0;
$url = explode('/',$_SERVER['REQUEST_URI']);
foreach($arResult["ROWS"] as $arItems){
	foreach($arItems as $arItem){
		if(is_array($arItem)){
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));
			if($arItem['PROPERTIES']['direction_doctor']['VALUE_XML_ID'] == $url[1]){
				$j++;
				if($j==1){$print.='<div class="mainContent__ArticleBlock__doctor__slider__item">';}
				$print.='<div class="mainContent__ArticleBlock__doctor">
					<div class="mainContent__ArticleBlock__doctor__img">
						<img src="'.$arItem["PICTURE"]["SRC"].'" alt="картинка">
					</div>
					<div class="mainContent__ArticleBlock__doctor__content">
						<div class="mainContent__ArticleBlock__doctor__name">
							<p><span>'.$arItem["NAME"].'</span> '.$arItem['PROPERTIES']['io_doctor']['VALUE'].'</p>
						</div>
						<div class="mainContent__ArticleBlock__doctor__desc">
							<p>'.$arItem['PREVIEW_TEXT'].'</p>
						</div>
					</div>
				</div>';
				if($j==3){
					$print.= '</div>';
					$j=0;
				}
			}
		}
	}
}
print $print;
?>