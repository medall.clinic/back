$(document).ready(function() {
    $("form.contacts_to_bitrix24").on("submit", addContactsToBitrix24);
    $("form").on("submit", function(){
        fbq("track", "Lead");
    });
});

function addContactsToBitrix24(e)
{
    e.preventDefault();
    var form = $(e.target);

    var serializedForm = form.serialize();

    var localStorageUtmSource = getLocalStorageUtmValue("utm_source");
    if(localStorageUtmSource && isFormNotHasUtmTag(serializedForm, "utm_source")){
        serializedForm += "&utm_source=" + localStorageUtmSource;
    }

    var localStorageUtmMedium = getLocalStorageUtmValue("utm_medium");
    if(localStorageUtmMedium && isFormNotHasUtmTag(serializedForm, "utm_medium")){
        serializedForm += "&utm_medium=" + localStorageUtmMedium;
    }

    var localStorageUtmTerm = getLocalStorageUtmValue("utm_term");
    if(localStorageUtmTerm && isFormNotHasUtmTag(serializedForm, "utm_term")){
        serializedForm += "&utm_term=" + localStorageUtmTerm;
    }

    $.post(
        "/bitrix24/add_contacts.php",
        serializedForm,
        function (response) {
            console.log(response)
            if (response.send) {
                console.log('Создан лид "Контактные данные с лэндинга"');
                var successText = "<p>Ваши контактные данные отправлены!</p>";
                successText += "<p>В ближайшее время с Вами свяжутся менеджеры клиники.</p>";
                form.html(successText);
            }
        },
        "json"
    )
    .fail(function (response) {
        console.log("Request fail: " + response);
    });

}

function getLocalStorageUtmValue(utmKey)
{
    var b24CrmGuestUtm = localStorage.getItem("b24_crm_guest_utm");

    if(!b24CrmGuestUtm){
        return false;
    }

    b24CrmGuestUtm = $.parseJSON(b24CrmGuestUtm);
    if(!b24CrmGuestUtm.list){
        return false;
    }

    if(!b24CrmGuestUtm.list[utmKey]){
        return false;
    }

    return b24CrmGuestUtm.list[utmKey];
}

function isFormNotHasUtmTag(serializedForm, utmKey)
{
    return serializedForm.indexOf(utmKey) == -1;
}