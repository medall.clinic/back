        <footer class="footer container" id="section-contacts">
            <div class="content">
				<? include __DIR__."/include/footer/logo.php" ?>
				<? include __DIR__."/include/footer/blocks.php" ?>
				<? include __DIR__."/include/footer/disclaimer.php" ?>
            </div>
        </footer>

    </body>

    <script src="<?= SITE_TEMPLATE_PATH ?>/scripts/jquery-3.2.1.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/scripts/bitrix24.js?v=3"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/scripts/script.js?v=9"></script>

    <? include $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/calltouch.html" ?>
	<!--calltouch request-->
	<script>
		jQuery(document).on('mouseup touchend', 'form [type="submit"]', function () {
			var form = jQuery(this).closest('form');
			var fio = form.find('[name="name"]').val();
			var phone = form.find('[name="phone"]').val();
			var email = form.find('[name="email"]').val();
			var comment = form.find('[name="comment"]').val();
			var ct_site_id = window.ct('calltracking_params', 'k0satx2r').siteId;
			var sub = 'Заявка с ' + location.hostname;
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				email: email,
				comment: comment,
				subject: sub,
				requestUrl: location.href,
				sessionId: window.ct('calltracking_params', 'k0satx2r').sessionId
			};
	
			if ((!!phone || !!email) && window.ct_snd_flag != 1) {
				window.ct_snd_flag = 1; setTimeout(function () { window.ct_snd_flag = 0; }, 60000);
				jQuery.ajax({
					url: 'https://api.calltouch.ru/calls-service/RestAPI/requests/' + ct_site_id + '/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		var _ctreq_b24 = function (data) {
			var sid = window.ct('calltracking_params', 'k0satx2r').siteId;
			var request = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
			var post_data = Object.keys(data).reduce(function (a, k) { if (!!data[k]) { a.push(k + '=' + encodeURIComponent(data[k])); } return a }, []).join('&');
			var url = 'https://api.calltouch.ru/calls-service/RestAPI/' + sid + '/requests/orders/register/';
			request.open("POST", url, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); request.send(post_data);
		};
		window.addEventListener('b24:form:submit', function (e) {
			var form = event.detail.object;
			if (form.validated) {
				var fio = ''; var phone = ''; var email = ''; var comment = '';
				form.getFields().forEach(function (el) {
					if (el.name == 'LEAD_NAME' || el.name == 'CONTACT_NAME') { fio = el.value(); }
					if (el.name == 'LEAD_PHONE' || el.name == 'CONTACT_PHONE') { phone = el.value(); }
					if (el.name == 'LEAD_EMAIL' || el.name == 'CONTACT_EMAIL') { email = el.value(); }
					if (el.name == 'LEAD_COMMENTS' || el.name == 'DEAL_COMMENTS ') { comment = el.value(); }
				});
				var sub = form.title || 'Заявка с формы Bitrix24 ' + location.hostname;
				var ct_data = { fio: fio, phoneNumber: phone, email: email, comment: comment, subject: sub, requestUrl: location.href, sessionId: window.ct('calltracking_params', 'k0satx2r').sessionId };
				console.log(ct_data);
				if (!!phone || !!email) _ctreq_b24(ct_data);
			}
		});
	</script>
	<!--calltouch request-->
</html>
