<?
$elementCode = $_REQUEST["ELEMENT_CODE"];
$elementCodeExplode = explode('?', $elementCode);
$elementCode = reset($elementCodeExplode);

$arOrder = [];
$arFilter = [
	"IBLOCK_ID" => IBLOCK_ID_LANDINGS,
	"CODE" => $elementCode,
];
$arGroupBy = false;
$arNavStartParams  = [];
$arSelectFields = [
	"IBLOCK_ID",
	"ID",
	"PROPERTY_COUNTERS",
];
CModule::IncludeModule("iblock");
$res_elements = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

$elements = [];
$arElement = $res_elements->Fetch();
if($arElement && !empty($arElement["PROPERTY_COUNTERS_VALUE"]["TEXT"])){
	$countersText = $arElement["PROPERTY_COUNTERS_VALUE"]["TEXT"];
	$countersText = str_replace('<script ', '<script data-skip-moving="true" ', $countersText);
	echo $countersText;
}
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
	 (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	 m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	 (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	 ym(67423126, "init", {
				clickmap:true,
				trackLinks:true,
				accurateTrackBounce:true,
				webvisor:true
	 });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/67423126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- calltouch -->
<script>
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)},m=typeof Array.prototype.find === 'function',n=m?"init-min.js":"init.js";s.async=true;s.src="https://mod.calltouch.ru/"+n+"?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","k0satx2r");
console.log('counters.php');
</script>
<!-- calltouch -->