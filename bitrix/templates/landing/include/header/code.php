<?
$elementCode = $_REQUEST["ELEMENT_CODE"] ? $_REQUEST["ELEMENT_CODE"] : $_REQUEST["CODE"];
$elementCodeExplode = explode('?', $elementCode);
$elementCode = reset($elementCodeExplode);

CModule::IncludeModule("iblock");
$res_elements = CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => IBLOCK_ID_LANDINGS,
        "CODE" => $elementCode,
    ],
    false,
    [],
    [
        "IBLOCK_ID",
        "ID",
        "PROPERTY_CODE_IN_HEAD",
    ]
);

$elements = [];
$codeText = '';
$arElement = $res_elements->Fetch();
if ($arElement && !empty($arElement["PROPERTY_CODE_IN_HEAD_VALUE"]["TEXT"])) {
    $codeText = $arElement["PROPERTY_CODE_IN_HEAD_VALUE"]["TEXT"];
    $codeText = str_replace('<script ', '<script data-skip-moving="true" ', $codeText);
    //echo $codeText;
}
?>
