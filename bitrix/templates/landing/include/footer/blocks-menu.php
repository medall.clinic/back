<div class="grid__cell grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
	<ul class="footer__menu">
		<li><a href="/plastic/">Пластическая хирургия</a></li>
		<li><a href="/cosmetology/">Косметология</a></li>
		<li><a href="/dentistry/">Стоматология</a></li>
		<li><a href="https://netvolos.ru/">Пересадка волос</a></li>
		<li><a href="https://yazv.net/">Флебология</a></li>
	</ul>
</div>