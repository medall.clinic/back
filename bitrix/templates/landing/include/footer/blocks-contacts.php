<div class="grid__cell grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12 footer__contacts">

	<h3 class="small">Контакты</h3>

	<p>
        <strong>Адрес:</strong> Левашовский пр., д.24,<br>
        500 м от ст. Чкаловская<br>
        <strong>WhatsApp:</strong> +7 921 441-39-26<br>

        <strong>Телефон:</strong>+7 (812) 603-02-01<br>

        <strong>Email:</strong> admin@medall.clinic<br>
        <strong>Instagram:</strong> @medall.clinic
    </p>

</div>