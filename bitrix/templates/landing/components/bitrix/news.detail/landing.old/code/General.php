<?
namespace Medreclama\Landing;

class General extends Landing
{
	public $images;
	public $text;
	public $isImagesOrFormOnLeft;
	public $isFormEnabled;
	public $isHideTextBlock;

	public $outerBlockClass;
	public $imageBlockClass;
	public $textBlockClass;
	public $formBlockClass;

	public $formUtmTags;
	public $formTitle;

	public $additionalText;

	public function __construct($elementId)
	{
		$element = $this->getElement($elementId);

		if(!$element){
			return null;
		}

		$this->title = $element->props["TITLE"]["~VALUE"];
		$this->text = $element->props["TEXT"]["~VALUE"] ? $element->props["TEXT"]["~VALUE"]["TEXT"] : false;
		$this->blockId = $this->getBlockId($element->props["BLOCK_ID"]);
		$this->menuItem = new MenuItem($this->landingProps["MENU_TITLE"]["VALUE"], $this->landingProps["MENU_LINK"]["VALUE"], $this->blockId);
		$this->images = $this->getImages($element->props["IMAGES"]["VALUE"]);
		$this->isImagesOrFormOnLeft = $element->props["IMAGES_OR_FORM_POSITION"]["VALUE"] != "справа";
		$this->isFormEnabled = (bool)$element->props["IS_FORM_ENABLED"]["VALUE"];
		$this->isHideTextBlock = $this->isHideTextBlock();

		$this->outerBlockClass = $this->getOuterBlockClass($element);
		$this->imageBlockClass = $this->getImageBlockClass();
		$this->textBlockClass = $this->getTextBlockClass();
		$this->formBlockClass = $this->getFormBlockClass();

		$this->formUtmTags = $this->getFormUtmTags();
		$this->formTitle = $this->getFormTitle($element->fields["NAME"]);

		$this->button = new Button($element->props["BUTTON_TEXT"], $element->props["BUTTON_LINK"], $element->props["BUTTON_FANCYBOX"]);

		$this->additionalText = $this->getAdditionalText($element->props["ADDITIONAL_TEXT"]);

		$this->isShow = $this->isShow();
	}

	public function isSingleImage(): bool
	{
		if(!$this->images){
			return false;
		}

		return count($this->images) == 1;
	}

	public function isMultipleImages(): bool
	{
		if(!$this->images){
			return false;
		}

		return count($this->images) > 1;
	}

	public function getSingleImage(): string
	{
		return reset($this->images);
	}

	private function getColor($propColorValue): string
	{
		if($propColorValue == "светлый"){
			return " page-section--color page-section--color-light";
		}

		if($propColorValue == "тёмный"){
			return " page-section--color page-section--color-dark";
		}

		return "";
	}

	private function getOuterBlockClass($element): string
	{
		$class = "page-section page-block";
		$class .= $this->getColor($element->props["BG_COLOR"]["VALUE"]);
		if(!$this->text){
			$class .= " page-block--no-body";
		}
		if($element->props["ADD_CLASS"]["~VALUE"]){
			$class .= " ".$element->props["ADD_CLASS"]["~VALUE"];
		}

		return $class;
	}

	private function getImageBlockClass(): string
	{
		$class = "page-block__image grid__cell";

		if($this->isHideTextBlock){
			$class .= " grid__cell--l-10 grid__cell--xs-12";
		}else{
			$class .= " grid__cell--l-5 grid__cell--m-6 grid__cell--xs-12";
		}

		if($this->isSingleImage()){
			$class .= " hidden-m hidden-s hidden-xs";
		}

		if($this->isMultipleImages()){
			$class .= " page-block__image--slider";
		}

		return $class;
	}

	private function getTextBlockClass(): string
	{
		$class = "page-block__info grid__cell";

		if($this->images OR $this->isFormEnabled){
			$class .= " grid__cell--l-7 grid__cell--m-6 grid__cell--xs-12";
		}else{
			$class .= " grid__cell--l-10 grid__cell--xs-12";
		}

		return $class;
	}

	private function getFormBlockClass(): string
	{
		return "page-block__form grid__cell grid__cell--l-5 grid__cell--m-6 grid__cell--xs-12";
	}

	private function getFormUtmTags(): array
	{
		if(empty($_GET)){
			return [];
		}

		$utmTags = [];
		foreach ($_GET as $paramName => $paramValue){
			if(strpos($paramName, 'utm_') !== false){
				$utmTags[] = new UtmTag($paramName, $paramValue);
			}
		}

		return $utmTags;
	}

	private function getFormTitle($name): string
	{
		global $APPLICATION;
		$curPage = $APPLICATION->GetCurPage();

		$title = (strpos($curPage, "landings") !== false) ? "лендинг" : "страница услуги";

		$title .= " - ".$curPage;
		$title .= ": ";
		$title .= "Блок страницы - ".$name;

		return $title;
	}

	private function getAdditionalText(array $prop): string
	{
		return $prop["~VALUE"] ? $prop["~VALUE"]["TEXT"] : "";
	}

	private function isHideTextBlock(): bool
	{
		return empty($this->text) && !empty($this->images);
	}

	private function isShow(): bool
	{
		if(empty($this->title) && empty($this->text) && empty($this->images)){
			return false;
		}

		return true;
	}

}
