<?
namespace Medreclama\Landing;

class Comfort extends Landing
{
	public $text;
	public $images;

	public function __construct($arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["COMFORT_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["COMFORT_BLOCK_ID"]);
		$this->text = $this->landingProps["COMFORT_TEXT"]["~VALUE"] ? $this->landingProps["COMFORT_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->images = $this->getImages($this->landingProps["COMFORT_IMAGES"]["VALUE"]);
		$this->button = new Button($this->landingProps["COMFORT_BUTTON_TEXT"], $this->landingProps["COMFORT_BUTTON_LINK"], $this->landingProps["COMFORT_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["COMFORT_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["COMFORT_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["COMFORT_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["COMFORT_MENU_TITLE"]["VALUE"], $this->landingProps["COMFORT_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(
			!$this->title &&
			!$this->text &&
			!$this->images
		){
			return false;
		}

		return true;
	}

}
