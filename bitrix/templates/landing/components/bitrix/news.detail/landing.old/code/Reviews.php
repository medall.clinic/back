<?
namespace Medreclama\Landing;

class Reviews extends Landing
{
	public $description;
	public $items;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["REVIEWS_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["REVIEWS_BLOCK_ID"]);
		if(!empty($this->landingProps["REVIEWS_DESCRIPTION"]["~VALUE"]["TEXT"])){
			$this->description = $this->landingProps["REVIEWS_DESCRIPTION"]["~VALUE"]["TEXT"];
		}
		$this->items = $this->getItems($this->landingProps["REVIEWS_ITEMS"]["VALUE"], $this->landingProps["REVIEWS_IMAGES"]["VALUE"]);

		$this->button = new Button($this->landingProps["REVIEWS_BUTTON_TEXT"], $this->landingProps["REVIEWS_BUTTON_LINK"], $this->landingProps["REVIEWS_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["REVIEWS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["REVIEWS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["REVIEWS_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["REVIEWS_MENU_TITLE"]["VALUE"], $this->landingProps["REVIEWS_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems($elementsId, $imagesId)
	{
		if(empty($elementsId) AND empty($imagesId)){
			return false;
		}

		$items = [];

		if($elementsId){
			$elements = $this->getElements($elementsId);
			if($elements){
				/** @var Element $element */
				foreach ($elements as $element){
					$item = new Review;
					$item->videoId = $element->props["YOUTUBE_ID"]["VALUE"];
					$item->picture = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
					$item->name = $element->props["NAME"]["~VALUE"];
					$item->description = $element->fields["~PREVIEW_TEXT"];
					$items[] = $item;
				}
			}
		}

		if($imagesId){
			$imagesSrc = $this->getImages($imagesId);
			/** @var Element $element */
			foreach ($imagesSrc as $imageSrc){
				$item = new Review;
				$item->picture = $imageSrc;
				$items[] = $item;
			}
		}

		return $items;
	}

	private function isShow()
	{
		if(!$this->items){
			return false;
		}

		return true;
	}
}
