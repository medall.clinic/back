<?
namespace Medreclama\Landing;

class Banner extends Landing
{
	public $subtitle;
	public $image;
	public $text;
	public $price;
	public $isPriceFrom;
	public $oldButton;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["BANNER_TITLE"]["~VALUE"];
		$this->subtitle = $this->landingProps["BANNER_SUBTITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["BANNER_BLOCK_ID"]);
		$this->image = $this->getImageSrc($this->landingProps["BANNER_IMAGE"]["VALUE"]);
		$this->text = $this->landingProps["BANNER_TEXT"]["~VALUE"] ? $this->landingProps["BANNER_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->price = \StringsHelper::numberFormat($this->landingProps["BANNER_PRICE"]["VALUE"]);
		$this->isPriceFrom = (bool)$this->landingProps["BANNER_IS_PRICE_FROM"]["~VALUE"];
		$this->button = new Button($this->landingProps["BANNER_BUTTON_TEXT"], $this->landingProps["BANNER_BUTTON_LINK"], $this->landingProps["BANNER_BUTTON_FANCYBOX"]);
		$this->oldButton = $this->landingProps["BANNER_BUTTON"]["VALUE"];
		$this->additionalText = $this->landingProps["BANNER_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["BANNER_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;
		$this->generalBlockId = $this->landingProps["BANNER_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["BANNER_MENU_TITLE"]["VALUE"], $this->landingProps["BANNER_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function isShow()
	{
		if(
			!$this->title &&
			!$this->image &&
			!$this->text &&
			!$this->price &&
			!$this->oldButton
		){
			return false;
		}

		return true;
	}

}
