<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<div class="page-subsection">
	<div class="banner-team__names-slider">
		<div class="splide">
			<div class="splide__track">
				<ul class="splide__list">
					<? foreach ($doctors->items as $doctor): ?>
                        <? include __DIR__."/doctors-block.php" ?>
					<? endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</div>
