<? /** @var $link Medreclama\Landing\Link */ ?>
<? /** @var $links Medreclama\Landing\Links */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $links->button ?>
<div class="page-section" id="<?= $links->blockId ?>">
    <div class="container">
        <div class="content">

            <? if($links->title): ?>
            <div class="page-subsection">
                <h2 class="text-align--center"><?= $links->title ?></h2>
            </div>
            <? endif ?>

            <div class="page-subsection">
                <div class="grid grid--padding-y grid--justify-center">
                    <? foreach ($links->items as $link): ?>
                    <a
                        class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12 link-block"
                        href="<?= $link->link ?>"
                    >
                        <p class="link-block__image">
                            <img src="<?= $link->image ?>" alt="<?= $link->text ?>" title="<?= $link->text ?>" style="display: block">
                            <span class="btn link-block__button">Подробнее</span>
                        </p>
                        <p><?= $link->text ?></p>
                    </a>
                    <? endforeach ?>
                </div>
            </div>

			<? if($button->isExist()): ?>
                <div class="page-subsection">
                    <div class="grid grid--justify-center">
                        <div class="grid__cell grid__cell--xs-auto">
                            <a
                                class="btn btn--wide"
                                href="<?= $button->link ?>"
								<?= $button->isFancybox ? "data-fancybox" : "" ?>
                            ><?= $button->text ?></a>
                        </div>
                    </div>
                </div>
			<? endif ?>

        </div>
    </div>
</div>
