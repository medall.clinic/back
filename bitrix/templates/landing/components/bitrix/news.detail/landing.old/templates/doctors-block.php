<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<li class="splide__slide">
	<div class="banner-team__names-slide">
		<div class="banner-team__name"><?= $doctor->name ?></div>

		<? if($doctor->description): ?>
            <div class="banner-team__description">
				<?= $doctor->description ?>
                <? if(!empty($doctor->link) && !empty($doctor->shortLink)): ?>
                    <br><a href="<?= $doctor->link ?>"><?= $doctor->shortLink ?></a>
                <? endif ?>
            </div>
		<? endif ?>

		<? if(!empty($doctor->link)): ?>
            <? if(empty($doctor->shortLink)): ?>
                <div class="banner-team__button">
                    <a class="btn" href="<?= $doctor->link ?>">Подробнее</a>
                </div>
            <? endif ?>
		<? else: ?>
            <div class="banner-team__button">
                <a class="btn" href="#landing-form">Записаться</a>
            </div>
		<? endif ?>

    </div>
</li>

<? if($doctor->hasDetails()): ?>
<div class="banner-team__pop-up" id="doctor-<?= $doctor->id ?>">

	<div class="banner-team__pop-up-close" data-fancybox-close></div>

	<div class="grid grid--nopadding">

		<div class="grid__cell grid__cell--m-5 grid__cell--xs-12">
			<? include __DIR__."/doctors-block-info.php" ?>
		</div>

		<div class="grid__cell grid__cell--m-7 grid__cell--xs-12">
			<? include __DIR__."/doctors-block-details.php" ?>
		</div>

	</div>
</div>
<? endif ?>
