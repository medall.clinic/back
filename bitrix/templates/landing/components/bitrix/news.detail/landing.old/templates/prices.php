<? /** @var $prices Medreclama\Landing\Prices */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $prices->button ?>
<section class="page-section page-section--text-align-center" id="<?= $prices->blockId ?>">
	<div class="container">
		<div class="content">

            <? if($prices->title): ?>
			<div class="page-subsection text-align--center">
				<h2><?= $prices->title ?></h2>
			</div>
            <? endif ?>

			<? if($prices->text): ?>
			<div class="page-subsection">
				<?= $prices->text ?>
			</div>
			<? endif ?>

            <? if($prices->complexItems): ?>
                <? include __DIR__."/prices-complex.php" ?>
            <? endif ?>

            <? if($prices->simpleItems): ?>
                <? include __DIR__."/prices-simple.php" ?>
            <? endif ?>

            <? if($prices->text2): ?>
			<div class="page-subsection">
                <?= $prices->text2 ?>
			</div>
			<? endif ?>

			<? if($button->isExist()): ?>
            <div class="page-subsection">
                <div class="grid grid--justify-center">
                    <div class="grid__cell grid__cell--xs-auto">
                        <a
                            class="btn btn--wide"
                            href="<?= $button->link ?>"
							<?= $button->isFancybox ? "data-fancybox" : "" ?>
                        ><?= $button->text ?></a>
                    </div>
                </div>
            </div>
			<? endif ?>

		</div>
	</div>
</section>
