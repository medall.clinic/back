<? /** @var $reason Medreclama\Landing\Reason */ ?>
<? /** @var $reasons Medreclama\Landing\Reasons */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $reasons->button ?>
<section class="page-section page-section--reasons page-section--text-align-center" id="<?= $reasons->blockId ?>">
	<div class="container">
		<div class="content">

            <? if($reasons->title): ?>
			<div class="page-subsection text-align--center">
				<h2><?= $reasons->title ?></h2>
			</div>
            <? endif ?>

            <? if($reasons->text): ?>
			<div class="page-subsection">
                <?= $reasons->text ?>
			</div>
            <? endif ?>

            <? if($reasons->items): ?>
			<div class="page-subsection">
				<ul class="grid grid--nopadding reasons">

                    <? foreach ($reasons->items as $reason): ?>

                        <? if($reason->image): ?>
                            <? include __DIR__."/reason-with-image.php" ?>
                        <? else: ?>
                            <? if(!$reason->isSmallSize): ?>
								<? include __DIR__."/reason-wo-image-big.php" ?>
                            <? else: ?>
								<? include __DIR__."/reason-wo-image-small.php" ?>
                            <? endif ?>
                        <? endif ?>

                    <? endforeach ?>

				</ul>
			</div>
            <? endif ?>

			<? if($reasons->hasListItems()): ?>
            <div class="page-subsection">
                <div class="grid grid--padding-y grid--justify-center hidden-xs hidden-s hidden-m">

					<? if($reasons->listItemsPros): ?>
                    <div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
                        <div class="indications">
                            <div class="indications__title">Показания</div>
                            <ul class="indications__list">
                                <? foreach ($reasons->listItemsPros as $listItemPro): ?>
                                    <li class="indications__item"><?= $listItemPro ?></li>
                                <? endforeach ?>
                            </ul>
                        </div>
                    </div>
                    <? endif ?>

					<? if($reasons->listItemsCons): ?>
                    <div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
                        <div class="indications indications--contraindications">
                            <div class="indications__title">Противопоказания</div>
                            <ul class="indications__list">
								<? foreach ($reasons->listItemsCons as $listItemCon): ?>
                                    <li class="indications__item"><?= $listItemCon ?></li>
								<? endforeach ?>
                            </ul>
                        </div>
                    </div>
                    <? endif ?>

                </div>
                <div class="slider slider--simple slider--indications splide hidden-l hidden-xl hidden-hd">
                  <div class="splide__track">
                    <ul class="splide__list">
                    <? if($reasons->listItemsPros): ?>
                        <li class="splide__slide">
                          <div class="indications">
                            <div class="indications__title">Показания</div>
                            <ul class="indications__list">
                              <? foreach ($reasons->listItemsPros as $listItemPro): ?>
                                  <li class="indications__item"><?= $listItemPro ?></li>
                              <? endforeach ?>
                            </ul>
                          </div>
                        </li>
                      <? endif ?>
                      <? if($reasons->listItemsCons): ?>
                        <li class="splide__slide">
                          <div class="indications indications--contraindications">
                            <div class="indications__title">Противопоказания</div>
                            <ul class="indications__list">
                              <? foreach ($reasons->listItemsCons as $listItemCon): ?>
                                <li class="indications__item"><?= $listItemCon ?></li>
                              <? endforeach ?>
                            </ul>
                          </div>
                        </li>
                      <? endif ?>
                    </ul>
                  </div>
                </div>
            </div>
            <? endif ?>

            <? if($button->isExist()): ?>
            <div class="page-subsection">
                <div class="grid grid--justify-center">
                    <div class="grid__cell grid__cell--xs-auto">
                        <a
                            class="btn btn--wide"
                            href="<?= $button->link ?>"
							<?= $button->isFancybox ? "data-fancybox" : "" ?>
                        ><?= $button->text ?></a>
                    </div>
                </div>
            </div>
            <? endif ?>

		</div>
	</div>
</section>
