<? /** @var $advantages Medreclama\Landing\Advantages */ ?>
<div class="page-subsection">
    <div class="grid grid--nopadding grid--cols-10 gallery-advantages">
		<? foreach ($advantages->images as $k => $image): ?>
			<? if(!$k || $k == 2): ?>
                <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-5 grid__cell--xs-5 gallery-advantages__item">
                    <img src="<?= $image ?>" alt=""/>
                </div>
			<? else: ?>
                <div class="grid__cell grid__cell--l-4 grid__cell--m-4 grid__cell--s-10 grid__cell--xs-10 gallery-advantages__item">
                    <img src="<?= $image ?>" alt=""/>
                </div>
			<? endif ?>
		<? endforeach ?>
        <div class="grid__cell grid__cell--hd-2 grid__cell--xl-2 grid__cell--l-2 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden gallery-advantages__end">
        </div>
    </div>
</div>
