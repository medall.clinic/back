<? /** @var $general Medreclama\Landing\General */ ?>

<? if($general->isHideTextBlock): ?>
    <h2 class="page-block__header"><?= $general->title ?></h2>
<? endif ?>

<? if($general->isSingleImage()): ?>
    <img src="<?= $general->getSingleImage() ?>" alt="" />
<? else: ?>
    <div class="page-block__slider">
        <div class="splide">
            <div class="splide__track">
                <ul class="splide__list">
                    <? foreach ($general->images as $image): ?>
                        <li class="splide__slide">
                            <img src="<?= $image ?>" alt="" />
                        </li>
                    <? endforeach ?>
                </ul>
            </div>
        </div>
    </div>
<? endif ?>
