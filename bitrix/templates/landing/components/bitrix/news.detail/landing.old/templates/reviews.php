<? /** @var $review Medreclama\Landing\Review */ ?>
<? /** @var $reviews Medreclama\Landing\Reviews */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $reviews->button ?>
<section class="page-section page-section--text-align-center" id="<?= $reviews->blockId ?>">

	<? if($reviews->title): ?>
    <div class="page-subsection text-align--center">
        <div class="container">
            <div class="content">
                <h2><?= $reviews->title ?></h2>
                <?= $reviews->description ?>
            </div>
        </div>
    </div>
	<? endif ?>

	<? if($reviews->items): ?>
        <div class="page-subsection">
            <div class="stories splide">
                <div class="splide__track">
                    <ul class="splide__list">
						<? foreach($reviews->items as $review): ?>
							<? include __DIR__."/review.php" ?>
						<? endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
	<? endif ?>

	<? if($button->isExist()): ?>
        <div class="page-subsection">
            <div class="container">
                <div class="content">
                    <div class="grid grid--justify-center">
                        <div class="grid__cell grid__cell--xs-auto">
                            <a
                                class="btn btn--wide"
                                href="<?= $button->link ?>"
								<?= $button->isFancybox ? "data-fancybox" : "" ?>
                            ><?= $button->text ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<? endif ?>

</section>






