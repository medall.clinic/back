<? /** @var $doctor Medreclama\Landing\Doctor */ ?>
<div class="banner-team__info">

	<div class="banner-team__name"><?= $doctor->name ?></div>

	<? if($doctor->description): ?>
		<div class="banner-team__description"><?= $doctor->description ?></div>
	<? endif ?>

	<? if($doctor->workExperience): ?>
		<div class="banner-team__experience">Стаж работы <?= $doctor->workExperience ?></div>
	<? endif ?>

	<? if($doctor->detailImage): ?>
		<div class="banner-team__image">
			<img src="<?= $doctor->detailImage ?>" alt="<?= $doctor->name ?>">
		</div>
	<? endif ?>

</div>
