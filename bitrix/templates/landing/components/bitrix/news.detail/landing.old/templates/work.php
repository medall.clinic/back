<? /** @var $work Medreclama\Landing\Work */ ?>
<li class="splide__slide">

	<? if($work->image): ?>
		<div class="works__image">
			<img src="<?= $work->image ?>" alt="<?= $work->doctor ?>, <?= $work->title ?>">
		</div>
	<? endif ?>

	<? if($work->description1 && $work->description2): ?>
		<div class="grid grid--nopadding works__description">
			<div class="grid__cell grid__cell--l-6 grid__cell--xs-12"><?= $work->description1 ?></div>
			<div class="grid__cell grid__cell--l-6 grid__cell--xs-12"><?= $work->description2 ?></div>
		</div>
	<? endif ?>

	<? if($work->description1 && !$work->description2): ?>
		<div class="grid grid--nopadding works__description">
			<div class="grid__cell grid__cell--xs-12"><?= $work->description1 ?></div>
		</div>
	<? endif ?>

	<? if($work->doctor or $work->title): ?>
	<div class="works__info">
		<? if($work->doctor): ?>
			<strong>Врач:</strong> <?= $work->doctor ?><br>
		<? endif ?>
		<? if($work->title): ?>
			<strong>Было выполнено:</strong> <?= $work->title ?>
		<? endif ?>
	</div>
	<? endif ?>

</li>