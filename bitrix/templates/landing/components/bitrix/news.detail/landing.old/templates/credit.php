<? /** @var $credit Medreclama\Landing\Credit */ ?>
<section
    class="page-section page-section--gradient page-section--gradient-reverse page-section--credit"
    id="<?= $credit->blockId ?>"
>
    <div class="container">
        <div class="content">

            <div class="page-subsection text-align--center">

                <? if($credit->title): ?>
                    <h2><?= $credit->title ?></h2>
				<? endif ?>

				<? if($credit->text): ?>
					<?= $credit->text ?>
				<? endif ?>

            </div>

            <div class="page-subsection">
                <div class="grid grid--padding-y grid--nowrap-hd grid--nowrap-xl grid--nowrap-l">

                    <div class="grid__cell grid__cell--l-auto grid__cell--s-6 grid__cell--xs-12">
                        <div class="block-icon block-icon--partners">
                            <div class="block-icon__name">Банки-партнёры</div>
                            <div class="block-icon__desc">Средства рассрочки предоставляются банками-партнерами — <strong>Tinkoff, Credit EuropeBank, МТС банк, Ренессанс, Восточный экспресс банк.</strong></div>
                        </div>
                    </div>

                    <div class="grid__cell grid__cell--l-auto grid__cell--s-6 grid__cell--xs-12">
                        <div class="block-icon block-icon--sum">
                            <div class="block-icon__name">Сумма рассрочки</div>
                            <div class="block-icon__desc">Рассрочка  предоставляется на сумму <strong>200 000 рублей</strong>.</div>
                        </div>
                    </div>

                    <div class="grid__cell grid__cell--l-auto grid__cell--s-6 grid__cell--xs-12">
                        <div class="block-icon block-icon--timeline">
                            <div class="block-icon__name">Срок рассрочки</div>
                            <div class="block-icon__desc">Рассрочку можно оформить на срок <strong>до 11 месяцев</strong>.</div>
                        </div>
                    </div>

                    <div class="grid__cell grid__cell--l-auto grid__cell--s-6 grid__cell--xs-12">
                        <div class="block-icon block-icon--time">
                            <div class="block-icon__name">Время</div>
                            <div class="block-icon__desc">Ответ банка для рассмотрения/ предоставления рассрочки требует <strong>не более 30 минут</strong>.</div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

