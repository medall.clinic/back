<? /** @var $general Medreclama\Landing\General */ ?>
<div class="<?= $general->outerBlockClass ?>" id="<?= $general->blockId ?>">
	<div class="container">
		<div class="content">
			<div class="grid grid--padding-y grid--justify-center">

				<? if($general->images OR $general->isFormEnabled): ?>
					<? if($general->isImagesOrFormOnLeft): ?>
                        <? if($general->isFormEnabled): ?>
                            <div class="<?= $general->formBlockClass ?>">
                                <? include __DIR__."/general-form.php" ?>
                            </div>
                        <? else: ?>
                            <div class="<?= $general->imageBlockClass ?>">
								<? include __DIR__."/general-images.php" ?>
                            </div>
						<? endif ?>
                        <? if(!$general->isHideTextBlock): ?>
                            <div class="<?= $general->textBlockClass ?>">
                                <? include __DIR__."/general-text.php" ?>
                            </div>
                        <? endif ?>
					<? else: ?>
				        <? if(!$general->isHideTextBlock): ?>
                            <div class="<?= $general->textBlockClass ?>">
                                <? include __DIR__."/general-text.php" ?>
                            </div>
                        <? endif ?>
						<? if($general->isFormEnabled): ?>
                            <div class="<?= $general->formBlockClass ?>">
								<? include __DIR__."/general-form.php" ?>
                            </div>
						<? else: ?>
                            <div class="<?= $general->imageBlockClass ?>">
								<? include __DIR__."/general-images.php" ?>
                            </div>
						<? endif ?>
					<? endif ?>
				<? else: ?>
                    <? if(!$general->isHideTextBlock): ?>
                        <div class="<?= $general->textBlockClass ?>">
                            <? include __DIR__."/general-text.php" ?>
                        </div>
                    <? endif ?>
				<? endif ?>
			</div>
		</div>
	</div>
</div>
