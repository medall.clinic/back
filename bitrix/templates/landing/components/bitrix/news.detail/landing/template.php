<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Medreclama\Landing\Landing;
$this->setFrameMode(true);

$models = require __DIR__."/code/init.php";

/** @var $model Landing */
foreach ($models as $model) {
	echo $model->render();
}




