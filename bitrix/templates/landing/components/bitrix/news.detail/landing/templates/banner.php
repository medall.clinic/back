<? /** @var $banner Medreclama\Landing\Banner */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $banner->button ?>
<section class="page-section home-header" id="<?= $banner->blockId ?>">
	<div class="container">
		<div class="content">
			<div class="grid grid--align-center">

                <div class="grid__cell grid__cell--l-7 grid__cell--m-12 grid__cell--s-12 grid__cell--xs-12 home-header__info">

                    <? if($banner->title): ?>
                        <h1 class="home-header__title"><?= $banner->title ?></h1>
                    <? endif ?>

					<? if($banner->subtitle): ?>
                        <div class="home-header__subtitle"><?= $banner->subtitle ?></div>
					<? endif ?>

                    <? if($banner->text): ?>
                        <div class="home-header__body">
                            <?= $banner->text ?>
                        </div>
                    <? endif ?>

                    <? if($button->isExist()): ?>
                        <div class="home-header__button">
                            <a
                                class="btn"
                                href="<?= $button->link ?>"
                                <?= $button->isFancybox ? "data-fancybox" : "" ?>
                            ><?= $button->text ?></a>
                        </div>
                    <? endif ?>

					<? if($banner->oldButton): ?>
                        <div class="home-header__button">
                            <a class="btn" href="#section-contact"><?= $banner->oldButton ?></a>
                        </div>
					<? endif ?>

                </div>

                <div class="grid__cell grid__cell--l-5 grid__cell--m-12 grid__cell--s-12 grid__cell--xs-12 home-header__image">
                    <img src="<?= $banner->image ?>" alt="<?= $banner->title ?>">
                </div>

			</div>
		</div>
	</div>
</section>
