<? /** @var $history Medreclama\Landing\History */ ?>
<section
    class="page-section page-section--text-align-center"
    id="<?= $history->blockId ?>"
>
    <div class="container">

        <div class="content">

            <script data-skip-moving="true" async src="https://www.instagram.com/embed.js"></script>

            <? if($history->title): ?>
                <div class="page-subsection text-align--center">
                    <h2><?= $history->title ?></h2>
                </div>
			<? endif ?>

			<? if($history->items): ?>
                <div class="page-subsection">
                    <div class="grid grid--padding-y grid--justify-center">
						<? foreach ($history->items as $item): ?>
                            <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
								<?= $item ?>
                            </div>
						<? endforeach ?>
                    </div>
                </div>
			<? endif ?>

        </div>

    </div>
</section>
