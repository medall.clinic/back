<? /** @var $work Medreclama\Landing\Work */ ?>
<? /** @var $works Medreclama\Landing\Works */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $works->button ?>
<section class="page-section" id="<?= $works->blockId ?>">

    <div class="container">
        <div class="content">

            <? if($works->title): ?>
                <div class="page-subsection">
                    <h2 class="text-align-center"><?= $works->title ?></h2>
                </div>
            <? endif ?>

            <? if($works->items): ?>
                <div class="page-subsection">
                    <div class="works">
                        <div class="splide">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <? foreach ($works->items as $work): ?>
                                        <? include __DIR__."/work.php" ?>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif ?>

			<? if($button->isExist()): ?>
            <div class="page-subsection">
                <div class="grid grid--justify-center">
                    <div class="grid__cell grid__cell--xs-auto">
                        <a
                            class="btn btn--wide"
                            href="<?= $button->link ?>"
							<?= $button->isFancybox ? "data-fancybox" : "" ?>
                        ><?= $button->text ?></a>
                    </div>
                </div>
            </div>
			<? endif ?>

        </div>
    </div>

</section>
