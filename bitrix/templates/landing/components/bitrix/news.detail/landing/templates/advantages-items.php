<? /** @var $advantage Medreclama\Landing\Advantage */ ?>
<? /** @var $advantages Medreclama\Landing\Advantages */ ?>
<div class="page-subsection">
	<div class="grid grid--padding-y">
		<? foreach ($advantages->items as $advantage): ?>
			<div class="grid__cell grid__cell--m-6 grid__cell--s-12 grid__cell--xs-12">
				<h3 class="small"><?= $advantage->title ?></h3>
				<?= $advantage->text ?>
			</div>
		<? endforeach ?>
	</div>
</div>
