<? /** @var $comfort Medreclama\Landing\Comfort */ ?>
<section class="page-section page-section--color" id="<?= $comfort->blockId ?>">
    <div class="container">
        <div class="content">
            <div class="page-subsection">
                <div class="grid grid--padding-y grid--align-center">

                    <? if($comfort->images): ?>
                    <div class="grid__cell grid__cell--m-6 grid__cell--s-hidden grid__cell--xs-hidden">
                        <div class="slider slider--simple splide">
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <? foreach($comfort->images as $image): ?>
                                    <li class="splide__slide">
                                        <div class="slider__image">
                                            <img src="<?= $image ?>" alt="">
                                        </div>
                                    </li>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
					<? endif ?>

                    <div class="grid__cell grid__cell--m-6 grid__cell--xs-12">

                        <? if($comfort->title): ?>
                        <div class="page-subsection">
                            <h2><?= $comfort->title ?></h2>
                        </div>
                        <? endif ?>

                        <? if($comfort->images): ?>
                        <div class="page-subsection hidden-hd hidden-xl hidden-l hidden-m">
                            <div class="slider slider--simple splide">
                                <div class="splide__track">
                                    <ul class="splide__list">
                                        <? foreach($comfort->images as $image): ?>
                                        <li class="splide__slide">
                                            <div class="slider__image">
                                                <img src="<?= $image ?>" alt="">
                                            </div>
                                        </li>
										<? endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
						<? endif ?>

                        <? if($comfort->text): ?>
                        <div class="page-subsection">
							<?= $comfort->text ?>
                        </div>
                        <? endif ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>