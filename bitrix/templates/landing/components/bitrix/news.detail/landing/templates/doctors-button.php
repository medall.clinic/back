<? /** @var $doctors Medreclama\Landing\Doctors */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $button = $doctors->button ?>
<? if($button->isExist()): ?>
<div class="page-subsection">
	<div class="grid grid--justify-center">
		<div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-10 grid__cell--xs-12">
			<a
				class="btn"
				href="<?= $button->link ?>"
				<?= $button->isFancybox ? "data-fancybox" : "" ?>
			><?= $button->text ?></a>
		</div>
	</div>
</div>
<? endif ?>

