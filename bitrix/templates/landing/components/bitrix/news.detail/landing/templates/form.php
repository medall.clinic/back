<? /** @var $form Medreclama\Landing\Form */ ?>
<? /** @var $utmTag Medreclama\Landing\UtmTag */ ?>
<section class="page-section" id="<?= $form->blockId ?>">
	<div class="container">
		<div class="content">
			<div class="grid grid--padding-y grid--align-center">

				<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
					<div class="page-subsection">
						<h2 class="gift" style="text-align: center">
                            Дарим подарок <small>за подписку на 
                            <a href="https://instagram.com/medall.clinic">@medall.clinic</a></small>
                        </h2>
					</div>
				</div>

				<div class="grid__cell grid__cell--l-6 grid__cell--xs-12"></div>

				<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
					<div class="page-subsection">
						<picture>
							<source srcset="/images/gift.webp?1 1x, /images/gift@2x.webp?1 2x" type="image/webp">
							<source srcset="/images/gift.png?1 1x, /images/gift@2x.png?1 2x" type="image/png">
              <img src="/images/gift.png?1" alt="">
						</picture>
					</div>
				</div>

				<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
                    <? if(!empty($form->script)): ?>
                        <?= $form->script ?>
                    <? else: ?>
                        <? include __DIR__."/form-bitrix-24.php" ?>
                    <? endif ?>
				</div>

			</div>
		</div>
	</div>
</section>
