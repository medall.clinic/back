<? /** @var $bannerFact Medreclama\Landing\BannerFact */ ?>
<? /** @var $bannerfacts Medreclama\Landing\BannerFacts */ ?>
<div class="page-section banner-facts" id="<?= $bannerfacts->blockId ?>">

    <? if(empty($bannerfacts->images)): ?>
        <div class="banner-facts__background"></div>
    <? elseif(count($bannerfacts->images) == 1): ?>
        <div class="banner-facts__background">
            <? foreach ($bannerfacts->images as $imageSrc): ?>
                <img src="<?= $imageSrc ?>" alt="" />
            <? endforeach ?>
        </div>
    <? else: ?>
        <div class="banner-facts__background splide">
            <div class="splide__track">
                <ul class="splide__list">
					<? foreach ($bannerfacts->images as $imageSrc): ?>
                        <li class="splide__slide">
                            <img src="<?= $imageSrc ?>" alt="" />
                        </li>
					<? endforeach ?>
                </ul>
            </div>
        </div>
    <? endif ?>

    <? if($bannerfacts->items): ?>
        <div class="container">
            <div class="content">
                <div class="grid grid--justify-center grid--nowrap-hd grid--nowrap-xl grid--nowrap-l banner-facts__list">
                    <? foreach($bannerfacts->items as $bannerFact): ?>
                    <div class="grid__cell grid__cell--l-auto grid__cell--m-6 grid__cell--xs-12 banner-facts__item">
                        <div class="banner-facts__name"><?= $bannerFact->title ?></div>
                        <div class="banner-facts__body"><?= $bannerFact->text ?></div>
                    </div>
                    <? endforeach ?>
                </div>
            </div>
        </div>
    <? endif ?>

</div>