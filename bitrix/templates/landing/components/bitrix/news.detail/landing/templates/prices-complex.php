<? /** @var $price Medreclama\Landing\Price */ ?>
<? /** @var $prices Medreclama\Landing\Prices */ ?>
<div class="page-subsection">
	<div class="grid grid--justify-center grid--padding-y prices">

		<? foreach($prices->complexItems as $price): ?>
			<div class="grid__cell grid__cell--l-5 grid__cell--m-6 grid__cell--xs-12 prices__item">
				<div class="price price--undefined">

                    <div class="price__inner">
						<div class="price__name"><?= $price->title ?></div>
						<div class="price__value"><span><?= $price->value ?></span> ₽</div>
						<div class="price__desc"><?= $price->description ?></div>
						<div class="price__doctors"><strong><?= $price->doctors ?></strong></div>
					</div>

					<a
                        class="price__button btn"
                        href="<?= $price->button->link ?>"
						<?= $price->button->isFancybox ? "data-fancybox" : "" ?>
                    ><?= $price->button->text ?></a>

				</div>
			</div>
		<? endforeach ?>

	</div>
</div>
