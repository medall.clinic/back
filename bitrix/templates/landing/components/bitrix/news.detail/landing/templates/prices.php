<? /** @var $prices Medreclama\Landing\Prices */ ?>
<? /** @var $button Medreclama\Landing\Button */ ?>
<? $pricesButton = $prices->button ?>
<section class="page-section page-section--text-align-center" id="<?= $prices->blockId ?>">
	<div class="container">
		<div class="content">

            <? if($prices->title): ?>
			<div class="page-subsection text-align--center">
				<h2><?= $prices->title ?></h2>
			</div>
            <? endif ?>

			<? if($prices->text): ?>
			<div class="page-subsection">
				<?= $prices->text ?>
			</div>
			<? endif ?>

            <? if($prices->complexItems): ?>
                <? include __DIR__."/prices-complex.php" ?>
            <? endif ?>

            <? if($prices->simpleItems): ?>
                <? include __DIR__."/prices-simple.php" ?>
            <? endif ?>

            <? if($prices->text2): ?>
			<div class="page-subsection">
                <?= $prices->text2 ?>
			</div>
			<? endif ?>

			<? if($pricesButton->isExist()): ?>
            <div class="page-subsection">
                <div class="grid grid--justify-center">
                    <div class="grid__cell grid__cell--xs-auto">
                        <a
                            class="btn btn--wide"
                            href="<?= $pricesButton->link ?>"
							<?= $pricesButton->isFancybox ? "data-fancybox" : "" ?>
                        ><?= $pricesButton->text ?></a>
                    </div>
                </div>
            </div>
			<? endif ?>

		</div>
	</div>
</section>
