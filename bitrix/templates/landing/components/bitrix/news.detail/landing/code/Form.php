<?
namespace Medreclama\Landing;

class Form extends Landing
{
	public $utmTags;
	public $script;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->getTitle($arResult["NAME"]);
		$this->blockId = $this->getBlockId($this->landingProps["FORM_BLOCK_ID"]);
		$this->utmTags = $this->getUtmTags();
		$this->button = new Button($this->landingProps["FORM_BUTTON_TEXT"], $this->landingProps["FORM_BUTTON_LINK"], $this->landingProps["FORM_BUTTON_FANCYBOX"]);

		$this->generalBlockId = $this->landingProps["FORM_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["FORM_MENU_TITLE"]["VALUE"], $this->landingProps["FORM_MENU_LINK"]["VALUE"], $this->blockId);

		$this->script = $this->landingProps["FORM_SCRIPT"]["~VALUE"] ? $this->landingProps["FORM_SCRIPT"]["~VALUE"]["TEXT"] : "";

		return true;
	}

	private function getTitle(string $landingName): string
	{
		$pageType = $this->isPageTypeLanding() ? "лендинг" : "страница услуги";

		if(!empty($this->landingProps["FORM_CONTACTS_TITLE"]["~VALUE"])){
			$title = trim($this->landingProps["FORM_CONTACTS_TITLE"]["~VALUE"]);
		}else{
			$title = trim($landingName);
		}

		return $pageType.": ".$title;
	}

	private function getUtmTags()
	{
		if(empty($_GET)){
			return false;
		}

		$utmTags = [];
		foreach ($_GET as $paramName => $paramValue){
			if(strpos($paramName, 'utm_') !== false){
				$utmTags[] = new UtmTag($paramName, $paramValue);
			}
		}

		return $utmTags;
	}

}
