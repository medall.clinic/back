<?
namespace Medreclama\Landing;

class MenuItem
{
	public $title;
	public $link;

	public function __construct($title, $link, $blockId)
	{
		$this->title = $title;
		$this->link = !empty($link) ? $link : "#".$blockId;
	}

	public function isExist(): bool
	{
		return !empty($this->title);
	}

}
