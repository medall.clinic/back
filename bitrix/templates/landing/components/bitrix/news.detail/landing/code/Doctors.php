<?
namespace Medreclama\Landing;

class Doctors extends Landing
{
	public $text;
	public $items;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->title = $this->landingProps["DOCTORS_TITLE"]["~VALUE"];
		$this->blockId = $this->getBlockId($this->landingProps["DOCTORS_BLOCK_ID"]);
		$this->text = $this->landingProps["DOCTORS_TEXT"]["~VALUE"] ? $this->landingProps["DOCTORS_TEXT"]["~VALUE"]["TEXT"] : "";
		$this->items = $this->getItems($this->landingProps);
		$this->button = new Button($this->landingProps["DOCTORS_BUTTON_TEXT"], $this->landingProps["DOCTORS_BUTTON_LINK"], $this->landingProps["DOCTORS_BUTTON_FANCYBOX"]);

		$this->additionalText = $this->landingProps["DOCTORS_ADDITIONAL_TEXT"]["~VALUE"] ? $this->landingProps["DOCTORS_ADDITIONAL_TEXT"]["~VALUE"]["TEXT"] : false;

		$this->generalBlockId = $this->landingProps["DOCTORS_GENERAL_BLOCK"]["VALUE"];
		$this->menuItem = new MenuItem($this->landingProps["DOCTORS_MENU_TITLE"]["VALUE"], $this->landingProps["DOCTORS_MENU_LINK"]["VALUE"], $this->blockId);

		$this->isShow = $this->isShow();

		return true;
	}

	private function getItems(array $props): array
	{
		$elementsId = [];

		if(!empty($props["DOCTORS_ITEMS"]["VALUE"])){
			$elementsId = array_merge($elementsId, $props["DOCTORS_ITEMS"]["VALUE"]);
		}

		if(!empty($props["DOCTORS_GLOBAL_ITEMS"]["VALUE"])){
			$elementsId = array_merge($elementsId, $props["DOCTORS_GLOBAL_ITEMS"]["VALUE"]);
		}

		if(empty($elementsId)){
			return [];
		}

		$elements = $this->getElements($elementsId);

		$items = [];

		/** @var Element $element */
		foreach ($elements as $element){
			$items[] = new Doctor($element);
		}

		return $items;
	}


	private function isShow()
	{
		if(!$this->items){
			return false;
		}

		return true;
	}

}
