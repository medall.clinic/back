<?
namespace Medreclama\Landing;

class Landing
{
	public $title;
	public $blockId;
	public $button;
	public $additionalText;
	public $generalBlockId;
	public $menuItem;
	public $landingProps;
	public $isShow;

	private $arResult;

	public function __construct(array $arResult)
	{
		$this->arResult = $arResult;
		$this->landingProps = $arResult["PROPERTIES"];
		$this->isShow = true;
	}

	public function create(string $modelName): ?self
	{
		if(strpos($modelName, "General") !== false){
			$className = "Medreclama\Landing\General";
			$elementId = (int)substr($modelName, strpos($modelName, '_') + 1);
			return new $className($elementId);
		}else{
			$className = "Medreclama\Landing\\".$modelName;
			if(!class_exists($className)){
				return null;
			}
			return new $className($this->arResult);
		}
	}

	protected function getElement(int $elementId): ?Element
	{
		$res = \CIBlockElement::GetList([], ["ID" => $elementId]);

		if(!$bxElement = $res->GetNextElement()){
			return null;
		}

		$element = new Element;
		$element->fields = $bxElement->GetFields();
		$element->props = $bxElement->GetProperties();

		return $element;
	}

	protected function getElements(array $elementsId): array
	{
		$res = \CIBlockElement::GetList(["id" => $elementsId], ["ID" => $elementsId]);

		$elements = [];

		while($bxElement = $res->GetNextElement()){
			$element = new Element;
			$element->fields = $bxElement->GetFields();
			$element->props = $bxElement->GetProperties();
			$elements[] = $element;
		}

		return $elements;
	}

	protected function getImages($imagesId)
	{
		if(empty($imagesId)){
			return false;
		}

		$images = [];

		foreach ($imagesId as $imageId){
			$images[] = $this->getImageSrc($imageId);
		}

		return $images;
	}

	protected function getImageSrc($imageId)
	{
		return !empty($imageId) ? \CFile::GetPath($imageId) : null;
	}

	protected function getPrice($price)
	{
		return !empty($price) ? \StringsHelper::numberFormat($price) : null;
	}

	public function getShortClassName(): string
	{
		return (new \ReflectionClass($this))->getShortName();
	}

	public function render(): string
	{
		$className = strtolower($this->getShortClassName());
		$templatePath = __DIR__."/../templates/".$className.".php";

		$result = "";

		if($this->isShow){
			ob_start();
			// в шаблонах используется переменная имя которой совпадает с именем класса
			$$className = $this;
			require $templatePath;
			$result .= ob_get_clean();
		}

		if(!empty($this->additionalText)){
			$additionalText = $this->addUtmTags($this->additionalText);
			$result .= $additionalText;
		}

		return $result;
	}

	public function getVertSortedModelsClassNames(): array
	{
		$vertSort = $this->landingProps["VERT_SORT"]["VALUE"];

		if(empty($vertSort)){
			return $this->getDefaultVertSortedModelsClassNames();
		}

		$vertSortElements = $this->getElements($vertSort);
		$classNames = [];
		foreach ($vertSortElements as $vertSortElement){
			if($this->isGeneralBlock($vertSortElement->fields["IBLOCK_CODE"])){
				$classNames[] = "General_".$vertSortElement->fields["ID"];
			}else{
				$classNames[] = ucfirst($vertSortElement->fields["CODE"]);
			}
		}

		return $classNames;
	}

	public function isPageTypeLanding(): bool
	{
		$resLandingsHomeSection = \CIBlockSection::GetByID(SECTION_ID_LANDINGS_LANDINGS);
		$landingsHomeSection = $resLandingsHomeSection->GetNext();

		$arOrder = ["LEFT_MARGIN" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => IBLOCK_ID_LANDINGS,
			"LEFT_MARGIN" => $landingsHomeSection["LEFT_MARGIN"],
			"RIGHT_MARGIN" => $landingsHomeSection["RIGHT_MARGIN"]
		];
		$bIncCnt = false;
		$Select = ["ID", "IBLOCK_ID", "NAME"];
		$NavStartParams = false;
		$rsSections = \CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

		while ($arSection = $rsSections->Fetch())
		{
			if((int)$arSection["ID"] == (int)$this->arResult["IBLOCK_SECTION_ID"]){
				return true;
			}
		}

		return false;
	}

	private function getDefaultVertSortedModelsClassNames(): array
	{
		return [
			"Banner",
			"BannerFacts",
			"Reasons",
			"Modeling",
			"Advantages",
			"Doctors",
			"Works",
			"History",
			"Prices",
			"Comfort",
			"Credit",
			"Stages",
			"Reviews",
			"Questions",
			"Form",
		];
	}

	protected function getBlockId($property)
	{
		return !empty($property["VALUE"]) ? $property["VALUE"] : $property["DEFAULT_VALUE"];
	}

	private function isGeneralBlock($iblockCode): bool
	{
		return $iblockCode == "general";
	}

	private function addUtmTags($text)
	{
		if(strpos($text, "</form>") === false){
			return $text;
		}

		if(empty($_GET)){
			return $text;
		}

		$utmTags = [];
		foreach ($_GET as $paramName => $paramValue){
			if(strpos($paramName, 'utm_') !== false){
				$utmTags[] = '<input type="hidden" name="'.$paramName.'" value="'.$paramValue.'">';
			}
		}

		$utmTags = implode("\n", $utmTags);

		$text = str_replace("</form>", $utmTags."\n</form>", $text);

		return $text;
	}

}