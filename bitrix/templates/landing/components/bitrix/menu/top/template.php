<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die() ?>

    <div class="header-mobile-switcher hidden-hd hidden-xl hidden-l"></div>
    <nav class="header-menu">
        <ul class="header-menu__list">
            <?foreach($arResult as $arItem):?>
                <li
                    class="header-menu__list-item"
                    id="item-<?= str_replace('#', '', $arItem["LINK"]) ?>"
                >
                    <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                </li>
            <?endforeach?>
            <li
                class="header-menu__list-item hidden-hd hidden-xl hidden-l"
                id="item-call"
            >
                <a href="tel:+78126030201">Позвонить</a>
            </li>
        </ul>
    </nav>
