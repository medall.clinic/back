<!DOCTYPE html>
<html class="landing-page" lang="ru">

<head>
	<? include __DIR__."/include/header/head.php" ?>
</head>

<body>


    <div class="container">
        <div class="content">

            <header class="header">
                <? include __DIR__."/include/header/logo.php" ?>
                <style media="screen">
                    .header__menu {display: flex;}
                    .header-mobile-switcher {margin: 0 10px;}
                    @media (min-width: 1000px) {.header__menu {flex-direction: column;align-items: flex-start;}}
                </style>

                <div class="header__menu">
                    <? include __DIR__."/include/header/contacts.php" ?>
                    <? include __DIR__."/include/header/menu.php" ?>
                </div>
          			<? /* <div class="header__announcement">Отдыхаем вместе с вами 31 декабря, 1, 2 и 7 января.<br>Оставляйте заявки, перезвоним в рабочие дни. До встречи!</div> */ ?>

            </header>

        </div>
    </div>
