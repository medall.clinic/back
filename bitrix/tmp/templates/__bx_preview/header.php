<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">
        
		<title><?$APPLICATION->ShowTitle()?></title>
    <link rel="icon" href="/favicon.svg" type="image/svg">
		<!--css-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat&amp;amp;subset=cyrillic&display=swap">
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/slick.css">
        <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fancybox.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">
		<link rel="preload" href="<?=SITE_TEMPLATE_PATH?>/css/main.css" as="style">
        <link rel="preload" href="<?= SITE_TEMPLATE_PATH ?>/fonts/roboto-light.woff2" as="font">
        <link rel="preload" href="<?= SITE_TEMPLATE_PATH ?>/fonts/times.woff2" as="font">
        <link rel="preload" href="<?= SITE_TEMPLATE_PATH ?>/fonts/mplus1p-light.woff2" as="font">
        <link rel="preload" href="<?= SITE_TEMPLATE_PATH ?>/fonts/mplus1p-regular.woff2" as="font">

		<?$APPLICATION->ShowHead();?>
	</head>
<body>

<? if(empty($_GET["disable_yandex_metrika"])): ?>
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
     (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
     m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
     (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

     ym(67423126, "init", {
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true
     });
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/67423126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
<? endif ?>

<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<!--
<script>
    (function(w,d,u){
        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
    })(window,document,'https://cdn.bitrix24.ru/b10164883/crm/site_button/loader_2_vs13gj.js');
</script>
-->

<div class="header">
	<div class="wrapper">
		<div class="top">
			<div class="logo">
				<a href="<?=SITE_DIR?>" title="<?=GetMessage("HDR_GOTO_MAIN")?>">
					<svg viewBox="0 0 481.6 154.1" width="100%">
						<path class="st0" d="M481.3,151.9c0,1.4-0.8,2.2-2.2,2.2h-21.5c-0.5,0-1-0.3-1.5-0.6c-0.4-0.4-0.6-0.9-0.6-1.5v-38.6
						c0-1.4,0.8-2.2,2.2-2.2s2.2,0.8,2.2,2.2v36.5h19.4C480.6,149.9,481.3,150.5,481.3,151.9 M399.1,151.9c0,1.4-0.8,2.2-2.2,2.2h-21.5
						c-0.5,0-1-0.3-1.5-0.6c-0.4-0.4-0.6-0.9-0.6-1.5v-38.6c0-1.4,0.8-2.2,2.2-2.2c1.4,0,2.2,0.8,2.2,2.2v36.5h19.4
						C398.5,149.9,399.1,150.5,399.1,151.9z M306.4,137l-9.3-18.7l-9.3,18.7H306.4z M318.5,152.1c0,0.5-0.3,1-0.6,1.4
						c-0.5,0.4-1,0.6-1.5,0.6c-0.9,0-1.5-0.4-1.9-1.1l-5.9-11.8c-0.3,0-0.5,0.1-0.8,0.1h-21.5c-0.3,0-0.5,0-0.8-0.1l-5.9,11.8
						c-0.4,0.8-1,1.1-1.8,1.1c-0.6,0-1.1-0.3-1.5-0.6c-0.5-0.4-0.6-0.9-0.6-1.5c0-0.4,0.1-0.6,0.3-1l19.2-38.2c0.5-1,1.1-1.5,2-1.5
						c0.9,0,1.5,0.5,2,1.5l19.2,38.2C318.4,151.4,318.5,151.7,318.5,152.1z M217.8,132.6c0-4.7-1.7-8.8-5-12.1c-3.3-3.3-7.4-5-12.1-5H190
						v34.4h10.7c4.7,0,8.8-1.7,12.1-5C216,141.5,217.8,137.4,217.8,132.6z M222,132.6c0,5.9-2,11-6.3,15.2c-4.2,4.2-9.2,6.3-15.2,6.3
						h-12.9c-0.5,0-1-0.3-1.5-0.6c-0.4-0.4-0.6-0.9-0.6-1.5v-38.6c0-0.5,0.3-1,0.6-1.5c0.4-0.4,0.9-0.6,1.5-0.6h12.9c5.9,0,11,2,15.2,6.3
						C219.9,121.6,222,126.8,222,132.6z M129.1,151.9c0,1.4-0.8,2.2-2.2,2.2h-21.3c-0.5,0-1-0.3-1.5-0.6c-0.4-0.4-0.6-0.9-0.6-1.5v-38.6
						c0-0.5,0.3-1,0.6-1.5c0.4-0.4,0.9-0.6,1.5-0.6h21.5c1.4,0,2.2,0.8,2.2,2.2s-0.8,2.2-2.2,2.2h-19.4v15.1h15.1c1.4,0,2.2,0.8,2.2,2.2
						s-0.8,2.2-2.2,2.2h-15.1V150h19.4C128.5,149.9,129.1,150.5,129.1,151.9z M47.1,152.1c0,1.4-0.8,2-2.2,2c-1.1,0-1.8-0.5-2-1.7
						l-7.2-32.1l-10.1,23.5c-0.5,1.1-1.1,1.7-2.2,1.7c-0.9,0-1.7-0.5-2.2-1.7l-10.1-23.5L4,152.4c-0.3,1.1-0.9,1.7-2,1.7
						c-1.5,0-2.3-0.6-2.3-2c0-0.1,0-0.4,0.1-0.5l8.6-38.3c0.4-1.3,1-2,2.2-2c0.9,0,1.7,0.5,2,1.7l10.7,25l10.7-25c0.5-1,1.1-1.7,2-1.7
						c1.1,0,1.9,0.6,2.2,2l8.6,38.2C47.1,151.7,47.1,151.9,47.1,152.1z"></path>
						<path id="SVGID_1_" class="st0" d="M320.7,7.9c-31.9,13.5-70.7,36.2-84.7,40c-14.4,4,29.3-34.2,10.9-37.2
						c-20.1-3.4-118.7,53.8-145.9,70.5c-38.5,23.9-70.4,40.8-1.1,7.7C125.3,76.8,210.1,29.4,224.6,28c20.1-1.9-23.8,36.4-5.9,36.9
						c11.5,0.3,55.8-24.5,85.7-35.5c51.1-18.7,28.9,10,17.6,23.5c-16.4,19.9-2.8,26.1,9.3,24.3c11.1-1.7,34.2-6.9,51.1-12.5
						c40.2-13.5,11.5-7.5-7.2-3.1c-65.5,15.8-22.4-0.3-1.4-37.2C385.8,3.5,360.5-9.1,320.7,7.9"></path>
					</svg>
				</a>
			</div>
			<div class="contacts">
				<div class="col">
					<a href="tel:+7 (812) 603-02-01" class="phone"><b>+7 (812) 603-02-01</b></a>
					<div class="address">Левашовский пр., д.24</div>
				</div>
				<div class="col">
					<a href="email:admin@medall.clinic" class="email"><strong>admin@medall.clinic</strong></a>
					<!--<div class="address"></div>-->
				</div>
			</div>
			<div class="navigation">
 <a class="search__link" href="#"></a>
				<?$APPLICATION->IncludeComponent(
	"bitrix:search.form",
	"flat",
	Array(
		"PAGE" => "#SITE_DIR#search/index.php"
	)
);?>
				<div class="burger">
					<div class="burger__line">
					</div>
					<div class="burger__line">
					</div>
					<div class="burger__line">
					</div>
				</div>
			</div>
		</div>
		<div class="bottom">
 <nav class="menu">
			<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "top",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N"
	)
);?> </nav>
		</div>
	</div>
</div>
 <nav class="menuMain">
<div class="menuMain__left">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"mainleft",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "mainright",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "mainleft",
		"USE_EXT" => "Y"
	)
);?>
</div>
<div class="menuMain__right">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"mainright",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "tree",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => "",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "mainright",
		"USE_EXT" => "N"
	)
);?>
</div>
 </nav>
