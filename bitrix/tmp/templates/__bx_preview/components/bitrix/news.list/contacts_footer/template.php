<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<section class="contacts">
	<div class="contacts__left">
		<div class="contacts__contentWrap wow animated fadeIn" data-wow-delay="0.3s">
			<div class="contacts__title title beforeLine">
				<h2>Контакты</h2>
			</div>
			<div class="contacts__tabs__wrap">
				<?$arResult2 = array();
				$Address = array();
				foreach($arResult["ITEMS"] as $arItem){$arResult2[$arItem["IBLOCK_SECTION_ID"]][]=$arItem;}?>
				<div class="contacts__tabs"><?
					$r=0;
					foreach($arResult2 as $k => $arItem){
						$r++;
						$act='';
						if($r==1){$act='active';}
						$res = CIBlockSection::GetByID($k);
						if($ar_res = $res->GetNext()){?>
							<div class="contacts__tabs__item <?=$act?>">
								<a class="contacts__tabs__link" href="#"><?=$ar_res['NAME']?></a>
							</div><?
						}
					}?>
				</div>
				<div class="contacts__tabs__content">
					<?$r=0;
					foreach($arResult2 as $k => $arItem){
						$r++;
						$act='';
						if($r==1){$act='active';}?>
						<div class="contacts__tabs__content__item <?=$act?>">
							<?$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_8_SECTION",$k,"UF_PHONE");
							if($arUF["UF_PHONE"]["VALUE"]!= ""){?>
								<a class="contacts__tabs__content__phone" href="tel:<?=$arUF["UF_PHONE"]["VALUE"]?>"><?=$arUF["UF_PHONE"]["VALUE"]?></a><?
							}
							$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_8_SECTION",$k,"UF_EMAIL");
							if($arUF["UF_EMAIL"]["VALUE"]!= ""){?>
								<a class="contacts__tabs__content__email" href="mailto:<?=$arUF["UF_EMAIL"]["VALUE"]?>"><?=$arUF["UF_EMAIL"]["VALUE"]?></a><?
							}
							foreach($arItem as $j){
								$tmp = explode(",", $j["DISPLAY_PROPERTIES"]['Address']['VALUE']);
								$point = array();
								$point["cord"] = Array((float)$tmp[0],(float)$tmp[1]);
								$point["balloon"] = $j['NAME'];
								
								$Address[] = $point;
								
								$this->AddEditAction($j['ID'], $j['EDIT_LINK'], CIBlock::GetArrayByID($j["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($j['ID'], $j['DELETE_LINK'], CIBlock::GetArrayByID($j["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
								
								<div class="contacts__tabs__content__address"><?=$j['NAME']?></div><?
							}?>
						</div><?
					}?>
				</div>
			</div>
		</div>
	</div>
	<div class="contacts__right">
		<div class="map" id="map"></div>
	</div>
</section>
<script type="text/javascript">window.map_addresses = <?echo json_encode($Address)?></script>