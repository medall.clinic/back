<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="homeBlockNews">
	<div class="container">
		<div class="newsPage__block">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
				<div class="newsPage__block__item">
					<?$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
					if($ar_res = $res->GetNext()){?>
						<div class="newsPage__block__subsection"><?echo $ar_res['NAME']?></div>
					<?}?>
					<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
						<a class="newsPage__block__title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
					<?endif;?>
					<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
						<div class="newsPage__block__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
					<?endif?>
					<a class="newsPage__block__img" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></a>
					<div class="newsPage__block__desc">
						<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
							<div class="newsPage__block__desc__text"><?echo $arItem["PREVIEW_TEXT"];?>...</div>
						<?endif;?>
						<a class="newsPage__block__hashTag" href="/search/index.php?tags=<?=$arItem["TAGS"]?>" target=_blank># <?echo $arItem["TAGS"]?></a>
						<div class="newsPage__block__more">
							<a class="more" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Читать</a>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>