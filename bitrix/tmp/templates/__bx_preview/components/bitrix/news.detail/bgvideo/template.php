<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<section class="plasticsMainScreen" style="background-image: url(<?=$arResult["DETAIL_PICTURE"]["SRC"]?>);">
	<div class="plasticsMainScreen__fullvideo">
		<video width="100%" height="auto" autoplay loop muted poster="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">
			<?foreach($arResult['DISPLAY_PROPERTIES']['video']['FILE_VALUE'] as $k => $v){?>
				<source src="<?=$v["SRC"]?>">
			<?}?>
		</video>
	</div>
	<div class="container">
		<div class="plasticsMainScreen__block">
			<?echo $arResult["DETAIL_TEXT"];?>
		</div>
	</div>
</section>