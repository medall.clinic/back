<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="homeMainScreen" style="background-image: url(<?=$arResult["DETAIL_PICTURE"]["SRC"]?>);">
	<div class="container">
		<div class="homeMainScreen__block">
			<div class="homeMainScreen__title title">
				<h1><?echo $arResult["DETAIL_TEXT"];?></h1>
			</div>
		</div>
	</div>
</section>