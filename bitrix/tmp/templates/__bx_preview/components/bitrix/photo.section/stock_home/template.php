<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<?foreach($arResult["ROWS"] as $arItems):?>
	<?foreach($arItems as $arItem):?>
		<?if(is_array($arItem)):?>
			<div class="discountBreastPlastic__item">
				<div class="discountBreastPlastic__item__bgImg" style="background-image: url(<?echo $arItem["DISPLAY_PROPERTIES"]['img_bg_action']['FILE_VALUE']['SRC']?>)"></div>
				<div class="container">
					<div class="discountBreastPlastic__block">
						<div class="discountBreastPlastic__right">
							<div class="discountBreastPlastic__img">
								<img src="<?echo $arItem["DISPLAY_PROPERTIES"]['img_main_action']['FILE_VALUE']['SRC']?>" alt="картинка">
							</div>
						</div>
						<div class="discountBreastPlastic__left">
							<div class="discountBreastPlastic__title title beforeLine">
								<p><?echo $arItem["DISPLAY_PROPERTIES"]['title_action']['~VALUE']['TEXT']?></p>
							</div>
							<div class="discountBreastPlastic__month">
								<p><?echo $arItem['DATE_ACTIVE_FROM']?> - <?echo $arItem['DATE_ACTIVE_TO']?></p>
							</div>
							<a class="discountBreastPlastic__button button" href="<?echo $arItem['DETAIL_PAGE_URL']?>">Узнать подробнее</a>
							<div class="discountBreastPlastic__allOffer">
								<a class="more" href="/stock/">Все спецпредложения</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?endif;?>
	<?endforeach?>
<?endforeach?>
