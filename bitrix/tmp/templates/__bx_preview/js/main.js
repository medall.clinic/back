$(document).ready(function() {
	// Липкая шапка
	$(window).scroll(function() {
		if($(this).scrollTop() > 30) {
			$(".header").addClass("scroll");
			$(".menuMain").addClass("scroll");
		}else {
			$(".header").removeClass("scroll");
			$(".menuMain").removeClass("scroll");
		}
	});


	(function () {
		// Вызов формы поиска
		var searchButton = $(".search__link"),
			searchForm = $(".search__form"),
			head = $(".header");				

		searchButton.on("click", function(evt) {
			evt.preventDefault();
			head.toggleClass("active");
			searchButton.toggleClass("active");
			searchForm.toggleClass("active");
		});

	})();

	// Главное меню
		// клик по бургеру
		$(".burger").on("click", function(e) {
			e.preventDefault();
			$(".header").toggleClass("active");
			$(this).toggleClass("active");
			$(".menuMain").toggleClass("active");
		});
		//клик по основным ссылкам
		$(".menuMain__link").on("click", function(e) {
			var $this = $(this),
				menuItem = $this.closest(".menuMain__item");
			if($this.next().length) {
				e.preventDefault();
				if(menuItem.hasClass("active")) {
					menuItem.removeClass("active");

				}else {
					menuItem.addClass("active")
							.siblings()
							.removeClass("active");
				}
			}
		});
		// Клик по второстепенным ссылкам
		$(".menuMain__submenu__link").on("click", function(e) {
			var $this = $(this),
				menuItem = $this.closest(".menuMain__submenu__item"),
				menuSubCurrent = menuItem.find(".menuMain__submenu__two"),
				menuSub = $(".menuMain__submenu__two");
			if($this.next().length) {
				e.preventDefault();
				if(!menuItem.hasClass("active")) {
					menuItem.addClass("active")
							.siblings()
							.removeClass("active");
					menuSub.stop().slideUp(400);
					menuSubCurrent.stop().slideDown(400);
				}else {
					menuItem.stop().removeClass("active");
					menuSub.stop().slideUp(400);
				}
			}
		});


	//Добавление класса подменю.
	$(".menuCategory__item").mouseenter( function() {
		$(this).addClass("active")
			   .siblings()
			   .removeClass("active");
	});
	$(".menuCategory__item").mouseleave( function() {
		$(this).removeClass("active");
	});

	// Инициализация слайдеров
	(function() {

		var allDoctorSlider = $(".allDoctorBlock__slider"),
			discountSlider = $(".discountBreastPlastic__slider"),
			ourDoctorSlider = $(".ourDoctors__slider"),
			reviewsSlider = $(".reviews__slider"),
			breastEnlargementSlider = $(".breastEnlargementMainScreen__slider"),
			asideDiscountSlider = $(".aside__discountBreastPlastic__slider"),
			doctorOfReferral = $(".mainContent__ArticleBlock__doctor__slider"),
			questionSlider = $(".askYourQuestion__slider"),
			licensesSlider = $(".licensesSlider"), // слайдер лицензий
			diplomasSlider = $(".diplomasSlider"); // слайдер дипломов
		
		// Подключение слайдера все доктора на главной странице
		if(allDoctorSlider.length) {
			allDoctorSlider.slick({
				slidesToShow: 3,
				slideToScroll: 1,
				speed: 500,
				prevArrow: $(".allDoctorBlock__prev"),
				nextArrow: $(".allDoctorBlock__next"),
				autoplay: true,
				autoplaySpeed: 3000,
				
				responsive: [
					{
						breakpoint: 780,
						settings: {
							slidesToShow: 1,
							autoplay: false
						}
					}
				]
			});
		}

		// Подключение слайдера на блоке СКИДКИ
		if(discountSlider.length) {
			$(".discountBreastPlastic__slider").slick({
				prevArrow: $(".discountBreastPlastic__prev"),
				nextArrow: $(".discountBreastPlastic__next"),
				speed: 1000,
				autoplay: true,
				autoplaySpeed: 4500,
				fade: true,
				responsive: [
					{
						breakpoint: 781,
						settings: {
							fade: false,
							autoplay: false,
							speed: 700
						}
					}
				]
			});
		}

		// Подключение слайдера на блоке НАШИ ВРАЧИ
		if(ourDoctorSlider.length) {
			$(".ourDoctors__slider").slick({
				prevArrow: $(".ourDoctors__slide__prev"),
				nextArrow: $(".ourDoctors__slide__next"),
				speed: 1000,
				autoplay: true,
				autoplaySpeed: 4500,
				fade: true,
				responsive: [
					{
						breakpoint: 781,
						settings: {
							fade: false,
							autoplay: false,
							speed: 700
						}
					}
				]
			});
		}

		// Подключение слайдера на блоке ОТЗЫВЫ
		if(reviewsSlider.length) {
			$(".reviews__slider").slick({
				prevArrow: $(".reviews__slider__control__prev"),
				nextArrow: $(".reviews__slider__control__next"),
				speed: 1000,
				// autoplay: true,
				// autoplaySpeed: 3000
			});
		}

		// Подключение слайдера на страницу Увеличения груди - Главный экран
		if(breastEnlargementSlider.length) {
			breastEnlargementSlider.slick({
				prevArrow: $(".breastEnlargementMainScreen__prev"),
				nextArrow: $(".breastEnlargementMainScreen__next"),
				speed: 1000,
				// autoplay: true,
				// autoplaySpeed: 3000
			});
		}
		// Подключение слайдера доктора направления 
		// на странице Увеличения груди - в сайдбаре
		if(doctorOfReferral.length) {
			doctorOfReferral.slick({
				prevArrow: $(".mainContent__doctor__prev"),
				nextArrow: $(".mainContent__doctor__next"),
				speed: 700,
				autoplay: true,
				autoplaySpeed: 4500,
				responsive: [
					{
						breakpoint: 781,
						settings: {
							autoplay: false
						}
					}
				]
			});
		}
		// Подключение слайдера скидок 
		// на странице Увеличения груди - в сайдбаре
		if(asideDiscountSlider.length) {
			asideDiscountSlider.slick({
				prevArrow: $(".discountBreastPlastic__prev.aside"),
				nextArrow: $(".discountBreastPlastic__next.aside"),
				speed: 700,
				autoplay: true,
				autoplaySpeed: 3000
			});
		}

		// Подключение слайдера на блоке ВОПРОСЫ
		if(questionSlider.length) {
			questionSlider.slick({
				prevArrow: $(".askYourQuestion__slide__prev"),
				nextArrow: $(".askYourQuestion__slide__next"),
				speed: 1000,
				// autoplay: true,
				// autoplaySpeed: 3000
			});
		}

		// Подключение слайдера на странице документы
		if(licensesSlider.length) {
			licensesSlider.slick({
				slidesToShow: 4,
				slideToScroll: 1,
				infinite: false,
				prevArrow: $(".licensesSlider__prev"),
				nextArrow: $(".licensesSlider__next"),
				speed: 500,
				responsive: [
					{
						breakpoint: 995,
						settings: {
							slidesToShow: 3
						}
					},
					{
						breakpoint: 780,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});
		}

		// Подключение слайдера на странице документы
		if(diplomasSlider.length) {
			diplomasSlider.slick({
				slidesToShow: 4,
				slideToScroll: 1,
				infinite: false,
				prevArrow: $(".diplomasSlider__prev"),
				nextArrow: $(".diplomasSlider__next"),
				speed: 500,
				responsive: [
					{
						breakpoint: 995,
						settings: {
							slidesToShow: 3
						}
					},
					{
						breakpoint: 780,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});
		}

	})();
	
	// Табы в контактах

	$(".contacts__tabs__link").on("click", function(evt) {
		evt.preventDefault();

		var $this = $(this),
			item = $this.closest(".contacts__tabs__item"),
			container = item.closest(".contacts__tabs__wrap"),
			content = container.find(".contacts__tabs__content__item"),
			ndx = item.index();
			currentContent = content.eq(ndx);
		
		item.addClass("active")
			.siblings()
			.removeClass("active");

		currentContent.addClass("active")
					  .siblings()
					  .removeClass("active");
	});


	//Анимация главной страницы
	if($(".mainWow").length) {
		wow = new WOW({
	      	offset:       150,
	      	mobile:       false
	    })
	    wow.init();
	}

	// Ссылка КАК ДОБРАТЬСЯ на странице контактов
	$(".contactsPage__content__address__linkDesc").on("click", function(e) {
		e.preventDefault();

		var item = $(this);

		if(item.hasClass("active")) {
			item.removeClass("active");
			item.next(".contactsPage__content__address__desc").slideUp(400);
		}else {
			item.addClass("active");
			item.next(".contactsPage__content__address__desc").slideDown(400);
		}
	});

	// Попапы на странице оборудования и доИпосле и Расценки и меню категорий

	(function() {

		var openPopup = $(".openPopup"), // класс для открытия попапа - в href указать id нужного попапа
			overlay = $(".overlay"),
			close = $(".popup__close, .overlay, .popupReviewStar__button"),
			popup = $(".popupEquipment, .popupBeforeAfter, .popup, .popupPrice, .popupMenuCategory, .popupReviewStar"),
			body = $("body");

		openPopup.click(function() {
			var href = $(this).attr("href");

			body.css("overflow","hidden");
			$(href).addClass("active");
			overlay.addClass("active");
		});
		close.click(function() {
			body.css("overflow","auto");
			popup.removeClass("active");
			overlay.removeClass("active");
		});

	})();

	// Клик по кнопкам ФАС и ПРОФИЛЬ
	(function() {

		$(".beforeAfterPage__tab").on("click", function(e) {
			e.preventDefault();

			var tab = $(this),
				container = tab.closest(".beforeAfterPage__item__wrapper"),
				tabOne = container.find(".beforeAfterPage__tab--one"),
				tabTwo = container.find(".beforeAfterPage__tab--two"),
				tabThree = container.find(".beforeAfterPage__tab--three"),
				content1 = container.find(".beforeAfterPage__img__one"),
				content2 = container.find(".beforeAfterPage__img__two");
				content3 = container.find(".beforeAfterPage__img__three");

			tab.addClass("active")
				.siblings()
				.removeClass("active");

			if(tabOne.hasClass("active")) {
				content1.addClass("active");
				content2.removeClass("active");
				content3.removeClass("active");
			}
			if(tabTwo.hasClass("active")) {
				content1.removeClass("active");
				content2.addClass("active");
				content3.removeClass("active");
			}
			if(tabThree.hasClass("active")) {
				content1.removeClass("active");
				content2.removeClass("active");
				content3.addClass("active");
			}

		})

	})();

	// Клик по кнопке фильтры
	$(".blockFilter__mobile__link").on("click", function(e) {
		e.preventDefault();
		var item = $(this),
			filterBlock = $(".blockFilter__mobile__content");

		if(item.hasClass("active")) {
			item.removeClass("active");
			filterBlock.slideUp(400);
		}else {
			item.addClass("active");
			filterBlock.slideDown(400);
		}
	});
	
	// Раскрытие категорий в сайдбаре на странице До и после
	(function() {
		const blockTitle = $(".beforeAfterPage__asideFilter__title");
		$(".beforeAfterPage__asideFilter__link").not(":first").children(".beforeAfterPage__asideFilter__title").removeClass("active");
		$(".beforeAfterPage__asideFilter__link").not(":first").children(".beforeAfterPage__asideFilter__block").hide();

		blockTitle.click(function(e) {
			e.preventDefault();

			const $this = $(this);
			const block = $this.parent(".beforeAfterPage__asideFilter__link");
			const blockContent = block.find(".beforeAfterPage__asideFilter__block");

			$this.toggleClass("active");
			blockContent.slideToggle(400);
		});
	})();

	// Раскрытие блоков на странице карточки доктора

	(function() {

		var linkOpen = $(".doctorCardPage__blockGrey__link");

		linkOpen.on("click", function(e) {
			e.preventDefault();

			var $this = $(this),
				item = $this.siblings(".doctorCardPage__blockGrey__content");
			
				$this.toggleClass("active");

				if(!item.hasClass("active")) {
					item.addClass("active");
					item.slideDown(400);
					$this.text("Свернуть");
				}else {
					item.removeClass("active");
					item.slideUp(400);
					$this.text("Смотреть полностью");
				}
		});

	})();

	// Открытие попапов с формой

	(function() {

		var openModalPopup = $(".openModalPopup"), // класс для открытия попапа - в href указать id нужного попапа
			close = $(".popup__close, .popup__button-close"),
			popup = $(".popup"),
			body = $("body");

		openModalPopup.click(function() {
			var href = $(this).attr("href");

			body.css("overflow","hidden");
			$(href).addClass("active");
		});
		close.click(function() {
			body.css("overflow","auto");
			popup.removeClass("active");
		});

	})();

	//карта yandex
	(function() {
		var map = $(".map")
		if(map.length) {

			ymaps.ready(init);
			var myMap;
			var iconSize,
				iconOffset;
			if($(window).width() > 780) {
				iconSize = [81, 115];
				iconOffset = [-40, -115];
			}
			if($(window).width() <= 780) {
				iconSize = [45, 64];
				iconOffset = [-22.5, -64];
			}
			if($(window).width() <= 480) {
				iconSize = [30, 40];
				iconOffset = [-15, -40];
			}
			function init () {
		  	myMap = new ymaps.Map('map', {
		    	center: [59.99,30.29],
		    	controls: ['zoomControl'],
		    	zoom: 12
		  	}, {
		    	searchControlProvider: 'yandex#search'
		  	});

		  	myMap.behaviors
		  		.disable('scrollZoom');

			//Добавление меток на карту
			for (i=0;i<window.map_addresses.length; i++) {
				myMap.geoObjects
				.add(new ymaps.Placemark(window.map_addresses[i]["cord"], {
					balloonContent: window.map_addresses[i]["balloon"],
					iconCaption: window.map_addresses[i]["balloon"],
					
				},
				{
					iconLayout: 'default#image',
					// Своё изображение иконки метки.
					iconImageHref: '/bitrix/templates/corp_services_gray2/img/icon/mark.png',
					// Размеры метки.
					iconImageSize: iconSize,
					// Смещение левого верхнего угла иконки относительно
					// её "ножки" (точки привязки).
					iconImageOffset: iconOffset
				}));
			}
			/*myMap.geoObjects
				.add(new ymaps.Placemark([59.94576801341803,30.28656889748377], {
					balloonContent: 'Текст метки',
					iconCaption: 'Текст метки',
				},
				{
					iconLayout: 'default#image',
		            // Своё изображение иконки метки.
		            iconImageHref: '/bitrix/templates/corp_services_gray2/img/icon/mark.png',
		            // Размеры метки.
		            iconImageSize: iconSize,
		            // Смещение левого верхнего угла иконки относительно
		            // её "ножки" (точки привязки).
		            iconImageOffset: iconOffset
				}))*/
		    }
		}
	})();
	
    //карта yandex на странице контакты

    (function() {

    	var map = $(".contactsPageMap");
    	if(map.length) {

    		ymaps.ready(init);
			var myMapContactsPage;
			var iconSize,
				iconOffset,
				zoom;
			if($(window).width() > 995) {
				iconSize = [81, 115];
				iconOffset = [-40, -115];
				zoom = 12;
			}
			if($(window).width() <= 995) {
				iconSize = [50, 70];
				iconOffset = [-22.5, -64];
				zoom = 11;
			}
			if($(window).width() <= 780) {
				iconSize = [45, 64];
				iconOffset = [-22.5, -64];
			}
			if($(window).width() <= 480) {
				iconSize = [30, 40];
				iconOffset = [-15, -40];
				zoom = 10;
			}
			function init () {
				myMapContactsPage = new ymaps.Map('contactsPageMap', {
					center: [59.99,30.29],
					controls: ['zoomControl'],
					zoom: zoom
				}, {
					searchControlProvider: 'yandex#search'
				});

				myMapContactsPage.behaviors
					.disable('scrollZoom');

				//Добавление меток на карту
				
				for (i=0;i<window.map_addresses.length; i++) {
					myMapContactsPage.geoObjects
					.add(new ymaps.Placemark(window.map_addresses[i]["cord"], {
						balloonContent: window.map_addresses[i]["balloon"],
						iconCaption: window.map_addresses[i]["balloon"],
						
					},
					{
						iconLayout: 'default#image',
						// Своё изображение иконки метки.
						iconImageHref: '/bitrix/templates/corp_services_gray2/img/icon/mark.png',
						// Размеры метки.
						iconImageSize: iconSize,
						// Смещение левого верхнего угла иконки относительно
						// её "ножки" (точки привязки).
						iconImageOffset: iconOffset
					}));
				}
			}
    	}

    })();
	
	// календарь
	/*let calendarTwo = $("#datetimepicker12");
	if(calendarTwo.length) {
		$('#datetimepicker12')
	    .datetimepicker({
	    	inline: true,
	    	format: 'YYYY-MM-DD',
	    	locale: 'ru',
			minDate: moment()
			
	    });
	}*/
	
	// Расписание врача
	var availableDates = window.timing_doctor;
	$(".datepicker").datepicker({
		dateFormat: 'mm.dd.yy',
		minDate: new Date(),
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		beforeShowDay: function(d) {
			var da = d.getDate();
			if(da<10) da = "0"+da;
			var Mo = d.getMonth()+1;
			if(Mo<9) Mo = "0"+Mo;
			var dmy = da + "." + Mo + "." + d.getFullYear();
			//console.log(dmy+' : '+($.inArray(dmy, availableDates)));
			if ($.inArray(dmy, availableDates) != -1) {return [true, "","Available"]; }
			else{return [false,"","unAvailable"]; }
		}
	});
	$("#doctor").val($(".popup__content-desc").html());
    
	// формы
	/*var str = location.href;
	var url = str.split('?');
	alert(url[0]);*/
	
	if(GetURLParameter('WEB_FORM_ID')!=undefined && GetURLParameter('WEB_FORM_ID')==1 && GetURLParameter('formresult')!=undefined && GetURLParameter('formresult')=="addok"){
		$("#Thanks").addClass("active");
	}
	
	if(GetURLParameter('WEB_FORM_ID')!=undefined && GetURLParameter('WEB_FORM_ID')==5 && GetURLParameter('formresult')!=undefined && GetURLParameter('formresult')=="addok"){
		$("#subscribeThanks").addClass("active");
	}
	
	$(".phone").mask("+7(999) 999-99-99");
	
});
// Get параметры
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&'); // разбиваем на фрагменты
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {return sParameterName[1];}
    }
}