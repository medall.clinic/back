<?php
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Gruppenbesitzer und Gruppenmoderatoren";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD_PROJECT"] = "Projektleiter und Projektmoderatoren";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Nur der Gruppenbesitzer";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER_PROJECT"] = "Nur Projektleiter";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Alle Gruppenmitglieder";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER_PROJECT"] = "Alle Projektteilnehmer";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_ALL"] = "Alle Nutzer";
