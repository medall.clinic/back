<?php
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Власник групи і модератори групи";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD_PROJECT"] = "Керівник проєкту та його помічники";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Тільки власник групи";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER_PROJECT"] = "Тільки керівник проєкту";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Всі члени групи";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER_PROJECT"] = "Всі учасники проєкту";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_ALL"] = "Всі користувачі";
