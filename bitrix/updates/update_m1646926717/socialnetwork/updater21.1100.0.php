<?php

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sonet_group'))
{
	if (!$DB->Query("SELECT AVATAR_TYPE FROM b_sonet_group WHERE 1=0", true))
	{
		$DB->Query("ALTER TABLE b_sonet_group ADD AVATAR_TYPE varchar(50) null");
	}
}

if (IsModuleInstalled('socialnetwork'))
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/images", "images/socialnetwork");
}

if ($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/socialnetwork/install/components/bitrix/socialnetwork.group/lang/de/component.php",
		"components/bitrix/socialnetwork.group/lang/de/component.php",
		"modules/socialnetwork/install/components/bitrix/socialnetwork.group/lang/en/component.php",
		"components/bitrix/socialnetwork.group/lang/en/component.php",
		"modules/socialnetwork/install/components/bitrix/socialnetwork.group/lang/ru/component.php",
		"components/bitrix/socialnetwork.group/lang/ru/component.php",
		"modules/socialnetwork/install/components/bitrix/socialnetwork.group/lang/ua/component.php",
		"components/bitrix/socialnetwork.group/lang/ua/component.php",
		"modules/socialnetwork/install/js/socialnetwork/common/socialnetwork.common.js",
		"js/socialnetwork/common/socialnetwork.common.js",
	);
	foreach ($arToDelete as $file)
	{
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
	}
}

if (IsModuleInstalled('socialnetwork') && $updater->CanUpdateDatabase())
{
	\Bitrix\Main\EventManager::getInstance()->registerEventHandler('main', 'OnBuildFilterFactoryMethods', 'socialnetwork', '\Bitrix\Socialnetwork\Filter\FactorySocialnetwork', 'onBuildFilterFactoryMethods');
}
