<?php
$MESS["SENDER_DISPATCH_STATE1_W"] = "In Arbeit";
$MESS["SENDER_DISPATCH_STATE_ERROR_CAMPAIGN_INACTIVE"] = "Newsletter-Kampagne ist nicht aktiv.";
$MESS["SENDER_DISPATCH_STATE_ERROR_CHANGE"] = "Status der Kampagne kann nicht von \"%old%\" auf \"%new%\" geändert werden.";
$MESS["SENDER_DISPATCH_STATE_H"] = "Angehalten";
$MESS["SENDER_DISPATCH_STATE_I"] = "Vorbereiten";
$MESS["SENDER_DISPATCH_STATE_M"] = "Segment wird vorbereitet";
$MESS["SENDER_DISPATCH_STATE_N"] = "Neu";
$MESS["SENDER_DISPATCH_STATE_P"] = "Angehalten";
$MESS["SENDER_DISPATCH_STATE_R"] = "Zum Senden bereit";
$MESS["SENDER_DISPATCH_STATE_S"] = "Wird jetzt gesendet";
$MESS["SENDER_DISPATCH_STATE_T"] = "Geplant";
$MESS["SENDER_DISPATCH_STATE_X"] = "Abgebrochen";
$MESS["SENDER_DISPATCH_STATE_Y"] = "Gesendet";
