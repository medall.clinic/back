<?
if(IsModuleInstalled('sender') && $updater->CanUpdateDatabase())
{
	RegisterModuleDependences("pull", "OnGetDependentModule", "sender", "Bitrix\\Sender\\SenderPullSchema", "OnGetDependentModule" );
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sender_list'))
{
	if ($DB->type == "MYSQL")
	{
		if ($updater->TableExists("b_sender_group"))
		{
			if (!$DB->Query("SELECT STATUS FROM b_sender_group WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sender_group ADD STATUS varchar(1) default 'N' null");
			}
		}
		if ($updater->TableExists("b_sender_group_connector"))
		{
			if (!$DB->Query("SELECT FILTER_ID FROM b_sender_group_connector WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sender_group_connector ADD FILTER_ID varchar(256)");
			}
		}
		if ($updater->TableExists("b_sender_mailing_chain"))
		{
			if (!$DB->Query("SELECT WAITING_RECIPIENT FROM b_sender_mailing_chain WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sender_mailing_chain ADD WAITING_RECIPIENT CHAR(1) default 'N'  NOT NULL");
			}
		}
		if (!$updater->TableExists("b_sender_group_data"))
		{
			$DB->Query("
				CREATE TABLE b_sender_group_data(
					ID int auto_increment primary key,
					GROUP_ID int not null,
					DATE_INSERT DATETIME default NOW() not null,
					FILTER_ID varchar(256) not null,
					CRM_ENTITY_ID int,
					CRM_ENTITY_TYPE_ID int,
					NAME varchar(511),
					CRM_ENTITY_TYPE varchar(128),
					CONTACT_ID int,
					COMPANY_ID int,
					EMAIL varchar(511),
					IM varchar(511),
					PHONE varchar(128),
					HAS_EMAIL varchar(1),
					HAS_IMOL varchar(1),
					HAS_PHONE varchar(1)
				)
			");

			if ($updater->TableExists("b_sender_group_data"))
			{
				if (!$DB->IndexExists("b_sender_group_data", array("GROUP_ID", "FILTER_ID")))
				{
					$DB->Query("
				create index IX_SENDER_GROUP_DATA_GROUP_ID_FILTER_ID
					on b_sender_group_data (GROUP_ID, FILTER_ID);
					");
				}
			}
		}

		if (!$updater->TableExists("b_sender_group_state"))
		{
			$DB->Query("
				CREATE TABLE b_sender_group_state(
					`ID` int auto_increment primary key,
					`GROUP_ID` int not null,
					`DATE_INSERT` datetime default NOW(),
					`FILTER_ID` varchar(256) not null,
					`STATE` int,
					`ENDPOINT` LONGTEXT,
					`OFFSET` INT
				)
			");
		}

		if ($updater->TableExists("b_sender_group_state"))
		{
			if (!$DB->IndexExists("b_sender_group_state", array("GROUP_ID", "FILTER_ID", ), true))
			{
				$DB->Query("CREATE INDEX IX_SENDER_GROUP_STATE_GROUP_ID_FILTER_ID ON b_sender_group_state(GROUP_ID, FILTER_ID)");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sender_list'))
{
	if ($DB->type == "MYSQL")
	{
		if (!$updater->TableExists("b_sender_group_queue"))
		{
			$DB->Query("
				CREATE TABLE b_sender_group_queue(
					`ID` bigint auto_increment primary key,
					`DATE_INSERT` DATETIME default NOW() not null,
					`GROUP_ID` int not null,
					`ENTITY_ID` int not null,
					`TYPE` int not null
				)
			");
		}
		if ($updater->TableExists("b_sender_group_queue"))
		{
			if (!$DB->IndexExists("b_sender_group_queue", array("TYPE", "ENTITY_ID", "GROUP_ID", ), true))
			{
				$DB->Query("CREATE INDEX IX_SENDER_GROUP_QUEUE_TYPE_ENTITY_ID_GROUP_ID ON b_sender_group_queue(TYPE, ENTITY_ID, GROUP_ID)");
			}
		}
	}
}

if(IsModuleInstalled('sender'))
{
	$updater->CopyFiles("install/components", "components");
}

?>
