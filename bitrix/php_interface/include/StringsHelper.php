<?
class StringsHelper
{
	public static function numberFormat($number)
	{
		if(!$number){
			return "";
		}

		return number_format($number, 0, '.', ' ');
	}

	public static function camelize($input)
	{
		return str_replace('_', '', ucwords($input, '_'));
	}

}
