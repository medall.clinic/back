<?
class iblockDoctorModel
{
	const IBLOCK_ID = 6;
	const PROP_PHOTO_DOCTOR_ID = 18;
	const PROP_CERTIFICATE_DOCTOR_ID = 14;

	public function getPropPhotoDoctorFileId($elementId)
	{
		$filesId = $this->getPropFilesId($elementId, self::PROP_PHOTO_DOCTOR_ID);
		return reset($filesId);
	}

	public function getPropCertificateDoctorFilesId($elementId)
	{
		return $this->getPropFilesId($elementId, self::PROP_CERTIFICATE_DOCTOR_ID);
	}

	private function getPropFilesId($elementId, $propId)
	{
		$dbProps = CIBlockElement::GetProperty(self::IBLOCK_ID, $elementId, [], ["ID" => $propId]);
		$filesId = [];
		while($arProps = $dbProps->Fetch()){
			$filesId[] = $arProps["VALUE"];
		}

		return $filesId;
	}

	public function isDoctorsIblock($iblockId)
	{
		return $iblockId == self::IBLOCK_ID;
	}

	public function isPropPhotoChanged($propValues)
	{
		$propPhotoDoctor = reset($propValues[self::PROP_PHOTO_DOCTOR_ID]);
		return (bool)$propPhotoDoctor["VALUE"]["name"];
	}

	public function isPropCertificateDoctorChanged($propValues)
	{
		$propCertificate = $propValues[self::PROP_CERTIFICATE_DOCTOR_ID];
		$propCertificateKeys = array_keys($propCertificate);

		foreach ($propCertificateKeys as $propValueId){
			if(strpos($propValueId, "n") === 0){
				return true;
			}
		}

		return false;
	}

}
