<?
class ImagesResizeHelper
{
	public static function resize($originalPath, $newWidth)
	{
		$originalWidth = DimensionHelper::getWidth($originalPath);
		$originalHeight = DimensionHelper::getHeight($originalPath);

		if($newWidth > $originalWidth){return false;}

		$newHeight = $originalHeight * ($newWidth / $originalWidth);

		$tempPath = $_SERVER["DOCUMENT_ROOT"].str_replace('.', '-temp.', $originalPath);
		$originalPath = $_SERVER["DOCUMENT_ROOT"].$originalPath;

		$sizes = ["width" => $newWidth, "height" => $newHeight];
		$result = CFile::ResizeImageFile($originalPath, $tempPath, $sizes);

		unlink($originalPath); // delete original file
		rename($tempPath, $originalPath); // rename temp file as original

		return $result;
	}

}
