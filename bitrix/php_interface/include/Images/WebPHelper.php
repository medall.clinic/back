<?
class WebPHelper
{
	public static function getOrCreateWebPUrl($src, array $sizes = null)
	{
		if(!$src){return "";}

		$webPHelper = new self();
		$webPPaths = [];

		$webPPaths1x = str_replace([".jpg", ".jpeg", ".png"], ".webp", strtolower($src));
		$webPPaths[] = $webPPaths1x;

		if(!file_exists($_SERVER["DOCUMENT_ROOT"]."/".$webPPaths1x)){
			$webPHelper->saveWebPImage($src, $webPPaths1x);
			if($sizes[0] && DimensionHelper::isImageWider($src, $sizes[0])){
				ImagesResizeHelper::resize($webPPaths1x, $sizes[0]);
			}
		}

		if(!empty($sizes[1])){
			$webPPaths2x = str_replace(".webp", "@2x.webp", $webPPaths1x);
			if(file_exists($_SERVER["DOCUMENT_ROOT"]."/".$webPPaths2x)){
				$webPPaths[] = $webPPaths2x." 2x";
			}else{
				// Делаем 2х-фото только если ориганальное фото шире чем 1х-ширина ($sizes[0])
				if(DimensionHelper::isImageWider($src, $sizes[0])){
					$webPHelper->saveWebPImage($src, $webPPaths2x);
					// Если 2х-фото шире чем 2х-ширина ($sizes[1]) - то уменьшаем 2х-фото до 2х-ширины
					if(DimensionHelper::isImageWider($src, $sizes[1])){
						ImagesResizeHelper::resize($webPPaths2x, $sizes[1]);
					}
					$webPPaths[] = $webPPaths2x." 2x";
				}
			}
		}

		return implode(", ", $webPPaths);
	}

	private function saveWebPImage($src, $webPSrc)
	{
		$root = $_SERVER["DOCUMENT_ROOT"];
		$src = $root."/".$src;
		$webPSrc = $root."/".$webPSrc;

		if($this->isImgPng($src)){
			$img = imagecreatefrompng($src);
			imagepalettetotruecolor($img);
			imagealphablending($img, true);
			imagesavealpha($img, true);
		} elseif($this->isImgJpeg($src)) {
			$img = imagecreatefromjpeg($src);
			imagepalettetotruecolor($img);
		} else {
			return false;
		}

		imagewebp($img, $webPSrc, 100);

		//x00 webp generation fix
		$fpr = fopen($webPSrc, "a+");
		fwrite($fpr, chr(0x00));
		fclose($fpr);

		imagedestroy($img);

		return true;
	}

	private function isImgPng($path)
	{
		return exif_imagetype($path) == IMAGETYPE_PNG;
	}

	private function isImgJpeg($path)
	{
		return exif_imagetype($path) == IMAGETYPE_JPEG;
	}

}
