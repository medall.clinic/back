<?
function WebPHelper($imagePath)
{
	$webpImagePath = str_replace([".jpg", ".png"], ".webp", strtolower($imagePath));

	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/".$webpImagePath)){
		return $webpImagePath;
	}

	return $imagePath;
}

function WebPHelper2x($imagePath)
{
	$webpImagePath = str_replace([".jpg", ".png"], "@2x.webp", strtolower($imagePath));

	if(file_exists($_SERVER["DOCUMENT_ROOT"]."/".$webpImagePath)){
		return $webpImagePath;
	}

	return $imagePath;
}

require_once __DIR__."/iblockDoctorModel.php";
require_once __DIR__."/ResizeHelper.php";

AddEventHandler("iblock", "OnAfterIBlockElementAdd", ["WebPConverter", "OnAfterIBlockElementUpdateHandler"]);
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", ["WebPConverter", "OnAfterIBlockElementUpdateHandler"]);

class WebPConverter
{
	/*
		1 - Сервисные блоки / Фоновые изображения
		5 - Сервисные блоки / Основные направления
		6 - Врачи / Врачи
		4 - Новости / Новости
		35 - Сервисные блоки / Услуги - Информация об отделении (слайды)
		9  - Статьи / Статьи
	*/
	private static $iblockIdsToTrack = [1, 5, 6, 4, 35, 9];

	public function OnAfterIBlockElementUpdateHandler(&$arFields)
	{
		if(!$arFields["RESULT"] OR !self::isThisIblockTracked($arFields["IBLOCK_ID"])){
			return;
		}

		if($arFields["PREVIEW_PICTURE_ID"]){
			self::saveWebPImage($arFields["PREVIEW_PICTURE_ID"]);
		}

		if($arFields["DETAIL_PICTURE_ID"]){
			self::saveWebPImage($arFields["DETAIL_PICTURE_ID"]);
		}

		$iblockDoctorModel = new iblockDoctorModel;
		if($iblockDoctorModel->isDoctorsIblock($arFields["IBLOCK_ID"])){

			if($iblockDoctorModel->isPropPhotoChanged($arFields["PROPERTY_VALUES"])){
				$propPhotoDoctorFileId = $iblockDoctorModel->getPropPhotoDoctorFileId($arFields["ID"]);
				if($propPhotoDoctorFileId){
					self::saveWebPImage($propPhotoDoctorFileId);
				}
			}

			if($iblockDoctorModel->isPropCertificateDoctorChanged($arFields["PROPERTY_VALUES"])){
				$resizeHelper = new ResizeHelper;

				$propCertificateDoctorFilesId = $iblockDoctorModel->getPropCertificateDoctorFilesId($arFields["ID"]);
				if($propCertificateDoctorFilesId){
					foreach ($propCertificateDoctorFilesId as $propCertificateDoctorFileId){
						self::saveWebPImage($propCertificateDoctorFileId);
						$resizedImagePath = $resizeHelper->resizeImage($propCertificateDoctorFileId, 414, 300);
						self::saveWebPImage(false, $resizedImagePath);
					}
				}
			}

		}


	}

	private static function saveWebPImage($imageId, $oldPath = false)
	{
		if(!$oldPath){
			$path = CFile::GetPath($imageId);
			$root = $_SERVER["DOCUMENT_ROOT"];
			$oldPath = $root."/".$path;
		}

		$newPath = $oldPath;
		$newPath = explode(".", $newPath);
		array_pop($newPath);
		$newPath[] = "webp";
		$newPath = implode(".", $newPath);

		if(file_exists($newPath)){
			return true;
		}

		if(self::isImgPng($oldPath)){
			$img = imagecreatefrompng($oldPath);
			imagepalettetotruecolor($img);
			imagealphablending($img, true);
			imagesavealpha($img, true);
		} elseif(self::isImgJpeg($oldPath)) {
			$img = imagecreatefromjpeg($oldPath);
			imagepalettetotruecolor($img);
		} else {
			return false;
		}

		imagewebp($img, $newPath, 100);

		//x00 webp generation fix
		$fpr = fopen($newPath, "a+");
		fwrite($fpr, chr(0x00));
		fclose($fpr);

		imagedestroy($img);

		return true;
	}

	private static function isImgPng($path)
	{
		return exif_imagetype($path) == IMAGETYPE_PNG;
	}

	private static function isImgJpeg($path)
	{
		return exif_imagetype($path) == IMAGETYPE_JPEG;
	}

	private static function isThisIblockTracked($iblockId)
	{
		return in_array($iblockId, self::$iblockIdsToTrack);
	}

}
