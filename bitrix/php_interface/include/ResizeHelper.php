<?
class ResizeHelper
{
	const PROPORTION = BX_RESIZE_IMAGE_PROPORTIONAL;

	public function resizeImage($propFileId, $width, $height, $asCopy = true)
	{
		$pathResizeFrom = $_SERVER["DOCUMENT_ROOT"].CFile::GetPath($propFileId);
		$pathResizeTo = str_replace('.jpg', '_resized.jpg', strtolower($pathResizeFrom));

		$file = CFile::GetByID($propFileId)->Fetch();

		if($this->isImgLandscape($file)){
			$width = $this->getCorrectWidth($file, $height);
		}else{
			$height = $this->getCorrectHeight($file, $height);
		}

		if(!$this->isImgWiderThenNeeded($file, $width) && !$this->isImgTallerThenNeeded($file, $height)){
			return true;
		}

		$sizes = ["width" => $width, "height" => $height];
		$resizeResult = CFile::ResizeImageFile($pathResizeFrom, $pathResizeTo, $sizes, self::PROPORTION, [], false, false);

		if(!$resizeResult){
			return false;
		}

		if(!$asCopy){
			$arImageSize = $this->getImageSize($pathResizeTo);
			$this->deleteOriginalImg($pathResizeFrom);
			$this->renameResizedImg($pathResizeTo, $pathResizeFrom);
			$this->setResizedImgCorrectDimensions($arImageSize, $propFileId);
			return $pathResizeFrom;
		}

		return $pathResizeTo;
	}

	private function deleteOriginalImg($path)
	{
		unlink($path);
	}

	private function renameResizedImg($pathResizeTo, $pathResizeFrom)
	{
		rename($pathResizeTo, $pathResizeFrom);
	}

	private function isImgLandscape($file)
	{
		return $file["WIDTH"] > $file["HEIGHT"];
	}

	private function isImgWiderThenNeeded(array $file, $width)
	{
		return $file["WIDTH"] > $width;
	}

	private function isImgTallerThenNeeded(array $file, $height)
	{
		return $file["HEIGHT"] > $height;
	}

	private function getCorrectWidth(array $file, $height)
	{
		return (ceil($file["WIDTH"] / $file["HEIGHT"])) * $height;
	}

	private function getCorrectHeight(array $file, $height)
	{
		return (ceil($file["HEIGHT"] / $file["WIDTH"])) * $height;
	}

	private function setResizedImgCorrectDimensions($arImageSize, $fileId)
	{
		global $DB;

		$fileSize = round(floatval($arImageSize[2]));
		$height = round(floatval($arImageSize[1]));
		$width = round(floatval($arImageSize[0]));
		$fileId = intval($fileId);

		$query = "UPDATE b_file SET FILE_SIZE='".$fileSize."', HEIGHT='".$height."', WIDTH='".$width."' WHERE ID=".$fileId;
		$DB->Query($query);
	}

	private function getImageSize($path)
	{
		$arImageSize = CFile::GetImageSize($path);

		$io = CBXVirtualIo::GetInstance();
		$f = $io->GetFile($path);

		$arImageSize[2] = $f->GetFileSize();

		return $arImageSize;
	}

}