<?
function getCurPage()
{
	global $APPLICATION;
	$curPage = explode('?', $APPLICATION->GetCurPage());
	$curPage = reset($curPage);

	return $curPage;
}

function isHomePage()
{
	$curPage = getCurPage();

	if($curPage == "/index-dev.php"){return true;} // temp - delete after development stage!

	return $curPage == "/";
}

function isInnerPage()
{
	return !isHomePage();
}

function isShowBreadcrumbsAndTitle()
{
	return isInnerPage();
}

function is404Page()
{
	return defined("ERROR_404") && ERROR_404 == "Y";
}

function isDiscountsPage()
{
	$curPage = getCurPage();
	return strpos($curPage, '/discounts/') !== false;
}

function isLandingPage()
{
	return defined("IS_LANDING_PAGE") && IS_LANDING_PAGE === true;
}

function isCategoryPage()
{
	return in_array(getCurPage(), [
		'/plastic/',
		'/cosmetology/',
        '/gynecology/',
        '/transplantation/',
        '/phlebology/',
        '/dentistry/'
	]);
}

function isSearchPage()
{
	$curPage = getCurPage();
	return strpos($curPage, '/search/') !== false;
}

function isDoctorsPage()
{
	$curPage = getCurPage();
	return $curPage === '/doctor/';
}

function isDoctorPage()
{
	$curPage = getCurPage();
	return (strpos($curPage, '/doctor/') !== false) && ($curPage !== '/doctor/');
}

function isDocumentsPage()
{
	$curPage = getCurPage();
	return $curPage === '/about/document/';
}

function isContactsPage()
{
	$curPage = getCurPage();
	return $curPage === '/about/contacts/';
}

function isVacanciesPage()
{
    $curPage = getCurPage();
    return $curPage;
}
