<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();
use Bitrix\Main\Loader;
$eventManager = \Bitrix\Main\EventManager::getInstance();

define("IBLOCK_ID_DOCTORS", 6);
define("IBLOCK_ID_PRICE", 32);
define("IBLOCK_ID_SERVICE", 38);
define("IBLOCK_ID_LANDINGS", 47);
define("IBLOCK_ID_LANDINGS_WORKS", 51);
define("IBLOCK_ID_CONSULTATION_REGISTER", 57);

define("SECTION_ID_SERVICE_COSMETOLOGY", 53);
define("SECTION_ID_SERVICE_GYNECOLOGY", 457);
define("SECTION_ID_SERVICE_TRANSPLANTATION", 458);
define("SECTION_ID_SERVICE_DENTISTRY", 484);
define("SECTION_ID_SERVICE_HAIR_TRANSPLANT", 92);
define("SECTION_ID_SERVICE_PHLEBOLOGY", 94);
define("SECTION_ID_SERVICE_PLASTIC", 93);

define("SECTION_ID_LANDINGS_LANDINGS", 157);
define("SECTION_ID_LANDINGS_SERVICE", 158);
define("SECTION_ID_LANDINGS_SERVICE_PLASTIC", 159);
define("SECTION_ID_LANDINGS_SERVICE_DENTISTRY", 179);

define("PROPERTY_ID_DIRECTION_DOCTOR_PLASTIC", 12);
$_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_CF_CONNECTING_IP'];

$count = 0;
$curPage = explode('?', $APPLICATION->GetCurPage());
$curPage = reset($curPage);

include __DIR__."/include/pages.php";
include __DIR__."/include/StringsHelper.php";
include __DIR__."/include/webp.php";
include __DIR__."/include/Images/DimensionHelper.php";
include __DIR__."/include/Images/WebPHelper.php";
include __DIR__."/include/Images/ImagesResizeHelper.php";

/*скрытые инфоблоков в админке*/

$eventManager->addEventHandler("main", "OnBuildGlobalMenu", "hideNoactiveIblocks");
function hideNoactiveIblocks(&$aGlobalMenu, &$aModuleMenu)
{

    /* Тип инфоблока 1 */
    $arrParentIBlockType = ['Вакансии'];

    /* Инфоблоки для скрытия 2 */
    $arrParentIBlockTitle = [''];

    /* Разделы для скрытия 3 */
    $arrSectionsToHide = [''];

    // Получаем активные инфоблоки
    $arNaIblocks = \Bitrix\Iblock\IblockTable::getList(['filter' => ['ACTIVE' => 'Y'], 'select' => ['*']])->fetchAll();

    if ($arNaIblocks) {
        foreach ($aModuleMenu as $m => $arMenu) {
            if (in_array($arMenu['title'], $arrParentIBlockType)) {
                unset($aModuleMenu[$m]);
            }

            if ($arMenu["module_id"] === 'iblock' && $arMenu["items"]) {
                foreach ($arMenu["items"] as $i => $arItem) {
                    if (strpos($arItem["items_id"], 'menu_iblock') !== false) { // пункты меню с инфоблоками
                        if (in_array($aModuleMenu[$m]["items"][$i]['text'], $arrParentIBlockTitle)) { // ID инфоблока в меню нет, проверить можно по URL
                            unset($aModuleMenu[$m]["items"][$i]); // Убираем пункт меню с неактивным инфоблоком
                        }
                        // Проверяем, совпадает ли название инфоблока с тем, что нужно скрыть
                        foreach ($arItem['items'] as $j => $arSubItem) {
                            // Если название раздела совпадает с тем, что нужно скрыть
                            if (in_array($arSubItem['text'], $arrSectionsToHide)) {

                                unset($aModuleMenu[$m]["items"][$i]["items"][$j]); // Убираем раздел "Тест"
                            }
                        }


                    }
                }
            }
        }
    }
}

AddEventHandler("main", "OnBeforeEventAdd", array("MyClass", "OnBeforeEventAddHandler"));
class MyClass
{
    public static function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        $arFields["AUTHOR"] = $_POST['name'];
        $arFields["PHONE"] = $_POST['phone'];
        $arFields["MESSAGE"] = $_POST['message'];

        $lid = 's1'; //Изменяем привязку к сайту
    }
}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("СreateScript", "OnAfterIBlockElementUpdate"));
class СreateScript
{
    public static array $arProperty = [];
    public static string $propertyCode = '';
    // создаем обработчик события "OnAfterIBlockElementUpdate"
    public static function OnAfterIBlockElementUpdate(&$arFields)
    {
        if(CModule::IncludeModule('iblock')) {
            if (!empty($arFields['CODE'])) {
                self::$propertyCode = $arFields['CODE'];

            }
            foreach ($arFields['PROPERTY_VALUES'] as $key => $arField){
                if ($key == '455' && is_array($arField)){
                    self::$arProperty = $arField;


                }

            }
            foreach (self::$arProperty as $arField) {
                if (!empty($arField['VALUE']['TEXT'])) {
                    $scriptCode = $arField['VALUE']['TEXT'];
                    // Имя JavaScript файла
                    $filename = $_SERVER['DOCUMENT_ROOT'] .'/bitrix/templates/tpl_medall/scripts/service/'.self::$propertyCode.'.js';

// Попытка открыть файл для записи ('w' означает запись). Если файл не существует, он будет создан.
                    $file = fopen($filename, 'w');

                    if ($file) {
                        // JavaScript код для записи в файл с использованием PHP переменной
                        $content = <<<EOT
                            const nodeScriptIs = (node) => node.tagName === 'SCRIPT';
                            
                            const nodeScriptClone = (node) => {
                              const script = document.createElement('script');
                              script.text = node.innerHTML;
                              let i = 0;
                              let attr;
                              const attrs = node.attributes;
                              while (i < attrs.length) {
                                script.setAttribute((attr = attrs[i]).name, attr.value);
                                i += 1;
                              }
                              return script;
                            };
                            
                            const nodeScriptReplace = (node) => {
                              if (nodeScriptIs(node) === true) {
                                node.parentNode.replaceChild(nodeScriptClone(node), node);
                              } else {
                                let i = 0;
                                const children = node.childNodes;
                                while (i < children.length) {
                                  nodeScriptReplace(children[i]);
                                  i += 1;
                                }
                              }
                            
                              return node;
                            };
                            
                            const counters = (code, where = 'body') => {
                              const place = ['body', 'head'].includes(where) ? where : 'body';
                              const block = document.createElement('div');
                              block.innerHTML = code;
                              const userEvents = () => {
                                window.removeEventListener('scroll', userEvents);
                                window.removeEventListener('mousemove', userEvents);
                                document[place].append(...nodeScriptReplace(block).children);
                              };
                              window.addEventListener('scroll', userEvents);
                              window.addEventListener('mousemove', userEvents);
                            };
                            counters(`$scriptCode`, 'head');
                            EOT;
                        fwrite($file, $content);

                        // Закрываем файл после завершения записи
                        fclose($file);
                    }

                }

            }


        }
    }
}
function getResizedImageCopyPath($imagePath)
{
    $imagePathExplode = explode('.', $imagePath);
    $beforeLastIndex = count($imagePathExplode) - 2;
    $imagePathExplode[$beforeLastIndex] = $imagePathExplode[$beforeLastIndex].'_resized';
    $resizedImagePath = implode('.', $imagePathExplode);

    if(file_exists($_SERVER["DOCUMENT_ROOT"]."/".$resizedImagePath)){
        return $resizedImagePath;
    }

    return $imagePath;
}

function getBeforeAfterCurSectionCode(array $request)
{
    foreach ($request as $paramName => $paramVal) {

        if (stripos($paramName, 'PROPERTY') === false) {
            continue;
        }

        if ($paramName == "PROPERTY_PROCEDURE_PLASTIC_BEFORE_AFTER") {
            return "plastic";
        }

        if ($paramName == "PROPERTY_PROCEDURE_COSMETOLOGY_BEFORE_AFTER") {
            return "cosmetology";
        }

        if ($paramName == "PROPERTY_PROCEDURE_PHLEBOLOGY_BEFORE_AFTER") {
            return "phlebology";
        }

        if ($paramName == "PROPERTY_PROCEDURE_HAIR_TRANSPLANT_BEFORE_AFTER") {
            return "hair_transplant";
        }

        if ($paramName == "PROPERTY_PROCEDURE_DENTISTRY_BEFORE_AFTER") {
            return "dentistry";
        }
    }
}

function getBeforeAfterCurElementCode(array $request)
{
    foreach ($request as $paramName => $paramVal) {

        if (stripos($paramName, 'PROPERTY') === false) {
            continue;
        }

        return $paramVal;
    }
}

function filterCode($code)
{
    $codeExplode = explode('/', $code);
    $code = end($codeExplode);
    $codeExplode = explode('?', $code);
    $code = reset($codeExplode);

    return $code;
}

function isLandingEndontia(string $curPage): bool
{
    return $curPage == "/landings/endodontiya/";
}

function hideContentForAdmin()
{
    global $USER;
    if ($USER->GetLogin() == "rt-webdesign-new"){
        return true;
    }

    return false;
}

function isServiceInNewDesign($curPage, $code): bool
{
    $developers = [
        "rt-webdesign-new",
        "dmitriy.m",
    ];
    global $USER;

    $elementCode = filterCode($code);

    $readyServicesCodes = [
        "m22",
        "emtone",
        "endodontiya",
        "all-on-4",
        "lecheniekariesa",
        "konturnaya-plastika",
        "hydra_peel",
        "smas_lifting",
        "udaleniezubov",
        "tens-terapiya",
        "emdogain",
        "neotlozhka",
        "restoration",
        "merch",
        "LDM1",
        "laserbiorev",
        "lift6medall",
        "fonoforezmedall",
        "LPG",
        "teammedall",
        "mikrotoki",
        "combichistka",
        "profcosmetics",
        "ultrazvukchistka",
        "piling",
        "laserudalenie",
        "lasershlif",
        "laboratorymedall",
        "lasersosudov",
        "lipolitiki",
        "placentaltherapy",
        "plazmolifting",
        "laserepil",
        "mesotherapy",
        "tredlifting",
        "botox",
        "kollagen",
        "biorevitalization",
        "mikrotokivolos",
        "plasmatherapyvolos",
        "mezoherapyvolos",
        "rassrochka",
        "manualnayaterapia",
        "osteopatiya",
        "ginecologia",
        "interoperatsionnaya-kosmetologiya",
        "podgotovka-k-operatsii",
        "reabilitatsiya-posle-operatsii",
        "hr",
        "kontur-intim",
        "laser-intim",
        "intimnyy-plazmolifting",
        "lechenie-vaginizma",
        "intimnyy-piling",
        "otbelivanie-intimnoy-zony",
        "intimnye-regenerativnye-patchi",
        "poslerodovoe-vosstanovlenie",
        "lechenie-nederzhaniya-mochi",
        "poslerodovoe-vosstanovlenie-2",
        "poslerodovoe-vosstanovlenie-3",
        "biorevitalization-intim",
        "check-up",
        "uzi",
        "patsientam",
        "letnee-predlozhenie",
        "o-klinike",
        "heleo",
        "smas_mpt",
        "rf-lifting",
        "anketa",
        "uvelichenie-grudi",
        "nalog",
    ];

    if(
        in_array($USER->GetLogin(), $developers)
        OR strpos($curPage, "plastic") !== false
        OR in_array($elementCode, $readyServicesCodes)
    ){
        return true;
    }

    return false;
}

function getIbElement($elementId, $isOnlyFields = false)
{
    $res = \CIBlockElement::GetList([], ["ID" => $elementId]);

    if(!$bxElement = $res->GetNextElement()){
        return null;
    }

    $element = new stdClass();
    $element->fields = $bxElement->GetFields();

    if($isOnlyFields){
        return $element->fields;
    }

    $element->props = $bxElement->GetProperties();


    return $element;
}


function getIbElementList($elementId, $isOnlyFields = false)
{
    $arFilter = [
        "IBLOCK_ID" => 68,
        'ACTIVE' => 'Y',
        "PROPERTY_DOCTOR" => $elementId
    ];
    $arSelect = [
        'NAME',
        'PROPERTY_PHOTO_BEFORE',
        'PROPERTY_PHOTO_AFTER',
        'PROPERTY_DOCTOR',
        'PROPERTY_SERVICE_BINDING',
        'PROPERTY_SERVICE_NAME'

    ];
    $res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    $arFieldsList = [];

    while ($bxElement = $res->GetNextElement()) {
        $arFieldsObj = new stdClass();
        $arFieldsObj->fields = $bxElement->GetFields();
        $arFieldsObj->props = $bxElement->GetProperties();
        $arFieldsList[] = $arFieldsObj;
    }

    return $arFieldsList; // Added return statement for better function output handling
}


function getIbElementFields($elementId)
{
    return getIbElement($elementId, true);
}

function getIbElementImageSrc($imageId)
{
    return !empty($imageId) ? \CFile::GetPath($imageId) : null;
}

function isUserDeveloper()
{
    global $USER;

    $allowedAdmins = ["rt-webdesign", "rt-webdesign-new", "anastasia.k", "dmitriy.m", "zarya"];
    if(!in_array($USER->GetLogin(), $allowedAdmins)){
        return false;
    }

    return true;
}



AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', ['LandingsDoctorsProperty', 'OnAfterIBlockElementUpdateHandler']);
AddEventHandler('iblock', 'OnBeforeIBlockElementUpdate', ['LandingsDoctorsProperty', 'OnBeforeIBlockElementUpdateHandler']);


class LandingsDoctorsProperty
{
    public static function OnBeforeIBlockElementUpdateHandler(&$arFieldsUser)
    {

        if ($arFieldsUser['IBLOCK_ID'] == 6) { //врачи
            {
                if (Loader::includeModule('iblock')) {

                    $iblock_id = 6; // врачи
                    $iblock_services = 38; // Услуги
                    $landing_iblock = 47;
                    $log = [];



                    /**
                     * Всегда считаем что модуль установлен,
                     * поэтому просто подключаем его
                     */
                    \Bitrix\Main\Loader::IncludeModule('iblock');

                    /**
                     * У меня для тестирования инфоблок с номером 5,
                     * у тебя может быть любой другой инфобло
                     *
                     * @var array Параметры фильтрации
                     */
                    $arFilter = [
                        'IBLOCK_ID' => 47,
                    ];

                    /**
                     * В Битриксе разделы хранятся по принципу Nested sets
                     *
                     * @var array Параметры сортировки
                     */
                    $arOrder = [
                        'LEFT_MARGIN' => 'ASC',
                    ];

                    /**
                     * @var array Поля разделов
                     */
                    $arSelectSection = [
                        'ID',
                        'LEFT_MARGIN',
                        'DEPTH_LEVEL',
                        'NAME',
                    ];

                    $resSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelectSection);

                    while($arSection = $resSections->fetch())
                    {
                        $arrLandingId[] = $arSection;
                    }
                    foreach ($arrLandingId as $item){
                        $idSections[] = $item['ID'];
                    }

                    /* Врачи start */

                    $arSelect = [
                        "ID",
                        "NAME",
                        'PROPERTY_33',
                        'PROPERTY_217',
                        'PROPERTY_218',
                        'PROPERTY_219',
                        'PROPERTY_220',

                    ];

                    $arFilter = ['IBLOCK_ID' => $iblock_id, 'ACTIVE' => 'Y',];
                    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
                    $arrDoctors = [];
                    $currentServicesElementName = [];
                    $services = [];
                    $arrLanding = [];
                    $currentArrLanding = [];
                    $arrLandingStatus = true;
                    $idSections = [];
                    $arrCurrentLandingUserId = [];
                    $arrCurrentServicesName = [];

                    while ($ob = $res->fetch()) {
                        $arrDoctors[] = $ob;
                    }

                    foreach ($arrDoctors as $item) {
                        if ($item['ID'] == $_REQUEST['ID']){
                            if (!empty($item['PROPERTY_33_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_33_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $currentServicesElementName = array_unique($currentServicesElementName);

                            }

                            if (!empty($item['PROPERTY_217_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_217_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }

                            if (!empty($item['PROPERTY_218_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_218_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }

                            if (!empty($item['PROPERTY_219_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_219_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $currentServicesElementName = array_unique($currentServicesElementName);

                            }

                            if (!empty($item['PROPERTY_220_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_220_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }
                        }
                    }

                    $currentServicesElementName = array_unique($currentServicesElementName);
                    $_SESSION['ARR_ELEMENTS'] = $currentServicesElementName;


                }
            }
        }
    }



    public static function OnAfterIBlockElementUpdateHandler(&$arFieldsUser)
    {

        if ($arFieldsUser['IBLOCK_ID'] == 6) { //врачи
            {
                if (Loader::includeModule('iblock')) {

                    $iblock_id = 6; // врачи
                    $iblock_services = 38; // Услуги
                    $landing_iblock = 47;
                    $arrIdLanding = '';
                    $arrException = ['1171',];



                    /**
                     * Всегда считаем что модуль установлен,
                     * поэтому просто подключаем его
                     */
                    \Bitrix\Main\Loader::IncludeModule('iblock');

                    /**
                     * У меня для тестирования инфоблок с номером 5,
                     * у тебя может быть любой другой инфобло
                     *
                     * @var array Параметры фильтрации
                     */
                    $arFilter = [
                        'IBLOCK_ID' => 47,
                    ];

                    /**
                     * В Битриксе разделы хранятся по принципу Nested sets
                     *
                     * @var array Параметры сортировки
                     */
                    $arOrder = [
                        'LEFT_MARGIN' => 'ASC',
                    ];

                    /**
                     * @var array Поля разделов
                     */
                    $arSelectSection = [
                        'ID',
                        'LEFT_MARGIN',
                        'DEPTH_LEVEL',
                        'NAME',
                    ];

                    $resSections = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelectSection);

                    while($arSection = $resSections->fetch())
                    {
                        $arrLandingId[] = $arSection;
                    }
                    foreach ($arrLandingId as $item){
                        $idSections[] = $item['ID'];
                    }

                    /* Врачи start */

                    $arSelect = [
                        "ID",
                        "NAME",
                        'PROPERTY_33',
                        'PROPERTY_217',
                        'PROPERTY_218',
                        'PROPERTY_219',
                        'PROPERTY_220',

                    ];

                    $arFilter = ['IBLOCK_ID' => $iblock_id, 'ACTIVE' => 'Y',];
                    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
                    $arrDoctors = [];
                    $currentServicesElementName = [];
                    $services = [];
                    $arrLanding = [];
                    $currentArrLanding = [];
                    $arrLandingStatus = true;
                    $idSections = [];
                    $arrCurrentLandingUserId = [];
                    $arrCurrentServicesName = [];

                    while ($ob = $res->fetch()) {
                        $arrDoctors[] = $ob;
                    }

                    foreach ($arrDoctors as $k => $item) {
                        if ($item['ID'] == $_REQUEST['ID']){
                            if (!empty($item['PROPERTY_33_VALUE'])) {

                                $res = CIBlockElement::GetByID($item['PROPERTY_33_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $arrIdLanding = $ar_res['ID'];
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }

                            if (!empty($item['PROPERTY_217_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_217_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $arrIdLanding = $ar_res['ID'];
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }

                            if (!empty($item['PROPERTY_218_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_218_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $arrIdLanding = $ar_res['ID'];
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }

                            if (!empty($item['PROPERTY_219_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_219_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $arrIdLanding = $ar_res['ID'];
                                $currentServicesElementName = array_unique($currentServicesElementName);

                            }

                            if (!empty($item['PROPERTY_220_VALUE'])) {
                                $res = CIBlockElement::GetByID($item['PROPERTY_220_VALUE']);
                                $ar_res = $res->GetNext();
                                $currentServicesElementName[] = trim($ar_res['NAME']);
                                $arrIdLanding = $ar_res['ID'];
                                $currentServicesElementName = array_unique($currentServicesElementName);


                            }
                        }
                    }

                    $currentServicesElementName = array_unique($currentServicesElementName);
                    $result = array_diff($currentServicesElementName,$_SESSION['ARR_ELEMENTS']);
                    $emptyResult = array_diff($_SESSION['ARR_ELEMENTS'],$currentServicesElementName);

                    $totalResult = array_shift($result);
                    $emptyTotalResult = array_shift($emptyResult);


                    /* Врачи end */

                    /* Услуги start */

                    $arSelectServices = [
                        'NAME',
                        'ID',

                    ];

                    $arFilterServices =  [
                        'IBLOCK_ID' => $landing_iblock,
                        'ACTIVE_DATE' => 'Y',
                        'ACTIVE' => 'Y',
                        'IBLOCK_SECTION_ID' => $idSections,
                    ];

                    $resService = CIBlockElement::GetList([], $arFilterServices, false, false, $arSelectServices);

                    while ($ob = $resService->fetch()) {
                        $services[] = $ob;
                    }

                    foreach ($services as $item) {

                        if (in_array(trim($item['NAME']),$currentServicesElementName)) {
                            $arrCurrentServicesName[] = trim($item['NAME']);
                            $arrCurrentServicesName = array_unique($arrCurrentServicesName);
                        }
                    }

                    /* Услуги end */

                    $arSelectLanding = [
                        'NAME',
                        'PROPERTY_489',
                        'PROPERTY_217',
                        'PROPERTY_218',
                        'PROPERTY_219',
                        'PROPERTY_220',
                        'PROPERTY_33',
                        'ID',

                    ];

                    $arFilterLanding =  [
                        'IBLOCK_ID' => $landing_iblock,
                        'ACTIVE_DATE' => 'Y',
                        'ACTIVE' => 'Y',
                        'IBLOCK_SECTION_ID' => $idSections,

                    ];

                    $resLanding = CIBlockElement::GetList([], $arFilterLanding, false, false, $arSelectLanding);

                    while ($ob = $resLanding->fetch()) {
                        $arrLanding[] = $ob;
                    }
                    $currentLandingName = [];
                    $arrCurrentLandingOtherValues = [];

                    foreach ($arrLanding as $landing) {
                        if (in_array(trim($landing['NAME']), $arrCurrentServicesName)) {
                            // Получаем остальные элементы массива
                            // Выводим результат
                            $currentLandingName[] = $landing['NAME'];
                            $currentLandingName = array_unique($currentLandingName);
                            // Добавляем другие значения $_REQUEST['ID'], удовлетворяющие условию
                        }
                    }
                    foreach ($arrLanding as $landing){
                        if (in_array($landing['ID'],$arrException)){
                            continue;
                        }
                        // Добавляем добавление

                        if (empty($emptyTotalResult) && $landing['ID'] == $arrIdLanding){
                            $arrCurrentLandingUserId[] = $landing['PROPERTY_489_VALUE'];
                            $arrCurrentLandingUserId[] = $_REQUEST['ID'];
                            $arrCurrentLandingUserId = array_unique($arrCurrentLandingUserId);
                            $currentArrLanding[] = $landing['ID'];
                            $currentArrLanding = array_unique($currentArrLanding);

                        }
                        // добавлеие/удаление
                        if (
                            !empty($emptyTotalResult)
                            && trim($emptyTotalResult) == trim($landing['NAME'])
                            && $landing['PROPERTY_489_VALUE'] != $_REQUEST['ID']
                        ) {
                            $arrCurrentLandingUserId[] = $landing['PROPERTY_489_VALUE'];
                            $arrCurrentLandingUserId = array_unique($arrCurrentLandingUserId);
                            $currentArrLanding[] = $landing['ID'];
                            $currentArrLanding = array_unique($currentArrLanding);

                        } elseif(
                            !empty($emptyTotalResult)
                            && trim($emptyTotalResult) == trim($landing['NAME'])
                            && $landing['PROPERTY_489_VALUE'] == $_REQUEST['ID']
                        ) {
                            $arrCurrentLandingUserId[] = '';
                            $arrCurrentLandingUserId = array_unique($arrCurrentLandingUserId);
                            $currentArrLanding[] = $landing['ID'];
                            $currentArrLanding = array_unique($currentArrLanding);
                        }

                    }


                    function removeEmptyElements(array $inputArray): array {
                        // Удаляем пустые элементы
                        return array_values(array_filter($inputArray));
                    }

                    $filteredArray = removeEmptyElements($arrCurrentLandingUserId);

                    foreach ($currentArrLanding as $key => $landingId) {
                        try {
                            if (!empty($filteredArray)){


                                $status = CIBlockElement::SetPropertyValueCode($landingId, 'DOCTORS_GLOBAL_ITEMS', $filteredArray);

                            }else{

                                $status = CIBlockElement::SetPropertyValueCode($landingId, 'DOCTORS_GLOBAL_ITEMS', $arrCurrentLandingUserId);

                            }
                            $arrCurrentLandingUserId = (string)$arrCurrentLandingUserId;

                            if ($status !== true) {
                                throw new \Exception("Failed to set property for landing ID: $landingId ". " " ."$arrCurrentLandingUserId");
                            }
                        } catch (\Exception $e) {
                            echo $e->getMessage();
                            die();
                        }
                    }





                }
            }
        }
    }


}
