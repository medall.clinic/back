<?php
/** @global CMain $APPLICATION */
/** @global string $DOCUMENT_ROOT */
/** @global string $REQUEST_METHOD */
/** @global string $RestoreDefaults */
$module_id = 'solverweb.sitemap';
$CAT_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($CAT_RIGHT > 'D') {
	IncludeModuleLangFile($DOCUMENT_ROOT . BX_ROOT . '/modules/main/options.php');

	include_once(__DIR__ . '/include.php');

	if ($REQUEST_METHOD == 'GET' && strlen($RestoreDefaults) > 0 && $CAT_RIGHT == 'W' && check_bitrix_sessid()) {
		COption::RemoveOption($module_id);
		$z = CGroup::GetList($sort = 'id', $order = 'asc', array('ACTIVE' => 'Y', 'ADMIN' => 'N'));
		while ($zr = $z->Fetch())
			$APPLICATION->DelGroupRight($module_id, array($zr['ID']));

		LocalRedirect($APPLICATION->GetCurPage() . '?lang=' . LANG . '&mid=' . urlencode($module_id));
	}

	$aTabs = array(
		array(
			'DIV' => 'edit1',
			'TAB' => GetMessage('MAIN_TAB_RIGHTS'),
			'ICON' => 'sitemap_settings',
			'TITLE' => GetMessage('MAIN_TAB_TITLE_RIGHTS')
		),
	);
	$tabControl = new CAdminTabControl('tabControl', $aTabs);

	$tabControl->Begin();
	?>
    <form method="POST"
          action="<?=$APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($module_id)?>&lang=<?=LANGUAGE_ID?>"
          name="<?=$module_id?>">
		<?php
		echo bitrix_sessid_post();

		$tabControl->BeginNextTab();

		require_once($DOCUMENT_ROOT . BX_ROOT . '/modules/main/admin/group_rights.php');

		$tabControl->Buttons();
		?>
        <script>
            function RestoreDefaults() {
                if (confirm('<?=GetMessageJS('MAIN_HINT_RESTORE_DEFAULTS_WARNING')?>'))
                    window.location = "<?=$APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?=LANGUAGE_ID?>&mid=<?=urlencode($module_id)?>&<?=bitrix_sessid_get()?>";
            }
        </script>
        <input type="submit" <?=($CAT_RIGHT < 'W') ? 'disabled' : ''?> name="Update" value="<?=GetMessage("MAIN_SAVE")?>">
        <input type="hidden" name="Update" value="Y">
        <input type="reset" name="reset" value="<?=GetMessage('MAIN_RESET')?>">
        <input type="button" <?=($CAT_RIGHT < 'W') ? 'disabled' : ''?>
               title="<?=GetMessage('MAIN_HINT_RESTORE_DEFAULTS')?>" OnClick="RestoreDefaults();"
               value="<?=GetMessage('MAIN_RESTORE_DEFAULTS')?>">
		<?php $tabControl->End(); ?>
    </form>
<?php
}