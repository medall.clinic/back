CREATE TABLE `sw_sitemap_xml` (
	`ID` int(11) NOT NULL AUTO_INCREMENT,
	`SITE_ID` char(2) NOT NULL,
	`ACTIVE` char(1) DEFAULT 'Y',
	`NAME` varchar(255) DEFAULT '',
	`AGENT` int(11) NULL,
	`TIMESTAMP_X` timestamp,
	`LAST_RUN` datetime NULL DEFAULT NULL,
	`SETTINGS` longtext,
	PRIMARY KEY (`ID`)
)