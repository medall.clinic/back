<?
use Bitrix\Main\Localization\Loc;

$module_id = "ram.watermark";

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MODULE_RIGHT === 'D')
{
	$APPLICATION->AuthForm(Loc::getMessage("ram.watermark_ACCESS_DENIED"));
}

Loc::loadMessages(__FILE__);

\Bitrix\Main\Loader::includeModule('ram.watermark');

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");

IncludeModuleLangFile(__FILE__);

$uploadDir = \Bitrix\Main\Config\Option::get("main", "upload_dir", "upload");

$aTabs = array(
	Array("DIV" => "params", "TAB" => Loc::getMessage("ram.watermark_TAB_PARAMS"), "TITLE" => Loc::getMessage("ram.watermark_TAB_PARAMS_TITLE")),
	Array("DIV" => "rights", "TAB" => Loc::getMessage("ram.watermark_TAB_RIGHTS"), "TITLE" => Loc::getMessage("ram.watermark_TAB_RIGHTS_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update.$RestoreDefaults)>0 && check_bitrix_sessid() && $MODULE_RIGHT === 'W')
{
	if (strlen($RestoreDefaults) > 0)
	{
		$z = \CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
		while ($zr = $z->Fetch())  $APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
		
		LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&".$tabControl->ActiveTabParam());
	}
	else
	{
		\Bitrix\Main\Config\Option::set('ram.watermark', 'include_files', htmlspecialcharsbx($_REQUEST['ramwatermark']['include_files']));
		\Bitrix\Main\Config\Option::set('ram.watermark', 'exclude_files', htmlspecialcharsbx($_REQUEST['ramwatermark']['exclude_files']));
		\Bitrix\Main\Config\Option::set('ram.watermark', 'convert_to_webp', htmlspecialcharsbx($_REQUEST['ramwatermark']['convert_to_webp']));
		\Bitrix\Main\Config\Option::set('ram.watermark', 'protect_time', htmlspecialcharsbx($_REQUEST['ramwatermark']['protect_time']));
		\Bitrix\Main\Config\Option::set('ram.watermark', 'show_modules', htmlspecialcharsbx($_REQUEST['ramwatermark']['show_modules']));
	}
}

$tabControl->Begin();
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&amp;lang=<?echo LANG?>" name="ram.watermark_settings">
<?=bitrix_sessid_post();?>
<?
$tabControl->BeginNextTab();
$includeFiles = \Bitrix\Main\Config\Option::get("ram.watermark", "include_files", "");
$excludeFiles = \Bitrix\Main\Config\Option::get("ram.watermark", "exclude_files", "/bitrix/admin/1c_exchange.php\n");
$convertToWebp = \Bitrix\Main\Config\Option::get("ram.watermark", "convert_to_webp", \Bitrix\Main\Config\Option::get("ram.watermark", "conver_to_webp", "N"));
$protectTime = \Bitrix\Main\Config\Option::get("ram.watermark", "protect_time", "0.5");
$showModules = \Bitrix\Main\Config\Option::get("ram.watermark", "show_modules", "N");
?>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_INCLUDE_FILES")?></td>
	<td width="50%">
		<textarea style='width: 300px; height: 60px;' name="ramwatermark[include_files]"><?=$includeFiles?></textarea>
	</td>
</tr>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_EXCLUDE_FILES")?></td>
	<td width="50%">
		<textarea style='width: 300px; height: 60px;' name="ramwatermark[exclude_files]"><?=$excludeFiles?></textarea>
	</td>
</tr>
<?
$webpConvertion = false;
if ($mainModuleInfo = CModule::CreateModuleObject('main'))
{
	$webpConvertion = CheckVersion($mainModuleInfo->MODULE_VERSION, '20.5.500');
}
if (!function_exists("imagecreatefromwebp")) $webpConvertion = false;
if (!$webpConvertion) $convertToWebp = "N";
?>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_CONVERT_TO_WEBP")?></td>
	<td>
		<input type="hidden" value="N" name="ramwatermark[convert_to_webp]"/>
		<input type="checkbox" value="Y" name="ramwatermark[convert_to_webp]" <?=($convertToWebp === "Y"?"checked":"")?> <?=(!$webpConvertion?"disabled":"")?>/>
	</td>
</tr>
<?
if (!$webpConvertion)
{
	?>
	<tr>
		<td colspan='2' align='center'>
			<div class='adm-info-message-wrap' align='center'>
				<div class='adm-info-message'><?=Loc::getMessage("ram.watermark_PARAMS_CONVERT_TO_WEBP_VERSION")?></div>
			</div>
		</td>
	</tr>
	<?
}
?>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_PROTECT_TIME")?></td>
	<td><input type="text" size="10" maxlength="255" value="<?=$protectTime?>" name="ramwatermark[protect_time]" /></td>
</tr>
<tr>
	<td colspan='2' align='center'>
		<div class='adm-info-message-wrap' align='center'>
			<div class='adm-info-message'><?=Loc::getMessage("ram.watermark_PARAMS_PROTECT_TIME_HIT")?></div>
		</div>
	</td>
</tr>
<tr>
	<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_SHOW_MODULES")?></td>
	<td>
		<input type="hidden" value="N" name="ramwatermark[show_modules]"/>
		<input type="checkbox" value="Y" name="ramwatermark[show_modules]" <?=($showModules === "Y"?"checked":"")?>/>
	</td>
</tr>
<?

$tabControl->BeginNextTab();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

if ($MODULE_RIGHT === 'W')
{
	$tabControl->Buttons();?>

	<script type="text/javascript">
	function confirmRestoreDefaults()
	{
		return confirm('<?=AddSlashes(Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>');
	}
	</script>
	<input type="submit" name="Update" value="<?=Loc::getMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?=Loc::getMessage("MAIN_RESET")?>">
	<input type="submit" name="RestoreDefaults" title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" onclick="return confirmRestoreDefaults();" value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS")?>">

	<?
}
?>

<?$tabControl->End();?>
</form>