<?
use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity;
use Bitrix\Main\Type;
use Bitrix\User;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

$module_id = 'ram.watermark';

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MODULE_RIGHT === 'D')
{
	$APPLICATION->AuthForm(Loc::getMessage('ACCESS_DENIED'));
}

Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle(Loc::getMessage('ram.watermark_TITLE'));

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
	
Loader::includeModule($module_id);

$context = Application::getInstance()->getContext();
$request = $context->getRequest();

if ($request->isPost() && $MODULE_RIGHT === 'W')
{
	$urls = Array();
	$includeUrls = Array();
	$excludeUrls = Array();
	$input_urls = explode("\n", $request['ramwatermark']['urls']);
	foreach ($input_urls as $url)
	{
		$url = trim($url);
		if (strlen($url) > 0 && (substr_count($url, "http://") || substr_count($url, "https://")))
		{
			$urls[] = $url;
			$includeUrls[] = base64_encode($url);
		}
	}
	$input_exclude_urls = explode("\n", $request['ramwatermark']['exclude_urls']);
	foreach ($input_exclude_urls as $url)
	{
		$url = trim($url);
		if (strlen($url) > 0)
		{
			$excludeUrls[] = base64_encode($url);
		}
	}
	
	if (!empty($includeUrls))
	{
		\Bitrix\Main\Application::getConnection()->truncateTable("ram_watermark_processurl");
		\Bitrix\Main\Application::getConnection()->truncateTable("ram_watermark_processimage");
		
		CJSCore::Init(array("jquery"));
		
		?>
		<div class="adm-info-message-wrap adm-info-message-gray ram-watermark-progress">
			<div class="adm-info-message">
				<div class="adm-info-message-title"><?=Loc::getMessage("ram.watermark_URLS_PROCESS")?></div>
				<div class="adm-progress-bar-outer" style="width: 500px;">
					<div class="adm-progress-bar-inner" style="width: 0px;">
						<div class="adm-progress-bar-inner-text" style="width: 500px;">0%</div>
					</div>0%
				</div>
				<br/>
				<div class="ram-watermark-progress-info"></div>
			</div>
		</div>
		<script type='text/javascript'>
			var ramwmIncludeUrls = <?=json_encode($includeUrls)?>;
			var ramwmExcludeUrls = <?=json_encode($excludeUrls)?>;
			var ramwmUrls = <?=json_encode($urls)?>;
			var ramwmTotalUrls = <?=count($urls)?>;
			var ramwmCheckedUrls = 0;
			var ramwmAjaxError = "<?=Loc::getMessage("ram.watermark_AJAX_ERROR")?>";
			
			$(document).ready(function()
			{
				RamWmProcessUrl();
			});
			
			function RamWmProcessUrl()
			{				
				if (ramwmUrls.length > 0)
				{
					var percent = (ramwmCheckedUrls/ramwmTotalUrls*100).toFixed(0);
					$('.ram-watermark-progress .adm-progress-bar-outer').html('<div class="adm-progress-bar-inner" style="width: '+(500*percent/100).toFixed(0)+'px;"><div class="adm-progress-bar-inner-text" style="width: 500px;">'+(ramwmCheckedUrls+' / '+ramwmTotalUrls)+'</div></div>'+(ramwmCheckedUrls+' / '+ramwmTotalUrls));
					
					$(".ram-watermark-progress-info").html(ramwmUrls[0]);
					
					var url = ramwmUrls[0];
					if (url.search("\\?") == -1) url += "?";
					else url += "&";
					url += "ramwmprocess=Y&clear_cache=Y&ramwminclude[]="+ramwmIncludeUrls.join("&ramwminclude[]=")+"&ramwmexclude[]="+ramwmExcludeUrls.join("&ramwmexclude[]=");
					
					// console.log(url);
					
					var ifr = $("<iframe/>",
					{
						src: url,
						style: "display: none;",
					});
					
					$(ifr).load(function()
					{
						if ($(ifr).attr("loaded") != "true")
						{
							setTimeout(function()
							{
								$(ifr).attr("loaded", "true").attr("src", "about:blank");
							}, 100);
						}
						else
						{
							setTimeout(function()
							{
								ramwmUrls.shift();
								$(ifr).remove();
								ramwmCheckedUrls++;
								setTimeout(function(){RamWmProcessUrl();}, 100);
							}, 100);
						}
					});
					
					$("body").append($(ifr));
				}
				else
				{
					$.ajax({url: '/bitrix/tools/ram.watermark.php', type: 'POST', data: {action: 'get_unvisited_processurl'}}).done(function(msg)
					{
						try
						{
							ramwmUrls = $.parseJSON(msg);
							if (ramwmUrls.length > 0)
							{
								ramwmTotalUrls += ramwmUrls.length;
								
								setTimeout(function(){RamWmProcessUrl();}, 500);
							}
							else
							{
								// console.log("done");
								
								document.location.search += "&step=images&ramwatermark[delete_cache]=<?=$request['ramwatermark']['delete_cache']?>";
							}
						}
						catch(e)
						{
							alert(ramwmAjaxError);
						}
					});
				}
			}
		</script>
		<?
	}
	else
	{
		LocalRedirect("ram.watermark_process.php?lang=".LANGUAGE_ID);
	}
}
else if ($request->get("step") === "images")
{
	if ($request['ramwatermark']['delete_cache'] === 'Y')
	{
		$uploadDir = \Bitrix\Main\Config\Option::get("main", "upload_dir", "upload");
		
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"]."/".$uploadDir."/resize_cache/".$module_id);
		
		\Bitrix\Main\Application::getConnection()->truncateTable("ram_watermark_image");
	}
	
	$totalImages = Ram\Watermark\ProcessimageTable::getList(Array("select" => Array("CNT"), "runtime" => Array(new Entity\ExpressionField("CNT", "COUNT(*)"))))->fetch();
	
	CJSCore::Init(array("jquery"));
	
	?>
	<div class="adm-info-message-wrap adm-info-message-gray ram-watermark-progress">
		<div class="adm-info-message">
			<div class="adm-info-message-title"><?=Loc::getMessage("ram.watermark_FILES_PROCESS")?></div>
			<div class="adm-progress-bar-outer" style="width: 500px;">
				<div class="adm-progress-bar-inner" style="width: 0px;">
					<div class="adm-progress-bar-inner-text" style="width: 500px;">0%</div>
				</div>0%
			</div>
		</div>
	</div>
	<script type='text/javascript'>
		var ramwmTotalImages = <?=$totalImages["CNT"]?>;
		var ramwmQueuedImages = <?=$totalImages["CNT"]?>;
		var ramwmAjaxError = "<?=Loc::getMessage("ram.watermark_AJAX_ERROR")?>";
		
		$(document).ready(function()
		{
			var percent;
			if (ramwmTotalImages == 0) percent = 100;
			else percent = ((ramwmTotalImages-ramwmQueuedImages)/ramwmTotalImages*100).toFixed(0);
			
			$('.ram-watermark-progress .adm-progress-bar-outer').html('<div class="adm-progress-bar-inner" style="width: '+(496*percent/100).toFixed(0)+'px;"><div class="adm-progress-bar-inner-text" style="width: 500px;">'+percent+'%</div></div>'+percent+'%');
			
			if (ramwmTotalImages != 0) RamWmProcessImage();
		});
		
		function RamWmProcessImage()
		{
			$.ajax({url: '/bitrix/tools/ram.watermark.php', type: 'POST', data: {action: 'process_images'}}).done(function(msg)
			{
				try
				{
					var json = $.parseJSON(msg);
					ramwmQueuedImages -= parseInt(json.count);
					
					var percent = ((ramwmTotalImages-ramwmQueuedImages)/ramwmTotalImages*100).toFixed(0);
				
					$('.ram-watermark-progress .adm-progress-bar-outer').html('<div class="adm-progress-bar-inner" style="width: '+(496*percent/100).toFixed(0)+'px;"><div class="adm-progress-bar-inner-text" style="width: 500px;">'+percent+'%</div></div>'+percent+'%');
					
					if (ramwmQueuedImages > 0)
					{
						setTimeout(function(){RamWmProcessImage();}, 500);
					}
				}
				catch(e)
				{
					alert(ramwmAjaxError);
				}
			});
		}
	</script>
	<?
}
else
{
	$aTabs = Array(
		Array("DIV" => "params", "TAB" => Loc::getMessage("ram.watermark_TABS_params"), "TITLE" => Loc::getMessage("ram.watermark_TABS_params_title")),
	);

	$tabControl = new \CAdminTabControl("ramWatermarkTabControl", $aTabs);
	
	?><form id='ram-watermark-form' method="POST" action="<?=$APPLICATION->GetCurPageParam()?>" name="ram_watermark_edit" enctype="multipart/form-data"><?=bitrix_sessid_post()?><?
	
	$tabControl->Begin();

	$tabControl->BeginNextTab();
	
	$url = (strtoupper($_SERVER["HTTPS"])==="ON"?"https":"http")."://".$_SERVER["SERVER_NAME"]."/\n";
	$excludeUrl = (strtoupper($_SERVER["HTTPS"])==="ON"?"https":"http")."://".$_SERVER["SERVER_NAME"]."/bitrix/\n".
		(strtoupper($_SERVER["HTTPS"])==="ON"?"https":"http")."://".$_SERVER["SERVER_NAME"]."/upload/\n".
		"/filter/\n".
		"sort=\n".
		"view=\n".
		"filter=\n";
	
	?>
	<tr>
		<td colspan='2'>
			<div class="adm-info-message-wrap" style="text-align: center;"><div class="adm-info-message"><?=Loc::getMessage("ram.watermark_PROCESS_INFO")?></div></div>
		</td>
	</tr>
	<tr>
		<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_SECTIONS")?></td>
		<td width="50%">
			<textarea style='width: 300px; height: 60px;' name="ramwatermark[urls]"><?=$url?></textarea>
		</td>
	</tr>
	<tr>
		<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_SECTIONS_EXCLUDE")?></td>
		<td width="50%">
			<textarea style='width: 300px; height: 60px;' name="ramwatermark[exclude_urls]"><?=$excludeUrl?></textarea>
		</td>
	</tr>
	<tr>
		<td width="50%"><?=Loc::getMessage("ram.watermark_PARAMS_CLEAR_CACHE")?></td>
		<td width="50%">
			<input type="hidden" name="ramwatermark[delete_cache]" value="N"/>
			<input type="checkbox" name="ramwatermark[delete_cache]" value="Y"/>
		</td>
	</tr>
	<?
	
	if ($MODULE_RIGHT === 'W')
	{
		$tabControl->Buttons();
		?><input class="adm-btn-green" type="submit" name="save"  value="<?=Loc::getMessage("ram.watermark_START")?>" title="<?=Loc::getMessage("ram.watermark_START_TITLE")?>" /><?
	}

	$tabControl->End();
	
	?></form><?
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>