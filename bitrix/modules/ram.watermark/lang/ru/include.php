<?
$MESS["ram.watermark_WATERMARKS"] = "Водяные знаки";
$MESS["ram.watermark_FILES_SEARCH"] = "Поиск файлов для обработки";
$MESS["ram.watermark_FILES_PROCESS"] = "Обработка файлов";


$MESS["ram.watermark_ALL_MODULES"] = "Все модули";
$MESS["ram.watermark_MEDIALIB"] = "Медиабиблиотека";
$MESS["ram.watermark_CATALOGS"] = "Каталоги";
$MESS["ram.watermark_ALL_IBLOCKS"] = "Все инфоблоки";
$MESS["ram.watermark_ELEMENTS"] = "Элементы";
$MESS["ram.watermark_SUBELEMENTS"] = "Элементы (включая подразделы)";
$MESS["ram.watermark_SECTIONS"] = "Разделы";
$MESS["ram.watermark_SECTION"] = "Данный раздел";
$MESS["ram.watermark_ALL_FORUMS"] = "Все форумы";
$MESS["ram.watermark_ALL_HIGHLOADBLOCKS"] = "Все highload-блоки";
$MESS["ram.watermark_ALL_COLLECTIONS"] = "Все коллекции";
$MESS["ram.watermark_SELECT"] = "Выбрать";
$MESS["ram.watermark_SUBELEMENTS_COLLECTION"] = "Элементы (включая подколлекции)";
$MESS["ram.watermark_ALL_IMAGES"] = "Все изображения";
$MESS["ram.watermark_PREVIEW"] = "Картинка для анонса";
$MESS["ram.watermark_DETAIL"] = "Детальная картинка";
$MESS["ram.watermark_ALL_FILE_PROPS"] = "Все файловые свойства";
$MESS["ram.watermark_DELETE_FILTER"] = "Удалить";
$MESS["ram.watermark_COPY_FILTER"] = "Копировать";


$MESS["ram.watermark_EXPIRE"] = "У модуля «Водяные знаки» закончился срок действия ключа. Вы не сможете больше получать важные обновления модуля и техподдержку. Для бесплатного продления модуля, оставьте отзыв на <a href='https://marketplace.1c-bitrix.ru/solutions/ram.watermark/' target='_blank'>Маркетплейсе</a> и запросите ключ, написав на <a href='mailto:mail@rommats.ru' target='_blank'>mail@rommats.ru</a>";

?>