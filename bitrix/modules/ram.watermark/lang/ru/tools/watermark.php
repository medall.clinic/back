<?
$MESS["ram.watermark_SUCCESS_UPLOAD"] = "Загрузка завершена. Загруженный файл можно выбрать в списке.";
$MESS["ram.watermark_ERROR_UPLOAD"] = "Ошибка загрузки файла";
$MESS["ram.watermark_ERROR_EXISTS"] = "Файл с таким именем уже существует";
$MESS["ram.watermark_ERROR_TTF_TYPE"] = "Можно загружать только файлы формата TTF";
$MESS["ram.watermark_ERROR_IMAGE_TYPE"] = "Можно загружать только файлы форматов JPG, PNG, GIF, BMP и WEBP";
$MESS["ram.watermark_FILES_PROCESS"] = "Обработка файлов";
$MESS["ram.watermark_FILES_PROCESS_NONE"] = "Обрабатывать нечего";
$MESS["ram.watermark_FILES_PROCESS_FINISH"] = "Обработка завершена";
$MESS["ram.watermark_FILES_SEARCH"] = "Поиск файлов для обработки";
$MESS["ram.watermark_FILES_SEARCH_FINISH"] = "Поиск завершен";
$MESS["ram.watermark_LIST"] = "Список водяных знаков";
$MESS["ram.watermark_V1_SET_CLEAR_STATUS"] = "Изменение статуса изображений";
$MESS["ram.watermark_V1_CLEAR_IBLOCK_CACHE"] = "Сброс кеша инфоблоков";
$MESS["ram.watermark_V1_CLEAR_FILES"] = "Очистка от знаков. Количество изображений, которые необходимо обработать: ";
$MESS["ram.watermark_V1_REMOVE_OLD_EVENTS"] = "Удаление неиспользуемых событий";
$MESS["ram.watermark_V1_ADD_NEW_EVENTS"] = "Добавление событий";
$MESS["ram.watermark_V1_CONVERT_DATA"] = "Конвертация данных";
$MESS["ram.watermark_V1_GET_DATA_V1"] = "Сбор данных";
$MESS["ram.watermark_V1_UPDATE_STRUCTURE_V1"] = "Подготовка таблиц";
$MESS['ram.watermark_EMPTY_SELECT'] = 'Не выбран объект (отсутствуют поля для обработки)';
$MESS['ram.watermark_DELETE_FILTER'] = 'Удалить';
$MESS['ram.watermark_COPY_FILTER'] = 'Копировать';
?>