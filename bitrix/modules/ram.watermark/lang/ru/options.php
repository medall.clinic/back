<?
$MESS["ram.watermark_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["ram.watermark_TAB_PARAMS"] = "Параметры";
$MESS["ram.watermark_TAB_PARAMS_TITLE"] = "Настройка общих параметров модуля";
$MESS["ram.watermark_TAB_RIGHTS"] = "Доступ";
$MESS["ram.watermark_TAB_RIGHTS_TITLE"] = "Уровень доступа к модулю";
$MESS["ram.watermark_PARAMS_INCLUDE_FILES"] = "Страницы, на которых необходимо <b>включить</b> обработку изображений (каждая страница с новой строки)";
$MESS["ram.watermark_PARAMS_EXCLUDE_FILES"] = "Страницы, на которых необходимо <b>отключить</b> обработку изображений (каждая страница с новой строки)";
$MESS["ram.watermark_PARAMS_CONVERT_TO_WEBP"] = "Конвертировать изображения в формат WEBP";
$MESS["ram.watermark_PARAMS_CONVERT_TO_WEBP_VERSION"] = "<span style='color:#f00;'>Версия Главного модуля должна быть не ниже 20.5.500. Версия PHP должна быть не ниже 5.4.0</span>";
$MESS["ram.watermark_PARAMS_PROTECT_TIME"] = "Время обработки изображений на лету (сек)";
$MESS["ram.watermark_PARAMS_PROTECT_TIME_HIT"] = "Если за отведенное время модуль не успел обработать все изображения на странице, то обработка продолжится при следующем обращении к странице.";
$MESS["ram.watermark_PARAMS_SHOW_MODULES"] = "Отображать все модули в настройках знака";
?>