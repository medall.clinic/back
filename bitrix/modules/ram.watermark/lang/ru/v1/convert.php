<?
$MESS['ram.watermark_V1_CONVERT_DESCRIPTION'] = "Для перехода на новую версию модуля, необходимо запустить мастер, который выполнит необходимые настройки. Время работы мастера зависит от количества изображений.";
$MESS['ram.watermark_V1_CONVERT_TITLE'] = "Мастер";
$MESS['ram.watermark_V1_CONVERT_BTN'] = "Запустить";
$MESS['ram.watermark_AJAX_FINISH'] = "Обработка завершена. Обновите страницу.";
?>