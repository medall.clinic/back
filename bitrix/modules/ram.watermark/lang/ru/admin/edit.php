<?
$MESS["ram.watermark_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["ram.watermark_COPY"] = "Копирование водяного знака";
$MESS["ram.watermark_EDIT"] = "Редактирование водяного знака";
$MESS["ram.watermark_ADD"] = "Создание водяного знака";
$MESS["ram.watermark_TABS_params"] = "Параметры";
$MESS["ram.watermark_TABS_params_title"] = "Параметры водяного знака";
$MESS["ram.watermark_TABS_filter"] = "Объекты/поля";
$MESS["ram.watermark_TABS_filter_title"] = "Выбор объектов/полей для нанесения водяного знака";
$MESS["ram.watermark_ERROR_EMPTY"] = "Не заполнено поле";
$MESS["ram.watermark_NAME"] = "Название";
$MESS["ram.watermark_V1_CONVERT"] = "Мастер настройки";
$MESS["ram.watermark_FILE_CACHE_NOTIFY"] = 'Для правильной работы модуля "Водяные знаки", необходимо в файле "/bitrix/php_interface/dbconn.php" установить значение константы "CACHED_b_file" равное 0.';
$MESS["ram.watermark_FILE_BUCKET_SIZE_NOTIFY"] = 'Для оптимальной работы модуля "Водяные знаки", необходимо в файле "/bitrix/php_interface/dbconn.php" установить значение константы "CACHED_b_file_bucket_size" равное 1.';

$MESS["ram.watermark_PREVIEW_SETTINGS_SCHEME"] = "Показать/скрыть границы знака";
$MESS["ram.watermark_PREVIEW_SETTINGS_WHITE"] = "Светлый фон";
$MESS["ram.watermark_PREVIEW_SETTINGS_BLACK"] = "Темный фон";
$MESS["ram.watermark_PREVIEW_SETTINGS_RED"] = "Красный фон";
$MESS["ram.watermark_PREVIEW_SETTINGS_GREEN"] = "Зеленый фон";
$MESS["ram.watermark_PREVIEW_SETTINGS_BLUE"] = "Синий фон";

$MESS['ram.watermark_ID_TITLE'] = 'ID';
$MESS['ram.watermark_ID_HINT'] = '';
$MESS['ram.watermark_ID_MEASURE'] = '';

$MESS['ram.watermark_ACTIVE_TITLE'] = 'Активность';
$MESS['ram.watermark_ACTIVE_HINT'] = '';
$MESS['ram.watermark_ACTIVE_MEASURE'] = '';

$MESS['ram.watermark_NAME_TITLE'] = 'Название';
$MESS['ram.watermark_NAME_HINT'] = '';
$MESS['ram.watermark_NAME_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_POSITION_TITLE'] = 'Позиция';
$MESS['ram.watermark_PARAMS_POSITION_HINT'] = 'Расположение знака на картинке. Для максимальной защиты, рекомендуется размещать знак в произвольном месте.';
$MESS['ram.watermark_PARAMS_POSITION_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_POSITION_tl'] = 'сверху слева';
$MESS['ram.watermark_PARAMS_POSITION_tc'] = 'сверху по центру';
$MESS['ram.watermark_PARAMS_POSITION_tr'] = 'сверху справа';
$MESS['ram.watermark_PARAMS_POSITION_ml'] = 'слева';
$MESS['ram.watermark_PARAMS_POSITION_mc'] = 'по центру';
$MESS['ram.watermark_PARAMS_POSITION_mr'] = 'справа';
$MESS['ram.watermark_PARAMS_POSITION_bl'] = 'снизу слева';
$MESS['ram.watermark_PARAMS_POSITION_bc'] = 'снизу по центру';
$MESS['ram.watermark_PARAMS_POSITION_br'] = 'снизу справа';
$MESS['ram.watermark_PARAMS_POSITION_all'] = 'замостить';
$MESS['ram.watermark_PARAMS_POSITION_shift_v'] = 'со сдвигом по вертикали';
$MESS['ram.watermark_PARAMS_POSITION_shift_h'] = 'со сдвигом по горизонтали';
$MESS['ram.watermark_PARAMS_POSITION_random'] = 'в произвольном месте';

$MESS['ram.watermark_PARAMS_TRANSPARENT_TITLE'] = 'Прозрачность';
$MESS['ram.watermark_PARAMS_TRANSPARENT_HINT'] = '';
$MESS['ram.watermark_PARAMS_TRANSPARENT_MEASURE'] = '%';

$MESS['ram.watermark_PARAMS_ROTATE_TITLE'] = 'Поворот';
$MESS['ram.watermark_PARAMS_ROTATE_HINT'] = '';
$MESS['ram.watermark_PARAMS_ROTATE_MEASURE'] = '&deg;';

$MESS['ram.watermark_PARAMS_SCALE_TITLE'] = 'Масштабирование';
$MESS['ram.watermark_PARAMS_SCALE_HINT'] = 'Водяной знак автоматически подстраивается под размеры картинки. При нулевом значении, знак не масштабируется и накладывается как есть, в зависимости от исходных размеров знака или шрифта.';
$MESS['ram.watermark_PARAMS_SCALE_MEASURE'] = '%';

$MESS['ram.watermark_PARAMS_MARGIN_TOP_TITLE'] = 'Отступ сверху';
$MESS['ram.watermark_PARAMS_MARGIN_TOP_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_TOP_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MARGIN_RIGHT_TITLE'] = 'Отступ справа';
$MESS['ram.watermark_PARAMS_MARGIN_RIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_RIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MARGIN_BOTTOM_TITLE'] = 'Отступ снизу';
$MESS['ram.watermark_PARAMS_MARGIN_BOTTOM_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_BOTTOM_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MARGIN_LEFT_TITLE'] = 'Отступ слева';
$MESS['ram.watermark_PARAMS_MARGIN_LEFT_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_LEFT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_TYPE_TITLE'] = 'Тип';
$MESS['ram.watermark_PARAMS_TYPE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TYPE_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_TYPE_text'] = 'текст';
$MESS['ram.watermark_PARAMS_TYPE_image'] = 'изображение';

$MESS['ram.watermark_PARAMS_TEXT_TITLE'] = 'Текст';
$MESS['ram.watermark_PARAMS_TEXT_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_MEASURE'] = '';
$MESS["ram.watermark_PARAMS_TEXT_DEFAULT"] = "Защита от
копирования";

$MESS['ram.watermark_PARAMS_TEXT_COLOR_TITLE'] = 'Цвет текста';
$MESS['ram.watermark_PARAMS_TEXT_COLOR_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_COLOR_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_FONT_TITLE'] = 'Шрифт';
$MESS['ram.watermark_PARAMS_TEXT_FONT_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_FONT_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_SIZE_TITLE'] = 'Размер шрифта';
$MESS['ram.watermark_PARAMS_TEXT_SIZE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_SIZE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_ALIGN_TITLE'] = 'Выравнивание текста';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_left'] = 'по левому краю';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_center'] = 'по центру';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_right'] = 'по правому краю';

$MESS['ram.watermark_PARAMS_TEXT_LEADING_TITLE'] = 'Междустрочный интервал';
$MESS['ram.watermark_PARAMS_TEXT_LEADING_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_LEADING_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_STROKE_TITLE'] = 'Обводка';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_STROKE_COLOR_TITLE'] = 'Цвет обводки';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_COLOR_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_COLOR_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_STROKE_SIZE_TITLE'] = 'Толщина обводки';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_SIZE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_SIZE_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_IMAGE_TITLE'] = 'Изображение';
$MESS['ram.watermark_PARAMS_IMAGE_HINT'] = '';
$MESS['ram.watermark_PARAMS_IMAGE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_REDUCE_SIZE_TITLE'] = 'Уменьшать размеры изображений';
$MESS['ram.watermark_PARAMS_REDUCE_SIZE_HINT'] = 'Все изображения пропорционально уменьшаются до указанных значений.';
$MESS['ram.watermark_PARAMS_REDUCE_SIZE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_MAX_WIDTH_TITLE'] = 'Максимальная ширина';
$MESS['ram.watermark_PARAMS_MAX_WIDTH_HINT'] = '';
$MESS['ram.watermark_PARAMS_MAX_WIDTH_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MAX_HEIGHT_TITLE'] = 'Максимальная высота';
$MESS['ram.watermark_PARAMS_MAX_HEIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_MAX_HEIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_JPEG_QUALITY_TITLE'] = 'Качество изображений';
$MESS['ram.watermark_PARAMS_JPEG_QUALITY_HINT'] = '';
$MESS['ram.watermark_PARAMS_JPEG_QUALITY_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_SITE_TITLE'] = 'Сайт';
$MESS['ram.watermark_PARAMS_LIMIT_SITE_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_SITES_TITLE'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITES_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITES_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITES_ADMIN'] = 'Административный раздел';

$MESS['ram.watermark_PARAMS_LIMIT_TYPE_TITLE'] = 'Тип файла';
$MESS['ram.watermark_PARAMS_LIMIT_TYPE_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_TYPE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_JPG_TITLE'] = 'Обрабатывать JPG';
$MESS['ram.watermark_PARAMS_LIMIT_JPG_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_JPG_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_PNG_TITLE'] = 'Обрабатывать PNG';
$MESS['ram.watermark_PARAMS_LIMIT_PNG_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_PNG_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_GIF_TITLE'] = 'Обрабатывать GIF';
$MESS['ram.watermark_PARAMS_LIMIT_GIF_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_GIF_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_BMP_TITLE'] = 'Обрабатывать BMP';
$MESS['ram.watermark_PARAMS_LIMIT_BMP_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_BMP_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_WEBP_TITLE'] = 'Обрабатывать WEBP';
$MESS['ram.watermark_PARAMS_LIMIT_WEBP_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_WEBP_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_SIZES_TITLE'] = 'Размеры изображения';
$MESS['ram.watermark_PARAMS_LIMIT_SIZES_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SIZES_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_MIN_WIDTH_TITLE'] = 'Минимальная ширина';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_WIDTH_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_WIDTH_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_MAX_WIDTH_TITLE'] = 'Максимальная ширина';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_WIDTH_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_WIDTH_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_MIN_HEIGHT_TITLE'] = 'Минимальная высота';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_HEIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_HEIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_MAX_HEIGHT_TITLE'] = 'Максимальная высота';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_HEIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_HEIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_DATE_TITLE'] = 'Дата создания';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_DATE_FROM_TITLE'] = 'с';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_FROM_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_FROM_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_DATE_TO_TITLE'] = 'по';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_TO_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_TO_MEASURE'] = '';

$MESS['ram.watermark_JPEG_QUALITY_TITLE'] = 'Качество изображений';
$MESS['ram.watermark_JPEG_QUALITY_HINT'] = 'Снижение веса изображений за счет потери качества. Оптимальное значение - 85%.';
$MESS['ram.watermark_JPEG_QUALITY_MEASURE'] = '%';

$MESS['ram.watermark_EMPTY_SELECT'] = 'Не выбран объект (отсутствуют поля для обработки)';
$MESS['ram.watermark_INCLUDE'] = 'Объекты';
$MESS['ram.watermark_ADD_INCLUDE'] = 'Добавить объект';
$MESS['ram.watermark_EXCLUDE'] = 'Исключения';
$MESS['ram.watermark_ADD_EXCLUDE'] = 'Добавить исключение';
$MESS['ram.watermark_LIMITS'] = 'Дополнительные ограничения';
$MESS['ram.watermark_LIMIT_SITE_DESCRIPTION'] = 'Водяной знак будет наложен только на указанных сайтах.';
$MESS['ram.watermark_LIMIT_TYPE_DESCRIPTION'] = 'Будут обработаны только отмеченные типы файлов.';
$MESS['ram.watermark_LIMIT_SIZES_DESCRIPTION'] = 'Изображения, чьи размеры подходят под указанные значения, будут обработаны. С помощью данной настройки можно ограничить обработку миниатюр.';
$MESS['ram.watermark_LIMIT_DATE_DESCRIPTION'] = 'Дата загрузки изображения. Если дата попадает в указанный период, то изображение будет обработано.';

$MESS["ram.watermark_SAVE"] = "Сохранить";
$MESS["ram.watermark_APPLY"] = "Применить";
$MESS["ram.watermark_CANCEL"] = "Отменить";
$MESS["ram.watermark_CONFIRM_SAVE_NO_FILTERS"] = "Вы не выбрали ни одного объекта для обработки. Сохранить в любом случае?";
?>