<?
$MESS["ram.watermark_TITLE"] = "Массовая обработка изображений";
$MESS["ram.watermark_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["ram.watermark_URLS_PROCESS"] = "Анализ страниц";
$MESS["ram.watermark_FILES_PROCESS"] = "Обработка изображений";
$MESS["ram.watermark_TABS_params"] = "Настройки";
$MESS["ram.watermark_TABS_params_title"] = "Настройки обработчика";
$MESS["ram.watermark_PARAMS_SECTIONS"] = "Разделы сайта, включая подразделы (каждый раздел с новой строки)";
$MESS["ram.watermark_PARAMS_SECTIONS_EXCLUDE"] = "Исключения (каждое исключение с новой строки)";
$MESS["ram.watermark_PARAMS_CLEAR_CACHE"] = "Удалить все ранее обработанные изображения";
$MESS["ram.watermark_START"] = "Начать";
$MESS["ram.watermark_START_TITLE"] = "Запустить поиск и обработку изображений";
$MESS["ram.watermark_PROCESS_INFO"] = "Пошаговый анализ разделов сайта, поиск и защита изображений. Обработчик полезен в случаях, когда при нанесении на лету страницы загружаются очень медленно или не загружаются вовсе.";
?>