<?
$MESS["ram.watermark_MODULE_NAME"] = "Водяные знаки";
$MESS["ram.watermark_MODULE_DESC"] = "Защита изображений с помощью водяных знаков.";
$MESS["ram.watermark_PARTNER_NAME"] = "Мациевский Роман";
$MESS["ram.watermark_PARTNER_URI"] = "http://rommats.ru";
$MESS["ram.watermark_UNINSTALL_TITLE"] = "Удаление модуля \"Водяные знаки\"";
$MESS["ram.watermark_AJAX_ERROR"] = "При запросе произошла ошибка. Обновите страницу. Если ошибка повторяется, обратитесь к разработчику модуля: mail@rommats.ru";
?>