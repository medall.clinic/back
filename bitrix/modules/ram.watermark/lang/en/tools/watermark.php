<?
$MESS["ram.watermark_SUCCESS_UPLOAD"] = "Loading is complete.";
$MESS["ram.watermark_ERROR_UPLOAD"] = "An error has occurred";
$MESS["ram.watermark_ERROR_EXISTS"] = "File exists";
$MESS["ram.watermark_ERROR_TTF_TYPE"] = "Supported types: TTF";
$MESS["ram.watermark_ERROR_IMAGE_TYPE"] = "Supported types: JPG, PNG, GIF, BMP, WEBP";
$MESS["ram.watermark_FILES_PROCESS"] = "Files protection";
$MESS["ram.watermark_FILES_PROCESS_NONE"] = "Everything is protected";
$MESS["ram.watermark_FILES_PROCESS_FINISH"] = "Protection is complete";
$MESS["ram.watermark_FILES_SEARCH"] = "Searching files";
$MESS["ram.watermark_FILES_SEARCH_FINISH"] = "Search is complete";
$MESS["ram.watermark_LIST"] = "Watermarks";
$MESS["ram.watermark_V1_SET_CLEAR_STATUS"] = "Images status changing";
$MESS["ram.watermark_V1_CLEAR_IBLOCK_CACHE"] = "Clearing cache";
$MESS["ram.watermark_V1_CLEAR_FILES"] = "Removing watermarks. Images remaining: ";
$MESS["ram.watermark_V1_REMOVE_OLD_EVENTS"] = "Removing unused events";
$MESS["ram.watermark_V1_ADD_NEW_EVENTS"] = "Events addition";
$MESS["ram.watermark_V1_CONVERT_DATA"] = "Data convertion";
$MESS["ram.watermark_V1_GET_DATA_V1"] = "Collecting data";
$MESS["ram.watermark_V1_UPDATE_STRUCTURE_V1"] = "Database tables preparing";
$MESS['ram.watermark_EMPTY_SELECT'] = 'Select object (no fields for protection)';
$MESS['ram.watermark_DELETE_FILTER'] = 'Delete';
$MESS['ram.watermark_COPY_FILTER'] = 'Copy';
?>