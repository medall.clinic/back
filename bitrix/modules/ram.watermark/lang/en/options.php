<?
$MESS["ram.watermark_ACCESS_DENIED"] = "Access denied";
$MESS["ram.watermark_TAB_PARAMS"] = "Settings";
$MESS["ram.watermark_TAB_PARAMS_TITLE"] = "General parameters";
$MESS["ram.watermark_TAB_RIGHTS"] = "Access";
$MESS["ram.watermark_TAB_RIGHTS_TITLE"] = "Module access permissions";
$MESS["ram.watermark_PARAMS_INCLUDE_FILES"] = "List of URLs (pages) with <b>enabled</b> images protection. Each page on a new line.";
$MESS["ram.watermark_PARAMS_EXCLUDE_FILES"] = "List of URLs (pages) with <b>disabled</b> images protection. Each page on a new line.";
$MESS["ram.watermark_PARAMS_CONVERT_TO_WEBP"] = "Convert images to WEBP";
$MESS["ram.watermark_PARAMS_CONVERT_TO_WEBP_VERSION"] = "<span style='color:#f00;'>The version of the Main module must be at least 20.5.500. PHP version must be at least 5.4.0</span>";
$MESS["ram.watermark_PARAMS_PROTECT_TIME"] = "Protection time limit (sec)";
$MESS["ram.watermark_PARAMS_PROTECT_TIME_HIT"] = "If time is up, the remaining pictures will be protected on next page reload.";
$MESS["ram.watermark_PARAMS_SHOW_MODULES"] = "Display all modules in watermark's settings.";
?>