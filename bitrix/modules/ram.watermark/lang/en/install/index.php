<?
$MESS["ram.watermark_MODULE_NAME"] = "Watermarks";
$MESS["ram.watermark_MODULE_DESC"] = "Image protection and optimization.";
$MESS["ram.watermark_PARTNER_NAME"] = "Matsievskii Roman";
$MESS["ram.watermark_PARTNER_URI"] = "http://rommats.ru";
$MESS["ram.watermark_UNINSTALL_TITLE"] = "Deleting \"Watermarks\"";
$MESS["ram.watermark_AJAX_ERROR"] = "An error has occurred. Refresh this page. If the error repeats, send a letter to: mail@rommats.ru";
?>