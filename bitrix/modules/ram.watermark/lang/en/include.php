<?
$MESS["ram.watermark_WATERMARKS"] = "Watermarks";
$MESS["ram.watermark_FILES_SEARCH"] = "Searching files for processing";
$MESS["ram.watermark_FILES_PROCESS"] = "Files handling";


$MESS["ram.watermark_ALL_MODULES"] = "All modules";
$MESS["ram.watermark_MEDIALIB"] = "Media Library";
$MESS["ram.watermark_ALL_IBLOCKS"] = "All Information blocks";
$MESS["ram.watermark_ELEMENTS"] = "Elements";
$MESS["ram.watermark_SUBELEMENTS"] = "Elements (with subsections)";
$MESS["ram.watermark_SECTIONS"] = "Sections";
$MESS["ram.watermark_SECTION"] = "This section";
$MESS["ram.watermark_ALL_FORUMS"] = "All Forums";
$MESS["ram.watermark_ALL_HIGHLOADBLOCKS"] = "All Highload information blocks";
$MESS["ram.watermark_ALL_COLLECTIONS"] = "All collections";
$MESS["ram.watermark_SELECT"] = "Select";
$MESS["ram.watermark_SUBELEMENTS_COLLECTION"] = "Elements (with subcollections)";
$MESS["ram.watermark_ALL_IMAGES"] = "All images";
$MESS["ram.watermark_PREVIEW"] = "Preview image";
$MESS["ram.watermark_DETAIL"] = "Detailed image";
$MESS["ram.watermark_ALL_FILE_PROPS"] = "File properties";
$MESS["ram.watermark_DELETE_FILTER"] = "Delete";
$MESS["ram.watermark_COPY_FILTER"] = "Copy";

?>