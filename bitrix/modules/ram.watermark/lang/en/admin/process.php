<?
$MESS["ram.watermark_TITLE"] = "Mass protection";
$MESS["ram.watermark_ACCESS_DENIED"] = "Access denied";
$MESS["ram.watermark_URLS_PROCESS"] = "Pages analysis";
$MESS["ram.watermark_FILES_PROCESS"] = "Images protection";
$MESS["ram.watermark_TABS_params"] = "Settings";
$MESS["ram.watermark_TABS_params_title"] = "General parameters";
$MESS["ram.watermark_PARAMS_SECTIONS"] = "List of site sections (with subsections). Each section on a new line.";
$MESS["ram.watermark_PARAMS_SECTIONS_EXCLUDE"] = "List of url exclusions. Each url on a new line.";
$MESS["ram.watermark_PARAMS_CLEAR_CACHE"] = "Clear all images from watermarks";
$MESS["ram.watermark_START"] = "Start";
$MESS["ram.watermark_START_TITLE"] = "Start searching and protection";
$MESS["ram.watermark_PROCESS_INFO"] = "Step-by-step protection. Useful for sites with large quantity of image on each page.";
?>