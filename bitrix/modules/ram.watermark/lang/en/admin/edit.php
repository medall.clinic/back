<?
$MESS["ram.watermark_ACCESS_DENIED"] = "Access denied";
$MESS["ram.watermark_COPY"] = "Copy watermark";
$MESS["ram.watermark_EDIT"] = "Modify watermark";
$MESS["ram.watermark_ADD"] = "Add watermark";
$MESS["ram.watermark_TABS_params"] = "Settings";
$MESS["ram.watermark_TABS_params_title"] = "General parameters";
$MESS["ram.watermark_TABS_filter"] = "Objects and fields";
$MESS["ram.watermark_TABS_filter_title"] = "Objects and fields for protection";
$MESS["ram.watermark_ERROR_EMPTY"] = "Empty field";
$MESS["ram.watermark_NAME"] = "Name";
$MESS["ram.watermark_V1_CONVERT"] = "Module convertion from vertion 1.x.x to 2.x.x";
$MESS["ram.watermark_FILE_CACHE_NOTIFY"] = 'Set constant "CACHED_b_file" to "0" in "/bitrix/php_interface/dbconn.php" file.';
$MESS["ram.watermark_FILE_BUCKET_SIZE_NOTIFY"] = 'Set constant "CACHED_b_file_bucket_size" to "1" in "/bitrix/php_interface/dbconn.php" file.';

$MESS["ram.watermark_PREVIEW_SETTINGS_SCHEME"] = "Show/hide borders";
$MESS["ram.watermark_PREVIEW_SETTINGS_WHITE"] = "Light background";
$MESS["ram.watermark_PREVIEW_SETTINGS_BLACK"] = "Dark background";
$MESS["ram.watermark_PREVIEW_SETTINGS_RED"] = "Red background";
$MESS["ram.watermark_PREVIEW_SETTINGS_GREEN"] = "Green background";
$MESS["ram.watermark_PREVIEW_SETTINGS_BLUE"] = "Blue background";

$MESS['ram.watermark_ID_TITLE'] = 'ID';
$MESS['ram.watermark_ID_HINT'] = '';
$MESS['ram.watermark_ID_MEASURE'] = '';

$MESS['ram.watermark_ACTIVE_TITLE'] = 'Active';
$MESS['ram.watermark_ACTIVE_HINT'] = '';
$MESS['ram.watermark_ACTIVE_MEASURE'] = '';

$MESS['ram.watermark_NAME_TITLE'] = 'Name';
$MESS['ram.watermark_NAME_HINT'] = '';
$MESS['ram.watermark_NAME_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_POSITION_TITLE'] = 'Position';
$MESS['ram.watermark_PARAMS_POSITION_HINT'] = 'Watermark position. Use random position for best protection.';
$MESS['ram.watermark_PARAMS_POSITION_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_POSITION_tl'] = 'top left';
$MESS['ram.watermark_PARAMS_POSITION_tc'] = 'top center';
$MESS['ram.watermark_PARAMS_POSITION_tr'] = 'top right';
$MESS['ram.watermark_PARAMS_POSITION_ml'] = 'left';
$MESS['ram.watermark_PARAMS_POSITION_mc'] = 'center';
$MESS['ram.watermark_PARAMS_POSITION_mr'] = 'right';
$MESS['ram.watermark_PARAMS_POSITION_bl'] = 'bottom left';
$MESS['ram.watermark_PARAMS_POSITION_bc'] = 'bottom center';
$MESS['ram.watermark_PARAMS_POSITION_br'] = 'bottom right';
$MESS['ram.watermark_PARAMS_POSITION_all'] = 'tiled';
$MESS['ram.watermark_PARAMS_POSITION_shift_v'] = 'tiled with vertical offset';
$MESS['ram.watermark_PARAMS_POSITION_shift_h'] = 'tiled with horizontal offset';
$MESS['ram.watermark_PARAMS_POSITION_random'] = 'random';

$MESS['ram.watermark_PARAMS_TRANSPARENT_TITLE'] = 'Transparency';
$MESS['ram.watermark_PARAMS_TRANSPARENT_HINT'] = '';
$MESS['ram.watermark_PARAMS_TRANSPARENT_MEASURE'] = '%';

$MESS['ram.watermark_PARAMS_ROTATE_TITLE'] = 'Rotation';
$MESS['ram.watermark_PARAMS_ROTATE_HINT'] = '';
$MESS['ram.watermark_PARAMS_ROTATE_MEASURE'] = '&deg;';

$MESS['ram.watermark_PARAMS_SCALE_TITLE'] = 'Scaling';
$MESS['ram.watermark_PARAMS_SCALE_HINT'] = 'Watermark scaling depending on image dimentions. With zero value watermark saves its base size.';
$MESS['ram.watermark_PARAMS_SCALE_MEASURE'] = '%';

$MESS['ram.watermark_PARAMS_MARGIN_TOP_TITLE'] = 'Top offset';
$MESS['ram.watermark_PARAMS_MARGIN_TOP_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_TOP_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MARGIN_RIGHT_TITLE'] = 'Right offset';
$MESS['ram.watermark_PARAMS_MARGIN_RIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_RIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MARGIN_BOTTOM_TITLE'] = 'Bottom offset';
$MESS['ram.watermark_PARAMS_MARGIN_BOTTOM_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_BOTTOM_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MARGIN_LEFT_TITLE'] = 'Left offset';
$MESS['ram.watermark_PARAMS_MARGIN_LEFT_HINT'] = '';
$MESS['ram.watermark_PARAMS_MARGIN_LEFT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_TYPE_TITLE'] = 'Type';
$MESS['ram.watermark_PARAMS_TYPE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TYPE_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_TYPE_text'] = 'text';
$MESS['ram.watermark_PARAMS_TYPE_image'] = 'image';

$MESS['ram.watermark_PARAMS_TEXT_TITLE'] = 'Text';
$MESS['ram.watermark_PARAMS_TEXT_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_MEASURE'] = '';
$MESS["ram.watermark_PARAMS_TEXT_DEFAULT"] = "Watermark";

$MESS['ram.watermark_PARAMS_TEXT_COLOR_TITLE'] = 'Text color';
$MESS['ram.watermark_PARAMS_TEXT_COLOR_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_COLOR_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_FONT_TITLE'] = 'Font';
$MESS['ram.watermark_PARAMS_TEXT_FONT_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_FONT_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_SIZE_TITLE'] = 'Font size';
$MESS['ram.watermark_PARAMS_TEXT_SIZE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_SIZE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_ALIGN_TITLE'] = 'Alignment';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_left'] = 'left';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_center'] = 'center';
$MESS['ram.watermark_PARAMS_TEXT_ALIGN_right'] = 'right';

$MESS['ram.watermark_PARAMS_TEXT_LEADING_TITLE'] = 'Line spacing';
$MESS['ram.watermark_PARAMS_TEXT_LEADING_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_LEADING_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_STROKE_TITLE'] = 'Stroke';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_STROKE_COLOR_TITLE'] = 'Stroke color';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_COLOR_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_COLOR_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_TEXT_STROKE_SIZE_TITLE'] = 'Stroke size';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_SIZE_HINT'] = '';
$MESS['ram.watermark_PARAMS_TEXT_STROKE_SIZE_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_IMAGE_TITLE'] = 'Image';
$MESS['ram.watermark_PARAMS_IMAGE_HINT'] = '';
$MESS['ram.watermark_PARAMS_IMAGE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_REDUCE_SIZE_TITLE'] = 'Auto-resize large images';
$MESS['ram.watermark_PARAMS_REDUCE_SIZE_HINT'] = '';
$MESS['ram.watermark_PARAMS_REDUCE_SIZE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_MAX_WIDTH_TITLE'] = 'Max width';
$MESS['ram.watermark_PARAMS_MAX_WIDTH_HINT'] = '';
$MESS['ram.watermark_PARAMS_MAX_WIDTH_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_MAX_HEIGHT_TITLE'] = 'Max height';
$MESS['ram.watermark_PARAMS_MAX_HEIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_MAX_HEIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_JPEG_QUALITY_TITLE'] = 'Quality';
$MESS['ram.watermark_PARAMS_JPEG_QUALITY_HINT'] = '';
$MESS['ram.watermark_PARAMS_JPEG_QUALITY_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_SITE_TITLE'] = 'Site';
$MESS['ram.watermark_PARAMS_LIMIT_SITE_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_SITES_TITLE'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITES_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITES_MEASURE'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SITES_ADMIN'] = 'Administrative panel';

$MESS['ram.watermark_PARAMS_LIMIT_TYPE_TITLE'] = 'File type';
$MESS['ram.watermark_PARAMS_LIMIT_TYPE_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_TYPE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_JPG_TITLE'] = 'JPG';
$MESS['ram.watermark_PARAMS_LIMIT_JPG_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_JPG_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_PNG_TITLE'] = 'PNG';
$MESS['ram.watermark_PARAMS_LIMIT_PNG_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_PNG_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_GIF_TITLE'] = 'GIF';
$MESS['ram.watermark_PARAMS_LIMIT_GIF_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_GIF_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_BMP_TITLE'] = 'BMP';
$MESS['ram.watermark_PARAMS_LIMIT_BMP_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_BMP_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_WEBP_TITLE'] = 'WEBP';
$MESS['ram.watermark_PARAMS_LIMIT_WEBP_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_WEBP_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_SIZES_TITLE'] = 'Image dimensions';
$MESS['ram.watermark_PARAMS_LIMIT_SIZES_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_SIZES_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_MIN_WIDTH_TITLE'] = 'Min width';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_WIDTH_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_WIDTH_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_MAX_WIDTH_TITLE'] = 'Max width';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_WIDTH_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_WIDTH_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_MIN_HEIGHT_TITLE'] = 'Min height';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_HEIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MIN_HEIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_MAX_HEIGHT_TITLE'] = 'Max height';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_HEIGHT_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_MAX_HEIGHT_MEASURE'] = 'px';

$MESS['ram.watermark_PARAMS_LIMIT_DATE_TITLE'] = 'Date creation';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_DATE_FROM_TITLE'] = 'from';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_FROM_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_FROM_MEASURE'] = '';

$MESS['ram.watermark_PARAMS_LIMIT_DATE_TO_TITLE'] = 'to';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_TO_HINT'] = '';
$MESS['ram.watermark_PARAMS_LIMIT_DATE_TO_MEASURE'] = '';

$MESS['ram.watermark_JPEG_QUALITY_TITLE'] = 'JPEG quality';
$MESS['ram.watermark_JPEG_QUALITY_HINT'] = 'Reducing image weight with quality loss. Optimal value - 85%';
$MESS['ram.watermark_JPEG_QUALITY_MEASURE'] = '%';

$MESS['ram.watermark_EMPTY_SELECT'] = 'Select object (no fields for protection)';
$MESS['ram.watermark_INCLUDE'] = 'Included objects';
$MESS['ram.watermark_ADD_INCLUDE'] = 'Add inclution';
$MESS['ram.watermark_EXCLUDE'] = 'Excluded objects';
$MESS['ram.watermark_ADD_EXCLUDE'] = 'Add exclusion';
$MESS['ram.watermark_LIMITS'] = 'Additional limits';
$MESS['ram.watermark_LIMIT_SITE_DESCRIPTION'] = 'Protect images on selected sites only.';
$MESS['ram.watermark_LIMIT_TYPE_DESCRIPTION'] = 'Protect selected file types only.';
$MESS['ram.watermark_LIMIT_SIZES_DESCRIPTION'] = 'Protect images of specified sizes.';
$MESS['ram.watermark_LIMIT_DATE_DESCRIPTION'] = 'Date creation of image.';

$MESS["ram.watermark_SAVE"] = "Save";
$MESS["ram.watermark_CANCEL"] = "Cancel";
$MESS["ram.watermark_CONFIRM_SAVE_NO_FILTERS"] = "No one object is included. Save anyway?";
?>