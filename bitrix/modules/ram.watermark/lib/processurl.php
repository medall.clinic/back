<?php
namespace Ram\Watermark;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ProcessurlTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> URL string mandatory
 * <li> VISITED bool optional default 'N'
 * </ul>
 *
 * @package Ram\Watermark
 **/

class ProcessurlTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'ram_watermark_processurl';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('PROCESSURL_ENTITY_ID_FIELD'),
			),
			'URL' => array(
				'data_type' => 'text',
				'required' => true,
				'title' => Loc::getMessage('PROCESSURL_ENTITY_URL_FIELD'),
			),
			'VISITED' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('PROCESSURL_ENTITY_VISITED_FIELD'),
			),
		);
	}
}
?>