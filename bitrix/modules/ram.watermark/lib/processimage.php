<?php
namespace Ram\Watermark;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ProcessimageTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> IMAGEID int mandatory
 * <li> PARAMS string optional
 * </ul>
 *
 * @package Ram\Watermark
 **/

class ProcessimageTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'ram_watermark_processimage';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('PROCESSIMAGE_ENTITY_ID_FIELD'),
			),
			'IMAGEID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('PROCESSIMAGE_ENTITY_IMAGEID_FIELD'),
			),
			'PARAMS' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('PROCESSIMAGE_ENTITY_PARAMS_FIELD'),
			),
		);
	}
}
?>