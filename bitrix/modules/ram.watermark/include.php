<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CModule::AddAutoloadClasses("ram.watermark", Array("CRamWatermarkImage" => "/classes/image.php"));

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/update_client_partner.php");

class CRamWatermark 
{
	public static $cache;
	
	public static function GetCache()
	{
		return \CRamWatermark::$cache;
	}
	
	public static function CleanClearImages()
	{
		$images = \Ram\Watermark\ImageTable::getList(Array("filter" => Array("HASH" => "clear")))->fetchAll();
		
		foreach ($images as $image)
		{
			\Ram\Watermark\ImageTable::update($image["ID"], ["HASH" => ""]);
		}
	}
	
	public static function LoadClearImages()
	{
		if (!isset(\CRamWatermark::$cache["CLEAR_IMAGES"]))
		{
			\CRamWatermark::$cache["CLEAR_IMAGES"] = [];
			
			$images = \Ram\Watermark\ImageTable::getList(Array("filter" => Array("HASH" => "clear")))->fetchAll();
			
			foreach ($images as $image)
			{
				\CRamWatermark::$cache["CLEAR_IMAGES"][] = $image["IMAGEID"]."_".$image["TAG"];
			}
		}
	}
	
	public static function OnBeforeResizeImage($arFile, $arParams, &$callbackData, &$bNeedResize, &$sourceImageFile, &$cacheImageFileTmp)
	{
		\CRamWatermark::LoadClearImages();
		
		\CRamWatermark::initTimeLimit();
		
		$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		
		\CRamWatermark::LoadLimitations();
		
		if (!isset(\CRamWatermark::$cache["SITE_ID"]))
		{
			$context = Bitrix\Main\Context::getCurrent();
			\CRamWatermark::$cache["SITE_ID"] = $context->getSite();
			if (!strlen(\CRamWatermark::$cache["SITE_ID"])) \CRamWatermark::$cache["SITE_ID"] = "admin";
		}
		
		if ($request["ramwmprocess"] === "Y" && !in_array($request->getRequestedPage(), \CRamWatermark::$cache['EXCLUDE_FILES']))
		{
			$params = serialize(Array('SITE_ID' => \CRamWatermark::$cache["SITE_ID"], 'RESIZE' => Array('width' => $arParams[0]['width'], 'height' => $arParams[0]['height'], 'resizeType' => $arParams[1], 'cacheImageFileTmp' => $cacheImageFileTmp)));
			
			if (!\Ram\Watermark\ProcessimageTable::getRow(Array("filter" => Array("IMAGEID" => $arFile["ID"], "PARAMS" => $params))))
			{
				\Ram\Watermark\ProcessimageTable::add(Array("IMAGEID" => $arFile["ID"], "PARAMS" => $params));
			}
		}
		else if (($request->isAdminSection() && in_array($request->getRequestedPage(), \CRamWatermark::$cache['INCLUDE_FILES'])) || (!$request->isAdminSection() && !in_array($request->getRequestedPage(), \CRamWatermark::$cache['EXCLUDE_FILES'])))
		{
			if ($request->isAdminSection())
			{
				\CRamWatermark::$cache["PROTECT_TIME_LIMIT"] = 99999;
			}
			
			if (\CRamWatermark::$cache["TOTAL_TIME"] < \CRamWatermark::$cache["PROTECT_TIME_LIMIT"])
			{
				$starttime = microtime(true);
			
				$res = \CRamWatermark::checkFile($arFile, \CRamWatermark::$cache["SITE_ID"], Array('width' => $arParams[0]['width'], 'height' => $arParams[0]['height'], 'resizeType' => $arParams[1]));
				if ($res)
				{
					if ($res['image']['MODULE'] === 'iblock')
					{
						\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"][$res['image']['ENTITY']] = $res['image']['ENTITY'];
					}
					
					if (file_exists($cacheImageFileTmp))
					{
						unlink($cacheImageFileTmp);
					}
					$cacheImageFileTmp = $_SERVER["DOCUMENT_ROOT"].$res['src'];
					$sourceImageFile = $res['source'];
					
					if ($res['image']['NEW'] === 'Y' || $res['image']['EXISTS'] === 'N')
					{
						\CRamWatermark::$cache["TOTAL_TIME"] += (microtime(true) - $starttime);
					}
					
					return true;
				}
				else
				{
					\CRamWatermark::$cache["TOTAL_TIME"] += (microtime(true) - $starttime);
					
					return false;
				}
			}
			else
			{
				\CRamWatermark::$cache["PROTECT_TIME_CLEAR"] = "Y";
				
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public static function OnAfterResizeImage($arFile, $arParams, &$callbackData, &$cacheImageFile, &$cacheImageFileTmp, &$arImageSize)
	{
		//
	}
	
	public static function OnAfterFileSave($arFields)
	{
		//
	}
	
	public static function initTimeLimit()
	{
		if (!isset(\CRamWatermark::$cache["TOTAL_TIME"]))
		{
			\CRamWatermark::$cache["TOTAL_TIME"] = 0;
		}
		
		if (!isset(\CRamWatermark::$cache["PROTECT_TIME_LIMIT"]))
		{
			\CRamWatermark::$cache["PROTECT_TIME_LIMIT"] = \Bitrix\Main\Config\Option::get("ram.watermark", "protect_time", "1");
		}
		
		if (!isset(\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"]))
		{
			\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"] = Array();
		}
		
		if (!isset(\CRamWatermark::$cache["PROTECT_TIME_CLEAR"]))
		{
			\CRamWatermark::$cache["PROTECT_TIME_CLEAR"] = "N";
		}
	}
	
	public static function OnGetFileSRC($arFile)
	{
		\CRamWatermark::LoadClearImages();
		
		\CRamWatermark::initTimeLimit();
		
		$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		
		\CRamWatermark::LoadLimitations();
		
		if (!isset(\CRamWatermark::$cache["SITE_ID"]))
		{
			$context = Bitrix\Main\Context::getCurrent();
			\CRamWatermark::$cache["SITE_ID"] = $context->getSite();
			if (!strlen(\CRamWatermark::$cache["SITE_ID"])) \CRamWatermark::$cache["SITE_ID"] = "admin";
		}
		
		if ($request["ramwmprocess"] === "Y" && !in_array($request->getRequestedPage(), \CRamWatermark::$cache['EXCLUDE_FILES']))
		{
			$params = serialize(Array('SITE_ID' => \CRamWatermark::$cache["SITE_ID"]));
			
			if (!\Ram\Watermark\ProcessimageTable::getRow(Array("filter" => Array("IMAGEID" => $arFile["ID"], "PARAMS" => $params))))
			{
				\Ram\Watermark\ProcessimageTable::add(Array("IMAGEID" => $arFile["ID"], "PARAMS" => $params));
				
				\CRamWatermark::clearResizeCache($arFile);
			}
		}
		else if (($request->isAdminSection() && in_array($request->getRequestedPage(), \CRamWatermark::$cache['INCLUDE_FILES'])) || (!$request->isAdminSection() && !in_array($request->getRequestedPage(), \CRamWatermark::$cache['EXCLUDE_FILES'])))
		{
			if ($request->isAdminSection())
			{
				\CRamWatermark::$cache["PROTECT_TIME_LIMIT"] = 99999;
			}
			
			$objectCache = 'FILESRC__'.$arFile['ID'];
		
			if (!isset(\CRamWatermark::$cache[$objectCache]))
			{
				if (\CRamWatermark::$cache["TOTAL_TIME"] < \CRamWatermark::$cache["PROTECT_TIME_LIMIT"])
				{
					$starttime = microtime(true);
					
					$res = \CRamWatermark::checkFile($arFile, \CRamWatermark::$cache["SITE_ID"], null);
					if ($res)
					{
						if ($res['image']['MODULE'] === 'iblock')
						{
							\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"][$res['image']['ENTITY']] = $res['image']['ENTITY'];
						}
						
						if ($res['clearResizeCache'])
						{
							\CRamWatermark::clearResizeCache($arFile);
						}
						
						\CRamWatermark::$cache[$objectCache] = $res['src'];
						
						if ($res['image']['NEW'] === 'Y' || $res['image']['EXISTS'] === 'N')
						{
							\CRamWatermark::$cache["TOTAL_TIME"] += (microtime(true) - $starttime);
						}
						
						return $res['src'];
					}
					else
					{
						\CRamWatermark::$cache[$objectCache] = false;
						
						\CRamWatermark::$cache["TOTAL_TIME"] += (microtime(true) - $starttime);
					}
				}
				else
				{
					\CRamWatermark::$cache["PROTECT_TIME_CLEAR"] = "Y";
				}
			}
			else
			{
				return \CRamWatermark::$cache[$objectCache];
			}
		}
	}
	
	public static function RelToAbsURL($rel, $base)
	{
		$base_data = parse_url($base);
		if (strpos($rel, "//") === 0)
		{
			return $base_data["scheme"].":".$rel;
		}
		if (parse_url($rel, PHP_URL_SCHEME) != "" )
		{
			return $rel;
		}
		if ($rel[0] == "#" || $rel[0] == "?")
		{
			return $base . $rel;
		}
		$base_data["path"] = preg_replace("#/[^/]*$#", "", $base_data["path"]);
		if ($rel[0] == "/")
		{
			$base_data["path"] = "";
		}
		$abs = $base_data["host"].$base_data["path"]."/".$rel;
		$abs = preg_replace("/(\/\.?\/)/", "/", $abs);
		$abs = preg_replace("/\/(?!\.\.)[^\/]+\/\.\.\//", "/", $abs);
		return $base_data["scheme"]."://".$abs;
	}
	
	private static function LoadLimitations()
	{
		if (!isset(\CRamWatermark::$cache['INCLUDE_FILES']))
		{
			$includeFiles = \Bitrix\Main\Config\Option::get("ram.watermark", "include_files", "");
			$includeFiles = explode("\n", $includeFiles);
			foreach ($includeFiles as $k => $v)
			{
				$includeFiles[$k] = trim($v);
			}
			\CRamWatermark::$cache['INCLUDE_FILES'] = $includeFiles;
		}
		
		if (!isset(\CRamWatermark::$cache['EXCLUDE_FILES']))
		{
			$excludeFiles = \Bitrix\Main\Config\Option::get("ram.watermark", "exclude_files", "");
			$excludeFiles = explode("\n", $excludeFiles);
			foreach ($excludeFiles as $k => $v)
			{
				$excludeFiles[$k] = trim($v);
			}
			\CRamWatermark::$cache['EXCLUDE_FILES'] = $excludeFiles;
		}
	}
	
	public static function GetUploadDir()
	{
		if (!isset(\CRamWatermark::$cache["UPLOAD_DIR"]))
		{
			\CRamWatermark::$cache["UPLOAD_DIR"] = \Bitrix\Main\Config\Option::get('main', 'upload_dir', 'upload');
		}
		
		return \CRamWatermark::$cache["UPLOAD_DIR"];
	}
	
	public static function OnEndBufferContent(&$content)
	{
		if (isset(\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"]) && !empty(\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"]) && \CRamWatermark::$cache["PROTECT_TIME_CLEAR"] === "Y")
		{
			$cacheManager = \Bitrix\Main\Application::getInstance()->getTaggedCache();
			
			foreach (\CRamWatermark::$cache["PROTECT_TIME_CLEAR_IBLOCKS"] as $iblockId)
			{
				$cacheManager->clearByTag('iblock_id_'.$iblockId);
			}
		}
		
		$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		
		\CRamWatermark::LoadLimitations();
		
		if ($request["ramwmprocess"] === "Y")
		{
			$base = (strtoupper($_SERVER["HTTPS"])==="ON"?"https":"http")."://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			
			if (!in_array($request->getRequestedPage(), \CRamWatermark::$cache['EXCLUDE_FILES']))
			{
				preg_match_all('/<a.*?href=["\']([^"\']+)["\']/i', $content, $hrefs);
				
				if (!empty($hrefs[1]))
				{
					foreach ($hrefs[1] as $k => $url)
					{
						if (substr_count($hrefs[1][$k], "logout") || substr_count($hrefs[1][$k], "bitrix_include_areas") || substr_count($hrefs[1][$k], "show_page_exec_time") || substr_count($hrefs[1][$k], "add_new_site_sol"))
						{
							unset($hrefs[1][$k]);
						}
						else
						{
							$hrefs[1][$k] = \CRamWatermark::RelToAbsURL($url, $base);
							
							$uri = new Bitrix\Main\Web\Uri(htmlspecialchars_decode($hrefs[1][$k]));
							$uri->deleteParams(Array("ramwmprocess", "clear_cache", "ramwminclude", "ramwmexclude"));
							$hrefs[1][$k] = $uri->getUri();
							
							if (substr_count($hrefs[1][$k], "#"))
							{
								$hrefs[1][$k] = explode("#", $hrefs[1][$k]);
								$hrefs[1][$k] = $hrefs[1][$k][0];
							}
							
							$check = false;
							
							foreach ($request["ramwminclude"] as $checkLink)
							{
								if (substr_count($hrefs[1][$k], base64_decode($checkLink)))
								{
									$check = true;
									break;
								}
							}
							
							if ($check)
							{
								foreach ($request["ramwmexclude"] as $checkLink)
								{
									if (substr_count($hrefs[1][$k], base64_decode($checkLink)))
									{
										$check = false;
										break;
									}
								}
							}
							
							if (!$check)
							{
								unset($hrefs[1][$k]);
							}
						}
					}
					
					$hrefs[1] = array_unique($hrefs[1]);
					
					foreach ($hrefs[1] as $url)
					{
						if (!\Ram\Watermark\ProcessurlTable::getRow(Array("filter" => Array("URL" => $url))))
						{
							\Ram\Watermark\ProcessurlTable::add(Array("URL" => $url, "VISITED" => false));
						}
					}
				}
				
				$content = "";
			}
			
			$curUri = new Bitrix\Main\Web\Uri(\CRamWatermark::RelToAbsURL($request->getRequestUri(), $base));
			$curUri->deleteParams(Array("ramwmprocess", "clear_cache", "ramwminclude", "ramwmexclude"));
			
			$curUrlRow = \Ram\Watermark\ProcessurlTable::getRow(Array("filter" => Array("URL" => $curUri->getUri())));
			if ($curUrlRow)
			{
				\Ram\Watermark\ProcessurlTable::update($curUrlRow["ID"], Array("VISITED" => true));
			}
			else
			{
				\Ram\Watermark\ProcessurlTable::add(Array("URL" => $curUri->getUri(), "VISITED" => true));
			}
		}
		else if (($request->isAdminSection() && in_array($request->getRequestedPage(), \CRamWatermark::$cache['INCLUDE_FILES'])) || (!$request->isAdminSection() && !in_array($request->getRequestedPage(), \CRamWatermark::$cache['EXCLUDE_FILES'])))
		{
			$filemanFilter = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM ram_watermark_filter as f, ram_watermark_mark as m WHERE f.MODULE="fileman" AND f.WMID = m.ID AND m.ACTIVE = "Y";')->fetch();
			
			if (!empty($filemanFilter))
			{
				$uploadDir = \CRamWatermark::GetUploadDir();
				
				$moduleFolder = '/'.$uploadDir.'/medialibrary/';
				
				preg_match_all('/<img(.*?)src=("|\')(.*?)("|\')(.*?)>/s', $content, $pregResult);
				
				$images = Array();
				
				foreach ($pregResult[3] as $image)
				{
					if (substr($image, 0, strlen($moduleFolder)) === $moduleFolder)
					{
						$imageSplit = explode('/', substr($image, 1));
						
						$images[$image] = $imageSplit;
					}
				}
				
				if (!empty($images))
				{
					$filter = Array("LOGIC" => "OR");
					
					foreach ($images as $image)
					{
						$subdir = implode("/", array_slice($image, 1, -1));
						
						$filter[] = Array("SUBDIR" => $subdir, "FILE_NAME" => urldecode(str_replace("%20", " ", $image[count($image) - 1])));
					}
					
					$dbImages = Bitrix\Main\FileTable::getList(Array("filter" => $filter))->fetchAll();
					
					foreach ($dbImages as $image)
					{
						$imagePath = "/".$uploadDir."/".$image["SUBDIR"]."/".$image["FILE_NAME"];
						
						if (!isset($images[$imagePath]))
						{
							$imagePath = str_replace(" ", "%20", $imagePath);
						}							
						
						$images[$imagePath] = CFile::GetPath($image["ID"]);
					}
					
					foreach ($images as $imageFrom => $imageTo)
					{
						$content = str_replace($imageFrom, $imageTo, $content);
					}
				}
			}
		}
	}
	
	public static function OnFileDelete($arFields)
	{
		$files = \Ram\Watermark\ImageTable::getList(Array('filter' => Array('IMAGEID' => $arFields['ID'])))->fetchAll();
		
		foreach ($files as $file)
		{
			$uploadDir = \CRamWatermark::GetUploadDir();
			$fileIdHash = md5($file['ID']);
			$protectedDir = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/resize_cache/ram.watermark/'.substr($fileIdHash, 0, 3).'/'.substr($fileIdHash, 3, 3).'/'.substr($fileIdHash, 6, 3).'/'.$file['ID'].'/';
			
			\Ram\Watermark\ImageTable::delete($file['ID']);
		
			if (file_exists($protectedDir))
			{
				\Bitrix\Main\IO\Directory::deleteDirectory($protectedDir);
			}
		}
	}
	
	public static function protectImage($params)
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$saveOriginalFileName = \Bitrix\Main\Config\Option::get('main', 'save_original_file_name', 'N');
		$fileIdHash = md5($params['file']['ID']);
		
		$protectedDir = '/'.$uploadDir.'/resize_cache/ram.watermark/'.substr($fileIdHash, 0, 3).'/'.substr($fileIdHash, 3, 3).'/'.substr($fileIdHash, 6, 3).'/'.$params['file']['ID'].'/';
		if (isset($params['resize']))
		{
			$protectedDir .= $params['resize']['width'].'_'.$params['resize']['height'].'_'.$params['resize']['resizeType'].'/';
		}
		
		$protectedFile = '';
		$sourceFile = pathinfo($params['arFile']['FILE_NAME']);
		
		$extension = $sourceFile['extension'];
		
		$sourceFilename = implode(".", array_slice(explode(".", $params['arFile']['FILE_NAME']), 0, -1));
		
		$convertToWebp = \Bitrix\Main\Config\Option::get("ram.watermark", "convert_to_webp", "N");
		
		if ($convertToWebp === "Y")
		{
			$extension = "webp";
		}
		
		if ($saveOriginalFileName === "Y")
		{
			$protectedFile = $sourceFilename.".".$extension;
		}
		else
		{
			$protectedFile = md5($sourceFilename).".".$extension;
		}
		
		$watermarksParams = Array();
		foreach ($params['watermarks'] as $watermark)
		{
			foreach ($watermark['PARAMS'] as $code => $value)
			{
				if (substr($code, 0, 6) === 'LIMIT_')
				{
					unset($watermark['PARAMS'][$code]);
				}
			}
			$watermarksParams[] = $watermark['PARAMS'];
		}
		$watermarksHash = md5(serialize($watermarksParams));
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$protectedDir.$protectedFile))
		{
			if ($params['file']['HASH'] === $watermarksHash)
			{
				$image = new \CRamWatermarkImage();
				$image->from($_SERVER['DOCUMENT_ROOT'].$protectedDir.$protectedFile);
				$return = Array
				(
					'src' => $protectedDir.$protectedFile,
					'width' => $image->getWidth(),
					'height' => $image->getHeight(),
					"EXISTS" => "Y",
				);
				
				$image->destroy();
				unset($image);
				
				return $return;
			}
			else
			{
				unlink($_SERVER['DOCUMENT_ROOT'].$protectedDir.$protectedFile);
			}
		}
		
		if ($params['arFile']['HANDLER_ID'])
		{
			$ob = new CCloudStorageBucket(intval($params['arFile']['HANDLER_ID']));
			$ob->Init();
			$ob->DownloadToFile($params['arFile'], $params['source']);
		}
		
		$image = new \CRamWatermarkImage();
		$image->from($params['source']);
		if (isset($params['resize']))
		{
			$image->resize($params['resize']);
		}
		else
		{
			$minWidth = $image->getWidth();
			$minHeight = $image->getHeight();
			foreach ($params['watermarks'] as $watermark)
			{
				if ($watermark['PARAMS']['REDUCE_SIZE'] === 'Y')
				{
					if ($minWidth == null || $minWidth > $watermark['PARAMS']['MAX_WIDTH']) $minWidth = $watermark['PARAMS']['MAX_WIDTH'];
					if ($minHeight == null || $minHeight > $watermark['PARAMS']['MAX_HEIGHT']) $minHeight = $watermark['PARAMS']['MAX_HEIGHT'];
				}
			}
			
			$image->resize(Array('resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL, 'width' => $minWidth, 'height' => $minHeight));
		}
		$minJPEGQuality = 100;
		foreach ($params['watermarks'] as $w => $watermark)
		{
			$watermarkCacheDir = '/'.$uploadDir.'/ram.watermark/images/cache/'.$watermark['ID'].'/';
			$watermarkCacheFile = $watermark['ID'].'.png';
			$wm = new \CRamWatermarkImage();
			if (!file_exists($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir.$watermarkCacheFile))
			{
				\CRamWatermark::createCache($watermark['ID'], $watermark['PARAMS']);
			}
			$wm->from($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir.$watermarkCacheFile);			
			$image->append($wm, $watermark['PARAMS'], $_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir.$watermark['ID']);
			
			if ($watermark['PARAMS']['JPEG_QUALITY'] < $minJPEGQuality)
			{
				$minJPEGQuality = $watermark['PARAMS']['JPEG_QUALITY'];
			}
			$wm->destroy();
			unset($wm);
		}
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].$protectedDir))
		{
			mkdir($_SERVER['DOCUMENT_ROOT'].$protectedDir, 0755, true);
		}
		
		if ($convertToWebp === "Y")
		{
			$image->setType("image/webp");
		}
		
		if ($image->save($_SERVER['DOCUMENT_ROOT'].$protectedDir.$protectedFile, $minJPEGQuality))
		{
			$image->optimize($_SERVER['DOCUMENT_ROOT'].$protectedDir.$protectedFile);
		}
		
		\Ram\Watermark\ImageTable::update($params['file']['ID'], Array('HASH' => $watermarksHash));
		
		$return = Array
		(
			'src' => $protectedDir.$protectedFile,
			'width' => $image->getWidth(),
			'height' => $image->getHeight(),
			"EXISTS" => "N"
		);
		
		$image->destroy();
		unset($image);
		
		return $return;
	}
	
	public static function createCache($id, $params)
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$watermarkCacheDir = '/'.$uploadDir.'/ram.watermark/images/cache/'.$id.'/';
		$watermarkImagesDir = '/'.$uploadDir.'/ram.watermark/images/watermarks/';
		$watermarkFontsDir = '/'.$uploadDir.'/ram.watermark/fonts/';
		$watermarkCacheFile = $id.'.png';
		
		$params['IMAGE'] = $_SERVER['DOCUMENT_ROOT'].$watermarkImagesDir.$params['IMAGE'];
		$params['TEXT_FONT'] = $_SERVER['DOCUMENT_ROOT'].$watermarkFontsDir.$params['TEXT_FONT'];
		
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir)) mkdir($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir, 0755, true);
		
		$wm = new \CRamWatermarkImage();
		
		if ($params['TYPE'] === 'image')
		{
			$wm->from($params['IMAGE']);

			if ($params['SCALE'] > 0)
			{
				if ($wm->getWidth() > 1000 || $wm->getHeight() > 1000)
				{
					$wm->resize(Array('resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL, 'width' => 1000, 'height' => 1000));
				}
			}
		}
		else
		{
			if ($params['SCALE'] > 0)
			{
				$textParams = $params;
				$textParams['TEXT_SIZE'] = 20;
				$wm->text($textParams);
			
				$ratio = $wm->getWidth() / $wm->getHeight();
			
				$h = 1000;
			
				if ($ratio > 1)
				{
					$h = 1000 / $ratio;
				}
			
				$textParams['TEXT_SIZE'] = $h / count(explode("\n", $textParams['TEXT'])) / $textParams['TEXT_LEADING'];
			
				$wm->text($textParams);
			}
			else
			{
				$wm->text($params);
			}
		}
		
		if ($params['ROTATE'] > 0)
		{
			$wm->rotate($params['ROTATE']);
		}
		if ($params['TRANSPARENT'] > 0)
		{
			$wm->transparent($params['TRANSPARENT']);
		}
		$wm->setType("image/png");
		$wm->save($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir.$watermarkCacheFile);
		$wm->destroy();
		unset($wm);
	}
	
	public static function checkFile($arFile, $siteId, $resizeParams = null)
	{
		$tag = (!empty($resizeParams)) ? $siteId.'_resize_'.$resizeParams['width'].'_'.$resizeParams['height'].'_'.$resizeParams['resizeType'] : $siteId.'_original';
		
		if (!isset(\CRamWatermark::$cache["WATERMARKS"]))
		{
			\CRamWatermark::$cache["WATERMARKS"] = $marks = \Ram\Watermark\MarkTable::getList(Array('order' => Array('ID' => 'ASC'), 'filter' => Array('ACTIVE' => 'Y'), 'select' => Array('*')))->fetchAll();
		}
		else
		{
			$marks = \CRamWatermark::$cache["WATERMARKS"];
		}
		
		if (!$marks)
		{
			$images = \Ram\Watermark\ImageTable::getList(Array('filter' => Array('IMAGEID' => $arFile['ID'])))->fetchAll();
			if (!empty($images))
			{
				foreach ($images as $image)
				{
					\Ram\Watermark\ImageTable::update($image['ID'], Array('HASH' => 'clear'));
				}
			}
			
			return false;
		}
		else if (in_array($arFile["ID"]."_".$tag, \CRamWatermark::$cache["CLEAR_IMAGES"]))
		{
			return false;
		}
		
		$uploadDir = \CRamWatermark::GetUploadDir();
		
		$image = \Ram\Watermark\ImageTable::getRow(Array('filter' => Array('IMAGEID' => $arFile['ID'], 'TAG' => $tag)));
		
		if (!$image && $tag === $siteId.'_original')
		{
			$image = \Ram\Watermark\ImageTable::getRow(Array('filter' => Array('IMAGEID' => $arFile['ID'], 'TAG' => 'original')));
			if ($image)
			{
				\Ram\Watermark\ImageTable::update($image['ID'], Array('TAG' => $siteId.'_original'));
			}
		}
		
		if (!$image && $tag !== $siteId.'_original')
		{
			$image = \Ram\Watermark\ImageTable::getRow(Array('filter' => Array('IMAGEID' => $arFile['ID'], 'TAG' => $siteId.'_original')));
			if ($image)
			{
				unset($image['ID']);
				$image['TAG'] = $tag;
				
				$res = \Ram\Watermark\ImageTable::add($image);
				if ($res->isSuccess())
				{
					$image = \Ram\Watermark\ImageTable::getRowById($res->getID());
				}
			}
		}
		
		$source = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/'.$arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
		
		if ($arFile['HANDLER_ID'])
		{
			$pathinfo = pathinfo($arFile['FILE_NAME']);
			$arFile['EXT'] = strtolower($pathinfo['extension']);
			
			$source = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/tmp/ram.watermark/'.substr(md5($arFile['ID']), 0, 1).'.'.$arFile['EXT'];
			
			if (Bitrix\Main\Loader::includeModule('clouds'))
			{
				$ob = new CCloudStorageBucket(intval($arFile['HANDLER_ID']));
				if (!$ob->Init() || $ob->ACTIVE !== "Y")
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		
		if (!$image)
		{
			if ($arFile['HANDLER_ID'])
			{
				$ob = new CCloudStorageBucket(intval($arFile['HANDLER_ID']));
				$ob->Init();
				$ob->DownloadToFile($arFile, $source);
			}
			
			if (isset($arFile['FORUM_ID']))
			{
				$arFile['MODULE_ID'] = 'forum';
			}
			
			$ENTITY = null;
			$OBJECT = null;
			$FIELD = null;
			$DATE = $arFile['TIMESTAMP_X'];
			$ITEM = null;
			$create = true;
			if (!file_exists($source)) return false;
			
			switch ($arFile['MODULE_ID'])
			{
				case 'iblock':
				{
					if (Bitrix\Main\Loader::includeModule('iblock'))
					{
						$element = \Bitrix\Iblock\ElementTable::getRow(Array('filter' => Array('LOGIC' => 'OR', Array('PREVIEW_PICTURE' => $arFile['ID']), Array('DETAIL_PICTURE' => $arFile['ID'])), 'select' => Array('ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'DATE_CREATE')));
						if ($element)
						{
							$ENTITY = $element['IBLOCK_ID'];
							$OBJECT = $element['ID'];
							$FIELD = $element['PREVIEW_PICTURE'] == $arFile['ID'] ? 'PREVIEW_PICTURE' : 'DETAIL_PICTURE';
							$ITEM = 'element';
						}
						else
						{
							$row = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM b_iblock_element_property as ep, b_iblock_property as p WHERE ep.VALUE='.$arFile['ID'].' AND ep.IBLOCK_PROPERTY_ID = p.ID AND p.PROPERTY_TYPE LIKE "F";')->fetch();
							if (!$row)
							{
								$allProps = CIBlockProperty::GetList(Array('name' => 'asc'), Array('ACTIVE' => 'Y', 'PROPERTY_TYPE' => 'F', 'VERSION' => 2, 'CHECK_PERMISSIONS' => 'N'));
								$sProps = Array();
								$mProps = Array();
								
								while ($arProp = $allProps -> Fetch())
								{
									if ($arProp['MULTIPLE'] === 'Y')
									{
										$mProps[$arProp['ID']] = $arProp['IBLOCK_ID'];
									}
									else
									{
										$sProps[$arProp['ID']] = $arProp['IBLOCK_ID'];
									}
								}
								
								if (!empty($mProps))
								{
									foreach ($mProps as $propId => $iblockId)
									{
										$row = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM b_iblock_element_prop_m'.$iblockId.' WHERE VALUE = '.$arFile['ID'].';')->fetch();
										
										if ($row)
										{
											break;
										}
									}
								}
								
								if (!$row && !empty($sProps))
								{
									foreach ($sProps as $propId => $iblockId)
									{
										$row = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM b_iblock_element_prop_s'.$iblockId.' WHERE PROPERTY_'.$propId.' = '.$arFile['ID'].';')->fetch();
											
										if ($row)
										{
											break;
										}
									}
								}
							}
							
							if ($row)
							{
								$element = \Bitrix\Iblock\ElementTable::getRow(Array('filter' => Array('ID' => $row['IBLOCK_ELEMENT_ID']), 'select' => Array('ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'IBLOCK_ID', 'DATE_CREATE')));
								if ($element)
								{
									$ENTITY = $element['IBLOCK_ID'];
									$OBJECT = $element['ID'];
									$FIELD = $row['IBLOCK_PROPERTY_ID'];
									$ITEM = 'element';
								}
							}
							else
							{
								$section = \Bitrix\Iblock\SectionTable::getRow(Array('filter' => Array('LOGIC' => 'OR', Array('PICTURE' => $arFile['ID']), Array('DETAIL_PICTURE' => $arFile['ID'])), 'select' => Array('ID', 'PICTURE', 'DETAIL_PICTURE', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'DATE_CREATE')));
								if ($section)
								{
									$ENTITY = $section['IBLOCK_ID'];
									$OBJECT = $section['ID'];
									$FIELD = $section['PICTURE'] == $arFile['ID'] ? 'PICTURE' : 'DETAIL_PICTURE';
									$ITEM = 'section';
								}
							}
						}
					}
					else
					{
						$create = false;
					}
					break;
				}
				case 'fileman':
				{
					if (Bitrix\Main\Loader::includeModule('fileman'))
					{
						CMedialib::Init();
						$row = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM b_medialib_item WHERE SOURCE_ID='.$arFile['ID'].';')->fetch();
						if ($row)
						{
							$OBJECT = $row['ID'];
							$DATE = $row['DATE_CREATE'];
						}
					}
					else
					{
						$create = false;
					}
					break;
				}
				case 'forum':
				{
					if (Bitrix\Main\Loader::includeModule('forum'))
					{
						$row = \Bitrix\Main\Application::getConnection()->query('SELECT f.FORUM_ID, f.MESSAGE_ID, m.POST_DATE FROM b_forum_file as f, b_forum_message as m WHERE f.FILE_ID='.$arFile['ID'].' AND m.ID = f.MESSAGE_ID;')->fetch();
						if ($row)
						{
							$ENTITY = $row['FORUM_ID'];
							$OBJECT = $row['MESSAGE_ID'];
						}
					}
					else
					{
						$create = false;
					}
					break;
				}
				case 'main':
				{
					if (Bitrix\Main\Loader::includeModule('highloadblock'))
					{
						$hlblocks = \Bitrix\Highloadblock\HighloadBlockTable::getList(Array('cache' => Array('ttl' => 60*60*24*365)))->fetchAll();
						foreach ($hlblocks as $hlblock)
						{
							$multipleFiles = Array();
							$singleFiles = Array();
							$hlblockProps = \Bitrix\Main\UserFieldTable::getList(Array('order' => Array('SORT' => 'ASC'), 'filter' => Array('ENTITY_ID' => 'HLBLOCK_'.$hlblock['ID'], 'USER_TYPE_ID' => 'file'), 'cache' => Array('ttl' => 60*60*24*365)))->fetchAll();
							foreach ($hlblockProps as $hlblockProp)
							{
								if ($hlblockProp['MULTIPLE'] === 'Y')
								{
									$multipleFiles[$hlblockProp['ID']] = $hlblockProp['FIELD_NAME'];
								}
								else
								{
									$singleFiles[$hlblockProp['ID']] = $hlblockProp['FIELD_NAME'];
								}
							}
							if (!empty($multipleFiles))
							{
								foreach ($multipleFiles as $propId => $propCode)
								{
									$row = \Bitrix\Main\Application::getConnection()->query('SELECT ID FROM '.$hlblock['TABLE_NAME'].'_'.strtolower($propCode).' WHERE VALUE='.$arFile['ID'].';')->fetch();
									if ($row)
									{
										$ENTITY = $hlblock['ID'];
										$FIELD = $propId;
										$arFile['MODULE_ID'] = 'highloadblock';
										break;
									}
								}
							}
							if (!$ENTITY && !empty($singleFiles))
							{
								$row = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM '.$hlblock['TABLE_NAME'].' WHERE '.implode('='.$arFile['ID'].' OR ', $singleFiles).'='.$arFile['ID'].';')->fetch();
								if ($row)
								{
									$ENTITY = $hlblock['ID'];
									foreach ($singleFiles as $propId => $propCode)
									{
										if ($row[$propCode] == $arFile['ID'])
										{
											$FIELD = $propId;
											break;
										}
									}
									$arFile['MODULE_ID'] = 'highloadblock';
								}
							}
							if ($ENTITY)
							{
								break;
							}
						}
					}
					if (Bitrix\Main\Loader::includeModule('iblock'))
					{
						$iblocks = \Bitrix\Iblock\IblockTable::getList(Array('select' => Array('ID'), 'cache' => Array('ttl' => 60*60*24*365)))->fetchAll();
						foreach ($iblocks as $iblock)
						{
							$multipleFiles = Array();
							$singleFiles = Array();
							$iblockProps = \Bitrix\Main\UserFieldTable::getList(Array('order' => Array('SORT' => 'ASC'), 'filter' => Array('ENTITY_ID' => 'IBLOCK_'.$iblock['ID'].'_SECTION', 'USER_TYPE_ID' => 'file'), 'cache' => Array('ttl' => 60*60*24*365)))->fetchAll();
							foreach ($iblockProps as $iblockProp)
							{
								if ($iblockProp['MULTIPLE'] === 'Y')
								{
									$multipleFiles[$iblockProp['ID']] = $iblockProp['FIELD_NAME'];
								}
								else
								{
									$singleFiles[$iblockProp['ID']] = $iblockProp['FIELD_NAME'];
								}
							}
							if (!empty($multipleFiles))
							{
								foreach ($multipleFiles as $propId => $propCode)
								{
									$row = \Bitrix\Main\Application::getConnection()->query('SELECT FIELD_ID, VALUE_ID FROM b_utm_iblock_'.$iblock['ID'].'_section WHERE VALUE_INT='.$arFile['ID'].';')->fetch();
									if ($row)
									{
										$ENTITY = $iblock['ID'];
										$OBJECT = $row['VALUE_ID'];
										$FIELD = $row['FIELD_ID'];
										$arFile['MODULE_ID'] = 'iblock';
										break;
									}
								}
							}
							if (!$ENTITY && !empty($singleFiles))
							{
								$row = \Bitrix\Main\Application::getConnection()->query('SELECT * FROM b_uts_iblock_'.$iblock['ID'].'_section WHERE '.implode('='.$arFile['ID'].' OR ', $singleFiles).'='.$arFile['ID'].';')->fetch();
								if ($row)
								{
									$ENTITY = $iblock['ID'];
									$OBJECT = $row['VALUE_ID'];
									foreach ($singleFiles as $propId => $propCode)
									{
										if ($row[$propCode] == $arFile['ID'])
										{
											$FIELD = $propId;
											break;
										}
									}
									$arFile['MODULE_ID'] = 'iblock';
									break;
								}
							}
							if ($ENTITY)
							{
								break;
							}
						}
						if ($OBJECT)
						{
							// $section = \Bitrix\Iblock\SectionTable::getRowById($OBJECT);
							// $DATE = $section['DATE_CREATE'];
							$ITEM = 'section';
						}
					}
					
					if (!$ENTITY)
					{
						$create = false;
					}
				}
			}
			
			if ($create)
			{
				$imageSize = getimagesize($source);
				if (extension_loaded('fileinfo'))
				{
					$fileInfo = finfo_open(FILEINFO_MIME_TYPE);
					$mime = finfo_file($fileInfo, $source);
				}
				else
				{
					$imageSizeMime = getimagesize($source);
					$mime = $imageSizeMime['mime'];
				}
				
				$date1 = new \Bitrix\Main\Type\DateTime($DATE);
				$date2 = \Bitrix\Main\Type\DateTime::createFromTimestamp(filectime($source));
				
				if ($date1 < $date2)
				{
					$imageDate = $date1;
				}
				else
				{
					$imageDate = $date2;
				}
				
				$res = \Ram\Watermark\ImageTable::add(Array
				(
					'IMAGEID' => $arFile['ID'],
					'WIDTH' => $imageSize[0],
					'HEIGHT' => $imageSize[1],
					'TYPE' => $mime,
					'MODULE' => $arFile['MODULE_ID'],
					'ENTITY' => $ENTITY,
					'OBJECT' => $OBJECT,
					'ITEM' => $ITEM,
					'FIELD' => $FIELD,
					'DATE' => $imageDate,
					'TAG' => $tag,
				));
				if ($res->isSuccess())
				{
					$image = \Ram\Watermark\ImageTable::getRowById($res->getID());
					
					$image["NEW"] = "Y";
				}
			}
		}
		
		if ($image && !in_array($image['TYPE'], Array('image/jpeg', 'image/png', 'image/gif', 'image/bmp', 'image/webp')))
		{
			unset($image);
		}
		
		if (!$image)
		{
			if ($arFile['HANDLER_ID'] && file_exists($source) && substr_count($source, $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/tmp/'))
			{
				unlink($source);
			}
			
			return false;
		}
		
		if ($image['MODULE'] === 'iblock' && Bitrix\Main\Loader::includeModule('iblock'))
		{
			if ($image['ITEM'] === 'element')
			{
				$element = \Bitrix\Iblock\ElementTable::getRow(Array('filter' => Array('ID' => $image['OBJECT']), 'select' => Array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID')));
				
				if ($element['IBLOCK_SECTION_ID'])
				{
					$objectCache = 'PARENTS__'.$image['MODULE'].'_element_'.$element['ID'];
					
					if (!isset(\CRamWatermark::$cache[$objectCache]))
					{
						$selfSections = Array();
						$sections = Array();
						
						$elementGroups = CIBlockElement::GetElementGroups($element['ID'], true);
						
						while ($elementGroup = $elementGroups->Fetch())
						{
							$selfSections[] = $elementGroup['ID'];
							
							$sectionChain = CIBlockSection::GetNavChain($element['IBLOCK_ID'], $elementGroup['ID'], Array('ID'));
							while ($sectionPath = $sectionChain->GetNext())
							{
								$sections[] = $sectionPath['ID'];
							}
						}
						
						$image['SELF_SECTIONS'] = $selfSections;
						$image['SECTIONS'] = $sections;
						
						\CRamWatermark::$cache[$objectCache] = Array('SELF_SECTIONS' => $selfSections, 'SECTIONS' => $sections);
					}
					else
					{
						$image['SELF_SECTIONS'] = \CRamWatermark::$cache[$objectCache]['SELF_SECTIONS'];
						$image['SECTIONS'] = \CRamWatermark::$cache[$objectCache]['SECTIONS'];
					}
				}
			}
			else
			{
				$objectCache = 'PARENTS__'.$image['MODULE'].'_section_'.$image['OBJECT'];
				
				if (!isset(\CRamWatermark::$cache[$objectCache]))
				{
					$sectionChain = CIBlockSection::GetNavChain($image['ENTITY'], $image['OBJECT'], Array('ID'));
					while ($sectionPath = $sectionChain->GetNext())
					{
						$sections[] = $sectionPath['ID'];
					}
					\CRamWatermark::$cache[$objectCache] = $image['SECTIONS'] = $sections;
				}
				else
				{
					$image['SECTIONS'] = \CRamWatermark::$cache[$objectCache];
				}
			}
		}
		else if ($image['MODULE'] === 'fileman' && Bitrix\Main\Loader::includeModule('fileman'))
		{
			CMedialib::Init();
			$collections = CMedialibItem::GetItemCollections(Array('ID' => $image['OBJECT']));
			$parentCollections = Array();
			foreach ($collections as $collection)
			{
				$objectCache = 'PARENTS__'.$image['MODULE'].'_'.$collection;
				
				if (!isset(\CRamWatermark::$cache[$objectCache]))
				{
					\CRamWatermark::$cache[$objectCache] = Array();
					$coll = CMedialibCollection::GetList(Array('arFilter' => Array('ID' => $collection)));
					$parentCollections[] = $coll[0]['ID'];
					\CRamWatermark::$cache[$objectCache][] = $coll[0]['ID'];
					while ($coll[0]['PARENT_ID'])
					{
						$coll = CMedialibCollection::GetList(Array('arFilter' => Array('ID' => $coll[0]['PARENT_ID'])));
						$parentCollections[] = $coll[0]['ID'];
						\CRamWatermark::$cache[$objectCache][] = $coll[0]['ID'];
					}
				}
				else
				{
					$parentCollections = \CRamWatermark::$cache[$objectCache];
				}
			}
			$image['COLLECTIONS'] = $collections;
			$image['PARENT_COLLECTIONS'] = $parentCollections;
		}
		
		$protectMarks = Array();

		foreach ($marks as $mark)
		{
			$objectCache = 'FILTERS__'.$mark['ID'];
			
			if (!isset(\CRamWatermark::$cache[$objectCache]))
			{
				\CRamWatermark::$cache[$objectCache] = $filters = \Ram\Watermark\FilterTable::getList(Array('filter' => Array('WMID' => $mark['ID'])))->fetchAll();
			}
			else
			{
				$filters = \CRamWatermark::$cache[$objectCache];
			}
			
			if (!$filters || empty($filters)) continue;
			
			$candidate = null;
			
			foreach ($filters as $filter)
			{
				if ($filter['MODULE'] === 'all')
				{
					$candidate = $filter['TYPE'] === 'include';
				}
				else if ($filter['MODULE'] === $image['MODULE'])
				{
					if ($filter['MODULE'] === 'iblock')
					{
						if ($filter['ENTITY'] === 'all')
						{
							if ($filter['OBJECT'] === 'elements' && $image['ITEM'] === 'element' && ($filter['FIELD'] === $image['FIELD'] || (is_numeric($image['FIELD']) && $filter['FIELD'] === 'FILE')))
							{
								$candidate = $filter['TYPE'] === 'include';
							}
							else if ($filter['OBJECT'] === 'sections' && $image['ITEM'] === 'section' && ($filter['FIELD'] === $image['FIELD'] || (is_numeric($image['FIELD']) && $filter['FIELD'] === 'FILE')))
							{
								$candidate = $filter['TYPE'] === 'include';
							}
						}
						else if (is_numeric($filter['ENTITY']) && $image['ENTITY'] == $filter['ENTITY'])
						{
							if (is_numeric($filter['GROUP']))
							{
								if ($filter['OBJECT'] === 'elements' && $image['ITEM'] === 'element' && $filter['FIELD'] === $image['FIELD'] && !empty($image['SELF_SECTIONS']) && in_array($filter['GROUP'], $image['SELF_SECTIONS']))
								{
									$candidate = $filter['TYPE'] === 'include';
								}
								else if ($filter['OBJECT'] === 'subelements' && $image['ITEM'] === 'element' && $filter['FIELD'] === $image['FIELD'] && !empty($image['SECTIONS']) && in_array($filter['GROUP'], $image['SECTIONS']))
								{
									$candidate = $filter['TYPE'] === 'include';
								}
								else if ($filter['OBJECT'] === 'sections' && $image['ITEM'] === 'section' && $filter['FIELD'] === $image['FIELD'] && !empty($image['SECTIONS']) && in_array($filter['GROUP'], $image['SECTIONS']))
								{
									$candidate = $filter['TYPE'] === 'include';
								}
								else if ($filter['OBJECT'] === 'section' && $image['ITEM'] === 'section' && $filter['FIELD'] === $image['FIELD'] && $image['OBJECT'] == $filter['GROUP'])
								{
									$candidate = $filter['TYPE'] === 'include';
								}
							}
							else
							{
								if ($filter['OBJECT'] === 'elements' && $image['ITEM'] === 'element' && $filter['FIELD'] === $image['FIELD'])
								{
									$candidate = $filter['TYPE'] === 'include';
								}
								else if ($filter['OBJECT'] === 'sections' && $image['ITEM'] === 'section' && $filter['FIELD'] === $image['FIELD'])
								{
									$candidate = $filter['TYPE'] === 'include';
								}
							}
						}
					}
					else if ($filter['MODULE'] === 'fileman')
					{
						if ($filter['ENTITY'] === 'all')
						{
							$candidate = $filter['TYPE'] === 'include';
						}
						else
						{
							if ($filter['OBJECT'] === 'elements' && !empty($image['COLLECTIONS']) && in_array($filter['ENTITY'], $image['COLLECTIONS']))
							{
								$candidate = $filter['TYPE'] === 'include';
							}
							else if ($filter['OBJECT'] === 'subelements' && !empty($image['PARENT_COLLECTIONS']) && in_array($filter['ENTITY'], $image['PARENT_COLLECTIONS']))
							{
								$candidate = $filter['TYPE'] === 'include';
							}
						}
					}
					else if ($filter['MODULE'] === 'highloadblock')
					{
						if ($filter['ENTITY'] === 'all')
						{
							$candidate = $filter['TYPE'] === 'include';
						}
						else if ($filter['ENTITY'] === $image['ENTITY'] && $filter['FIELD'] === $image['FIELD'])
						{
							$candidate = $filter['TYPE'] === 'include';
						}
					}
					else if ($filter['MODULE'] === 'forum')
					{
						if ($filter['ENTITY'] === 'all')
						{
							$candidate = $filter['TYPE'] === 'include';
						}
						else if ($filter['ENTITY'] === $image['ENTITY'])
						{
							$candidate = $filter['TYPE'] === 'include';
						}
					}
					else
					{
						$candidate = $filter['TYPE'] === 'include';
					}
				}
				
				if ($candidate != null && $candidate == false) break;
			}
			
			if ($candidate)
			{
				$protectMarks[] = $mark;
			}
		}
		
		foreach ($protectMarks as $k => $mark)
		{
			$deleteMark = false;
			
			if ($mark['PARAMS']['LIMIT_SITE'] === 'Y')
			{
				if (!empty($mark['PARAMS']['LIMIT_SITES']) && !in_array($siteId, $mark['PARAMS']['LIMIT_SITES']))
				{
					$deleteMark = true;
				}
			}
			
			if (!$deleteMark && $mark['PARAMS']['LIMIT_TYPE'] === 'Y')
			{
				switch ($image['TYPE'])
				{
					case 'image/jpeg':
					{
						if ($mark['PARAMS']['LIMIT_JPG'] !== 'Y') $deleteMark = true;
						break;
					}
					case 'image/png':
					{
						if ($mark['PARAMS']['LIMIT_PNG'] !== 'Y') $deleteMark = true;
						break;
					}
					case 'image/gif':
					{
						if ($mark['PARAMS']['LIMIT_GIF'] !== 'Y') $deleteMark = true;
						break;
					}
					case 'image/bmp':
					{
						if ($mark['PARAMS']['LIMIT_BMP'] !== 'Y') $deleteMark = true;
						break;
					}
					case 'image/webp':
					{
						if ($mark['PARAMS']['LIMIT_WEBP'] !== 'Y') $deleteMark = true;
						break;
					}
				}
			}
			
			$createEmpty = false;
			
			if (!$deleteMark && $mark['PARAMS']['LIMIT_SIZES'] === 'Y')
			{
				if (!empty($resizeParams))
				{
					if ($resizeParams['width'] < $mark['PARAMS']['LIMIT_MIN_WIDTH'] || $resizeParams['width'] > $mark['PARAMS']['LIMIT_MAX_WIDTH'] || $resizeParams['height'] < $mark['PARAMS']['LIMIT_MIN_HEIGHT'] || $resizeParams['height'] > $mark['PARAMS']['LIMIT_MAX_HEIGHT'])
					{
						$createEmpty = true;
						$deleteMark = true;
					}
				}
				else
				{
					if ($image['WIDTH'] < $mark['PARAMS']['LIMIT_MIN_WIDTH'] || $image['WIDTH'] > $mark['PARAMS']['LIMIT_MAX_WIDTH'] || $image['HEIGHT'] < $mark['PARAMS']['LIMIT_MIN_HEIGHT'] || $image['HEIGHT'] > $mark['PARAMS']['LIMIT_MAX_HEIGHT'])
					{
						$deleteMark = true;
					}
				}
			}
			
			if (!$deleteMark)
			{
				if ($mark['PARAMS']['LIMIT_DATE'] === 'Y' && $image['DATE'])
				{
					if ($mark['PARAMS']['LIMIT_DATE_FROM'])
					{
						$mark['PARAMS']['LIMIT_DATE_FROM'] = new \Bitrix\Main\Type\DateTime($mark['PARAMS']['LIMIT_DATE_FROM']);
						
						if ($image['DATE'] < $mark['PARAMS']['LIMIT_DATE_FROM']) $deleteMark = true;
					}
					
					if ($mark['PARAMS']['LIMIT_DATE_TO'])
					{
						$mark['PARAMS']['LIMIT_DATE_TO'] = new \Bitrix\Main\Type\DateTime($mark['PARAMS']['LIMIT_DATE_TO']);
						
						if ($image['DATE'] > $mark['PARAMS']['LIMIT_DATE_TO']) $deleteMark = true;
					}
				}
			}
			
			if ($deleteMark)
			{
				unset($protectMarks[$k]);
			}
		}
		
		if (!empty($protectMarks) || (empty($protectedMarks) && $createEmpty))
		{
			$protectData = Array
			(
				'source' => $source,
				'file' => $image,
				'watermarks' => $createEmpty && empty($protectMarks) ? Array() : $protectMarks,
				'arFile' => $arFile,
			);
			
			if (!empty($resizeParams))
			{
				$protectData['resize'] = $resizeParams;
			}
			
			$protectImage = \CRamWatermark::protectImage($protectData);
			
			if ($protectImage === false)
			{
				return false;
			}
			else
			{
				$image["EXISTS"] = $protectImage["EXISTS"];
				
				return Array('src' => $protectImage['src'], 'source' => $source, 'clearResizeCache' => empty($resizeParams), 'image' => $image);
			}
		}
		else
		{
			if ($image["HASH"] !== "clear")
			{
				\Ram\Watermark\ImageTable::update($image["ID"], Array("HASH" => "clear"));
			}
			
			if ($arFile["HANDLER_ID"] && file_exists($source) && substr_count($source, $_SERVER["DOCUMENT_ROOT"]."/".$uploadDir."/tmp/"))
			{
				unlink($source);
			}
			
			return false;
		}
	}
	
	public static function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		$MODULE_ID = basename(dirname(__FILE__));
		
		global $APPLICATION;
		
		$MODULE_RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);
		
		if ($MODULE_RIGHT !== 'D')
		{
			$aMenu = array(
				"parent_menu" => "global_menu_services",
				"section" => $MODULE_ID,
				"sort" => 10,
				'text' => GetMessage("ram.watermark_WATERMARKS"),
				'url' => $MODULE_ID.'_list.php?lang='.LANGUAGE_ID,
				'module_id' => $MODULE_ID,
				"title" => GetMessage("ram.watermark_WATERMARKS"),
				"more_url" => Array($MODULE_ID.'_edit.php', $MODULE_ID.'_process.php'),
				"icon" => "ram-watermark-menu-icon",
			);
			
			$aModuleMenu[] = $aMenu;
			
			$APPLICATION->SetAdditionalCSS('/bitrix/panel/'.$MODULE_ID.'/menu.css');
		}
		else return false;
	}
	
	public static function resize($params)
	{
		if (!isset($params['file']) || !isset($params['watermark'])) return false;
		
		if (!is_array($params['file']) && intval($params['file']) > 0) $arFile = CFile::GetFileArray($params['file']);
		else $arFile = $params['file'];
		
		if (!is_array($arFile) || !array_key_exists('FILE_NAME', $arFile) || strlen($arFile['FILE_NAME']) <= 0) return false;
		$resizeParams = Array();
		
		if (!isset($params['width']) || !isset($params['height']) || ($params['width'] > $arFile['WIDTH'] && $params['height'] > $arFile['HEIGHT']))
		{
			$resizeParams['width'] = $arFile['WIDTH'];
			$resizeParams['height'] = $arFile['HEIGHT'];
		}
		else
		{
			$resizeParams['width'] = $params['width'];
			$resizeParams['height'] = $params['height'];
		}
		
		if (!isset($params['resizeType'])) $resizeParams['resizeType'] = BX_RESIZE_IMAGE_PROPORTIONAL;
		else $resizeParams['resizeType'] = $params['resizeType'];
		
		if (!isset($params['jpegQuality'])) $resizeParams['jpegQuality'] = 100;
		else $resizeParams['jpegQuality'] = $params['jpegQuality'];
		
		$marks = \Ram\Watermark\MarkTable::getList(Array('order' => Array('ID' => 'ASC'), 'filter' => Array('ACTIVE' => 'Y', 'ID' => $params['watermark']), 'select' => Array('*')))->fetchAll();
		if (!$marks) return false;
		
		foreach ($marks as $k => $mark)
		{
			$marks[$k]['PARAMS']['JPEG_QUALITY'] = $resizeParams['jpegQuality'];
		}
		
		if (!isset(\CRamWatermark::$cache["SITE_ID"]))
		{
			$context = Bitrix\Main\Context::getCurrent();
			\CRamWatermark::$cache["SITE_ID"] = $context->getSite();
			if (!strlen(\CRamWatermark::$cache["SITE_ID"])) \CRamWatermark::$cache["SITE_ID"] = "admin";
		}
		
		$tag = \CRamWatermark::$cache["SITE_ID"].'_resize_manual_'.$resizeParams['width'].'_'.$resizeParams['height'].'_'.$resizeParams['resizeType'];
		$image = \Ram\Watermark\ImageTable::getRow(Array('filter' => Array('IMAGEID' => $arFile['ID'], 'TAG' => $tag)));
		if (!$image)
		{
			$image = \Ram\Watermark\ImageTable::getRow(Array('filter' => Array('IMAGEID' => $arFile['ID'], 'TAG' => \CRamWatermark::$cache["SITE_ID"].'_original')));
			if ($image)
			{
				unset($image['ID']);
				$image['TAG'] = $tag;
				
				$res = \Ram\Watermark\ImageTable::add($image);
				if ($res->isSuccess())
				{
					$image = \Ram\Watermark\ImageTable::getRowById($res->getID());
				}
			}
		}
		if (!$image) return false;
		
		$uploadDir = \CRamWatermark::GetUploadDir();
		$source = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/'.$arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
		if ($arFile['HANDLER_ID'])
		{
			$pathinfo = pathinfo($arFile['FILE_NAME']);
			$arFile['EXT'] = $pathinfo['extension'];
			$source = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/tmp/'.substr(md5($arFile['SRC']), 0, 3).'/'.$arFile['FILE_NAME'];
			
			if (Bitrix\Main\Loader::includeModule('clouds'))
			{
				$ob = new CCloudStorageBucket(intval($arFile['HANDLER_ID']));
				if ($ob->Init() && $ob->ACTIVE === "Y")
				{
					$ob->DownloadToFile($arFile, $source);
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		
		$protectData = Array
		(
			'source' => $source,
			'file' => $image,
			'watermarks' => $marks,
			'arFile' => $arFile,
		);
		if (!empty($resizeParams))
		{
			$protectData['resize'] = $resizeParams;
		}
		$protectImage = \CRamWatermark::protectImage($protectData);
		
		if ($arFile['HANDLER_ID'] && file_exists($source) && substr_count($source, $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/tmp/'))
		{
			unlink($source);
		}
		
		return Array('SRC' => $protectImage['src'], 'WIDTH' => $protectImage['width'], 'HEIGHT' => $protectImage['height'], 'src' => $protectImage['src'], 'width' => $protectImage['width'], 'height' => $protectImage['height']);
	}
	
	public static function clearCache($id)
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$watermarkCacheDir = '/'.$uploadDir.'/ram.watermark/images/cache/'.$id.'/';
		
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir))
		{
			\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER['DOCUMENT_ROOT'].$watermarkCacheDir);
		}
	}
	
	public static function clearResizeCache($file)
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$resizeDir = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/resize_cache/'.$file['SUBDIR'].'/';
		if (file_exists($resizeDir))
		{
			$resizeCacheDirs = scandir($resizeDir);
			foreach ($resizeCacheDirs as $resizeCacheDir)
			{
				if ($resizeCacheDir !== '.' && $resizeCacheDir !== '..' && is_dir($resizeDir.$resizeCacheDir))
				{
					if (file_exists($resizeDir.$resizeCacheDir.'/'.$file['FILE_NAME']))
					{
						unlink($resizeDir.$resizeCacheDir.'/'.$file['FILE_NAME']);
					}
				}
			}
		}
		if (file_exists($resizeDir.$file['FILE_NAME']))
		{
			unlink($resizeDir.$file['FILE_NAME']);
		}
	}
	
	public static function clearFile($ID)
	{
		$arFile = \CFile::GetByID($ID);
		$arFile = $arFile->Fetch();
		if (!$arFile['ID']) return true;
		$filePath = $arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
		$imagesDir = \Bitrix\Main\Config\Option::get("ram.watermark", "images_dir");
		$uploadDir = \CRamWatermark::GetUploadDir();
		if (file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath))
		{
			if ($arFile['HANDLER_ID'])
			{
				if (Bitrix\Main\Loader::includeModule('clouds'))
				{
					$ob = new CCloudStorageBucket(intval($arFile['HANDLER_ID']));
					if ($ob->Init() && $ob->ACTIVE === "Y")
					{
						$ob->DeleteFile("/".$filePath);
						$ob->DecFileCounter((float)$arFile["FILE_SIZE"]);
						$ob->SaveFile("/".$filePath, CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath));
						$sizes = getimagesize($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath);
						$filesize = filesize($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath);
						$ob->IncFileCounter((float)$filesize);
						unlink($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath);
						$path = '/resize_cache/'.$ID."/";
						$arCloudFiles = $ob->ListFiles($path, true);
						if(is_array($arCloudFiles["file"]))
						{
							foreach($arCloudFiles["file"] as $i => $file_name)
							{
								$tmp = $ob->DeleteFile($path.$file_name);
								if ($tmp) $ob->DecFileCounter($arCloudFiles["file_size"][$i]);
							}
						}
						$obCache = new CPHPCache();
						$obCache->CleanDir("clouds");
					}
				}
				else return false;
			}
			else
			{
				if (!copy($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath, $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/'.$filePath)) return false;
				unlink($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/ram.watermark/images/'.$imagesDir.'/'.$uploadDir.'/'.$filePath);
				clearstatcache(true, $_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/'.$filePath);
				$sizes = getimagesize($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/'.$filePath);
				$filesize = filesize($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/'.$filePath);
				if (file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/resize_cache/'.$arFile['SUBDIR']))
				{
					$resizeCacheDirs = scandir($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/resize_cache/'.$arFile['SUBDIR']);
					foreach ($resizeCacheDirs as $resizeCacheDir)
					{
						if ($resizeCacheDir !== '.' && $resizeCacheDir !== '..')
						{
							if (file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/resize_cache/'.$arFile['SUBDIR'].'/'.$resizeCacheDir.'/'.$arFile['FILE_NAME']))
							{
								unlink($_SERVER['DOCUMENT_ROOT'].'/'.$uploadDir.'/resize_cache/'.$arFile['SUBDIR'].'/'.$resizeCacheDir.'/'.$arFile['FILE_NAME']);
							}
						}
					}
				}
			}
			global $DB;
			$DB->Query("UPDATE b_file SET WIDTH=".$sizes[0].", HEIGHT=".$sizes[1].", FILE_SIZE=".$filesize." WHERE ID=".intval($ID));
			\CFile::CleanCache($ID);
		}
		return true;
	}
	
	public static function getFonts()
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$font_dir = '/'.$uploadDir.'/ram.watermark/fonts/';
		$font_image_dir = '/'.$uploadDir.'/ram.watermark/images/fonts/';
		$font_files = scandir($_SERVER['DOCUMENT_ROOT'].$font_dir);
		$fonts = Array();
		
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].$font_image_dir))
		{
			mkdir($_SERVER['DOCUMENT_ROOT'].$font_image_dir, 0755, true);
		}
		
		foreach ($font_files as $k => $font_file)
		{
			if (substr_count(strtolower($font_file), '.ttf'))
			{
				$font_image = str_replace('.ttf', '.png', strtolower($font_file));
				$font_info = \CRamWatermark::getFontInfo($_SERVER['DOCUMENT_ROOT'].$font_dir.$font_file);
				
				if (!file_exists($_SERVER['DOCUMENT_ROOT'].$font_image_dir.strtolower($font_image)))
				{
					$image = new \CRamWatermarkImage();
					$image->text(Array('TEXT' => $font_info[1].' '.$font_info[2], 'TEXT_SIZE' => 11, 'TEXT_ALIGN' => 'left', 'TEXT_LEADING' => '1.5', 'TEXT_COLOR' => '#000000', 'TEXT_FONT' => $_SERVER['DOCUMENT_ROOT'].$font_dir.$font_file));
					$image->setType("image/png");
					$image->save($_SERVER['DOCUMENT_ROOT'].$font_image_dir.strtolower($font_image));
					$image->destroy();
					unset($image);
				}
				
				$fontName = strtolower(str_replace('.ttf', '', $font_file));
				$fontName = preg_replace('/[^\\w]/', '', $fontName);
				
				$fonts[$font_file] = Array('name' => $fontName, 'title' => $font_info[1].' '.$font_info[2], 'src' => $font_image_dir.$font_image);
			}
		}
		
		uasort($fonts, function($a, $b){return strcasecmp($a['name'], $b['name']);});
		
		return $fonts;
	}
	
	private static function dec2ord($dec)
	{
		return \CRamWatermark::dec2hex(ord($dec));
	}
	
	private static function dec2hex($dec)
	{
		return str_repeat('0', 2-strlen(($hex=strtoupper(dechex($dec))))).$hex;
	}
	
	private static function getFontInfo($fontfile)
	{
		$fd = fopen($fontfile, "r");
		$text = fread($fd, filesize($fontfile));
		fclose ($fd);

		$number_of_tables = hexdec(\CRamWatermark::dec2ord($text[4]).\CRamWatermark::dec2ord($text[5]));

		for ($i=0;$i<$number_of_tables;$i++)
		{
			$tag = $text[12+$i*16].$text[12+$i*16+1].$text[12+$i*16+2].$text[12+$i*16+3];

			if ($tag == 'name')
			{
				$ntOffset = hexdec(\CRamWatermark::dec2ord($text[12+$i*16+8]).\CRamWatermark::dec2ord($text[12+$i*16+8+1]).\CRamWatermark::dec2ord($text[12+$i*16+8+2]).\CRamWatermark::dec2ord($text[12+$i*16+8+3]));

				$offset_storage_dec = hexdec(\CRamWatermark::dec2ord($text[$ntOffset+4]).\CRamWatermark::dec2ord($text[$ntOffset+5]));
				$number_name_records_dec = hexdec(\CRamWatermark::dec2ord($text[$ntOffset+2]).\CRamWatermark::dec2ord($text[$ntOffset+3]));
			}
		}

		$storage_dec = $offset_storage_dec + $ntOffset;
		$storage_hex = strtoupper(dechex($storage_dec));

		for ($j=0;$j<$number_name_records_dec;$j++)
		{
			$platform_id_dec = hexdec(\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+0]).\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+1]));
			$name_id_dec = hexdec(\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+6]).\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+7]));
			$string_length_dec = hexdec(\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+8]).\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+9]));
			$string_offset_dec = hexdec(\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+10]).\CRamWatermark::dec2ord($text[$ntOffset+6+$j*12+11]));

			if (!empty($name_id_dec) and empty($font_tags[$name_id_dec]))
			{
				for($l=0;$l<$string_length_dec;$l++)
				{
					if (ord($text[$storage_dec+$string_offset_dec+$l]) == '0') { continue; }
					else { $font_tags[$name_id_dec] .= ($text[$storage_dec+$string_offset_dec+$l]); }
				}
			}
		}
		return $font_tags;
	}
	
	public static function getImages()
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$image_dir = '/'.$uploadDir.'/ram.watermark/images/watermarks/';
		$image_files = scandir($_SERVER['DOCUMENT_ROOT'].$image_dir);
		$images = Array();
		foreach ($image_files as $k => $image_file)
		{
			if ($image_file === '.' || $image_file === '..') continue;
			
			$file_info = pathinfo($_SERVER['DOCUMENT_ROOT'].$image_dir.$image_file);
			
			if (in_array(strtolower($file_info['extension']), Array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp')))
			{				
				$images[$image_file] = Array('title' => $image_file, 'src' => $image_dir.$image_file);
			}
		}
		
		return $images;
	}
	
	public static function adminFilterList($arParams)
	{
		$arResult = Array();
		switch ($arParams['OBJECT'])
		{
			case 'modules':
			{
				$showModules = \Bitrix\Main\Config\Option::get("ram.watermark", "show_modules", "N");
				
				$arResult = Array('all' => Array('NAME' => Loc::getMessage('ram.watermark_ALL_MODULES'), 'ID' => $arParams['TYPE'].'_all'));
				
				$dbModules = CModule::GetList();
				while ($module = $dbModules->Fetch())
				{
					$moduleId = $module['ID'];
					if (getLocalPath('modules/'.$moduleId.'/install/index.php') !== false)
					{
						$moduleInfo = CModule::CreateModuleObject($moduleId);
						
						if ($moduleInfo)
						{
							if ($moduleId === 'fileman')
							{
								$moduleInfo->MODULE_NAME = Loc::getMessage('ram.watermark_MEDIALIB').' ('.$moduleInfo->MODULE_NAME.')';
							}
							else if ($moduleId === 'iblock')
							{
								$moduleInfo->MODULE_NAME = Loc::getMessage('ram.watermark_CATALOGS').' ('.$moduleInfo->MODULE_NAME.')';
							}
							
							$arResult[$moduleId] = Array('NAME' => $moduleInfo->MODULE_NAME, 'ID' => $arParams['TYPE'].'_'.$moduleId);
						}
					}
				}
				
				uasort($arResult, function ($a, $b){return strcasecmp($a['NAME'], $b['NAME']);});
				
				$topModules = Array();
				
				if (isset($arResult['iblock']))
				{
					$topModules['iblock'] = $arResult['iblock'];
					unset($arResult['iblock']);
				}
				if (isset($arResult['fileman']))
				{
					$topModules['fileman'] = $arResult['fileman'];
					unset($arResult['fileman']);
				}
				if (isset($arResult['highloadblock']))
				{
					$topModules['highloadblock'] = $arResult['highloadblock'];
					unset($arResult['highloadblock']);
				}
				if (isset($arResult['forum']))
				{
					$topModules['forum'] = $arResult['forum'];
					unset($arResult['forum']);
				}
				
				if ($showModules === "Y")
				{
					$topModules["-----"] = Array('NAME' => "-----", 'ID' => -1);
					
					if (isset($arResult['all']))
					{
						$topModules['all'] = $arResult['all'];
						unset($arResult['all']);
					}
					
					$arResult = array_merge($topModules, $arResult);
				}
				else
				{
					$arResult = $topModules;
				}
				
				break;
			}
			case 'iblock':
			{
				if (Bitrix\Main\Loader::includeModule('iblock'))
				{
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_IBLOCKS'), 'ID' => $arParams['TYPE'].'_iblock_all');
					$dbIBlocks = CIBlock::GetList(Array('iblock_type' => 'ASC', 'name' => 'ASC'), Array('CHECK_PERMISSIONS' => 'N'), false);
					$type = null;
					while ($arIBlock = $dbIBlocks->Fetch())
					{
						if ($type == null || $arIBlock['IBLOCK_TYPE_ID'] !== $type)
						{
							$type = $arIBlock['IBLOCK_TYPE_ID'];
							$arResult[] = Array('NAME' => $type, 'ID' => -1);
						}
						$arResult[] = Array('NAME' => $arIBlock['NAME'].' ('.$arIBlock['ID'].')', 'ID' => $arParams['TYPE'].'_iblock_'.$arIBlock['ID']);
					}
				}
				
				break;
			}
			case 'iblock_all':
			{
				$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ELEMENTS'), 'ID' => $arParams['TYPE'].'_iblock_all_elements');
				$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_SECTIONS'), 'ID' => $arParams['TYPE'].'_iblock_all_sections');
				
				break;
			}
			case 'iblock_id':
			{
				if (Bitrix\Main\Loader::includeModule('iblock'))
				{
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ELEMENTS'), 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_elements');
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_SECTIONS'), 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_sections');
					$dbSections = CIBlockSection::GetList(Array('NAME' => 'ASC'), Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'DEPTH_LEVEL' => 1, 'CHECK_PERMISSIONS' => 'N'));
					while ($arSection = $dbSections->GetNext())
					{
						$arResult[] = Array('NAME' => '. '.$arSection['NAME'], 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_'.$arSection['ID']);
					}
				}
				
				break;
			}
			case 'iblock_section':
			{
				if (Bitrix\Main\Loader::includeModule('iblock'))
				{
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ELEMENTS'), 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_'.$arParams['SECTION_ID'].'_elements');
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_SUBELEMENTS'), 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_'.$arParams['SECTION_ID'].'_subelements');
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_SECTIONS'), 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_'.$arParams['SECTION_ID'].'_sections');
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_SECTION'), 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_'.$arParams['SECTION_ID'].'_section');
					$dbSection = CIBlockSection::GetList(Array('NAME' => 'ASC'), Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arParams['SECTION_ID'], 'CHECK_PERMISSIONS' => 'N'));
					if ($arSection = $dbSection->GetNext())
					{
						$dbSections = CIBlockSection::GetList(Array('NAME' => 'ASC'), Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'SECTION_ID' => $arParams['SECTION_ID'], 'DEPTH_LEVEL' => $arSection['DEPTH_LEVEL'] + 1, 'CHECK_PERMISSIONS' => 'N'));
						while ($arSection = $dbSections->GetNext())
						{
							$arResult[] = Array('NAME' => '. '.$arSection['NAME'], 'ID' => $arParams['TYPE'].'_iblock_'.$arParams['IBLOCK_ID'].'_'.$arSection['ID']);
						}
					}
				}
				
				break;
			}
			case 'forum':
			{
				if (Bitrix\Main\Loader::includeModule('forum'))
				{
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_FORUMS'), 'ID' => $arParams['TYPE'].'_forum_all');
					$dbForums = CForumNew::GetList(Array('NAME' => 'ASC'), Array());
					while ($arForum = $dbForums->Fetch())
					{
						$arResult[] = Array('NAME' => $arForum['NAME'], 'ID' => $arParams['TYPE'].'_forum_'.$arForum['ID']);
					}
				}
				
				break;
			}
			case 'highloadblock':
			{
				if (Bitrix\Main\Loader::includeModule('highloadblock'))
				{
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_HIGHLOADBLOCKS'), 'ID' => $arParams['TYPE'].'_highloadblock_all');
					$dbHighloadblocks = \Bitrix\Highloadblock\HighloadBlockTable::getList(Array('select' => Array('*', 'NAME_LANG' => 'LANG.NAME'), 'order' => Array('NAME_LANG' => 'ASC', 'NAME' => 'ASC')));
					while ($arHighloadblock = $dbHighloadblocks -> fetch())
					{
						if (strlen($arHighloadblock['NAME_LANG'])) $arHighloadblock['NAME'] = $arHighloadblock['NAME_LANG'];
						
						$arResult[] = Array('NAME' => $arHighloadblock['NAME'], 'ID' => $arParams['TYPE'].'_highloadblock_'.$arHighloadblock['ID']);
					}
				}
				
				break;
			}
			case 'fileman':
			{
				if (Bitrix\Main\Loader::includeModule('fileman'))
				{
					CMedialib::Init();
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_COLLECTIONS'), 'ID' => $arParams['TYPE'].'_fileman_all');
					$arCollections = CMedialibCollection::GetList(Array('arFilter' => Array('PARENT_ID' => 0)));
					foreach ($arCollections as $arCollection)
					{
						$arResult[] = Array('NAME' => '. '.$arCollection['NAME'], 'ID' => $arParams['TYPE'].'_fileman_'.$arCollection['ID']);
					}
				}
				
				break;
			}
			case 'fileman_collection':
			{
				if (Bitrix\Main\Loader::includeModule('fileman'))
				{
					CMedialib::Init();
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_ELEMENTS'), 'ID' => $arParams['TYPE'].'_fileman_'.$arParams['COLLECTION_ID'].'_elements');
					$arResult[] = Array('NAME' => Loc::getMessage('ram.watermark_SUBELEMENTS_COLLECTION'), 'ID' => $arParams['TYPE'].'_fileman_'.$arParams['COLLECTION_ID'].'_subelements');
					$arCollections = CMedialibCollection::GetList(Array('arFilter' => Array('PARENT_ID' => $arParams['COLLECTION_ID'])));
					foreach ($arCollections as $arCollection)
					{
						$arResult[] = Array('NAME' => '. '.$arCollection['NAME'], 'ID' => $arParams['TYPE'].'_fileman_'.$arCollection['ID']);
					}
				}
				
				break;
			}
		}
		
		if (!empty($arResult))
		{
			$arResult = array_merge(Array(Array('NAME' => Loc::getMessage('ram.watermark_SELECT'), 'ID' => '')), $arResult);
		}
		
		return $arResult;
	}
	
	public static function adminFilter($FULL_ID)
	{
		$ID = explode('_', $FULL_ID);
		
		$COUNT_ID = count($ID);
		
		$TYPE = $ID[0];
		$MODULE_ID = $ID[1];
		
		$arList = Array();
		$arFields = Array();
		$arResult = Array();
		
		if ($COUNT_ID == 2)
		{
			$arList = \CRamWatermark::adminFilterList(Array('OBJECT' => $MODULE_ID, 'TYPE' => $TYPE));
			
			if (empty($arList))
			{
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_IMAGES'), 'ID' => 'ALL', 'HIDDEN' => 'Y');
			}
		}
		else if ($MODULE_ID === 'iblock' && Bitrix\Main\Loader::includeModule('iblock'))
		{
			$IBLOCK_ID = $ID[2];
			
			if ($COUNT_ID == 3)
			{
				if ($IBLOCK_ID === 'all')
				{
					$arList = \CRamWatermark::adminFilterList(Array('OBJECT' => 'iblock_all', 'TYPE' => $TYPE));
				}
				else
				{
					$arList = \CRamWatermark::adminFilterList(Array('OBJECT' => 'iblock_id', 'IBLOCK_ID' => $IBLOCK_ID, 'TYPE' => $TYPE));
				}
			}
			else if ($COUNT_ID == 4)
			{
				if (is_numeric($ID[3]))
				{
					$arList = \CRamWatermark::adminFilterList(Array('OBJECT' => 'iblock_section', 'IBLOCK_ID' => $IBLOCK_ID, 'SECTION_ID' => $ID[3], 'TYPE' => $TYPE));
				}
			}
			
			if ($ID[count($ID) - 1] === 'elements' || $ID[count($ID) - 1] === 'subelements')
			{
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_PREVIEW'), 'ID' => 'PREVIEW_PICTURE');
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_DETAIL'), 'ID' => 'DETAIL_PICTURE');
				
				if ($IBLOCK_ID === 'all')
				{
					$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_FILE_PROPS'), 'ID' => 'FILE');
				}
				else
				{
					$dbProperties = CIBlockProperty::GetList(Array('name' => 'asc'), Array('IBLOCK_ID' => $IBLOCK_ID, 'PROPERTY_TYPE' => 'F', 'CHECK_PERMISSIONS' => 'N'));
					while ($arProperty = $dbProperties->GetNext())
					{
						$arFields[] = Array('NAME' => $arProperty['NAME'].' ('.$arProperty['CODE'].')', 'ID' => $arProperty['ID']);
					}
				}
			}
			else if ($ID[count($ID) - 1] === 'sections' || $ID[count($ID) - 1] === 'section')
			{
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_PREVIEW'), 'ID' => 'PICTURE');
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_DETAIL'), 'ID' => 'DETAIL_PICTURE');
				
				if ($IBLOCK_ID === 'all')
				{
					$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_FILE_PROPS'), 'ID' => 'FILE');
				}
				else
				{
					$dbProperties = \Bitrix\Main\UserFieldTable::getList(Array('order' => Array('SORT' => 'ASC'), 'filter' => Array('ENTITY_ID' => 'IBLOCK_'.$IBLOCK_ID.'_SECTION', 'USER_TYPE_ID' => 'file')));
					while ($arProperty = $dbProperties->fetch())
					{
						$arPropertyLang = CUserTypeEntity::GetByID($arProperty['ID']);
						
						$arFields[] = Array('NAME' => $arPropertyLang['EDIT_FORM_LABEL'][LANGUAGE_ID].' ('.$arProperty['FIELD_NAME'].')', 'ID' => $arProperty['ID']);
					}
				}
			}
		}
		else if ($MODULE_ID === 'highloadblock')
		{
			if (is_numeric($ID[2]))
			{
				$dbProperties = \Bitrix\Main\UserFieldTable::getList(Array('order' => Array('SORT' => 'ASC'), 'filter' => Array('ENTITY_ID' => 'HLBLOCK_'.$ID[2], 'USER_TYPE_ID' => 'file')));
				while ($arProperty = $dbProperties->fetch())
				{
					$arPropertyLang = CUserTypeEntity::GetByID($arProperty['ID']);
					
					$arFields[] = Array('NAME' => $arPropertyLang['EDIT_FORM_LABEL'][LANGUAGE_ID].' ('.$arProperty['FIELD_NAME'].')', 'ID' => $arProperty['ID']);
				}
			}
			else
			{
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_FILE_PROPS'), 'ID' => 'FILE', 'HIDDEN' => 'Y');
			}
		}
		else if ($MODULE_ID === 'fileman')
		{
			if ($COUNT_ID == 3)
			{
				if (is_numeric($ID[2]))
				{
					$arList = \CRamWatermark::adminFilterList(Array('OBJECT' => 'fileman_collection', 'COLLECTION_ID' => $ID[2], 'TYPE' => $TYPE));
				}
				else
				{
					$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_IMAGES'), 'ID' => 'ALL', 'HIDDEN' => 'Y');
				}
			}
			else
			{
				$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_IMAGES'), 'ID' => 'ALL', 'HIDDEN' => 'Y');
			}
		}
		else
		{
			$arFields[] = Array('NAME' => Loc::getMessage('ram.watermark_ALL_IMAGES'), 'ID' => 'ALL', 'HIDDEN' => 'Y');
		}
		
		if (!empty($arList))
		{
			$arResult['list'] = $arList;
		}
		
		if (!empty($arFields))
		{
			$arResult['fields'] = $arFields;
		}
		
		return $arResult;
	}
	
	public static function showAdminFilter($arrFilters, $type)
	{
		$arModules = \CRamWatermark::adminFilterList(Array('OBJECT' => 'modules', 'TYPE' => $type));
		
		foreach ($arrFilters as $arrFilter)
		{
			echo("<div class='ramwmadmin-filter'><div class='ramwmadmin-filterselects'><div class='ramwmadmin-filterselect'><select onchange='RamWmAdminFilterSelectChange(this);'>");
			
			foreach ($arModules as $moduleID => $arModule)
			{
				$selected = $moduleID === $arrFilter['PATH'][0] ? 'selected' : '';
				
				echo("<option ".($arModule['ID']==-1?"disabled='disabled'":"")." ".$selected." value='".$arModule['ID']."'>".$arModule['NAME']."</option>");
			}
			
			echo("</select></div>");
			
			$htmlFields = '';
			
			foreach ($arrFilter['PATH'] as $k => $part)
			{
				$filterId = implode('_', array_slice($arrFilter['PATH'], 0, $k + 1));
				
				$arrFilterPart = \CRamWatermark::adminFilter($type.'_'.$filterId);
				
				if (isset($arrFilterPart['list']))
				{
					$selectedId = $type.'_'.$filterId.'_'.$arrFilter['PATH'][$k + 1];
					
					if ($k == 1 && count($arrFilter['PATH']) == 4 && $arrFilter['PATH'][0] === 'iblock' && Bitrix\Main\Loader::includeModule('iblock'))
					{
						$arSectionFullPath = Array();
						
						$sectionPath = CIBlockSection::GetNavChain($arrFilter['PATH'][1], $arrFilter['PATH'][2], Array('ID'));
						while ($arSectionPath = $sectionPath -> Fetch())
						{
							$arSectionFullPath[] = $arSectionPath['ID'];
						}

						$arSectionFullPath = array_slice($arSectionFullPath, 0, count($arSectionFullPath) - 1);
						
						foreach ($arSectionFullPath as $sectionPathId)
						{
							echo("<div class='ramwmadmin-filterselect'><select onchange='RamWmAdminFilterSelectChange(this);'>");
							
							foreach ($arrFilterPart['list'] as $arItem)
							{
								$selected = $arItem['ID'] === $type.'_'.$filterId.'_'.$sectionPathId ? 'selected' : '';
								
								echo("<option ".$selected." value='".$arItem['ID']."'>".$arItem['NAME']."</option>");
							}
							
							echo("</select></div>");
							
							$arrFilterPart = \CRamWatermark::adminFilter($type.'_'.$filterId.'_'.$sectionPathId);
						}
					}
					else if ($k == 0 && count($arrFilter['PATH']) == 3 && $arrFilter['PATH'][0] === 'fileman')
					{
						$arCollections = Array();
						
						$arCollection = CMedialibCollection::GetList(Array('arFilter' => Array('ID' => $arrFilter['PATH'][$k + 1])));
						
						while ($arCollection[0]['PARENT_ID'])
						{
							$arCollection = CMedialibCollection::GetList(Array('arFilter' => Array('ID' => $arCollection[0]['PARENT_ID'])));
							
							array_unshift($arCollections, $arCollection[0]['ID']);
						}
						
						foreach ($arCollections as $collectionId)
						{
							echo("<div class='ramwmadmin-filterselect'><select onchange='RamWmAdminFilterSelectChange(this);'>");
							
							foreach ($arrFilterPart['list'] as $arItem)
							{
								$selected = $arItem['ID'] === $type.'_'.$filterId.'_'.$collectionId ? 'selected' : '';
								
								echo("<option ".$selected." value='".$arItem['ID']."'>".$arItem['NAME']."</option>");
							}
							
							echo("</select></div>");
							
							$arrFilterPart = \CRamWatermark::adminFilter($type.'_'.$filterId.'_'.$collectionId);
						}
					}
					
					echo("<div class='ramwmadmin-filterselect'><select onchange='RamWmAdminFilterSelectChange(this);'>");
					
					foreach ($arrFilterPart['list'] as $arItem)
					{
						$selected = $arItem['ID'] === $selectedId ? 'selected' : '';
						
						echo("<option ".$selected." ".($arItem['ID']==-1?"disabled='disabled'":"")." value='".$arItem['ID']."'>".$arItem['NAME']."</option>");
					}
					
					echo("</select></div>");
				}
				
				if (isset($arrFilterPart['fields']))
				{
					foreach ($arrFilterPart['fields'] as $arItem)
					{
						if ($arItem['HIDDEN'] === 'Y')
						{
							$htmlFields .= '<div class="ramwmadmin-filterfield hidden"><input type="checkbox" checked="checked" name="FILTERS['.$type.'_'.$filterId.'][]" value="'.$arItem['ID'].'"/>'.$arItem['NAME'].'</div>';
						}
						else
						{
							$checked = !empty($arrFilter['FIELDS']) && in_array($arItem['ID'], $arrFilter['FIELDS']) ? 'checked="checked"' : '';
							
							$htmlFields .= '<label class="ramwmadmin-filterfield"><input type="checkbox" '.$checked.' name="FILTERS['.$type.'_'.$filterId.'][]" value="'.$arItem['ID'].'"/>'.$arItem['NAME'].'</label>';
						}
					}
				}
			}
			
			echo("</div><div class='ramwmadmin-filterfields'>".$htmlFields."</div><div class='ramwmadmin-filterdelete' onclick='RamWmAdminFilterDelete(this);' title='".Loc::getMessage('ram.watermark_DELETE_FILTER')."'></div><div class='ramwmadmin-filtercopy' onclick='RamWmAdminFilterCopy(this);' title='".Loc::getMessage('ram.watermark_COPY_FILTER')."'></div></div>");
		}
	}
	
	public static function showAdminParam($paramCode, $paramData)
	{
		$uploadDir = \CRamWatermark::GetUploadDir();
		$paramGroup = isset($paramData['GROUP']) && !empty($paramData['GROUP']) ? ' ramwmadmin-paramgroup_'.implode(' ramwmadmin-paramgroup_', $paramData['GROUP']) : '';
		
		echo("<div class='ramwmadmin-param ramwmadmin-param_".$paramData['TYPE'].$paramGroup."'><div class='ramwmadmin-paramtitle'>");
		
		if ($paramData['HINT'])
		{
			echo("<div class='ramwmadmin-paramhint'>".$paramData['TITLE']."<div class='ramwmadmin-paramhintvalue'><div>".$paramData['HINT']."</div></div></div>");
		}
		else
		{
			echo($paramData['TITLE']);
		}
		
		echo("</div><div class='ramwmadmin-parambody' onmouseout='".$paramData['OUT']."' onmouseover='".$paramData['OVER']."'>");
		
		switch ($paramData['TYPE'])
		{
			case 'value':
			{
				echo($paramData['VALUE']."<input type='hidden' name='".$paramCode."' value='".$paramData['VALUE']."' />");
				
				break;
			}
			case 'text':
			{
				echo("<input type='text' name='".$paramCode."' onchange='".$paramData['EVENT']."' value='".$paramData['VALUE']."' />");
				
				break;
			}
			case 'textarea':
			{
				echo("<textarea name='".$paramCode."' oninput='".$paramData['EVENT']."' >".$paramData['VALUE']."</textarea>");
				
				break;
			}
			case 'checkbox':
			{
				echo("<input type='hidden' name='".$paramCode."' value='N'/><input type='checkbox' name='".$paramCode."' value='Y' onchange='".$paramData['EVENT']."' ".($paramData['VALUE'] === 'Y'?'checked':'')." />");
				
				break;
			}
			case 'number':
			{
				if (!$paramData['VALUE'])
				{
					$paramData['VALUE'] = 0;
				}
				
				echo("<div class='ramwmadmin-range'><input type='range' name='".$paramCode."' oninput='".$paramData['EVENT']."' min='".$paramData['MIN']."' max='".$paramData['MAX']."' step='".$paramData['STEP']."' value='".$paramData['VALUE']."'/><span class='ramwmadmin-rangemin'>".$paramData['MIN']."</span><span class='ramwmadmin-rangemax'>".$paramData['MAX']."</span></div><input type='text' value='".$paramData['VALUE']."' name='range'/>");
				
				break;
			}
			case 'select':
			{
				if ($paramData["MULTIPLE"] === "Y")
				{
					echo("<select multiple name='".$paramCode."[]' onchange='".$paramData['EVENT']."'>");
				
					foreach ($paramData['VALUES'] as $v => $t)
					{
						echo("<option value='".$v."' ".(in_array($v, $paramData['VALUE'])?'selected':'').">".$t."</option>");
					}
					
					echo("</select>");
				}
				else
				{
					echo("<select name='".$paramCode."' onchange='".$paramData['EVENT']."'>");
				
					foreach ($paramData['VALUES'] as $v => $t)
					{
						echo("<option value='".$v."' ".($v == $paramData['VALUE']?'selected':'').">".$t."</option>");
					}
					
					echo("</select>");
				}
				
				break;
			}
			case 'color':
			{
				if (!$paramData['VALUE'])
				{
					$paramData['VALUE'] = "#FFFFFF";
				}
				
				echo("<label class='ramwmadmin-color' style='background-color: ".$paramData['VALUE'].";'><input type='text' name='".$paramCode."' onchange='".$paramData['EVENT']."' value='".$paramData['VALUE']."'/></label>");
				
				break;
			}
			case 'font':
			{
				echo("<div class='ramwmadmin-fonts'>");
				
				foreach ($paramData['VALUES'] as $fontValue => $fontData)
				{
					echo("<label title='".$fontData['title']."'><input type='radio' ".($fontValue===$paramData['VALUE']?'checked':'')." name='".$paramCode."' onchange='".$paramData['EVENT']."' value='".$fontValue."' /><span><img height='16' src='".$fontData['src']."'/></span><div style='font-family: \"".$fontData['name']."\";'>1</div><style type=\"text/css\">@font-face {font-family: '".$fontData['name']."'; src: url('/".$uploadDir."/ram.watermark/fonts/".$fontValue."'); font-weight: normal;}</style></label>");
				}
				
				echo("</div><div class='ramwmadmin-fontupload'><input id='ramwmadmin-fontuploadinput' type='file' onchange='RamWmAdminUploadFont(this);'/></div>");
				
				break;
			}
			case 'image':
			{
				echo("<div class='ramwmadmin-images'>");
				
				foreach ($paramData['VALUES'] as $imageValue => $imageData)
				{
					$image = new \CRamWatermarkImage();
					$image->from($_SERVER['DOCUMENT_ROOT'].$imageData['src']);
					$image->resize(Array('width' => 100, 'height' => 100, 'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL));
					if ($image->luminance() >= 127)
					{
						echo("<label title='".$imageData['title']."'><input type='radio' ".($imageValue===$paramData['VALUE']?'checked':'')." name='".$paramCode."' onchange='".$paramData['EVENT']."' value='".$imageValue."' /><span class='dark'><img src='".$imageData['src']."'/></span></label>");
					}
					else
					{
						echo("<label title='".$imageData['title']."'><input type='radio' ".($imageValue===$paramData['VALUE']?'checked':'')." name='".$paramCode."' onchange='".$paramData['EVENT']."' value='".$imageValue."' /><span><img src='".$imageData['src']."'/></span></label>");
					}
				}
				
				echo("</div><div class='ramwmadmin-imageupload'><input id='ramwmadmin-imageuploadinput' type='file' onchange='RamWmAdminUploadImage(this);'/></div>");
				
				break;
			}
			case 'date':
			{
				echo("<div class='ramwmadmin-date'><input type='text' value='".$paramData['VALUE']."' name='".$paramCode."' onchange='".$paramData['EVENT']."' onclick='BX.calendar({node: this, field: this});'></div>");
			}
		}
		
		echo("</div><div class='ramwmadmin-parammeasure'>".$paramData['MEASURE']."</div></div>");
	}
	
	public static function CheckExpireDate()
	{
		$notifyExists = \Bitrix\Main\Config\Option::get("ram.watermark", "expire_notify_exists", "N");
		$stableVersionsOnly = \Bitrix\Main\Config\Option::get("main", "stable_versions_only", "Y");
		
		$moduleInfo = CModule::CreateModuleObject("ram.watermark");
		$moduleDemo = defined(str_replace(".", "_", "ram.watermark")."_DEMO") ? "Y" : "N";
		
		if ($moduleDemo !== "Y")
		{
			$updatesList = CUpdateClientPartner::GetUpdatesList($errorMessage, LANGUAGE_ID, $stableVersionsOnly, Array("ram.watermark"), Array("fullmoduleinfo" => "Y"));
			
			foreach ($updatesList["MODULE"] as $k => $v)
			{
				if ($v["@"]["ID"] === "ram.watermark")
				{
					if ($v["@"]["UPDATE_END"] === "Y" && $notifyExists !== "Y")
					{
						$notifyList = CAdminNotify::GetList(Array("ID" => "DESC"), Array("MODULE_ID" => "ram.watermark", "TAG" => "EXPIRE"));
						
						if (!$notify = $notifyList->Fetch())
						{
							CAdminNotify::Add
							(
								Array
								(
									"MESSAGE" => Loc::getMessage("ram.watermark_EXPIRE"),
									"MODULE_ID" => "ram.watermark",
									"TAG" => "EXPIRE",
								)
							);
							
							\Bitrix\Main\Config\Option::set("ram.watermark", "expire_notify_exists", "Y");
						}
					}
					
					break;
				}
			}
		}
		
		return "CRamWatermark::CheckExpireDate();";
	}
}
?>