<?
IncludeModuleLangFile(__FILE__);
Class ram_watermark extends CModule
{
	const MODULE_ID = 'ram.watermark';
	var $MODULE_ID = 'ram.watermark'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("ram.watermark_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("ram.watermark_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("ram.watermark_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("ram.watermark_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		global $DB, $APPLICATION;

		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::MODULE_ID."/install/db/".strtolower($DB->type)."/install.sql");

		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
		
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $APPLICATION, $DB, $errors;

		$this->errors = false;

		$this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/".self::MODULE_ID."/install/db/".strtolower($DB->type)."/uninstall.sql");

		if (!empty($this->errors))
		{
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
		
		return true;
	}

	function InstallEvents()
	{
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CRamWatermark', 'OnBuildGlobalMenu', 100);
		RegisterModuleDependences('main', 'OnFileDelete', self::MODULE_ID, 'CRamWatermark', 'OnFileDelete', 100);
		RegisterModuleDependences('main', 'OnAfterFileSave', self::MODULE_ID, 'CRamWatermark', 'OnAfterFileSave', 100);
		RegisterModuleDependences('main', 'OnBeforeResizeImage', self::MODULE_ID, 'CRamWatermark', 'OnBeforeResizeImage', 10);
		RegisterModuleDependences('main', 'OnAfterResizeImage', self::MODULE_ID, 'CRamWatermark', 'OnAfterResizeImage', 10);
		RegisterModuleDependences('main', 'OnGetFileSRC', self::MODULE_ID, 'CRamWatermark', 'OnGetFileSRC', 10);
		RegisterModuleDependences('main', 'OnEndBufferContent', self::MODULE_ID, 'CRamWatermark', 'OnEndBufferContent', 10);
		
		return true;
	}

	function UnInstallEvents()
	{
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CRamWatermark', 'OnBuildGlobalMenu');
		UnRegisterModuleDependences('main', 'OnFileDelete', self::MODULE_ID, 'CRamWatermark', 'OnFileDelete');
		UnRegisterModuleDependences('main', 'OnAfterFileSave', self::MODULE_ID, 'CRamWatermark', 'OnAfterFileSave');
		UnRegisterModuleDependences('main', 'OnBeforeResizeImage', self::MODULE_ID, 'CRamWatermark', 'OnBeforeResizeImage');
		UnRegisterModuleDependences('main', 'OnAfterResizeImage', self::MODULE_ID, 'CRamWatermark', 'OnAfterResizeImage');
		UnRegisterModuleDependences('main', 'OnGetFileSRC', self::MODULE_ID, 'CRamWatermark', 'OnGetFileSRC');
		UnRegisterModuleDependences('main', 'OnEndBufferContent', self::MODULE_ID, 'CRamWatermark', 'OnEndBufferContent');
		
		return true;
	}

	function InstallFiles($arParams = array())
	{
		$uploadDir = \Bitrix\Main\Config\Option::get("main", "upload_dir", "upload");
		
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/panel", $_SERVER["DOCUMENT_ROOT"]."/bitrix/panel", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/upload", $_SERVER["DOCUMENT_ROOT"]."/".$uploadDir, true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/fonts", $_SERVER["DOCUMENT_ROOT"]."/".$uploadDir."/".self::MODULE_ID."/fonts");
		
		\Bitrix\Main\Config\Option::set(self::MODULE_ID, "exclude_files", "/bitrix/admin/1c_exchange.php\n");
		
		$dbconn = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/dbconn.php";
		$dbconn_backup = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/dbconn.backup.php";
		@copy($dbconn, $dbconn_backup);
		if (file_exists($dbconn) && file_exists($dbconn_backup))
		{
			$dbconn_content = file_get_contents($dbconn);
			$dbconn_content = str_replace(Array("define(\"CACHED_b_file\", 3600);", "define(\"CACHED_b_file_bucket_size\", 10);"), Array("define(\"CACHED_b_file\", 0);", "define(\"CACHED_b_file_bucket_size\", 1);"), $dbconn_content);
			file_put_contents($dbconn, $dbconn_content);
		}
		
		return true;
	}

	function UnInstallFiles()
	{
		$uploadDir = \Bitrix\Main\Config\Option::get("main", "upload_dir", "upload");
		
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools");
		
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"]."/bitrix/panel/".self::MODULE_ID);
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"]."/".$uploadDir."/".self::MODULE_ID);
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"]."/".$uploadDir."/resize_cache/".self::MODULE_ID);
		
		\Bitrix\Main\Config\Option::delete(self::MODULE_ID);
		
		$dbconn = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/dbconn.php";
		$dbconn_backup = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/dbconn.backup2.php";
		@copy($dbconn, $dbconn_backup);
		if (file_exists($dbconn) && file_exists($dbconn_backup))
		{
			$dbconn_content = file_get_contents($dbconn);
			$dbconn_content = str_replace(Array("define(\"CACHED_b_file\", 0);", "define(\"CACHED_b_file_bucket_size\", 1);"), Array("define(\"CACHED_b_file\", 3600);", "define(\"CACHED_b_file_bucket_size\", 10);"), $dbconn_content);
			file_put_contents($dbconn, $dbconn_content);
		}
		
		return true;
	}

	function DoInstall()
	{
		$this->InstallFiles();
		$this->InstallDB();
		$this->InstallEvents();
		RegisterModule(self::MODULE_ID);
		
		$agent = CAgent::GetList(Array(), Array("NAME" => "CRamWatermark::CheckExpireDate();", "MODULE_ID" => "ram.watermark"))->Fetch();
		if (!$agent)
		{
			CAgent::Add(Array("NAME" => "CRamWatermark::CheckExpireDate();", "MODULE_ID" => "ram.watermark"));
		}
	}

	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION, $step;
		$version1 = \Bitrix\Main\Application::getConnection()->query('SHOW COLUMNS FROM ram_watermark_image LIKE "STATUS"')->fetch();
		$version1 = !empty($version1);
		$step = IntVal($step);
		
		if ($step < 2 && $version1)
		{
			$this->UnInstallEvents();
			$APPLICATION->IncludeAdminFile(GetMessage("ram.watermark_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/".self::MODULE_ID."/v1/uninstall.php");
		}
		else
		{
			CAgent::RemoveModuleAgents('ram.watermark');
			$this->UnInstallEvents();
			UnRegisterModule(self::MODULE_ID);
			$this->UnInstallDB();
			$this->UnInstallFiles();
		}
	}
}
?>
