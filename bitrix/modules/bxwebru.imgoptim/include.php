<?
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

//Зарегистрируем дополнительные классы
Loader::registerAutoLoadClasses(
	"bxwebru.imgoptim",
	array(
		'bxwebru\ImgOptim\Test' => 'lib/Test.php',
		'bxwebru\ImgOptim\Main' => 'lib/Main.php',
	)
);