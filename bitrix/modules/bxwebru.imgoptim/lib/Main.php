<?php

namespace bxwebru\ImgOptim;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;

class Main
{

	/**
	 * ������ ������� mime_content_type
	 *
	 * @param $filename string ��� �����
	 * @return bool|string ��� MIME ��� false
	 */
	function mimeContentType($filename)
	{
		$result = new \finfo();

		if (is_resource($result) === true)
		{
			return $result->file($filename, FILEINFO_MIME_TYPE);
		}

		return false;
	}

	/**
	 * ������������ CURLFile �� ���� � �����
	 *
	 * @param $file string ���� � ����� ��� ��������
	 * @return \CURLFile ������ CURLFile
	 */
	private function makeCurlFile($file)
	{
		$info = pathinfo($file);
		$name = $info['basename'];
		$output = new \CURLFile($file, self::mimeContentType($file), $name);
		return $output;
	}

	/**
	 * @param $file string ���� � ����� ��� ��������
	 * @return array ������ ������ �������
	 * @throws \Bitrix\Main\ArgumentNullException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	private function sendFile($file)
	{
		$module_id = pathinfo(dirname(__DIR__))["basename"];

		$secretString = randString(10);

		$ch = curl_init("https://imgoptim.bxweb.ru/worker/upload.php");
		$data = array(
			'file' => self::makeCurlFile($file),
			'secret' => $secretString,
			'png_compress_rate' => Option::get($module_id, "png_compress_rate", "3"),
			'png_to_webp' => Option::get($module_id, "png_to_webp", "N"),
			'jpg_progressive' => Option::get($module_id, "jpg_progressive", "Y"),
			'jpg_quality' => Option::get($module_id, "jpg_quality", "80"),
			'jpg_to_webp' => Option::get($module_id, "jpg_to_webp", "N")

		);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // stop verifying certificate
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // if any redirection after upload
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$resultJSON = curl_exec($ch);
		curl_close($ch);

		$result = json_decode($resultJSON, true);
		$result["SECRET"] = $secretString;
		return $result;
	}

	/**
	 * @param $fileID int ������������� �����
	 * @param $secretString string �������� �����
	 * @param $localFileName string ��������� ���� � �����
	 * @param string $getWebp string ����������� � ������� webp
	 * @return void ������ ������ �������
	 */
	private function getFile($fileID, $secretString, $localFileName, $getWebp = "N")
	{
		$ch = curl_init("https://imgoptim.bxweb.ru/worker/download.php");

		//�������� ��� �����, ���� �������� WEBP
		if ($getWebp == "Y"):
			$arLocalFile = pathinfo($localFileName);
			$localFileName = "{$arLocalFile['dirname']}/{$arLocalFile['filename']}.webp";
		endif;

		$localFile = fopen($localFileName, 'wb');
		$data = array(
			'file_id' => $fileID,
			'secret' => $secretString,
			'get_webp' => $getWebp
		);
		curl_setopt($ch, CURLOPT_FILE, $localFile);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // stop verifying certificate
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // if any redirection after upload
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_exec($ch);
		curl_close($ch);

		fclose($localFile);

//		return json_decode($resultJSON, true);
	}

	/**
	 * ����������� ����������� ��� �������
	 *
	 * @param $arFile array ������ ���������� � �����
	 * @param $arResizeParams
	 * @param $callbackData
	 * @param $cacheImageFile
	 * @param $cacheImageFileTmp string ���� � ����������� �����
	 * @param $arImageSize
	 * @throws \Bitrix\Main\ArgumentNullException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function onResize($arFile, $arResizeParams, &$callbackData, &$cacheImageFile, &$cacheImageFileTmp, &$arImageSize)
	{
		$module_id = pathinfo(dirname(__DIR__))["basename"];
		$wasOptimized = "N";

		//���� ��������� ����������� ��� �������
		if (Option::get($module_id, "on_resize", "Y") == "Y"):

			switch ($arFile["CONTENT_TYPE"]):
				case "image/png":

					//���� ������������ PNG ��������
					if ((Option::get($module_id, "local_png_optim", "Y") == "Y") && (Test::localPngOptim() || Test::localPngquantOptim())):

						//������� ����� webp, ���� ���������
						if ((Option::get($module_id, "png_to_webp", "N") == "Y") && (Test::localWebpOptim())):
							$arWebpFile = pathinfo($cacheImageFileTmp);
							$webpFile = "{$arWebpFile["dirname"]}/{$arWebpFile["filename"]}.webp";
							exec("cwebp -q 85 {$cacheImageFileTmp} -o {$webpFile}");
						endif;

						//��������� ����������� png
						if (Test::localPngquantOptim()):
							//���������� pngquant
							exec("pngquant --force --output {$cacheImageFileTmp} {$cacheImageFileTmp}");
							$wasOptimized = "Y";
						else:
							//����� ���������� optipng
							$pngCompressRate = Option::get($module_id, "png_compress_rate", "3");
							exec("optipng -quiet -o{$pngCompressRate} " . $cacheImageFileTmp);
							$wasOptimized = "Y";
						endif;

					endif;

					//���� PNG �� ������������� - ������������ ��������
					if ($wasOptimized !== "Y"):
						if ((Option::get($module_id, "remote_png_optim", "Y") == "Y") && (Test::remoteOptim())):
							$arUploadResult = self::sendFile($cacheImageFileTmp);

							if ($arUploadResult["STATUS"] == "OK"):
								//������� ��������������� ����
								self::getFile($arUploadResult["ID"], $arUploadResult["SECRET"], $cacheImageFileTmp);

								//������� ����� � ������� WEBP
								if (Option::get($module_id, "png_to_webp", "N") == "Y")
									self::getFile($arUploadResult["ID"], $arUploadResult["SECRET"], $cacheImageFileTmp, "Y");
							endif;
						endif;
					endif;

					break;

				case "image/jpeg":

					//���� ������������ JPG ��������
					if ((Option::get($module_id, "local_jpg_optim", "Y") == "Y") && (Test::localJpgOptim())):

						//������� ����� webp, ���� ���������
						if ((Option::get($module_id, "jpg_to_webp", "N") == "Y") && (Test::localWebpOptim())):
							$arWebpFile = pathinfo($cacheImageFileTmp);
							$webpFile = "{$arWebpFile["dirname"]}/{$arWebpFile["filename"]}.webp";
							exec("cwebp -q 85 {$cacheImageFileTmp} -o {$webpFile}");
						endif;

						//��������� ����������� jpg
						$jpgQuality = Option::get($module_id, "jpg_quality", "80");
						$jpgProgressive = "";
						if (Option::get($module_id, "jpg_progressive", "Y") == "Y") $jpgProgressive = "--all-progressive";
						exec("jpegoptim -m{$jpgQuality} -s -q {$jpgProgressive} " . $cacheImageFileTmp);

						$wasOptimized = "Y";
					endif;

					//���� JPG �� ������������� - ������������ ��������
					if ($wasOptimized !== "Y"):
						if ((Option::get($module_id, "remote_jpg_optim", "Y") == "Y") && (Test::remoteOptim())):
							$arUploadResult = self::sendFile($cacheImageFileTmp);

							if ($arUploadResult["STATUS"] == "OK"):
								//������� ���������������� ����
								self::getFile($arUploadResult["ID"], $arUploadResult["SECRET"], $cacheImageFileTmp);

								//������� ����� � ������� WEBP
								if (Option::get($module_id, "jpg_to_webp", "N") == "Y")
									self::getFile($arUploadResult["ID"], $arUploadResult["SECRET"], $cacheImageFileTmp, "Y");
							endif;
						endif;
					endif;

					break;
			endswitch;

		endif;


	}

	/**
	 * Callback-������� ������ ����� ����� �� ���� � ������� WEBP
	 *
	 * @param $arMatches
	 * @return mixed
	 */
	function replaceImageToWeb($arMatches)
	{
		$fileInfo = $_SERVER["DOCUMENT_ROOT"] . $arMatches[1] . "/resize_cache/" . $arMatches[2] . "/" . $arMatches[3] . $arMatches[4] . "webp";
		$result = $arMatches[0];

		//���� ���� ������
		if (file_exists($fileInfo))
			$result = str_replace($arMatches[3] . $arMatches[4] . $arMatches[5], $arMatches[3] . $arMatches[4] . "webp", $arMatches[0]);

		return $result;
	}

	/**
	 * ������ ���� ���������� ������ �� WEBP
	 *
	 * @param $content
	 * @throws \Bitrix\Main\ArgumentNullException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function onGeneratePage(&$content)
	{
		$module_id = pathinfo(dirname(__DIR__))["basename"];

		if (Option::get($module_id, "replace_to_webp", "N") == "Y")
			$content = preg_replace_callback(
				'/src=["|\'](\S*)\/resize_cache\/(\S*)\/(\S*)(.)(jpg|png)["|\']/im',
				'self::replaceImageToWeb',
				$content
			);

	}

	/**
	 * ����������� ������� WebPJS
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentNullException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function appendWebpjsScript()
	{
		$module_id = pathinfo(dirname(__DIR__))["basename"];

		if (Option::get($module_id, "use_webpjs", "N") == "Y"):
			Asset::getInstance()->addJs("/bitrix/js/" . $module_id . "/polyfills.js");
			Asset::getInstance()->addJs("/bitrix/js/" . $module_id . "/webp-hero.js");
			Asset::getInstance()->addJs("/bitrix/js/" . $module_id . "/webp-worker.js");
		endif;
		return false;
	}

}