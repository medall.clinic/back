<?php

namespace bxwebru\ImgOptim;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class Test
{

	/**
	 * �������� ����������� ��������� ����������� PNG
	 *
	 * @return bool
	 */
	public static function localPngOptim()
	{

		exec('optipng -v', $s);
		return ($s ? true : false);
	}

	/**
	 * �������� ����������� ��������� ����������� PNG (pngquant)
	 *
	 * @return bool
	 */
	public static function localPngquantOptim()
	{

		exec('pngquant -h', $s);
		return ($s ? true : false);
	}

	/**
	 * �������� ����������� ��������� ����������� JPG
	 *
	 * @return bool
	 */
	public static function localJpgOptim()
	{

		exec('jpegoptim --version', $s);
		return ($s ? true : false);
	}

	/**
	 * �������� ����������� ��������� ����������� WEBP
	 *
	 * @return bool
	 */
	public static function localWebpOptim()
	{

		exec('cwebp -version', $s);
		return ($s ? true : false);
	}


	/**
	 * �������� ����������� ��������� �����������
	 *
	 * @return bool
	 */
	public static function remoteOptim()
	{
		if (in_array('curl', get_loaded_extensions())):
			return true;
		else:
			return false;
		endif;
	}


}