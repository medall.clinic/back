<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

//��������� �������� �����
Loc::loadMessages(__FILE__);


class bxwebru_imgoptim extends CModule
{
	var $MODULE_ID = 'bxwebru.imgoptim';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_GROUP_RIGHTS = "Y";
	var $PARTNER_NAME = 'bXweb.ru';
	var $PARTNER_URI = 'https://bxweb.ru/';

	/**
	 * bxwebru_imgoptim constructor.
	 * ������� ���������� � ������
	 */
	public function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");;

		//$this->MODULE_ID = str_replace("_", ".", get_class($this));
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("BXWEB_IMGOPTIM_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("BXWEB_IMGOPTIM_DESCRIPTION");
		$this->PARTNER_NAME = 'bXweb.ru';
		$this->PARTNER_URI = 'https://bxweb.ru/';

		return false;
	}

	/**
	 * ���������� �������� ���������
	 *
	 * @return bool
	 */
	public function DoInstall()
	{

		global $APPLICATION;

		if (CheckVersion(ModuleManager::getVersion("main"), "14.00.00"))
		{

			$this->InstallFiles();
			$this->InstallDB();

			ModuleManager::registerModule($this->MODULE_ID);

			$this->InstallEvents();
		} else
		{

			$APPLICATION->ThrowException(
				Loc::getMessage("BXWEB_IMGOPTIM_INSTALL_ERROR_VERSION")
			);
		}

		$APPLICATION->IncludeAdminFile(
			Loc::getMessage("BXWEB_IMGOPTIM_INSTALL_TITLE") . " \"" . Loc::getMessage("BXWEB_IMGOPTIM_NAME") . "\"",
			__DIR__ . "/step.php"
		);

		return false;
	}

	/**
	 * ����������� ������ � �������
	 *
	 * @return bool
	 */
	public function InstallFiles()
	{
		CopyDirFiles(
			__DIR__ . "/assets/scripts",
			Application::getDocumentRoot() . "/bitrix/js/" . $this->MODULE_ID . "/",
			true,
			true
		);

		return false;
	}

	/**
	 * �������� ������ � ��
	 *
	 * @return bool
	 */
	public function InstallDB()
	{
		return false;
	}


	/**
	 * ����������� �������
	 *
	 * @return bool
	 */
	public function InstallEvents()
	{

		//��� ��������� ������� �����������
		EventManager::getInstance()->registerEventHandler(
			"main",
			"OnAfterResizeImage",
			$this->MODULE_ID,
			"bxwebru\ImgOptim\Main",
			"onResize"
		);

		//��� ��������� ��������� ��������
		EventManager::getInstance()->registerEventHandler(
			"main",
			"OnEndBufferContent",
			$this->MODULE_ID,
			"bxwebru\ImgOptim\Main",
			"onGeneratePage"
		);

		//����� ���������� ��������� ��������
		EventManager::getInstance()->registerEventHandler(
			"main",
			"OnBeforeEndBufferContent",
			$this->MODULE_ID,
			"bxwebru\ImgOptim\Main",
			"appendWebpjsScript"
		);

		return false;
	}


	/**
	 * ���������� �������� ��������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentNullException
	 */
	public function DoUninstall()
	{

		global $APPLICATION;

		$this->UnInstallFiles();
		$this->UnInstallDB();
		$this->UnInstallEvents();

		ModuleManager::unRegisterModule($this->MODULE_ID);

		$APPLICATION->IncludeAdminFile(
			Loc::getMessage("BXWEB_IMGOPTIM_UNINSTALL_TITLE") . " \"" . Loc::getMessage("BXWEB_IMGOPTIM_NAME") . "\"",
			__DIR__ . "/unstep.php"
		);

		return false;
	}

	/**
	 * �������� ���������� ������
	 *
	 * @return bool
	 */
	public function UnInstallFiles()
	{
		Directory::deleteDirectory(
			Application::getDocumentRoot() . "/bitrix/js/" . $this->MODULE_ID
		);

		return false;
	}

	/**
	 * �������� ������ �� ��
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentNullException
	 */
	public function UnInstallDB()
	{
		Option::delete($this->MODULE_ID);

		return false;
	}


	/**
	 * �������� ����������� �������
	 *
	 * @return bool
	 */
	public function UnInstallEvents()
	{

		//��� ��������� ������� �����������
		EventManager::getInstance()->unRegisterEventHandler(
			"main",
			"OnAfterResizeImage",
			$this->MODULE_ID,
			"bxwebru\ImgOptim\Main",
			"onResize"
		);


		//��� ��������� ��������� ��������
		EventManager::getInstance()->unRegisterEventHandler(
			"main",
			"OnEndBufferContent",
			$this->MODULE_ID,
			"bxwebru\ImgOptim\Main",
			"onGeneratePage"
		);


		//����� ���������� ��������� ��������
		EventManager::getInstance()->unRegisterEventHandler(
			"main",
			"OnBeforeEndBufferContent",
			$this->MODULE_ID,
			"bxwebru\ImgOptim\Main",
			"appendWebpjsScript"
		);

		return false;
	}
}