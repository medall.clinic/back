<?
$MESS["BXWEB_IMGOPTIM_NAME"] = "Универсальный оптимизатор изображений";
$MESS["BXWEB_IMGOPTIM_DESCRIPTION"] = "Выполняет оптимизацию и сжатие изображений без потери качества. Работает даже на виртуальном хостинге";
$MESS["BXWEB_IMGOPTIM_PARTNER_NAME"] = "bXweb.ru";
$MESS["BXWEB_IMGOPTIM_PARTNER_URI"] = "https://bxweb.ru/";
$MESS["BXWEB_IMGOPTIM_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["BXWEB_IMGOPTIM_INSTALL_TITLE"] = "Установка модуля";
$MESS["BXWEB_IMGOPTIM_UNINSTALL_TITLE"] = "Деинсталляция модуля";