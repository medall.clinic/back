<?
$MESS["BXWEB_IMGOPTIM_OPTIONS_LOCAL_PNG_ERROR"] = "Для использования локальной оптимизация PNG необходимо установить пакет pngquant (https://pngquant.org/) или пакет optipng (http://optipng.sourceforge.net)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_LOCAL_JPG_ERROR"] = "Для использования локальной оптимизация JPEG необходимо установить пакет jpegoptim (http://freshmeat.sourceforge.net/projects/jpegoptim)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_REMOTE_ERROR"] = "Невозможна оптимизация на удаленном сервере. Включите библиотеку CURL для PHP";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_NAME"] = "Настройка параметров модуля";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_HEADER_LOCAL_PNG"] = "Настройки оптимизации для PNG";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_LOCAL_PNG_OPTIM"] = "Оптимизировать локально";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_REMOTE_PNG_OPTIM"] = "Оптимизировать удаленно";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_PNG_COMPRESS_RATE"] = "Степень сжатия у png-файлов (1-7)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_HEADER_LOCAL_JPG"] = "Настройки оптимизации для JPEG";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_LOCAL_JPG_OPTIM"] = "Оптимизировать локально";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_REMOTE_JPG_OPTIM"] = "Оптимизировать удаленно";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_JPG_QUALITY"] = "Качество jpeg-файлов при сжатии (0-100)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_JPG_PROGRESSIVE"] = "Включить progressive jpeg";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_COMMON"] = "Общие настройки";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_ON_RESIZE"] = "Оптимизация при ресайзе";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_ON_SAVE"] = "Оптимизация при сохранении";
$MESS["BXWEB_IMGOPTIM_OPTIONS_INPUT_APPLY"] = "Сохранить";
$MESS["BXWEB_IMGOPTIM_OPTIONS_INPUT_DEFAULT"] = "По умолчанию";
$MESS["BXWEB_IMGOPTIM_DONATE_HEADER"] = "Поддержать разработчиков";
$MESS["BXWEB_IMGOPTIM_DONATE_1"] = "Вы используете бесплатный модуль разработанный <a href=\"https://bxweb.ru\" class=\"c-badge c-badge--rounded c-badge--ghost c-badge--brand\" target=\"_blank\">bxWeb.ru</a>";
$MESS["BXWEB_IMGOPTIM_DONATE_2"] = "<p>Если вам понравился модуль, вы можете:</p>
	<ul>
		<li>поддержать выпуск обновлений модуля</li>
		<li>поддержать работу сервера оптимизации изображений</li>
		<li>поддержать команду <b>bxWeb.ru</b></li>
	</ul>
	<p><b>Ваша поддержка нам очень важна. Спасибо вам большое!</b></p>";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_REPLACE_WEBP"] = "Заменять изображения на WEBP (в публичной части)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_WEBP_PNG_OPTIM"] = "Создавать копию в формате WEBP";
$MESS["BXWEB_IMGOPTIM_OPTIONS_TAB_WEBP_JPG_OPTIM"] = "Создавать копию в формате WEBP";
$MESS["BXWEB_IMGOPTIM_OPTIONS_LOCAL_WEBP_ERROR"] = "Для использования локальной конвертации изображений в формат WEBP необходимо установить пакет cwebp (https://developers.google.com/speed/webp/docs/cwebp)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_USE_WEBPJS"] = "Подключать webp-hero (поддержка WEBP в несовместимых браузерах)";
$MESS["BXWEB_IMGOPTIM_OPTIONS_USE_WEBPJS_ERROR"] = "Для корректной работы WEBP во всех браузерах рекомендуется подключать webp-hero (https://github.com/chase-moskal/webp-hero/)";
?>