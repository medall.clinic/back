<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);


$arTabs = array(
	array(
		"DIV" => "edit",
		"TAB" => Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_NAME"),
		"TITLE" => Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_NAME"),
		"OPTIONS" => array()
	)
);

//��������� ��������� PNG
$arOptionParam = array(
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_HEADER_LOCAL_PNG")
);

if (\bxwebru\ImgOptim\Test::localPngOptim()):
	//����������� ��������� ��������� PNG
	$arOptionParam[] = array(
		"local_png_optim",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_LOCAL_PNG_OPTIM"),
		"Y",
		array("checkbox")
	);
endif;

if (\bxwebru\ImgOptim\Test::remoteOptim()):
	//����������� ��������� ��������� PNG
	$arOptionParam[] = array(
		"remote_png_optim",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_REMOTE_PNG_OPTIM"),
		"Y",
		array("checkbox")
	);
endif;

$arOptionParam[] = array(
	"png_to_webp",
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_WEBP_PNG_OPTIM"),
	"N",
	array("checkbox")
);

$arOptionParam[] = array(
	"png_compress_rate",
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_PNG_COMPRESS_RATE"),
	"3",
	array("text", 2)
);


$arTabs[0]["OPTIONS"] = array_merge($arTabs[0]["OPTIONS"], $arOptionParam);
//---------------------


//��������� ��������� JPEG
$arOptionParam = array(
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_HEADER_LOCAL_JPG")
);

if (\bxwebru\ImgOptim\Test::localJpgOptim()):
	//����������� ��������� ��������� JPG
	$arOptionParam[] = array(
		"local_jpg_optim",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_LOCAL_JPG_OPTIM"),
		"Y",
		array("checkbox")
	);
endif;

if (\bxwebru\ImgOptim\Test::remoteOptim()):
	//����������� ��������� ��������� JPG
	$arOptionParam[] = array(
		"remote_jpg_optim",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_REMOTE_JPG_OPTIM"),
		"Y",
		array("checkbox")
	);
endif;

$arOptionParam[] = array(
	"jpg_to_webp",
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_WEBP_JPG_OPTIM"),
	"N",
	array("checkbox")
);

$arOptionParam[] = array(
	"jpg_quality",
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_JPG_QUALITY"),
	"80",
	array("text", 2)
);

$arOptionParam[] = array(
	"jpg_progressive",
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_JPG_PROGRESSIVE"),
	"Y",
	array("checkbox")
);

$arTabs[0]["OPTIONS"] = array_merge($arTabs[0]["OPTIONS"], $arOptionParam);
//---------------------


//����� ��������� ���������
$arOptionParam = array(
	Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_COMMON"),
	array(
		"on_resize",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_ON_RESIZE"),
		"Y",
		array("checkbox")
	),
	/*
	array(
		"on_save",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_ON_SAVE"),
		"Y",
		array("checkbox")
	),*/
	array(
		"replace_to_webp",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_TAB_REPLACE_WEBP"),
		"N",
		array("checkbox")
	),
	array(
		"use_webpjs",
		Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_USE_WEBPJS"),
		"N",
		array("checkbox")
	),
);

//������� ���������
$arTabs[0]["OPTIONS"] = array_merge($arTabs[0]["OPTIONS"], $arOptionParam);
//---------------------

$tabControl = new CAdminTabControl(
	"tabControl",
	$arTabs
);
?>

<?
//����������� ��������� ����������� PNG (pngquant)
if (!\bxwebru\ImgOptim\Test::localPngquantOptim())
{
	//����������� ��������� ����������� PNG
	if (!\bxwebru\ImgOptim\Test::localPngOptim())
		$arInfoMessage[]=Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_LOCAL_PNG_ERROR");
}

//����������� ��������� ����������� JPEG
if (!\bxwebru\ImgOptim\Test::localJpgOptim())
	$arInfoMessage[]=Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_LOCAL_JPG_ERROR");

//����������� ��������� ����������� WEBP
if (!\bxwebru\ImgOptim\Test::localWebpOptim())
	$arInfoMessage[]=Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_LOCAL_WEBP_ERROR");

//������������ �� ����������� WebpJS
$arInfoMessage[]=Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_USE_WEBPJS_ERROR");

//���� ���� �������������� ���������
if ($arInfoMessage):
	CAdminMessage::showMessage(array(
		"MESSAGE" => implode($arInfoMessage,"\n\n"),
		"TYPE" => 'OK',
	));
endif;

//����������� ��������� ����������
if (!\bxwebru\ImgOptim\Test::remoteOptim()):
	CAdminMessage::showMessage(array(
		"MESSAGE" => Loc::getMessage("BXWEB_IMGOPTIM_OPTIONS_REMOTE_ERROR"),
		"TYPE" => 'ERROR',
	));
endif;
?>

<?
$tabControl->Begin();
?>


<?
//����������� ����� � �����������
?>
<form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>"
	  method="post">

	<?
	foreach ($arTabs as $aTab)
	{

		if ($aTab["OPTIONS"])
		{

			$tabControl->BeginNextTab();

			__AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
		}
	}
	?>

	<?
	$tabControl->Buttons();
	?>

	<input type="submit" name="apply" value="<? echo(Loc::GetMessage("BXWEB_IMGOPTIM_OPTIONS_INPUT_APPLY")); ?>"
		   class="adm-btn-save"/>
	<input type="submit" name="default" value="<? echo(Loc::GetMessage("BXWEB_IMGOPTIM_OPTIONS_INPUT_DEFAULT")); ?>"/>

	<?
	echo(bitrix_sessid_post());
	?>

</form>

<?
$tabControl->End();
?>

<div>

	<h2><?= Loc::getMessage('BXWEB_IMGOPTIM_DONATE_HEADER'); ?></h2>
	<table>
		<tr>
			<td style="padding-right: 30px;">
				<iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%B4%D0%B4%D0%B5%D1%80%D0%B6%D0%BA%D0%B0%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D1%8B%D1%85%20%D0%BE%D0%B1%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B9%20(imgOptim)&targets-hint=&default-sum=100&button-text=13&payment-type-choice=on&mobile-payment-type-choice=on&hint=&successURL=&quickpay=shop&account=41001321447277"
						width="423" height="250" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
			</td>
			<td>
				<p>
					<?= Loc::getMessage('BXWEB_IMGOPTIM_DONATE_1'); ?>
				</p>
				<?= Loc::getMessage('BXWEB_IMGOPTIM_DONATE_2'); ?>
			</td>
		</tr>
	</table>

</div>

<?
if ($request->isPost() && check_bitrix_sessid())
{

	foreach ($arTabs as $aTab)
	{
		foreach ($aTab["OPTIONS"] as $arOption)
		{

			if (!is_array($arOption))
			{

				continue;
			}

			if ($arOption["note"])
			{

				continue;
			}

			if ($request["apply"])
			{

				$optionValue = $request->getPost($arOption[0]);

				if ($arOption[0] == "switch_on")
				{
					if ($optionValue == "")
					{

						$optionValue = "N";
					}
				}

				//�������� ����������� ����������
				if ($arOption[0] == "png_compress_rate"):
					if (intval($optionValue) < 1) $optionValue = 1;
					if (intval($optionValue) > 7) $optionValue = 7;
				endif;

				if ($arOption[0] == "jpg_quality"):
					if (intval($optionValue) <= 0) $optionValue = 0;
					if (intval($optionValue) > 100) $optionValue = 100;
				endif;

				Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
			} elseif ($request["default"])
			{

				Option::set($module_id, $arOption[0], $arOption[2]);
			}
		}
	}

	LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG);
}
?>
