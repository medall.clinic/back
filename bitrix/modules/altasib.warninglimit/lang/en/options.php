<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Glower                           #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2012 ALTASIB             #
#################################################
?>
<?
$MESS ['ALTASIB_IS'] = "Магазин готовых решений для 1С-Битрикс";

$MESS['ALTASIB_ENABLE_WARNINGLIMIT'] = 'Enable warning window';
$MESS['ALTASIB_ENABLE_WARNINGLIMIT_AUTH'] = '<font color="red">TURN OFF</font> for authorized users';
$MESS ['ALTASIB_WARNINGLIMIT_WIDTH'] = 'Width of window';
$MESS ['ALTASIB_WARNINGLIMIT_TITLE'] = 'Title';
$MESS ['ALTASIB_WARNINGLIMIT_TEXT'] = 'Text of message';
$MESS ['ALTASIB_WARNINGLIMIT_CHECKBOX_TEXT'] = 'Text on the label of checkbox';
$MESS ['ALTASIB_WARNINGLIMIT_ENTER_TEXT'] = 'Text, wich will write after checked';
$MESS ['ALTASIB_WARNINGLIMIT_LOGO'] = 'Way to logo';
$MESS ['ALTASIB_WARNINGLIMIT_LOGO_HEIGHT'] = 'Enter height';
$MESS ['ALTASIB_WARNINGLIMIT_LOGO_WIDTH'] = 'Enter width';
$MESS ['ALTASIB_WARNINGLIMIT_RULE_TEXT'] = 'Message to know the rules';
$MESS ['ALTASIB_WARNINGLIMIT_HREF_TEXT'] = 'Text of link';
$MESS ['ALTASIB_WARNINGLIMIT_RULE_HREF'] = 'Adress of rule page (empty - rules will don\'t show)';
$MESS ['ALTASIB_WARNINGLIMIT_CHECKBOX_URLS'] = 'Show/not show the window for the folowing pages and sections';
$MESS ['ALTASIB_WARNINGLIMIT_URLS'] = 'if the field is empty, the window will show for all pages and sections';
$MESS ['ALTASIB_WARNINGLIMIT_DENIED_TITLE'] = 'Title of page, which will show if user press cancel';
$MESS ['ALTASIB_WARNINGLIMIT_DENIED_TEXT'] = 'Text of page, which will show if user press cancel';

$MESS['ENABLE_WARNINGLIMIT_DEF'] = 'Y';
$MESS['DISABLE_AUTH_DEF'] = 'Y';
$MESS ['WIDTH_DEF'] = '500';
$MESS ['TITLE_DEF'] = 'Warning!';
$MESS ['TEXT_DEF'] = 'Warning content!!';
$MESS ['CHECKBOX_TEXT_DEF'] = 'I\'m 18 year old ';
$MESS ['ENTER_TEXT_DEF'] = 'Enter';
$MESS ['LOGO_DEF'] = '/bitrix/images/altasib.warninglimit/logo.gif';
$MESS ['LOGO_HEIGHT_DEF'] = '30';
$MESS ['LOGO_WIDTH_DEF'] = '';
$MESS ['RULE_TEXT_DEF'] = 'you can know rules by clicking ';
$MESS ['RULE_HREF_DEF'] = '';
$MESS ['HREF_TEXT_DEF'] = 'the link';
$MESS ['CHECKBOX_URLS_DEF'] = 'N';
$MESS ['URLS_DEF'] = '';
$MESS ['DENIED_TITLE_DEF'] = 'Access denied!';
$MESS ['DENIED_TEXT_DEF'] = 'You haven\'t agree with rules!';
?>
