<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Glower                           #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2012 ALTASIB             #
#################################################
?>
<?
$MESS['ALTASIB_WARNINGLIMIT_MODULE_TITLE'] = 'Предупреждение об ограничении доступа';
$MESS['ALTASIB_WARNINGLIMIT_COMMENT'] = 'Содержимое может быть опасно!';
$MESS['ALTASIB_WARNINGLIMIT_CLOSE_TEXT'] = 'закрыть';
?>
