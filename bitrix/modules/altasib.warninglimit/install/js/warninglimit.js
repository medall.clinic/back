$(document).ready(function()
{
    $("body").first().append('<form id="alx_send" method="post" action = "'+alx_warninglimit_cur_page+'">'+
    '<div id = "alx_modal_window">'+
    '<div class="alx_warninglimit_logo"><img src="'+alx_warninglimit_img_logo+'"'+alx_warninglimit_logo_height+alx_warninglimit_logo_width+'/></div>'+
    '<a href=\"'+alx_warninglimit_cur_page+'" id="alx_close" onClick="$(\'#alx_send\').submit(); return false;">'+alx_warninglimit_close_text+'</a>'+
    '<div class="alx_warninglimit_txt"><div class="alx_warninglimit_title">'+alx_warninglimit_title+'</div>'+alx_warninglimit_text+'</div>'+
    '<div class="alx_warninglimit_check"><label><input id = "alx_choice" type = "checkbox" value = "false" onClick = "alx_choice_click()"/>'+alx_warninglimit_checkbox_text+'</label></div>'+
    '<div class="alx_warning_come_in"><a href="" id="alx_accept" style = "display:none" onClick = "return alx_accept_click()">'+alx_warninglimit_enter_text+'</a></div>'+
    '<div class="alx_warning_limit_dop_txt">'+alx_warninglimit_rule_href+'</div><input type = "hidden" name = "alx_access" value = "n" style = "display:none">'+
    '</div></form>');

    $('body').append('<div id="alx_fade"></div>');
    $('#alx_fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();
    $('#alx_modal_window').css({ 'width': alx_warninglimit_width+'px' });

    var HeightScreen = $(window).height();
    var WidthScreen = $(window).width();
    var Height = $('#alx_modal_window').height();
    var Width = $('#alx_modal_window').width();

    $('#alx_modal_window').css('left', (WidthScreen-Width)/2+ 'px');
    $('#alx_modal_window').css('top', (HeightScreen-Height)/2+ 'px');
    $('#alx_modal_window').css({'display':'block'});
    $('#alx_modal_window').fadeIn('slow');

    window.scrollTo(0, 0);
    return false;
});

function alx_accept_click()
{
    document.cookie = "alx_accepted_"+alx_warninglimit_SID+"=Y; ; expires=Wed, 1 Mar 2052 00:00:00; path=/;";
    $('#alx_modal_window, #alx_fade').fadeOut('slow');
    return false;
};

function alx_choice_click()
{
    if($('#alx_choice').prop('checked'))
        $('#alx_accept').fadeIn();
    else
        $('#alx_accept').fadeOut();
};

