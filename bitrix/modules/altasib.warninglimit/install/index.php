<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Glower                           #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2012 ALTASIB             #
#################################################
?>
<?
global $MESS;
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen("/index.php"));
IncludeModuleLangFile(__FILE__);

Class altasib_warninglimit extends CModule
{
        var $MODULE_ID = "altasib.warninglimit";
        var $MODULE_VERSION;
        var $MODULE_VERSION_DATE;
        var $MODULE_NAME;
        var $MODULE_DESCRIPTION;
        var $MODULE_CSS;
        var $PARTNER_NAME;
        var $PARTNER_URI;

        function altasib_warninglimit()
        {
                $arModuleVersion = array();

                $path = str_replace("\\", "/", __FILE__);
                $path = substr($path, 0, strlen($path) - strlen("/index.php"));
                include($path."/version.php");

                if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
                {
                        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
                        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
                }
                else
                {
                        $this->MODULE_VERSION = "1.0";
                        $this->MODULE_VERSION_DATE = "2012-10-18 10:00:00";
                }

                $this->MODULE_NAME = GetMessage("ALTASIB_WARNINGLIMIT_MODULE_NAME");
                $this->MODULE_DESCRIPTION = GetMessage("ALTASIB_WARNINIGLIMIT_MODULE_DESCRIPTION");

                $this->PARTNER_NAME = "ALTASIB";
                $this->PARTNER_URI = "http://www.altasib.ru/";
        }
        function DoInstall()
        {
                global $DB, $APPLICATION, $step;

					$step = IntVal($step);
					if($step < 2)
					{
						$GLOBALS["install_step"] = 1;
						$APPLICATION->IncludeAdminFile(GetMessage("ALTASIB_WARNINGLIMIT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.warninglimit/install/step1.php");
					}
					elseif ($step == 2)
					{
						$this->InstallFiles();
						$this->InstallDB();
					
						$GLOBALS["errors"] = $this->errors;
						$GLOBALS["install_step"] = 2;
						$APPLICATION->IncludeAdminFile(GetMessage("ALTASIB_WARNINGLIMIT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.warninglimit/install/step1.php");
					}
        }
        function DoUninstall()
        {
                global $DB, $APPLICATION, $step;
                $step = IntVal($step);
				if($step < 2)
				{
				    $APPLICATION->IncludeAdminFile(GetMessage("ALTASIB_WARNINGLIMIT_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.warninglimit/install/unstep1.php");
				}
				elseif($step == 2)
				{
					$this->UnInstallDB();
					$this->UnInstallFiles(array(
                                "deleterules" => $_REQUEST["deleterules"],
								"rule" => $_REQUEST["rule"],
                        ));
					$APPLICATION->IncludeAdminFile(GetMessage("ALTASIB_WARNINGLIMIT_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.warninglimit/install/unstep2.php");
				}
        }
        function InstallDB()
        {
                $this->errors = false;

                RegisterModule("altasib.warninglimit");
                RegisterModuleDependences("main","OnProlog","altasib.warninglimit","WARNINGLIMIT_alx","WARNINGLIMITOnProlog", "100");
        }
        function UnInstallDB($arParams = array())
        {
                $this->errors = false;

                UnRegisterModuleDependences("main", "OnProlog", "altasib.warninglimit", "WARNINGLIMIT_alx", "WARNINGLIMITOnProlog");
                COption::RemoveOption("altasib_warninglimit");
                UnRegisterModule("altasib.warninglimit");

                return true;

        }

        function InstallFiles()
        {
                CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.warninglimit/install/js",$_SERVER["DOCUMENT_ROOT"]."/bitrix/js/altasib.warninglimit",true,true);
                CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/altasib.warninglimit/install/images",$_SERVER["DOCUMENT_ROOT"]."/bitrix/images/altasib.warninglimit",true,true);
				
				$bReWriteAdditionalFiles = (($GLOBALS["public_rewrite"] == "Y") ? true : false);
			
				if($GLOBALS["install_public"] == "Y" && !empty($GLOBALS["public_dir"]))
				{
					$bReWriteAdditionalFiles = (($GLOBALS["public_rewrite"] == "Y") ? true : false);
					$file = $_SERVER["DOCUMENT_ROOT"].$GLOBALS['public_dir'];
					COption::setOptionString("altasib_warninglimit", "rule_href", $GLOBALS["public_dir"]);
					
					if(!file_exists($file) || $bReWriteAdditionalFiles) 
					{
						$fp = fopen($file, "w"); 
						fwrite($fp, GetMessage('FILE_TEXT'));
						fclose (fp);
					}
				}
                return true;
        }

        function UnInstallFiles($arParams = array())
        {
                DeleteDirFilesEx("/bitrix/js/altasib.warninglimit");
                DeleteDirFilesEx("/bitrix/images/altasib.warninglimit");
				$rule = COption::GetOptionString('altasib_warninglimit', 'rule_href', '');
				if ($arParams['deleterules']) 
				{
					DeleteDirFilesEx($arParams['rule']);
				}
                return true;
        }
}
?>
