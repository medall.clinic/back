<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Andrew N. Popov                  #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2010 ALTASIB             #
#################################################
?>
<?
IncludeModuleLangFile(__FILE__);
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="lang" value="<?echo LANG?>">
        <input type="hidden" name="id" value="altasib.warninglimit">
        <input type="hidden" name="uninstall" value="Y">
        <input type="hidden" name="step" value="2">
		<input type="hidden" name="rule" value="<?=COption::GetOptionString('altasib_warninglimit', 'rule_href', '');?>">
        <?echo CAdminMessage::ShowMessage(GetMessage("MOD_UNINST_WARN"))?>
		<?
		$rule = COption::GetOptionString('altasib_warninglimit', 'rule_href', '');
		if(trim($rule)!=''):
		?>
        <p><input type="checkbox" name="deleterules" id="deleterules" value="Y"><label for="deleterules"><?echo GetMessage("ALTASIB_WARNINGLIMIT_DEL_RULE").COption::GetOptionString('altasib_warninglimit', 'rule_href', '');?></label></p>
		<?endif?>
        <input type="submit" name="inst" value="<?echo GetMessage("MOD_UNINST_DEL")?>">
</form>