<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Glower                           #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2012 ALTASIB             #
#################################################
?>

<?
IncludeModuleLangFile(__FILE__);

if ($GLOBALS["install_step"] == 2):

	if(!check_bitrix_sessid()) return;

	if($ex = $APPLICATION->GetException())
		echo CAdminMessage::ShowMessage(Array(
			"TYPE" => "ERROR",
			"MESSAGE" => GetMessage("MOD_INST_ERR"),
			"DETAILS" => $ex->GetString(),
			"HTML" => true,
		));
	else
		echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));

?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
        <input type="hidden" name="lang" value="<?echo LANG?>">
        <input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>">
</form>
<?
	return;
endif;

?>
<form action="<?=$APPLICATION->GetCurPage()?>" name="form1">
<?=bitrix_sessid_post()?>
<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
<input type="hidden" name="id" value="altasib.warninglimit" />
<input type="hidden" name="install" value="Y" />
<input type="hidden" name="step" value="2" />
<script language="JavaScript">
<!--
function ChangeInstallPublic(val)
{
	document.form1.public_dir.disabled = !val;
	document.form1.public_rewrite.disabled = !val;
}
//-->
</script>

<p class="warninglimit-install-fields">
	<table cellpadding="3" cellspacing="0" border="0" width="0%">
		<tr>
			<td colspan="2">
				<input type="checkbox" name="install_public" value="Y" id="id_install_public" onclick="ChangeInstallPublic(this.checked)">
				<label for="id_install_public"><?= GetMessage("ALTASIB_WARNINGLIMIT_COPY_RULE") ?></label></td> 
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<div class="warninglimit-install-field warninglimit-install-field-copy">
					<label for="public_dir"><?=GetMessage("ALTASIB_WARNINGLIMIT_COPY_DIR")?></label>
					<input type="input" name="public_dir" id="public_dir" value="/rules.php" size="40" />
				</div>
				<div class="warninglimit-install-field warninglimit-install-field-rewrite">
					<input type="checkbox" name="public_rewrite" id="public_rewrite" value="Y" />
					<label for="public_rewrite"><?=GetMessage("INSTALL_PUBLIC_REW")?></label>
				</div>
			</td>
		</tr>
	</table>
</p>
<style>
	p.warninglimit-install-fields table, p.warninglimit-install-fields td,  p.warninglimit-install-fields label{font-size:100%;}
</style>
<script language="JavaScript">
<!--
ChangeInstallPublic(false);
//-->
</script>
<br />
<input type="submit" name="inst" value="<?= GetMessage("MOD_INSTALL")?>" />
</form>
<??>