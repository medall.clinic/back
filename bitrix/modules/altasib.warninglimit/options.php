<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Glower                           #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2012 ALTASIB             #
#################################################
?>
<?
IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].BX_ROOT.'/modules/main/options.php');

if(!$USER->IsAdmin()) return;

$module_id = 'altasib_warninglimit';
$strWarning = '';

$arAllOptions = array(
        'main' => Array(
                Array('enable_warninglimit', GetMessage('ENABLE_WARNINGLIMIT'), GetMessage('ENABLE_WARNINGLIMIT_DEF'), array('checkbox')),
                Array('disable_auth', GetMessage('DISABLE_AUTH'), GetMessage('DISABLE_AUTH_DEF'), array('checkbox')),
                                Array('width', GetMessage('WIDTH'), GetMessage('WIDTH_DEF'), Array('text', 5)),
                Array('title', GetMessage('TITLE'), GetMessage('TITLE_DEF'), Array('text', 50)),
                Array('text', GetMessage('TEXT'), GetMessage('TEXT_DEF'), Array('textarea',8,43)),
                Array('checkbox_text', GetMessage('CHECKBOX_TEXT'), GetMessage('CHECKBOX_TEXT_DEF'), Array('text', 50)),
                Array('enter_text', GetMessage('ENTER_TEXT'), GetMessage('ENTER_TEXT_DEF'), Array('text', 50)),
                Array('logo', GetMessage('LOGO'), GetMessage('LOGO_DEF'), Array('text', 50)),
                                Array('logo_height', GetMessage('LOGO_HEIGHT'), GetMessage('LOGO_HEIGHT_DEF'), Array('text', 5)),
                                Array('logo_width', GetMessage('LOGO_WIDTH'), GetMessage('LOGO_WIDTH_DEF'), Array('text', 5)),
                                Array('rule_href', GetMessage('RULE_HREF'), GetMessage('RULE_HREF_DEF'), Array('text', 50)),
                Array('rule_text', GetMessage('RULE_TEXT'), GetMessage('RULE_TEXT_DEF'), Array('textarea',2,43)),
                Array('href_text', GetMessage('HREF_TEXT'), GetMessage('HREF_TEXT_DEF'), Array('text',50)),
                Array('checkbox_urls', GetMessage('CHECKBOX_URLS'), GetMessage('CHECKBOX_URLS_DEF'), array('checkbox')),
                Array('urls', GetMessage('URLS'), GetMessage('URLS_DEF'), Array('textarea',10, 43)),
                Array('denied_title', GetMessage('DENIED_TITLE'), GetMessage('DENIED_TITLE_DEF'), Array('text', 50)),
                Array('denied_text', GetMessage('DENIED_TEXT'), GetMessage('DENIED_TEXT_DEF'), Array('textarea',2, 43)),
        )
);

$aTabs = array(
        array('DIV' => 'edit1', 'TAB' => GetMessage('MAIN_TAB_SET'), 'TITLE' => GetMessage('MAIN_TAB_TITLE_SET')),
        array('DIV' => 'edit2', 'TAB' => GetMessage('MAIN_TAB_RIGHTS'), 'TITLE' => GetMessage('MAIN_TAB_TITLE_RIGHTS')),
);

//Restore defaults
if ($USER->IsAdmin() && $_SERVER['REQUEST_METHOD']=='GET' && strlen($RestoreDefaults)>0 && check_bitrix_sessid())
{
        COption::RemoveOption('altasib_warninglimit');
}
$tabControl = new CAdminTabControl('tabControl', $aTabs);

function ShowParamsHTMLByArray($arParams)
{
        foreach($arParams as $Option)
        {
                 __AdmSettingsDrawRow('altasib_warninglimit', $Option);
        }
}

//Save options
if($REQUEST_METHOD=='POST' && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
{
        if(strlen($RestoreDefaults)>0)
        {
                COption::RemoveOption('altasib_warninglimit');
        }
        else
        {
                foreach($arAllOptions as $aOptGroup)
                {
                        foreach($aOptGroup as $option)
                        {
                                __AdmSettingsSaveOption($module_id, $option);
                        }
                }
        }
        if(strlen($Update)>0 && strlen($_REQUEST['back_url_settings'])>0)
                LocalRedirect($_REQUEST['back_url_settings']);
        else
                LocalRedirect($APPLICATION->GetCurPage().'?mid='.urlencode($mid).'&lang='.urlencode(LANGUAGE_ID).'&back_url_settings='.urlencode($_REQUEST['back_url_settings']).'&'.$tabControl->ActiveTabParam());
}
?>

<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&amp;lang=<?echo LANG?>'>
<?
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<div style="background-color: #fff; padding: 0; border-top: 1px solid #8E8E8E; border-bottom: 1px solid #8E8E8E;  margin-bottom: 15px;"><div style="background-color: #8E8E8E; height: 30px; padding: 7px; border: 1px solid #fff">
        <a href="http://www.is-market.ru?param=cl" target="_blank"><img src="/bitrix/images/altasib.warninglimit/is-market.gif" style="float: left; margin-right: 15px;" border="0" /></a>
        <div style="margin: 13px 0px 0px 0px">
                <a href="http://www.is-market.ru?param=cl" target="_blank" style="color: #fff; font-size: 10px; text-decoration: none"><?=GetMessage("ALTASIB_IS")?></a>
        </div>
</div></div>
<?
ShowParamsHTMLByArray($arAllOptions['main']);
$tabControl->BeginNextTab();
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
$tabControl->Buttons();?>


<script language='JavaScript'>

function RestoreDefaults()
{
        if(confirm('<?echo AddSlashes(GetMessage('MAIN_HINT_RESTORE_DEFAULTS_WARNING'))?>'))
                window.location = '<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)?>&<?=bitrix_sessid_get()?>';
}
</script>

<script language='JavaScript'>
function RestoreDefaults()
{
        if(confirm('<?echo AddSlashes(GetMessage('MAIN_HINT_RESTORE_DEFAULTS_WARNING'))?>'))
                window.location = '<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)?>&<?=bitrix_sessid_get()?>';
}
</script>

<div align='left'>
        <input type='hidden' name='Update' value='Y'>
        <input type='submit' <?if(!$USER->IsAdmin())echo ' disabled ';?> name='Update' value='<?echo GetMessage('MAIN_SAVE')?>'>
        <input type='reset' <?if(!$USER->IsAdmin())echo ' disabled ';?> name='reset' value='<?echo GetMessage('MAIN_RESET')?>'>
        <input type='button' <?if(!$USER->IsAdmin())echo ' disabled ';?>  type='button' title='<?echo GetMessage('MAIN_HINT_RESTORE_DEFAULTS')?>' OnClick='RestoreDefaults();' value='<?echo GetMessage('MAIN_RESTORE_DEFAULTS')?>'>
</div>
<?$tabControl->End();?>
<?=bitrix_sessid_post();?>
</form>
