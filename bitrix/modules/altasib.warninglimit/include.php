<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Glower                           #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2012 ALTASIB             #
#################################################
?>
<?
//global $DBType;
IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/altasib.warninglimit/options.php');

$arClassesList = array(
    // main classes
    // API classes
);

Class WARNINGLIMIT_alx
{
    Function WARNINGLIMITOnProlog()
    {
        global $APPLICATION, $USER;
        if (IsModuleInstalled('altasib.warninglimit'))
        {
            if(!defined(ADMIN_SECTION) && ADMIN_SECTION !== true)
            {
                $warninglimit_enabled = COption::GetOptionString('altasib_warninglimit', 'enable_warninglimit',GetMessage('ENABLE_WARNINGLIMIT_DEF'));
                if($warninglimit_enabled == 'Y')
                {
                //if user press cancel, show only header, footer, and error information
                    if($_SESSION['alx_access'] == 'n')
                    {
                        $denied_title = COption::GetOptionString('altasib_warninglimit', 'denied_title', GetMessage('DENIED_TITLE_DEF'));
                        $denied_text = COption::GetOptionString('altasib_warninglimit', 'denied_text', GetMessage('DENIED_TEXT_DEF'));
                        ChangeChars($denied_title);
                        ChangeChars($denied_text);
                        $rule_href = COption::GetOptionString('altasib_warninglimit', 'rule_href', GetMessage('RULE_HREF_DEF'));

                        require($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/header.php');
                        $APPLICATION->SetTitle($denied_title);
                        $APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");

                        preg_match_all("/#(.+)#/", $denied_text, $out);
                        if(trim($rule_href)!='')
                            $alx_a = '<a href = "'.$rule_href.'">'.$out[1][0].'</a>';
                        else
                            $alx_a = $out[1][0];

						$tmp = preg_replace("/#(.+)#/", $alx_a, $denied_text);
                            $denied_text = $tmp;
                        echo $denied_text;
                        require($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/footer.php');
                        //require($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/epilog.php');
                        unset($_SESSION["alx_access"]);
                        die();
                    }

                    if($_POST['alx_access']=='n')
                    {
                        $_SESSION['alx_access'] = 'n';
                        unset($_POST['alx_access']);
                        LocalRedirect($APPLICATION->GetCurPage());
                    }

                    $disable_auth = COption::GetOptionString('altasib_warninglimit', 'disable_auth', GetMessage('DISABLE_AUTH_DEF'));

                                        $checkbox_urls = COption::GetOptionString('altasib_warninglimit', 'checkbox_urls', GetMessage('CHECKBOX_URLS_DEF'));
                                        $urls = COption::GetOptionString('altasib_warninglimit', 'urls', GetMessage('URLS_DEF'));
                                        $rule_text = COption::GetOptionString('altasib_warninglimit', 'rule_text', GetMessage('RULE_TEXT_DEF'));
                                        $rule_href = COption::GetOptionString('altasib_warninglimit', 'rule_href', GetMessage('RULE_HREF_DEF'));
                                        $href_text = COption::GetOptionString('altasib_warninglimit', 'href_text', GetMessage('HREF_TEXT_DEF'));

                                        $find = 'N';
                                        $warning = 'Y';

                                        //if current page is page with rules
                                        if(!empty($rule_href))
                                        {
                                                if(stripos(trim($APPLICATION->GetCurPage(true)), trim(RelativeURL($rule_href)))===0)
                                                {
                                                         $warning = 'N';
                                                         $find = 'Y';
                                                }
                                        }

                                        //if current page is in the fiter list
                                        if(!empty($urls) && $find == 'N')
                                        {
                                                $exception_links = explode("\n", $urls);
                                                foreach ($exception_links as $key => $value)
                                                {
                                                         if(stripos(trim($APPLICATION->GetCurPage(true)), trim(RelativeURL($exception_links[$key])))===0)
                                                         {
                                                                  $find = 'Y';
                                                                  break;
                                                         }
                                                }
                                                if(($find == 'Y' && $checkbox_urls == 'N') || ($find == 'N' && $checkbox_urls == 'Y'))
                                                   $warning = 'N';
                                        }

                            $alx_cookie = 'alx_accepted'.'_'.SITE_ID;
                                        //check for showing warning window
                                        if((($disable_auth != 'Y' && $USER->IsAuthorized()) || !$USER->IsAuthorized()) && $_COOKIE[$alx_cookie]!='Y' && $warning == 'Y')
                                        {
                                                WARNINGLIMIT_alx::addScriptOnSite();
                                                return true;
                                        }
                                }
            }
        }
    }

    function addScriptOnSite()
    {
                global $APPLICATION;
        CUtil::InitJSCore(Array('jquery'));
        $title = COption::GetOptionString('altasib_warninglimit', 'title', GetMessage('TITLE_DEF'));
        $text = COption::GetOptionString('altasib_warninglimit', 'text', GetMessage('TEXT_DEF'));
        $width = COption::GetOptionString('altasib_warninglimit', 'width', GetMessage('WIDTH_DEF'));
        $checkbox_text = COption::GetOptionString('altasib_warninglimit', 'checkbox_text', GetMessage('CHECKBOX_TEXT_DEF'));
        $enter_text = COption::GetOptionString('altasib_warninglimit', 'enter_text', GetMessage('ENTER_TEXT_DEF'));
        $logo = COption::GetOptionString('altasib_warninglimit', 'logo', GetMessage('LOGO_DEF'));
        $logo_height = COption::GetOptionString('altasib_warninglimit', 'logo_height', GetMessage('LOGO_HEIGHT_DEF'));
        $logo_width = COption::GetOptionString('altasib_warninglimit', 'logo_width', GetMessage('LOGO_WIDTH_DEF'));
        $rule_text = COption::GetOptionString('altasib_warninglimit', 'rule_text', GetMessage('RULE_TEXT_DEF'));
        $rule_href = COption::GetOptionString('altasib_warninglimit', 'rule_href', GetMessage('RULE_HREF_DEF'));
        $href_text = COption::GetOptionString('altasib_warninglimit', 'href_text', GetMessage('HREF_TEXT_DEF'));

                ChangeChars($title);
                ChangeChars($text);
                ChangeChars($checkbox_text);
                ChangeChars($enter_text);
                ChangeChars($rule_text);
                ChangeChars($href_text);

         //for IE -----------//
        if(empty($logo_height))
            $logo_height = 'var alx_warninglimit_logo_height = "";';
        else
            $logo_height = 'var alx_warninglimit_logo_height = "height = \"'.$logo_height.'\"";';
        if(empty($logo_width))
            $logo_width = 'var alx_warninglimit_logo_width = "";';
        else
                        $logo_width = 'var alx_warninglimit_logo_width = "width = \"'.$logo_width.'\"";';

        $APPLICATION->AddHeadString('<link href="/bitrix/js/altasib.warninglimit/styles.css" type="text/css" rel="stylesheet" />',true);

        if(!empty($rule_href))
        {
            $altasib_rule_href = '<br/>'.$rule_text.'<a href=\"'.$rule_href.'\" id=\"alx_rule\">'.$href_text.'</a>';
        }
        $APPLICATION->AddHeadString('<script type="text/javascript">top.BX["alx_warninglimit_cur_page"] = true; var alx_warninglimit_SID = "'.SITE_ID.'"; var alx_warninglimit_width = "'.$width.'"; var alx_warninglimit_cur_page = "'.$APPLICATION->GetCurPage().'"; var alx_warninglimit_img_logo = "'.$logo.'"; '.$logo_height.$logo_width.'var alx_warninglimit_title = "'.$title.'"; var alx_warninglimit_text = "'.$text.'"; var alx_warninglimit_checkbox_text = "'.$checkbox_text.'"; var alx_warninglimit_enter_text = "'.$enter_text.'"; var alx_warninglimit_rule_href = "'.$altasib_rule_href.'"; var alx_warninglimit_close_text = "'.GetMessage('ALTASIB_WARNINGLIMIT_CLOSE_TEXT').'";</script>');

        $APPLICATION->AddHeadScript('/bitrix/js/altasib.warninglimit/warninglimit.js');
    }
}

//function converts url to relative link
if(!function_exists("RelativeURL"))
{
    function RelativeURL($url_change)
    {
        if(substr($url_change,0,7)=='http://')
            $url_change = substr($url_change,7);
        if(substr($url_change,0,4)=='www.')
            $url_change = substr($url_change,4);
        if(substr($url_change,0,strlen($_SERVER[HTTP_HOST])) == $_SERVER[HTTP_HOST])
            $url_change = substr($url_change,strlen($_SERVER[HTTP_HOST]));
        return $url_change;
    }
}
if(!function_exists("ChangeChars"))
{
        function ChangeChars(&$str)
        {
            $str = htmlspecialcharsEx($str);
                $str = str_replace("\r", "", $str);
                $str = str_replace("\n", "<br/>", $str);
                return $str;
        }
}
?>
