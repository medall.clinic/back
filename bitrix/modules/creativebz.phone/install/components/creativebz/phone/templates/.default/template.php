<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponentTemplate $this */
$frame = $this->createFrame()->begin('');

$phone = $arResult['PHONES'][$arResult['PHONE_ID']];
$clickCode = '';
if(!empty($phone['PROPERTY_GOAL_NAME_VALUE'])){
    $clickCode = "onclick=\"".
        (empty($arResult['YA_COUNTER_ID']) ? '' :
            "ym({$arResult['YA_COUNTER_ID']}, 'reachGoal', '{$phone['PROPERTY_GOAL_NAME_VALUE']}_click');").
        " if(typeof ga == 'function')ga('send', 'event', 'creativebz', '{$phone['PROPERTY_GOAL_NAME_VALUE']}_click');\"";
}
if ($arResult['CLASS']) {
    $clickCode .= ' class="'.$arResult['CLASS'].'"';
}
if ($arResult['ID']) {
    $clickCode .= ' id="'.$arResult['ID'].'"';
}
if(!empty($phone['PROPERTY_LINK_VALUE'])){?>
    <a href="<?=strpos($phone['PROPERTY_LINK_VALUE'], '@') ? 'mailto' : 'tel'?>:<?=$phone['PROPERTY_LINK_VALUE']?>" <?=
        $clickCode?>><?=$phone['NAME']?></a>
<?}else{
    echo $phone['NAME'];
}
if($useCounter) {
    ?>
    <script>
        <?if(!isset($GLOBALS['CREATIVEBZ_PHONE'])):?>
            window.creativePhones = {
                yaCounter: <?=$arResult['YA_COUNTER_ID']?>,
                goals: {},
                counterLoaded: window.creativePhones ? window.creativePhones.counterLoaded : false,
                phonesLoaded: window.creativePhones ? window.creativePhones.phonesLoaded : false
            };
        <?endif?>
        window.creativePhones.goals['<?=$phone['PROPERTY_GOAL_NAME_VALUE']?>'] = false;
		window.onload = function() {
            if (window.ym !== undefined) ym(<?=$arResult['YA_COUNTER_ID']?>, 'reachGoal', '<?=$phone['PROPERTY_GOAL_NAME_VALUE']?>');
        }
    </script>
    <?
    $GLOBALS['CREATIVEBZ_PHONE'] = true;
}
$frame->end();