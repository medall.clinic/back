<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $this */

CModule::IncludeModule('creativebz.phone');
$arResult = CrPhone::getResult($arParams, $this);
$filter = '/[^\w -]/';
$arResult['CLASS'] = !empty($arParams['CLASS']) ? str_replace('quot', '', preg_replace($filter, '', $arParams['CLASS'])) : '';
$arResult['ID'] = !empty($arParams['ID']) ? str_replace('quot', '', preg_replace($filter, '', $arParams['ID'])) : '';
$this->includeComponentTemplate();