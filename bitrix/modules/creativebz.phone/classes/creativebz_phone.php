<?php
class CreativebzPhone{
    public static function OnAfterIBlockElementAdd(&$arFields)
    {
        if(!$arFields['RESULT']) return;

        $iblock = COption::GetOptionInt('creativebz.phone', 'IBLOCK_ID');
        if(!empty($arFields['IBLOCK_ID']) && $iblock == $arFields['IBLOCK_ID']){
            if($prop = CIBlockProperty::GetList(array(), array('CODE' => 'GOAL_NAME', 'IBLOCK_ID' => $iblock))->Fetch()){
                if(empty($arFields['PROPERTY_VALUES'][$prop['ID']]) ||
                    empty($arFields['PROPERTY_VALUES'][$prop['ID']]['n0']['VALUE'])
                ){
                    $name = 'creativebz_phone_'.$arFields['RESULT'];
                    $arFields['PROPERTY_VALUES'][$prop['ID']]['n0']['VALUE'] = $name;
                    CIBlockElement::SetPropertyValuesEx($arFields['RESULT'], $arFields['IBLOCK_ID'],
                        array('GOAL_NAME' => $name));
                }
            }
        }
    }
	
	public static function OnAdminTabControlBegin(&$arFields)
    {
        $iblock = COption::GetOptionInt('creativebz.phone', 'IBLOCK_ID');
		if(strpos($_SERVER['REQUEST_URI'],'iblock_element_edit.php') !== false) {
			if($prop = CIBlockProperty::GetList(array(), array('CODE' => 'COUNTRY', 'IBLOCK_ID' => $iblock))->Fetch()){
				$countryIdCode = $prop['ID'];
			}
			if($prop = CIBlockProperty::GetList(array(), array('CODE' => 'REGION', 'IBLOCK_ID' => $iblock))->Fetch()){
				$regionIdCode = $prop['ID'];
			}
			//print $iblock;
			if(strpos($_SERVER['REQUEST_URI'],'IBLOCK_ID='.$iblock) !== false) {
				print '
				<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
				<script>
				 $(document).ready(function() {
					var globalCountryID = "#tr_PROPERTY_'.$countryIdCode.'";
					var globalRegionID = "#tr_PROPERTY_'.$regionIdCode.'";
					$(globalCountryID).on("change", function() {
						var id = $(this).find(":selected").val();
						if (id > 0) {
							var val = $(this).find("select option[value="+id+"]").text();
							if (val == "'.GetMessage("CREATIVEBZ_PHONE_ROSSIA").'") {
							    $(globalRegionID).fadeIn();
							} else $(globalRegionID).fadeOut();
						}
					});
					//$(globalCountryID+" select option").eq(0).remove();
					//$(globalRegionID+" select option").eq(0).remove();
				 });
                </script>';
			}
		}
    }
}