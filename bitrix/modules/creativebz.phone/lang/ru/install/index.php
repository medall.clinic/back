<?
$MESS["CREATIVE_PHONE_MODULE_NAME"] = "Замена номера 2.0";
$MESS["CREATIVE_PHONE_MODULE_DESCRIPTION"] = "Автоматическая замена номера по критериям";
$MESS["CREATIVE_PHONE_MODULE_INST_ERR"] = "При установке возникла ошибка";
$MESS["CREATIVE_PHONE_MODULE_IBLOCK_TYPE"] = "Телефоны";
$MESS["CREATIVE_PHONE_MODULE_UTM_HINT"] = "Вставьте значение \"utm_source=yandex\" для отслеживания Яндекс.Директ";
$MESS["CREATIVE_PHONE_MODULE_URL_HINT"] = "Вставьте значение \"yandex.ru\" для отслеживания естественной выдачи яндекса";
$MESS["CREATIVE_PHONE_MODULE_SIGNIF_HINT"] = "Используется для атрибуции \"Последнее значимое взаимодействие\"";
$MESS["CREATIVE_PHONE_MODULE_NAME_DEFAULT"] = "+7 (123) 456-78-90";
$MESS["CREATIVE_PHONE_MODULE_LINK"] = "Ссылка";
$MESS["CREATIVE_PHONE_MODULE_LINK_HINT"] = "Вставьте номер телефона в формате +71234567890, чтобы номер стал кликабельным на смартфонах или email адрес, чтобы открывалось окно с отправкой письма";
$MESS["CREATIVE_PHONE_MODULE_CHEL"] = "Челябинская область%";
$MESS["CREATIVE_PHONE_MODULE_PHONE_UTM"] = "+7 (123) 456-78-90 я";
$MESS["CREATIVE_PHONE_MODULE_PHONE_URL"] = "+7 (999) 456-78-90 б";
$MESS["CREATIVE_PHONE_MODULE_PHONE_CHEL"] = "+7 (123) 456-78-90 ч";
$MESS["CREATIVE_PHONE_MODULE_GOAL_NAME"] = "Идентификатор события на просмотр";
$MESS["CREATIVE_PHONE_MODULE_GOAL_NAME_HINT"] = "При создании заполняется автоматически. Цель срабатывает на показ данного номера телефона (на нажатие - с добавлением в конце _click). Чтобы цель срабатывала, необходимо заполнить идентификатор счетчика на странице настроек модуля.";
$MESS["CREATIVEBZ_PHONE_ROSSIA"] = "Россия";

?>