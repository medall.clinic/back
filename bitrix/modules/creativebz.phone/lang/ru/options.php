<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['PREFERENCES_BLOCK_ID'] = "ID инфоблока со списком телефонов";
$MESS['PREFERENCES_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['PREFERENCES_INVALID_VALUE'] = "Введено неверное значение";
$MESS['PREFERENCES_PROP'] = "Свойство";
$MESS['PREFERENCES_DENIED'] = "Доступ запрещен";
$MESS['PREFERENCES_YA_COUNTER'] = "Номер вашего счетчика метрики";
$MESS['PREFERENCES_YA_COUNTER_HINT'] = "Введите номер счетчика, чтобы получать достижения цели с идентификатором, указанном в настройках телефона, при нажатии на номер телефона на сайте";
$MESS['PREFERENCES_MODEL_DESCR'] = "Выбор модели атрибуции";
$MESS['PREFERENCES_FIRST_MODEL'] = "Первое взаимодействие";
$MESS['PREFERENCES_SECOND_MODEL'] = "Последнее взаимодействие";
$MESS['PREFERENCES_THIRD_MODEL'] = "Последнее значимое взаимодействие";
$MESS['PREFERENCES_UP_CITIES'] = "Загрузить новый файл cities.txt (обновление БД IP)";
$MESS['PREFERENCES_UP_CIDR'] = "Загрузить новый файл cidr_optim.txt (обновление БД IP)";
$MESS['PREFERENCES_USES_DB'] = "Использовать базу для определения города по IP";
$MESS['PREFERENCES_UPDDATE_GEOBASE'] = "Дата последней проверки обновлений базы IPGeoBase";
$MESS['PREFERENCES_UPDDATE_GEOLITE'] = "Дата последней проверки обновлений базы GeoLite2";
$MESS['PREFERENCES_UPDDATE_UPDBTN'] = "Проверить обновления";
$MESS['PREFERENCES_UPDDATE_NOTUPDATED'] = "Не проверялось";

