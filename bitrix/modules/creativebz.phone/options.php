<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'creativebz.phone');

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

if (!$USER->isAdmin()) {
    $APPLICATION->authForm(Loc::getMessage('PREFERENCES_DENIED'));
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');

$arIBlocks = array('REFERENCE' => array(), 'REFERENCE_ID' => array());
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"]));
while($arRes = $db_iblock->Fetch()){
    $arIBlocks['REFERENCE'][] = $arRes["NAME"].' ['.$arRes["ID"].']';
    $arIBlocks['REFERENCE_ID'][] = $arRes["ID"];
}

$attribution['REFERENCE'] = array(Loc::getMessage("PREFERENCES_FIRST_MODEL"),Loc::getMessage("PREFERENCES_SECOND_MODEL"),Loc::getMessage("PREFERENCES_THIRD_MODEL"));
$attribution['REFERENCE_ID'] = array(1,2,3);

$uses_db['REFERENCE'] = array('IPGeoBase','GeoLite2');
$uses_db['REFERENCE_ID'] = array(1,2);


if ($request->isPost() && check_bitrix_sessid()) {
    if ($request->getPost('IBLOCK_ID')) {
        Option::set(ADMIN_MODULE_NAME, "IBLOCK_ID", $request->getPost('IBLOCK_ID'));
        Option::set(ADMIN_MODULE_NAME, "YA_COUNTER_ID", $request->getPost('YA_COUNTER_ID'));
		Option::set(ADMIN_MODULE_NAME, "MODEL_ATTRIBUTION", $request->getPost('MODEL_ATTRIBUTION'));
		Option::set(ADMIN_MODULE_NAME, "USES_DB", $request->getPost('USES_DB'));
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("PREFERENCES_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
        
        function clearFolder($flname) {
            if (file_exists($flname)) {
                foreach (glob($flname.'*') as $file) {
                    if (is_dir($file)) {
                        clearFolder($file.'/');
                    } else {
                        unlink($file);
                    }
                }
            }
        }
        
        if (isset($_POST['checkupd'])) {
            Option::set(ADMIN_MODULE_NAME, "LASTUPDATEGEOLITE", time());
            $lnk = 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=69geiaKYXcEjZcxK&suffix=tar.gz';
            $fl = file_get_contents($lnk);
            if ($fl) {
                file_put_contents(__DIR__.'/tmp.tar.gz',$fl);

                require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/tar_gz.php');
                
                $oArchiver = new CArchiver(__DIR__.'/tmp.tar.gz');
                mkdir(__DIR__.'/ex');
                $tres = $oArchiver->extractFiles(__DIR__.'/ex');
                unlink(__DIR__.'/tmp.tar.gz');
                $files1 = scandir(__DIR__.'/ex');
                foreach($files1 as $oneFile) {
                    if ($oneFile != '.' && $oneFile != '..' && is_dir(__DIR__.'/ex/'.$oneFile)) {
                        $flName = __DIR__.'/ex/'.$oneFile.'/GeoLite2-City.mmdb';
                        if (file_exists($flName)) {
                            copy($flName,__DIR__.'/classes/geoip/GeoLite2-City.mmdb');
                        }
                    }
                }
                clearFolder(__DIR__.'/ex/');
            }
        }
        
        
		
		if($_FILES['UPDATECITIES']['size'] > 0 && $_FILES['UPDATECITIES']['tmp_name'] != '') {
            $backupCit = __DIR__.'/classes/ipgeo/cities_backup.txt';
            $origCit = __DIR__.'/classes/ipgeo/cities.txt';
            if (!file_exists($backupCit)) {
                copy($origCit, $backupCit);
            }
            if (file_exists($origCit)) {
                unlink($origCit);
            }
            if (file_exists($_FILES['UPDATECITIES']['tmp_name'])) {
                copy($_FILES['UPDATECITIES']['tmp_name'],$origCit);
            }
            
        }
        if($_FILES['UPDATECIDR']['size'] > 0 && $_FILES['UPDATECIDR']['tmp_name'] != '') {
            $backupCit = __DIR__.'/classes/ipgeo/cidr_optim_backup.txt';
            $origCit = __DIR__.'/classes/ipgeo/cidr_optim.txt';
            if (!file_exists($backupCit)) {
                copy($origCit, $backupCit);
            }
            if (file_exists($origCit)) {
                unlink($origCit);
            }
            if (file_exists($_FILES['UPDATECIDR']['tmp_name'])) {
                copy($_FILES['UPDATECIDR']['tmp_name'],$origCit);
            }
        }
		
    } else {
        CAdminMessage::showMessage(Loc::getMessage("PREFERENCES_INVALID_VALUE"));
    }
}


$tabControl = new CAdminTabControl("tabControl", array(
    array(
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("MAIN_TAB_SET"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_SET"),
    ),
));

$tabControl->begin();
?>

<form method="post" class="props" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(),
    urlencode(ADMIN_MODULE_NAME), LANGUAGE_ID)?>" enctype="multipart/form-data">
    <?php
    echo bitrix_sessid_post();
    $tabControl->beginNextTab();
    ?>
    <tr>
        <td width="50%"><label for="IBLOCK_ID"><?=Loc::getMessage("PREFERENCES_BLOCK_ID") ?>:</label></td>
        <td>
            <?=SelectBoxFromArray('IBLOCK_ID', $arIBlocks, Option::get(ADMIN_MODULE_NAME, 'IBLOCK_ID'))?>
        </td>
    </tr>
    <tr>
        <td>
            <span id="hint_1"></span>
            <script type="text/javascript">BX.hint_replace(BX('hint_1'), '<?=Loc::getMessage("PREFERENCES_YA_COUNTER_HINT") ?>');</script>
            <label for="YA_COUNTER_ID"><?=Loc::getMessage("PREFERENCES_YA_COUNTER") ?>:</label></td>
        <td>
            <input class="adm-input" type="number" min="0" step="1" id="YA_COUNTER_ID" name="YA_COUNTER_ID" value="<?=
                Option::get(ADMIN_MODULE_NAME, 'YA_COUNTER_ID')?>">
        </td>
    </tr>
	<tr>
        <td width="50%"><label for="MODEL_ATTRIBUTION"><?=Loc::getMessage("PREFERENCES_MODEL_DESCR") ?>:</label></td>
        <td>
            <?=SelectBoxFromArray('MODEL_ATTRIBUTION', $attribution, Option::get(ADMIN_MODULE_NAME, 'MODEL_ATTRIBUTION'))?>
        </td>
    </tr>
    
    <tr>
        <td width="50%"><label for="USES_DB"><?=Loc::getMessage("PREFERENCES_USES_DB") ?>:</label></td>
        <td>
            <?=SelectBoxFromArray('USES_DB', $uses_db, Option::get(ADMIN_MODULE_NAME, 'USES_DB'))?>
        </td>
    </tr>
    
    <tr>
        <td width="50%"><label><?=Loc::getMessage("PREFERENCES_UPDDATE_GEOBASE") ?>:</label></td>
        <td>
            31.10.2017
        </td>
    </tr>
    <tr>
        <td width="50%"><label><?=Loc::getMessage("PREFERENCES_UPDDATE_GEOLITE") ?>:</label></td>
        <td>
            <?print (Option::get(ADMIN_MODULE_NAME, 'LASTUPDATEGEOLITE') != '' ? date('d.m.Y',Option::get(ADMIN_MODULE_NAME, 'LASTUPDATEGEOLITE')) : Loc::getMessage("PREFERENCES_UPDDATE_NOTUPDATED"))?> <input type="submit" name="checkupd" class="button" value="<?=Loc::getMessage("PREFERENCES_UPDDATE_UPDBTN") ?>">
        </td>
    </tr>
    
	
	<? /*<tr>
        <td width="50%"><label for="UPDATECITIES"><?=Loc::getMessage("PREFERENCES_UP_CITIES") ?>:</label></td>
        <td>
            <input type="file" name="UPDATECITIES">
        </td>
    </tr>
    <tr>
        <td width="50%"><label for="UPDATECIDR"><?=Loc::getMessage("PREFERENCES_UP_CIDR") ?>:</label></td>
        <td>
            <input type="file" name="UPDATECIDR">
        </td>
    </tr>*/ ?>
    

    <?php
    $tabControl->buttons();
    ?>
    <input type="submit"
           name="save"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
           />
    <?php
    $tabControl->end();
    ?>
</form>
<style>
    .props input[type='text']{width: 85%}
</style>