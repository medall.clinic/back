<?php

namespace Bitrix\Main;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Type\Date;

final class License
{
	private ?string $key = null;

	private const DOMAINS_STORE_LICENSE = [
		'ru' => 'https://util.1c-bitrix.ru',
		'ua' => 'https://util.bitrix.ua',
		'en' => 'https://util.bitrixsoft.com',
		'kz' => 'https://util.1c-bitrix.kz',
		'by' => 'https://util.1c-bitrix.by',
	];
	public const URL_BUS_EULA = [
		'ru' => 'https://www.1c-bitrix.ru/download/law/eula_bus.pdf',
		'by' => 'https://www.1c-bitrix.by/download/law/eula_bus.pdf',
		'kz' => 'https://www.1c-bitrix.kz/download/law/eula_bus.pdf',
		'ua' => 'https://www.bitrix.ua/download/law/eula_bus.pdf',
	];
	public const URL_CP_EULA = [
		'ru' => 'https://www.1c-bitrix.ru/download/law/eula_cp.pdf',
		'by' => 'https://www.1c-bitrix.by/download/law/eula_cp.pdf',
		'kz' => 'https://www.1c-bitrix.kz/download/law/eula_cp.pdf',
		'en' => 'https://www.bitrix24.com/eula/',
		'br' => 'https://www.bitrix24.com.br/eula/',
		'fr' => 'https://www.bitrix24.fr/eula/',
		'pl' => 'https://www.bitrix24.pl/eula/',
		'it' => 'https://www.bitrix24.it/eula/',
		'la' => 'https://www.bitrix24.es/eula/',
	];

	public function getKey(): string
	{
		if ($this->key === null)
		{
			$licenseFile = Loader::getDocumentRoot() . '/bitrix/license_key.php';

			$LICENSE_KEY = '';
			if (file_exists($licenseFile))
			{
				include($licenseFile);
			}
			$this->key = ($LICENSE_KEY == '' || strtoupper($LICENSE_KEY) == 'DEMO' ? 'DEMO' : $LICENSE_KEY);
		}
		return $this->key;
	}

	public function getHashLicenseKey(): string
	{
		return md5($this->getKey());
	}

	public function getPublicHashKey(): string
	{
		return md5('BITRIX' . $this->getKey() . 'LICENCE');
	}

	public function isDemoKey(): bool
	{
		return $this->getKey() == 'DEMO';
	}

	public function getBuyLink(): string
	{
		return $this->getDomainStoreLicense()
			. '/key_update.php?license_key='
			. $this->getHashLicenseKey()
			. '&tobasket=y&lang='
			. LANGUAGE_ID;
	}

	public function getDocumentationLink(): string
	{
		$region = $this->getRegion();

		if (in_array($region, ['ru', 'kz', 'by']))
		{
			return 'https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=135&LESSON_ID=25720';
		}

		return 'https://training.bitrix24.com/support/training/course/index.php?COURSE_ID=178&LESSON_ID=25932&LESSON_PATH=17520.17562.25930.25932';
	}

	public function getDomainStoreLicense(): string
	{
		return self::DOMAINS_STORE_LICENSE[$this->getRegion()] ?? self::DOMAINS_STORE_LICENSE['ru'];
	}

	public function isDemo(): bool
	{
		return defined('DEMO') && DEMO === 'Y';
	}

	public function isTimeBound(): bool
	{
		return defined('TIMELIMIT_EDITION') && TIMELIMIT_EDITION === 'Y';
	}

	public function isEncoded(): bool
	{
		return defined('ENCODE') && ENCODE === 'Y';
	}

	public function getExpireDate(): ?Date
	{
		$date = (int)$GLOBALS['SiteExpireDate'];
		if ($date > 0)
		{
			return Date::createFromTimestamp($date);
		}

		return null;
	}

	public function getRegion(): ?string
	{
		if (Loader::includeModule('bitrix24'))
		{
			return \CBitrix24::getPortalZone();
		}

		$region = Option::get('main', '~PARAM_CLIENT_LANG');
		if (!empty($region))
		{
			return $region;
		}

		$region = $this->getRegionByVendor();
		if (!empty($region))
		{
			return $region;
		}

		return $this->getRegionByLanguage();
	}

	public function getEulaLink(): string
	{
		if (ModuleManager::isModuleInstalled('intranet'))
		{
			return self::URL_CP_EULA[$this->getRegion()] ?? self::URL_CP_EULA['en'];
		}

		return self::URL_BUS_EULA[$this->getRegion()] ?? self::URL_BUS_EULA['ru'];
	}

	private function getRegionByVendor(): ?string
	{
		$vendor = Option::get('main', 'vendor');
		if ($vendor === 'ua_bitrix_portal')
		{
			return 'ua';
		}
		if ($vendor === 'bitrix_portal')
		{
			return 'en';
		}
		if ($vendor === '1c_bitrix_portal')
		{
			return 'ru';
		}

		return null;
	}

	private function getRegionByLanguage(): ?string
	{
		$documentRoot = Application::getDocumentRoot();

		if (file_exists($documentRoot . '/bitrix/modules/main/lang/ua'))
		{
			return 'ua';
		}
		if (file_exists($documentRoot . '/bitrix/modules/main/lang/by'))
		{
			return 'by';
		}
		if (file_exists($documentRoot . '/bitrix/modules/main/lang/kz'))
		{
			return 'kz';
		}
		if (file_exists($documentRoot . '/bitrix/modules/main/lang/ru'))
		{
			return 'ru';
		}

		return null;
	}

	public function getPartnerId(): int
	{
		return (int)Option::get('main', '~PARAM_PARTNER_ID', 0);
	}

	public function getMaxUsers(): int
	{
		return (int)Option::get('main', 'PARAM_MAX_USERS', 0);
	}

	public function isExtraCountable(): bool
	{
		return Option::get('main', '~COUNT_EXTRA', 'N') === 'Y' && ModuleManager::isModuleInstalled('extranet');
	}

	public function getActiveUsersCount(Date $lastLoginDate = null)
	{
		$connection = Application::getConnection();

		if ($lastLoginDate !== null)
		{
			// logged in today
			$filter = "AND U.LAST_LOGIN > ".$connection->getSqlHelper()->convertToDbDate($lastLoginDate);
		}
		else
		{
			// logged in total
			$filter = "AND U.LAST_LOGIN IS NOT NULL";
		}

		if (ModuleManager::isModuleInstalled("intranet"))
		{
			$sql = "
				SELECT COUNT(DISTINCT U.ID)
				FROM
					b_user U
					INNER JOIN b_user_field F ON F.ENTITY_ID = 'USER' AND F.FIELD_NAME = 'UF_DEPARTMENT'
					INNER JOIN b_utm_user UF ON
						UF.FIELD_ID = F.ID
						AND UF.VALUE_ID = U.ID
						AND UF.VALUE_INT > 0
				WHERE U.ACTIVE = 'Y'
					" . $filter . "
			";
			$count = (int)$connection->queryScalar($sql);

			if ($this->isExtraCountable())
			{
				$groupId = (int)Option::get('extranet', 'extranet_group');
				if ($groupId > 0)
				{
					$sql = "
						SELECT COUNT(1)
						FROM
							b_user U
							INNER JOIN b_user_group UG ON UG.USER_ID = U.ID AND UG.GROUP_ID = " . $groupId . "
							LEFT JOIN (
								SELECT UF.VALUE_ID 
								FROM 
									b_user_field F
									INNER JOIN b_utm_user UF ON UF.FIELD_ID = F.ID AND UF.VALUE_INT > 0
								WHERE F.ENTITY_ID = 'USER' AND F.FIELD_NAME = 'UF_DEPARTMENT'
							) D ON D.VALUE_ID = U.ID
						WHERE U.ACTIVE = 'Y'
							" . $filter . "
							AND D.VALUE_ID IS NULL
					";
					$count += (int)$connection->queryScalar($sql);
				}
			}
		}
		else
		{
			$sql = "
				SELECT COUNT(ID)
				FROM b_user U
				WHERE U.ACTIVE = 'Y'
					" . $filter . "
			";
			$count = (int)$connection->queryScalar($sql);
		}

		return $count;
	}
}
