<?php

namespace Bitrix\Main\UpdateSystem;

use Bitrix\Main\Application;
use Bitrix\Main\Security\Cipher;
use Bitrix\Main\Security\SecurityException;

class HashCodeParser
{
	private string $hashCode;

	public function __construct(string $hashCode)
	{
		$this->hashCode = $hashCode;
	}

	/**
	 * @throws SecurityException
	 */
	public function parse()
	{
		$data = base64_decode($this->hashCode);
		$data = unserialize($data, ['allowed_classes' => false]);

		if (openssl_verify($data['info'], $data['signature'], $this->getPublicKeyPem(), "sha256WithRSAEncryption") == 1)
		{
			$licenseKey = Application::getInstance()->getLicense()->getHashLicenseKey();
			$cipher = new Cipher();

			$result = $cipher->decrypt($data['info'], $licenseKey);

			return unserialize($result, ['allowed_classes' => false]);
		}

		throw new SecurityException('Error verify openssl [HCPP01]');
	}

	private function getPublicKeyPem(): string
	{
		return '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6hcxIqiitUZRMwYiukSU
h9xa5fEDYlccbW3vj8Ava35vKqVN4iB9tqCX7jU86qAa2v37mbTF6pcY6HGPAhRF
bpnwXOY7YGxB1nSKZvE+jARbiLLBgZ1cG6Z0duu5i1XhpIRL1cN0Hh5fezpjXC6O
YxYq0nToHTjyRb1yczwtmiRwYqudXg/xWxppqwF0tUld3QBr3i68B8jqMm+TjdeA
u/fg1J0JGtR4/zK4G7YJNvhmuhrRGkyAQV0TVu5LEugSxjApRmIJQNHQMK0Eh93w
MZoFoPp9SgJ7GaFU8kzS+EQcntYxb1NHUJUIvTdiuRUeFKlyTdxIrH6CL//apMH3
FwIDAQAB
-----END PUBLIC KEY-----';
	}
}
