<?php

namespace Bitrix\Main\UpdateSystem;

use Bitrix\Main\Application;
use Bitrix\Main\License;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use Bitrix\Main\SiteTable;

class PortalInfo
{
	private License $license;

	public function __construct()
	{
		$this->license = Application::getInstance()->getLicense();
	}

	public function common(): array
	{
		global $DB;

		return [
			'LICENSE_KEY' => $this->license->getHashLicenseKey(),
			'lang' => LANGUAGE_ID,
			'utf' => defined('BX_UTF') ? 'Y' : 'N',
			'stable' => \COption::GetOptionString('main', 'stable_versions_only', 'Y'),
			'CANGZIP' => function_exists("gzcompress") ? "Y" : "N",
			'SUPD_DBS' => $DB->type,
			'XE' => (isset($DB->XE) && $DB->XE) ? "Y" : "N",
			'SUPD_URS' => $this->license->getActiveUsersCount(),
			'CLIENT_SITE' => $_SERVER["SERVER_NAME"],
			'spd' => \COption::GetOptionString('main', 'crc_code', ''),
			'dbv' => $this->getDBVersion(),
			'SUPD_VER' => defined("UPDATE_SYSTEM_VERSION_A") ? UPDATE_SYSTEM_VERSION_A : '',
			'SUPD_SRS' => $this->getClusterServersCount() ?? 'RU',
			'SUPD_CMP' => 'N',
			'SUPD_STS' => $this->getSitesCount() ?? 'RA',
			'LICENSE_SIGNED' => $this->getLicenseSign(),
			'CLIENT_PHPVER' => phpversion(),
			'NGINX' => \COption::GetOptionString("main", "update_use_nginx", "Y"),
			'SMD' => \COption::GetOptionString("main", "update_safe_mode", "N"),
			'VERSION' => SM_VERSION,
			'TYPENC' => $this->getLicenseType(),
			'CHHB' => $_SERVER["HTTP_HOST"],
			'CSAB' => $_SERVER["SERVER_ADDR"],
			'SUID' => $GLOBALS["APPLICATION"]->GetServerUniqID(),
		];
	}

	private function getDBVersion(): string
	{
		global $DB;
		$dbv = $DB->GetVersion();

		return $dbv !== false ? $dbv : "";
	}

	private function getClusterServersCount(): ?int
	{
		if (Loader::includeModule('cluster') && class_exists("CCluster"))
		{
			return \CCluster::getServersCount();
		}

		return null;
	}

	private function getSitesCount(): ?int
	{
		return SiteTable::getCount(['=ACTIVE' => 'Y']);
	}

	private function getLicenseSign(): string
	{
		require_once(Application::getDocumentRoot() . "/bitrix/modules/main/classes/general/update_client.php");
		$newLicenceSignedKey = \CUpdateClient::getNewLicenseSignedKey();
		return $newLicenceSignedKey."-".\COption::GetOptionString("main", $newLicenceSignedKey, "N");
	}

	public function getLicenseType(): string
	{
		if ($this->license->isDemo())
		{
			return 'D';
		}
		elseif ($this->license->isEncoded())
		{
			return 'E';
		}
		elseif ($this->license->isTimeBound())
		{
			return 'T';
		}
		else
		{
			return 'F';
		}
	}

	public function getModules(): array
	{
		require_once(Application::getDocumentRoot() . "/bitrix/modules/main/classes/general/update_client.php");
		$errors = '';
		$arClientModules = \CUpdateClient::GetCurrentModules($errors);
		$isExpertMode = (\CUpdateExpertMode::isEnabled() && \CUpdateExpertMode::isCorrectModulesStructure([]));
		if ($isExpertMode)
		{
			$arClientModules = \CUpdateExpertMode::processModulesFrom([], $arClientModules);
		}

		if (!empty($errors))
		{
			throw new SystemException($errors);
		}

		return $arClientModules;
	}

	public function getLanguages(): array
	{
		require_once(Application::getDocumentRoot() . "/bitrix/modules/main/classes/general/update_client.php");
		$errors = '';

		$languages = \CUpdateClient::GetCurrentLanguages($errors);
		if (!empty($errors))
		{
			throw new SystemException($errors);
		}

		return $languages;
	}
}
