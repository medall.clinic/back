<?php

namespace Bitrix\Main\UpdateSystem;

use Bitrix\Main\Application;
use Bitrix\Main\Result;
use Bitrix\Main\Security\SecurityException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\Json;

class ActivationSystem
{
	/**
	 * @throws SystemException
	 */
	public function reincarnate(Coupon $coupon): Result
	{
		// build request
		$builder = new ReincarnationRequestBuilder($coupon);
		$request = (new RequestFactory($builder))->build();
		// send request
		$response = $request->send();
		// parse response
		$parser = new UpdateServerDataParser($response);
		$licenseInfo = $parser->parse();

		if (isset($licenseInfo['ERROR']))
		{
			throw new SystemException(($licenseInfo['ERROR']['_VALUE'] ?? 'Unknown error').' [ASR01]');
		}

		$licenseInfo = $licenseInfo['RENT'] ?? [];
		if (empty($licenseInfo))
		{
			throw new SystemException('Not found license info [ASR02]');
		}
		$this->applyLicenseInfo($licenseInfo, $coupon->getKey());
		$result = new Result();

		return $result->setData($licenseInfo);
	}

	/**
	 * @throws SystemException
	 */
	protected function applyLicenseInfo(array $licenseInfo, string $key): void
	{
		$v1Value = $licenseInfo["V1"];
		$v2Value = $licenseInfo["V2"];

		if (empty($v1Value) || empty($v2Value))
		{
			throw new SystemException('Server response is not recognized [ASALI01]');
		}

		\COption::SetOptionString('main', '~SAAS_MODE', 'Y');
		\COption::SetOptionString('main', 'admin_passwordh', $v1Value);

		if (is_writable($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin"))
		{
			if ($fp = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/define.php", 'w'))
			{
				fwrite($fp, "<"."?Define(\"TEMPORARY_CACHE\", \"".$v2Value."\");?".">");
				fclose($fp);
			}
			else
			{
				throw new SystemException('File open fails [ASALI02]');
			}
		}
		else
		{
			throw new SystemException('Folder is not writable [ASALI03]');
		}

		if (isset($licenseInfo["DATE_TO_SOURCE"]))
		{
			\COption::SetOptionString('main', "~support_finish_date", $licenseInfo["DATE_TO_SOURCE"]);
		}
		if (isset($licenseInfo["MAX_SITES"]))
		{
			\COption::SetOptionString('main', "PARAM_MAX_SITES", intval($licenseInfo["MAX_SITES"]));
		}
		if (isset($licenseInfo["MAX_USERS"]))
		{
			\COption::SetOptionString('main', "PARAM_MAX_USERS", intval($licenseInfo["MAX_USERS"]));
		}
		if (isset($licenseInfo["MAX_USERS_STRING"]))
		{
			\COption::SetOptionString('main', "~PARAM_MAX_USERS", $licenseInfo["MAX_USERS_STRING"]);
		}
		if (isset($licenseInfo["DATE_TO_SOURCE_STRING"]))
		{
			\COption::SetOptionString('main', "~PARAM_FINISH_DATE", $licenseInfo["DATE_TO_SOURCE_STRING"]);
		}
		if (isset($licenseInfo["ISLC"]))
		{
			if (is_writable($_SERVER['DOCUMENT_ROOT']."/bitrix"))
			{
				if ($fp = fopen($_SERVER['DOCUMENT_ROOT']."/bitrix/license_key.php", "wb"))
				{
					fputs($fp, '<'.'?$LICENSE_KEY = "'.EscapePHPString($key).'";?'.'>');
					fclose($fp);
				}
				else
				{
					throw new SystemException('File open fails [ASALI04]');
				}
			}
			else
			{
				throw new SystemException('Folder is not writable [ASALI05]');
			}
		}
	}

	/**
	 * @throws SecurityException
	 * @throws SystemException
	 */
	public function activateByHash(string $hashKey): Result
	{
		$parser = new HashCodeParser($hashKey);
		$licenseInfo = $parser->parse();
		if (empty($licenseInfo))
		{
			throw new SystemException('Not found license info [ASAH01]');
		}

		$key = Application::getInstance()->getLicense()->getKey();
		$this->applyLicenseInfo($licenseInfo, $key);
		$result = new Result();

		return $result->setData($licenseInfo);
	}

	/**
	 * @throws SystemException
	 */
	public function sendInfoToPartner(string $name, string $phone, string $email): Result
	{
		$builder = new PartnerInfoRequestBuilder($name, $phone, $email);
		$request = (new RequestFactory($builder))->build();
		// send request
		$response = $request->send();
		$response = Json::decode($response);

		if (!isset($response['result']) || $response['result'] === 'error')
		{
			$debug = [
				'message' => 'Error send partner info [ASSITP01]',
				'response' => $response,
				'request' => $request
			];
			throw new SystemException(($response["error"] ?? 'Unknown error').' [ASSITP01]');
		}

		return (new Result())->setData($response);
	}
}
