<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
CModule::IncludeModule('iblock');
IncludeModuleLangFile(__FILE__);

$arSections = array('all' => GetMessage('CREATIVEBZ_PHONE_ALL'));
$dbRes = CIBlockSection::GetList(array('SORT' => 'ASC', 'NAME' => 'ASC'),
    array('IBLOCK_ID' => COption::GetOptionInt('creativebz.phone', 'IBLOCK_ID')), false, array('ID', 'NAME'));
while($arRes = $dbRes->Fetch()){
    $arSections[$arRes['ID']] = $arRes['NAME'];
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "SECTION_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CREATIVEBZ_PHONE_SECTION"),
            "TYPE" => "LIST",
            "VALUES" => $arSections,
            "DEFAULT" => 'all',
            "ADDITIONAL_VALUES" => "N",
            "REFRESH" => "N",
        ),
        "CLASS" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CREATIVEBZ_PHONE_CLASS"),
            "TYPE" => "STRING",
        ),
        "ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CREATIVEBZ_PHONE_ID"),
            "TYPE" => "STRING",
        ),
		"CACHE_TIME"  =>  array("DEFAULT"=>36000000),
	),
);
