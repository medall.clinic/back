<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CREATIVE_PHONE_NAME"),
	"DESCRIPTION" => GetMessage("CREATIVE_PHONE_DESC"),
	"CACHE_PATH" => "N",
	"SORT" => 100,
	"PATH" => array(
		"ID" => "creativebz",
		"NAME" => "Creativebz",
	    "SORT" => 2000,
	),
);