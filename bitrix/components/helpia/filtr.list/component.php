<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Context,
	Bitrix\Main\Type\DateTime,
	Bitrix\Main\Loader,
	Bitrix\Iblock;

if(!isset($arParams["CACHE_TIME"])) $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_ID"] = intval($arParams["~IBLOCK_ID"]);
if($arParams["IBLOCK_ID"]==''){
	$this->abortResultCache();
	ShowError(GetMessage("T_FILTR_IBLOCK_ID"));
	return;
}

if($arParams["PROPERTY_CODE"]==''){
	$this->abortResultCache();
	ShowError(GetMessage("T_FILTR_PROPERTY_CODE"));
	return;
}
$arParams["PROPERTY_CODE"] = strtoupper($arParams["PROPERTY_CODE"]);

$arResult = array();
$arResult['PROPERTY_CODE'] = $arParams["PROPERTY_CODE"];

// Получим все разделы
$resSections = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, Array('ID','NAME'));
while($arSection = $resSections->fetch() ){$arResult['sections'][$arSection["ID"]] = $arSection["NAME"];}

// получим все направления
$res =  CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, false, Array("PROPERTY_".$arParams["PROPERTY_CODE"]));

while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();
	$arResult['direction'][$arFields['PROPERTY_'.$arParams["PROPERTY_CODE"].'_ENUM_ID']] = $arFields['PROPERTY_'.$arParams["PROPERTY_CODE"].'_VALUE'];
}

$this->IncludeComponentTemplate();
?>