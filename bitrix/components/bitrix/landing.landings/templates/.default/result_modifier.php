<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$arException = [
    'Отзывы - Пушняков',
];
global $USER;
$currentUserId = $USER->GetID(); // Получить ID текущего пользователя
$userGroups = CUser::GetUserGroup($currentUserId); // Получить массив групп текущего пользователя

// Проверка принадлежности к группе с ID 112
$isInGroup = in_array(10, $userGroups);

if ($isInGroup) {
    foreach ($arResult['LANDINGS'] as $i => $item) {
        if (in_array(trim($item['TITLE']), $arException)) {
            unset($arResult['LANDINGS'][$i]);
        }
    }

    // Переиндексация массива (опционально)
    $arResult['LANDINGS'] = array_values($arResult['LANDINGS']);
}
