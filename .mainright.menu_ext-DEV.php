<?
\Bitrix\Main\Loader::IncludeModule('iblock');

$res = \CIBlockSection::GetList(
	[],
	["IBLOCK_ID" => IBLOCK_ID_PRICE, "DEPTH_LEVEL" => 1],
	false,
	["ID", "NAME", "CODE", "SECTION_PAGE_URL"]
);

$aMenuLinksExt = [];
$aMenuLinksExt[] = [
	"Прайс",
	"/price/",
	[],
	["FROM_IBLOCK" => IBLOCK_ID_PRICE, "IS_PARENT" => 1, "DEPTH_LEVEL" => 1],
	""
];

while($ar_res = $res->GetNext()){
	$aMenuLinksExt[] = [
		$ar_res["NAME"],
		$ar_res["SECTION_PAGE_URL"],
		[],
		["FROM_IBLOCK" => IBLOCK_ID_PRICE, "IS_PARENT" => "", "DEPTH_LEVEL" => 2],
		""
	];

}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);