<section class="page-section page-section--background-color page-section--background-color--gradient container">
  <div class="content">
    <h2 class="page-subsection page-subheader">специальные предложения</h2>
    <div class="slider page-subsection">
      <div class="slider__slides swiper">
        <div class="slider__wrapper swiper-wrapper">
          <div class="slide swiper-slide service-item">
            <div class="service-item__title">-35% на уход за волосами</div>
            <div class="service-item__description"><p>Курс из 3-х процедур комплекса мезотерапия + плазмотерапия волосяной области</p><p>за 29 900 ₽ вместо <span style="text-decoration: line-through">46 200 ₽</span></p></div>
            <picture class="service-item__picture">
              <source srcset="/images/socials/service-1_xs.webp 1x, /images/socials/service-1_xs@2x.webp 2x" media="(max-width: 478px)" type="image/webp">
              <source srcset="/images/socials/service-1.webp 1x, /images/socials/service-1@2x.webp 2x" media="(min-width:480px)" type="image/webp"><img class="service-item__image" src="/images/socials/service-1.jpg" alt="-35% на уход за волосами" title="-35% на уход за волосами" loading="lazy" decoding="async">
            </picture>
          </div>
          <div class="slide swiper-slide service-item">
            <div class="service-item__title">-35% на уход за лицом</div>
            <div class="service-item__description"><p>Чистка + пилинг + биоревитализация вокруг глаз</p><p>за 12 155 ₽ вместо <span style="text-decoration: line-through">18 700 ₽</span></p></div>
            <picture class="service-item__picture">
              <source srcset="/images/socials/service-2_xs.webp 1x, /images/socials/service-2_xs@2x.webp 2x" media="(max-width: 478px)" type="image/webp">
              <source srcset="/images/socials/service-2.webp 1x, /images/socials/service-2@2x.webp 2x" media="(min-width:480px)" type="image/webp"><img class="service-item__image" src="/images/socials/service-2.jpg" alt="-35% на уход за лицом" title="-35% на уход за лицом" loading="lazy" decoding="async">
            </picture>
          </div>
          <div class="slide swiper-slide service-item">
            <div class="service-item__title">-10% при записи с подругой</div>
            <div class="service-item__description"><p>при записи с подругой скидка 10% на все услуги косметологии</p></div>
            <picture class="service-item__picture">
              <source srcset="/images/socials/service-3_xs.webp 1x, /images/socials/service-3_xs@2x.webp 2x" media="(max-width: 478px)" type="image/webp">
              <source srcset="/images/socials/service-3.webp 1x, /images/socials/service-3@2x.webp 2x" media="(min-width:480px)" type="image/webp"><img class="service-item__image" src="/images/socials/service-3.jpg" alt="-10% при записи с подругой" title="-10% при записи с подругой" loading="lazy" decoding="async">
            </picture>
          </div>
          <div class="slide swiper-slide service-item">
            <div class="service-item__title">-50% на лечение целлюлита</div>
            <div class="service-item__description"><p>Первая процедура на аппарате EMTONE со скидкой 50%</p></div>
            <picture class="service-item__picture">
              <source srcset="/images/socials/service-4_xs.webp 1x, /images/socials/service-4_xs@2x.webp 2x" media="(max-width: 478px)" type="image/webp">
              <source srcset="/images/socials/service-4.webp 1x, /images/socials/service-4@2x.webp 2x" media="(min-width:480px)" type="image/webp"><img class="service-item__image" src="/images/socials/service-4.jpg" alt="-50% на лечение целлюлита" title="-50% на лечение целлюлита" loading="lazy" decoding="async">
            </picture>
          </div>
          <div class="slide swiper-slide service-item">
            <div class="service-item__title">консультация пластического хирурга — 0 ₽</div>
            <div class="service-item__description"><p>Запишитесь на бесплатную консультацию пластического хирурга и станьте ближе к фигуре мечты</p></div>
            <picture class="service-item__picture">
              <source srcset="/images/socials/service-5_xs.webp 1x, /images/socials/service-5_xs@2x.webp 2x" media="(max-width: 478px)" type="image/webp">
              <source srcset="/images/socials/service-5.webp 1x, /images/socials/service-5@2x.webp 2x" media="(min-width:480px)" type="image/webp"><img class="service-item__image" src="/images/socials/service-5.jpg" alt="консультация пластического хирурга — 0 ₽" title="консультация пластического хирурга — 0 ₽" loading="lazy" decoding="async">
            </picture>
          </div>
          <div class="slide swiper-slide service-item">
            <div class="service-item__title">-25% на чистку зубов</div>
            <div class="service-item__description"><p>Запишитесь на профессиональную чистку зубов и сделайте свою улыбку ярче</p></div>
            <picture class="service-item__picture">
              <source srcset="/images/socials/service-6_xs.webp 1x, /images/socials/service-6_xs@2x.webp 2x" media="(max-width: 478px)" type="image/webp">
              <source srcset="/images/socials/service-6.webp 1x, /images/socials/service-6@2x.webp 2x" media="(min-width:480px)" type="image/webp"><img class="service-item__image" src="/images/socials/service-6.jpg" alt="-25% на чистку зубов" title="-25% на чистку зубов" loading="lazy" decoding="async">
            </picture>
          </div>
        </div>
        <div class="slider__arrow slider__arrow--prev"></div>
        <div class="slider__arrow slider__arrow--next"></div>
        <div class="slider__pagination"></div>
      </div>
    </div>
    <div class="details-block">узнать подробности и записаться можно на 2 и 3 этажах клиники</div>
  </div>
</section>