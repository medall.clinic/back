<section class="page-section container">
	<div class="content">
		<h2 class="page-subheader">Записаться на приём</h2>

		<form class="form par contacts_to_bitrix24">

			<div class="form-input form-input--text">
				<input class="form-input__field" type="text" name="name" required="required" placeholder="Имя">
			</div>

			<div class="form-input form-input--text">
				<input class="form-input__field" type="text" name="second_name" required="required" placeholder="Фамилия">
			</div>

			<div class="form-input form-input--tel">
				<input class="form-input__field" type="tel" name="phone" required="required" placeholder="Телефон">
			</div>

			<div class="form-input form-input--textarea">
				<textarea class="form-input__field" name="comments" required="required" placeholder="Комментарии"></textarea>
			</div>

			<div class="form-input form-input--select">
				<select class="form-input__field" name="direction">
					<option value="">Выберите отделение</option>
					<option value="пластическая хирургия">пластическая хирургия</option>
					<option value="цифровая стоматология">цифровая стоматология</option>
					<option value="медицинская косметология">медицинская косметология</option>
					<option value="пересадка волос">пересадка волос</option>
					<option value="флебология и лечение трофических язв">флебология и лечение трофических язв</option>
					<option value="лабораторная служба">лабораторная служба</option>
				</select>
			</div>

			<button class="button" type="submit">Записаться</button>

			<input type="hidden" name="title" value="<?= $b24FormTitle ?>">

			<? foreach ($_GET as $paramName => $paramValue): ?>
				<? if(strpos($paramName, 'utm_') !== false): ?>
					<input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
				<? endif ?>
			<? endforeach ?>

		</form>

	</div>
</section>
