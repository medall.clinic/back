<section class="page-section page-section--background-color page-section--background-color--gradient container">
	<div class="content">
		<div class="grid grid--cols--12">
			<div class="grid__cell grid__cell--xs--12 grid__cell--s--8 grid__cell--m--6 grid__cell--l--5">
				<h1 class="page-header"><span>ЗДРАВСТВУЙТЕ! </span><span>БЛАГОДАРИМ </span><span>ЗА ДОВЕРИЕ </span><span>И ВЫБОР </span><span>КЛИНИКИ </span><span>MEDALL!</span></h1>
				<p>Подписывайтесь на наши социальные сети, там мы делимся новостями клиники и действующими акциями.</p>
			</div>
		</div>
	</div>
</section>
