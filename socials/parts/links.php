<section class="page-section container">
  <div class="content">
    <h2 class="page-subsection page-subheader">Мы в социальных сетях</h2>
    <div class="grid grid--justify--center page-subsection">
      <a class="button button--icon button--icon--vk grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://vk.com/medall.clinic" target="_blank">Вконтакте</a>
      <a class="button button--icon button--icon--tg grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://t.me/medall_clinic" target="_blank">Telegram</a>
      <a class="button button--icon button--icon--youtube grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://www.youtube.com/channel/UC3g-SUaeaT7wH5adRuUZjMg" target="_blank">YouTube</a>
      <a class="button button--icon button--icon--instagram grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://www.instagram.com/medall.clinic/" target="_blank">Instagram</a>
      <a class="button button--icon button--icon--zen grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://dzen.ru/id/62569cf18d98716e46246d00" target="_blank">Дзен</a>
    </div>
  </div>
</section>