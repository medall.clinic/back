<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./template/styles/style.css">
    <title></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
  </head>
  <body>
    <header class="header container <? /* hidden-hd hidden-xl hidden-l */ ?>">
      <div class="header__content content">
        <div class="header__item"><a class="header__logo" href="/"></a></div>
        <div class="header__item header__contacts">+ 7 (812) 660-39-85<br>Левашовский пр., д.24<br><a href="mailto:admin@medall.clinic">admin@medall.clinic</a></div>
      </div>
    </header>
    <section class="page-section page-section--background-color page-section--background-color--gradient container">
      <div class="content">
        <div class="grid grid--cols--12">
          <div class="grid__cell grid__cell--xs--12 grid__cell--s--8 grid__cell--m--6 grid__cell--l--5">
            <h1><span>Здравствуйте! </span><span>Будем </span><span>рады </span><span>увидеть </span><span>Вас </span><span>в клинике </span><span>MEDALL! </span></h1>
            <p>Оставьте свои контактные данные, с Вами свяжутся в ближайшее время.</p>
          </div>
        </div>
      </div>
    </section>
    <section class="page-section container ig-illustration">
      <div class="content">
        <div class="ig-illustration__inner">
          <div class="ig-illustration__image">
            <svg class="ig-illustration__img" viewBox="0 0 391 239" fill="none" xmlns="http://www.w3.org/2000/svg">
              <image href="./images/ig-illustration.jpg" clip-path="url(#ig-illustration-clip)" style="width: 100%"></image>
              <defs>
                <clipPath class="ig-illustration__clip" id="ig-illustration-clip">
                  <path d="M391.021 92.4849C399.36 156.612 317.804 221.735 210.496 235.689C103.187 249.642 9.43664 208.969 1.09816 144.842C-7.24032 80.7147 72.9908 17.4181 180.299 3.46462C287.608 -10.4888 382.683 28.358 391.021 92.4849Z" fill="#000"></path>
                </clipPath>
              </defs>
            </svg>
            <svg class="ig-illustration__border" viewBox="0 0 372 267" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M364.604 54.8958C377.544 84.2425 369.387 119.668 345.558 153.271C321.736 186.864 282.301 218.551 232.887 240.341C183.473 262.131 133.481 269.878 92.6128 264.811C51.7328 259.742 20.0762 241.873 7.13615 212.526C-5.80387 183.18 2.35303 147.754 26.1817 114.151C50.0034 80.5586 89.4389 48.8708 138.853 27.081C188.267 5.29124 238.259 -2.45537 279.127 2.61152C320.007 7.67987 351.664 25.5492 364.604 54.8958Z" stroke="#045658"></path>
            </svg>
          </div>
        </div>
      </div>
    </section>
    <section class="page-section container">
      <div class="content">
        <h2>Записаться на приём</h2>

        <form class="form par contacts_to_bitrix24">

          <div class="form-input form-input--text">
            <input class="form-input__field" type="text" name="name" required="required" placeholder="Имя">
          </div>

          <div class="form-input form-input--text">
            <input class="form-input__field" type="text" name="second_name" required="required" placeholder="Фамилия">
          </div>

          <div class="form-input form-input--tel">
            <input class="form-input__field" type="tel" name="phone" required="required" placeholder="Телефон">
          </div>

          <div class="form-input form-input--textarea">
            <textarea class="form-input__field" name="comments" required="required" placeholder="Комментарии"></textarea>
          </div>

          <div class="form-input form-input--select">
            <select class="form-input__field" name="direction">
              <option value="">Выберите отделение</option>
              <option value="пластическая хирургия">пластическая хирургия</option>
              <option value="цифровая стоматология">цифровая стоматология</option>
              <option value="медицинская косметология">медицинская косметология</option>
              <option value="пересадка волос">пересадка волос</option>
              <option value="флебология и лечение трофических язв">флебология и лечение трофических язв</option>
              <option value="лабораторная служба">лабораторная служба</option>
            </select>
          </div>

          <button class="button" type="submit">Записаться</button>

          <input type="hidden" name="title" value="Instagram-taplink">

            <? foreach ($_GET as $paramName => $paramValue): ?>
                <? if(strpos($paramName, 'utm_') !== false): ?>
                    <input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
                <? endif ?>
            <? endforeach ?>

        </form>

      </div>
    </section>
    <section class="page-section page-section--background-color page-section--background-color--dark container">
      <div class="content">
        <div class="grid grid--cols--12">
          <div class="grid__cell grid__cell--xs--12 grid__cell--l--4">
            <h2>Направления клиники Medall</h2>
          </div>
          <div class="grid__cell grid__cell--xs--12 grid__cell--l--8">
            <ol class="list-ordered par">
              <li class="list-ordered__item">отделение пластической хирургии
              </li>
              <li class="list-ordered__item">центр цифровой стоматологии
              </li>
              <li class="list-ordered__item">отделение медицинской косметологии
              </li>
              <li class="list-ordered__item">отделение по пересадке волос
              </li>
              <li class="list-ordered__item">отделение флебологии и лечения трофических язв
              </li>
              <li class="list-ordered__item">лабораторная служба
              </li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="page-section page-section--background-color page-section--background-color--gradient container">
      <div class="content">
        <div class="page-subsection">
          <div class="grid grid--align-items--center">
            <div class="grid__cell grid__cell--xs--12 grid__cell--l--8">
              <h2>Узнать больше об услугах и врачах</h2>
            </div>
            <div class="grid__cell grid__cell--xs--12 grid__cell--l--4">
              <a class="button button--icon button--icon--link par" href="/">сайт medal
              </a>
            </div>
          </div>
        </div>
        <div class="page-subsection">
          <div class="grid grid--cols--12">
            <div class="grid__cell grid__cell--xs--12 grid__cell--l--8">
              <h2>Мы в социальных сетях</h2>
            </div>
            <div class="grid__cell grid__cell--xs--12 grid__cell--l--4">
              <a class="button button--icon button--icon--instagram par" href="https://www.instagram.com/medall.clinic/">Instagram
              </a>
              <a class="button button--icon button--icon--youtube par" href="https://www.youtube.com/channel/UC3g-SUaeaT7wH5adRuUZjMg">YouTube
              </a>
              <a class="button button--icon button--icon--tiktok par" href="https://vm.tiktok.com/ZSJWcGp4n/">TikTok
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
  <script src="./template/scripts/script.js"></script>
  <script src="/bitrix/templates/landing/scripts/jquery-3.2.1.min.js"></script>
  <script src="/bitrix/templates/landing/scripts/bitrix24.js"></script>
</html>
