<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Medall - Instagram-taplink");
?>
<main>
  <? include __DIR__."/parts/greetings.php" ?>
  <? include __DIR__."/parts/illustration.php" ?>
  <? include __DIR__."/parts/links.php" ?>
  <? include __DIR__."/parts/offers.php" ?>
  <? include __DIR__."/parts/branches.php" ?>
</main>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
