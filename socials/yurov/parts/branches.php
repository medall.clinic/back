<section class="page-section page-section--background-color page-section--background-color--dark container">
	<div class="content">
		<div class="grid grid--cols--12">
			<div class="grid__cell grid__cell--xs--12 grid__cell--l--4">
				<h2 class="page-subheader">Направления клиники Medall</h2>
			</div>
			<div class="grid__cell grid__cell--xs--12 grid__cell--l--8">
				<ol class="list-ordered par">
					<li class="list-ordered__item">отделение пластической хирургии
					</li>
					<li class="list-ordered__item">центр цифровой стоматологии
					</li>
					<li class="list-ordered__item">отделение медицинской косметологии
					</li>
					<li class="list-ordered__item">отделение по пересадке волос
					</li>
					<li class="list-ordered__item">отделение флебологии и лечения трофических язв
					</li>
					<li class="list-ordered__item">лабораторная служба
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
