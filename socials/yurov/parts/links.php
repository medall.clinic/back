
<section class="page-section page-section--background-color page-section--background-color--light container">
        <div class="content">
          <h2 class="page-subsection page-subheader">Контакты</h2>
          <div class="grid grid--justify--center page-subsection">
            <a class="button button--icon button--icon--instagram grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://instagram.com/yurovtaras?igshid=YmMyMTA2M2Y=" target="_blank">личный блог
            </a>
            <a class="button button--icon button--icon--instagram grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://instagram.com/dr.yurov?igshid=YmMyMTA2M2Y=" target="_blank">профессиональный блог
            </a>
            <a class="button button--icon button--icon--website grid__cell grid__cell--l--4 grid__cell--xs--12 par" href="https://doctoryurov.ru/" target="_blank">мой сайт
            </a>
          </div>
        </div>
      </section>