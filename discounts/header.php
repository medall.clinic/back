<?php

$arSelect = array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_BANNER_MOBILE", "PROPERTY_BANNER_DESKTOP", 'PREVIEW_PICTURE');
$arFilter = array("IBLOCK_ID" => IntVal(67), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", '=CODE' => 'home');
$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$arFields = [];
$picture = [];

$discounts_header_m_png = '';
$discounts_header_m_png_2x = '';
$discounts_header_m_webp = '';
$discounts_header_m_webp_2x = '';

$discounts_header_s_webp = '';
$discounts_header_s_webp_2x = '';
$discounts_header_s_png = '';
$discounts_header_s_png_2x = '';

$previewPictureDesktop = [];
$previewPictureMobile = [];


while ($ob = $res->GetNext()) {
    $arFields[] = $ob;
}

if (!empty($arFields[0]['PROPERTY_BANNER_DESKTOP_VALUE'])) {
    $previewPictureDesktop = CFile::GetFileArray($arFields[0]['PROPERTY_BANNER_DESKTOP_VALUE']);
    $discounts_header_m_png = $previewPictureDesktop['SRC'];
    $discounts_header_m_webp = WebPHelper::getOrCreateWebPUrl($discounts_header_m_png,[800,1600]);

}

if (!empty($arFields[0]['PROPERTY_BANNER_MOBILE_VALUE'])) {
    $previewPictureMobile = CFile::GetFileArray($arFields[0]['PROPERTY_BANNER_MOBILE_VALUE']);
    $discounts_header_s_png = $previewPictureMobile['SRC'];
    $discounts_header_s_webp = WebPHelper::getOrCreateWebPUrl($discounts_header_s_png);

}
?>
<div class="discounts-header">

	<h1 class="discounts-header__title">акции и специальные предложения клиники medall</h1>

    <picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($discounts_header_s_png, [800, 1600]) ?>" type="image/webp">

        <img class="discounts-header__image hidden-hd hidden-xl hidden-l hidden-m hidden-s" src="<?php echo $discounts_header_s_png?>" loading="lazy" decoding="async" alt="акции и специальные предложения клиники medall">
    </picture>

    <picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($discounts_header_m_png, [800, 1600]) ?>" type="image/webp">

        <img class="discounts-header__image hidden-xs" src="<?php echo $discounts_header_m_png?>" loading="lazy" decoding="async" alt="акции и специальные предложения клиники medall">
    </picture>

</div>
