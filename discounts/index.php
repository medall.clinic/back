<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акции");
$APPLICATION->SetPageProperty("description", "Акции. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic.");
?>
<? include __DIR__."/header.php" ?>
<? /* include __DIR__."/gift.php" */ ?>
<? include __DIR__."/plastic.php" ?>
<? include __DIR__."/dentistry.php" ?>
<? include __DIR__."/cosmetology.php" ?>
<? include __DIR__."/gynecology.php" ?>
<? include __DIR__."/hair.php" ?>
<? include __DIR__."/form-wrapper.php" ?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
