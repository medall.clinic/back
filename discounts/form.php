<form
	class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 discounts-form contacts_to_bitrix24 form form-ajax home-form" data-is-validate="true" data-is-answer-in-popup="true" data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации" id="discounts-form"
>

	<h2 class="discounts-subtitle">заполните данные и мы совсем скоро свяжемся с вами</h2>

	<p class="discounts-form__description">В комментарии напишите, какая услуга и акция вас интересует</p>

	<label class="form-input form-input--text discounts-form__field discounts-form__field">
		<input class="form-input__field" type="text" placeholder="ФИО *" name="name" data-validation-required>
	</label>

	<label class="form-input form-input--tel discounts-form__field discounts-form__field">
		<input class="form-input__field" type="tel" placeholder="Телефон *" name="phone" data-validation-required>
	</label>

	<? /* <label class="form-input form-input--email discounts-form__field discounts-form__field">
		<input class="form-input__field" type="email" placeholder="Электронная почта" name="email">
	</label> */?>

	<label class="form-input form-input--textarea discounts-form__field discounts-form__field">
		<textarea class="form-input__field" placeholder="Комментарии" name="comment"></textarea>
	</label>

	<p class="discounts-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</p>

    <input type="hidden" name="title" value="страница Акций">
		<input type="hidden" name="action" value="consultation_register">

	<button
        class="button button--type--submit discounts-form__submit"
        type="submit"
    >Оставить заявку</button>

	<? if(!empty($_GET)): ?>
		<? foreach ($_GET as $paramName => $paramValue): ?>
			<? if(strpos($paramName, 'utm_') !== false): ?>
                <input type="hidden" name="<?= $paramName ?>" value="<?= $paramValue ?>">
			<? endif ?>
		<? endforeach ?>
	<? endif ?>

</form>

<style>
    .invalid-feedback{
        padding: 5px;
        color: IndianRed;
        font-size: 0.87rem;
        opacity: 0.8;
        bottom: 0;
    }
    input.is-invalid, textarea.is-invalid{
        border-color: IndianRed;
    }
</style>
