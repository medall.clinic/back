<?php
$arSelect = [
    'ID',
    'NAME',
    'DATE_ACTIVE_FROM',
    'PROPERTY_657',
    'PROPERTY_658',
    'PROPERTY_DATA_TIME',
    'PREVIEW_PICTURE',
    'IBLOCK_SECTION_ID',
    'PREVIEW_TEXT',
    'PROPERTY_BUTTON_TEXT',
    'PROPERTY_BUTTON_LINK',
];
$arFilter = ['IBLOCK_ID' => IntVal(67), 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => 472];
$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, false, $arSelect);
$arFields = [];
while ($ob = $res->Fetch()) {
    $arFields[] = $ob;
}
$previewPicture = [];
$sectionName = '';
$arSelectSection = ['NAME'];
$arFilterSection = ['IBLOCK_ID' => IntVal(67), 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', 'ID' => 472];
$arFieldsSection = [];
$resSection = CIBlockSection::GetList([], $arFilterSection, false, false, $arSelectSection);

while ($obSection = $resSection->GetNext()) {
    $arFieldsSection[] = $obSection;
}
$sectionName = $arFieldsSection[0]['NAME']


?>
<?php if (!empty($arFields[0]['NAME'])) { ?>
    <section class='page-section discounts-section'>
        <?php if (!empty($sectionName)) { ?>
            <h2 class="discounts-subtitle discounts-section__title"><?php echo $sectionName ?></h2>
        <?php } ?>
        <?php foreach ($arFields as $field) {
            $arButtons = CIBlock::GetPanelButtons(
                67,
                $field['ID'], // указываем либо ID элемента, либо ID раздела ниже
                $field['IBLOCK_SECTION_ID']
            );
            $editLink = $arButtons["edit"]["edit_element"]["ACTION_URL"];
            $deleteLink = $arButtons["edit"]["delete_element"]["ACTION_URL"];

            if (!empty($field['PREVIEW_PICTURE'])) {
                $previewPicture = $field['PREVIEW_PICTURE'];

                $previewPicture = CFile::GetFileArray($previewPicture);

            }


            ?>


            <?php
            $comp = new CBitrixComponent;
            $comp->AddEditAction($field['ID'], $editLink, CIBlock::GetArrayByID(67, "ELEMENT_EDIT"));
            $comp->AddDeleteAction($field['ID'], $deleteLink, CIBlock::GetArrayByID(67, "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
            <div class='container discounts-item' id="<?= $comp->GetEditAreaId($field['ID']) ?>">
                <div class='content discounts-item__inner'>
                    <?php if (!empty($field['NAME'])) { ?>
                        <h3 class='discounts-item__title'>
                            <?php echo $field['NAME'] ?>
                        </h3>
                    <?php } ?>
                    <?php if (!empty($field['PREVIEW_TEXT'])) { ?>
                        <p class='discounts-item__description'><?php echo $field['PREVIEW_TEXT'] ?></p>
                    <?php } ?>

                    <?php
                    if ($field['PROPERTY_DATA_TIME_VALUE']) {
                        ?>
                        <div class='discounts-item__date'>Акция действует до
                            <?php echo $field['PROPERTY_DATA_TIME_VALUE'] ?> г.
                        </div>

                    <?php } ?>
                    <?php if (
                        !empty($field['PROPERTY_BUTTON_LINK_VALUE'])
                        && !empty($field['PROPERTY_BUTTON_TEXT_VALUE'])
                    ) { ?>
                        <a class='button button--secondary button--size--small discounts-item__button'
                           href='<?php echo $field['PROPERTY_BUTTON_LINK_VALUE'] ?>'>
                            <?php echo $field['PROPERTY_BUTTON_TEXT_VALUE'] ?>
                        </a>
                    <?php } elseif(
                        empty($field['PROPERTY_BUTTON_TEXT_VALUE'])
                        && !empty($field['PROPERTY_BUTTON_LINK_VALUE'])
                    ) { ?>
                        <a class='button button--secondary button--size--small discounts-item__button'
                           href='<?php echo $field['PROPERTY_BUTTON_LINK_VALUE'] ?>'>
                            Получить предложение
                        </a>
                    <?php } elseif(
                        !empty($field['PROPERTY_BUTTON_TEXT_VALUE'])
                        && empty($field['PROPERTY_BUTTON_LINK_VALUE'])
                    ) { ?>
                        <a class='button button--secondary button--size--small discounts-item__button'
                           href='#discounts-form'>
                            <?php echo $field['PROPERTY_BUTTON_TEXT_VALUE'] ?>
                        </a>
                    <?php } else { ?>
                        <a class='button button--secondary button--size--small discounts-item__button'
                           href='#discounts-form'>Получить предложение
                        </a>
                    <?php } ?>

                    <picture>
                        <source
                                srcset="<?= WebPHelper::getOrCreateWebPUrl($previewPicture['SRC'], [800, 1600]) ?>"
                                type="image/webp"
                        >
                        <?php if (!empty($previewPicture['SRC'])) { ?>
                            <img class='discounts-item__image' src='<?php echo $previewPicture['SRC'] ?>' loading='lazy'
                                 decoding='async' alt="<?php echo $field['NAME'] ?>">

                        <?php } ?>
                    </picture>
                </div>
            </div>

        <?php } ?>
    </section>

<?php } ?>
