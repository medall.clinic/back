<section class="page-section discounts-section">

	<h2 class="discounts-subtitle discounts-section__title">Клиника MEDALL</h2>

	<div class="container discounts-item">
		<div class="content discounts-item__inner">
			<h3 class="discounts-item__title">Подарочный сертификат</h3>
			<p class="discounts-item__description">Подарите своим близким подарочный сертификат на все виды услуг. Сертификат можно приобрести на сумму от 5 000 ₽ до 1 000 000 ₽</p>
			<a class="button button--secondary button--size--small discounts-item__button" href="#discounts-form">Получить предложение</a>
			<picture>
				<source srcset="/images/discounts/podarochnyj-sertifikat_s.webp, /images/discounts/podarochnyj-sertifikat_s@2x.webp 2x" type="image/webp" media="(max-width: 760px)">
				<source srcset="/images/discounts/podarochnyj-sertifikat_m.webp, /images/discounts/podarochnyj-sertifikat_m@2x.webp 2x" type="image/webp" media="(min-width: 761px) and (max-width: 1200px)">
				<source srcset="/images/discounts/podarochnyj-sertifikat_l.webp, /images/discounts/podarochnyj-sertifikat_l@2x.webp 2x" type="image/webp" media="(min-width: 1201px)">
				<source srcset="/images/discounts/podarochnyj-sertifikat_s.jpg, /images/discounts/podarochnyj-sertifikat_s@2x.jpg 2x" media="(max-width: 760px)">
				<source srcset="/images/discounts/podarochnyj-sertifikat_m.jpg, /images/discounts/podarochnyj-sertifikat_m@2x.jpg 2x" media="(min-width: 761px) and (max-width: 1200px)">
				<source srcset="/images/discounts/podarochnyj-sertifikat_l.jpg, /images/discounts/podarochnyj-sertifikat_l@2x.jpg 2x" media="(min-width: 1201px)"><img class="discounts-item__image" src="/images/discounts/podarochnyj-sertifikat.jpg" loading="lazy" decoding="async">
			</picture>
		</div>
	</div>

</section>