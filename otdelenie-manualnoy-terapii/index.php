<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<? $APPLICATION->AddHeadScript("/bitrix/templates/landing/scripts/bitrix24.js"); ?>
<? $APPLICATION->SetPageProperty("description", "Отделение мануальной терапии в Спб. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic."); ?>
<? $pathLanding24 = "/bitrix/templates/corp_services_gray2_new/css/landing24" ?>
<? /* $APPLICATION->SetAdditionalCSS($pathLanding24."/font-awesome.min.css"); ?>
<? $APPLICATION->SetAdditionalCSS($pathLanding24."/bootstrap.min.css"); ?>
<? $APPLICATION->SetAdditionalCSS($pathLanding24."/themes_core.min.css"); ?>
<? $APPLICATION->SetAdditionalCSS($pathLanding24."/style.min.css"); ?>
<? $APPLICATION->SetAdditionalCSS($pathLanding24."/photography.min.css"); */ ?>
<? $APPLICATION->SetAdditionalCSS($pathLanding24."/service.css"); ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"service",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "2075",
		"FIELD_CODE" => array("PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE",""),
		"IBLOCK_ID" => "38",
		"IBLOCK_TYPE" => "service",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(
			"slider_card",
			"slider_card_add_content",

			"links_block",
			"links_block_add_content",

			"general_desc",
			"general_desc_items",
			"general_desc_add_content",

			"advantages",
			"advantages_items",
			"advantages_add_content",

			"video",

			"stages",
			"stages_items",
			"stages_add_content",

			"recommendations",
			"recommendations_items",
			"recommendations_add_content",

			"prices",
			"prices_add_content",

			"alternative_techniques",
			"alternative_techniques_add_content",

			"reviews",
			"reviews_add_content",

			"doctor_directions",
			"doctor_directions_add_content",

			"department_info",
			"department_info_add_content",

			"feedback_add_content",

			"installment_add_content",

			"aside_block_articles_bg",
			"aside_block_articles_wo_bg",

			"vert_sort",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
);?>
