<?
class MainLeftMenuExtModel
{
	public function __construct()
	{
		\Bitrix\Main\Loader::IncludeModule('iblock');
	}

	public function getMenuLinksExt(): array
	{
		$sectionsId = $this->getSectionsId();
		$services = $this->getServices($sectionsId);
		$menuLinksExt = $this->makeMenuLinksExt($services);
		$menuLinksExt = $this->addManualMenuLinksExt($menuLinksExt);

		return $menuLinksExt;
	}

	private function getSectionsId()
	{
		$sectionsServiceId = $this->getSectionsServiceId();
		$sectionsLandingId = $this->getSectionsLandingId();

		$sectionsId = array_merge($sectionsServiceId, $sectionsLandingId);

		return $sectionsId;
	}

	private function getSectionsServiceId()
	{
		$sectionsId = [];

		$res = \CIBlockSection::GetList(
			[],
			["IBLOCK_ID" => IBLOCK_ID_SERVICE, "DEPTH_LEVEL" => 1],
			false,
			["ID", "NAME", "CODE"]
		);

		while($ar_res = $res->Fetch()){

			$skip = ["Пересадка волос", "Флебология", "Пластика", "Отделение мануальной терапии"];
			if(in_array($ar_res['NAME'], $skip)){
				continue;
			}

			$sectionsId[$ar_res['ID']] = Array(
				"ID" => $ar_res['ID'],
				"NAME" => $ar_res['NAME'],
				"CODE" => $ar_res['CODE'],
			);
		}

		return $sectionsId;
	}

	private function getSectionsLandingId()
	{
		$sectionsId = [];

		$res = \CIBlockSection::GetList(
			[],
			["IBLOCK_ID" => IBLOCK_ID_LANDINGS, "SECTION_ID" => SECTION_ID_LANDINGS_SERVICE, "ACTIVE" => "Y"],
			false,
			["ID", "NAME", "CODE"]
		);

		while($ar_res = $res->Fetch()){
			$sectionsId[$ar_res['ID']] = Array(
				"ID" => $ar_res['ID'],
				"NAME" => $ar_res['NAME'],
				"CODE" => $ar_res['CODE'],
			);
		}

		return $sectionsId;
	}

	private function getServices(array $sectionsId)
	{
		$services = [];

		foreach($sectionsId as $k => $v){
			$intCount = CIBlockSection::GetCount(array('SECTION_ID' => $v['ID']));

			if($intCount != 0){
				$resSections = \CIBlockSection::GetList(Array(), Array('SECTION_ID' => $v['ID'], "ACTIVE"=>"Y"), false, Array('ID','NAME'));
				while($arSection = $resSections->fetch()){

					$services[$v['NAME']][$arSection['ID']]['NAME']= $arSection['NAME'];

					$res = CIBlockElement::GetList(
						[],
						[
							"IBLOCK_SECTION_ID"=> $arSection['ID'],
							"ACTIVE"=>"Y",
							"CHECK_PERMISSIONS" => "Y"
						],
						false,
						false,
						['ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL']
					);

					while($ob = $res->GetNextElement()){
						$arProps = $ob->GetFields();
						$services[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['NAME']= $arProps['NAME'];
						$services[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['CODE']= $v['CODE'];
						$services[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['ELEMENT_CODE']= $arProps['CODE'];
						$services[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['ELEMENT_URL']= $arProps['DETAIL_PAGE_URL'];
					}
				}
			}
			else{
				$res = CIBlockElement::GetList(
					Array(),
					Array("SECTION_ID"=>$v['ID'], "ACTIVE"=>"Y"),
					false,
					false,
					Array('ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL')
				);

				if($res->SelectedRowsCount() > 0){
					while($ob = $res->GetNextElement()){
						$arProps = $ob->GetFields();
						$services[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['NAME']= $arProps['NAME'];
						$services[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['CODE']= $v['CODE'];
						$services[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['ELEMENT_CODE']= $arProps['CODE'];
						$services[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['ELEMENT_URL']= $arProps['DETAIL_PAGE_URL'];
					}
				}else{
					$services[$v['NAME']] = [];
				}
			}
		}

		return $services;
	}

	private function makeMenuLinksExt(array $services)
	{
		$aMenuLinksExt = [];

		foreach($services as $k => $v){

			if(mb_strtolower($k) == "косметология"){
				$urlMainDirection = "/cosmetology/";
			}elseif (mb_strtolower($k) == "стоматология"){
				$urlMainDirection = "/dentistry/";
			}elseif (mb_strtolower($k) == "пластическая хирургия"){
				$urlMainDirection = "/plastic/";
			}else{
				$urlMainDirection = "";
			}

			$aMenuLinksExt[] = Array(
				$k,
				$urlMainDirection,
				Array(),
				Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
				""
			);

			foreach($v as $j => $l){

				if($l['NAME']!=''){
					$DEPTH_LEVEL = '3';
					$aMenuLinksExt[] = Array(
						$l['NAME'],
						"",
						Array(),
						Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"2"),
						""
					);
				}
				else{
					$DEPTH_LEVEL = '2';
				}

				if(!empty($l['SECTION'])){
					foreach($l['SECTION'] as $h => $r){

						if(!empty($r["ELEMENT_URL"])) {
							$url = $r["ELEMENT_URL"] . "/";
						}elseif(!empty($r["ELEMENT_CODE"])){
							$url = $r['CODE']."/".$r["ELEMENT_CODE"]."/";
						}else{
							$url = $r['CODE']."/detail.php?ID=".$h;
						}

						$aMenuLinksExt[] = Array(
							$r['NAME'],
							$url,
							Array(),
							Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>$DEPTH_LEVEL),
							""
						);
					}
				}

			}
		}

		return $aMenuLinksExt;
	}

	private function addManualMenuLinksExt(array $menuLinksExt)
	{
		$menuLinksExt[] = [
			"Отделение мануальной терапии",
			"/otdelenie-manualnoy-terapii/",
			[],
			["IS_EXTERNAL" => "1"]
		];

		$menuLinksExt[] = [
			"Пересадка волос",
			"https://netvolos.ru",
			[],
			["IS_EXTERNAL" => "1"]
		];

		$menuLinksExt[] = [
			"Флебология",
			"https://yazv.net",
			[],
			["IS_EXTERNAL" => "1"]
		];

		$menuLinksExt[] = [
			"Лабораторная диагностика",
			"https://medall.space",
			[],
			["IS_EXTERNAL" => "1"]
		];

		return $menuLinksExt;
	}

}
