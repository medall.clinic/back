<?
// для каждой секции нужно добавить имя пункта меню.
// если название не указано -> не создавать пункт меню

$aMenuLinks = [];

CModule::IncludeModule("iblock");

$elementCode = filterCode($_REQUEST["ELEMENT_CODE"]);

$arOrder = [];
$arFilter = ["IBLOCK_ID" => IBLOCK_ID_LANDINGS, "CODE" => $elementCode];
$arGroupBy = false;
$arNavStartParams = [];
$arSelectFields = ["IBLOCK_ID", "ID", "NAME"];

$res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
$bxElement = $res->GetNextElement();

if($bxElement){
	$arResult = $bxElement->GetFields();
	$arResult["PROPERTIES"]  = $bxElement->GetProperties();

	$path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/landing/components/bitrix/news.detail/landing/code/init.php";
	$models = require $path;

	if($models){
		/** @val $model Medreclama\Landing\Landing */
		foreach ($models as $model) {
			if($model->menuItem->isExist()){
				$aMenuLinks[] = [$model->menuItem->title, $model->menuItem->link];
			}
		}
	}
}