import os
import re
from lxml import etree
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet

xmlPath =  os.path.join('import', 'Price.xml')

def clean_text(text):
    # Удаляем цифры и символы, подобные "2 - "
    return re.sub(r'\d+\s*-\s*', '', text)


def extract_data_from_xml(file_path):
    tree = etree.parse(file_path)
    root = tree.getroot()
    ns = {'ns': 'S2'}

    data = []
    column_headers = ['Код', 'Артикул', 'Услуга', 'Наименование услуги по ОКМУ федерального справочника', 'Цены']
    data.append(column_headers)

    parent_uid = '052676d9-74e7-11ef-82be-0015179e7828'

    parent_services = {}
    # Гинекология
    # Эстетическая гинекология
    # Аппартная гинекология
    # Флебология
    # Трихология
    # Сестринские манипуляции
    # Импланты
    # Компрессионное белье
    # Ассистенция врача-пластического хирурга длительностью 4-5 часов
    # Ассистенция врача-пластического хирурга длительностью 6 и более часов
    # Ассистенция врача-пластического хирурга длительностью до 3 часов
    # Осмотр врача хирурга в отделении стационара
    # Осмотр врача-пластического хирурга в отделении стационара
    # Прием (осмотр, консультация) врача-пластического хирурга к.м.н. первичный
    # Осмотр (консультация) врачом-анестезиологом-реаниматологом
    # первичный
    execute_remove = [

        '82e12a40-508b-11ee-b639-0015179e7828',
        '52151e31-5398-11ee-b63a-0015179e7828',
        '0e7d65e6-5c28-11ee-b63a-0015179e7828',
        '1e70cd0e-770c-11ec-a06c-0015179e7828',
        '1e70cceb-770c-11ec-a06c-0015179e7828',
        'fad7c2da-74c9-11ee-b63f-0015179e7828',
        '699f0578-8189-11ef-82dc-0015179e7828',
        '763ae6bf-8189-11ef-82dc-0015179e7828',
        '2622fa64-68ee-11ef-9e9f-0015179e7828',
        '3ce14ec2-68ee-11ef-9e9f-0015179e7828',
        'f66ee5d0-68ed-11ef-9e9f-0015179e7828',
        '294ec2c8-0067-11ee-93aa-0015179e7828',
        'dcdddd0b-8fcb-11ec-af2e-0015179e7828',
        '106e3575-6135-11ee-b63c-0015179e7828',
        'f89287b9-6e75-11ee-b63f-0015179e7828',
        '71573bbd-864e-11ef-82f2-0015179e7828',
    ]
    execute_add = [
        '4e94bc85-8013-11ef-82d5-0015179e7828'
    ]

    for catalog in root.findall('ns:Каталог', namespaces=ns):
        is_folder = catalog.find('ns:ЭтоПапка', namespaces=ns)
        uid = catalog.find('ns:UID', namespaces=ns)
        parent_id = catalog.find('ns:Родитель', namespaces=ns)

        if is_folder is not None and is_folder.text == 'true' and uid is not None:
            if parent_id is not None and parent_id.text == parent_uid and uid.text not in execute_remove or parent_id.text in execute_add:
                parent_services[uid.text] = {
                    'catalog': catalog,
                    'children': []
                }

    for catalog in root.findall('ns:Каталог', namespaces=ns):
        is_folder = catalog.find('ns:ЭтоПапка', namespaces=ns)
        parent_id = catalog.find('ns:Родитель', namespaces=ns)
        uid = catalog.find('ns:UID', namespaces=ns)

        if is_folder is not None and is_folder.text == 'false' and parent_id is not None:
            if parent_id.text in parent_services and (uid is None or uid.text not in execute_remove):
                code = catalog.find('ns:Код', namespaces=ns)
                artiqul = catalog.find('ns:Артикул', namespaces=ns)
                serviceName = catalog.find('ns:Наименование', namespaces=ns)
                serviceNameFull = catalog.find('ns:ПолноеНаименование', namespaces=ns)
                price = catalog.find('ns:Цена', namespaces=ns)

                code_text = code.text if code is not None and code.text is not None else ''
                artiqul_text = artiqul.text if artiqul is not None and artiqul.text is not None else ''
                serviceName_text = serviceName.text if serviceName is not None and serviceName.text is not None else ''
                serviceNameFull_text = serviceNameFull.text if serviceNameFull is not None and serviceNameFull.text is not None else ''

                price_value = price.text.strip() if price is not None and price.text is not None else '0'
                price_value_formatted = f"{int(price_value):,}".replace(",",
                                                                        " ") + " ₽" if price_value != '0' else '0 ₽'

                if price_value != '0':
                    parent_services[parent_id.text]['children'].append(
                        [code_text, artiqul_text, serviceName_text, serviceNameFull_text, price_value_formatted])

    for parent_uid, parent_info in parent_services.items():
        # Проверяем, нужно ли генерировать запись для родительской услуги
        if parent_info['children'] and parent_uid not in execute_remove:  # Услуга должна иметь дочерние элементы
            parent_name = clean_text(parent_info['catalog'].find('ns:Наименование', namespaces=ns).text)
            data.append(['', '', parent_name, '', ''])  # Заголовок родительской услуги
            for child in parent_info['children']:
                data.append(
                    [child[0], child[1], '  ' + child[2], child[3], child[4]])  # Добавляем дочернюю услугу с отступом

    return data


def create_pdf_with_table(file_name, data):
    # Путь к шрифту Roboto
    font_path = os.path.join('font', 'Roboto-Regular.ttf')
    bold_font_path = os.path.join('font', 'Roboto-Bold.ttf')

    # Регистрация шрифтов Roboto
    pdfmetrics.registerFont(TTFont('Roboto', font_path))
    pdfmetrics.registerFont(TTFont('RobotoBold', bold_font_path))

    # Создаем документ
    doc = SimpleDocTemplate(file_name, pagesize=[1180, 3100])
    elements = []

    # Стиль для текста
    styles = getSampleStyleSheet()
    style = styles['Normal']
    style.fontName = 'Roboto'
    style.fontSize = 12
    style.alignment = 1  # По центру

    # Стиль для заголовка таблицы
    header_style = getSampleStyleSheet().get('Normal')
    header_style.fontName = 'RobotoBold'  # Жирный шрифт для заголовков
    header_style.fontSize = 12
    header_style.alignment = 1  # По центру

    # Стиль для ячеек столбца "Услуга"
    left_style = getSampleStyleSheet().get('Normal')
    left_style.fontName = 'Roboto'
    left_style.fontSize = 12
    left_style.alignment = 0  # По левому краю

    # Форматирование данных
    formatted_data = []
    for row_idx, row in enumerate(data):
        formatted_row = []
        for col_idx, item in enumerate(row):
            # Определяем стиль на основе индекса столбца
            if row_idx == 0 or row[0] == '':  # Для заголовков
                current_style = header_style
            elif col_idx == 2:  # Столбец "Услуга"
                current_style = left_style
            elif col_idx == 3:
                current_style = left_style

            else:
                current_style = style  # Для остальных столбцов

            formatted_row.append(Paragraph(str(item), current_style))

        formatted_data.append(formatted_row)

    # Создание таблицы
    table = Table(formatted_data, colWidths=[130, 130, 400, 400, 100])  # Настройте ширину колонок

    # Установка стиля таблицы
    table.setStyle(TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.whitesmoke),  # Заголовок
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.black),  # Цвет текста заголовка
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('TOPPADDING', (0, 0), (-1, 0), 10),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 10),
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
    ]))

    # Применяем серый фон только для строк с родительскими услугами
    row_index = 1  # Чтобы начать с первой строки за пределами заголовка
    for row in data[1:]:  # Начинаем с первой строки данных (игнорируем заголовок)
        if row[0] == '':  # Если первый элемент пустой, это родительская услуга
            table.setStyle(TableStyle([
                ('BACKGROUND', (0, row_index), (-1, row_index), colors.lightgrey),
                ('TOPPADDING', (0, row_index), (-1, row_index), 10),  # Увеличиваем отступ сверху
                ('BOTTOMPADDING', (0, row_index), (-1, row_index), 10),
            ]))

        if row[3] != '':  # Если первый элемент пустой, это родительская услуга
            table.setStyle(TableStyle([
                ('TOPPADDING', (0, row_index), (-1, row_index), 5),  # Увеличиваем отступ сверху
                ('BOTTOMPADDING', (0, row_index), (-1, row_index), 5),
            ]))
        row_index += 1  # Увеличиваем индекс строки

    # Добавляем таблицу в элементы документа
    elements.append(table)

    # Генерация PDF
    doc.build(elements)



# Путь к файлу XML и создание PDF
xml_file_path = xmlPath # Убедитесь, что путь к файлу корректный
pdf_file_name = "example_table_with_prices.pdf"

# Извлечение данных из XML
data = extract_data_from_xml(xml_file_path)

# Создание PDF с таблицей
create_pdf_with_table(pdf_file_name, data)
