<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Приветствие от компании Medall");
$APPLICATION->SetPageProperty("description", "Приветствие Медалл. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic.");
?>
<main>
  <? include __DIR__."/parts/greetings.php" ?>
  <? include __DIR__."/parts/illustration.php" ?>
  <? include __DIR__."/parts/form-appointment.php" ?>
  <? include __DIR__."/parts/branches.php" ?>
  <? include __DIR__."/parts/bottom.php" ?>
</main>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
