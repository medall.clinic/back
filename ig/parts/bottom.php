<section class="page-section page-section--background-color page-section--background-color--gradient container">
	<div class="content">

		<div class="page-subsection">
			<div class="grid grid--align-items--center">
				<div class="grid__cell grid__cell--xs--12 grid__cell--l--8">
					<h2 class="page-subheader">Узнать больше об услугах и врачах</h2>
				</div>
				<div class="grid__cell grid__cell--xs--12 grid__cell--l--4">
					<a class="button button--icon button--icon--link par" href="/">сайт medal
					</a>
				</div>
			</div>
		</div>

		<div class="page-subsection">
			<div class="grid grid--cols--12">
				<div class="grid__cell grid__cell--xs--12 grid__cell--l--8">
					<h2 class="page-subheader">Мы в социальных сетях</h2>
				</div>
				<div class="grid__cell grid__cell--xs--12 grid__cell--l--4">
					<a class="button button--icon button--icon--youtube par" href="https://www.youtube.com/@medallclinic">YouTube
					</a>
					<a class="button button--icon button--icon--tiktok par" href="https://vm.tiktok.com/ZSJWcGp4n/">TikTok
					</a>
          <a class="button button--icon button--icon--vk par" href="https://vk.com/medall.clinic">Вконтакте
          </a>
          <a class="button button--icon button--icon--tg par" href="https://t.me/medall_clinic">Telegram
          </a>
				</div>
			</div>
		</div>

	</div>
</section>
