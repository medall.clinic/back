<section class="page-section page-section--background-color page-section--background-color--gradient container">
	<div class="content">
		<div class="grid grid--cols--12">
			<div class="grid__cell grid__cell--xs--12 grid__cell--s--8 grid__cell--m--6 grid__cell--l--5">
				<h1 class="page-header"><span>Здравствуйте! </span><span>Будем </span><span>рады </span><span>увидеть </span><span>Вас </span><span>в клинике </span><span>MEDALL! </span></h1>
				<p>Оставьте свои контактные данные, с Вами свяжутся в ближайшее время.</p>
			</div>
		</div>
	</div>
</section>
