<section class="page-section container ig-illustration">
	<div class="content">
		<div class="ig-illustration__inner">
			<div class="ig-illustration__image">
				<svg class="ig-illustration__img" viewBox="0 0 391 239" fill="none" xmlns="http://www.w3.org/2000/svg">
					<image href="./images/ig-illustration.jpg" clip-path="url(#ig-illustration-clip)" style="width: 100%"></image>
					<defs>
						<clipPath class="ig-illustration__clip" id="ig-illustration-clip">
							<path d="M391.021 92.4849C399.36 156.612 317.804 221.735 210.496 235.689C103.187 249.642 9.43664 208.969 1.09816 144.842C-7.24032 80.7147 72.9908 17.4181 180.299 3.46462C287.608 -10.4888 382.683 28.358 391.021 92.4849Z" fill="#000"></path>
						</clipPath>
					</defs>
				</svg>
				<svg class="ig-illustration__border" viewBox="0 0 372 267" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M364.604 54.8958C377.544 84.2425 369.387 119.668 345.558 153.271C321.736 186.864 282.301 218.551 232.887 240.341C183.473 262.131 133.481 269.878 92.6128 264.811C51.7328 259.742 20.0762 241.873 7.13615 212.526C-5.80387 183.18 2.35303 147.754 26.1817 114.151C50.0034 80.5586 89.4389 48.8708 138.853 27.081C188.267 5.29124 238.259 -2.45537 279.127 2.61152C320.007 7.67987 351.664 25.5492 364.604 54.8958Z" stroke="#045658"></path>
				</svg>
			</div>
		</div>
	</div>
</section>