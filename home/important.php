<section class="container page-section page-section--background-color page-section--background-color--dark page-section--logo home-important">
	<div class="content">

		<h2 class="page-subsection text-align-center">Для нас важно:</h2>

		<div class="slider slider--full-width home-important__slider page-subsection">
			<div class="slider__slides swiper">
				<div class="slider__wrapper swiper-wrapper">
					<div class="slide swiper-slide home-important__slide">
						<p>Провести качественную диагностику, предложив несколько планов лечения</p>
					</div>
					<div class="slide swiper-slide home-important__slide">
						<p>Подарить эмоции  и впечатления  от общения  с нашей командой  и локацией</p>
					</div>
					<div class="slide swiper-slide home-important__slide">
						<p>Сформировать у каждого ценность заботы о себе</p>
					</div>
					<div class="slide swiper-slide home-important__slide">
						<p>Оказать комплексный подход к здоровью  и красоте</p>
					</div>
				</div>
				<div class="slider__pagination"></div>
			</div>
		</div>

	</div>
</section>