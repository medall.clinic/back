<section class="container page-section page-section--background-color page-section--background-color--light page-section--logo home-category-directions">
        <h2 class="home-category-directions__title page-section__title text-align-center page-subsection content">Медицинские услуги
        </h2>
        <div class="slider home-category-directions__slider page-subsection content">
          <div class="slider__slides swiper">
            <div class="slider__wrapper swiper-wrapper">
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">Грудь</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/plastic/uvelichenie-grudi-usluga/">Увеличение груди</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/podtyazhka-grudi-uslugi-/">Подтяжка груди</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/umenshenie-molochnykh-zhelez-uslugi/">Уменьшение молочных желез</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/korrektsiya-soska-i-areoly-/">Коррекция соска и ареолы</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/umenshenie-molochnykh-zhelez-uslugi8660/">Удаление имплантов</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/ginekomastiya-/">Гинекомастия</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-plastic-3_s.webp, /images/directions-plastic-3_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-plastic-3_m.webp, /images/directions-plastic-3_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-plastic-3_s.jpg, /images/directions-plastic-3_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-plastic-3_m.jpg, /images/directions-plastic-3_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-plastic-3.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">Лицо</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/plastic/blefaroplastika-uslugi-/">Блефаропластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/kheyloplastika-/">Хейлопластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/rinoplastika/">Ринопластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/udalenie-komkov-bisha-/">Удаление комков Биша</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/otoplastika-/">Отопластика (пластика ушей)</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/podtyazhka-litsa-/">Подтяжка кожи лица и шеи</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/mentoplastika-/">Ментопластика</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-plastic-1_s.webp, /images/directions-plastic-1_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-plastic-1_m.webp, /images/directions-plastic-1_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-plastic-1_s.jpg, /images/directions-plastic-1_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-plastic-1_m.jpg, /images/directions-plastic-1_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-plastic-1.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">Тело</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/plastic/lipomodelirovanie-uslugi/">Липомоделирование</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/uvelichenie-yagodits-implantami-/">Пластика ягодиц</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/abdominoplastika-uslugi-/">Абдоминопластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/liposaktsiya-tela-uslugi-/">Липосакция тела</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/lipofiling-tela-uslugi-/">Липофилинг тела</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/umenshenie-talii/">Уменьшение талии</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/brakhioplastika-uslugi-/">Брахиопластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/plastic/labioplastica-/">Интимная пластика</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-plastic-2_s.webp, /images/directions-plastic-2_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-plastic-2_m.webp, /images/directions-plastic-2_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-plastic-2_s.jpg, /images/directions-plastic-2_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-plastic-2_m.jpg, /images/directions-plastic-2_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-plastic-2.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">аппаратные методики</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/mikrotoki/">Микротоковая терапия</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/LPG/">LPG</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/lift6medall/">LIFT 6</a>
                            </li>
                            <? /* <li class="category-direction__item"><a href="/cosmetology/laserbiorev/">Безинъекционная лазерная биоревитализация</a>
                            </li> */?>
                            <li class="category-direction__item"><a href="/cosmetology/LDM1/">Ультрафонофорез LDM</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/emtone/">Emtone</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/hydra_peel/">HydraPeel</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/m22/">М22</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/smas_lifting/">SMAS - лифтинг</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/heleo/">Heleo</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/rf-lifting/">Rf-лифтинг</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/smas_mpt/">ULTRAFORMER МРТ</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-1_s.webp, /images/directions-cosmetology-1_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-1_m.webp, /images/directions-cosmetology-1_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-1_s.jpg, /images/directions-cosmetology-1_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-1_m.jpg, /images/directions-cosmetology-1_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-1.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">уходовые процедуры</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/piling/">Химический пилинг</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/ultrazvukchistka/">Чистка лица</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-2_s.webp, /images/directions-cosmetology-2_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-2_m.webp, /images/directions-cosmetology-2_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-2_s.jpg, /images/directions-cosmetology-2_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-2_m.jpg, /images/directions-cosmetology-2_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-2.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">лазерные методики</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/lasersosudov/">Лазерное лечение сосудов</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/laserepil/">Лазерная эпиляция</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/lasershlif/">Лазерная шлифовка CO2</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/laserudalenie/">Лазерное удаление новообразований</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-3_s.webp, /images/directions-cosmetology-3_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-3_m.webp, /images/directions-cosmetology-3_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-3_s.jpg, /images/directions-cosmetology-3_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-3_m.jpg, /images/directions-cosmetology-3_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-3.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">инъекционные методики</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/biorevitalization/">Биоревитализация</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/kollagen/">Коллагенотерапия</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/botox/">Ботулинотерапия</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/mesotherapy/">Мезотерапия</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/plazmolifting/">Плазмолифтинг</a>
                            </li>
                            <? /* <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/placentaltherapy/">Плацентарная терапия</a>
                            </li> */?>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/lipolitiki/">Липолитики</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/tredlifting/">Тредлифтинг</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/inekcionnaya/konturnaya-plastika/">Контурная пластика</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-4_s.webp, /images/directions-cosmetology-4_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-4_m.webp, /images/directions-cosmetology-4_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-4_s.jpg, /images/directions-cosmetology-4_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-4_m.jpg, /images/directions-cosmetology-4_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-4.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">трихология</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/mezoherapyvolos/">Мезотерапия для волос</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/plasmatherapyvolos/">Плазмотерапия для волос</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/mikrotokivolos/">Микротоковая терапия для волос</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-5_s.webp, /images/directions-cosmetology-5_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-5_m.webp, /images/directions-cosmetology-5_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-5_s.jpg, /images/directions-cosmetology-5_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-5_m.jpg, /images/directions-cosmetology-5_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-5.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">остеопатия и мануальная терапия</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/manualnayaterapia/">Мануальная терапия</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/osteopatiya/">Остеопатия</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-6_s.webp, /images/directions-cosmetology-6_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-6_m.webp, /images/directions-cosmetology-6_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-6_s.jpg, /images/directions-cosmetology-6_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-6_m.jpg, /images/directions-cosmetology-6_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-6.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">подготовка к операции и реабилитация</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/cosmetology/podgotovka-k-operatsii/">Подготовка к операции</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/interoperatsionnaya-kosmetologiya/">Интероперационная косметология</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/reabilitatsiya-posle-operatsii/">Реабилитация после операции</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-7_s.webp, /images/directions-cosmetology-7_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-7_m.webp, /images/directions-cosmetology-7_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-7_s.jpg, /images/directions-cosmetology-7_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-7_m.jpg, /images/directions-cosmetology-7_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-7.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">лабораторная диагностика</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/laboratorymedall/">Лабораторная диагностика</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-cosmetology-8_s.webp, /images/directions-cosmetology-8_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-8_m.webp, /images/directions-cosmetology-8_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-cosmetology-8_s.jpg, /images/directions-cosmetology-8_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-cosmetology-8_m.jpg, /images/directions-cosmetology-8_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-cosmetology-8.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ТЕРАПИЯ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/restoration/">Эстетическая реставрация зубов</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/endodontiya/">Эндодонтия</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/otbelivanie-zubov/">Отбеливание зубов</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/professionalnaya-gigiena/">Профессиональная гигиена</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/lecheniekariesa/">Лечение кариеса под микроскопом</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-1_s.webp, /images/directions-stomatology-1_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-1_m.webp, /images/directions-stomatology-1_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-1_s.jpg, /images/directions-stomatology-1_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-1_m.jpg, /images/directions-stomatology-1_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-1.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ПАРОДОНТОЛОГИЯ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/emdogain/">Emdogain</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/lechenie-stomatita-/">Лечение стоматита</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/lechenie-gingivita-/">Лечение гингивита</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-2_s.webp, /images/directions-stomatology-2_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-2_m.webp, /images/directions-stomatology-2_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-2_s.jpg, /images/directions-stomatology-2_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-2_m.jpg, /images/directions-stomatology-2_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-2.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ОРТОДОНТИЯ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/nevidimaya-breket-sistema-sistema-incognito-/">Невидимая брекет-система (система Incognito™)</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/kapy-3d-smile/">Капы 3D Smile</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/ispravleniye%20vestibulyarnoy%20breket-sistemi/">Исправление вестибулярной брекет-системой (наружные брекеты)</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/elaynery-invisiling-/">Элайнеры Invisiling</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/elaynery/">Элайнеры Flexiligner</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/tens-terapiya/">TENS-терапия</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-3_s.webp, /images/directions-stomatology-3_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-3_m.webp, /images/directions-stomatology-3_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-3_s.jpg, /images/directions-stomatology-3_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-3_m.jpg, /images/directions-stomatology-3_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-3.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ОРТОПЕДИЯ И ПРОТЕЗИРОВАНИЕ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/protezirovanie-koronkami/">Протезирование коронками</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/protezirovanie-vkladkami/">Протезирование вкладками</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/viniry/">Виниры</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-4_s.webp, /images/directions-stomatology-4_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-4_m.webp, /images/directions-stomatology-4_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-4_s.jpg, /images/directions-stomatology-4_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-4_m.jpg, /images/directions-stomatology-4_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-4.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ЛЕЧЕНИЕ ПОД НАРКОЗОМ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/lechenie-pod-narkozom/">Лечение под наркозом</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-5_s.webp, /images/directions-stomatology-5_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-5_m.webp, /images/directions-stomatology-5_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-5_s.jpg, /images/directions-stomatology-5_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-5_m.jpg, /images/directions-stomatology-5_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-5.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ХИРУРГИЯ И ИМПЛАНТАЦИЯ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/udaleniezubov/">Удаление зубов</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/kostnaya-plastika/">Костная пластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/implantatsiya-all-on-6-/">Имплантация All-on-6</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/all-on-4/">Имплантация All-on-4</a>
                            </li>
                            <li class="category-direction__item"><a href="/dentistry/implantatsiya-zuba/">Одномоментная имплантация</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-6_s.webp, /images/directions-stomatology-6_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-6_m.webp, /images/directions-stomatology-6_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-6_s.jpg, /images/directions-stomatology-6_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-6_m.jpg, /images/directions-stomatology-6_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-6.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ДЕТСКАЯ СТОМАТОЛОГИЯ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/lechenie-vremennykh-zubov/">Детская стоматология</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-7_s.webp, /images/directions-stomatology-7_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-7_m.webp, /images/directions-stomatology-7_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-7_s.jpg, /images/directions-stomatology-7_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-7_m.jpg, /images/directions-stomatology-7_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-7.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ИССЛЕДОВАНИЯ И ЦИФРОВЫЕ ТЕХНОЛОГИИ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/dentistry/klkt/">КЛКТ</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-stomatology-8_s.webp, /images/directions-stomatology-8_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-8_m.webp, /images/directions-stomatology-8_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-stomatology-8_s.jpg, /images/directions-stomatology-8_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-stomatology-8_m.jpg, /images/directions-stomatology-8_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-stomatology-8.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ИНЪЕКЦИОННЫЕ ПРОЦЕДУРЫ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/plastic/kontur-intim/">Интимная контурная пластика</a>
                            </li>
                            <li class="category-direction__item"><a href="/gynecology/intimnyy-plazmolifting/">Плазмотерапия</a>
                            </li>
                            <li class="category-direction__item"><a href="/cosmetology/biorevitalization-intim/">Интимная биоревитализация</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-gynecology-1_s.webp, /images/directions-gynecology-1_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-1_m.webp, /images/directions-gynecology-1_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-gynecology-1_s.jpg, /images/directions-gynecology-1_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-1_m.jpg, /images/directions-gynecology-1_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-gynecology-1.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">АППАРАТНЫЕ ПРОЦЕДУРЫ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/gynecology/laser-intim/">Лазерное омоложение</a>
                            </li>
                            <li class="category-direction__item"><a href="/gynecology/uzi/">УЗИ</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-gynecology-2_s.webp, /images/directions-gynecology-2_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-2_m.webp, /images/directions-gynecology-2_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-gynecology-2_s.jpg, /images/directions-gynecology-2_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-2_m.jpg, /images/directions-gynecology-2_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-gynecology-2.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">ЛЕЧЕНИЕ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/gynecology/poslerodovoe-vosstanovlenie/">Послеродовое восстановление</a>
                            </li>
                            <li class="category-direction__item"><a href="/gynecology/lechenie-nederzhaniya-mochi/">Лечение недержания мочи</a>
                            </li>
                            <li class="category-direction__item"><a href="/gynecology/lechenie-vaginizma/">Лечение вагинизма</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-gynecology-3_s.webp, /images/directions-gynecology-3_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-3_m.webp, /images/directions-gynecology-3_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-gynecology-3_s.jpg, /images/directions-gynecology-3_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-3_m.jpg, /images/directions-gynecology-3_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-gynecology-3.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
              <div class="category-direction swiper-slide home-category-directions__slide">
                <div class="category-direction__title">УХОДОВЫЕ ПРОЦЕДУРЫ</div>
                <div class="category-direction__menu form-input form-input--select">
                  <div class="category-direction__menu-name form-input__current">Виды услуг</div>
                      <ul class="category-direction__list form-input__options">
                            <li class="category-direction__item"><a href="/gynecology/otbelivanie-intimnoy-zony/">Интимное отбеливание</a>
                            </li>
                            <li class="category-direction__item"><a href="/gynecology/intimnye-regenerativnye-patchi/">Интимные патчи</a>
                            </li>
                            <li class="category-direction__item"><a href="/gynecology/intimnyy-piling/">Интимный пилинг</a>
                            </li>
                      </ul>
                </div>
                <picture>
                  <source srcset="/images/directions-gynecology-4_s.webp, /images/directions-gynecology-4_s@2x.webp 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-4_m.webp, /images/directions-gynecology-4_m@2x.webp 2x" media="(min-width: 720px)"/>
                  <source srcset="/images/directions-gynecology-4_s.jpg, /images/directions-gynecology-4_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/directions-gynecology-4_m.jpg, /images/directions-gynecology-4_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/directions-gynecology-4.jpg" loading="lazy" decoding="async"/>
                </picture>
              </div>
            </div>
            <div class="slider__arrow slider__arrow--prev"></div>
            <div class="slider__arrow slider__arrow--next"></div>
            <div class="slider__pagination"></div>
          </div>
        </div>
      </section>
