<section class="container page-section page-section--logo page-section--background-color page-section--background-color--light home-staff">
	<div class="content">

		<h2 class="page-section__title page-subsection text-align-center">Наши специалисты</h2>

		<div class="form-input form-input--select form-input--secondary page-subsection home-staff__select page-subsection home-staff__select">
			<select class="form-input__field">
				<option value="all">Все направления</option>
				<option value="plastic">Пластическая хирургия</option>
				<option value="dentistry">Стоматология</option>
				<option value="cosmetology">Косметология</option>
				<option value="hair_transplant">Пересадка волос</option>
				<option value="phlebology">Флебология</option>
			</select>
			<span class="label form-input__label">Все направления</span>
		</div>

        <div class="slider page-subsection home-staff__slider">
			<div class="slider__slides swiper">
                <? include __DIR__."/staff-component.php" ?>
			</div>
		</div>

	</div>
</section>