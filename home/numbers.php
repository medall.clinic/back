<section class="container page-section page-section--logo page-section--logo--center page-section--background-color">
	<div class="content">

		<p class="home-description page-subsection">Наша миссия — улучшить качество жизни человека через комплексную заботу о своем здоровье.</p>

		<div class="home-facts page-subsection">

			<div class="home-facts__item">
				<div class="home-facts__title">>16</div>
				<div class="home-facts__description">лет работы</div>
			</div>

			<div class="home-facts__item">
				<div class="home-facts__title">>40к</div>
				<div class="home-facts__description">довольных пациентов</div>
			</div>

			<div class="home-facts__item">
				<div class="home-facts__title">>50</div>
				<div class="home-facts__description">экспертов</div>
			</div>

			<div class="home-facts__item">
				<div class="home-facts__title">4.9</div>
				<div class="home-facts__description">средний рейтинг на независимых площадках</div>
			</div>

		</div>

	</div>
</section>