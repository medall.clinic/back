<section class="container page-section page-section--background-color home-header">
	<div class="content home-header__inner">

		<h1 class="home-header__title" >MEDALL – место, где рождается эстетика лица, тела и улыбки</h1>
		<a class="button home-header__button" href="#home-form">Получить консультацию</a>

	</div>
</section>
