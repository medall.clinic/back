<section class="container page-section page-section--background-color home-reasons inverted">

    <h2 class="home-reasons__title page-section__title text-align-center content">почему мы?</h2>

    <div class="slider slider--full-width home-reasons__slider">
        <div class="slider__slides swiper">
            <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-reasons__slide">
                    <picture>
                        <source srcset="/images/home-reasons-1_s.webp?v=1, /images/home-reasons-1_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-1_m.webp, /images/home-reasons-1_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-reasons-1_s.jpg, /images/home-reasons-1_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-1_m.jpg, /images/home-reasons-1_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-reasons__image" src="/images/home-reasons-1.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                    </picture>
                    <div class="home-reasons__name">ЭСТЕТИКА УДОБСТВО БЕЗОПАСНОСТЬ</div>
                    <div class="home-reasons__description">В MEDALL атмосферно. На каждом этапе посещения клиники Вы пребываете в комфорте и эстетике, настраиваясь на волну преображения и заботы о себе.</div>
                </div>
                <div class="slide swiper-slide home-reasons__slide">
                    <picture>
                        <source srcset="/images/home-reasons-2_s.webp?v=1, /images/home-reasons-2_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-2_m.webp, /images/home-reasons-2_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-reasons-2_s.jpg, /images/home-reasons-2_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-2_m.jpg, /images/home-reasons-2_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-reasons__image" src="/images/home-reasons-2.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                    </picture>
                    <div class="home-reasons__name">ЭКСПЕРТНОСТЬ КОМАНДА КОМПЛЕКСНАЯ РАБОТА</div>
                    <div class="home-reasons__description">Большая команда экспертов из областей эстетической медицины и цифровой стоматологии объединяет свои знания и навыки для достижения ключевой цели — здоровый и гармоничный пациент.</div>
                </div>
                <div class="slide swiper-slide home-reasons__slide">
                    <picture>
                        <source srcset="/images/home-reasons-3_s.webp?v=1, /images/home-reasons-3_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-3_m.webp, /images/home-reasons-3_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-reasons-3_s.jpg, /images/home-reasons-3_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-3_m.jpg, /images/home-reasons-3_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-reasons__image" src="/images/home-reasons-3.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                    </picture>
                    <div class="home-reasons__name">ЗАБОТА<br>СЕРВИС ИНДИВИДУАЛЬНОСТь</div>
                    <div class="home-reasons__description">За каждым пациентом в клинике прикрепляется персональный куратор. Мы всегда напомним о приёме, предложим вкусный кофе и чай и проводим к доктору. После операции вызовем Вам такси бизнес-класса и подарим персональный подарок.</div>
                </div>
                <div class="slide swiper-slide home-reasons__slide">
                    <picture>
                        <source srcset="/images/home-reasons-4_s.webp?v=1, /images/home-reasons-4_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-4_m.webp, /images/home-reasons-4_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-reasons-4_s.jpg, /images/home-reasons-4_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-reasons-4_m.jpg, /images/home-reasons-4_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-reasons__image" src="/images/home-reasons-4.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                    </picture>
                    <div class="home-reasons__name">БРЕНД СМЫСЛЫ ЦЕННОСТИ</div>
                    <div class="home-reasons__description">Мы отличаемся от многих на рынке медицинских услуг. Да, мы продаем не просто услугу, а ценность и мотивацию вести здоровый образ жизни, совершенствоваться и любить себя.</div>
                </div>
            </div>
            <div class="slider__arrow slider__arrow--prev"></div>
            <div class="slider__arrow slider__arrow--next"></div>
            <div class="slider__pagination"></div>
        </div>
    </div>

</section>
