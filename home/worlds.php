<section class="container page-section page-section--background-color slider-world">
        <div class="slider slider--full-width slider-world__slider">
          <div class="slider__slides swiper">
            <div class="slider__wrapper swiper-wrapper">
              <div class="slide swiper-slide slider-world__slide slider-world__slide--1">
                <picture>
                  <source srcset="/images/category/plastic/slider-worlds-1_s.webp, /images/category/plastic/slider-worlds-1_s@2x.webp 2x" type="image/webp" media="(max-width: 719px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-1_l.webp, /images/category/plastic/slider-worlds-1_l@2x.webp 2x" type="image/webp" media="(min-width: 720px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-1_s.jpg, /images/category/plastic/slider-worlds-1_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-1_l.jpg, /images/category/plastic/slider-worlds-1_l@2x.jpg 2x" media="(min-width: 720px)"/><img class="slider-world__image" src="/images/category/plastic/slider-worlds-1.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                </picture>
                <div class="slider-world__description">MEDALL — больше, чем качественная медицина. Это новое отношение к себе, трансформации и поиск смыслов.<br>Мы за новую философию в мире эстетики и назвали её:</div><img class="slider-world__logo" src="/images/category/plastic/slider-world-logo.svg" loading="lazy" decoding="async" alt=""/>
                <div class="slider-world__description">Она о том, как важен баланс эволюции внутреннего и внешнего мира каждого человека.</div>
              </div>
              <div class="slide swiper-slide slider-world__slide slider-world__slide--2">
                <picture>
                  <source srcset="/images/category/plastic/slider-worlds-2_s.webp, /images/category/plastic/slider-worlds-2_s@2x.webp 2x" type="image/webp" media="(max-width: 719px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-2_l.webp, /images/category/plastic/slider-worlds-2_l@2x.webp 2x" type="image/webp" media="(min-width: 720px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-2_s.jpg, /images/category/plastic/slider-worlds-2_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-2_l.jpg, /images/category/plastic/slider-worlds-2_l@2x.jpg 2x" media="(min-width: 720px)"/><img class="slider-world__image" src="/images/category/plastic/slider-worlds-2.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                </picture>
                <div class="slider-world__description">Наша цель — обратить внимание общества на то, что стремиться к внешней красоте также ценно и правильно, как и стремиться к глубокому духовному внутреннему миру. Тело и разум неразделимы и выбирать между умом и красотой больше не нужно.</div>
              </div>
              <div class="slide swiper-slide slider-world__slide slider-world__slide--3 inverted">
                <picture>
                  <source srcset="/images/category/plastic/slider-worlds-3_s.webp, /images/category/plastic/slider-worlds-3_s@2x.webp 2x" type="image/webp" media="(max-width: 719px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-3_l.webp, /images/category/plastic/slider-worlds-3_l@2x.webp 2x" type="image/webp" media="(min-width: 720px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-3_s.jpg, /images/category/plastic/slider-worlds-3_s@2x.jpg 2x" media="(max-width: 719px)"/>
                  <source srcset="/images/category/plastic/slider-worlds-3_l.jpg, /images/category/plastic/slider-worlds-3_l@2x.jpg 2x" media="(min-width: 720px)"/><img class="slider-world__image" src="/images/category/plastic/slider-worlds-3.jpg" loading="lazy" decoding="async" alt="" width="1920" height="885"/>
                </picture>
                <div class="slider-world__description">За годы работы мы убедились, что наши пациенты — глубокие, интересные и сильные личности, готовые к переменам в жизни. Мы служим им и помогаем проявить внутреннюю красоту во внешнюю. Откликается?</div>
                <a class="button slider-world__button" href="/hr/">Увидеть больше
                </a>
              </div>
            </div>
            <div class="slider__arrow slider__arrow--prev"></div>
            <div class="slider__arrow slider__arrow--next"></div>
            <div class="slider__pagination"></div>
          </div>
        </div>
      </section>
