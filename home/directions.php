<section class="container page-section page-section--background-color home-directions inverted">

    <h2 class="home-directions__title page-section__title text-align-center content">Медицинские направления</h2>

    <div class="slider slider--full-width home-directions__slider">
        <div class="slider__slides swiper">
            <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-directions__slide">
                    <picture>
                        <source srcset="/images/home-directions-1_s.webp?v=1, /images/home-directions-1_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-1_m.webp, /images/home-directions-1_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-directions-1_s.jpg, /images/home-directions-1_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-1_m.jpg, /images/home-directions-1_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-directions__image" src="/images/home-directions-1.jpg" loading="lazy" decoding="async" alt=""/>
                    </picture>
                    <div class="home-directions__info">
                        <div class="home-directions__name">Цифровая стоматология</div>
                        <div class="home-directions__description">Комплексный подход, экстра уровень услуг. Топ-10 клиник России.</div>
                        <a class="button home-directions__button" href="/dentistry/">Перейти к услугам
                        </a>
                    </div>
                </div>
                <div class="slide swiper-slide home-directions__slide">
                    <picture>
                        <source srcset="/images/home-directions-2_s.webp?v=1, /images/home-directions-2_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-2_m.webp, /images/home-directions-2_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-directions-2_s.jpg, /images/home-directions-2_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-2_m.jpg, /images/home-directions-2_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-directions__image" src="/images/home-directions-2.jpg" loading="lazy" decoding="async" alt=""/>
                    </picture>
                    <div class="home-directions__info">
                        <div class="home-directions__name">Пластическая хирургия</div>
                        <div class="home-directions__description">Все виды операций, 15 высококлассных хирургов, комфортабельный стационар и сервис.</div>
                        <a class="button home-directions__button" href="/plastic/">Перейти к услугам
                        </a>
                    </div>
                </div>
                <div class="slide swiper-slide home-directions__slide">
                    <picture>
                        <source srcset="/images/home-directions-3_s.webp?v=1, /images/home-directions-3_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-3_m.webp, /images/home-directions-3_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-directions-3_s.jpg, /images/home-directions-3_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-3_m.jpg, /images/home-directions-3_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-directions__image" src="/images/home-directions-3.jpg" loading="lazy" decoding="async" alt=""/>
                    </picture>
                    <div class="home-directions__info">
                        <div class="home-directions__name">Косметология</div>
                        <div class="home-directions__description">Уходовые, инъекционные и аппаратные методики. Услуги подготовки и реабилитации к операциям.</div>
                        <a class="button home-directions__button" href="/cosmetology/">Перейти к услугам
                        </a>
                    </div>
                </div>
                <div class="slide swiper-slide home-directions__slide">
                    <picture>
                        <source srcset="/images/home-directions-4_s.webp?v=1, /images/home-directions-4_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-4_m.webp, /images/home-directions-4_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-directions-4_s.jpg, /images/home-directions-4_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-4_m.jpg, /images/home-directions-4_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-directions__image" src="/images/home-directions-4.jpg" loading="lazy" decoding="async" alt=""/>
                    </picture>
                    <div class="home-directions__info">
                        <div class="home-directions__name">Пересадка волос</div>
                        <div class="home-directions__description">Опыт работы с 2006 года, свыше 5000 успешных операций, гарантия результата, авторская методика.</div>
                        <a class="button home-directions__button" href="/transplantation/">Перейти к услугам
                        </a>
                    </div>
                </div>
                <div class="slide swiper-slide home-directions__slide">
                    <picture>
                        <source srcset="/images/home-directions-5_s.webp?v=1, /images/home-directions-5_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-5_m.webp, /images/home-directions-5_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-directions-5_s.jpg, /images/home-directions-5_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-5_m.jpg, /images/home-directions-5_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-directions__image" src="/images/home-directions-5.jpg" loading="lazy" decoding="async" alt=""/>
                    </picture>
                    <div class="home-directions__info">
                        <div class="home-directions__name">флебология</div>
                        <div class="home-directions__description">Вылечено более 2000 пациентов, считавшихся безнадёжными. Уникальные авторские методики лечения.</div>
                        <a class="button home-directions__button" href="/phlebology/">Перейти к услугам
                        </a>
                    </div>
                </div>
                <div class="slide swiper-slide home-directions__slide">
                    <picture>
                        <source srcset="/images/home-directions-6_s.webp?v=1, /images/home-directions-6_s@2x.webp?v=1 2x" type="image/webp" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-6_m.webp, /images/home-directions-6_m@2x.webp 2x" type="image/webp" media="(min-width: 761px)"/>
                        <source srcset="/images/home-directions-6_s.jpg, /images/home-directions-6_s@2x.jpg 2x" media="(max-width: 760px)"/>
                        <source srcset="/images/home-directions-6_m.jpg, /images/home-directions-6_m@2x.jpg 2x" media="(min-width: 761px)"/><img class="home-directions__image" src="/images/home-directions-6.jpg" loading="lazy" decoding="async" alt=""/>
                    </picture>
                    <div class="home-directions__info">
                        <div class="home-directions__name">Лабораторная диагностика</div>
                        <div class="home-directions__description">Для удобства пациентов мы осуществляем забор анализов в нашей клинике.</div>
                        <a class="button home-directions__button" href="/laboratorymedall/">Узнать подробнее
                        </a>
                    </div>
                </div>
            </div>
            <div class="slider__arrow slider__arrow--prev"></div>
            <div class="slider__arrow slider__arrow--next"></div>
            <div class="slider__pagination"></div>
        </div>
    </div>

</section>
