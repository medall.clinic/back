<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Оснащение клинике эстетической медицины и цифровой стоматологии Medall.");
$APPLICATION->SetPageProperty("title", "Оснащение клиники Medall");
$APPLICATION->SetTitle("Оснащение клиники");

// Получим все разделы
$arSections = array();
\Bitrix\Main\Loader::IncludeModule('iblock');
$resSections = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => 10), false, Array('ID','NAME'));
while($arSection = $resSections->fetch() ){$arSections[$arSection["ID"]] = $arSection["NAME"];}

// получим все направления
$arDirection = array();
$res =  CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 10), false, false, Array("PROPERTY_direction_equipment"));
while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();
	$arDirection[$arFields['PROPERTY_DIRECTION_EQUIPMENT_ENUM_ID']] = $arFields['PROPERTY_DIRECTION_EQUIPMENT_VALUE'];
}

// Задаем фильтр
global $arFilter;
if(!empty($_REQUEST["SECTION_ID"])){$arFilter= Array("SECTION_ID" => $_REQUEST["SECTION_ID"]);}
if(!empty($_REQUEST["PROPERTY_DIRECTION_EQUIPMENT_VALUE"])){$arFilter= Array("PROPERTY_DIRECTION_EQUIPMENT_VALUE" => $_REQUEST["PROPERTY_DIRECTION_EQUIPMENT_VALUE"]);}

?>
<div class="innerPageContent">
	<div class="container">
		<div class="breadCrumbs">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0"
			));?>
		</div>
		<div class="mainContent__title title">
			<h1>Оснащение клиники</h1>
		</div>
		<div class="mainContent__title__desc">
			<p>современные технологии</p>
		</div>
		<div class="blockFilter">
			<form method="get" action="" enctype="multipart/form-data">
				<?foreach($arSections as $k=>$v){?>
					<label class="blockFilter__label">
						<input type="checkbox" value="<?echo $k;?>" name="SECTION_ID" onclick="submit();">
						<span class="blockFilter__checkbox__text"><?echo $v;?></span>
					</label>
				<?}?>
				<div class="select__block">
					<select class="select" name="PROPERTY_DIRECTION_EQUIPMENT_VALUE" onChange="submit();">
						<option value="">Все направления</option>
						<?foreach($arDirection as $k=>$v){
							if(!empty($_REQUEST["PROPERTY_DIRECTION_EQUIPMENT_VALUE"]) && $_REQUEST["PROPERTY_DIRECTION_EQUIPMENT_VALUE"] == $v){
								?><option value="<?echo $v;?>" selected><?echo $v;?></option><?
							} else{
								?><option value="<?echo $v;?>"><?echo $v;?></option><?
							}
						}?>
					</select>
				</div>
			</form>
		</div>
		
		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"equipment", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "equipment",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100000000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "company_equipment",
			1 => "direction_equipment",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "equipment"
	),
	false
);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
