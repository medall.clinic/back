<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Рассрочка в клинике эстетической медицины и цифровой стоматологии Medall.");
$APPLICATION->SetTitle("Документы");?>
<? $pathTemplateLanding = "/bitrix/templates/landing/" ?>
<? $APPLICATION->SetAdditionalCSS($pathTemplateLanding."/styles/style.css?v=2"); ?>
<!-- Документы-->

<div class="mainContent doctorCardPageContainer" style="margin-top: 144px">
    <section class="mainContent__left">
        <div class="mainContent__wrap">

			<? include __DIR__."/include/breadcrumbs.php" ?>

            <div class="innerPageContent__mainBlock">
                <div class="documentsPage__content" style="margin-bottom: 72px">
					<? include __DIR__."/include/component.php" ?>
					<? include __DIR__."/include/form/index.php" ?>
                </div>

            </div>

        </div>
    </section>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
