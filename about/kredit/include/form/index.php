<? $APPLICATION->AddHeadScript("/bitrix/templates/landing/scripts/bitrix24.js"); ?>

<div class="landing-page" style="margin-top: 20px; margin-bottom: 120px; font-size: inherit;">
	<section class="page-section" id="section-contact">

		<div class="page-subsection">
			<h2 style="font-size: 32px; line-height 1.25; font-family: inherit;">Остались вопросы? Оставьте свои контактные данные и мы обязательно свяжемся с Вами.</h2>
		</div>

		<div class="page-subsection">
			<? include __DIR__."/form.php" ?>
		</div>

	</section>
</div>