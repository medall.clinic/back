<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "О клинике эстетической медицины и цифровой стоматологии Medall.");
$APPLICATION->SetPageProperty("title", "О клинике Medall");
$APPLICATION->SetTitle("О клинике Medall");?>
<!-- Главный экран-->
<div class="aboutClinikMainScreen">
	<div class="container">
		<div class="aboutClinikMainScreen__title title">
			<h1>Клиника Medall – всё лучшее в медицине для Вас!</h1>
		</div>
		<div class="aboutClinikMainScreen__desc">
			<p>
				 МЕДАЛЛ – это комплекс современных клиник, <br>
				 работающих в области эстетической косметологии, <br>
				 хирургии и стоматологии.
			</p>
			<p>
				 В состав МЕДАЛЛ входят:
			</p>
		</div>
	</div>
</div>
 <!-- Блок с ссылками-->
<div class="linksBlock linksBlockAbout">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"main_direction",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "main_direction",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array(0=>"PREVIEW_TEXT",1=>"",),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "service_inf",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "6",
		"PROPERTY_CODE" => array(0=>"URL",1=>"",),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N"
	)
);?>
</div>
 <!-- Основной контент-->
<div class="mainContent">
 <section class="mainContent__left">
	<div class="mainContent__wrap">
		<div class="breadCrumbs">
 <a href="#">главная</a>
			- о клинике
		</div>
		<div class="mainContent__title title">
			<h1>О клинике</h1>
		</div>
		<div class="mainContent__title__desc">
			<p>
				 Главная цель работы – создавать красоту, улучшая здоровье.
			</p>
		</div>
		<div class="mainContent__text">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"about",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "about",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "183",
		"FIELD_CODE" => array(0=>"PREVIEW_TEXT",1=>"DETAIL_TEXT",2=>"",),
		"IBLOCK_ID" => "11",
		"IBLOCK_TYPE" => "service_inf",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(0=>"header_about",1=>"link_about",2=>"text_about",3=>"licenses_about",4=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
);?>
			<div class="tour3D">
				<div class="tour3D__title title">
					 3D тур
				</div>
				<div class="tour3D__desc">
					 Взгляните на нашу клинику
				</div>
 <a class="tour3D__button buttonTwo" href="#">Смотреть тур</a>
			</div>
		</div>
	</div>
 </section> <aside class="mainContent__aside mainContent__aside__aboutClinik">
	<div class="mainContent__aside__wrap">
 <section class="mainContent__ArticleBlock mainContent__newsBlock">
		<div class="mainContent__ArticleBlock__title mainContent__text__title">
			 Новости
		</div>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"news_card",
	Array(
		"ACTIVE_DATE_FORMAT" => "d/m/y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("TAGS",""),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("fix","direction",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "RAND",
		"SORT_BY2" => "RAND",
		"SORT_ORDER1" => "RAND",
		"SORT_ORDER2" => "RAND",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		<div class="mainContent__ArticleBlock__banner" style="background-image:url(img/medicalEquipment.jpg);">
			<div class="mainContent__ArticleBlock__banner__title">
				 Медицинское <br>
				 оснащение
			</div>
 <a class="more mainContent__ArticleBlock__banner__link" href="#">Смотреть</a>
		</div>
 </section>
	</div>
 </aside>
</div>
 <!-- Скидка на пластику груди--> <section class="discountBreastPlastic">
<div class="discountBreastPlastic__wrap">
	<div class="discountBreastPlastic__slider">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"stock_home",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "stock_home",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array(0=>"DATE_ACTIVE_FROM",1=>"DATE_ACTIVE_TO",2=>"",),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "stock",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array(0=>"title_action",1=>"img_main_action",2=>"img_bg_action",3=>"",),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N"
	)
);?>
	</div>
	<div class="discountBreastPlastic__controlsWrap">
		<div class="discountBreastPlastic__control discountBreastPlastic__prev">
		</div>
		<div class="discountBreastPlastic__control discountBreastPlastic__next">
		</div>
	</div>
</div>
 </section>
<!-- Передовые технологии--> <section class="onlyHiTech">
<div class="container">
	<div class="onlyHiTech__icon wow animated fadeInUp">
	</div>
	<div class="onlyHiTech__title title wow animated fadeInUp" data-wow-delay="0.5s">
		<h2>Только передовые технологии</h2>
	</div>
	<div class="onlyHiTech__desc wow animated fadeInUp" data-wow-delay="0.8s">
		<p>
			 Новейшее оборудование и комфортабельный стационар для Вас
		</p>
	</div>
 <a class="onlyHiTech__link buttonTwo wow animated fadeInUp" href="/about/equipment/" data-wow-delay="1s">Смотреть оборудование</a>
</div>
 </section>
<!-- Наши врачи-->
<div class="ourDoctors">
	<div class="container">
		<div class="ourDoctors__slider__wrap">
			<div class="ourDoctors__slider">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"doctor_servis",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "doctor_servis",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array(0=>"PREVIEW_TEXT",1=>"",),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "doctor",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array(0=>"io_doctor",1=>"",),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",2=>"",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N"
	)
);?>
			</div>
			<div class="ourDoctors__slider__control__block">
				<div class="ourDoctors__slider__control ourDoctors__slide__prev">
				</div>
				<div class="ourDoctors__slider__control ourDoctors__slide__next">
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
