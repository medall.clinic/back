<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Видео клиники Medall");
$APPLICATION->SetTitle("Видео");
$APPLICATION->SetPageProperty("description", "Видео. Medall - клиника эстетической медицины и цифровой стоматологии, больше 16 лет работы, >40000 довольных пациентов, >50 экспертов, средний рейтинг 4,9. Звоните: +7 (812) 603-02-01 или пишите на почту: admin@medall.clinic.");
?>
<!-- Видео-->
<div class="mainContent innerPageContent innerPageContent--padding">
 <section class="mainContent__left">
	<div class="mainContent__wrap">
		<div class="breadCrumbs">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
		</div>
		<div class="mainContent__title title">
			<h1>Видео</h1>
		</div>
		<div class="innerPageContent__mainBlock">
			<div class="reviewPage__content">
			<?$APPLICATION->IncludeComponent(
				"helpia:filtr.list",
				"",
				Array(
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"IBLOCK_ID" => "25",
					"MESSAGE_404" => "",
					"PROPERTY_CODE" => "direction_video",
					"SET_STATUS_404" => "N",
					"SHOW_404" => "N",
					"USE_SHARE" => "N"
				)
			);?>
				
					 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"video", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "25",
		"IBLOCK_TYPE" => "video",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "video",
			1 => "direction_video",
			2 => "doctor_video",
			3 => "load_video",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "video"
	),
	false
);?>
			</div>
		</div>
	</div>
 </section> <aside class="mainContent__aside pricePage__aside reviewPage__aside">
	<div class="mainContent__aside__wrap">
 <section class="mainContent__ArticleBlock mainContent__ArticleBlock__beforeAfter">
		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"before_after_right",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "before_after_right",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "21",
		"IBLOCK_TYPE" => "before_after",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"Profile_before_after",2=>"fas_before_after",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?> </section> <section class="mainContent__ArticleBlock mainContent__newsBlock">
		<div class="mainContent__ArticleBlock__title mainContent__text__title">
			 Новости
		</div>
		 <?$APPLICATION->IncludeComponent("bitrix:news.list", "video_news", Array(
	"ACTIVE_DATE_FORMAT" => "22/02/07",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"COMPONENT_TEMPLATE" => "review_news",
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "TAGS",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "4",	// Код информационного блока
		"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "3",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "fix",
			1 => "direction",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "RAND",	// Поле для первой сортировки новостей
		"SORT_BY2" => "RAND",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "RAND",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "RAND",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
	),
	false
);?>
</section>
		<div class="mainContent__aside__discount">
			<div class="discountBreastPlastic__wrap">
				<div class="aside__discountBreastPlastic__slider">
					 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"stock_prise",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "stock_home",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array(0=>"DATE_ACTIVE_FROM",1=>"DATE_ACTIVE_TO",2=>"",),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "stock",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array(0=>"title_action",1=>"img_main_action",2=>"img_bg_action",3=>"",),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N"
	)
);?>
				</div>
				<div class="discountBreastPlastic__controlsWrap">
					<div class="discountBreastPlastic__control discountBreastPlastic__prev aside">
					</div>
					<div class="discountBreastPlastic__control discountBreastPlastic__next aside">
					</div>
				</div>
			</div>
		</div>
	</div>
 </aside>
</div>
 <!-- Слайдер появляется если разрешение экрана меньше 995px		--> <section class="discountBreastPlastic mainContent__aside__discount__tablets">
<div class="discountBreastPlastic__wrap">
	<div class="discountBreastPlastic__slider">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"stock_prise",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "stock_home",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array(0=>"DATE_ACTIVE_FROM",1=>"DATE_ACTIVE_TO",2=>"",),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "stock",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array(0=>"title_action",1=>"img_main_action",2=>"img_bg_action",3=>"",),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N"
	)
);?>
	</div>
	<div class="discountBreastPlastic__controlsWrap">
		<div class="discountBreastPlastic__control discountBreastPlastic__prev">
		</div>
		<div class="discountBreastPlastic__control discountBreastPlastic__next">
		</div>
	</div>
</div>
 </section>
<!-- Готовы начать-->
<div class="articlePlasticSurgery articlesOnTheTopic">
	<div class="articlePlasticSurgery__item articlePlasticSurgery__textLeft wow animated fadeIn" data-wow-delay="0.9s">
		<div class="articlePlasticSurgery__item__desc articlesOnTheTopic__desc title">
			<p>
				 Готовы начать?
			</p>
		</div>
		<div class="articlePlasticSurgery__item__currentArticles">
			<p>
				 Мы ждем Вашего звонка
			</p>
		</div>
	</div>
	<div class="articlePlasticSurgery__item articlePlasticSurgery__textRight wow animated fadeInRight" data-wow-delay="1.1s">
		<div class="articlesOnTheTopic__callBack">
			<div class="articlesOnTheTopic__callBack__text">
				<p>
					 Позвоните +7 (812) 603-02-01
				</p>
				<p>
					 или оставьте номер телефона и мы вам перезвоним
				</p>
			</div>
 <a class="articlesOnTheTopic__callBack__button buttonTwo call" href="#" onclick="return false">Заказать звонок</a>
		</div>
	</div>
</div>
 <!-- Мнение о клинике--> <section class="weOfferLowPayment opinionAboutClinic">
<div class="container">
	<div class="weOfferLowPayment__icon wow animated fadeInRight" data-wow-delay="0.3s">
	</div>
	<div class="opinionAboutClinic__title title wow animated fadeInLeft" data-wow-delay="0.5s">
		<h2>У вас есть свое мнение о нашей клинике?</h2>
	</div>
	<div class="weOfferLowPayment__desc wow animated fadeInRight" data-wow-delay="0.7s">
		<p>
			 Оставьте свой отзыв. Нам очень важно что Вы думаете о нашей работе.
		</p>
	</div>
 <a class="weOfferLowPayment__link buttonTwo wow animated fadeIn comment" href="#" data-wow-delay="1s" onclick="return false">Оставить отзыв</a>
</div>
 </section> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
