<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контакты клиники эстетической медицины и цифровой стоматологии Medall.");
$APPLICATION->SetPageProperty("title", "Контакты клиники Medall");
$APPLICATION->SetTitle("Контакты клиники Medall");?>
<section class="container page-section">
	<div class="content">
		<h1>Контакты</h1>
		<div class="page-subsection grid">
			<div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
				<div class="page-subsection">
					<?$APPLICATION->IncludeComponent(
						"bitrix:news.detail",
						"contacts",
						array(
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"ADD_ELEMENT_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"BROWSER_TITLE" => "-",
							"CACHE_GROUPS" => "N",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							// "DISPLAY_DATE" => "Y",
							// "DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"ELEMENT_CODE" => "",
							"ELEMENT_ID" => "2234",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"IBLOCK_ID" => "11",
							"IBLOCK_TYPE" => "service_inf",
							"IBLOCK_URL" => "",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"MESSAGE_404" => "",
							"META_DESCRIPTION" => "-",
							"META_KEYWORDS" => "-",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_TITLE" => "Страница",
							"PROPERTY_CODE" => array(
								0 => "",
								1 => "fire_document",
								2 => "diplomas_document",
								3 => "licenses_document",
								4 => "",
							),
							"SET_BROWSER_TITLE" => "Y",
							"SET_CANONICAL_URL" => "N",
							"SET_LAST_MODIFIED" => "N",
							"SET_META_DESCRIPTION" => "Y",
							"SET_META_KEYWORDS" => "Y",
							"SET_STATUS_404" => "N",
							"SET_TITLE" => "Y",
							"SHOW_404" => "N",
							"STRICT_SECTION_CHECK" => "N",
							"USE_PERMISSIONS" => "N",
							"USE_SHARE" => "N",
							"COMPONENT_TEMPLATE" => "document"
						),
						false
					);?>
				</div>
				<div class="page-subsection grid">
					<a class="link-icon link-icon--icon--vk" href="https://vk.com/medall.clinic" target="_blank" rel="nofollow"></a>
					<a class="link-icon link-icon--icon--telegram" href="https://t.me/medall_clinic" target="_blank" rel="nofollow"></a>
					<a class="link-icon link-icon--icon--instagram" href="https://instagram.com/medall.clinic" target="_blank" rel="nofollow"></a>
					<a class="link-icon link-icon--icon--zen" href="https://dzen.ru/id/62569cf18d98716e46246d00" target="_blank" rel="nofollow"></a>
					<a class="link-icon link-icon--icon--youtube" href="https://www.youtube.com/@medallclinic" target="_blank" rel="nofollow"></a>
				</div>
			</div>
			<div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
				<div class="contacts-map" id="contacts-map"></div>
				<script type="text/javascript" data-skip-moving="true" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
				<script data-skip-moving="true">
					let isYandexMapShowed = false;
					document.addEventListener("scroll", function () {
						if (!isYandexMapShowed) {
							ymaps.ready(init);
							isYandexMapShowed = true;
						}
					});
					
					function init() {
						const windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
						let iconSize,
							iconOffset;
					
						if (windowWidth > 780) {
							iconSize = [162, 230];
							iconOffset = [-81, -143.75];
						}
					
						if (windowWidth <= 780) {
							iconSize = [90, 128];
							iconOffset = [-45, -80];
						}
					
						if (windowWidth <= 480) {
							iconSize = [60, 80];
							iconOffset = [-30, -50];
						}
					
						const myMap = new ymaps.Map('contacts-map', {
							center: [59.966217579684, 30.29],
							controls: ['zoomControl'],
							zoom: 12
						}, {
							searchControlProvider: 'yandex#search'
						});
					
						myMap.behaviors.disable('scrollZoom');
					
						const balloon = 'Левашовский пр., д.24, 500 м от ст. Чкаловская'
					
						//Добавление меток на карту
						myMap.geoObjects.add(
							new ymaps.Placemark(
								[59.966217579684, 30.285785228836],
								{
									balloonContent: balloon,
									iconCaption: balloon,
								},
								{
									iconLayout: 'default#image',
									iconImageHref: '/bitrix/templates/tpl_medall/images/common.svg#pin',
									// iconImageHref: '/bitrix/templates/tpl_medall/images/pin.svg', // Своё изображение иконки метки
									iconImageSize: iconSize, // Размеры метки
									iconImageOffset: iconOffset // Смещение левого верхнего угла иконки относительно её "ножки" (точки привязки).
								}
							)
						);
					}
				</script>
				<br>
				<p>Схема проезда:</p>
				<img alt="схема проезда.jpg" src="/upload/medialibrary/229/3ey1lgl2pud6nl0ykhrw7wnqieglto6r.jpg" title="схема проезда.jpg">
			</div>
		</div>
	</div>
</section>
<section class="container page-section">
	<div class="content grid grid--justify--center">
		<h2 class="text-align-center">ОСТАЛИСЬ ВОПРОСЫ ИЛИ ХОТИТЕ ОСТАВИТЬ ОТЗЫВ? ОСТАВЬТЕ СВОИ КОНТАКТНЫЕ ДАННЫЕ И МЫ ОБЯЗАТЕЛЬНО СВЯЖЕМСЯ С ВАМИ.</h2>
		<? include __DIR__."/form.php" ?>
	</div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
