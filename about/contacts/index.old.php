<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контакты клиники эстетической медицины и цифровой стоматологии Medall.");
$APPLICATION->SetTitle("");?><!-- контакты-->
<? $pathTemplateLanding = "/bitrix/templates/landing/" ?>
<? $APPLICATION->SetAdditionalCSS($pathTemplateLanding."/styles/style.css?v=2"); ?>
<div class="contactsPage">
	<div class="container">

		<div class="breadCrumbs">
			 <?$APPLICATION->IncludeComponent(
				"bitrix:breadcrumb",
				"",
				Array(
					"PATH" => "",
					"SITE_ID" => "s1",
					"START_FROM" => "0"
				)
			);?>
		</div>

		<style media="screen">
			.contacts {
				display: flex;
				flex-wrap: wrap;
				margin: -10px;
			}
			.conatcts__left,
			.conatcts__right {
				width: 600px;
				min-width: 50%;
				max-width: 100%;
				padding: 10px;
			}
		</style>
		<div class="contacts">
			<div class="conatcts__left">
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.detail",
					"kredit",
					array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_ELEMENT_CHAIN" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"BROWSER_TITLE" => "-",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						// "DISPLAY_DATE" => "Y",
						// "DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"ELEMENT_CODE" => "",
						"ELEMENT_ID" => "2234",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"IBLOCK_ID" => "11",
						"IBLOCK_TYPE" => "service_inf",
						"IBLOCK_URL" => "",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"MESSAGE_404" => "",
						"META_DESCRIPTION" => "-",
						"META_KEYWORDS" => "-",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Страница",
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "fire_document",
							2 => "diplomas_document",
							3 => "licenses_document",
							4 => "",
						),
						"SET_BROWSER_TITLE" => "Y",
						"SET_CANONICAL_URL" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "Y",
						"SHOW_404" => "N",
						"STRICT_SECTION_CHECK" => "N",
						"USE_PERMISSIONS" => "N",
						"USE_SHARE" => "N",
						"COMPONENT_TEMPLATE" => "document"
					),
					false
				);?>
				<? $APPLICATION->AddHeadScript("/bitrix/templates/landing/scripts/bitrix24.js"); ?>
				<style media="screen">
					.contacts-social {
						display: flex;
						flex-wrap: nowrap;
						align-items: center;
						margin: -10px;
						margin-top: 10px;
					}
					.contacts-social__item {
						margin: 10px;
					}
					.contacts-social__item a {
						border-bottom: none;
					}
					.contacts-social__item a use{
						fill: #bfe0ca;
					}
					.contacts-social__item a:hover use{
						fill: rgb(90, 137, 136);
					}
				</style>
				<div class="contacts-social">		<div class="contacts-social__item">
							<a href="https://www.instagram.com/medall.clinic/"><svg class="footer__social__item__svg inst" width="33" height="33" fill="#fff">
				 <use xlink:href="#instagram"></use>
				                                </svg></a>
						</div>		<div class="contacts-social__item">
							<a href="https://www.youtube.com/channel/UC3g-SUaeaT7wH5adRuUZjMg"><svg class="footer__social__item__svg youtube" width="50" height="50">
				                                    <use xlink:href="#youtube"></use>
				                                </svg></a>
						</div>		<div class="contacts-social__item">
							<a href="https://vm.tiktok.com/ZSJWcGp4n/ "><svg class="footer__social__item__svg inst" width="33" height="33">
				 <use xlink:href="#tiktok" ></use>
				                                </svg></a>
						</div></div>
				<div class="landing-page" style="margin-top: 20px; margin-bottom: 120px; font-size: inherit;">
					<section class="page-section" id="section-contact">

						<div class="page-subsection">
							<h2 style="font-size: 32px; line-height 1.25; font-family: inherit;">Остались вопросы? Оставьте свои контактные данные и мы обязательно свяжемся с Вами.</h2>
						</div>

						<div class="page-subsection">
                            <? include __DIR__."/form.php" ?>
                        </div>

					</section>
				</div>
			</div>
			<div class="conatcts__right">
					<div class="contactsPageMap" id="contactsPageMap">
			</div>
		</div>


		 <? /* $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"contacts",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "contacts",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("Address",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
); */ ?>
	</div>
</div>

</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
