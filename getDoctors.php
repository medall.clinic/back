<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
$data = [];
$previewText = '';
$doctorIds = [];
$arFieldsDirectionSection = [];
$arFieldsSelectSection = ['NAME', 'ID','CODE',];
$arIdServiceSectionElements = [];
$arIdServiceElements = [];
$arDoctors = [];
$doctorDesc = [];
$jsonData = '';
$arrException = ['Флебология', 'Гинекология', 'Пересадка волос', 'Рассрочка'];
$addedNames = array();
// Получение разделов
if (CModule::IncludeModule("iblock")){
    $rsGeneralPageSection = CIBlockSection::GetByID(433);
    if ($arParentSection = $rsGeneralPageSection->GetNext()) {
        $arFilterSection = [
            'IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
            '!=NAME' => $arrException,
            '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
            '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
            '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
        ];
        $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilterSection, false, $arFieldsSelectSection);
        while ($arSect = $rsSect->GetNext()) {
            $arFieldsDirectionSection[] = $arSect;
        }
    }

// Получение ID услуг
    $sectionIds = [160, 159,179];
    foreach ($sectionIds as $sectionId) {
        $rsGeneralPageSection = CIBlockSection::GetByID($sectionId);
        if ($arParentSection = $rsGeneralPageSection->GetNext()) {
            $arFilterSection = [
                'IBLOCK_ID' => 47,
                'ACTIVE' => 'Y',
                '!=NAME' => $arrException,
                '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
                '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
            ];
            $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilterSection, false, $arFieldsSelectSection);
            while ($arSect = $rsSect->GetNext()) {
                $arIdServiceSectionElements[] = $arSect['ID'];
            }
        }
    }

// Получение данных услуг
    $arFilter = ['IBLOCK_ID' => 47, 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $arIdServiceSectionElements,];
    $arSelect = ['ID', 'IBLOCK_ID', 'NAME', 'DATE_ACTIVE_FROM', 'DETAIL_PAGE_URL', 'IBLOCK_SECTION_ID',];
    $arFields = [];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNext()) {
        $arFields[] = $ob;
        $arIdServiceElements[] = $ob['ID'];
    }
    $serviceData = [];
    foreach ($arFields as $service) {
        $serviceData[$service['ID']] = [
            'NAME' => $service['NAME'],
            'URL' => $service['DETAIL_PAGE_URL'],
            'IBLOCK_SECTION_ID' => $service['IBLOCK_SECTION_ID'],
            'ID' => $service['ID'],
        ];
    }

// Получение данных врачей
    $arFilterDoctor = [
        'IBLOCK_ID' => 6,
        'ACTIVE' => 'Y',
        'PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE' => $arIdServiceElements,
        'PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE' => $arIdServiceElements,
        'PROPERTY_SPECIALIZATION_DOCTOR_DENTISTRY_VALUE' => $arIdServiceElements,

    ];
    $arSelectDoctor = [
        'ID',
        'IBLOCK_ID',
        'NAME',
        'PREVIEW_TEXT',
        'DETAIL_PAGE_URL',
        'DATE_ACTIVE_FROM',
        'IBLOCK_SECTION_ID',
        'PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC',
        'PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY',
        'PROPERTY_SPECIALIZATION_DOCTOR_DENTISTRY',
        'PROPERTY_IO_DOCTOR',
    ];
    $arFieldsDoctor = [];
    $res = CIBlockElement::GetList([], $arFilterDoctor, false, false, $arSelectDoctor);
    while ($obDoctor = $res->GetNext()) {
        $arFieldsDoctor[] = $obDoctor;
    }
    $doctorData = [];

    foreach ($arFieldsDoctor as $doctor) {
        if (!empty($doctor['PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE']) || !empty($doctor['PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE']) || !empty($doctor['PROPERTY_SPECIALIZATION_DOCTOR_DENTISTRY_VALUE'])){
            $doctorData[] = [
                'URL' => $doctor['DETAIL_PAGE_URL'],
                'IO' => $doctor['PROPERTY_IO_DOCTOR_VALUE'],
                'NAME' => $doctor['NAME'],
                'TEXT' => $doctor['PREVIEW_TEXT'], // Предполагается, что в PREVIEW_TEXT хранится ссылка на страницу врача
                'PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE' => $doctor['PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE'],
                'PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE' => $doctor['PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE'],
                'PROPERTY_SPECIALIZATION_DOCTOR_DENTISTRY_VALUE' => $doctor['PROPERTY_SPECIALIZATION_DOCTOR_DENTISTRY_VALUE'],



            ];
        }



    }

// Формирование массива data

    $data = [];
    $addedNames = [];

    foreach ($arFieldsDirectionSection as $direction) {
        $sectionName = trim($direction['NAME']);
        $data[$sectionName] = [];

        foreach ($serviceData as $serviceId => $service) {

            if (strpos($service['URL'], $direction['CODE']) !== false) {
                $serviceName = trim($service['NAME']);
                $data[$sectionName][$serviceName] = [];

                foreach ($doctorData as $doctorId => $doctor) {

                    if ($doctor['PROPERTY_SPECIALIZATION_DOCTOR_PLASTIC_VALUE'] == $service['ID']) {
                        $addedNames[] = $doctor['NAME'];
                        $doctorName = trim($doctor['NAME']) . ' ' . trim($doctor['IO']);
                        $data[$sectionName][$serviceName][] = [
                            'value' => $doctorName,
                            'label' => $doctorName,
                        ];
                    }

                    if ($doctor['PROPERTY_SPECIALIZATION_DOCTOR_COSMETOLOGY_VALUE'] == $service['ID']) {
                        $doctorName = trim($doctor['NAME']) . ' ' . trim($doctor['IO']);
                        $data[$sectionName][$serviceName][] = [
                            'value' => $doctorName,
                            'label' => $doctorName,
                        ];


                    }

                    if ($doctor['PROPERTY_SPECIALIZATION_DOCTOR_DENTISTRY_VALUE'] == $service['ID']) {
                        $doctorName = trim($doctor['NAME']) . ' ' . trim($doctor['IO']);
                        $data[$sectionName][$serviceName][] = [
                            'value' => $doctorName,
                            'label' => $doctorName,
                        ];


                    }

                }

                // Удаление дубликатов
                foreach ($data as $section => $services) {
                    foreach ($services as $serviceName => $doctors) {
                        $data[$section][$serviceName] = array_map("unserialize", array_unique(array_map("serialize", $doctors)));
                    }
                }
            }
        }
    }

// Преобразуем массив $data в JSON без числовых индексов
    $dataWithoutNumericKeys = [];

    foreach ($data as $section => $services) {
        foreach ($services as $service => $doctors) {
            $dataWithoutNumericKeys[$section][$service] = array_values($doctors);
        }
    }

    $jsonData = json_encode($dataWithoutNumericKeys, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

// Вывод результата для проверки
    echo $jsonData;

}



