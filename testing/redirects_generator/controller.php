<?
require __DIR__."/require_models.php";

//$taskId = "1000025";
$taskId = "1000032";

$refParser = new RefParser;

if($taskId == "1000025"){
	$engine = new GeneratorEngineRewriteRuleComplex;
	$refParser->selfDomain = "https://medall.clinic/";
}else{
	$engine = new GeneratorEngineRewriteRuleSimple;
	$refParser->selfDomain = "http://www.medcentergoroda.ru/";
}


if(!empty($_POST)){
    $refPairs = $refParser->parse($taskId);

	$redirectsGenerator = new RedirectsGenerator;
	$string = $redirectsGenerator->generate($refPairs, $engine);

	$writer = new Writer;
	try{
		$writer->write($string, $taskId);
		$result = "Редиректы сохранены";
	}catch (Exception $e){
		$error = $e->getMessage();
	}
}