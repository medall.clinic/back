<?
class Writer
{
	const PATH = "/../result/result-";

	public function write($string, $taskId)
	{
		$path = __DIR__.self::PATH.$taskId.".txt";
		if(!file_put_contents($path, $string)){
			throw new Exception("Ошибка во время записи в файл");
		}
	}

}