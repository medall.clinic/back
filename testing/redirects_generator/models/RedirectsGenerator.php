<?
class RedirectsGenerator
{
	public function generate($pairs, $engine)
	{
		$string = "";
		foreach ($pairs as $pair){
			$string .= $engine->generateItem($pair);
		}

		return $string;
	}

}