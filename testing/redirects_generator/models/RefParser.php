<?
class RefParser
{
	const REF_PATH = "/../include/ref-";

	public $selfDomain;

	public function parse($taskId)
	{
		$refPath = __DIR__.self::REF_PATH.$taskId.".txt";
		$content = file_get_contents($refPath);
		$strings = explode(PHP_EOL, $content);

		$strings = array_filter($strings);

		$pairs = [];
		foreach ($strings as $string){
			$pairs[] = explode("\t", $string);
		}

		$pairs = $this->trimPairsWithEmptyFromPart($pairs);

		$pairs = array_map(function($pair){
			return $this->trimBaseDomain($pair);
		}, $pairs);

		return $pairs;
	}

	private function trimBaseDomain($pair)
	{
		return array_map(function($pairPart){
			if($pairPart == $this->selfDomain){
				return "/";
			}
			return str_replace($this->selfDomain, "", $pairPart);
		}, $pair);
	}

	private function trimPairsWithEmptyFromPart($pairs)
	{
		$pairs = array_map(function($pair){
			if(empty($pair[0])){
				return false;
			}
			return $pair;
		}, $pairs);

		$pairs = array_filter($pairs);

		return $pairs;
	}

}