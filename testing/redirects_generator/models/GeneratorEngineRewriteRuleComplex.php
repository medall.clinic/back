<?
class GeneratorEngineRewriteRuleComplex
{
	public function generateItem($pair)
	{
		$from = $pair[0];
		$to = $pair[1];

		$fromExplode = explode('?', $from);
		$fromURI = $fromExplode[0];
		$fromParam = $fromExplode[1];

		$string = "RewriteCond %{REQUEST_URI} ".$fromURI."\n";
		$string .= "RewriteCond %{QUERY_STRING} ".$fromParam."$\n";
		$string .= "RewriteRule ^(.*)$	/".$to."? [R=301,L]\n\n";

		return $string;
	}

}