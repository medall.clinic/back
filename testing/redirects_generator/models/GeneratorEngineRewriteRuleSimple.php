<?
class GeneratorEngineRewriteRuleSimple
{
	public function generateItem($pair)
	{
		$from = $pair[0];
		$to = $pair[1];

		$string = "RewriteRule ^(".$from.")$ ".$to." [R=301,L]\n";

		return $string;
	}

}