<? ini_set("display_errors", 1) ?>
<? error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED) ?>
<? $title = "1000025 - Подготовка редиректов на medall" ?>
<? require __DIR__."/controller.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $title ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

	<div class="container">

        <br><h3><?= $title ?></h3><br>

        <form action="" method="post">
            <input type="submit" value="Генерировать редиректы" class="btn btn-info" name="generate">
        </form>

		<? if(!empty($result)): ?>
            <br>
            <div class="alert alert-success">
                <h5 class="alert-heading">Готово!</h5>
            </div>
            <?= $result ?>
		<? endif ?>

		<? if(!empty($error)): ?>
            <br>
            <div class="alert alert-danger">
                <h5 class="alert-heading">Ошибка!</h5>
            </div>
			<?= $error ?>
		<? endif ?>

	</div>


</body>
</html>
