<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Врачи нашей клиники");
$APPLICATION->SetPageProperty("title", "Врачи клиники Medall");
$APPLICATION->SetTitle("Врачи клиники Medall");?>
      <section class="container page-section">
        <div class="content">
          <h1>Врачи</h1>
          <p class="p text-marked">Уникальные специалисты
          </p>
        </div>
      </section>

        <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doctor",
	array(
		"ACTIVE_DATE_FORMAT" => "22/02/07",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "TAGS",
			1 => "",
		),
		"FILTER_NAME" => "arFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "doctor",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "1000000000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "medallnav",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "io_doctor",
			1 => "direction_doctor",
			2 => "quote_doctor",
			3 => "experience_doctor",
			4 => "count_operations_doctor",
			5 => "education_doctor",
			6 => "advanced_training_doctor",
			7 => "direction_doctors",
			8 => "result_doctor",
			9 => "specialization_doctor",
			10 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "doctor",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
<section class="container page-section" id="home-form">
	<div class="content">

		<div class="page-subsection">
			<h2 class="page-section__title text-align-center">Записаться на приём</h2>
			<p class="page-section__subtitle">В комментариях укажите, какая услуга, доктор или вопрос Вас интересует.</p>
		</div>

		<div class="page-subsection grid grid--justify--center">
			<form class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form form-ajax home-form" data-is-validate="true" data-is-answer-in-popup="true" data-answer-success="Спасибо за обращение | Наш администратор свяжется с вами в течении часа чтобы согласовать дату и время консультации">

				<label class="form-input form-input--text form__input">
						<input class="form-input__field" type="text" name="name" data-validation-required="">
						<span class="label form-input__label">ФИО</span>
				</label>

				<label class="form-input form-input--tel form__input">
						<input class="form-input__field" type="tel" name="phone" data-validation-required="">
						<span class="label form-input__label">Телефон</span>
				</label>


				<label class="form-input form-input--textarea form__input">
						<textarea class="form-input__field" name="comment"></textarea>
						<span class="label form-input__label">Комментарии</span>
				</label>

				<button class="button form__submit" type="submit">Оставить заявку</button>

				<div class="form__agreement home-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">обработку персональных данных</a>.</div>
				
				<input type="hidden" name="title" value="Страница врачи">
				<input type="hidden" name="action" value="consultation_register">
			</form>
		</div>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
