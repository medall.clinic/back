<?
\Bitrix\Main\Loader::IncludeModule('iblock');
$servis = Array();
$section_ID_servis = array();
$aMenuLinksExt = Array();


$res = \CIBlockSection::GetList(
	[],
	["IBLOCK_ID" => IBLOCK_ID_SERVICE, "DEPTH_LEVEL" => 1],
	false,
	["ID", "NAME", "CODE"]
);

while($ar_res = $res->Fetch()){

  $skip = array("Пересадка волос", "Флебология");
	if(in_array($ar_res['NAME'], $skip)){
		continue;
	}

	// if($ar_res['NAME'] == "Пересадка волос"){
	// 	continue;
	// }

   $section_ID_servis[$ar_res['ID']] = Array(
		"ID" => $ar_res['ID'], 
		"NAME" => $ar_res['NAME'], 
		"CODE" => $ar_res['CODE'], 
	);
}

foreach($section_ID_servis as $k => $v){
	$intCount = CIBlockSection::GetCount(array('SECTION_ID' => $v['ID']));
	if($intCount!=0){
		$resSections = \CIBlockSection::GetList(Array(), Array('SECTION_ID' => $v['ID'], "ACTIVE"=>"Y"), false, Array('ID','NAME'));
		while($arSection = $resSections->fetch()){
			$servis[$v['NAME']][$arSection['ID']]['NAME']= $arSection['NAME'];
			$res = CIBlockElement::GetList(
				Array(),
				Array("IBLOCK_SECTION_ID"=> $arSection['ID'], "ACTIVE"=>"Y"),
				false,
				false,
				Array('ID', 'NAME', 'CODE')
			);
			while($ob = $res->GetNextElement()){ 
				$arProps = $ob->GetFields();  
				$servis[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['NAME']= $arProps['NAME'];
				$servis[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['CODE']= $v['CODE'];
				$servis[$v['NAME']][$arSection['ID']]['SECTION'][$arProps['ID']]['ELEMENT_CODE']= $arProps['CODE'];
			}
		}
	}
	else{
		$res = CIBlockElement::GetList(
			Array(),
			Array("SECTION_ID"=>$v['ID'], "ACTIVE"=>"Y"),
			false,
			false,
			Array('ID', 'NAME', 'CODE')
		);

		if($res->SelectedRowsCount() > 0){
			while($ob = $res->GetNextElement()){
				$arProps = $ob->GetFields();
				$servis[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['NAME']= $arProps['NAME'];
				$servis[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['CODE']= $v['CODE'];
				$servis[$v['NAME']][$v['ID']]['SECTION'][$arProps['ID']]['ELEMENT_CODE']= $arProps['CODE'];
			}
		}else{
			$servis[$v['NAME']] = [];
		}
	}
}

foreach($servis as $k => $v){
	$aMenuLinksExt[] = Array(
		$k, 
		"", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"), 
		"" 
	);
	foreach($v as $j => $l){
		if($l['NAME']!=''){
			$DEPTH_LEVEL = '3';
			$aMenuLinksExt[] = Array(
				$l['NAME'], 
				"", 
				Array(), 
				Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"2"),
				"" 
			);
		}
		else{
			$DEPTH_LEVEL = '2';
		}

		if(!empty($l['SECTION'])){
			foreach($l['SECTION'] as $h => $r){

				if(!empty($r["ELEMENT_CODE"])){
					$url = $r['CODE']."/".$r["ELEMENT_CODE"]."/";
				}else{
					$url = $r['CODE']."/detail.php?ID=".$h;
				}

				$aMenuLinksExt[] = Array(
					$r['NAME'],
					$url,
					Array(),
					Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>$DEPTH_LEVEL),
					""
				);
			}
		}

	}
}

$aMenuLinksExt[] = [
	"Пересадка волос",
	"https://netvolos.ru",
	[],
	["IS_EXTERNAL" => "1"]
];

$aMenuLinksExt[] = [
	"Флебология",
	"https://yazv.net",
	[],
	["IS_EXTERNAL" => "1"]
];

$aMenuLinksExt[] = [
	"Лабораторная диагностика", 
  "https://medall.space", 
	[],
	["IS_EXTERNAL" => "1"]
];

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
?> 
