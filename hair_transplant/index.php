<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?><!-- Главный экран--> <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"bgvideo",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => 153,
		"FIELD_CODE" => array("",""),
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "service_inf",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array("","video",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
);?> <!--Блок с формой--> <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"consultation_servis",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>

<!-- меню внутренних страниц-->
<section class="menuCategory_block">

<div class="block_title">Услуги</div>
<div class="block_sub-title">Всё лучшее в медицине для Вас!</div>

	<div class="menuCategory">

    <?
    $serviceIBlockId = IBLOCK_ID_SERVICE;
    $filterDirection = [
        "SECTION_ID" => SECTION_ID_SERVICE_HAIR_TRANSPLANT,
        "INCLUDE_SUBSECTIONS" => "Y"
    ];
    if(!empty($_GET["old"])){
        $serviceIBlockId = 15;
        $filterDirection = "";
    }
    ?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"menu_servis",
		Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array("",""),
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",

			//"IBLOCK_ID" => "15",
			"IBLOCK_ID" => $serviceIBlockId,
			"FILTER_NAME" => "filterDirection",

            "IBLOCK_TYPE" => "service",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"INCLUDE_SUBSECTIONS" => "Y",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "20000000",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array("",""),
			"SET_BROWSER_TITLE" => "Y",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "Y",
			"SHOW_404" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"STRICT_SECTION_CHECK" => "N"
		)
	);?>
	</div>
</section>

<!-- Скидка на пластику груди--> <section class="discountBreastPlastic">
<div class="discountBreastPlastic__wrap">
	<div class="discountBreastPlastic__slider">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"stock_home",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array("DATE_ACTIVE_FROM","DATE_ACTIVE_TO",""),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "stock",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array("title_action","direction_action","moreprocedure_action","bg_action","img_main_action","img_bg_action",""),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N"
	)
);?><br>
	</div>
	<div class="discountBreastPlastic__controlsWrap">
		<div class="discountBreastPlastic__control discountBreastPlastic__prev">
		</div>
		<div class="discountBreastPlastic__control discountBreastPlastic__next">
		</div>
	</div>
</div>
 </section>
<!-- Передовые технологии--> <section class="onlyHiTech">
<div class="container">
	<div class="onlyHiTech__icon wow animated fadeInUp">
	</div>
	<div class="onlyHiTech__title title wow animated fadeInUp" data-wow-delay="0.5s">
		<h2>Только передовые технологии</h2>
	</div>
	<div class="onlyHiTech__desc wow animated fadeInUp" data-wow-delay="0.8s">
		<p>
			 Новейшее оборудование и комфортабельный стационар для Вас
		</p>
	</div>
 <a class="onlyHiTech__link buttonTwo wow animated fadeInUp" href="/about/equipment/" data-wow-delay="1s">Смотреть оборудование</a>
</div>
 </section>
<!-- Наши врачи-->
<div class="ourDoctors">
	<div class="container">
		<div class="ourDoctors__slider__wrap">
			<div class="ourDoctors__slider">
				 <?
     $filterDoctorDirectionHairTransplant = ["PROPERTY_direction_doctor" => 15];
     $APPLICATION->IncludeComponent(
	"bitrix:photo.section",
	"doctor_servis",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "filterDoctorDirectionHairTransplant",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "doctor",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Фотографии",
		"PAGE_ELEMENT_COUNT" => "20",
		"PROPERTY_CODE" => array("","photo_doctor"),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N"
	)
);?>
			</div>
			<div class="ourDoctors__slider__control__block">
				<div class="ourDoctors__slider__control ourDoctors__slide__prev">
				</div>
				<div class="ourDoctors__slider__control ourDoctors__slide__next">
				</div>
			</div>
		</div>
	</div>
</div>
 <!-- Отзывы--> <section class="reviews">
<div class="container">
	<div class="reviews__desc wow animated fadeInLeft">
		<p>
			 Мы гордимся результатами
		</p>
	</div>
	<div class="reviews__title title beforeLine wow animated fadeInRight" data-wow-delay="0.5s">
		<h2>Что говорят наши пациенты</h2>
	</div>
	<div class="reviews__slogan wow animated fadeInLeft" data-wow-delay="0.8s">
		<p>
			 Положительные отзывы наших пациентов - доказательство нашей успешной работы
		</p>
	</div>
</div>
<div class="reviews__slider__wrap wow animated fadeInRight" data-wow-delay="0.8s">
	<div class="reviews__slider__block">
		<div class="reviews__slider">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"review_mini",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "17",
		"IBLOCK_TYPE" => "review",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("","doctor_review",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
	</div>
	<div class="reviews__slider__controls__block">
		<div class="reviews__slider__control reviews__slider__control__prev">
		</div>
		<div class="reviews__slider__control reviews__slider__control__next">
		</div>
	</div>
</div>
<div class="container">
	<div class="reviews__link wow animated fadeInLeft" data-wow-delay="0.8s">
 <a class="buttonTwo" href="/review">Все отзывы</a>
	</div>
</div>
 </section>
<!-- Наша философия--> <section class="ourPhilosophy">
<div class="container">
	<div class="ourPhilosophy__block">
		<div class="ourPhilosophy__left wow animated fadeInLeft">
			<div class="ourPhilosophy__left__text">
				<p>
					 Медицинский центр «MEDALL» работает уже более 15 лет и нам доверяют свыше 30 000 пациентов.
				</p>
				<p>
					 Мы развиваемся и стремимся быть лучшими в своем деле, поэтому наши специалисты постоянно повышают свою квалификацию.
				</p>
				<p>
					 В Клинике применяется только новейшее диагностическое и лечебное оборудование, передовые медицинские технологии и материалы.
				</p>
			</div>
		</div>
		<div class="ourPhilosophy__right wow animated fadeInRight">
			<div class="ourPhilosophy__title title">
				<h2>Наша философия</h2>
			</div>
			<div class="ourPhilosophy__desc">
				<p>
					 Основной принцип нашей работы – предлагать пациенту не отдельную методику, а комплексную программу коррекции. Поэтому мы собрали команду профессионалов, которые дополняют друг друга и способны решить любую проблему по профильным медицинским направлениям Клиники.
				</p>
				<p>
					 Открытый, доверительный контакт между врачом и пациентом - незаменимая основа для достижения успешного результата. В Клинике создана уютная и практически семейная атмосфера. Врачи работают как единая команда, нацеленная на удовлетворение индивидуальных потребностей каждого пациента.
				</p>
				<p>
					 MEDALL - все лучшее в медицине для Вас!
				</p>
			</div>
			<div class="ourPhilosophy__link">
 <a class="button" href="/about">О клинике</a>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- Статья про пластику груди-->
<div class="articlePlasticSurgery">
	<div class="articlePlasticSurgery__item">
		<div class="articlePlasticSurgery__item__links">
 <a class="articlePlasticSurgery__item__title Modeling wow animated fadeIn" href="/" data-wow-delay="0.3s" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/bg/3dModeling.jpg);">3D моделирование</a>
			<a class="articlePlasticSurgery__item__title result wow animated fadeIn" href="/before_after/" data-wow-delay="0.5s" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/bg/beforeAndAfter.jpg);">До и после</a>
		</div>
	</div>
	<div class="articlePlasticSurgery__item">
		<div class="articlePlasticSurgery__item__video wow animated fadeIn" data-wow-delay="0.7s">
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"articles_mini",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "articles",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("display_page_articles",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
</div>
 <!-- Мы предлагаем низкий платеж--> <section class="weOfferLowPayment">
<div class="container">
	<div class="weOfferLowPayment__icon wow animated fadeInUp" data-wow-delay="0.3s">
	</div>
	<div class="weOfferLowPayment__title title wow animated fadeInUp" data-wow-delay="0.5s">
		<h2>Мы предлагаем низкие ежемесячные платежи</h2>
	</div>
	<div class="weOfferLowPayment__desc wow animated fadeInUp" data-wow-delay="0.7s">
		<p>
			 Быстро, легко и безопасно. Мы предоставляем комфортные программы кредитования/рассрочки при оптимальных условиях. Сотрудники нашей Клиники могут помочь вам подобрать приемлемую для вас программу финансирования выбранных медицинских процедур.
		</p>
	</div>
 <a class="weOfferLowPayment__link buttonTwo wow animated fadeInUp" href="/" data-wow-delay="1s">Условия получения</a>
</div>
 </section> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>