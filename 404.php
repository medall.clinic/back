<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Not Found");
$APPLICATION->SetPageProperty('robots', 'noindex');
?>

<section class="container page-section page-section--logo page-section--background-color page-section--background-color--light error-page">
    <div class="content error-page__content">
        <h1 class="error-page__title">404
        </h1>
        <p class="h1 error-page__subtitle">К сожалению, страница не найдена
        </p>
        <p class="error-page__info">Мы не можем найти страницу, которую вы искали, но у нас есть много других интересных материалов и предложений.</p>
        <a class="button p" href="/">Вернуться на главную
        </a>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
