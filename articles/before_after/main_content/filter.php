<div class="blockFilter">
	<form method="get" action="" enctype="multipart/form-data">
		<div class="select__block">
			<select class="select" name="PROPERTY_DOCTOR_BEFORE_AFTER" onchange="submit();">
				<option value="">Все врачи</option>
				<? foreach($arDOCTOR as $k => $v): ?>
					<? if(!empty($_REQUEST["PROPERTY_DOCTOR_BEFORE_AFTER"]) && $_REQUEST["PROPERTY_DOCTOR_BEFORE_AFTER"] == $k): ?>
						<option value="<?= $k ?>" selected><?= $v['NAME'] ?></option>
                    <? else: ?>
						<option value="<?= $k ?>"><?= $v['NAME'] ?></option>
                    <? endif ?>
                <? endforeach?>
			</select>
		</div>
	</form>
</div>