<?php
$APPLICATION->SetTitle("");

// получим всех врачей
$arDOCTOR = array();
$res =  CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 21), false, false, Array("PROPERTY_DOCTOR_BEFORE_AFTER"));
while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();
	$arFilter1 = Array("ID"=>$arFields['PROPERTY_DOCTOR_BEFORE_AFTER_VALUE']);
	$res1 = CIBlockElement::GetList(Array(), $arFilter1);
	if ($ob = $res1->GetNextElement()){
		$arFields1 = $ob->GetFields(); // поля элемента
		$arProps = $ob->GetProperties(); // свойства элемента
		$arDOCTOR[$arFields1['ID']]['NAME'] = $arFields1['NAME'];
		$arDOCTOR[$arFields1['ID']]['io_doctor'] = $arProps['io_doctor']['VALUE'];
	}
}


$arRez = array();
// Получим все инфоблоки
$arIblocks = CIBlock::GetList(Array(), Array('TYPE'=>'service', 'SITE_ID'=>'s1', 'ACTIVE'=>'Y', "CNT_ACTIVE"=>"Y"), true);
while($arIblock = $arIblocks->Fetch()) {
	$arRez[$arIblock['ID']]['NAME'] = $arIblock['NAME'];
	$arRez[$arIblock['ID']]['CODE'] = $arIblock['CODE'];
	// Получим все разделы
	$resSections = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => $arIblock['ID']), false, Array('ID','NAME'));
	if ($resSections->SelectedRowsCount()==0){
		//Получить все элементы
		$arElements = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> $arIblock['ID'], "ACTIVE"=>"Y"), false, false, Array('ID','NAME'));
		while($ob = $arElements->GetNextElement()){
			$arElement = $ob->GetFields();
			$arRez[$arIblock['ID']]["elements"][$arElement["ID"]]=$arElement["NAME"];
		}
	}
	else{
		while($arSection = $resSections->GetNext()){
			$arRez[$arIblock['ID']]['sections'][$arSection["ID"]]["NAME"] = $arSection["NAME"];
			//Получить все элементы
			$arElements = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> $arIblock['ID'],"IBLOCK_SECTION_ID"=> $arSection["ID"], "ACTIVE"=>"Y"), false, false, Array('ID','NAME'));
			while($ob = $arElements->GetNextElement()){
				$arElement = $ob->GetFields();
				$arRez[$arIblock['ID']]['sections'][$arSection["ID"]]["elements"][$arElement["ID"]]=$arElement["NAME"];
			}
		}
	}
}

// Задаем фильтр
global $arFilter;

if(!empty($_REQUEST["PROPERTY_DOCTOR_BEFORE_AFTER"])){
	$arFilter= Array("PROPERTY_DOCTOR_BEFORE_AFTER" => $_REQUEST["PROPERTY_DOCTOR_BEFORE_AFTER"]);
}

if(!empty($_REQUEST["PROPERTY_DIRECTION_BEFORE_AFTER_VALUE"])){
	$direction = $_REQUEST["PROPERTY_DIRECTION_BEFORE_AFTER_VALUE"];

	if($direction == "Пластика"){
		$arFilter = ["PROPERTY_DIRECTION_BEFORE_AFTER" => 43];
	}

	if($direction == "Косметология"){
		$arFilter = ["PROPERTY_DIRECTION_BEFORE_AFTER" => 44];
	}

	if($direction == "Флебология"){
		$arFilter = ["PROPERTY_DIRECTION_BEFORE_AFTER" => 45];
	}

	if($direction == "Пересадка волос"){
		$arFilter = ["PROPERTY_DIRECTION_BEFORE_AFTER" => 46];
	}

	if($direction == "Стоматология"){
		$arFilter = ["PROPERTY_DIRECTION_BEFORE_AFTER" => 47];
	}

}