<aside class="mainContent__aside">
	<div class="mainContent__aside__wrap">
		<div class="beforeAfterPage__asideFilter">
			<form method="get" action="" enctype="multipart/form-data">

				<?
				$curSectionCode = getBeforeAfterCurSectionCode($_GET);
				$curElementCode = getBeforeAfterCurElementCode($_GET);
				?>

				<? foreach($arRez as $k => $v): ?>

					<div class="beforeAfterPage__asideFilter__link">

						<a
							class="beforeAfterPage__asideFilter__title <?= $v["CODE"] == $curSectionCode ? "active" : "" ?>"
							href="#"
						><?= $v['NAME'] ?></a>

						<div class="beforeAfterPage__asideFilter__block" <?= $v["CODE"] == $curSectionCode ? "style='display: block'" : "" ?>>
							<? $val = strtoupper("property_procedure_".$v['CODE']."_before_after") ?>
							<? if($v['sections']): ?>

								<? foreach($v['sections'] as $j => $l): ?>
									<div class="beforeAfterPage__asideFilter__block__desc"><?= $l['NAME'] ?>:</div>
									<? foreach($l['elements'] as $h => $d): ?>
										<label class="blockFilter__label">
											<input
												type="checkbox"
												value="<?= $h ?>"
												name="<?= $val ?>"
												onclick="submit();"
											>
											<span class="blockFilter__checkbox__text <?= ($h == $curElementCode) ? "active" : "" ?>"><?= $d ?></span>
										</label>
									<? endforeach ?>
								<? endforeach ?>

							<? else: ?>

								<? foreach($v['elements'] as $j => $l):?>
									<label class="blockFilter__label">
										<input
											type="checkbox"
											value="<?= $h ?>"
											name="<?= $val ?>"
											onclick="submit();"
										>
										<span class="blockFilter__checkbox__text <?= ($h == $curElementCode) ? "active" : "" ?>"><?= $l ?></span>
									</label>
								<? endforeach ?>

							<? endif?>
						</div>
					</div>

				<? endforeach ?>
			</form>
		</div>
	</div>
</aside>
