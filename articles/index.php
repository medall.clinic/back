<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Статьи клиники эстетической медицины и цифровой стоматологии Medall.");
$APPLICATION->SetPageProperty("title", "Блог клиники Medall");
$APPLICATION->SetTitle("Статьи");

// Задаем фильтр
if(!empty($_REQUEST["TAGS"])){$arFilter= Array("TAGS" => $_REQUEST["TAGS"]);}
?><!-- Статьи-->
<?php $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/include/breadcrumb_inc.php"
    )
);?>
<section class="container page-section">
    <div class="content">
        <div class="page-subsection">
            <h1>Статьи</h1>
            <p class="text-marked">ВСЕ САМОЕ ИНТЕРЕСНОЕ И ПОЛЕЗНОЕ</p>
        </div>

        <div class="articles-layout page-subsection">
            <div class="articles-layout__filter page-subsection">
                <div class="form-input form-input--select form-input--secondary articles-layout__select">
                    <?php $APPLICATION->IncludeComponent(
                        "helpia:filtr.list",
                        "helpia",
                        array(
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "IBLOCK_ID" => "9",
                            "MESSAGE_404" => "",
                            "PROPERTY_CODE" => "direction_articles",
                            "SET_STATUS_404" => "N",
                            "SHOW_404" => "N",
                            "USE_SHARE" => "N",
                            "COMPONENT_TEMPLATE" => "helpia",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO"
                        ),
                        false
                    ); ?>


            </div>
            <div class="grid page-subsection articles-layout__list">
                <?php $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "articles",
                    array(
                        "ACTIVE_DATE_FORMAT" => "22/02/07",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(
                            0 => "TAGS",
                            1 => "",
                        ),
                        "FILTER_NAME" => "arFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "9",
                        "IBLOCK_TYPE" => "articles",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "9",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => $_REQUEST["ID"],
                        "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "fix",
                            2 => "direction",
                            3 => "",
                        ),
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "COMPONENT_TEMPLATE" => "articles",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                    false
                ); ?>
            </div>
        </div>

    </div>
    </div>
</section>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
