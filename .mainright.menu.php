<?
$aMenuLinks = Array(
  Array(
		"Документы",
		"about/document/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"0", "DEPTH_LEVEL"=>"1", "IS_EXTERNAL" => "1"),
		""
	),
  Array(
		"Рассрочка",
		"rassrochka/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"0", "DEPTH_LEVEL"=>"1", "IS_EXTERNAL" => "1"),
		""
	),
   /* Array(
      "О клинике",
      "/about/",
      Array(),
      Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
      ""
   ),
   Array(
		"О клинике",
		"about/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Оборудование",
		"about/equipment",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Новости",
		"about/news/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Документы",
		"about/document",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),Array(
		"Вопрос-ответ",
		"about/faq",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),Array(
		"Видео",
		"about/video",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
  Array(
		"Расценки",
		"about/price",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	), */

	Array(
		"Наши врачи",
		"doctor/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
		""
	),
	Array(
		"Пластикаа",
		"/doctor/?PROPERTY_DIRECTION_DOCTOR_VALUE=Пластика",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Косметология",
		"/doctor/?PROPERTY_DIRECTION_DOCTOR_VALUE=Косметология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Флебология",
		"/doctor/?PROPERTY_DIRECTION_DOCTOR_VALUE=Флебология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Пересадка волос",
		"/doctor/?PROPERTY_DIRECTION_DOCTOR_VALUE=Пересадка+волос",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Стоматология",
		"/doctor/?PROPERTY_DIRECTION_DOCTOR_VALUE=Стоматология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),

	/* Array(
		"Отзывы",
		"review/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
		""
	),
	Array(
		"Стоматология",
		"/review/?direction=Стоматология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Пластика",
		"/review/?direction=Пластика",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Косметология",
		"/review/?direction=Косметология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),

	Array(
		"Пересадка волос",
		"/review/?direction=Пересадка+волос",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Флебология",
		"/review/?direction=Флебология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),


	Array(
		"До и после",
		"before_after/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
		""
	),
	Array(
		"Пластика",
		"/before_after/?PROPERTY_DIRECTION_BEFORE_AFTER_VALUE=Пластика",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Косметология",
		"/before_after/?PROPERTY_DIRECTION_BEFORE_AFTER_VALUE=Косметология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Флебология",
		"/before_after/?PROPERTY_DIRECTION_BEFORE_AFTER_VALUE=Флебология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Пересадка волос",
		"/before_after/?PROPERTY_DIRECTION_BEFORE_AFTER_VALUE=Пересадка+волос",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Стоматология",
		"/before_after/?PROPERTY_DIRECTION_BEFORE_AFTER_VALUE=Стоматология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	), */
	// Array(
	// 	"Статьи",
	// 	"articles/",
	// 	Array(),
	// 	Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
	// 	""
	// ),
	Array(
		"Пластика",
		"/articles/?PROPERTY_DIRECTION_ARTICLES_VALUE=Пластика",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Косметология",
		"/articles/?PROPERTY_DIRECTION_ARTICLES_VALUE=Косметология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Флебология",
		"/articles/?PROPERTY_DIRECTION_ARTICLES_VALUE=Флебология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Пересадка волос",
		"/articles/?PROPERTY_DIRECTION_ARTICLES_VALUE=Пересадка+волос",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Стоматология",
		"/articles/?PROPERTY_DIRECTION_ARTICLES_VALUE=Стоматология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
  Array(
		"Вакансии",
		"hr/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"0", "DEPTH_LEVEL"=>"1", "IS_EXTERNAL" => "1"),
		""
	),
	/* Array(
		"Акции",
		"stock/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"),
		""
	),
	Array(
		"Пластика",
		"/stock/?PROPERTY_DIRECTION_ACTION_VALUE=Пластика",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Косметология",
		"/stock/?PROPERTY_DIRECTION_ACTION_VALUE=Косметология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Флебология",
		"/stock/?PROPERTY_DIRECTION_ACTION_VALUE=Флебология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Пересадка волос",
		"/stock/?PROPERTY_DIRECTION_ACTION_VALUE=Пересадка+волос",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	),
	Array(
		"Стоматология",
		"/stock/?PROPERTY_DIRECTION_ACTION_VALUE=Стоматология",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		""
	), */
	Array(
		"Контакты",
		"about/contacts/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"0", "DEPTH_LEVEL"=>"1", "IS_EXTERNAL" => "1"),
		""
	),
	Array(
		"Пациентам",
		"patsientam/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"0", "DEPTH_LEVEL"=>"1", "IS_EXTERNAL" => "1"),
		""
	),
	Array(
		"Акции",
		"discounts/",
		Array(),
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"0", "DEPTH_LEVEL"=>"1", "IS_EXTERNAL" => "1"),
		""
	),
);
?>
