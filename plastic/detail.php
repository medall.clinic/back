<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");

$elementCode = $_REQUEST["CODE"];
$elementCodeExplode = explode('?', $elementCode);
$elementCode = reset($elementCodeExplode);

$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"card_servis",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => $elementCode,
		"ELEMENT_ID" => "",
		"FIELD_CODE" => array("PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE",""),
		"IBLOCK_ID" => "12",
		"IBLOCK_TYPE" => "service",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array("title_addtext1_card","addtext1_card","title_addtext2_card","addtext2_card","title_addtext3_card","addtext3_card","subtitle_card","promo_header_card","promo_link_card","photogallery_subtitle_card","photogallery_link_card","text_box1_card","text_box2_card","text_box3_card","text_box4_card","text_box5_card","d_header_card","d_subtitle_card","d_link_card","slider_card","img_addtext1_card","img_addtext2_card","img_addtext3_card","tiles_service_card","articles_card","photogallery_img_card","promo_img_card","sale_card","faq_card","related_articles_card","img_related_articles_card","articles_box1_card","articles_box2_card","articles_box3_card","articles_box4_card","articles_box5_card","d_link_img","doctor_directions","before_and_after"),
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>