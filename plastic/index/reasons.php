<section class="container page-section page-section--background-color home-reasons inverted">
	<h2 class="home-reasons__title page-section__title text-align-center content">Пластическая хирургия MEDALL это:
	</h2>
	<div class="slider slider--full-width home-reasons__slider">
		<div class="slider__slides swiper">
			<div class="slider__wrapper swiper-wrapper">
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-1.webp, /images/category/plastic-reasons-1@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-1.jpg, /images/category/plastic-reasons-1@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-1.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">Высоко квалифицированные хирурги</div>
					<a class="button home-reasons__button" href="#">Выбрать врача
					</a>
					<div class="home-reasons__description"> Наши врачи регулярно повышают свою квалификацию как в России, так и за рубежом. Являются действующими членами известных профильных ассоциаций и участниками международных конгрессов. Их опыт подтверждён десятками тысяч оказанных услуг. Каждый пациент сможет выбрать в MEDALL лучшего для себя специалиста.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-2.webp, /images/category/plastic-reasons-2@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-2.jpg, /images/category/plastic-reasons-2@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-2.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">3D-моделирование будущего результата</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description"> С помощью инновационной программы Crisalix доктор создаст визуальный образ будущего результата. Благодаря реалистичному моделированию, Вы сможете точно увидеть будущие формы, которые будут соответствовать желаемому результату и особенностям тела.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-3.webp, /images/category/plastic-reasons-3@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-3.jpg, /images/category/plastic-reasons-3@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-3.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">ONLINE и OFFLINE форматы консультаций</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description">Мы ценим Ваше время, а также знаем, что пациенты приезжают к нам на операции с разных уголков России и даже из других стран. Не стоит откладывать визит к доктору! Вы можете познакомиться и обсудить все нюансы онлайн, а затем еще раз перед операцией. У кого же есть возможность приехать к нам лично — будем рады увидеться с Вами как можно скорее.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-4.webp, /images/category/plastic-reasons-4@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-4.jpg, /images/category/plastic-reasons-4@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-4.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">Высокий сервис и комфорт</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description">В MEDALL комфортно и нет ощущения медучреждения в привычном устаревшем смысле. Вам придется по вкусу наш современный минималистичный интерьер. На каждом этапе посещения клиники Вы пребываете в комфорте и эстетике, настраиваясь на волну преображения и заботы о себе.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-5.webp, /images/category/plastic-reasons-5@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-5.jpg, /images/category/plastic-reasons-5@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-5.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">Ваш персональный куратор</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description">После консультации у доктора, за каждым пациентом в клинике прикрепляется персональный куратор. Он будет с Вами на связи по всем предоперационным вопросам, в день операции и при выписке. Пациенты MEDALL отмечают, как важна и помогает им эта поддержка.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-6.webp, /images/category/plastic-reasons-6@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-6.jpg, /images/category/plastic-reasons-6@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-6.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">Комфортабельные палаты</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description">Для каждого пациента предусмотрена индивидуальная палата с круглосуточным медицинским наблюдением, ортопедической кроватью, мобильной кнопкой вызова персонала, туалетной комнатой с гигиеническим душем и необходимыми принадлежностями (халат, тапочки, полотенца), сейфом, Wi-Fi, телевизором с онлайн-кинотеатром, с набором для чая/кофемашиной.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-7.webp, /images/category/plastic-reasons-7@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-7.jpg, /images/category/plastic-reasons-7@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-7.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">Услуги для реабилитации</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description">Если Вы хотите, чтобы процесс восстановления прошёл комфортнее и быстрее, рекомендуем воспользоваться услугами реабилитации непосредственно в нашей клинике: скорейшее заживление швов, минимизация рубцов, минимизация отёков и болевых ощущений. Только после рекомендаций Вашего доктора.</div>
				</div>
				<div class="slide swiper-slide home-reasons__slide">
					<picture>
						<source srcset="/images/category/plastic-reasons-8.webp, /images/category/plastic-reasons-8@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/category/plastic-reasons-8.jpg, /images/category/plastic-reasons-8@2x.jpg 2x"/><img class="home-reasons__image" src="/images/category/plastic-reasons-8.jpg" loading="lazy" decoding="async" width="1920" height="885"/>
					</picture>
					<div class="home-reasons__name">Пакет лекарств при выписке</div>
					<a class="button home-reasons__button" href="#">Получить консультацию
					</a>
					<div class="home-reasons__description">Всё, о чем Вы должны заботиться в период реабилитации — это соблюдать рекомендации доктора и думать о будущем результате. Покупку необходимых лекарств мы берем на себя и выдадим вместе с назначением Вашего доктора.</div>
				</div>
			</div>
			<div class="slider__arrow slider__arrow--prev"></div>
			<div class="slider__arrow slider__arrow--next"></div>
			<div class="slider__pagination"></div>
		</div>
	</div>
</section>
