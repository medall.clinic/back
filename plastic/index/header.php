<section class="page-section page-section--background-color page-section--background-color--dark category-header">

	<picture>
		<source srcset="/images/category/category-header_s.webp, /images/category/category-header_s@2x.webp 2x" media="(max-width: 719px)"/>
		<source srcset="/images/category/category-header_m.webp, /images/category/category-header_m@2x.webp 2x" media="(min-width: 720px)"/>
		<source srcset="/images/category/category-header_s.jpg, /images/category/category-header_s@2x.jpg 2x" media="(max-width: 719px)"/>
		<source srcset="/images/category/category-header_m.jpg, /images/category/category-header_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-header__image" src="/images/category/category-header.jpg" loading="lazy" decoding="async"/>
	</picture>

	<h1 class="category-header__title">Пластическая хирургия MEDALL</h1>

	<div class="category-header__info">
		<ul class="p list-unordered">
			<li class="list-unordered__item">высококвалифицированные хирурги
			</li>
			<li class="list-unordered__item">большой опыт и мастерство
			</li>
			<li class="list-unordered__item">современные методы и технологии
			</li>
			<li class="list-unordered__item">персональный куратор
			</li>
			<li class="list-unordered__item">комфортабельный стационар
			</li>
			<li class="list-unordered__item">бизнес-такси после операции
			</li>
			<li class="list-unordered__item">мерч в подарок <span class="text-borders">скоро</span>
			</li>
			<li class="list-unordered__item">реабилитационные услуги
			</li>
		</ul>
	</div>

	<a class="button category-header__button" href="#">Получить консультацию</a>

</section>
