<section class="container page-section page-section--background-color page-section--background-color--light page-section--logo category-directions">
	<h2 class="category-directions__title page-section__title text-align-center page-subsection content">Медицинские направления
	</h2>
	<div class="slider category-directions__slider page-subsection">
		<div class="slider__slides swiper">
			<div class="slider__wrapper swiper-wrapper">
				<div class="category-direction swiper-slide category-directions__slide">
					<div class="category-direction__title">Лицо</div>
					<div class="category-direction__menu form-input form-input--select">
						<div class="category-direction__menu-name form-input__current">Виды услуг</div>
						<ul class="category-direction__list form-input__options">
							<li class="category-direction__item"><a href="#">Блефаропластика</a>
							</li>
							<li class="category-direction__item"><a href="#">Хейлопластика</a>
							</li>
							<li class="category-direction__item"><a href="#">Ринопластика</a>
							</li>
							<li class="category-direction__item"><a href="#">Удаление комков Биша</a>
							</li>
							<li class="category-direction__item"><a href="#">Отопластика (пластика ушей)</a>
							</li>
							<li class="category-direction__item"><a href="#">Подтяжка кожи лица и шеи</a>
							</li>
							<li class="category-direction__item"><a href="#">Ментопластика</a>
							</li>
						</ul>
					</div>
					<picture>
						<source srcset="/images/category/directions-plastic-1_s.webp, /images/category/directions-plastic-1_s@2x.webp 2x" media="(max-width: 719px)"/>
						<source srcset="/images/category/directions-plastic-1_m.webp, /images/category/directions-plastic-1_m@2x.webp 2x" media="(min-width: 720px)"/>
						<source srcset="/images/category/directions-plastic-1_s.jpg, /images/category/directions-plastic-1_s@2x.jpg 2x" media="(max-width: 719px)"/>
						<source srcset="/images/category/directions-plastic-1_m.jpg, /images/category/directions-plastic-1_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/category/directions-plastic-1.jpg" loading="lazy" decoding="async"/>
					</picture>
				</div>
				<div class="category-direction swiper-slide category-directions__slide">
					<div class="category-direction__title">Тело</div>
					<div class="category-direction__menu form-input form-input--select">
						<div class="category-direction__menu-name form-input__current">Виды услуг</div>
						<ul class="category-direction__list form-input__options">
							<li class="category-direction__item"><a href="#">Липомоделирование</a>
							</li>
							<li class="category-direction__item"><a href="#">Абдоминопластика</a>
							</li>
							<li class="category-direction__item"><a href="#">Липосакция тела</a>
							</li>
							<li class="category-direction__item"><a href="#">Липофилинг тела</a>
							</li>
							<li class="category-direction__item"><a href="#">Увеличение ягодиц</a>
							</li>
							<li class="category-direction__item"><a href="#">Интимная пластика</a>
							</li>
							<li class="category-direction__item"><a href="#">Брахиопластика</a>
							</li>
						</ul>
					</div>
					<picture>
						<source srcset="/images/category/directions-plastic-2_s.webp, /images/category/directions-plastic-2_s@2x.webp 2x" media="(max-width: 719px)"/>
						<source srcset="/images/category/directions-plastic-2_m.webp, /images/category/directions-plastic-2_m@2x.webp 2x" media="(min-width: 720px)"/>
						<source srcset="/images/category/directions-plastic-2_s.jpg, /images/category/directions-plastic-2_s@2x.jpg 2x" media="(max-width: 719px)"/>
						<source srcset="/images/category/directions-plastic-2_m.jpg, /images/category/directions-plastic-2_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/category/directions-plastic-2.jpg" loading="lazy" decoding="async"/>
					</picture>
				</div>
				<div class="category-direction swiper-slide category-directions__slide">
					<div class="category-direction__title">Грудь</div>
					<div class="category-direction__menu form-input form-input--select">
						<div class="category-direction__menu-name form-input__current">Виды услуг</div>
						<ul class="category-direction__list form-input__options">
							<li class="category-direction__item"><a href="#">Увеличение груди</a>
							</li>
							<li class="category-direction__item"><a href="#">Подтяжка груди</a>
							</li>
							<li class="category-direction__item"><a href="#">Уменьшение молочных желез</a>
							</li>
							<li class="category-direction__item"><a href="#">Коррекция соска и ареолы</a>
							</li>
							<li class="category-direction__item"><a href="#">Удаление имплантов</a>
							</li>
							<li class="category-direction__item"><a href="#">Гинекомастия</a>
							</li>
						</ul>
					</div>
					<picture>
						<source srcset="/images/category/directions-plastic-3_s.webp, /images/category/directions-plastic-3_s@2x.webp 2x" media="(max-width: 719px)"/>
						<source srcset="/images/category/directions-plastic-3_m.webp, /images/category/directions-plastic-3_m@2x.webp 2x" media="(min-width: 720px)"/>
						<source srcset="/images/category/directions-plastic-3_s.jpg, /images/category/directions-plastic-3_s@2x.jpg 2x" media="(max-width: 719px)"/>
						<source srcset="/images/category/directions-plastic-3_m.jpg, /images/category/directions-plastic-3_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="category-direction__image" src="/images/category/directions-plastic-3.jpg" loading="lazy" decoding="async"/>
					</picture>
				</div>
			</div>
			<div class="slider__pagination"></div>
		</div>
	</div>
</section>
