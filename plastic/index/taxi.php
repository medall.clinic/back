<section class="page-section page-section--background-color page-section--background-color--dark banner-taxi">
	<div class="banner-taxi__title">Бизнес-такси до дома</div>
	<div class="banner-taxi__info">После выписки Ваш персональный куратор бесплатно закажет такси бизнес-класса до выбранного адреса. А также проводит до автомобиля и поможет комфортно разместиться в нём.</div>
	<a class="button banner-taxi__button" href="#">Записаться на приём
	</a>
	<picture>
		<source srcset="/images/category/banner-taxi_s.webp, /images/category/banner-taxi_s@2x.webp 2x" media="(max-width: 719px)"/>
		<source srcset="/images/category/banner-taxi_m.webp, /images/category/banner-taxi_m@2x.webp 2x" media="(min-width: 720px)"/>
		<source srcset="/images/category/banner-taxi_s.jpg, /images/category/banner-taxi_s@2x.jpg 2x" media="(max-width: 719px)"/>
		<source srcset="/images/category/banner-taxi_m.jpg, /images/category/banner-taxi_m@2x.jpg 2x" media="(min-width: 720px)"/><img class="banner-taxi__image" src="/images/category/banner-taxi.jpg" loading="lazy" decoding="async"/>
	</picture>
</section>
