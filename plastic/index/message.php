<section class="container page-section page-section--background-color page-section--background-color--dark page-section--logo banner-message">
	<div class="content banner-message__inner">
		<h2 class="page-section__title banner-message__title text-align-center">от первого лица
		</h2>
		<div class="banner-message__info">
			<p>Здравствуйте, я генеральный директор MEDALL — Дмитрий Евгеньевич Метелев. За годы работы в сфере эстетической медицины, она открыла для меня новые личные и профессиональные смыслы, о которых я даже и не подозревал 16 лет назад. Когда в 2007 году, я врач-хирург, выпускник Военно-медицинской академии начал развивать направление эстетической медицины, то относился к нему скорее по-предпринимательски, больше как к бьюти-бизнесу, чем как к серьезной отрасли медицины. </p>
			<p>Казалось, что косметология, пластическая хирургия и пересадка волос — не настоящие отрасли медицины. Они ничего не лечат, жизни не спасают, пользы здоровью не приносят, а главное в них нет места глобальной миссии врача. Но на этом можно было начать зарабатывать. Внутренний рационалист победил и я запустил это направление. </p>
			<p>С годами я сделал вывод: пластическая хирургия — это не стыдно. Стремиться быть красивым и видеть в этом важную часть своей жизни — не стыдно. Пластика действительно помогает людям решить те проблемы, которые они не заслужили, помогает вернуть качество, а часто и смысл жизни.</p>
		</div>
		<div class="banner-message__name"> Метелев Д.Е.</div>
		<picture>
			<source srcset="/images/category/banner-message_s.webp, /images/category/banner-message_s@2x.webp 2x" media="(max-width: 719px)"/>
			<source srcset="/images/category/banner-message_m.webp, /images/category/banner-message_m@2x.webp 2x" media="(min-width: 720px)"/>
			<source srcset="/images/category/banner-message_s.png, /images/category/banner-message_s@2x.png 2x" media="(max-width: 719px)"/>
			<source srcset="/images/category/banner-message_m.png, /images/category/banner-message_m@2x.png 2x" media="(min-width: 720px)"/><img class="banner-message__image" src="/images/category/banner-message.png" alt="" loading="lazy" decoding="async"/>
		</picture>
	</div>
</section>
