<section class="container page-section page-section--logo page-section--background-color page-section--background-color--light home-staff">
	<div class="content">
		<h2 class="page-section__title page-subsection text-align-center">Пластические хирурги
		</h2>
		<div class="slider page-subsection home-staff__slider">
			<div class="slider__slides swiper">
				<div class="slider__wrapper swiper-wrapper">
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">Гогохия Тамара Зауровна</div>
						<div class="home-staff__description">Заведующий отделением пластической хирургии, пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-1.webp" type="image/webp"/>
							<source srcset="/images/doctor-1.jpg"/><img class="home-staff__image" src="/images/doctor-1.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">Романчишен Филипп Анатольевич</div>
						<div class="home-staff__description">Главный врач клиники, пластический хирург, кандидат медицинских наук</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-2.webp" type="image/webp"/>
							<source srcset="/images/doctor-2.jpg"/><img class="home-staff__image" src="/images/doctor-2.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">Мохамед Ева Юсуфовна</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-3.webp" type="image/webp"/>
							<source srcset="/images/doctor-3.jpg"/><img class="home-staff__image" src="/images/doctor-3.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">кизюн яна васильевна</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-4.webp" type="image/webp"/>
							<source srcset="/images/doctor-4.jpg"/><img class="home-staff__image" src="/images/doctor-4.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">ПАЦЮК Алиса Игоревна</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-5.webp" type="image/webp"/>
							<source srcset="/images/doctor-5.jpg"/><img class="home-staff__image" src="/images/doctor-5.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">АНЧУГОВА Анна Анатольевна</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-6.webp" type="image/webp"/>
							<source srcset="/images/doctor-6.jpg"/><img class="home-staff__image" src="/images/doctor-6.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">ЦЕХМИСТРО Яна Вячеславовна</div>
						<div class="home-staff__description">Пластический хирург, помощник главного врача  по медицинской части</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-7.webp" type="image/webp"/>
							<source srcset="/images/doctor-7.jpg"/><img class="home-staff__image" src="/images/doctor-7.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">БЕЛОБОРОДОВА Екатерина Андреевна</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-8.webp" type="image/webp"/>
							<source srcset="/images/doctor-8.jpg"/><img class="home-staff__image" src="/images/doctor-8.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">АРУТЮНЯН Левон</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-9.webp" type="image/webp"/>
							<source srcset="/images/doctor-9.jpg"/><img class="home-staff__image" src="/images/doctor-9.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">ЖОРЖЕОС Михаил Фавазович</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-10.webp" type="image/webp"/>
							<source srcset="/images/doctor-10.jpg"/><img class="home-staff__image" src="/images/doctor-10.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">ГЕВЛИЧ Елена Константиновна</div>
						<div class="home-staff__description">Врач-оториноларинголог I-ой категории, ЛОР, челюстной-лицевой хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-11.webp" type="image/webp"/>
							<source srcset="/images/doctor-11.jpg"/><img class="home-staff__image" src="/images/doctor-11.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">ВЫСТУПЕЦ Борис Владимирович</div>
						<div class="home-staff__description">Кандидат медицинских наук, ведущий пластический хирург клиники</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-12.webp" type="image/webp"/>
							<source srcset="/images/doctor-12.jpg"/><img class="home-staff__image" src="/images/doctor-12.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">МАЛЬЦЕВА Екатерина Николаевна</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-13.webp" type="image/webp"/>
							<source srcset="/images/doctor-13.jpg"/><img class="home-staff__image" src="/images/doctor-13.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
					<div class="slide swiper-slide home-staff__slide" data-category="plastics">
						<div class="home-staff__name">ЖУРАВЛЕВ Роман Вячеславович</div>
						<div class="home-staff__description">Пластический хирург</div>
						<a class="button home-staff__button" href="#">Подробнее
						</a>
						<picture>
							<source srcset="/images/doctor-14.webp" type="image/webp"/>
							<source srcset="/images/doctor-14.jpg"/><img class="home-staff__image" src="/images/doctor-14.jpg" loading="lazy" decoding="async"/>
						</picture>
					</div>
				</div>
				<div class="slider__arrow slider__arrow--prev"></div>
				<div class="slider__arrow slider__arrow--next"></div>
				<div class="slider__pagination"></div>
			</div>
		</div>
	</div>
</section>
