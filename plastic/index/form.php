<section class="container page-section">
	<div class="content">
		<div class="page-subsection">
			<h2 class="page-section__title text-align-center">Записаться на приём
			</h2>
			<p class="page-section__subtitle">В комментариях укажите, какая услуга, доктор или вопрос Вас интересует.</p>
		</div>
		<div class="page-subsection grid grid--justify--center home-form">
			<form class="grid__cell grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12 form">
				<label class="form-input form-input--text form__input form__input">
					<input class="form-input__field" type="text">
					<span class="label form-input__label">ФИО
                </span>
				</label>
				<label class="form-input form-input--tel form__input form__input">
					<input class="form-input__field" type="tel">
					<span class="label form-input__label">Телефон
                </span>
				</label>
				<label class="form-input form-input--email form__input form__input">
					<input class="form-input__field" type="email">
					<span class="label form-input__label">Электронная почта
                </span>
				</label>
				<label class="form-input form-input--textarea form__input form__input">
					<textarea class="form-input__field"></textarea>
					<span class="label form-input__label">Комментарии
                </span>
				</label>
				<button class="button form__submit" type="submit">Оставить заявку
				</button>
				<div class="form__agreement home-form__agreement">Нажимая  кнопку «Отправить» я даю согласие на <a href="#">обработку персональных данных</a>.</div>
			</form>
		</div>
	</div>
</section>
