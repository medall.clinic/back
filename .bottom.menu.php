<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$aMenuLinks = Array(
	Array(
		"Пластическая хирургия", 
		"plastic/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"), 
		"" 
	),
	Array(
		"Грудь", 
		"plastic/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Тело", 
		"plastic/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Лицо", 
		"plastic/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Медицинская косметология", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"), 
		"" 
	),
	Array(
		"Терапевтическая", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Инъекционная", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Аппаратная", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Лазерная", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Дерматология", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Трихология", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Реабилитация", 
		"cosmetology/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Центр флебологии", 
		"https://yazv.net/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"), 
		"" 
	),
	Array(
		"Лечение трофических язв", 
		"https://yazv.net/lechenie-yazvv", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Лечение варикоза", 
		"https://yazv.net/lechenie-varikoza", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Лечение тромбоза", 
		"https://yazv.net/lechenie-trombov", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Лечение лифедемы", 
		"https://yazv.net/lechenie-limfedem", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Пересадка волос", 
		"https://netvolos.ru/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"), 
		"" 
	),
	Array(
		"Трансплантация волос", 
		"https://netvolos.ru/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Цифровая стоматология", 
		"dentistry/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"1", "DEPTH_LEVEL"=>"1"), 
		"" 
	),
	Array(
		"Лечение зубов", 
		"dentistry/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Удаление зубов", 
		"dentistry/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
	Array(
		"Лечение десен", 
		"dentistry/", 
		Array(), 
		Array("FROM_IBLOCK"=>"1", "IS_PARENT"=>"", "DEPTH_LEVEL"=>"2"),
		"" 
	),
);
?>
