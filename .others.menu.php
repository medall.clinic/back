<?php
$aMenuLinks = [
	[
		'Врачи',
		'/doctor/',
		['/doctor/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 1,
			'DEPTH_LEVEL' => 1,
		],
	],
	[
		'Пластическая хирургия',
		'/doctor/#plastic',
		['/doctor/#plastic'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Косметология',
		'/doctor/#cosmetology',
		['/doctor/#cosmetology'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Стоматология',
		'/doctor/#dentistry',
		['/doctor/#dentistry'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Пересадка волос',
		'/doctor/#hair-transplant',
		['/doctor/#hair-transplant'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Флебология',
		'/doctor/#phlebology',
		['/doctor/#phlebology'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Результаты работ',
		'/rezultaty-rabot/',
		[],
		[],
		'',
	],
	[
		'Цены',
		'/prices/',
		[],
		[],
		'',
	],
	[
		'Акции',
		'/discounts/',
		[],
		[],
		'',
	],
	[
		'Отзывы',
		'/review/',
		[],
		[],
		'',
	],
	[
		'Пациентам',
		'',
		['/patsientam/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 1,
			'DEPTH_LEVEL' => 1,
		],
	],
	[
		'Документы',
		'/about/document/',
		['/about/document/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Рассрочка',
		'/rassrochka/',
		['/rassrochka/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Мерч',
		'/merch/',
		['/merch/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Анкета здоровья',
		'/anketa/',
		['/anketa/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Налоговый вычет',
		'/nalog/',
		['/nalog/'],
		[
			'FROM_IBLOCK' => 1,
			'IS_PARENT' => 0,
			'DEPTH_LEVEL' => 2,
		],
	],
	[
		'Вакансии',
		'/hr/',
		[],
		[],
		'',
	],
	[
		'Контакты',
		'/about/contacts/',
		[],
		[],
		'',
	],
];
